 
   ============================================================================
 
   MODUL       ENT.  EXT.  PRG  FILE
   PR19        0007  0005   A   O.PR19                          
   PRPL        0003  0001   A   O.PRPL                          
   IN/CMD      0003  0000   A   O.IN/CMD;CMD/LESEN/SYSIN        
   OUT         0007  0000   A   O.OUT;OUT/UNTERPROGRAMME        
   PRBU        0001  0000   A   O.PRBU                          
   -------------------------------------------------------
   PROG1       0021  0001   L   O.PROG1                         
 
   ============================================================================
 
   MODUL       Z  D    P    S1   S2   S3   S4   S5   S6   S7   C1   C2   C3   A
   PR19        00 .... 0000 .... .... .... .... .... .... .... 0000 .... .... N
   PRPL        37 .... 0850 .... .... .... .... .... .... .... .... .... .... N
   IN/CMD      4C .... 0EB5 .... .... .... .... .... .... .... .... .... .... N
   OUT         .. .... 10F1 .... .... .... .... .... .... .... .... .... .... N
   PRBU        .. .... 1184 .... .... .... .... .... .... .... .... .... .... N
   ----------------------------------------------------------------------------
   PROG1       00 .... 0000 .... .... .... .... .... .... .... 0000 .... .... N
               4E .... 1185 .... .... .... .... .... .... .... 0028 .... .... 
 
   ============================================================================
 
   SYMBOL      T  SG  ADR   ENTRY IN    EXTERNAL IN
   IN/CMD      Y  P   0F4C  IN/CMD      
   IN/CMD/BUF  Y  P   0EC4  IN/CMD      
   IN/CMD/TMO  Y  P   0F14  IN/CMD      PR19       PRPL       
   OUT/BUF     Y  P   10F1  OUT         
   OUT/END     Y  P   117D  OUT         
   OUT/VOR     Y  P   1149  OUT         
   OUT/VOR0    Y  P   1155  OUT         
   OUT/VOR1    Y  P   115E  OUT         
   OUT/VORA    Y  P   1167  OUT         
   OUT/VORCH   Y  P   1170  OUT         
   PLL/CL      Y  P   0B8F  PRPL        PR19       
   PLL/IP      Y  P   0B94  PRPL        PR19       
   PLL/OP      Y  P   0B7A  PRPL        PR19       
   PROGACT     Y  P   002E  PR19        
   PROGBLNK    Y  P   0639  PR19        
   PROGDACT    Y  P   00CE  PR19        
   PROGENQ     Y  P   0021  PR19        
   PROGLOAD    Y  P   0295  PR19        
   PROGPROG    Y  P   03BF  PR19        
   PROGPROM    Y  P   010A  PR19        
   RAM/BUFFER  Y  P   1185  PRBU        
 U STOR/COMP   X                        PR19       
 
   ============================================================================
 
