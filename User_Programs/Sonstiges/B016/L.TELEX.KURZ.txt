   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   PRODUKTBLATT    T E L E X 2 0 0 0
 
 
   Das System TELEX2000 ist ein Software-Paket welches die Bearbeitung
   von Fernschreiben auf einer Datenverarbeitungsanlage ermoeglicht.
   Mit seiner Hilfe koennen im Dialogbetrieb an einem Terminal
   Fernschreiben erfasst und deren Absendung ueberwacht und gesteuert
   werden.
 
   Zu hardware-maessigen Kopplung an das Fernschreibnetz wird ein
   intelligentes Interface   T O P C A L L   T X / T T X   verwendet.
   Dieses ermoeglicht den Anschluss an mehrere Telex/Teletex-Leitungen.
   Die zu sendenden Fernschreiben werden im TX/TTX zwischengespeichert,
   sodass eine Absendung auch moeglich ist, wenn die EDV-Anlage ausser
   Betrieb ist (Ausnutzung des guenstigen Nachttarifes).
 
 
   Das System TELEX2000 ist ein modulares System. Es kann an
   verschiedene Systemumgebungen und an die speziellen Beduerfnisse
   einer Installation angepasst werden.
 
   Dazu ist das System in einzelne Bausteine unterteilt.
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   PRODUKTBLATT  TELEX2000-BAS   (Basissystem)
 
 
   Dieser Baustein ist auf alle Faelle notwendig, er stellt das Grund-
   system dar, welches durch andere Bausteine in seinen Funktionen
   erweitert werden kann.
 
   Folgende Funktionen werden von TELEX2000-BAS abgedeckt:
 
 
   1. Dialogfunktionen (Anwender)
 
      Alle Dialogeingaben werden ueber Bildschirmmasken gesteuert.
 
      Alle Dialogfunktionen werden je Benutzer durchgefuehrt
      (d.h. z.B. der Benutzer 'MAIER' sieht die Fernschreiben
      des Benutzers 'MUELLER' nicht).
 
   1.1. Erfassen eines Fernschreibens
 
      Erfasst werden: Empfaenger, Sendezeitpunkt und Text.
 
      Der Empfaenger wird durch Telexnummer und Kenngeber festgelegt.
 
      Der Sendezeitpunkt legt fest, wann das Telex gesendet werden soll
      (guenstiger Nachttarif!).
 
      Fuer die Eingabe des Textes steht ein einfach zu bedienender
      Schirm-Editor zur Verfuegung. Ausserdem kann ein anderes
      Fernschreiben als Vorlage verwendet werden (fuer Rundschreiben).
 
      Fuer jedes Fernschreiben wird eine eindeutige Nummer vergeben,
      welche auch am Fernschreiben ausgedruckt wird.
      Fuer alle weiteren Funktionen wird das Fernschreiben durch
      diese Nummer identifiziert.
 
   1.2. Korrektur eines Fernschreibens
 
      Solange ein Fernschreiben nicht gesendet ist, kann es korrigiert
      oder geloescht werden.
 
   1.3. Inhaltsverzeichnisse (nicht gesendet - gesendet)
 
      Sobald ein Telex eingegeben ist, scheint es mit einer ent-
      sprechenden Statusangabe im Inhaltsverzeichnis der nicht
      gesendeten Fernschreiben auf. An dieser Statusangabe kann man
      z.B. erkennen, ob bereits versucht wird das Telex zu senden.
 
      Ist ein Telex gesendet, so scheint es im Inhaltsverzeichnis der
      gesendeten Telex auf.
 
      Dadurch kann der Benutzer ueberwachen, ob und wann sein Fern-
      schreiben abgesendet wird.
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
 
 
 
   1.4. automatischer Ausdruck der gesendeten Fernschreiben
 
      Auf Wunsch werden alle (ab dem letzten Ausdruck) gesendeten
      Fernschreiben nach Benutzer sortiert ausgedruckt.
      Dadurch ist es nicht mehr notwendig, dass das Fernschreiber-
      protokoll muehsam an die einzelnen Absender verteilt
      wird.
 
 
 
 
   2. Batchfunktionen
 
      Fuer folgende Funktionen stehen Batch-Programme zur Verfuegung:
 
   2.1. Archivierung der gesendeten Fernschreiben
 
      Die gesendeten Fernschreiben werden 10 Tage nach dem Absenden
      aus dem Dialogsystem in eine Archiv-Datei uebernommen.
 
   2.2. Ausdrucken eines Fernschreibens
 
      Der Ausdruck eines Fernschreibens ist durch Angabe der Nummer
      moeglich.
 
 
 
 
   3. Anpassung an verschiedene Systemumgebungen
 
      Das System kann unter verschiedenen Systemumgebungen eingesetzt
      werden.
 
      Dazu muss der jeweils passende Basisteil gewaehlt werden:
 
      TELEX2000-BAS-SAP          vollintegriert im SAP-System
                                 (bei Siemens: SEV - SAFIR EINKAUF...)
 
      TELEX2000-BAS-AMOR         unter dem Anwendermonitor AMOR
 
      TELEX2000-BAS-UTM          unter dem Transaktionsmonitor UTM
 
      Weitere Versionen sind moeglich.
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   PRODUKTBLATT    TELEX2000-BNG   (Benutzergruppen)
 
 
   Im System TELEX2000 weren alle Funktionen getrennt nach Benutzern
   durchgefuehrt. Durch diesen Erweiterungsbaustein koennen mehrere
   Benutzer zu einer Benutzergruppe zusammenfasst werden (z.B.
   die Telex-Benutzer im Rohstoff-Einkauf werden als Gruppe EINKAUFR
   deklariert).
 
   Jedes Mitglied einer Gruppe ist gleichwertig und sieht die Daten
   der anderen Mitglieder der Gruppe und kann sie aendern.
   So koennen mehrere Sachbearbeiter, die zusammenarbeiten, auch
   gleichzeitig arbeiten (da ihre Benutzernamen eindeutig sind)
   und trotzdem stehen die daten der ganzen Gruppe zur Verfuegung.
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   PRODUKTBLATT    TELEX2000-TLN   (Teilnehmerverzeichnis)
 
 
   Wird an denselben Empfaenger mehrmals ein Fernschreiben geschickt,
   so ist jedesmal dessen Fernschreibnummer und Kenngeber anzugeben.
   Dies ist nicht nur umstaendlich, sondern auch fehleranfaellig.
   Daher kann mit dem Baustein TELEX2000-TLN ein Teilnehmerverzeichnis
   aufgebaut und gewartet werden.
   Dieses wird getrennt je Benutzer (Benutzergruppe) gefuehrt.
 
   Wird nun ein Teilnehmer zum ersten Mal angeschrieben, so wird auf
   Wunsch ein Eintrag im Teilnehmerverzeichnis angelegt, wobei ein frei
   waehlbarer Name vergeben wird. Wird dieser Teilnehmer nun wieder
   angeschrieben, so ist nur mehr sein Name anzugeben.
   Ist der Name nicht mehr genau bekannt, so kann mit Matchcode
   gesucht werden.
 
 
   Im Batch kann ein Gesamtausdruck des Teilnehmerverzeichnisses
   abgerufen werden.
 
 
   In der SAP-Version kann zusaetzlich zum Teilnehmerverzeichnis auch
   auf die Stammdateien (Kunden, Lieferanten) zugegriffen werden.
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   PRODUKTBLATT    TELEX2000-BIF   (Benutzerinterface)
 
 
   Mithilfe dieses Bausteines kann auf der Basis einer COBOL-CALL-
   Schnittstelle auf den Telex-Datenbestand zugegriffen werden.
 
   Dadurch ist es moeglich aus eigenen Anwendungen Fernschreiben in das
   System einzuschleusen oder Statusabfragen ueber die im System
   vorhandenen Fernschreiben durchzufuehren.
 
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   PRODUKTBLATT    TELEX2000-CCI   (Fernschreiben ueber CCI)
 
 
   CCI (Consortium Communications International) ist ein Fernschreib-
   Vermittlungssystem in England, ueber welches Fernschreiben nach
   Uebersee kostenguenstig uebertragen werden koennen.
 
   Ein Fernschreiben, welches ueber CCI gesendet wird, muss einen
   bestimmten Aufbau haben. Dies wird durch den Baustein TELEX2000-CCI
   unterstuetzt.
A
   Ing. Franz Beranek  Software Beratung                             SBB
 
 
   FUNKTIONSERWEITERUNGEN   T E L E X 2 0 0 0
 
 
   Folgende Erweiterungen sind geplant und koennen bei entsprechender
   Nachfrage realisiert werden.
 
   - Bearbeitung eingehender Fernschreiben
 
   - Verwendung von Textbausteinen (eventuell auch mit Zugriff auf
     eigene, bereits vorhandene Textbausteine)
 
   - Abrechnungssystem zu Ermittlung der Verteilung der Fernschreib-
     kosten auf die einzelnen Benutzer (Benutzergruppen)
 
