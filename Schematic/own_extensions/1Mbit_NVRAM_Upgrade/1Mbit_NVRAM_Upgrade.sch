EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5E9C8DF8
P 6400 4475
F 0 "#PWR?" H 6400 4225 50  0001 C CNN
F 1 "GND" H 6405 4302 50  0000 C CNN
F 2 "" H 6400 4475 50  0001 C CNN
F 3 "" H 6400 4475 50  0001 C CNN
	1    6400 4475
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9C9191
P 4225 3975
F 0 "#PWR?" H 4225 3725 50  0001 C CNN
F 1 "GND" H 4230 3802 50  0000 C CNN
F 2 "" H 4225 3975 50  0001 C CNN
F 3 "" H 4225 3975 50  0001 C CNN
	1    4225 3975
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9C98EE
P 4225 1950
F 0 "#PWR?" H 4225 1800 50  0001 C CNN
F 1 "+5V" H 4240 2123 50  0000 C CNN
F 2 "" H 4225 1950 50  0001 C CNN
F 3 "" H 4225 1950 50  0001 C CNN
	1    4225 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9C9ECE
P 4425 2100
F 0 "C?" H 4517 2146 50  0000 L CNN
F 1 "47n" H 4517 2055 50  0000 L CNN
F 2 "" H 4425 2100 50  0001 C CNN
F 3 "~" H 4425 2100 50  0001 C CNN
	1    4425 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9CA310
P 4425 2225
F 0 "#PWR?" H 4425 1975 50  0001 C CNN
F 1 "GND" H 4425 2075 50  0000 C CNN
F 2 "" H 4425 2225 50  0001 C CNN
F 3 "" H 4425 2225 50  0001 C CNN
	1    4425 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4225 2275 4225 1975
Wire Wire Line
	4225 1975 4425 1975
Wire Wire Line
	4425 1975 4425 2000
Wire Wire Line
	4225 1975 4225 1950
Connection ~ 4225 1975
Wire Wire Line
	6400 1975 6400 1950
$Comp
L power:GND #PWR?
U 1 1 5E9CB6C8
P 6600 2200
F 0 "#PWR?" H 6600 1950 50  0001 C CNN
F 1 "GND" H 6600 2075 50  0000 C CNN
F 2 "" H 6600 2200 50  0001 C CNN
F 3 "" H 6600 2200 50  0001 C CNN
	1    6600 2200
	1    0    0    -1  
$EndComp
Connection ~ 6400 1975
$Comp
L power:+5V #PWR?
U 1 1 5E9CB6D6
P 6400 1950
F 0 "#PWR?" H 6400 1800 50  0001 C CNN
F 1 "+5V" H 6415 2123 50  0000 C CNN
F 2 "" H 6400 1950 50  0001 C CNN
F 3 "" H 6400 1950 50  0001 C CNN
	1    6400 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9CB6E4
P 6600 2100
F 0 "C?" H 6508 2146 50  0000 R CNN
F 1 "47n" H 6508 2055 50  0000 R CNN
F 2 "" H 6600 2100 50  0001 C CNN
F 3 "~" H 6600 2100 50  0001 C CNN
	1    6600 2100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 1975 6600 2000
Wire Wire Line
	6400 2275 6400 1975
Wire Wire Line
	6400 1975 6600 1975
Wire Wire Line
	4425 2225 4425 2200
Text Label 5675 2475 0    50   ~ 0
A0
Text Label 5675 2575 0    50   ~ 0
A1
Text Label 5675 2675 0    50   ~ 0
A2
Text Label 5675 2775 0    50   ~ 0
A3
Text Label 5675 2875 0    50   ~ 0
A4
Text Label 5675 2975 0    50   ~ 0
A5
Text Label 5675 3075 0    50   ~ 0
A6
Text Label 5675 3175 0    50   ~ 0
A7
Text Label 5675 3275 0    50   ~ 0
A8
Text Label 5675 3375 0    50   ~ 0
A9
Text Label 5675 3475 0    50   ~ 0
A10
Text Label 5675 3575 0    50   ~ 0
A11
Text Label 5675 3675 0    50   ~ 0
A12
Text Label 3500 2975 0    50   ~ 0
A13
Text Label 3500 3075 0    50   ~ 0
A14
Text Label 3500 3175 0    50   ~ 0
A15
Wire Wire Line
	5900 2475 5675 2475
Wire Wire Line
	5900 2575 5675 2575
Wire Wire Line
	5900 2675 5675 2675
Wire Wire Line
	5900 2775 5675 2775
Wire Wire Line
	5900 2875 5675 2875
Wire Wire Line
	5900 2975 5675 2975
Wire Wire Line
	5900 3075 5675 3075
Wire Wire Line
	5900 3175 5675 3175
Wire Wire Line
	5900 3275 5675 3275
Wire Wire Line
	5900 3375 5675 3375
Wire Wire Line
	5900 3475 5675 3475
Wire Wire Line
	5900 3575 5675 3575
Wire Wire Line
	5900 3675 5675 3675
Wire Wire Line
	6900 3575 7125 3575
Wire Wire Line
	3725 3175 3500 3175
Wire Wire Line
	3725 3075 3500 3075
Wire Wire Line
	3725 2875 3500 2875
Wire Wire Line
	3725 2975 3500 2975
Wire Wire Line
	3725 2575 3500 2575
Wire Wire Line
	3725 2675 3500 2675
Wire Wire Line
	3725 2775 3500 2775
Wire Wire Line
	7125 2875 6900 2875
Wire Wire Line
	7125 2775 6900 2775
Wire Wire Line
	7125 2675 6900 2675
Wire Wire Line
	7125 2475 6900 2475
Wire Wire Line
	7125 2575 6900 2575
Wire Wire Line
	7125 2975 6900 2975
Wire Wire Line
	7125 3075 6900 3075
Wire Wire Line
	7125 3175 6900 3175
Text Label 7125 2475 2    50   ~ 0
D0
Text Label 7125 2575 2    50   ~ 0
D1
Text Label 7125 2675 2    50   ~ 0
D2
Text Label 7125 2775 2    50   ~ 0
D3
Text Label 7125 2875 2    50   ~ 0
D4
Text Label 7125 2975 2    50   ~ 0
D5
Text Label 7125 3075 2    50   ~ 0
D6
Text Label 7125 3175 2    50   ~ 0
D7
Text Label 7125 3575 2    50   ~ 0
~OE
Text HLabel 800  1100 0    50   Input ~ 0
A0
Text HLabel 800  1200 0    50   Input ~ 0
A1
Text HLabel 800  1300 0    50   Input ~ 0
A2
Text HLabel 800  1400 0    50   Input ~ 0
A3
Text HLabel 800  1500 0    50   Input ~ 0
A4
Text HLabel 800  1600 0    50   Input ~ 0
A5
Text HLabel 800  1700 0    50   Input ~ 0
A6
Text HLabel 800  1800 0    50   Input ~ 0
A7
Text HLabel 800  1900 0    50   Input ~ 0
A8
Text HLabel 800  2000 0    50   Input ~ 0
A9
Text HLabel 800  2100 0    50   Input ~ 0
A10
Text HLabel 800  2200 0    50   Input ~ 0
A11
Text HLabel 800  2300 0    50   Input ~ 0
A12
Text HLabel 800  2400 0    50   Input ~ 0
A13
Text HLabel 800  2500 0    50   Input ~ 0
A14
Text HLabel 800  2600 0    50   Input ~ 0
A15
Text Label 1675 1900 2    50   ~ 0
~OE
Text Label 1025 1400 2    50   ~ 0
A3
Text Label 1025 1300 2    50   ~ 0
A2
Text Label 1025 1700 2    50   ~ 0
A6
Wire Wire Line
	800  2600 1025 2600
Wire Wire Line
	800  1900 1025 1900
Text Label 1025 2200 2    50   ~ 0
A11
Wire Wire Line
	800  2500 1025 2500
Wire Wire Line
	800  1800 1025 1800
Text Label 1025 2100 2    50   ~ 0
A10
Text Label 1025 2300 2    50   ~ 0
A12
Text Label 1025 1200 2    50   ~ 0
A1
Wire Wire Line
	800  1500 1025 1500
Wire Wire Line
	1450 1900 1675 1900
Text Label 1025 1600 2    50   ~ 0
A5
Text Label 1025 1100 2    50   ~ 0
A0
Wire Wire Line
	800  2300 1025 2300
Wire Wire Line
	800  1600 1025 1600
Wire Wire Line
	800  1300 1025 1300
Text Label 1025 1800 2    50   ~ 0
A7
Wire Wire Line
	800  1100 1025 1100
Text Label 1025 1900 2    50   ~ 0
A8
Wire Wire Line
	800  2400 1025 2400
Wire Wire Line
	800  1400 1025 1400
Wire Wire Line
	800  1700 1025 1700
Wire Wire Line
	800  2000 1025 2000
Wire Wire Line
	800  1200 1025 1200
Text Label 1025 2000 2    50   ~ 0
A9
Text Label 1025 1500 2    50   ~ 0
A4
Wire Wire Line
	800  2100 1025 2100
Wire Wire Line
	800  2200 1025 2200
Text Label 1025 2600 2    50   ~ 0
A15
Text Label 1025 2500 2    50   ~ 0
A14
Text Label 1025 2400 2    50   ~ 0
A13
Text Label 1775 1100 2    50   ~ 0
~BANK0
Text Label 1775 1200 2    50   ~ 0
~BANK1
Text Label 1775 1300 2    50   ~ 0
~BANK2
Text Label 1775 1400 2    50   ~ 0
~BANK3
Text Label 1775 1500 2    50   ~ 0
~USR1
Text Label 1775 1600 2    50   ~ 0
~USR2
Text Label 1775 1700 2    50   ~ 0
~USR3
Text Label 1775 1800 2    50   ~ 0
~USR4
Wire Wire Line
	1775 1100 1450 1100
Wire Wire Line
	1775 1200 1450 1200
Wire Wire Line
	1775 1300 1450 1300
Wire Wire Line
	1775 1400 1450 1400
Wire Wire Line
	1775 1500 1450 1500
Wire Wire Line
	1775 1600 1450 1600
Wire Wire Line
	1775 1700 1450 1700
Wire Wire Line
	1775 1800 1450 1800
Text HLabel 1450 1100 0    50   Input ~ 0
~BANK0
Text HLabel 1450 1200 0    50   Input ~ 0
~BANK1
Text HLabel 1450 1300 0    50   Input ~ 0
~BANK2
Text HLabel 1450 1400 0    50   Input ~ 0
~BANK3
Text HLabel 1450 1500 0    50   Input ~ 0
~USR1
Text HLabel 1450 1600 0    50   Input ~ 0
~USR2
Text HLabel 1450 1700 0    50   Input ~ 0
~USR3
Text HLabel 1450 1800 0    50   Input ~ 0
~USR4
Text HLabel 1450 1900 0    50   Input ~ 0
~OE
Text Label 3500 2775 0    50   ~ 0
~BANK2
Text Label 3500 2675 0    50   ~ 0
~BANK1
Text Label 3500 2575 0    50   ~ 0
~BANK0
Text Label 3500 2875 0    50   ~ 0
~BANK3
Text HLabel 800  2775 0    50   Input ~ 0
D0
Text HLabel 800  2875 0    50   Input ~ 0
D1
Text HLabel 800  2975 0    50   Input ~ 0
D2
Text HLabel 800  3075 0    50   Input ~ 0
D3
Text HLabel 800  3175 0    50   Input ~ 0
D4
Text HLabel 800  3275 0    50   Input ~ 0
D5
Text HLabel 800  3375 0    50   Input ~ 0
D6
Text HLabel 800  3475 0    50   Input ~ 0
D7
Text Label 1025 2775 2    50   ~ 0
D0
Text Label 1025 3075 2    50   ~ 0
D3
Text Label 1025 3375 2    50   ~ 0
D6
Wire Wire Line
	1025 3375 800  3375
Text Label 1025 3475 2    50   ~ 0
D7
Wire Wire Line
	1025 3475 800  3475
Wire Wire Line
	1025 2775 800  2775
Text Label 1025 3175 2    50   ~ 0
D4
Wire Wire Line
	1025 3275 800  3275
Text Label 1025 2875 2    50   ~ 0
D1
Text Label 1025 3275 2    50   ~ 0
D5
Wire Wire Line
	1025 3075 800  3075
Wire Wire Line
	1025 3175 800  3175
Wire Wire Line
	1025 2975 800  2975
Wire Wire Line
	1025 2875 800  2875
Text Label 1025 2975 2    50   ~ 0
D2
$Comp
L Memory_RAM:AS6C4008-55PCN U?
U 1 1 5E9C3D52
P 6400 3375
F 0 "U?" H 6000 4525 50  0000 C CNN
F 1 "AS6C4008-55PCN" H 5950 4425 50  0000 C CNN
F 2 "Package_DIP:DIP-32_W15.24mm" H 6400 3475 50  0001 C CNN
F 3 "https://www.alliancememory.com/wp-content/uploads/pdf/AS6C4008.pdf" H 6400 3475 50  0001 C CNN
	1    6400 3375
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3675 7125 3675
Text Label 7125 3675 2    50   ~ 0
~WE
Text Label 1675 2000 2    50   ~ 0
~WE
Wire Wire Line
	1450 2000 1675 2000
Text HLabel 1450 2000 0    50   Input ~ 0
~WE
$Comp
L Logic_Programmable:PAL20L8 U?
U 1 1 5E9C9E9E
P 4225 3175
F 0 "U?" H 3825 4075 50  0000 C CNN
F 1 "PAL20L8" H 3950 4000 50  0000 C CNN
F 2 "" H 4225 3175 50  0001 C CNN
F 3 "" H 4225 3175 50  0001 C CNN
	1    4225 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	7225 3475 6900 3475
Text Label 3400 3275 0    50   ~ 0
~USR1
Text Label 3400 3375 0    50   ~ 0
~USR2
Text Label 3400 3475 0    50   ~ 0
~USR3
Text Label 3400 3575 0    50   ~ 0
~USR4
Wire Wire Line
	3400 3275 3725 3275
Wire Wire Line
	3400 3375 3725 3375
Wire Wire Line
	3400 3475 3725 3475
Wire Wire Line
	3400 3575 3725 3575
NoConn ~ 3725 2475
NoConn ~ 3725 3775
NoConn ~ 4725 3175
NoConn ~ 3725 3675
Text Label 5025 2975 2    50   ~ 0
EA13
Text Label 5025 2875 2    50   ~ 0
EA14
Text Label 5025 2575 2    50   ~ 0
EA15
Text Label 5025 2675 2    50   ~ 0
EA16
Text Label 5025 2775 2    50   ~ 0
EA17
Text Label 5025 2475 2    50   ~ 0
EA18
Text Label 5025 3075 2    50   ~ 0
~ECE
Wire Wire Line
	5900 3775 5550 3775
Wire Wire Line
	5900 3875 5550 3875
Wire Wire Line
	5900 3975 5550 3975
Wire Wire Line
	5900 4075 5550 4075
Wire Wire Line
	5900 4175 5550 4175
Wire Wire Line
	5900 4275 5550 4275
Text Label 5550 3875 0    50   ~ 0
EA14
Text Label 5550 3775 0    50   ~ 0
EA13
Text Label 5550 4175 0    50   ~ 0
EA17
Text Label 5550 3975 0    50   ~ 0
EA15
Text Label 5550 4275 0    50   ~ 0
EA18
Text Label 5550 4075 0    50   ~ 0
EA16
Text Label 7225 3475 2    50   ~ 0
~ECE
Wire Wire Line
	4725 2475 5025 2475
Wire Wire Line
	4725 2575 5025 2575
Wire Wire Line
	4725 2675 5025 2675
Wire Wire Line
	4725 2775 5025 2775
Wire Wire Line
	4725 2875 5025 2875
Wire Wire Line
	4725 2975 5025 2975
Wire Wire Line
	4725 3075 5025 3075
Entry Wire Line
	7225 3475 7325 3375
Entry Wire Line
	5450 3875 5550 3775
Entry Wire Line
	5450 3975 5550 3875
Entry Wire Line
	5450 4075 5550 3975
Entry Wire Line
	5450 4175 5550 4075
Entry Wire Line
	5450 4275 5550 4175
Entry Wire Line
	5450 4375 5550 4275
Entry Wire Line
	5025 2475 5125 2375
Entry Wire Line
	5025 2575 5125 2475
Entry Wire Line
	5025 2675 5125 2575
Entry Wire Line
	5025 2775 5125 2675
Entry Wire Line
	5025 2875 5125 2775
Entry Wire Line
	5025 2975 5125 2875
Entry Wire Line
	5025 3075 5125 2975
Wire Bus Line
	5125 3600 5450 3600
Wire Bus Line
	5450 4700 7325 4700
Wire Bus Line
	7325 4700 7325 3375
Wire Bus Line
	5450 3600 5450 4700
Wire Bus Line
	5125 2375 5125 3600
$EndSCHEMATC
