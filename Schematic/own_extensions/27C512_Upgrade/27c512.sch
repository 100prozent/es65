EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L Memory_EPROM:27C512 U?
U 1 1 5E9C36C1
P 4600 3375
F 0 "U?" H 4250 4525 50  0000 C CNN
F 1 "27C512" H 4350 4425 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm" H 4600 3375 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 4600 3375 50  0001 C CNN
	1    4600 3375
	1    0    0    -1  
$EndComp
$Comp
L Logic_Programmable:GAL16V8 U?
U 1 1 5E9C5C5E
P 2850 2950
F 0 "U?" H 2450 3625 50  0000 C CNN
F 1 "GAL16V8" H 2575 3550 50  0000 C CNN
F 2 "" H 2850 2950 50  0001 C CNN
F 3 "" H 2850 2950 50  0001 C CNN
	1    2850 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9C8DF8
P 4600 4475
F 0 "#PWR?" H 4600 4225 50  0001 C CNN
F 1 "GND" H 4605 4302 50  0000 C CNN
F 2 "" H 4600 4475 50  0001 C CNN
F 3 "" H 4600 4475 50  0001 C CNN
	1    4600 4475
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9C9191
P 2850 3650
F 0 "#PWR?" H 2850 3400 50  0001 C CNN
F 1 "GND" H 2855 3477 50  0000 C CNN
F 2 "" H 2850 3650 50  0001 C CNN
F 3 "" H 2850 3650 50  0001 C CNN
	1    2850 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9C98EE
P 2850 1925
F 0 "#PWR?" H 2850 1775 50  0001 C CNN
F 1 "+5V" H 2865 2098 50  0000 C CNN
F 2 "" H 2850 1925 50  0001 C CNN
F 3 "" H 2850 1925 50  0001 C CNN
	1    2850 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9C9ECE
P 3050 2075
F 0 "C?" H 3142 2121 50  0000 L CNN
F 1 "47n" H 3142 2030 50  0000 L CNN
F 2 "" H 3050 2075 50  0001 C CNN
F 3 "~" H 3050 2075 50  0001 C CNN
	1    3050 2075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9CA310
P 3050 2200
F 0 "#PWR?" H 3050 1950 50  0001 C CNN
F 1 "GND" H 3050 2050 50  0000 C CNN
F 2 "" H 3050 2200 50  0001 C CNN
F 3 "" H 3050 2200 50  0001 C CNN
	1    3050 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2250 2850 1950
Wire Wire Line
	2850 1950 3050 1950
Wire Wire Line
	3050 1950 3050 1975
Wire Wire Line
	2850 1950 2850 1925
Connection ~ 2850 1950
Wire Wire Line
	4600 1975 4600 1950
$Comp
L power:GND #PWR?
U 1 1 5E9CB6C8
P 4800 2200
F 0 "#PWR?" H 4800 1950 50  0001 C CNN
F 1 "GND" H 4800 2075 50  0000 C CNN
F 2 "" H 4800 2200 50  0001 C CNN
F 3 "" H 4800 2200 50  0001 C CNN
	1    4800 2200
	1    0    0    -1  
$EndComp
Connection ~ 4600 1975
$Comp
L power:+5V #PWR?
U 1 1 5E9CB6D6
P 4600 1950
F 0 "#PWR?" H 4600 1800 50  0001 C CNN
F 1 "+5V" H 4615 2123 50  0000 C CNN
F 2 "" H 4600 1950 50  0001 C CNN
F 3 "" H 4600 1950 50  0001 C CNN
	1    4600 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9CB6E4
P 4800 2100
F 0 "C?" H 4708 2146 50  0000 R CNN
F 1 "47n" H 4708 2055 50  0000 R CNN
F 2 "" H 4800 2100 50  0001 C CNN
F 3 "~" H 4800 2100 50  0001 C CNN
	1    4800 2100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4800 1975 4800 2000
Wire Wire Line
	4600 2275 4600 1975
Wire Wire Line
	4600 1975 4800 1975
Wire Wire Line
	3050 2200 3050 2175
Text Label 3975 2475 0    50   ~ 0
A0
Text Label 3975 2575 0    50   ~ 0
A1
Text Label 3975 2675 0    50   ~ 0
A2
Text Label 3975 2775 0    50   ~ 0
A3
Text Label 3975 2875 0    50   ~ 0
A4
Text Label 3975 2975 0    50   ~ 0
A5
Text Label 3975 3075 0    50   ~ 0
A6
Text Label 3975 3175 0    50   ~ 0
A7
Text Label 3975 3275 0    50   ~ 0
A8
Text Label 3975 3375 0    50   ~ 0
A9
Text Label 3975 3475 0    50   ~ 0
A10
Text Label 3975 3575 0    50   ~ 0
A11
Text Label 3975 3675 0    50   ~ 0
A12
Text Label 2125 2950 0    50   ~ 0
A13
Text Label 2125 3050 0    50   ~ 0
A14
Text Label 2125 3150 0    50   ~ 0
A15
Wire Wire Line
	4200 2475 3975 2475
Wire Wire Line
	4200 2575 3975 2575
Wire Wire Line
	4200 2675 3975 2675
Wire Wire Line
	4200 2775 3975 2775
Wire Wire Line
	4200 2875 3975 2875
Wire Wire Line
	4200 2975 3975 2975
Wire Wire Line
	4200 3075 3975 3075
Wire Wire Line
	4200 3175 3975 3175
Wire Wire Line
	4200 3275 3975 3275
Wire Wire Line
	4200 3375 3975 3375
Wire Wire Line
	4200 3475 3975 3475
Wire Wire Line
	4200 3575 3975 3575
Wire Wire Line
	4200 3675 3975 3675
Wire Wire Line
	4200 4275 3975 4275
Wire Wire Line
	2350 3150 2125 3150
Wire Wire Line
	2350 3050 2125 3050
Wire Wire Line
	3575 3150 3350 3150
Wire Wire Line
	3575 2850 3350 2850
Wire Wire Line
	2350 2850 2125 2850
Wire Wire Line
	2350 2950 2125 2950
Wire Wire Line
	3575 2750 3350 2750
Wire Wire Line
	2350 2550 2125 2550
Wire Wire Line
	2350 2650 2125 2650
Wire Wire Line
	2350 2750 2125 2750
Wire Wire Line
	5225 2875 5000 2875
Wire Wire Line
	5225 2775 5000 2775
Wire Wire Line
	5225 2675 5000 2675
Wire Wire Line
	5225 2475 5000 2475
Wire Wire Line
	5225 2575 5000 2575
Wire Wire Line
	5225 2975 5000 2975
Wire Wire Line
	5225 3075 5000 3075
Wire Wire Line
	5225 3175 5000 3175
Text Label 5225 2475 2    50   ~ 0
D0
Text Label 5225 2575 2    50   ~ 0
D1
Text Label 5225 2675 2    50   ~ 0
D2
Text Label 5225 2775 2    50   ~ 0
D3
Text Label 5225 2875 2    50   ~ 0
D4
Text Label 5225 2975 2    50   ~ 0
D5
Text Label 5225 3075 2    50   ~ 0
D6
Text Label 5225 3175 2    50   ~ 0
D7
Text Label 3975 4275 0    50   ~ 0
~OE
Text HLabel 800  1100 0    50   Input ~ 0
A0
Text HLabel 800  1200 0    50   Input ~ 0
A1
Text HLabel 800  1300 0    50   Input ~ 0
A2
Text HLabel 800  1400 0    50   Input ~ 0
A3
Text HLabel 800  1500 0    50   Input ~ 0
A4
Text HLabel 800  1600 0    50   Input ~ 0
A5
Text HLabel 800  1700 0    50   Input ~ 0
A6
Text HLabel 800  1800 0    50   Input ~ 0
A7
Text HLabel 800  1900 0    50   Input ~ 0
A8
Text HLabel 800  2000 0    50   Input ~ 0
A9
Text HLabel 800  2100 0    50   Input ~ 0
A10
Text HLabel 800  2200 0    50   Input ~ 0
A11
Text HLabel 800  2300 0    50   Input ~ 0
A12
Text HLabel 800  2400 0    50   Input ~ 0
A13
Text HLabel 800  2500 0    50   Input ~ 0
A14
Text HLabel 800  2600 0    50   Input ~ 0
A15
Text Label 1675 1900 2    50   ~ 0
~OE
Text Label 1025 1400 2    50   ~ 0
A3
Text Label 1025 1300 2    50   ~ 0
A2
Text Label 1025 1700 2    50   ~ 0
A6
Wire Wire Line
	800  2600 1025 2600
Wire Wire Line
	800  1900 1025 1900
Text Label 1025 2200 2    50   ~ 0
A11
Wire Wire Line
	800  2500 1025 2500
Wire Wire Line
	800  1800 1025 1800
Text Label 1025 2100 2    50   ~ 0
A10
Text Label 1025 2300 2    50   ~ 0
A12
Text Label 1025 1200 2    50   ~ 0
A1
Wire Wire Line
	800  1500 1025 1500
Wire Wire Line
	1450 1900 1675 1900
Text Label 1025 1600 2    50   ~ 0
A5
Text Label 1025 1100 2    50   ~ 0
A0
Wire Wire Line
	800  2300 1025 2300
Wire Wire Line
	800  1600 1025 1600
Wire Wire Line
	800  1300 1025 1300
Text Label 1025 1800 2    50   ~ 0
A7
Wire Wire Line
	800  1100 1025 1100
Text Label 1025 1900 2    50   ~ 0
A8
Wire Wire Line
	800  2400 1025 2400
Wire Wire Line
	800  1400 1025 1400
Wire Wire Line
	800  1700 1025 1700
Wire Wire Line
	800  2000 1025 2000
Wire Wire Line
	800  1200 1025 1200
Text Label 1025 2000 2    50   ~ 0
A9
Text Label 1025 1500 2    50   ~ 0
A4
Wire Wire Line
	800  2100 1025 2100
Wire Wire Line
	800  2200 1025 2200
Text Label 1025 2600 2    50   ~ 0
A15
Text Label 1025 2500 2    50   ~ 0
A14
Text Label 1025 2400 2    50   ~ 0
A13
Text Label 1775 1100 2    50   ~ 0
~BANK0
Text Label 1775 1200 2    50   ~ 0
~BANK1
Text Label 1775 1300 2    50   ~ 0
~BANK2
Text Label 1775 1400 2    50   ~ 0
~BANK3
Text Label 1775 1500 2    50   ~ 0
~USR1
Text Label 1775 1600 2    50   ~ 0
~USR2
Text Label 1775 1700 2    50   ~ 0
~USR3
Text Label 1775 1800 2    50   ~ 0
~USR4
Wire Wire Line
	1775 1100 1450 1100
Wire Wire Line
	1775 1200 1450 1200
Wire Wire Line
	1775 1300 1450 1300
Wire Wire Line
	1775 1400 1450 1400
Wire Wire Line
	1775 1500 1450 1500
Wire Wire Line
	1775 1600 1450 1600
Wire Wire Line
	1775 1700 1450 1700
Wire Wire Line
	1775 1800 1450 1800
Text HLabel 1450 1100 0    50   Input ~ 0
~BANK0
Text HLabel 1450 1200 0    50   Input ~ 0
~BANK1
Text HLabel 1450 1300 0    50   Input ~ 0
~BANK2
Text HLabel 1450 1400 0    50   Input ~ 0
~BANK3
Text HLabel 1450 1500 0    50   Input ~ 0
~USR1
Text HLabel 1450 1600 0    50   Input ~ 0
~USR2
Text HLabel 1450 1700 0    50   Input ~ 0
~USR3
Text HLabel 1450 1800 0    50   Input ~ 0
~USR4
Text HLabel 1450 1900 0    50   Input ~ 0
~OE
Text Label 2125 2750 0    50   ~ 0
~BANK2
Text Label 2125 2650 0    50   ~ 0
~BANK1
Text Label 2125 2550 0    50   ~ 0
~BANK0
Text Label 2125 2850 0    50   ~ 0
~BANK3
NoConn ~ 2350 3250
NoConn ~ 2350 3350
NoConn ~ 3350 3050
NoConn ~ 3350 2950
Wire Wire Line
	3925 3775 3925 2450
Wire Wire Line
	3925 3775 4200 3775
Wire Wire Line
	3350 2450 3925 2450
Wire Wire Line
	3825 2550 3825 3875
Wire Wire Line
	3825 3875 4200 3875
Wire Wire Line
	3350 2550 3825 2550
Wire Wire Line
	3725 3975 3725 2650
Wire Wire Line
	3725 3975 4200 3975
Wire Wire Line
	3350 2650 3725 2650
Wire Wire Line
	3575 3150 3575 4175
Wire Wire Line
	3575 4175 4200 4175
Text Label 3575 2850 2    50   ~ 0
COM
Text Label 3575 2750 2    50   ~ 0
USR
Text Notes 3375 2050 0    50   ~ 0
COM and USR Output\nare just for debugging
Text HLabel 800  2775 0    50   Input ~ 0
D0
Text HLabel 800  2875 0    50   Input ~ 0
D1
Text HLabel 800  2975 0    50   Input ~ 0
D2
Text HLabel 800  3075 0    50   Input ~ 0
D3
Text HLabel 800  3175 0    50   Input ~ 0
D4
Text HLabel 800  3275 0    50   Input ~ 0
D5
Text HLabel 800  3375 0    50   Input ~ 0
D6
Text HLabel 800  3475 0    50   Input ~ 0
D7
Text Label 1025 2775 2    50   ~ 0
D0
Text Label 1025 3075 2    50   ~ 0
D3
Text Label 1025 3375 2    50   ~ 0
D6
Wire Wire Line
	1025 3375 800  3375
Text Label 1025 3475 2    50   ~ 0
D7
Wire Wire Line
	1025 3475 800  3475
Wire Wire Line
	1025 2775 800  2775
Text Label 1025 3175 2    50   ~ 0
D4
Wire Wire Line
	1025 3275 800  3275
Text Label 1025 2875 2    50   ~ 0
D1
Text Label 1025 3275 2    50   ~ 0
D5
Wire Wire Line
	1025 3075 800  3075
Wire Wire Line
	1025 3175 800  3175
Wire Wire Line
	1025 2975 800  2975
Wire Wire Line
	1025 2875 800  2875
Text Label 1025 2975 2    50   ~ 0
D2
NoConn ~ 2350 2450
$EndSCHEMATC
