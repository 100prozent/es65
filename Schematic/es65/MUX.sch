EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 16 16
Title "es65 MUC Board"
Date "2020-03-31"
Rev "v0.2"
Comp "Offner Robert"
Comment1 "v0.2: U3,U4 Input ch."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1050 1950 1350 1950
Wire Wire Line
	1050 3450 1350 3450
Wire Wire Line
	1050 2150 1350 2150
Wire Wire Line
	1050 2450 1350 2450
Wire Wire Line
	1050 3850 1350 3850
NoConn ~ 1050 1050
NoConn ~ 1050 3950
Wire Wire Line
	2825 2050 3125 2050
Wire Wire Line
	1050 2350 1350 2350
Wire Wire Line
	2825 1950 3125 1950
Wire Wire Line
	1925 2750 2225 2750
Text Label 1350 3750 2    50   ~ 0
AD6
Text Label 1350 3850 2    50   ~ 0
AD7
Text Label 1350 3650 2    50   ~ 0
AD5
Text Label 1350 3550 2    50   ~ 0
AD4
Text Label 1350 3450 2    50   ~ 0
AD3
Text Label 1350 3350 2    50   ~ 0
AD2
Text Label 1350 3250 2    50   ~ 0
AD1
Text Label 1350 3150 2    50   ~ 0
AD0
Text Label 1350 2650 2    50   ~ 0
AA15
Text Label 1350 2150 2    50   ~ 0
AA10
Text Label 1350 2350 2    50   ~ 0
AA12
Text Label 1350 2250 2    50   ~ 0
AA11
Text Label 1350 2550 2    50   ~ 0
AA14
Text Label 1350 2450 2    50   ~ 0
AA13
Text Label 1350 1950 2    50   ~ 0
AA8
Text Label 1350 1550 2    50   ~ 0
AA4
Text Label 1350 2050 2    50   ~ 0
AA9
Text Label 1350 1650 2    50   ~ 0
AA5
Text Label 1350 1850 2    50   ~ 0
AA7
Text Label 1350 1750 2    50   ~ 0
AA6
Text Label 2225 2750 2    50   ~ 0
PH2b
Text Label 1350 1250 2    50   ~ 0
AA1
Text Label 1350 1350 2    50   ~ 0
AA2
Text Label 1350 1450 2    50   ~ 0
AA3
Text Label 1350 1150 2    50   ~ 0
AA0
NoConn ~ 1050 2750
Wire Wire Line
	1050 2050 1350 2050
Wire Wire Line
	1050 1450 1350 1450
Wire Wire Line
	1050 950  1350 950 
Wire Wire Line
	1050 3150 1350 3150
Wire Wire Line
	1100 4050 1100 4100
Wire Wire Line
	1050 1250 1350 1250
Wire Wire Line
	2225 950  2225 825 
NoConn ~ 1050 2850
Wire Wire Line
	1050 4050 1100 4050
Wire Wire Line
	1350 950  1350 825 
Wire Wire Line
	1050 3250 1350 3250
Wire Wire Line
	1050 1550 1350 1550
Wire Wire Line
	1050 1350 1350 1350
Wire Wire Line
	1050 1150 1350 1150
Wire Wire Line
	1925 4050 1975 4050
Wire Wire Line
	1975 4050 1975 4100
Wire Wire Line
	1050 2550 1350 2550
Wire Wire Line
	1050 3550 1350 3550
Wire Wire Line
	1925 950  2225 950 
Wire Wire Line
	1050 3350 1350 3350
Wire Wire Line
	1050 1850 1350 1850
Wire Wire Line
	1050 2650 1350 2650
Wire Wire Line
	1050 2250 1350 2250
Wire Wire Line
	1050 1650 1350 1650
Wire Wire Line
	1050 1750 1350 1750
Wire Wire Line
	1050 3650 1350 3650
Wire Wire Line
	1050 3750 1350 3750
NoConn ~ 2825 1250
NoConn ~ 2825 1350
NoConn ~ 2825 950 
NoConn ~ 2825 1650
NoConn ~ 2825 4050
NoConn ~ 2825 1450
NoConn ~ 2825 1550
Wire Wire Line
	2825 2150 3125 2150
$Comp
L power:GND #PWR?
U 1 1 5E9AC88A
P 1975 4100
AR Path="/5E84A03F/5E9AC88A" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E9AC88A" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E9AC88A" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E9AC88A" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E9AC88A" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E9AC88A" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9AC88A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1975 3850 50  0001 C CNN
F 1 "GND" H 1975 3950 50  0000 C CNN
F 2 "" H 1975 4100 50  0001 C CNN
F 3 "" H 1975 4100 50  0001 C CNN
	1    1975 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E8910E1
P 2225 825
AR Path="/5E84A03F/5E8910E1" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E8910E1" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E8910E1" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E8910E1" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E8910E1" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E8910E1" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E8910E1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2225 675 50  0001 C CNN
F 1 "+5V" H 2240 998 50  0000 C CNN
F 2 "" H 2225 825 50  0001 C CNN
F 3 "" H 2225 825 50  0001 C CNN
	1    2225 825 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5ED165B6
P 1350 825
AR Path="/5E84A03F/5ED165B6" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5ED165B6" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5ED165B6" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5ED165B6" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5ED165B6" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5ED165B6" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5ED165B6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1350 675 50  0001 C CNN
F 1 "+5V" H 1365 998 50  0000 C CNN
F 2 "" H 1350 825 50  0001 C CNN
F 3 "" H 1350 825 50  0001 C CNN
	1    1350 825 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E8910E4
P 1100 4100
AR Path="/5E84A03F/5E8910E4" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E8910E4" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E8910E4" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E8910E4" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E8910E4" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E8910E4" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E8910E4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1100 3850 50  0001 C CNN
F 1 "GND" H 1100 3950 50  0000 C CNN
F 2 "" H 1100 4100 50  0001 C CNN
F 3 "" H 1100 4100 50  0001 C CNN
	1    1100 4100
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 1 1 5E9B3049
P 850 2500
AR Path="/5E84A03F/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5EAB9D7B/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5E83BCE9/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5EA2DD9E/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5EA9FEC9/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5EB2C18C/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5E890F69/5E9B3049" Ref="P?"  Part="1" 
AR Path="/5E9B3049" Ref="P?"  Part="1" 
F 0 "P?" H 767 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 767 816 50  0000 C CNN
F 2 "" H 850 2500 50  0000 C CNN
F 3 "" H 850 2500 50  0000 C CNN
	1    850  2500
	-1   0    0    1   
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 3 1 5E9AC88C
P 1725 2500
AR Path="/5E84A03F/5E9AC88C" Ref="P?"  Part="3" 
AR Path="/5EAB9D7B/5E9AC88C" Ref="P?"  Part="2" 
AR Path="/5E83BCE9/5E9AC88C" Ref="P?"  Part="3" 
AR Path="/5EA2DD9E/5E9AC88C" Ref="P?"  Part="3" 
AR Path="/5EA9FEC9/5E9AC88C" Ref="P?"  Part="3" 
AR Path="/5EB2C18C/5E9AC88C" Ref="P?"  Part="3" 
AR Path="/5E890F69/5E9AC88C" Ref="P?"  Part="3" 
AR Path="/5E9AC88C" Ref="P?"  Part="3" 
F 0 "P?" H 1642 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 1642 816 50  0000 C CNN
F 2 "" H 1725 2500 50  0000 C CNN
F 3 "" H 1725 2500 50  0000 C CNN
	3    1725 2500
	-1   0    0    1   
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 2 1 5E8F44F4
P 2625 2500
AR Path="/5E84A03F/5E8F44F4" Ref="P?"  Part="2" 
AR Path="/5EAB9D7B/5E8F44F4" Ref="P?"  Part="3" 
AR Path="/5E83BCE9/5E8F44F4" Ref="P?"  Part="2" 
AR Path="/5EA2DD9E/5E8F44F4" Ref="P?"  Part="2" 
AR Path="/5EA9FEC9/5E8F44F4" Ref="P?"  Part="2" 
AR Path="/5EB2C18C/5E8F44F4" Ref="P?"  Part="2" 
AR Path="/5E890F69/5E8F44F4" Ref="P?"  Part="2" 
AR Path="/5E8F44F4" Ref="P?"  Part="2" 
F 0 "P?" H 2542 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 2542 816 50  0000 C CNN
F 2 "" H 2625 2500 50  0000 C CNN
F 3 "" H 2625 2500 50  0000 C CNN
	2    2625 2500
	-1   0    0    1   
$EndComp
NoConn ~ 2825 1050
NoConn ~ 2825 1150
Text Label 3125 2050 2    50   ~ 0
~BANK1
Text Label 3125 1950 2    50   ~ 0
~BANK0
Text Label 3125 2150 2    50   ~ 0
~BANK2
Text Label 3125 2250 2    50   ~ 0
~BANK3
Text HLabel 4050 675  0    50   Input ~ 0
R{slash}Wb
Wire Wire Line
	4050 675  4350 675 
Wire Wire Line
	4050 575  4350 575 
Text Label 4350 575  2    50   ~ 0
PH2b
Text HLabel 4050 575  0    50   Input ~ 0
PH2b
Text Label 3725 2750 2    50   ~ 0
AD6
Text HLabel 3425 750  0    50   Input ~ 0
AA2
Text Label 3725 1850 2    50   ~ 0
AA13
Text Label 3725 2450 2    50   ~ 0
AD3
Text Label 3725 550  2    50   ~ 0
AA0
Text Label 3725 1950 2    50   ~ 0
AA14
Text Label 3725 2550 2    50   ~ 0
AD4
Text Label 3725 1250 2    50   ~ 0
AA7
Text Label 3725 850  2    50   ~ 0
AA3
Text Label 3725 750  2    50   ~ 0
AA2
Text Label 3725 650  2    50   ~ 0
AA1
Text Label 3725 950  2    50   ~ 0
AA4
Wire Wire Line
	3425 1250 3725 1250
Wire Wire Line
	3425 1450 3725 1450
Wire Wire Line
	3425 1950 3725 1950
Wire Wire Line
	3425 1750 3725 1750
Wire Wire Line
	3425 2050 3725 2050
Text Label 3725 1350 2    50   ~ 0
AA8
Text Label 3725 2650 2    50   ~ 0
AD5
Text Label 3725 2050 2    50   ~ 0
AA15
Wire Wire Line
	3425 650  3725 650 
Wire Wire Line
	3425 550  3725 550 
Wire Wire Line
	3425 1550 3725 1550
Wire Wire Line
	3425 1650 3725 1650
Text Label 3725 1750 2    50   ~ 0
AA12
Wire Wire Line
	3425 1850 3725 1850
Wire Wire Line
	3425 950  3725 950 
Wire Wire Line
	3425 1350 3725 1350
Text HLabel 3425 550  0    50   Input ~ 0
AA0
Text Label 3725 2150 2    50   ~ 0
AD0
Text Label 3725 1650 2    50   ~ 0
AA11
Text Label 3725 2250 2    50   ~ 0
AD1
Wire Wire Line
	3425 1050 3725 1050
Wire Wire Line
	3425 850  3725 850 
Text Label 3725 1550 2    50   ~ 0
AA10
Wire Wire Line
	3425 2750 3725 2750
Wire Wire Line
	3425 2850 3725 2850
Text HLabel 3425 650  0    50   Input ~ 0
AA1
Wire Wire Line
	3425 2150 3725 2150
Text Label 3725 1050 2    50   ~ 0
AA5
Wire Wire Line
	3425 2550 3725 2550
Wire Wire Line
	3425 750  3725 750 
Wire Wire Line
	3425 2450 3725 2450
Wire Wire Line
	3425 2650 3725 2650
Wire Wire Line
	3425 2250 3725 2250
Wire Wire Line
	3425 2350 3725 2350
Wire Wire Line
	3425 1150 3725 1150
Text HLabel 3425 950  0    50   Input ~ 0
AA4
Text HLabel 3425 1050 0    50   Input ~ 0
AA5
Text HLabel 3425 1150 0    50   Input ~ 0
AA6
Text HLabel 3425 2150 0    50   Input ~ 0
AD0
Text HLabel 3425 1250 0    50   Input ~ 0
AA7
Text HLabel 3425 1650 0    50   Input ~ 0
AA11
Text HLabel 3425 1550 0    50   Input ~ 0
AA10
Text HLabel 3425 1750 0    50   Input ~ 0
AA12
Text HLabel 3425 1950 0    50   Input ~ 0
AA14
Text HLabel 3425 2250 0    50   Input ~ 0
AD1
Text HLabel 3425 2050 0    50   Input ~ 0
AA15
Text HLabel 3425 1350 0    50   Input ~ 0
AA8
Text HLabel 3425 2750 0    50   Input ~ 0
AD6
Text HLabel 3425 2850 0    50   Input ~ 0
AD7
Text HLabel 3425 2650 0    50   Input ~ 0
AD5
Text Label 3725 2350 2    50   ~ 0
AD2
Text Label 3725 1450 2    50   ~ 0
AA9
Text HLabel 3425 2550 0    50   Input ~ 0
AD4
Text Label 3725 1150 2    50   ~ 0
AA6
Text Label 3725 2850 2    50   ~ 0
AD7
Text HLabel 3425 1850 0    50   Input ~ 0
AA13
Text HLabel 3425 2450 0    50   Input ~ 0
AD3
Text HLabel 3425 1450 0    50   Input ~ 0
AA9
Text HLabel 3425 850  0    50   Input ~ 0
AA3
Text HLabel 3425 2350 0    50   Input ~ 0
AD2
Wire Wire Line
	1925 2950 2225 2950
NoConn ~ 1050 2950
Text Label 4350 675  2    50   ~ 0
~R~Wb
NoConn ~ 1925 1050
NoConn ~ 1925 1150
NoConn ~ 1925 1250
NoConn ~ 1925 1350
NoConn ~ 1925 2250
NoConn ~ 1925 2350
NoConn ~ 1925 2450
NoConn ~ 1925 2550
NoConn ~ 1925 2650
NoConn ~ 1925 2850
NoConn ~ 1925 3050
NoConn ~ 1925 3250
NoConn ~ 1925 3350
NoConn ~ 1925 3450
NoConn ~ 1925 3550
Wire Wire Line
	1925 3150 2225 3150
Wire Wire Line
	2825 2750 3125 2750
Wire Wire Line
	2825 2850 3125 2850
Wire Wire Line
	2825 2950 3125 2950
Wire Wire Line
	2825 3050 3125 3050
Wire Wire Line
	2825 3150 3125 3150
Wire Wire Line
	2825 3250 3125 3250
Wire Wire Line
	2825 3350 3125 3350
Wire Wire Line
	2825 3450 3125 3450
Wire Wire Line
	2825 3550 3125 3550
Wire Wire Line
	2825 3650 3125 3650
Wire Wire Line
	2825 3750 3125 3750
Wire Wire Line
	2825 3850 3125 3850
Wire Wire Line
	2825 2550 3125 2550
Wire Wire Line
	2825 2450 3125 2450
Wire Wire Line
	2825 2250 3125 2250
Wire Wire Line
	2825 2350 3125 2350
Wire Wire Line
	2825 2650 3125 2650
Wire Wire Line
	1050 3050 1350 3050
Text Label 1350 3050 2    50   ~ 0
SYNb
$Comp
L 74xx:74LS245 U6
U 1 1 5EB9584B
P 2825 5500
AR Path="/5EB2C18C/5EB9584B" Ref="U6"  Part="1" 
AR Path="/5E890F69/5EB9584B" Ref="U?"  Part="1" 
F 0 "U6" H 2425 6250 50  0000 C CNN
F 1 "74LS245" H 2550 6150 50  0000 C CNN
F 2 "" H 2825 5500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 2825 5500 50  0001 C CNN
	1    2825 5500
	1    0    0    -1  
$EndComp
Text Label 3625 5300 2    50   ~ 0
AD3
Text Label 3625 5200 2    50   ~ 0
AD2
Text Label 3625 5100 2    50   ~ 0
AD1
Text Label 3625 5000 2    50   ~ 0
AD0
Wire Wire Line
	3325 5000 3625 5000
Wire Wire Line
	3325 5100 3625 5100
Wire Wire Line
	3325 5400 3625 5400
Wire Wire Line
	3325 5200 3625 5200
Wire Wire Line
	3325 5500 3625 5500
Wire Wire Line
	3325 5600 3625 5600
Wire Wire Line
	3325 5300 3625 5300
Wire Wire Line
	3325 5700 3625 5700
Text Label 3625 5600 2    50   ~ 0
AD6
Text Label 3625 5700 2    50   ~ 0
AD7
Text Label 3625 5500 2    50   ~ 0
AD5
Text Label 3625 5400 2    50   ~ 0
AD4
$Comp
L power:+5V #PWR?
U 1 1 5EB98995
P 2825 4450
AR Path="/5EB2C18C/5EB98995" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EB98995" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2825 4300 50  0001 C CNN
F 1 "+5V" H 2825 4600 50  0000 C CNN
F 2 "" H 2825 4450 50  0001 C CNN
F 3 "" H 2825 4450 50  0001 C CNN
	1    2825 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2825 4450 2825 4475
$Comp
L power:GND #PWR?
U 1 1 5EB99793
P 3000 4700
AR Path="/5EB2C18C/5EB99793" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EB99793" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3000 4450 50  0001 C CNN
F 1 "GND" H 3000 4550 50  0000 C CNN
F 2 "" H 3000 4700 50  0001 C CNN
F 3 "" H 3000 4700 50  0001 C CNN
	1    3000 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EB9A11B
P 3000 4600
AR Path="/5EB2C18C/5EB9A11B" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EB9A11B" Ref="C?"  Part="1" 
F 0 "C?" H 3092 4646 50  0000 L CNN
F 1 "22n" H 3092 4555 50  0000 L CNN
F 2 "" H 3000 4600 50  0001 C CNN
F 3 "~" H 3000 4600 50  0001 C CNN
	1    3000 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2825 4475 3000 4475
Wire Wire Line
	3000 4475 3000 4500
Connection ~ 2825 4475
Wire Wire Line
	2825 4475 2825 4700
$Comp
L power:GND #PWR?
U 1 1 5EB9ABFD
P 2825 6300
AR Path="/5EB2C18C/5EB9ABFD" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EB9ABFD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2825 6050 50  0001 C CNN
F 1 "GND" H 2825 6150 50  0000 C CNN
F 2 "" H 2825 6300 50  0001 C CNN
F 3 "" H 2825 6300 50  0001 C CNN
	1    2825 6300
	1    0    0    -1  
$EndComp
Text Label 4350 1250 2    50   ~ 0
SYNb
Wire Wire Line
	4050 1250 4350 1250
Wire Wire Line
	4050 1575 4350 1575
Text Label 4350 1375 2    50   ~ 0
~NMI
Wire Wire Line
	4050 1375 4350 1375
Wire Wire Line
	4050 1475 4350 1475
Text Label 4350 1475 2    50   ~ 0
~IRQ
Text Label 4350 1575 2    50   ~ 0
~RST
Text HLabel 4050 1250 0    50   Input ~ 0
SYNb
Text HLabel 4050 1375 0    50   Input ~ 0
~NMI
Text HLabel 4050 1475 0    50   Input ~ 0
~IRQ
Text HLabel 4050 1575 0    50   Input ~ 0
~RST
$Comp
L 74xx:74LS244 U2
U 1 1 5EBA250D
P 1325 7500
AR Path="/5EB2C18C/5EBA250D" Ref="U2"  Part="1" 
AR Path="/5E890F69/5EBA250D" Ref="U?"  Part="1" 
F 0 "U2" H 950 8250 50  0000 C CNN
F 1 "74LS244" H 1050 8150 50  0000 C CNN
F 2 "" H 1325 7500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS244" H 1325 7500 50  0001 C CNN
	1    1325 7500
	1    0    0    -1  
$EndComp
Text Label 525  7000 0    50   ~ 0
AA15
Text Label 525  7500 0    50   ~ 0
AA10
Text Label 525  7300 0    50   ~ 0
AA12
Text Label 525  7400 0    50   ~ 0
AA11
Text Label 525  7100 0    50   ~ 0
AA14
Text Label 525  7200 0    50   ~ 0
AA13
Text Label 525  7700 0    50   ~ 0
AA8
Text Label 525  9525 0    50   ~ 0
AA4
Text Label 525  7600 0    50   ~ 0
AA9
Text Label 525  9425 0    50   ~ 0
AA5
Text Label 525  9225 0    50   ~ 0
AA7
Text Label 525  9325 0    50   ~ 0
AA6
Text Label 525  9825 0    50   ~ 0
AA1
Text Label 525  9725 0    50   ~ 0
AA2
Text Label 525  9625 0    50   ~ 0
AA3
Text Label 525  9925 0    50   ~ 0
AA0
Wire Wire Line
	825  7600 525  7600
Wire Wire Line
	2125 7400 1825 7400
Wire Wire Line
	2125 7600 1825 7600
Wire Wire Line
	2125 7300 1825 7300
Wire Wire Line
	2125 7500 1825 7500
Wire Wire Line
	2125 7700 1825 7700
Wire Wire Line
	825  7100 525  7100
Wire Wire Line
	2125 7000 1825 7000
Wire Wire Line
	825  7000 525  7000
Wire Wire Line
	825  7400 525  7400
Wire Wire Line
	2125 7200 1825 7200
Wire Wire Line
	2125 7100 1825 7100
Wire Wire Line
	825  7700 525  7700
Wire Wire Line
	825  7500 525  7500
Wire Wire Line
	825  7200 525  7200
Wire Wire Line
	825  7300 525  7300
$Comp
L power:GND #PWR?
U 1 1 5EBA5389
P 1325 8300
AR Path="/5EB2C18C/5EBA5389" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBA5389" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1325 8050 50  0001 C CNN
F 1 "GND" H 1325 8150 50  0000 C CNN
F 2 "" H 1325 8300 50  0001 C CNN
F 3 "" H 1325 8300 50  0001 C CNN
	1    1325 8300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1325 6450 1325 6475
Wire Wire Line
	1325 6475 1325 6700
Wire Wire Line
	1325 6475 1500 6475
$Comp
L power:GND #PWR?
U 1 1 5EBA60C4
P 1500 6700
AR Path="/5EB2C18C/5EBA60C4" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBA60C4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1500 6450 50  0001 C CNN
F 1 "GND" H 1500 6550 50  0000 C CNN
F 2 "" H 1500 6700 50  0001 C CNN
F 3 "" H 1500 6700 50  0001 C CNN
	1    1500 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EBA60D2
P 1500 6600
AR Path="/5EB2C18C/5EBA60D2" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EBA60D2" Ref="C?"  Part="1" 
F 0 "C?" H 1592 6646 50  0000 L CNN
F 1 "22n" H 1592 6555 50  0000 L CNN
F 2 "" H 1500 6600 50  0001 C CNN
F 3 "~" H 1500 6600 50  0001 C CNN
	1    1500 6600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EBA60DF
P 1325 6450
AR Path="/5EB2C18C/5EBA60DF" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBA60DF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1325 6300 50  0001 C CNN
F 1 "+5V" H 1325 6600 50  0000 C CNN
F 2 "" H 1325 6450 50  0001 C CNN
F 3 "" H 1325 6450 50  0001 C CNN
	1    1325 6450
	1    0    0    -1  
$EndComp
Connection ~ 1325 6475
Wire Wire Line
	1500 6475 1500 6500
Wire Wire Line
	825  9325 525  9325
Wire Wire Line
	1325 8700 1500 8700
Wire Wire Line
	825  9625 525  9625
Wire Wire Line
	825  9225 525  9225
Wire Wire Line
	1325 8700 1325 8925
$Comp
L power:+5V #PWR?
U 1 1 5EBA76A5
P 1325 8675
AR Path="/5EB2C18C/5EBA76A5" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBA76A5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1325 8525 50  0001 C CNN
F 1 "+5V" H 1325 8825 50  0000 C CNN
F 2 "" H 1325 8675 50  0001 C CNN
F 3 "" H 1325 8675 50  0001 C CNN
	1    1325 8675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EBA76B2
P 1500 8925
AR Path="/5EB2C18C/5EBA76B2" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBA76B2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1500 8675 50  0001 C CNN
F 1 "GND" H 1500 8775 50  0000 C CNN
F 2 "" H 1500 8925 50  0001 C CNN
F 3 "" H 1500 8925 50  0001 C CNN
	1    1500 8925
	1    0    0    -1  
$EndComp
Wire Wire Line
	825  9725 525  9725
Wire Wire Line
	825  9925 525  9925
Wire Wire Line
	1325 8675 1325 8700
Wire Wire Line
	825  9825 525  9825
Wire Wire Line
	1500 8700 1500 8725
$Comp
L Device:C_Small C?
U 1 1 5EBA76CD
P 1500 8825
AR Path="/5EB2C18C/5EBA76CD" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EBA76CD" Ref="C?"  Part="1" 
F 0 "C?" H 1592 8871 50  0000 L CNN
F 1 "22n" H 1592 8780 50  0000 L CNN
F 2 "" H 1500 8825 50  0001 C CNN
F 3 "~" H 1500 8825 50  0001 C CNN
	1    1500 8825
	1    0    0    -1  
$EndComp
Connection ~ 1325 8700
Wire Wire Line
	825  9425 525  9425
Wire Wire Line
	825  9525 525  9525
$Comp
L power:GND #PWR?
U 1 1 5EBA76DD
P 1325 10525
AR Path="/5EB2C18C/5EBA76DD" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBA76DD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1325 10275 50  0001 C CNN
F 1 "GND" H 1325 10375 50  0000 C CNN
F 2 "" H 1325 10525 50  0001 C CNN
F 3 "" H 1325 10525 50  0001 C CNN
	1    1325 10525
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS244 U1
U 1 1 5EBA76FD
P 1325 9725
AR Path="/5EB2C18C/5EBA76FD" Ref="U1"  Part="1" 
AR Path="/5E890F69/5EBA76FD" Ref="U?"  Part="1" 
F 0 "U1" H 950 10475 50  0000 C CNN
F 1 "74LS244" H 1050 10375 50  0000 C CNN
F 2 "" H 1325 9725 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS244" H 1325 9725 50  0001 C CNN
	1    1325 9725
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 9825 1825 9825
Wire Wire Line
	2125 9725 1825 9725
Wire Wire Line
	2125 9425 1825 9425
Wire Wire Line
	2125 9925 1825 9925
Wire Wire Line
	2125 9625 1825 9625
Wire Wire Line
	2125 9525 1825 9525
Wire Wire Line
	2125 9225 1825 9225
Wire Wire Line
	2125 9325 1825 9325
Wire Wire Line
	825  7900 800  7900
Wire Wire Line
	825  8000 800  8000
Wire Wire Line
	2325 6000 2275 6000
Wire Wire Line
	2325 5900 2025 5900
Wire Wire Line
	2325 5600 2025 5600
Wire Wire Line
	2325 5500 2025 5500
Wire Wire Line
	2325 5200 2025 5200
Wire Wire Line
	2325 5700 2025 5700
Wire Wire Line
	2325 5400 2025 5400
Wire Wire Line
	2325 5300 2025 5300
Wire Wire Line
	2325 5000 2025 5000
Wire Wire Line
	2325 5100 2025 5100
$Comp
L power:GND #PWR?
U 1 1 5EBB185D
P 800 8050
AR Path="/5EB2C18C/5EBB185D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBB185D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 800 7800 50  0001 C CNN
F 1 "GND" H 800 7900 50  0000 C CNN
F 2 "" H 800 8050 50  0001 C CNN
F 3 "" H 800 8050 50  0001 C CNN
	1    800  8050
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  7900 800  8000
Connection ~ 800  8000
Wire Wire Line
	800  8000 800  8050
Connection ~ 800  10225
Wire Wire Line
	825  10225 800  10225
Wire Wire Line
	825  10125 800  10125
$Comp
L power:GND #PWR?
U 1 1 5EBB1C5B
P 800 10275
AR Path="/5EB2C18C/5EBB1C5B" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBB1C5B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 800 10025 50  0001 C CNN
F 1 "GND" H 800 10125 50  0000 C CNN
F 2 "" H 800 10275 50  0001 C CNN
F 3 "" H 800 10275 50  0001 C CNN
	1    800  10275
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  10125 800  10225
Wire Wire Line
	800  10225 800  10275
Text Label 2125 9925 2    50   ~ 0
A0
Text Label 2125 9825 2    50   ~ 0
A1
Text Label 2125 9725 2    50   ~ 0
A2
Text Label 2125 9625 2    50   ~ 0
A3
Text Label 2125 9525 2    50   ~ 0
A4
Text Label 2125 9425 2    50   ~ 0
A5
Text Label 2125 9325 2    50   ~ 0
A6
Text Label 2125 9225 2    50   ~ 0
A7
Text Label 2125 7700 2    50   ~ 0
A8
Text Label 2125 7600 2    50   ~ 0
A9
Text Label 2125 7500 2    50   ~ 0
A10
Text Label 2125 7400 2    50   ~ 0
A11
Text Label 2125 7300 2    50   ~ 0
A12
Text Label 2125 7200 2    50   ~ 0
A13
Text Label 2125 7100 2    50   ~ 0
A14
Text Label 2125 7000 2    50   ~ 0
A15
Text Label 2025 5000 0    50   ~ 0
D0
Text Label 2025 5100 0    50   ~ 0
D1
Text Label 2025 5200 0    50   ~ 0
D2
Text Label 2025 5300 0    50   ~ 0
D3
Text Label 2025 5400 0    50   ~ 0
D4
Text Label 2025 5500 0    50   ~ 0
D5
Text Label 2025 5600 0    50   ~ 0
D6
Text Label 2025 5700 0    50   ~ 0
D7
$Comp
L 65xx:6522 U12
U 1 1 5EBBBC4C
P 6600 2400
AR Path="/5EB2C18C/5EBBBC4C" Ref="U12"  Part="1" 
AR Path="/5E890F69/5EBBBC4C" Ref="U?"  Part="1" 
F 0 "U12" H 6250 3775 50  0000 C CNN
F 1 "6522" H 6300 3700 50  0000 C CIB
F 2 "" H 6600 2550 50  0001 C CNN
F 3 "http://www.6502.org/documents/datasheets/mos/mos_6522_preliminary_nov_1977.pdf" H 6600 2550 50  0001 C CNN
	1    6600 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 700  6600 725 
Wire Wire Line
	6600 725  6600 950 
Wire Wire Line
	6600 725  6775 725 
$Comp
L power:GND #PWR?
U 1 1 5EBC1585
P 6775 950
AR Path="/5EB2C18C/5EBC1585" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBC1585" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6775 700 50  0001 C CNN
F 1 "GND" H 6775 800 50  0000 C CNN
F 2 "" H 6775 950 50  0001 C CNN
F 3 "" H 6775 950 50  0001 C CNN
	1    6775 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EBC1593
P 6775 850
AR Path="/5EB2C18C/5EBC1593" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EBC1593" Ref="C?"  Part="1" 
F 0 "C?" H 6867 896 50  0000 L CNN
F 1 "22n" H 6867 805 50  0000 L CNN
F 2 "" H 6775 850 50  0001 C CNN
F 3 "~" H 6775 850 50  0001 C CNN
	1    6775 850 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EBC15A0
P 6600 700
AR Path="/5EB2C18C/5EBC15A0" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBC15A0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6600 550 50  0001 C CNN
F 1 "+5V" H 6600 850 50  0000 C CNN
F 2 "" H 6600 700 50  0001 C CNN
F 3 "" H 6600 700 50  0001 C CNN
	1    6600 700 
	1    0    0    -1  
$EndComp
Connection ~ 6600 725 
Wire Wire Line
	6775 725  6775 750 
$Comp
L power:GND #PWR?
U 1 1 5EBC2A30
P 6600 3850
AR Path="/5EB2C18C/5EBC2A30" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBC2A30" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6600 3600 50  0001 C CNN
F 1 "GND" H 6600 3700 50  0000 C CNN
F 2 "" H 6600 3850 50  0001 C CNN
F 3 "" H 6600 3850 50  0001 C CNN
	1    6600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3200 5700 3200
Text Label 5700 3400 0    50   ~ 0
D6
Wire Wire Line
	6000 3400 5700 3400
Text Label 5700 3200 0    50   ~ 0
D4
Text Label 5700 2900 0    50   ~ 0
D1
Wire Wire Line
	6000 3000 5700 3000
Wire Wire Line
	6000 3500 5700 3500
Wire Wire Line
	6000 2900 5700 2900
Wire Wire Line
	6000 3300 5700 3300
Wire Wire Line
	6000 3100 5700 3100
Text Label 5700 3100 0    50   ~ 0
D3
Text Label 5700 2800 0    50   ~ 0
D0
Text Label 5700 3500 0    50   ~ 0
D7
Text Label 5700 3300 0    50   ~ 0
D5
Wire Wire Line
	6000 2800 5700 2800
Text Label 5700 3000 0    50   ~ 0
D2
$Comp
L 74xx:74LS86 U29
U 1 1 5EBCB926
P 1225 5325
AR Path="/5EB2C18C/5EBCB926" Ref="U29"  Part="1" 
AR Path="/5E890F69/5EBCB926" Ref="U?"  Part="1" 
F 0 "U29" H 1225 5650 50  0000 C CNN
F 1 "74LS86" H 1225 5559 50  0000 C CNN
F 2 "" H 1225 5325 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 1225 5325 50  0001 C CNN
	1    1225 5325
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U29
U 2 1 5EBCFD5C
P 9175 10050
AR Path="/5EB2C18C/5EBCFD5C" Ref="U29"  Part="2" 
AR Path="/5E890F69/5EBCFD5C" Ref="U?"  Part="2" 
F 0 "U29" H 9175 10375 50  0000 C CNN
F 1 "74LS86" H 9175 10284 50  0000 C CNN
F 2 "" H 9175 10050 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 9175 10050 50  0001 C CNN
	2    9175 10050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U29
U 3 1 5EBD0822
P 1200 4700
AR Path="/5EB2C18C/5EBD0822" Ref="U29"  Part="3" 
AR Path="/5E890F69/5EBD0822" Ref="U?"  Part="3" 
F 0 "U29" H 1200 5025 50  0000 C CNN
F 1 "74LS86" H 1200 4934 50  0000 C CNN
F 2 "" H 1200 4700 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 1200 4700 50  0001 C CNN
	3    1200 4700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U29
U 4 1 5EBD2040
P 1225 5850
AR Path="/5EB2C18C/5EBD2040" Ref="U29"  Part="4" 
AR Path="/5E890F69/5EBD2040" Ref="U?"  Part="4" 
F 0 "U29" H 1225 6175 50  0000 C CNN
F 1 "74LS86" H 1225 6084 50  0000 C CNN
F 2 "" H 1225 5850 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 1225 5850 50  0001 C CNN
	4    1225 5850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U29
U 5 1 5EBD38BA
P 10950 2550
AR Path="/5EB2C18C/5EBD38BA" Ref="U29"  Part="5" 
AR Path="/5E890F69/5EBD38BA" Ref="U?"  Part="5" 
F 0 "U29" H 10875 2575 50  0000 L CNN
F 1 "74LS86" H 10825 2500 50  0000 L CNN
F 2 "" H 10950 2550 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 10950 2550 50  0001 C CNN
	5    10950 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9AC88D
P 10950 1800
AR Path="/5EB2C18C/5E9AC88D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9AC88D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10950 1650 50  0001 C CNN
F 1 "+5V" H 10950 1950 50  0000 C CNN
F 2 "" H 10950 1800 50  0001 C CNN
F 3 "" H 10950 1800 50  0001 C CNN
	1    10950 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10950 1825 10950 2050
Wire Wire Line
	11125 1825 11125 1850
$Comp
L Device:C_Small C?
U 1 1 5E9AC88E
P 11125 1950
AR Path="/5EB2C18C/5E9AC88E" Ref="C?"  Part="1" 
AR Path="/5E890F69/5E9AC88E" Ref="C?"  Part="1" 
F 0 "C?" H 11217 1996 50  0000 L CNN
F 1 "22n" H 11217 1905 50  0000 L CNN
F 2 "" H 11125 1950 50  0001 C CNN
F 3 "~" H 11125 1950 50  0001 C CNN
	1    11125 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10950 1800 10950 1825
Wire Wire Line
	10950 1825 11125 1825
Connection ~ 10950 1825
$Comp
L power:GND #PWR?
U 1 1 5E9AC88F
P 11125 2050
AR Path="/5EB2C18C/5E9AC88F" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9AC88F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11125 1800 50  0001 C CNN
F 1 "GND" H 11125 1900 50  0000 C CNN
F 2 "" H 11125 2050 50  0001 C CNN
F 3 "" H 11125 2050 50  0001 C CNN
	1    11125 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9AC890
P 10950 3050
AR Path="/5EB2C18C/5E9AC890" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9AC890" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10950 2800 50  0001 C CNN
F 1 "GND" H 10950 2900 50  0000 C CNN
F 2 "" H 10950 3050 50  0001 C CNN
F 3 "" H 10950 3050 50  0001 C CNN
	1    10950 3050
	1    0    0    -1  
$EndComp
Text Label 600  4800 0    50   ~ 0
PH2b
Wire Wire Line
	900  4800 600  4800
$Comp
L power:GND #PWR?
U 1 1 5EBDE807
P 850 4850
AR Path="/5E84A03F/5EBDE807" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EBDE807" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EBDE807" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5EBDE807" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5EBDE807" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5EBDE807" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBDE807" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 850 4600 50  0001 C CNN
F 1 "GND" H 850 4700 50  0000 C CNN
F 2 "" H 850 4850 50  0001 C CNN
F 3 "" H 850 4850 50  0001 C CNN
	1    850  4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  4850 850  4600
Wire Wire Line
	850  4600 900  4600
$Comp
L power:GND #PWR?
U 1 1 5EBDF134
P 875 5975
AR Path="/5E84A03F/5EBDF134" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EBDF134" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EBDF134" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5EBDF134" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5EBDF134" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5EBDF134" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBDF134" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 875 5725 50  0001 C CNN
F 1 "GND" H 875 5825 50  0000 C CNN
F 2 "" H 875 5975 50  0001 C CNN
F 3 "" H 875 5975 50  0001 C CNN
	1    875  5975
	1    0    0    -1  
$EndComp
Wire Wire Line
	875  5975 875  5950
Wire Wire Line
	875  5950 925  5950
Wire Wire Line
	1500 4700 1800 4700
Text Label 1800 4700 2    50   ~ 0
PH2bb
Wire Wire Line
	6000 1400 5700 1400
Text Label 5700 1400 0    50   ~ 0
PH2bb
Wire Wire Line
	1525 5325 1625 5325
$Comp
L power:GND #PWR?
U 1 1 5EBECF6C
P 875 5450
AR Path="/5E84A03F/5EBECF6C" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EBECF6C" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EBECF6C" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5EBECF6C" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5EBECF6C" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5EBECF6C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EBECF6C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 875 5200 50  0001 C CNN
F 1 "GND" H 875 5300 50  0000 C CNN
F 2 "" H 875 5450 50  0001 C CNN
F 3 "" H 875 5450 50  0001 C CNN
	1    875  5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	875  5450 875  5425
Wire Wire Line
	875  5425 925  5425
Wire Wire Line
	6000 1300 5700 1300
Text Label 1925 5325 2    50   ~ 0
~RSTbb
Wire Wire Line
	925  5225 650  5225
Wire Wire Line
	1925 3950 2225 3950
Wire Wire Line
	1925 3650 2225 3650
Text Label 2225 3750 2    50   ~ 0
~IRQ
Wire Wire Line
	1925 3750 2225 3750
Text Label 2225 3650 2    50   ~ 0
~NMI
Text Label 2225 3950 2    50   ~ 0
~RSTb
Wire Wire Line
	1925 3850 2225 3850
Text Label 2225 3850 2    50   ~ 0
~RST
Text Label 650  5225 0    50   ~ 0
~RSTb
Text Label 5700 1300 0    50   ~ 0
~RSTbb
$Comp
L 74xx:74LS06 U27
U 7 1 5E87940D
P 8250 4200
AR Path="/5EB2C18C/5E87940D" Ref="U27"  Part="7" 
AR Path="/5E890F69/5E87940D" Ref="U?"  Part="7" 
F 0 "U27" H 8150 4225 50  0000 L CNN
F 1 "74LS06" H 8125 4150 50  0000 L CNN
F 2 "" H 8250 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 8250 4200 50  0001 C CNN
	7    8250 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E87BA37
P 8250 3450
AR Path="/5EB2C18C/5E87BA37" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E87BA37" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8250 3300 50  0001 C CNN
F 1 "+5V" H 8250 3600 50  0000 C CNN
F 2 "" H 8250 3450 50  0001 C CNN
F 3 "" H 8250 3450 50  0001 C CNN
	1    8250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3475 8250 3700
Wire Wire Line
	8425 3475 8425 3500
Wire Wire Line
	8250 3450 8250 3475
$Comp
L Device:C_Small C?
U 1 1 5E9AC886
P 8425 3600
AR Path="/5EB2C18C/5E9AC886" Ref="C?"  Part="1" 
AR Path="/5E890F69/5E9AC886" Ref="C?"  Part="1" 
F 0 "C?" H 8517 3646 50  0000 L CNN
F 1 "22n" H 8517 3555 50  0000 L CNN
F 2 "" H 8425 3600 50  0001 C CNN
F 3 "~" H 8425 3600 50  0001 C CNN
	1    8425 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3475 8425 3475
Connection ~ 8250 3475
$Comp
L power:GND #PWR?
U 1 1 5E9B303C
P 8425 3700
AR Path="/5EB2C18C/5E9B303C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9B303C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8425 3450 50  0001 C CNN
F 1 "GND" H 8425 3550 50  0000 C CNN
F 2 "" H 8425 3700 50  0001 C CNN
F 3 "" H 8425 3700 50  0001 C CNN
	1    8425 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9B303D
P 8250 4700
AR Path="/5EB2C18C/5E9B303D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9B303D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8250 4450 50  0001 C CNN
F 1 "GND" H 8250 4550 50  0000 C CNN
F 2 "" H 8250 4700 50  0001 C CNN
F 3 "" H 8250 4700 50  0001 C CNN
	1    8250 4700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 5E888207
P 5125 1400
AR Path="/5EB2C18C/5E888207" Ref="J?"  Part="1" 
AR Path="/5E890F69/5E888207" Ref="J?"  Part="1" 
F 0 "J?" H 5175 1717 50  0000 C CNN
F 1 "IRQ_SEL" H 5175 1626 50  0000 C CNN
F 2 "" H 5125 1400 50  0001 C CNN
F 3 "~" H 5125 1400 50  0001 C CNN
	1    5125 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 1300 5475 1300
Wire Wire Line
	5475 1300 5475 1400
Wire Wire Line
	5475 1600 6000 1600
Wire Wire Line
	5425 1400 5475 1400
Connection ~ 5475 1400
Wire Wire Line
	5475 1400 5475 1500
Wire Wire Line
	5425 1500 5475 1500
Connection ~ 5475 1500
Wire Wire Line
	5475 1500 5475 1600
Wire Wire Line
	5425 1600 5475 1600
Connection ~ 5475 1600
Wire Wire Line
	1925 1450 2200 1450
Wire Wire Line
	1925 1550 2200 1550
Wire Wire Line
	1925 1650 2200 1650
Wire Wire Line
	1925 1750 2200 1750
Wire Wire Line
	1925 1850 2200 1850
Wire Wire Line
	1925 1950 2200 1950
Wire Wire Line
	1925 2050 2200 2050
Wire Wire Line
	1925 2150 2200 2150
Text Label 2200 1550 2    50   ~ 0
~IRQ1
Text Label 2200 1950 2    50   ~ 0
~IRQ5
Text Label 2200 1850 2    50   ~ 0
~IRQ4
Text Label 2200 1750 2    50   ~ 0
~IRQ3
Text Label 2200 2150 2    50   ~ 0
~IRQ7
Text Label 2200 1650 2    50   ~ 0
~IRQ2
Text Label 2200 2050 2    50   ~ 0
~IRQ6
Text Label 2200 1450 2    50   ~ 0
~IRQ0
Text Label 4375 1825 2    50   ~ 0
~IRQ1
Text Label 4375 2125 2    50   ~ 0
~IRQ4
Text Label 4375 2425 2    50   ~ 0
~IRQ7
Text Label 4375 1925 2    50   ~ 0
~IRQ2
Wire Wire Line
	4100 2425 4375 2425
Wire Wire Line
	4100 2125 4375 2125
Wire Wire Line
	4100 2325 4375 2325
Text Label 4375 2225 2    50   ~ 0
~IRQ5
Text Label 4375 2325 2    50   ~ 0
~IRQ6
Wire Wire Line
	4100 1825 4375 1825
Wire Wire Line
	4100 1725 4375 1725
Text Label 4375 1725 2    50   ~ 0
~IRQ0
Text Label 4375 2025 2    50   ~ 0
~IRQ3
Wire Wire Line
	4100 2025 4375 2025
Wire Wire Line
	4100 2225 4375 2225
Wire Wire Line
	4100 1925 4375 1925
Wire Wire Line
	4100 2725 4400 2725
Wire Wire Line
	4100 2625 4400 2625
Wire Wire Line
	4100 2825 4400 2825
Wire Wire Line
	4100 2525 4400 2525
Text Label 4400 2625 2    50   ~ 0
~BANK1
Text Label 4400 2525 2    50   ~ 0
~BANK0
Text Label 4400 2725 2    50   ~ 0
~BANK2
Text Label 4400 2825 2    50   ~ 0
~BANK3
Text HLabel 4100 1725 0    50   Input ~ 0
IRQ0
Text HLabel 4100 1825 0    50   Input ~ 0
IRQ1
Text HLabel 4100 1925 0    50   Input ~ 0
IRQ2
Text HLabel 4100 2025 0    50   Input ~ 0
IRQ3
Text HLabel 4100 2125 0    50   Input ~ 0
IRQ4
Text HLabel 4100 2225 0    50   Input ~ 0
IRQ5
Text HLabel 4100 2325 0    50   Input ~ 0
IRQ6
Text HLabel 4100 2425 0    50   Input ~ 0
IRQ7
Text HLabel 4100 2525 0    50   Input ~ 0
~BANK0
Text HLabel 4100 2625 0    50   Input ~ 0
~BANK1
Text HLabel 4100 2725 0    50   Input ~ 0
~BANK2
Text HLabel 4100 2825 0    50   Input ~ 0
~BANK3
Text Label 3125 2750 2    50   ~ 0
~USR1
Text Label 3125 2850 2    50   ~ 0
~USR2
Text Label 3125 2950 2    50   ~ 0
~USR3
Text Label 3125 3050 2    50   ~ 0
~USR4
Wire Wire Line
	4050 825  4350 825 
Wire Wire Line
	4050 925  4350 925 
Wire Wire Line
	4050 1025 4350 1025
Wire Wire Line
	4050 1125 4350 1125
Text Label 4350 925  2    50   ~ 0
~USR2
Text Label 4350 825  2    50   ~ 0
~USR1
Text Label 4350 1125 2    50   ~ 0
~USR4
Text Label 4350 1025 2    50   ~ 0
~USR3
Text HLabel 4050 825  0    50   Input ~ 0
~USR1
Text HLabel 4050 925  0    50   Input ~ 0
~USR2
Text HLabel 4050 1025 0    50   Input ~ 0
~USR3
Text HLabel 4050 1125 0    50   Input ~ 0
~USR4
Wire Wire Line
	925  5750 625  5750
Text Label 1825 5850 2    50   ~ 0
R~W~bb
Wire Wire Line
	1525 5850 1825 5850
Wire Wire Line
	6000 2600 5700 2600
Wire Wire Line
	4925 1300 4500 1300
Wire Wire Line
	4925 1400 4500 1400
Wire Wire Line
	4925 1500 4500 1500
Wire Wire Line
	4925 1600 4500 1600
Text Label 4500 1600 0    50   ~ 0
~IRQ6
Text Label 4500 1500 0    50   ~ 0
~IRQ4
Text Label 4500 1400 0    50   ~ 0
~IRQ2
Text Label 4500 1300 0    50   ~ 0
~IRQ0
Wire Wire Line
	5700 2200 6000 2200
Wire Wire Line
	5700 2300 6000 2300
Wire Wire Line
	5700 2100 6000 2100
Wire Wire Line
	5700 2400 6000 2400
Text Label 5700 2100 0    50   ~ 0
A0
Text Label 5700 2200 0    50   ~ 0
A1
Text Label 5700 2300 0    50   ~ 0
A2
Text Label 5700 2400 0    50   ~ 0
A3
$Comp
L power:GND #PWR?
U 1 1 5EF8D67A
P 15450 2975
AR Path="/5EB2C18C/5EF8D67A" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EF8D67A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15450 2725 50  0001 C CNN
F 1 "GND" H 15450 2825 50  0000 C CNN
F 2 "" H 15450 2975 50  0001 C CNN
F 3 "" H 15450 2975 50  0001 C CNN
	1    15450 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	15450 725  15450 750 
Wire Wire Line
	15450 750  15625 750 
$Comp
L power:GND #PWR?
U 1 1 5EF8DC9B
P 15625 975
AR Path="/5EB2C18C/5EF8DC9B" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EF8DC9B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15625 725 50  0001 C CNN
F 1 "GND" H 15625 825 50  0000 C CNN
F 2 "" H 15625 975 50  0001 C CNN
F 3 "" H 15625 975 50  0001 C CNN
	1    15625 975 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF8DCA9
P 15625 875
AR Path="/5EB2C18C/5EF8DCA9" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EF8DCA9" Ref="C?"  Part="1" 
F 0 "C?" H 15717 921 50  0000 L CNN
F 1 "22n" H 15717 830 50  0000 L CNN
F 2 "" H 15625 875 50  0001 C CNN
F 3 "~" H 15625 875 50  0001 C CNN
	1    15625 875 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EF8DCB6
P 15450 725
AR Path="/5EB2C18C/5EF8DCB6" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EF8DCB6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15450 575 50  0001 C CNN
F 1 "+5V" H 15450 875 50  0000 C CNN
F 2 "" H 15450 725 50  0001 C CNN
F 3 "" H 15450 725 50  0001 C CNN
	1    15450 725 
	1    0    0    -1  
$EndComp
Connection ~ 15450 750 
Wire Wire Line
	15625 750  15625 775 
Wire Wire Line
	15450 750  15450 1075
Wire Wire Line
	15850 1675 16000 1675
Text Label 16000 1875 2    50   ~ 0
D6
Wire Wire Line
	15850 1875 16000 1875
Text Label 16000 1675 2    50   ~ 0
D4
Text Label 16000 1375 2    50   ~ 0
D1
Wire Wire Line
	15850 1475 16000 1475
Wire Wire Line
	15850 1975 16000 1975
Wire Wire Line
	15850 1375 16000 1375
Wire Wire Line
	15850 1775 16000 1775
Wire Wire Line
	15850 1575 16000 1575
Text Label 16000 1575 2    50   ~ 0
D3
Text Label 16000 1275 2    50   ~ 0
D0
Text Label 16000 1975 2    50   ~ 0
D7
Text Label 16000 1775 2    50   ~ 0
D5
Wire Wire Line
	15850 1275 16000 1275
Text Label 16000 1475 2    50   ~ 0
D2
Wire Wire Line
	15050 1275 14875 1275
Wire Wire Line
	15050 1775 14875 1775
Wire Wire Line
	15050 1875 14875 1875
Wire Wire Line
	15050 1975 14875 1975
Wire Wire Line
	15050 2075 14875 2075
Wire Wire Line
	15050 2175 14875 2175
Wire Wire Line
	15050 2275 14625 2275
Wire Wire Line
	15050 2525 15000 2525
Wire Wire Line
	15050 2625 15000 2625
Text Label 14875 1275 0    50   ~ 0
A0
Wire Wire Line
	13800 2900 13975 2900
Wire Wire Line
	13800 2900 13800 3125
$Comp
L power:+5V #PWR?
U 1 1 5EF900DC
P 13800 2875
AR Path="/5EB2C18C/5EF900DC" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EF900DC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13800 2725 50  0001 C CNN
F 1 "+5V" H 13800 3025 50  0000 C CNN
F 2 "" H 13800 2875 50  0001 C CNN
F 3 "" H 13800 2875 50  0001 C CNN
	1    13800 2875
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF900E9
P 13975 3125
AR Path="/5EB2C18C/5EF900E9" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EF900E9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13975 2875 50  0001 C CNN
F 1 "GND" H 13975 2975 50  0000 C CNN
F 2 "" H 13975 3125 50  0001 C CNN
F 3 "" H 13975 3125 50  0001 C CNN
	1    13975 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	13800 2875 13800 2900
Wire Wire Line
	13975 2900 13975 2925
$Comp
L Device:C_Small C?
U 1 1 5EF900F9
P 13975 3025
AR Path="/5EB2C18C/5EF900F9" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EF900F9" Ref="C?"  Part="1" 
F 0 "C?" H 14067 3071 50  0000 L CNN
F 1 "22n" H 14067 2980 50  0000 L CNN
F 2 "" H 13975 3025 50  0001 C CNN
F 3 "~" H 13975 3025 50  0001 C CNN
	1    13975 3025
	1    0    0    -1  
$EndComp
Connection ~ 13800 2900
$Comp
L power:GND #PWR?
U 1 1 5EF90107
P 13800 4725
AR Path="/5EB2C18C/5EF90107" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EF90107" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13800 4475 50  0001 C CNN
F 1 "GND" H 13800 4575 50  0000 C CNN
F 2 "" H 13800 4725 50  0001 C CNN
F 3 "" H 13800 4725 50  0001 C CNN
	1    13800 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	13300 3825 13225 3825
Wire Wire Line
	13225 3825 13225 3275
$Comp
L power:+5V #PWR?
U 1 1 5EFD2EFD
P 13225 3275
AR Path="/5EB2C18C/5EFD2EFD" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFD2EFD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13225 3125 50  0001 C CNN
F 1 "+5V" H 13225 3425 50  0000 C CNN
F 2 "" H 13225 3275 50  0001 C CNN
F 3 "" H 13225 3275 50  0001 C CNN
	1    13225 3275
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS244 U13
U 1 1 5EF90127
P 13800 3925
AR Path="/5EB2C18C/5EF90127" Ref="U13"  Part="1" 
AR Path="/5E890F69/5EF90127" Ref="U?"  Part="1" 
F 0 "U13" H 13425 4675 50  0000 C CNN
F 1 "74LS244" H 13525 4575 50  0000 C CNN
F 2 "" H 13800 3925 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS244" H 13800 3925 50  0001 C CNN
	1    13800 3925
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFD47CB
P 13225 4100
AR Path="/5EB2C18C/5EFD47CB" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFD47CB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13225 3850 50  0001 C CNN
F 1 "GND" H 13225 3950 50  0000 C CNN
F 2 "" H 13225 4100 50  0001 C CNN
F 3 "" H 13225 4100 50  0001 C CNN
	1    13225 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	13300 3925 13225 3925
Wire Wire Line
	13300 3525 12975 3525
Wire Wire Line
	13300 3625 12975 3625
Wire Wire Line
	13300 3725 12975 3725
Wire Wire Line
	13300 4025 12975 4025
Wire Wire Line
	13300 4125 12975 4125
$Comp
L es65-rescue:6331-my_ics U17
U 1 1 5EFE0AE8
P 5375 4500
F 0 "U17" H 5100 5025 50  0000 C CNN
F 1 "6331" H 5150 4950 50  0000 C CNN
F 2 "" H 5075 4600 50  0001 C CNN
F 3 "" H 5075 4600 50  0001 C CNN
	1    5375 4500
	1    0    0    -1  
$EndComp
Text Notes 4975 3900 0    50   ~ 0
MUC I/O
$Comp
L power:GND #PWR?
U 1 1 5EFE2BDA
P 5375 5100
AR Path="/5EB2C18C/5EFE2BDA" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFE2BDA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5375 4850 50  0001 C CNN
F 1 "GND" H 5375 4950 50  0000 C CNN
F 2 "" H 5375 5100 50  0001 C CNN
F 3 "" H 5375 5100 50  0001 C CNN
	1    5375 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFE38C1
P 5550 3925
AR Path="/5EB2C18C/5EFE38C1" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFE38C1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5550 3675 50  0001 C CNN
F 1 "GND" H 5550 3775 50  0000 C CNN
F 2 "" H 5550 3925 50  0001 C CNN
F 3 "" H 5550 3925 50  0001 C CNN
	1    5550 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5375 3675 5375 3700
Wire Wire Line
	5375 3700 5550 3700
Wire Wire Line
	5550 3700 5550 3725
$Comp
L Device:C_Small C?
U 1 1 5EFE38D3
P 5550 3825
AR Path="/5EB2C18C/5EFE38D3" Ref="C?"  Part="1" 
AR Path="/5E890F69/5EFE38D3" Ref="C?"  Part="1" 
F 0 "C?" H 5642 3871 50  0000 L CNN
F 1 "22n" H 5642 3780 50  0000 L CNN
F 2 "" H 5550 3825 50  0001 C CNN
F 3 "~" H 5550 3825 50  0001 C CNN
	1    5550 3825
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EFE38E0
P 5375 3675
AR Path="/5EB2C18C/5EFE38E0" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFE38E0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5375 3525 50  0001 C CNN
F 1 "+5V" H 5375 3825 50  0000 C CNN
F 2 "" H 5375 3675 50  0001 C CNN
F 3 "" H 5375 3675 50  0001 C CNN
	1    5375 3675
	1    0    0    -1  
$EndComp
Connection ~ 5375 3700
Wire Wire Line
	5375 3700 5375 4000
$Comp
L Device:R_Network07 RN2
U 1 1 5EFE6AB5
P 10300 950
F 0 "RN2" H 10325 1200 50  0000 C CNN
F 1 "1k" H 10300 1125 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP8" V 10775 950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 10300 950 50  0001 C CNN
	1    10300 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EFE7CE3
P 10000 750
AR Path="/5EB2C18C/5EFE7CE3" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFE7CE3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10000 600 50  0001 C CNN
F 1 "+5V" H 10000 900 50  0000 C CNN
F 2 "" H 10000 750 50  0001 C CNN
F 3 "" H 10000 750 50  0001 C CNN
	1    10000 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 1150 10000 1525
Wire Wire Line
	10200 1150 10200 1525
Wire Wire Line
	10300 1150 10300 1525
Wire Wire Line
	10400 1150 10400 1525
Wire Wire Line
	10500 1150 10500 1525
Wire Wire Line
	10600 1150 10600 1525
Text Label 10000 1525 1    50   ~ 0
PLUP1
Text Label 10200 1525 1    50   ~ 0
PLUP3
Text Label 10400 1525 1    50   ~ 0
PLUP5
Text Label 10500 1525 1    50   ~ 0
PLUP6
Text Label 6100 3800 3    50   ~ 0
PLUP6
Text Label 5875 3800 3    50   ~ 0
PLUP1
Text Label 5950 3800 3    50   ~ 0
PLUP3
Text Label 6025 3800 3    50   ~ 0
PLUP5
Wire Wire Line
	5875 4200 5725 4200
Wire Wire Line
	5875 3800 5875 4200
Wire Wire Line
	4825 4400 4975 4400
Wire Wire Line
	4825 4300 4975 4300
Wire Wire Line
	4825 4600 4975 4600
Wire Wire Line
	4825 4500 4975 4500
Text Label 4825 4300 0    50   ~ 0
A4
Text Label 4825 4400 0    50   ~ 0
A5
Text Label 4825 4500 0    50   ~ 0
A6
Text Label 4825 4600 0    50   ~ 0
A7
Text Label 5525 1900 0    50   ~ 0
~CS2_6522
Wire Wire Line
	5525 1900 6000 1900
Text Label 6525 4900 2    50   ~ 0
~CS2_6522
$Comp
L power:+5V #PWR?
U 1 1 5EFFC093
P 5925 1700
AR Path="/5EB2C18C/5EFFC093" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EFFC093" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5925 1550 50  0001 C CNN
F 1 "+5V" H 5925 1850 50  0000 C CNN
F 2 "" H 5925 1700 50  0001 C CNN
F 3 "" H 5925 1700 50  0001 C CNN
	1    5925 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5925 1700 5925 1800
Wire Wire Line
	5925 1800 6000 1800
Text Label 10600 1525 1    50   ~ 0
~CS2_6522
$Comp
L power:GND #PWR?
U 1 1 5F018BDC
P 6125 6775
AR Path="/5EB2C18C/5F018BDC" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F018BDC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6125 6525 50  0001 C CNN
F 1 "GND" H 6125 6625 50  0000 C CNN
F 2 "" H 6125 6775 50  0001 C CNN
F 3 "" H 6125 6775 50  0001 C CNN
	1    6125 6775
	1    0    0    -1  
$EndComp
Text Label 14875 2175 0    50   ~ 0
A9
Text Label 14875 2075 0    50   ~ 0
A8
Text Label 14875 1275 0    50   ~ 0
A0
Text Label 14875 1975 0    50   ~ 0
A7
Text Label 14875 1875 0    50   ~ 0
A6
Text Label 14875 1775 0    50   ~ 0
A5
$Comp
L power:GND #PWR?
U 1 1 5F02C648
P 14050 2325
AR Path="/5EB2C18C/5F02C648" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F02C648" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14050 2075 50  0001 C CNN
F 1 "GND" H 14050 2175 50  0000 C CNN
F 2 "" H 14050 2325 50  0001 C CNN
F 3 "" H 14050 2325 50  0001 C CNN
	1    14050 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5F02CD84
P 14625 2000
F 0 "R3" H 14684 2046 50  0000 L CNN
F 1 "10k" H 14684 1955 50  0000 L CNN
F 2 "" H 14625 2000 50  0001 C CNN
F 3 "~" H 14625 2000 50  0001 C CNN
	1    14625 2000
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Open JP?
U 1 1 5F02BEE2
P 14275 2275
F 0 "JP?" H 14275 2510 50  0000 C CNN
F 1 "Jumper_2_Open" H 14275 2419 50  0000 C CNN
F 2 "" H 14275 2275 50  0001 C CNN
F 3 "~" H 14275 2275 50  0001 C CNN
	1    14275 2275
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F02E75C
P 14625 1900
AR Path="/5EB2C18C/5F02E75C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F02E75C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14625 1750 50  0001 C CNN
F 1 "+5V" H 14625 2050 50  0000 C CNN
F 2 "" H 14625 1900 50  0001 C CNN
F 3 "" H 14625 1900 50  0001 C CNN
	1    14625 1900
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:27C16-my_ics U11
U 1 1 5F02F1C9
P 15450 2075
F 0 "U11" H 15175 3100 50  0000 C CNN
F 1 "27C16" H 15225 3025 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 15450 2075 50  0001 C CNN
F 3 "" H 15450 2075 50  0001 C CNN
	1    15450 2075
	1    0    0    -1  
$EndComp
NoConn ~ 15050 2425
Wire Wire Line
	15000 2525 15000 2625
Connection ~ 15000 2625
Wire Wire Line
	15000 2625 14550 2625
Text Label 14550 2625 0    50   ~ 0
~CE_EPROM
Wire Wire Line
	11000 4100 11300 4100
Wire Wire Line
	11000 3500 11300 3500
Wire Wire Line
	11000 3600 11300 3600
Connection ~ 11700 3000
Wire Wire Line
	11700 3000 11875 3000
Text Notes 11300 3200 0    50   ~ 0
MUC Vek
Wire Wire Line
	11000 3900 11300 3900
$Comp
L power:+5V #PWR?
U 1 1 5F04D435
P 11700 2975
AR Path="/5EB2C18C/5F04D435" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F04D435" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11700 2825 50  0001 C CNN
F 1 "+5V" H 11700 3125 50  0000 C CNN
F 2 "" H 11700 2975 50  0001 C CNN
F 3 "" H 11700 2975 50  0001 C CNN
	1    11700 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 3700 11300 3700
Wire Wire Line
	11700 2975 11700 3000
Wire Wire Line
	11700 3000 11700 3300
$Comp
L power:GND #PWR?
U 1 1 5F04D447
P 11875 3225
AR Path="/5EB2C18C/5F04D447" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F04D447" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11875 2975 50  0001 C CNN
F 1 "GND" H 11875 3075 50  0000 C CNN
F 2 "" H 11875 3225 50  0001 C CNN
F 3 "" H 11875 3225 50  0001 C CNN
	1    11875 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 3800 11300 3800
$Comp
L power:GND #PWR?
U 1 1 5F04D455
P 11700 4600
AR Path="/5EB2C18C/5F04D455" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F04D455" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11700 4350 50  0001 C CNN
F 1 "GND" H 11700 4450 50  0000 C CNN
F 2 "" H 11700 4600 50  0001 C CNN
F 3 "" H 11700 4600 50  0001 C CNN
	1    11700 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F04D463
P 11875 3125
AR Path="/5EB2C18C/5F04D463" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F04D463" Ref="C?"  Part="1" 
F 0 "C?" H 11967 3171 50  0000 L CNN
F 1 "22n" H 11967 3080 50  0000 L CNN
F 2 "" H 11875 3125 50  0001 C CNN
F 3 "~" H 11875 3125 50  0001 C CNN
	1    11875 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	11875 3000 11875 3025
Wire Wire Line
	11225 4300 11300 4300
Wire Wire Line
	11300 4400 11225 4400
Wire Wire Line
	11225 4400 11225 4300
Wire Wire Line
	11000 4000 11300 4000
$Comp
L es65-rescue:6306-my_ics U18
U 1 1 5F04D484
P 11700 3800
F 0 "U18" H 11400 4325 50  0000 C CNN
F 1 "6306" H 11450 4250 50  0000 C CNN
F 2 "" H 11400 3900 50  0001 C CNN
F 3 "" H 11400 3900 50  0001 C CNN
	1    11700 3800
	1    0    0    -1  
$EndComp
Connection ~ 11225 4400
Wire Wire Line
	11225 4475 11225 4400
$Comp
L power:GND #PWR?
U 1 1 5F04D494
P 11225 4475
AR Path="/5EB2C18C/5F04D494" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F04D494" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11225 4225 50  0001 C CNN
F 1 "GND" H 11225 4325 50  0000 C CNN
F 2 "" H 11225 4475 50  0001 C CNN
F 3 "" H 11225 4475 50  0001 C CNN
	1    11225 4475
	1    0    0    -1  
$EndComp
Text Label 11000 3500 0    50   ~ 0
A1
Text Label 11000 3600 0    50   ~ 0
A2
Text Label 11000 3700 0    50   ~ 0
A3
Text Label 11000 3800 0    50   ~ 0
A4
Text Label 11000 3900 0    50   ~ 0
A5
Text Label 11000 4000 0    50   ~ 0
A6
Text Label 11000 4100 0    50   ~ 0
A7
Wire Wire Line
	12450 3700 12050 3700
Wire Wire Line
	13225 4100 13225 3925
$Comp
L 74xx:74LS148 U10
U 1 1 5F06F172
P 10025 6725
F 0 "U10" H 9675 7375 50  0000 C CNN
F 1 "74LS148" H 9775 7300 50  0000 C CNN
F 2 "" H 10025 6725 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS148" H 10025 6725 50  0001 C CNN
	1    10025 6725
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Network05 RN1
U 1 1 5F077690
P 9450 950
F 0 "RN1" H 9450 1200 50  0000 C CNN
F 1 "470E" H 9475 1125 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP6" V 9825 950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9450 950 50  0001 C CNN
	1    9450 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 1150 9250 1575
Wire Wire Line
	9350 1150 9350 1575
Wire Wire Line
	9450 1150 9450 1575
Wire Wire Line
	9550 1150 9550 1575
Wire Wire Line
	9650 1150 9650 1575
Text Label 9250 1575 1    50   ~ 0
PLUP7
Text Label 9350 1575 1    50   ~ 0
PLUP8
Text Label 9450 1575 1    50   ~ 0
PLUP9
Text Label 9550 1575 1    50   ~ 0
PLUP10
Text Label 9650 1575 1    50   ~ 0
PLUP11
$Comp
L power:+5V #PWR?
U 1 1 5F0789F2
P 9250 750
AR Path="/5EB2C18C/5F0789F2" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F0789F2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9250 600 50  0001 C CNN
F 1 "+5V" H 9250 900 50  0000 C CNN
F 2 "" H 9250 750 50  0001 C CNN
F 3 "" H 9250 750 50  0001 C CNN
	1    9250 750 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 1 1 5F07FC73
P 8875 7850
F 0 "U3" H 8875 8175 50  0000 C CNN
F 1 "74LS32" H 8875 8084 50  0000 C CNN
F 2 "" H 8875 7850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8875 7850 50  0001 C CNN
	1    8875 7850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 2 1 5F083243
P 8625 7475
F 0 "U3" H 8625 7800 50  0000 C CNN
F 1 "74LS32" H 8625 7709 50  0000 C CNN
F 2 "" H 8625 7475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8625 7475 50  0001 C CNN
	2    8625 7475
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 3 1 5F084209
P 8375 7100
F 0 "U3" H 8375 7425 50  0000 C CNN
F 1 "74LS32" H 8375 7334 50  0000 C CNN
F 2 "" H 8375 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8375 7100 50  0001 C CNN
	3    8375 7100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 4 1 5F084A94
P 8125 6725
F 0 "U3" H 8125 7050 50  0000 C CNN
F 1 "74LS32" H 8125 6959 50  0000 C CNN
F 2 "" H 8125 6725 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8125 6725 50  0001 C CNN
	4    8125 6725
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 5 1 5F0854C2
P 9600 2550
F 0 "U3" H 9525 2575 50  0000 L CNN
F 1 "74LS32" H 9450 2500 50  0000 L CNN
F 2 "" H 9600 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 9600 2550 50  0001 C CNN
	5    9600 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8425 6725 9525 6725
Wire Wire Line
	9025 7100 9025 6825
Wire Wire Line
	9025 6825 9525 6825
Wire Wire Line
	8675 7100 9025 7100
Wire Wire Line
	8925 7475 9125 7475
Wire Wire Line
	9125 7475 9125 6925
Wire Wire Line
	9125 6925 9525 6925
Wire Wire Line
	9250 7850 9250 7025
Wire Wire Line
	9250 7025 9525 7025
$Comp
L power:GND #PWR?
U 1 1 5F0B5A70
P 9475 7175
AR Path="/5EB2C18C/5F0B5A70" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F0B5A70" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9475 6925 50  0001 C CNN
F 1 "GND" H 9475 7025 50  0000 C CNN
F 2 "" H 9475 7175 50  0001 C CNN
F 3 "" H 9475 7175 50  0001 C CNN
	1    9475 7175
	1    0    0    -1  
$EndComp
Wire Wire Line
	9475 7175 9475 7125
Wire Wire Line
	9475 7125 9525 7125
$Comp
L power:GND #PWR?
U 1 1 5F0B633F
P 10025 7425
AR Path="/5EB2C18C/5F0B633F" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F0B633F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10025 7175 50  0001 C CNN
F 1 "GND" H 10025 7275 50  0000 C CNN
F 2 "" H 10025 7425 50  0001 C CNN
F 3 "" H 10025 7425 50  0001 C CNN
	1    10025 7425
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 1300 7550 1300
Wire Wire Line
	7200 1400 7550 1400
Wire Wire Line
	7200 1500 7550 1500
Wire Wire Line
	7200 1600 7550 1600
Wire Wire Line
	7200 1700 7550 1700
Wire Wire Line
	7200 1800 7550 1800
Wire Wire Line
	7200 1900 7550 1900
Wire Wire Line
	7200 2000 7550 2000
Wire Wire Line
	7200 2500 7550 2500
Wire Wire Line
	7200 2600 7550 2600
Wire Wire Line
	7200 2700 7550 2700
Wire Wire Line
	7200 2800 7550 2800
Wire Wire Line
	7200 2900 7550 2900
Wire Wire Line
	7200 3000 7550 3000
Wire Wire Line
	7200 3100 7550 3100
Text Label 7550 1300 2    50   ~ 0
PA0
Text Label 7550 1400 2    50   ~ 0
PA1
Text Label 7550 1500 2    50   ~ 0
PA2
Text Label 7550 1600 2    50   ~ 0
PA3
Text Label 7550 1700 2    50   ~ 0
PA4
Text Label 7550 1800 2    50   ~ 0
PA5
Text Label 7550 1900 2    50   ~ 0
PA6
Text Label 7550 2000 2    50   ~ 0
PA7
Text Label 7550 2500 2    50   ~ 0
PB0
Text Label 7550 2700 2    50   ~ 0
PB2
Text Label 7550 2800 2    50   ~ 0
PB3
Text Label 7550 2900 2    50   ~ 0
PB4
Text Label 7550 3000 2    50   ~ 0
PB5
Text Label 7550 3100 2    50   ~ 0
PB6
Text Label 7550 3200 2    50   ~ 0
PB7
Text Label 7550 2200 2    50   ~ 0
CA1
Text Label 7575 7750 0    50   ~ 0
PA7
Wire Wire Line
	9175 7850 9250 7850
Text Label 7575 7950 0    50   ~ 0
~IRQ7
Text Label 7575 7575 0    50   ~ 0
~IRQ6
Text Label 7575 7375 0    50   ~ 0
PA6
Text Label 7575 7000 0    50   ~ 0
PA5
Text Label 7575 6625 0    50   ~ 0
PA4
Text Label 7575 7200 0    50   ~ 0
~IRQ5
Text Label 7575 6825 0    50   ~ 0
~IRQ4
Wire Wire Line
	7575 5375 8300 5375
Wire Wire Line
	7575 5750 8000 5750
$Comp
L 74xx:74LS32 U9
U 4 1 5F0EA334
P 8600 5275
F 0 "U9" H 8600 5600 50  0000 C CNN
F 1 "74LS32" H 8600 5509 50  0000 C CNN
F 2 "" H 8600 5275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8600 5275 50  0001 C CNN
	4    8600 5275
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U9
U 3 1 5F0EA344
P 8300 5650
F 0 "U9" H 8300 5975 50  0000 C CNN
F 1 "74LS32" H 8300 5884 50  0000 C CNN
F 2 "" H 8300 5650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8300 5650 50  0001 C CNN
	3    8300 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 6375 8450 6375
$Comp
L 74xx:74LS32 U9
U 2 1 5F0EA354
P 8550 6025
F 0 "U9" H 8550 6350 50  0000 C CNN
F 1 "74LS32" H 8550 6259 50  0000 C CNN
F 2 "" H 8550 6025 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8550 6025 50  0001 C CNN
	2    8550 6025
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 5550 7575 5550
Wire Wire Line
	9050 6475 9175 6475
Wire Wire Line
	8300 5175 7600 5175
$Comp
L 74xx:74LS32 U9
U 1 1 5F0EA37C
P 8750 6475
F 0 "U9" H 8750 6800 50  0000 C CNN
F 1 "74LS32" H 8750 6709 50  0000 C CNN
F 2 "" H 8750 6475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8750 6475 50  0001 C CNN
	1    8750 6475
	1    0    0    -1  
$EndComp
Wire Wire Line
	9175 6475 9175 6625
Wire Wire Line
	9175 6625 9525 6625
Wire Wire Line
	9525 6525 9225 6525
Wire Wire Line
	9225 6525 9225 6025
Wire Wire Line
	8850 6025 9225 6025
Wire Wire Line
	9525 6425 9325 6425
Wire Wire Line
	9325 6425 9325 5650
Wire Wire Line
	9325 5650 8600 5650
Wire Wire Line
	8900 5275 9425 5275
Wire Wire Line
	9525 6325 9425 6325
Wire Wire Line
	9425 5275 9425 6325
Wire Wire Line
	7575 7000 8075 7000
Wire Wire Line
	7575 7200 8075 7200
Wire Wire Line
	7575 7575 8325 7575
Wire Wire Line
	7575 7375 8325 7375
Wire Wire Line
	7575 7750 8575 7750
Wire Wire Line
	7575 7950 8575 7950
Wire Wire Line
	8275 6575 8275 6350
Wire Wire Line
	8275 6350 7575 6350
Wire Wire Line
	8275 6575 8450 6575
Wire Wire Line
	8350 6375 8350 6225
Wire Wire Line
	8350 6225 7575 6225
Wire Wire Line
	7575 6125 8250 6125
Wire Wire Line
	7575 5925 8250 5925
Text Label 7575 5550 0    50   ~ 0
PA1
Text Label 7575 6225 0    50   ~ 0
PA3
Text Label 7575 5925 0    50   ~ 0
PA2
Text Label 7600 5175 0    50   ~ 0
PA0
Text Label 7575 5750 0    50   ~ 0
~IRQ1
Text Label 7575 6125 0    50   ~ 0
~IRQ2
Text Label 7575 5375 0    50   ~ 0
~IRQ0
Text Label 7575 6350 0    50   ~ 0
~IRQ3
Wire Wire Line
	7575 6625 7825 6625
Wire Wire Line
	7575 6825 7825 6825
$Comp
L 74xx:74LS06 U27
U 1 1 5F12308B
P 14775 4650
F 0 "U27" H 14775 4967 50  0000 C CNN
F 1 "74LS06" H 14775 4876 50  0000 C CNN
F 2 "" H 14775 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 14775 4650 50  0001 C CNN
	1    14775 4650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS06 U27
U 5 1 5F125FC3
P 10525 9950
F 0 "U27" H 10525 10267 50  0000 C CNN
F 1 "74LS06" H 10525 10176 50  0000 C CNN
F 2 "" H 10525 9950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 10525 9950 50  0001 C CNN
	5    10525 9950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS06 U27
U 6 1 5F1267B9
P 10925 6925
F 0 "U27" H 10925 7242 50  0000 C CNN
F 1 "74LS06" H 10925 7151 50  0000 C CNN
F 2 "" H 10925 6925 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 10925 6925 50  0001 C CNN
	6    10925 6925
	1    0    0    -1  
$EndComp
Text Label 11525 6925 2    50   ~ 0
~IRQ
Wire Wire Line
	11225 6925 11525 6925
NoConn ~ 10525 6825
$Comp
L 74xx:74LS00 U26
U 2 1 5F138CFD
P 11175 5175
F 0 "U26" H 11175 5500 50  0000 C CNN
F 1 "74LS00" H 11175 5409 50  0000 C CNN
F 2 "" H 11175 5175 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 11175 5175 50  0001 C CNN
	2    11175 5175
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U26
U 5 1 5F13AEEE
P 9150 2550
F 0 "U26" H 9050 2575 50  0000 L CNN
F 1 "74LS00" H 9025 2500 50  0000 L CNN
F 2 "" H 9150 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 9150 2550 50  0001 C CNN
	5    9150 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F15EDDF
P 14925 6350
AR Path="/5EB2C18C/5F15EDDF" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F15EDDF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14925 6200 50  0001 C CNN
F 1 "+5V" H 14925 6500 50  0000 C CNN
F 2 "" H 14925 6350 50  0001 C CNN
F 3 "" H 14925 6350 50  0001 C CNN
	1    14925 6350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U25
U 2 1 5F17F8A6
P 4150 3850
F 0 "U25" H 4150 4175 50  0000 C CNN
F 1 "74LS08" H 4150 4084 50  0000 C CNN
F 2 "" H 4150 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 4150 3850 50  0001 C CNN
	2    4150 3850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U25
U 4 1 5F1822FB
P 11450 10800
F 0 "U25" H 11450 11125 50  0000 C CNN
F 1 "74LS08" H 11450 11034 50  0000 C CNN
F 2 "" H 11450 10800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 11450 10800 50  0001 C CNN
	4    11450 10800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U25
U 5 1 5F18366C
P 8700 2550
F 0 "U25" H 8600 2600 50  0000 L CNN
F 1 "74LS08" H 8575 2500 50  0000 L CNN
F 2 "" H 8700 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 8700 2550 50  0001 C CNN
	5    8700 2550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS30 U19
U 2 1 5F19D1D0
P 10050 2550
F 0 "U19" H 9950 2600 50  0000 L CNN
F 1 "74LS30" H 9925 2500 50  0000 L CNN
F 2 "" H 10050 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS30" H 10050 2550 50  0001 C CNN
	2    10050 2550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS107 U20
U 1 1 5F1A3E22
P 15425 4825
F 0 "U20" H 15525 5150 50  0000 C CNN
F 1 "74LS107" H 15475 5075 50  0000 C CNN
F 2 "" H 15425 4825 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS107" H 15425 4825 50  0001 C CNN
	1    15425 4825
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS107 U20
U 2 1 5F1A4C17
P 12500 5975
F 0 "U20" H 12500 6342 50  0000 C CNN
F 1 "74LS107" H 12500 6251 50  0000 C CNN
F 2 "" H 12500 5975 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS107" H 12500 5975 50  0001 C CNN
	2    12500 5975
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS107 U20
U 3 1 5F1A543E
P 10050 4200
F 0 "U20" H 9950 4250 50  0000 L CNN
F 1 "74LS107" H 9875 4150 50  0000 L CNN
F 2 "" H 10050 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS107" H 10050 4200 50  0001 C CNN
	3    10050 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 6075 12800 6075
$Comp
L power:GND #PWR?
U 1 1 5F1A90B0
P 15075 4975
AR Path="/5EB2C18C/5F1A90B0" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F1A90B0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15075 4725 50  0001 C CNN
F 1 "GND" H 15075 4825 50  0000 C CNN
F 2 "" H 15075 4975 50  0001 C CNN
F 3 "" H 15075 4975 50  0001 C CNN
	1    15075 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	15075 4975 15075 4925
Wire Wire Line
	15075 4925 15125 4925
$Comp
L power:GND #PWR?
U 1 1 5F1A988D
P 12150 6125
AR Path="/5EB2C18C/5F1A988D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F1A988D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12150 5875 50  0001 C CNN
F 1 "GND" H 12150 5975 50  0000 C CNN
F 2 "" H 12150 6125 50  0001 C CNN
F 3 "" H 12150 6125 50  0001 C CNN
	1    12150 6125
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 6125 12150 6075
Wire Wire Line
	12150 6075 12200 6075
Wire Wire Line
	15125 4825 14825 4825
Text Label 14825 4825 0    50   ~ 0
PH2bb
Wire Wire Line
	12200 5975 11900 5975
Text Label 11900 5975 0    50   ~ 0
PH2bb
$Comp
L 74xx:74LS00 U26
U 1 1 5F134FDE
P 12025 5075
F 0 "U26" H 12025 5400 50  0000 C CNN
F 1 "74LS00" H 12025 5309 50  0000 C CNN
F 2 "" H 12025 5075 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 12025 5075 50  0001 C CNN
	1    12025 5075
	1    0    0    -1  
$EndComp
Wire Wire Line
	11350 4975 11725 4975
Text Label 11350 4975 0    50   ~ 0
PH2bb
Wire Wire Line
	15075 4650 15100 4650
Wire Wire Line
	15100 4650 15100 4725
Wire Wire Line
	15100 4725 15125 4725
$Comp
L power:+5V #PWR?
U 1 1 5F1C57E5
P 15100 4300
AR Path="/5EB2C18C/5F1C57E5" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F1C57E5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15100 4150 50  0001 C CNN
F 1 "+5V" H 15100 4450 50  0000 C CNN
F 2 "" H 15100 4300 50  0001 C CNN
F 3 "" H 15100 4300 50  0001 C CNN
	1    15100 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5F1C57F3
P 15100 4400
F 0 "R1" H 15041 4446 50  0000 R CNN
F 1 "820E" H 15041 4355 50  0000 R CNN
F 2 "" H 15100 4400 50  0001 C CNN
F 3 "~" H 15100 4400 50  0001 C CNN
	1    15100 4400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	15100 4500 15100 4650
Connection ~ 15100 4650
Text Label 14450 4200 3    50   ~ 0
PLUP2
Wire Wire Line
	14450 4200 14450 4650
Wire Wire Line
	14450 4650 14475 4650
Wire Wire Line
	14450 4650 14350 4650
Connection ~ 14450 4650
Text Notes 14225 4650 0    50   ~ 0
n.p.
Text Label 15975 4925 2    50   ~ 0
DL5
Wire Wire Line
	15725 4925 15975 4925
$Comp
L 74xx:74LS02 U28
U 1 1 5F1E5EB3
P 8400 9675
F 0 "U28" H 8400 10000 50  0000 C CNN
F 1 "74LS02" H 8400 9909 50  0000 C CNN
F 2 "" H 8400 9675 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls02" H 8400 9675 50  0001 C CNN
	1    8400 9675
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS02 U28
U 2 1 5F1E8742
P 7725 9950
F 0 "U28" H 7725 10275 50  0000 C CNN
F 1 "74LS02" H 7725 10184 50  0000 C CNN
F 2 "" H 7725 9950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls02" H 7725 9950 50  0001 C CNN
	2    7725 9950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS02 U28
U 3 1 5F1E92F2
P 3000 10725
F 0 "U28" H 3000 11050 50  0000 C CNN
F 1 "74LS02" H 3000 10959 50  0000 C CNN
F 2 "" H 3000 10725 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls02" H 3000 10725 50  0001 C CNN
	3    3000 10725
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS02 U28
U 4 1 5F1ED285
P 11725 5875
F 0 "U28" H 11725 6200 50  0000 C CNN
F 1 "74LS02" H 11725 6109 50  0000 C CNN
F 2 "" H 11725 5875 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls02" H 11725 5875 50  0001 C CNN
	4    11725 5875
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS02 U28
U 5 1 5F1EF15C
P 10500 2550
F 0 "U28" H 10400 2575 50  0000 L CNN
F 1 "74LS02" H 10375 2500 50  0000 L CNN
F 2 "" H 10500 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls02" H 10500 2550 50  0001 C CNN
	5    10500 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F2028C2
P 8300 700
AR Path="/5EB2C18C/5F2028C2" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F2028C2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8300 550 50  0001 C CNN
F 1 "+5V" H 8300 850 50  0000 C CNN
F 2 "" H 8300 700 50  0001 C CNN
F 3 "" H 8300 700 50  0001 C CNN
	1    8300 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 1100 8500 1475
Wire Wire Line
	8800 1100 8800 1475
Wire Wire Line
	8700 1100 8700 1475
Wire Wire Line
	8600 1100 8600 1475
$Comp
L Device:R_Network07 RN3
U 1 1 5F2028DC
P 8600 900
F 0 "RN3" H 8625 1150 50  0000 C CNN
F 1 "1k" H 8600 1075 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP8" V 9075 900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 8600 900 50  0001 C CNN
	1    8600 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 1100 8900 1475
Wire Wire Line
	8400 1100 8400 1475
Wire Wire Line
	8300 1100 8300 1475
Text Label 8300 1475 1    50   ~ 0
PLUP12
Text Label 8400 1475 1    50   ~ 0
PLUP13
Text Label 8500 1475 1    50   ~ 0
PLUP14
Text Label 8600 1475 1    50   ~ 0
PLUP15
Text Label 8700 1475 1    50   ~ 0
PLUP16
Text Label 8800 1475 1    50   ~ 0
PLUP17
Text Label 8900 1475 1    50   ~ 0
PLUP18
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 5F204D9F
P 6600 8300
F 0 "J?" H 6518 8717 50  0000 C CNN
F 1 "Conn_01x06" H 6518 8626 50  0000 C CNN
F 2 "" H 6600 8300 50  0001 C CNN
F 3 "~" H 6600 8300 50  0001 C CNN
	1    6600 8300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6800 8200 6925 8200
Wire Wire Line
	6925 8200 6925 7950
$Comp
L power:+5V #PWR?
U 1 1 5F206026
P 6925 7950
AR Path="/5EB2C18C/5F206026" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F206026" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6925 7800 50  0001 C CNN
F 1 "+5V" H 6925 8100 50  0000 C CNN
F 2 "" H 6925 7950 50  0001 C CNN
F 3 "" H 6925 7950 50  0001 C CNN
	1    6925 7950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 9575 7225 9575
Wire Wire Line
	8875 9950 8050 9950
Wire Wire Line
	8050 9775 8100 9775
$Comp
L Device:R_Small R2
U 1 1 5F226291
P 8050 10300
F 0 "R2" H 7991 10346 50  0000 R CNN
F 1 "330E" H 7991 10255 50  0000 R CNN
F 2 "" H 8050 10300 50  0001 C CNN
F 3 "~" H 8050 10300 50  0001 C CNN
	1    8050 10300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8050 10200 8050 9950
Connection ~ 8050 9950
$Comp
L Device:C_Small C?
U 1 1 5F227AB6
P 8050 10625
F 0 "C?" H 8142 10671 50  0000 L CNN
F 1 "100n" H 8142 10580 50  0000 L CNN
F 2 "" H 8050 10625 50  0001 C CNN
F 3 "~" H 8050 10625 50  0001 C CNN
	1    8050 10625
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 10525 8050 10475
$Comp
L power:GND #PWR?
U 1 1 5F228563
P 8050 10725
AR Path="/5EB2C18C/5F228563" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F228563" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8050 10475 50  0001 C CNN
F 1 "GND" H 8050 10575 50  0000 C CNN
F 2 "" H 8050 10725 50  0001 C CNN
F 3 "" H 8050 10725 50  0001 C CNN
	1    8050 10725
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U22
U 1 1 5F239A62
P 9925 9950
F 0 "U22" H 9925 10275 50  0000 C CNN
F 1 "74LS132" H 9925 10184 50  0000 C CNN
F 2 "" H 9925 9950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 9925 9950 50  0001 C CNN
	1    9925 9950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U22
U 2 1 5F23DCD5
P 6750 11025
F 0 "U22" H 6750 11350 50  0000 C CNN
F 1 "74LS132" H 6750 11259 50  0000 C CNN
F 2 "" H 6750 11025 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 6750 11025 50  0001 C CNN
	2    6750 11025
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U22
U 4 1 5F2405ED
P 8725 10575
F 0 "U22" H 8725 10900 50  0000 C CNN
F 1 "74LS132" H 8725 10809 50  0000 C CNN
F 2 "" H 8725 10575 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 8725 10575 50  0001 C CNN
	4    8725 10575
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U22
U 5 1 5F24180A
P 9600 4200
F 0 "U22" H 9525 4250 50  0000 L CNN
F 1 "74LS132" H 9450 4150 50  0000 L CNN
F 2 "" H 9600 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 9600 4200 50  0001 C CNN
	5    9600 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8425 10475 8050 10475
Connection ~ 8050 10475
Wire Wire Line
	8050 10475 8050 10400
$Comp
L power:+5V #PWR?
U 1 1 5F254C7A
P 8375 10425
AR Path="/5EB2C18C/5F254C7A" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F254C7A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8375 10275 50  0001 C CNN
F 1 "+5V" H 8375 10575 50  0000 C CNN
F 2 "" H 8375 10425 50  0001 C CNN
F 3 "" H 8375 10425 50  0001 C CNN
	1    8375 10425
	1    0    0    -1  
$EndComp
Wire Wire Line
	8375 10425 8375 10675
Wire Wire Line
	8375 10675 8425 10675
Wire Wire Line
	9025 10575 9075 10575
Wire Wire Line
	8850 10150 8875 10150
$Comp
L power:GND #PWR?
U 1 1 5F270E41
P 1625 5575
AR Path="/5E84A03F/5F270E41" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5F270E41" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5F270E41" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5F270E41" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5F270E41" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5F270E41" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F270E41" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1625 5325 50  0001 C CNN
F 1 "GND" H 1625 5425 50  0000 C CNN
F 2 "" H 1625 5575 50  0001 C CNN
F 3 "" H 1625 5575 50  0001 C CNN
	1    1625 5575
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F27134B
P 1625 5475
F 0 "C?" H 1717 5521 50  0000 L CNN
F 1 "3n3" H 1717 5430 50  0000 L CNN
F 2 "" H 1625 5475 50  0001 C CNN
F 3 "~" H 1625 5475 50  0001 C CNN
	1    1625 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	1625 5375 1625 5325
Connection ~ 1625 5325
Wire Wire Line
	1625 5325 1925 5325
Wire Wire Line
	2275 6000 2275 6500
Wire Wire Line
	2275 6500 3400 6500
Wire Wire Line
	3400 6500 3400 7000
Wire Wire Line
	3400 7000 3350 7000
Connection ~ 5875 4200
Wire Wire Line
	5875 4200 6525 4200
Text Label 6525 4200 2    50   ~ 0
~CE_DATA
Wire Wire Line
	14325 3425 14325 3825
Wire Wire Line
	14325 3425 14300 3425
Connection ~ 14325 3825
Wire Wire Line
	14325 3825 14300 3825
Wire Wire Line
	14375 3525 14375 3925
Wire Wire Line
	14375 3525 14300 3525
Connection ~ 14375 3925
Wire Wire Line
	14375 3925 14300 3925
Wire Wire Line
	14425 3625 14425 4025
Wire Wire Line
	14425 3625 14300 3625
Connection ~ 14425 4025
Wire Wire Line
	14425 4025 14300 4025
Wire Wire Line
	14475 3725 14475 4125
Wire Wire Line
	14475 3725 14300 3725
Connection ~ 14475 4125
Wire Wire Line
	14475 4125 14300 4125
Wire Wire Line
	14625 2100 14625 2275
Connection ~ 14625 2275
Wire Wire Line
	14625 2275 14475 2275
Wire Wire Line
	14075 2275 14050 2275
Wire Wire Line
	14050 2275 14050 2325
$Comp
L 74xx:74LS32 U9
U 5 1 5F2BBB5D
P 9150 4200
F 0 "U9" H 9050 4250 50  0000 L CNN
F 1 "74LS32" H 9000 4150 50  0000 L CNN
F 2 "" H 9150 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 9150 4200 50  0001 C CNN
	5    9150 4200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U21
U 5 1 5F2BFE87
P 8700 4200
F 0 "U21" H 8600 4275 50  0000 L CNN
F 1 "74LS32" H 8550 4175 50  0000 L CNN
F 2 "" H 8700 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8700 4200 50  0001 C CNN
	5    8700 4200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U21
U 4 1 5F2C3C87
P 7025 7025
F 0 "U21" H 7025 7350 50  0000 C CNN
F 1 "74LS32" H 7025 7259 50  0000 C CNN
F 2 "" H 7025 7025 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7025 7025 50  0001 C CNN
	4    7025 7025
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U21
U 3 1 5F2C3C96
P 7050 6475
F 0 "U21" H 7050 6800 50  0000 C CNN
F 1 "74LS32" H 7050 6709 50  0000 C CNN
F 2 "" H 7050 6475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7050 6475 50  0001 C CNN
	3    7050 6475
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U21
U 2 1 5F2C3CA5
P 7550 10925
F 0 "U21" H 7550 11250 50  0000 C CNN
F 1 "74LS32" H 7550 11159 50  0000 C CNN
F 2 "" H 7550 10925 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7550 10925 50  0001 C CNN
	2    7550 10925
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U21
U 1 1 5F2C3CB6
P 3475 7750
F 0 "U21" H 3475 8075 50  0000 C CNN
F 1 "74LS32" H 3475 7984 50  0000 C CNN
F 2 "" H 3475 7750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3475 7750 50  0001 C CNN
	1    3475 7750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5F2FA838
P 7325 8400
F 0 "R7" V 7129 8400 50  0000 C CNN
F 1 "82E" V 7220 8400 50  0000 C CNN
F 2 "" H 7325 8400 50  0001 C CNN
F 3 "~" H 7325 8400 50  0001 C CNN
	1    7325 8400
	0    1    1    0   
$EndComp
Wire Wire Line
	7225 8400 6800 8400
Wire Wire Line
	6800 8500 6925 8500
Wire Wire Line
	6925 8500 6925 8550
$Comp
L power:GND #PWR?
U 1 1 5F301D64
P 6925 8550
AR Path="/5EB2C18C/5F301D64" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F301D64" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6925 8300 50  0001 C CNN
F 1 "GND" H 6925 8400 50  0000 C CNN
F 2 "" H 6925 8550 50  0001 C CNN
F 3 "" H 6925 8550 50  0001 C CNN
	1    6925 8550
	1    0    0    -1  
$EndComp
Text Notes 7775 1975 1    50   ~ 0
Interrupt Select\n-- all Output --
Text Notes 6975 4200 2    50   ~ 0
F8C8-F8FF
Text Notes 12475 3900 0    50   ~ 0
FF00-FFF9\nFC00-FEFF
Text Notes 12475 3500 0    50   ~ 0
FFFA-FFFB
Text Notes 12475 3600 0    50   ~ 0
FFFC-FFFD
Text Notes 12475 3700 0    50   ~ 0
FFFE-FFFF
Text Label 4600 3850 2    50   ~ 0
PB0
Wire Wire Line
	4450 3850 4600 3850
Text Label 7550 2600 2    50   ~ 0
~IRQ1
Wire Wire Line
	6750 6375 6575 6375
Wire Wire Line
	6575 6925 6725 6925
Text Label 6575 6375 0    50   ~ 0
PB2
Wire Wire Line
	6575 6925 6575 6750
Wire Wire Line
	6575 6750 7400 6750
Wire Wire Line
	7400 6750 7400 6475
Wire Wire Line
	7400 6475 7350 6475
Text Label 8950 9675 2    50   ~ 0
PB3
Text Notes 7575 2900 0    50   ~ 0
PB4-not popul.
Text Notes 7575 3000 0    50   ~ 0
PB5-not popul.
Text Label 2225 3150 2    50   ~ 0
PB6
Text Label 2225 2950 2    50   ~ 0
R~W~b
Text Label 625  5750 0    50   ~ 0
R~W~b
Text Label 2025 5900 0    50   ~ 0
R~W~bb
Text Label 5700 2600 0    50   ~ 0
R~W~bb
$Comp
L Jumper:Jumper_2_Bridged JP?
U 1 1 5F3EDB0B
P 7975 2200
F 0 "JP?" H 7975 2395 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 7975 2304 50  0000 C CNN
F 2 "" H 7975 2200 50  0001 C CNN
F 3 "~" H 7975 2200 50  0001 C CNN
	1    7975 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2200 7775 2200
Wire Wire Line
	8175 2200 8300 2200
Wire Wire Line
	8300 2200 8300 3200
Wire Wire Line
	7200 3200 8300 3200
Wire Wire Line
	2825 1750 3125 1750
Wire Wire Line
	2825 1850 3125 1850
Text Label 3125 1850 2    50   ~ 0
CA1
Text Label 3125 1750 2    50   ~ 0
PB7
NoConn ~ 7200 2300
NoConn ~ 7200 3400
NoConn ~ 7200 3500
Wire Wire Line
	12975 3425 13300 3425
Text Label 12975 3425 0    50   ~ 0
A4
Text Label 12975 3525 0    50   ~ 0
A3
Text Label 12975 3725 0    50   ~ 0
A1
Text Label 12975 3625 0    50   ~ 0
A2
Text Label 12975 4025 0    50   ~ 0
PB0
Text Notes 12700 4125 0    50   ~ 0
P6/U7
$Comp
L 74xx:74LS377 U4
U 1 1 5F43C0A6
P 3125 9250
F 0 "U4" H 2725 10025 50  0000 C CNN
F 1 "74LS377" H 2850 9925 50  0000 C CNN
F 2 "" H 3125 9250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS377" H 3125 9250 50  0001 C CNN
	1    3125 9250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F43F9BE
P 3125 10050
AR Path="/5EB2C18C/5F43F9BE" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F43F9BE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3125 9800 50  0001 C CNN
F 1 "GND" H 3125 9900 50  0000 C CNN
F 2 "" H 3125 10050 50  0001 C CNN
F 3 "" H 3125 10050 50  0001 C CNN
	1    3125 10050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F440027
P 3125 8200
AR Path="/5EB2C18C/5F440027" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F440027" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3125 8050 50  0001 C CNN
F 1 "+5V" H 3125 8350 50  0000 C CNN
F 2 "" H 3125 8200 50  0001 C CNN
F 3 "" H 3125 8200 50  0001 C CNN
	1    3125 8200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F440034
P 3300 8450
AR Path="/5EB2C18C/5F440034" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F440034" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3300 8200 50  0001 C CNN
F 1 "GND" H 3300 8300 50  0000 C CNN
F 2 "" H 3300 8450 50  0001 C CNN
F 3 "" H 3300 8450 50  0001 C CNN
	1    3300 8450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F440042
P 3300 8350
AR Path="/5EB2C18C/5F440042" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F440042" Ref="C?"  Part="1" 
F 0 "C?" H 3392 8396 50  0000 L CNN
F 1 "22n" H 3392 8305 50  0000 L CNN
F 2 "" H 3300 8350 50  0001 C CNN
F 3 "~" H 3300 8350 50  0001 C CNN
	1    3300 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3125 8225 3125 8450
Connection ~ 3125 8225
Wire Wire Line
	3125 8200 3125 8225
Wire Wire Line
	3125 8225 3300 8225
Wire Wire Line
	3300 8225 3300 8250
$Comp
L power:GND #PWR?
U 1 1 5F4419B3
P 4750 5850
AR Path="/5EB2C18C/5F4419B3" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F4419B3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4750 5600 50  0001 C CNN
F 1 "GND" H 4750 5700 50  0000 C CNN
F 2 "" H 4750 5850 50  0001 C CNN
F 3 "" H 4750 5850 50  0001 C CNN
	1    4750 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F4419C0
P 4575 5600
AR Path="/5EB2C18C/5F4419C0" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F4419C0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4575 5450 50  0001 C CNN
F 1 "+5V" H 4575 5750 50  0000 C CNN
F 2 "" H 4575 5600 50  0001 C CNN
F 3 "" H 4575 5600 50  0001 C CNN
	1    4575 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F4419CE
P 4750 5750
AR Path="/5EB2C18C/5F4419CE" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F4419CE" Ref="C?"  Part="1" 
F 0 "C?" H 4842 5796 50  0000 L CNN
F 1 "22n" H 4842 5705 50  0000 L CNN
F 2 "" H 4750 5750 50  0001 C CNN
F 3 "~" H 4750 5750 50  0001 C CNN
	1    4750 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 5625 4750 5650
$Comp
L 74xx:74LS377 U5
U 1 1 5F4419EF
P 4575 6650
F 0 "U5" H 4175 7425 50  0000 C CNN
F 1 "74LS377" H 4300 7325 50  0000 C CNN
F 2 "" H 4575 6650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS377" H 4575 6650 50  0001 C CNN
	1    4575 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F4419FC
P 4575 7450
AR Path="/5EB2C18C/5F4419FC" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F4419FC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4575 7200 50  0001 C CNN
F 1 "GND" H 4575 7300 50  0000 C CNN
F 2 "" H 4575 7450 50  0001 C CNN
F 3 "" H 4575 7450 50  0001 C CNN
	1    4575 7450
	1    0    0    -1  
$EndComp
Connection ~ 4575 5625
Wire Wire Line
	4575 5625 4575 5850
Wire Wire Line
	4575 5600 4575 5625
Wire Wire Line
	4575 5625 4750 5625
Wire Wire Line
	2625 9450 2325 9450
Wire Wire Line
	2625 9350 2325 9350
Wire Wire Line
	2625 9050 2325 9050
Wire Wire Line
	2625 8750 2325 8750
Wire Wire Line
	2625 9250 2325 9250
Wire Wire Line
	2625 8950 2325 8950
Wire Wire Line
	2625 8850 2325 8850
Wire Wire Line
	2625 9150 2325 9150
Wire Wire Line
	4075 6150 3775 6150
Wire Wire Line
	4075 6550 3775 6550
Wire Wire Line
	4075 6750 3775 6750
Wire Wire Line
	4075 6850 3775 6850
Wire Wire Line
	4075 6250 3775 6250
Wire Wire Line
	4075 6450 3775 6450
Wire Wire Line
	4075 6350 3775 6350
Wire Wire Line
	4075 6650 3775 6650
Wire Wire Line
	2625 9650 2225 9650
Wire Wire Line
	2625 9750 2325 9750
Wire Wire Line
	4075 7050 3675 7050
Wire Wire Line
	4075 7150 3775 7150
Text Label 2925 7850 0    50   ~ 0
R~W~bb
Wire Wire Line
	3175 7850 2925 7850
Wire Wire Line
	3150 7225 3150 7650
Text Label 3150 7225 3    50   ~ 0
PLUP15
Wire Wire Line
	3150 7650 3175 7650
$Comp
L 74xx:74LS06 U27
U 4 1 5F125774
P 2825 7650
F 0 "U27" H 2825 7967 50  0000 C CNN
F 1 "74LS06" H 2825 7876 50  0000 C CNN
F 2 "" H 2825 7650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 2825 7650 50  0001 C CNN
	4    2825 7650
	1    0    0    -1  
$EndComp
Connection ~ 3150 7650
Wire Wire Line
	3125 7650 3150 7650
Text Label 2275 7650 0    50   ~ 0
PH2bb
Wire Wire Line
	2275 7650 2525 7650
Wire Wire Line
	3775 7750 4225 7750
Text Label 4225 7750 2    50   ~ 0
WritePulse
Text Label 2225 9650 0    50   ~ 0
WritePulse
Text Label 3675 7050 0    50   ~ 0
WritePulse
Wire Wire Line
	2750 6900 2350 6900
Text Label 2350 6900 0    50   ~ 0
~CE_EPROM
Text Label 2425 7100 0    50   ~ 0
~CE_DATA
Wire Wire Line
	2750 7100 2425 7100
$Comp
L 74xx:74LS08 U25
U 3 1 5F1814E9
P 3050 7000
F 0 "U25" H 3050 7325 50  0000 C CNN
F 1 "74LS08" H 3050 7234 50  0000 C CNN
F 2 "" H 3050 7000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 3050 7000 50  0001 C CNN
	3    3050 7000
	1    0    0    -1  
$EndComp
Text Label 6525 4600 2    50   ~ 0
~CS_U7
Wire Wire Line
	6100 3800 6100 4800
Wire Wire Line
	6025 3800 6025 4700
Wire Wire Line
	5950 3800 5950 4500
Wire Wire Line
	5725 4500 5950 4500
Wire Wire Line
	6025 4700 5725 4700
Wire Wire Line
	5725 4800 6100 4800
Wire Wire Line
	5725 4900 6525 4900
Text Label 10300 1525 1    50   ~ 0
~CS_U7
Wire Wire Line
	5725 4600 6525 4600
NoConn ~ 10100 1150
Text Label 4825 4200 0    50   ~ 0
A3
Wire Wire Line
	4825 4200 4975 4200
Text Notes 6975 4500 2    50   ~ 0
F8C8-F8CF
NoConn ~ 5725 4300
NoConn ~ 5725 4400
Wire Wire Line
	5950 4500 6525 4500
Connection ~ 5950 4500
Text Notes 6975 4600 2    50   ~ 0
F8D0-F8DF
Text Notes 6975 4700 2    50   ~ 0
F8E0-F8E7
Wire Wire Line
	6025 4700 6525 4700
Connection ~ 6025 4700
Wire Wire Line
	6100 4800 6525 4800
Connection ~ 6100 4800
Text Notes 6975 4800 2    50   ~ 0
F8E8-F8EF
Text Notes 6975 4900 2    50   ~ 0
F8F0-F8FF
Text Label 6525 4800 2    50   ~ 0
~CS_U5
Text Label 3775 7150 0    50   ~ 0
~CS_U5
Text Label 2325 9750 0    50   ~ 0
~CS_U4
Text Label 6525 4700 2    50   ~ 0
~CS_U4
Text Notes 3000 9975 2    50   ~ 0
F8E0-F8E7
Text Notes 4400 7375 2    50   ~ 0
F8E8-F8EF
Text Notes 3375 6325 2    50   ~ 0
F8C8-F8FF
Text Notes 2950 6250 0    50   ~ 0
FC00-FFFF
Text Label 3125 2350 2    50   ~ 0
~BANK4
Text Label 3125 2450 2    50   ~ 0
~BANK5
Text Label 3125 2550 2    50   ~ 0
~BANK6
Text Label 3125 2650 2    50   ~ 0
~BANK7
Text Label 3125 3150 2    50   ~ 0
~USR5
Text Label 3125 3250 2    50   ~ 0
~USR6
Text Label 3125 3350 2    50   ~ 0
~USR7
Text Label 3125 3450 2    50   ~ 0
~USR8
Text Label 5425 6850 2    50   ~ 0
~USR2
Text Label 3975 9350 2    50   ~ 0
~BANK3
Text Label 3975 9450 2    50   ~ 0
~BANK1
Text Label 5425 6750 2    50   ~ 0
~USR4
Text Label 3975 8750 2    50   ~ 0
~BANK0
Text Label 5425 6150 2    50   ~ 0
~USR1
Text Label 5425 6650 2    50   ~ 0
~USR5
Text Label 5425 6250 2    50   ~ 0
~USR3
Text Label 3975 8850 2    50   ~ 0
~BANK2
Text Label 5425 6450 2    50   ~ 0
~USR8
Text Label 5425 6350 2    50   ~ 0
~USR6
Text Label 3975 8950 2    50   ~ 0
~BANK5
Text Label 3975 9150 2    50   ~ 0
~BANK6
Text Label 3975 9250 2    50   ~ 0
~BANK4
Text Label 5425 6550 2    50   ~ 0
~USR7
Text Label 3975 9050 2    50   ~ 0
~BANK7
Wire Wire Line
	3625 8750 3975 8750
Wire Wire Line
	3625 8850 3975 8850
Wire Wire Line
	3625 8950 3975 8950
Wire Wire Line
	3625 9050 3975 9050
Wire Wire Line
	3625 9150 3975 9150
Wire Wire Line
	3625 9250 3975 9250
Wire Wire Line
	3625 9350 3975 9350
Wire Wire Line
	3625 9450 3975 9450
Wire Wire Line
	5075 6250 5425 6250
Wire Wire Line
	5075 6750 5425 6750
Wire Wire Line
	5075 6350 5425 6350
Wire Wire Line
	5075 6450 5425 6450
Wire Wire Line
	5075 6550 5425 6550
Wire Wire Line
	5075 6850 5425 6850
Wire Wire Line
	5075 6150 5425 6150
Wire Wire Line
	5075 6650 5425 6650
Text Notes 5100 4975 0    50   ~ 0
F800
Text Label 4500 4700 0    50   ~ 0
~CE_MUCIO
Wire Wire Line
	4500 4700 4975 4700
Wire Wire Line
	5650 6575 5650 6475
Wire Wire Line
	5525 5675 5725 5675
Wire Wire Line
	6300 5175 6300 5200
Wire Wire Line
	6475 5775 7025 5775
Wire Wire Line
	5525 5975 5725 5975
Wire Wire Line
	6125 5175 6125 5475
Wire Wire Line
	5525 6175 5725 6175
Wire Wire Line
	5525 6375 5725 6375
Wire Wire Line
	5525 6075 5725 6075
Text Label 7025 5775 2    50   ~ 0
~CE_MUCIO
Wire Wire Line
	5650 6650 5650 6575
Wire Wire Line
	5525 5875 5725 5875
Text Notes 7050 5675 0    50   ~ 0
FC00-FFFF
Wire Wire Line
	5725 6575 5650 6575
Wire Wire Line
	6125 5175 6300 5175
Wire Wire Line
	5525 5775 5725 5775
Wire Wire Line
	5650 6475 5725 6475
Wire Wire Line
	6125 5150 6125 5175
Text Notes 5725 5375 0    50   ~ 0
MUC PI
Wire Wire Line
	6475 5975 7025 5975
Wire Wire Line
	6475 5675 7025 5675
Wire Wire Line
	5525 6275 5725 6275
Text Label 5525 5675 0    50   ~ 0
A8
Text Label 5525 5775 0    50   ~ 0
A9
Text Label 5525 5975 0    50   ~ 0
A11
Text Label 5525 6275 0    50   ~ 0
A14
Text Label 5525 6375 0    50   ~ 0
A15
Text Label 7025 5675 2    50   ~ 0
~CE_EPROM
Connection ~ 5650 6575
Text Label 5525 6075 0    50   ~ 0
A12
Connection ~ 6125 5175
Text Label 5525 6175 0    50   ~ 0
A13
Text Label 5525 5875 0    50   ~ 0
A10
$Comp
L power:GND #PWR?
U 1 1 5F0471B4
P 5650 6650
AR Path="/5EB2C18C/5F0471B4" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F0471B4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5650 6400 50  0001 C CNN
F 1 "GND" H 5650 6500 50  0000 C CNN
F 2 "" H 5650 6650 50  0001 C CNN
F 3 "" H 5650 6650 50  0001 C CNN
	1    5650 6650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F00ED98
P 6125 5150
AR Path="/5EB2C18C/5F00ED98" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F00ED98" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6125 5000 50  0001 C CNN
F 1 "+5V" H 6125 5300 50  0000 C CNN
F 2 "" H 6125 5150 50  0001 C CNN
F 3 "" H 6125 5150 50  0001 C CNN
	1    6125 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F00ED88
P 6300 5400
AR Path="/5EB2C18C/5F00ED88" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F00ED88" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6300 5150 50  0001 C CNN
F 1 "GND" H 6300 5250 50  0000 C CNN
F 2 "" H 6300 5400 50  0001 C CNN
F 3 "" H 6300 5400 50  0001 C CNN
	1    6300 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F00ED7B
P 6300 5300
AR Path="/5EB2C18C/5F00ED7B" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F00ED7B" Ref="C?"  Part="1" 
F 0 "C?" H 6392 5346 50  0000 L CNN
F 1 "22n" H 6392 5255 50  0000 L CNN
F 2 "" H 6300 5300 50  0001 C CNN
F 3 "~" H 6300 5300 50  0001 C CNN
	1    6300 5300
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:6306-my_ics U8
U 1 1 5F00DBF4
P 6125 5975
F 0 "U8" H 5825 6500 50  0000 C CNN
F 1 "6306" H 5875 6425 50  0000 C CNN
F 2 "" H 5825 6075 50  0001 C CNN
F 3 "" H 5825 6075 50  0001 C CNN
	1    6125 5975
	1    0    0    -1  
$EndComp
Text Notes 7050 5775 0    50   ~ 0
F800
Text Notes 7250 5975 2    50   ~ 0
FF00
Wire Wire Line
	10800 4200 11300 4200
Text Label 7025 5975 2    50   ~ 0
~CE_MUCVEK
Text Notes 11950 4500 2    50   ~ 0
FF00
Text Label 10800 4200 0    50   ~ 0
~CE_MUCVEK
Text Notes 12725 4225 0    50   ~ 0
FFFA-FFFB
Text Notes 12750 3350 0    50   ~ 0
FC00-FFF9
Text Label 14500 1925 2    50   ~ 0
EPROM_A1
Text Label 14500 1825 2    50   ~ 0
EPROM_A2
Text Label 14500 1725 2    50   ~ 0
EPROM_A3
Text Label 14500 1625 2    50   ~ 0
EPROM_A4
Text Label 14675 1575 0    50   ~ 0
EPROM_A3
Text Label 14675 1675 0    50   ~ 0
EPROM_A4
Text Label 14675 1375 0    50   ~ 0
EPROM_A1
Text Label 14900 4025 2    50   ~ 0
EPROM_A2
Text Label 14900 3925 2    50   ~ 0
EPROM_A3
Text Label 14900 3825 2    50   ~ 0
EPROM_A4
Text Label 14900 4125 2    50   ~ 0
EPROM_A1
Wire Wire Line
	14475 4125 14900 4125
Wire Wire Line
	14425 4025 14900 4025
Wire Wire Line
	14375 3925 14900 3925
Wire Wire Line
	14325 3825 14900 3825
Wire Wire Line
	12475 1925 12600 1925
Wire Wire Line
	12850 1625 12600 1625
Wire Wire Line
	12850 1725 12850 1825
$Comp
L power:GND #PWR?
U 1 1 5F423AC0
P 12050 1975
AR Path="/5EB2C18C/5F423AC0" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F423AC0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12050 1725 50  0001 C CNN
F 1 "GND" H 12050 1825 50  0000 C CNN
F 2 "" H 12050 1975 50  0001 C CNN
F 3 "" H 12050 1975 50  0001 C CNN
	1    12050 1975
	1    0    0    -1  
$EndComp
Wire Wire Line
	12925 1925 12600 1925
Wire Wire Line
	13600 700  13600 725 
Wire Wire Line
	12925 2125 12550 2125
Wire Wire Line
	13425 700  13600 700 
Text Label 14675 1475 0    50   ~ 0
EPROM_A2
Text Notes 11050 1425 0    50   ~ 0
FFFE-FFFF
Wire Wire Line
	13950 1225 13925 1225
Wire Wire Line
	12925 1625 12850 1625
Wire Wire Line
	12875 1225 12925 1225
Wire Wire Line
	14100 1925 13925 1925
Wire Wire Line
	13425 700  13425 925 
Text Notes 11525 1875 0    50   ~ 0
FFFC-FFFD
Connection ~ 12850 1725
Wire Wire Line
	14050 1825 13925 1825
Wire Wire Line
	14000 1725 13925 1725
Wire Wire Line
	14100 1525 13925 1525
Wire Wire Line
	13950 1225 13950 1625
Wire Wire Line
	14000 1325 13925 1325
$Comp
L power:+5V #PWR?
U 1 1 5F420618
P 12600 1600
AR Path="/5EB2C18C/5F420618" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F420618" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12600 1450 50  0001 C CNN
F 1 "+5V" H 12600 1725 50  0000 C CNN
F 2 "" H 12600 1600 50  0001 C CNN
F 3 "" H 12600 1600 50  0001 C CNN
	1    12600 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	12925 1725 12850 1725
Wire Wire Line
	12850 1725 12850 1625
Wire Wire Line
	12600 1600 12600 1625
$Comp
L Device:R_Small R4
U 1 1 5F420626
P 12600 1825
F 0 "R4" H 12541 1871 50  0000 R CNN
F 1 "10k" H 12541 1780 50  0000 R CNN
F 2 "" H 12600 1825 50  0001 C CNN
F 3 "~" H 12600 1825 50  0001 C CNN
	1    12600 1825
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12925 1825 12850 1825
Wire Wire Line
	14050 1425 14050 1825
Wire Wire Line
	14050 1425 13925 1425
$Comp
L Jumper:Jumper_2_Open JP?
U 1 1 5F4232ED
P 12275 1925
F 0 "JP?" H 12275 2160 50  0000 C CNN
F 1 "Jumper_2_Open" H 12275 2069 50  0000 C CNN
F 2 "" H 12275 1925 50  0001 C CNN
F 3 "~" H 12275 1925 50  0001 C CNN
	1    12275 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	12550 2225 12925 2225
Text Label 11875 1425 0    50   ~ 0
IRQ_SRC1l
Text Label 11875 1525 0    50   ~ 0
IRQ_SRC0l
Text Label 11875 1325 0    50   ~ 0
IRQ_SRC2l
Wire Wire Line
	11875 1525 12925 1525
Wire Wire Line
	14100 1525 14100 1925
Wire Wire Line
	13425 675  13425 700 
Wire Wire Line
	12875 1300 12875 1225
Wire Wire Line
	13950 1625 13925 1625
Wire Wire Line
	14000 1325 14000 1725
Connection ~ 13425 700 
Connection ~ 14100 1925
$Comp
L power:+5V #PWR?
U 1 1 5F0626C5
P 13425 675
AR Path="/5EB2C18C/5F0626C5" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F0626C5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13425 525 50  0001 C CNN
F 1 "+5V" H 13425 825 50  0000 C CNN
F 2 "" H 13425 675 50  0001 C CNN
F 3 "" H 13425 675 50  0001 C CNN
	1    13425 675 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0626B7
P 12875 1300
AR Path="/5EB2C18C/5F0626B7" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F0626B7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12875 1050 50  0001 C CNN
F 1 "GND" H 12875 1125 50  0000 C CNN
F 2 "" H 12875 1300 50  0001 C CNN
F 3 "" H 12875 1300 50  0001 C CNN
	1    12875 1300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS244 U14
U 1 1 5F0626A9
P 13425 1725
AR Path="/5EB2C18C/5F0626A9" Ref="U14"  Part="1" 
AR Path="/5E890F69/5F0626A9" Ref="U?"  Part="1" 
F 0 "U14" H 13050 2475 50  0000 C CNN
F 1 "74LS244" H 13150 2375 50  0000 C CNN
F 2 "" H 13425 1725 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS244" H 13425 1725 50  0001 C CNN
	1    13425 1725
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F062689
P 13600 825
AR Path="/5EB2C18C/5F062689" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F062689" Ref="C?"  Part="1" 
F 0 "C?" H 13692 871 50  0000 L CNN
F 1 "22n" H 13692 780 50  0000 L CNN
F 2 "" H 13600 825 50  0001 C CNN
F 3 "~" H 13600 825 50  0001 C CNN
	1    13600 825 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F06267B
P 13425 2525
AR Path="/5EB2C18C/5F06267B" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F06267B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13425 2275 50  0001 C CNN
F 1 "GND" H 13425 2375 50  0000 C CNN
F 2 "" H 13425 2525 50  0001 C CNN
F 3 "" H 13425 2525 50  0001 C CNN
	1    13425 2525
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F06264D
P 13600 925
AR Path="/5EB2C18C/5F06264D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F06264D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13600 675 50  0001 C CNN
F 1 "GND" H 13600 775 50  0000 C CNN
F 2 "" H 13600 925 50  0001 C CNN
F 3 "" H 13600 925 50  0001 C CNN
	1    13600 925 
	1    0    0    -1  
$EndComp
Connection ~ 12850 1625
Connection ~ 12600 1625
Wire Wire Line
	12050 1925 12050 1975
Wire Wire Line
	12075 1925 12050 1925
Wire Wire Line
	11875 1325 12925 1325
Wire Wire Line
	11875 1425 12925 1425
Text Notes 11525 1475 0    50   ~ 0
Interrupt \nSource
Connection ~ 12600 1925
Wire Wire Line
	12600 1625 12600 1725
Wire Wire Line
	14100 1925 14500 1925
Wire Wire Line
	15050 1375 14675 1375
Wire Wire Line
	15050 1475 14675 1475
Wire Wire Line
	15050 1575 14675 1575
Wire Wire Line
	15050 1675 14675 1675
Wire Wire Line
	13950 1625 14500 1625
Connection ~ 13950 1625
Wire Wire Line
	14500 1725 14000 1725
Connection ~ 14000 1725
Wire Wire Line
	14050 1825 14500 1825
Connection ~ 14050 1825
Wire Wire Line
	10050 3475 10225 3475
$Comp
L power:+5V #PWR?
U 1 1 5F56BAE1
P 10050 3450
AR Path="/5EB2C18C/5F56BAE1" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F56BAE1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10050 3300 50  0001 C CNN
F 1 "+5V" H 10050 3600 50  0000 C CNN
F 2 "" H 10050 3450 50  0001 C CNN
F 3 "" H 10050 3450 50  0001 C CNN
	1    10050 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56BAEF
P 10225 3600
AR Path="/5EB2C18C/5F56BAEF" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F56BAEF" Ref="C?"  Part="1" 
F 0 "C?" H 10317 3646 50  0000 L CNN
F 1 "22n" H 10317 3555 50  0000 L CNN
F 2 "" H 10225 3600 50  0001 C CNN
F 3 "~" H 10225 3600 50  0001 C CNN
	1    10225 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F56BAFC
P 10225 3700
AR Path="/5EB2C18C/5F56BAFC" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F56BAFC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10225 3450 50  0001 C CNN
F 1 "GND" H 10225 3550 50  0000 C CNN
F 2 "" H 10225 3700 50  0001 C CNN
F 3 "" H 10225 3700 50  0001 C CNN
	1    10225 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F56BB09
P 10050 4600
AR Path="/5EB2C18C/5F56BB09" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F56BB09" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10050 4350 50  0001 C CNN
F 1 "GND" H 10050 4450 50  0000 C CNN
F 2 "" H 10050 4600 50  0001 C CNN
F 3 "" H 10050 4600 50  0001 C CNN
	1    10050 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10225 3475 10225 3500
Connection ~ 10050 3475
Wire Wire Line
	10050 3450 10050 3475
Wire Wire Line
	10500 1825 10675 1825
Connection ~ 10500 1825
Wire Wire Line
	10500 1825 10500 2050
Wire Wire Line
	10675 1825 10675 1850
$Comp
L power:GND #PWR?
U 1 1 5F573D61
P 10675 2050
AR Path="/5EB2C18C/5F573D61" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F573D61" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10675 1800 50  0001 C CNN
F 1 "GND" H 10675 1900 50  0000 C CNN
F 2 "" H 10675 2050 50  0001 C CNN
F 3 "" H 10675 2050 50  0001 C CNN
	1    10675 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F573D6F
P 10675 1950
AR Path="/5EB2C18C/5F573D6F" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F573D6F" Ref="C?"  Part="1" 
F 0 "C?" H 10767 1996 50  0000 L CNN
F 1 "22n" H 10767 1905 50  0000 L CNN
F 2 "" H 10675 1950 50  0001 C CNN
F 3 "~" H 10675 1950 50  0001 C CNN
	1    10675 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F573D7C
P 10500 3050
AR Path="/5EB2C18C/5F573D7C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F573D7C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10500 2800 50  0001 C CNN
F 1 "GND" H 10500 2900 50  0000 C CNN
F 2 "" H 10500 3050 50  0001 C CNN
F 3 "" H 10500 3050 50  0001 C CNN
	1    10500 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 1800 10500 1825
$Comp
L power:+5V #PWR?
U 1 1 5F573D8A
P 10500 1800
AR Path="/5EB2C18C/5F573D8A" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F573D8A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10500 1650 50  0001 C CNN
F 1 "+5V" H 10500 1950 50  0000 C CNN
F 2 "" H 10500 1800 50  0001 C CNN
F 3 "" H 10500 1800 50  0001 C CNN
	1    10500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 1825 10225 1825
Connection ~ 10050 1825
Wire Wire Line
	10050 1825 10050 2050
Wire Wire Line
	10225 1825 10225 1850
$Comp
L power:GND #PWR?
U 1 1 5F5AEF51
P 10225 2050
AR Path="/5EB2C18C/5F5AEF51" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5AEF51" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10225 1800 50  0001 C CNN
F 1 "GND" H 10225 1900 50  0000 C CNN
F 2 "" H 10225 2050 50  0001 C CNN
F 3 "" H 10225 2050 50  0001 C CNN
	1    10225 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F5AEF5F
P 10225 1950
AR Path="/5EB2C18C/5F5AEF5F" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F5AEF5F" Ref="C?"  Part="1" 
F 0 "C?" H 10317 1996 50  0000 L CNN
F 1 "22n" H 10317 1905 50  0000 L CNN
F 2 "" H 10225 1950 50  0001 C CNN
F 3 "~" H 10225 1950 50  0001 C CNN
	1    10225 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F5AEF6C
P 10050 3050
AR Path="/5EB2C18C/5F5AEF6C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5AEF6C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10050 2800 50  0001 C CNN
F 1 "GND" H 10050 2900 50  0000 C CNN
F 2 "" H 10050 3050 50  0001 C CNN
F 3 "" H 10050 3050 50  0001 C CNN
	1    10050 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 1800 10050 1825
$Comp
L power:+5V #PWR?
U 1 1 5F5AEF7A
P 10050 1800
AR Path="/5EB2C18C/5F5AEF7A" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5AEF7A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10050 1650 50  0001 C CNN
F 1 "+5V" H 10050 1950 50  0000 C CNN
F 2 "" H 10050 1800 50  0001 C CNN
F 3 "" H 10050 1800 50  0001 C CNN
	1    10050 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 1825 8875 1825
Connection ~ 8700 1825
Wire Wire Line
	8700 1825 8700 2050
Wire Wire Line
	8875 1825 8875 1850
$Comp
L power:GND #PWR?
U 1 1 5F5C23AE
P 8875 2050
AR Path="/5EB2C18C/5F5C23AE" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C23AE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8875 1800 50  0001 C CNN
F 1 "GND" H 8875 1900 50  0000 C CNN
F 2 "" H 8875 2050 50  0001 C CNN
F 3 "" H 8875 2050 50  0001 C CNN
	1    8875 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F5C23BC
P 8875 1950
AR Path="/5EB2C18C/5F5C23BC" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F5C23BC" Ref="C?"  Part="1" 
F 0 "C?" H 8967 1996 50  0000 L CNN
F 1 "22n" H 8967 1905 50  0000 L CNN
F 2 "" H 8875 1950 50  0001 C CNN
F 3 "~" H 8875 1950 50  0001 C CNN
	1    8875 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F5C23C9
P 8700 3050
AR Path="/5EB2C18C/5F5C23C9" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C23C9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8700 2800 50  0001 C CNN
F 1 "GND" H 8700 2900 50  0000 C CNN
F 2 "" H 8700 3050 50  0001 C CNN
F 3 "" H 8700 3050 50  0001 C CNN
	1    8700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 1800 8700 1825
$Comp
L power:+5V #PWR?
U 1 1 5F5C23D7
P 8700 1800
AR Path="/5EB2C18C/5F5C23D7" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C23D7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8700 1650 50  0001 C CNN
F 1 "+5V" H 8700 1950 50  0000 C CNN
F 2 "" H 8700 1800 50  0001 C CNN
F 3 "" H 8700 1800 50  0001 C CNN
	1    8700 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F5C4731
P 9150 3050
AR Path="/5EB2C18C/5F5C4731" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C4731" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9150 2800 50  0001 C CNN
F 1 "GND" H 9150 2900 50  0000 C CNN
F 2 "" H 9150 3050 50  0001 C CNN
F 3 "" H 9150 3050 50  0001 C CNN
	1    9150 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F5C473E
P 9150 1800
AR Path="/5EB2C18C/5F5C473E" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C473E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9150 1650 50  0001 C CNN
F 1 "+5V" H 9150 1950 50  0000 C CNN
F 2 "" H 9150 1800 50  0001 C CNN
F 3 "" H 9150 1800 50  0001 C CNN
	1    9150 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F5C474B
P 9325 2050
AR Path="/5EB2C18C/5F5C474B" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C474B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9325 1800 50  0001 C CNN
F 1 "GND" H 9325 1900 50  0000 C CNN
F 2 "" H 9325 2050 50  0001 C CNN
F 3 "" H 9325 2050 50  0001 C CNN
	1    9325 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1800 9150 1825
$Comp
L Device:C_Small C?
U 1 1 5F5C475A
P 9325 1950
AR Path="/5EB2C18C/5F5C475A" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F5C475A" Ref="C?"  Part="1" 
F 0 "C?" H 9417 1996 50  0000 L CNN
F 1 "22n" H 9417 1905 50  0000 L CNN
F 2 "" H 9325 1950 50  0001 C CNN
F 3 "~" H 9325 1950 50  0001 C CNN
	1    9325 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1825 9325 1825
Connection ~ 9150 1825
Wire Wire Line
	9150 1825 9150 2050
Wire Wire Line
	9325 1825 9325 1850
$Comp
L power:+5V #PWR?
U 1 1 5F5C5453
P 9600 1800
AR Path="/5EB2C18C/5F5C5453" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C5453" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9600 1650 50  0001 C CNN
F 1 "+5V" H 9600 1950 50  0000 C CNN
F 2 "" H 9600 1800 50  0001 C CNN
F 3 "" H 9600 1800 50  0001 C CNN
	1    9600 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F5C5460
P 9775 2050
AR Path="/5EB2C18C/5F5C5460" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C5460" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9775 1800 50  0001 C CNN
F 1 "GND" H 9775 1900 50  0000 C CNN
F 2 "" H 9775 2050 50  0001 C CNN
F 3 "" H 9775 2050 50  0001 C CNN
	1    9775 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F5C546E
P 9775 1950
AR Path="/5EB2C18C/5F5C546E" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F5C546E" Ref="C?"  Part="1" 
F 0 "C?" H 9867 1996 50  0000 L CNN
F 1 "22n" H 9867 1905 50  0000 L CNN
F 2 "" H 9775 1950 50  0001 C CNN
F 3 "~" H 9775 1950 50  0001 C CNN
	1    9775 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 1800 9600 1825
Connection ~ 9600 1825
Wire Wire Line
	9775 1825 9775 1850
$Comp
L power:GND #PWR?
U 1 1 5F5C547E
P 9600 3050
AR Path="/5EB2C18C/5F5C547E" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F5C547E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9600 2800 50  0001 C CNN
F 1 "GND" H 9600 2900 50  0000 C CNN
F 2 "" H 9600 3050 50  0001 C CNN
F 3 "" H 9600 3050 50  0001 C CNN
	1    9600 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 1825 9775 1825
Wire Wire Line
	9600 1825 9600 2050
Wire Wire Line
	8700 3475 8875 3475
Connection ~ 8700 3475
Wire Wire Line
	8700 3475 8700 3700
Wire Wire Line
	8875 3475 8875 3500
$Comp
L power:GND #PWR?
U 1 1 5F621349
P 8875 3700
AR Path="/5EB2C18C/5F621349" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F621349" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8875 3450 50  0001 C CNN
F 1 "GND" H 8875 3550 50  0000 C CNN
F 2 "" H 8875 3700 50  0001 C CNN
F 3 "" H 8875 3700 50  0001 C CNN
	1    8875 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F621357
P 8875 3600
AR Path="/5EB2C18C/5F621357" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F621357" Ref="C?"  Part="1" 
F 0 "C?" H 8967 3646 50  0000 L CNN
F 1 "22n" H 8967 3555 50  0000 L CNN
F 2 "" H 8875 3600 50  0001 C CNN
F 3 "~" H 8875 3600 50  0001 C CNN
	1    8875 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F621364
P 8700 4700
AR Path="/5EB2C18C/5F621364" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F621364" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8700 4450 50  0001 C CNN
F 1 "GND" H 8700 4550 50  0000 C CNN
F 2 "" H 8700 4700 50  0001 C CNN
F 3 "" H 8700 4700 50  0001 C CNN
	1    8700 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 3450 8700 3475
$Comp
L power:+5V #PWR?
U 1 1 5F621372
P 8700 3450
AR Path="/5EB2C18C/5F621372" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F621372" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8700 3300 50  0001 C CNN
F 1 "+5V" H 8700 3600 50  0000 C CNN
F 2 "" H 8700 3450 50  0001 C CNN
F 3 "" H 8700 3450 50  0001 C CNN
	1    8700 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F623777
P 9150 4700
AR Path="/5EB2C18C/5F623777" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F623777" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9150 4450 50  0001 C CNN
F 1 "GND" H 9150 4550 50  0000 C CNN
F 2 "" H 9150 4700 50  0001 C CNN
F 3 "" H 9150 4700 50  0001 C CNN
	1    9150 4700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F623784
P 9150 3450
AR Path="/5EB2C18C/5F623784" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F623784" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9150 3300 50  0001 C CNN
F 1 "+5V" H 9150 3600 50  0000 C CNN
F 2 "" H 9150 3450 50  0001 C CNN
F 3 "" H 9150 3450 50  0001 C CNN
	1    9150 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3450 9150 3475
Wire Wire Line
	9150 3475 9150 3700
$Comp
L Device:C_Small C?
U 1 1 5F623794
P 9325 3600
AR Path="/5EB2C18C/5F623794" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F623794" Ref="C?"  Part="1" 
F 0 "C?" H 9417 3646 50  0000 L CNN
F 1 "22n" H 9417 3555 50  0000 L CNN
F 2 "" H 9325 3600 50  0001 C CNN
F 3 "~" H 9325 3600 50  0001 C CNN
	1    9325 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9325 3475 9325 3500
$Comp
L power:GND #PWR?
U 1 1 5F6237A2
P 9325 3700
AR Path="/5EB2C18C/5F6237A2" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F6237A2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9325 3450 50  0001 C CNN
F 1 "GND" H 9325 3550 50  0000 C CNN
F 2 "" H 9325 3700 50  0001 C CNN
F 3 "" H 9325 3700 50  0001 C CNN
	1    9325 3700
	1    0    0    -1  
$EndComp
Connection ~ 9150 3475
Wire Wire Line
	9150 3475 9325 3475
$Comp
L Device:C_Small C?
U 1 1 5F624D0D
P 9775 3600
AR Path="/5EB2C18C/5F624D0D" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F624D0D" Ref="C?"  Part="1" 
F 0 "C?" H 9867 3646 50  0000 L CNN
F 1 "22n" H 9867 3555 50  0000 L CNN
F 2 "" H 9775 3600 50  0001 C CNN
F 3 "~" H 9775 3600 50  0001 C CNN
	1    9775 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F624D1A
P 9600 3450
AR Path="/5EB2C18C/5F624D1A" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F624D1A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9600 3300 50  0001 C CNN
F 1 "+5V" H 9600 3600 50  0000 C CNN
F 2 "" H 9600 3450 50  0001 C CNN
F 3 "" H 9600 3450 50  0001 C CNN
	1    9600 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9775 3475 9775 3500
Wire Wire Line
	9600 3450 9600 3475
Wire Wire Line
	9600 3475 9600 3700
Wire Wire Line
	9600 3475 9775 3475
$Comp
L power:GND #PWR?
U 1 1 5F624D2B
P 9600 4700
AR Path="/5EB2C18C/5F624D2B" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F624D2B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9600 4450 50  0001 C CNN
F 1 "GND" H 9600 4550 50  0000 C CNN
F 2 "" H 9600 4700 50  0001 C CNN
F 3 "" H 9600 4700 50  0001 C CNN
	1    9600 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F624D38
P 9775 3700
AR Path="/5EB2C18C/5F624D38" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F624D38" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9775 3450 50  0001 C CNN
F 1 "GND" H 9775 3550 50  0000 C CNN
F 2 "" H 9775 3700 50  0001 C CNN
F 3 "" H 9775 3700 50  0001 C CNN
	1    9775 3700
	1    0    0    -1  
$EndComp
Connection ~ 9600 3475
Wire Wire Line
	10050 3475 10050 3800
Wire Wire Line
	10525 6925 10625 6925
Text Label 10900 6325 2    50   ~ 0
IRQ_SRC0
Wire Wire Line
	10525 6325 10900 6325
Wire Wire Line
	10525 6425 10900 6425
Text Label 10900 6425 2    50   ~ 0
IRQ_SRC1
Text Label 10900 6525 2    50   ~ 0
IRQ_SRC2
Wire Wire Line
	10525 6525 10900 6525
Wire Wire Line
	10025 5800 10025 6025
Wire Wire Line
	10025 5800 10200 5800
Connection ~ 10025 5800
Wire Wire Line
	10200 5800 10200 5825
$Comp
L power:GND #PWR?
U 1 1 5F69F6D4
P 10200 6025
AR Path="/5EB2C18C/5F69F6D4" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F69F6D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10200 5775 50  0001 C CNN
F 1 "GND" H 10200 5875 50  0000 C CNN
F 2 "" H 10200 6025 50  0001 C CNN
F 3 "" H 10200 6025 50  0001 C CNN
	1    10200 6025
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F69F6E2
P 10200 5925
AR Path="/5EB2C18C/5F69F6E2" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F69F6E2" Ref="C?"  Part="1" 
F 0 "C?" H 10292 5971 50  0000 L CNN
F 1 "22n" H 10292 5880 50  0000 L CNN
F 2 "" H 10200 5925 50  0001 C CNN
F 3 "~" H 10200 5925 50  0001 C CNN
	1    10200 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	10025 5775 10025 5800
$Comp
L power:+5V #PWR?
U 1 1 5F69F6F0
P 10025 5775
AR Path="/5EB2C18C/5F69F6F0" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F69F6F0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10025 5625 50  0001 C CNN
F 1 "+5V" H 10025 5925 50  0000 C CNN
F 2 "" H 10025 5775 50  0001 C CNN
F 3 "" H 10025 5775 50  0001 C CNN
	1    10025 5775
	1    0    0    -1  
$EndComp
Text Label 6525 4500 2    50   ~ 0
~CS_U16
Text Label 13000 6075 2    50   ~ 0
DL6
Wire Wire Line
	11475 5175 11725 5175
Wire Wire Line
	10875 5275 10825 5275
Wire Wire Line
	10825 5275 10825 5075
Wire Wire Line
	10825 5075 10875 5075
Text Label 12750 4425 0    50   ~ 0
~SEL_FFFA
Text Label 12450 3500 2    50   ~ 0
~SEL_FFFA
Wire Wire Line
	12050 3500 12450 3500
Wire Wire Line
	12750 4425 13300 4425
Text Label 12450 3600 2    50   ~ 0
~SEL_FFFC
Text Label 12550 2225 0    50   ~ 0
~SEL_FFFC
Wire Wire Line
	12050 3600 12450 3600
Text Label 12550 2125 0    50   ~ 0
~SEL_FFFE
Text Label 12450 3700 2    50   ~ 0
~SEL_FFFE
Text Label 12450 3800 2    50   ~ 0
~SEL_FF00
Text Label 12775 4325 0    50   ~ 0
~SEL_FF00
Wire Wire Line
	12050 3800 12450 3800
Wire Wire Line
	12775 4325 13300 4325
Text Label 10450 5275 0    50   ~ 0
~SEL_FFFA
Wire Wire Line
	10450 5275 10825 5275
Connection ~ 10825 5275
Text Label 12775 5075 2    50   ~ 0
~FFFA_Pulse
Wire Wire Line
	12775 5075 12325 5075
Text Label 14925 5300 0    50   ~ 0
~FFFA_Pulse
Wire Wire Line
	14925 5300 15425 5300
Wire Wire Line
	15425 5300 15425 5125
Wire Wire Line
	12000 6425 12500 6425
Text Label 12000 6425 0    50   ~ 0
~FFFA_Pulse
Wire Wire Line
	12500 6275 12500 6425
Wire Wire Line
	12025 5875 12200 5875
Text Label 11125 5975 0    50   ~ 0
R~W~bb
Wire Wire Line
	11425 5975 11125 5975
Connection ~ 11400 5775
Wire Wire Line
	11400 5775 11425 5775
Text Notes 11175 5775 0    50   ~ 0
n.p.
Wire Wire Line
	11400 5325 11400 5775
Wire Wire Line
	11400 5775 11300 5775
Text Label 11400 5325 3    50   ~ 0
PLUP9
Text Label 7175 8100 2    50   ~ 0
MULTI
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 5F7E0747
P 5850 8300
F 0 "J?" H 5768 8717 50  0000 C CNN
F 1 "Conn_01x06" H 5768 8626 50  0000 C CNN
F 2 "" H 5850 8300 50  0001 C CNN
F 3 "~" H 5850 8300 50  0001 C CNN
	1    5850 8300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 8100 6500 8100
Wire Wire Line
	5950 8200 6500 8200
Wire Wire Line
	5950 8300 6500 8300
Wire Wire Line
	5950 8400 6500 8400
Wire Wire Line
	5950 8500 6500 8500
Wire Wire Line
	5950 8600 6500 8600
Text Label 6125 8100 0    50   ~ 0
wht
Text Label 6125 8200 0    50   ~ 0
brn
Text Label 6125 8300 0    50   ~ 0
grn
Text Label 6125 8400 0    50   ~ 0
yel
Text Label 6125 8500 0    50   ~ 0
gry
Text Label 6125 8600 0    50   ~ 0
pnk
$Comp
L es65-rescue:Switch_SPTT_small-my_switches SW?
U 1 1 5F7E8A6E
P 4950 8150
F 0 "SW?" H 4950 8490 50  0000 C CNN
F 1 "Switch_SPTT_small" H 4950 8399 50  0000 C CNN
F 2 "" H 4950 8150 50  0000 C CNN
F 3 "" H 4950 8150 50  0000 C CNN
	1    4950 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5125 8100 5650 8100
Text Label 5475 8100 2    50   ~ 0
MULTI
Wire Wire Line
	5650 8300 5250 8300
Wire Wire Line
	5250 8300 5250 8200
Wire Wire Line
	5250 8200 5125 8200
Text Label 7175 8300 2    50   ~ 0
SINGLE
Wire Wire Line
	5650 8200 5525 8200
$Comp
L power:+5V #PWR?
U 1 1 5F7F045B
P 4500 8000
AR Path="/5EB2C18C/5F7F045B" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F7F045B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4500 7850 50  0001 C CNN
F 1 "+5V" H 4500 8150 50  0000 C CNN
F 2 "" H 4500 8000 50  0001 C CNN
F 3 "" H 4500 8000 50  0001 C CNN
	1    4500 8000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 8000 4500 8150
$Comp
L power:+5V #PWR?
U 1 1 5F7F10D3
P 5525 7950
AR Path="/5EB2C18C/5F7F10D3" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F7F10D3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5525 7800 50  0001 C CNN
F 1 "+5V" H 5525 8100 50  0000 C CNN
F 2 "" H 5525 7950 50  0001 C CNN
F 3 "" H 5525 7950 50  0001 C CNN
	1    5525 7950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 7950 5525 8200
Text Label 5250 8300 0    50   ~ 0
SINGLE
Wire Wire Line
	4500 8150 4775 8150
Text Label 7225 9850 0    50   ~ 0
MULTI
Wire Wire Line
	7225 9850 7425 9850
Wire Wire Line
	7175 8100 6800 8100
Wire Wire Line
	7175 8300 6800 8300
Text Label 7225 9575 0    50   ~ 0
SINGLE
Wire Notes Line
	4375 7675 6325 7675
Wire Notes Line
	6325 7675 6325 8775
Wire Notes Line
	6325 8775 4375 8775
Wire Notes Line
	4375 8775 4375 7675
Text Notes 4400 7775 0    50   ~ 0
Front Panel\n
Wire Wire Line
	5650 8400 5125 8400
Wire Wire Line
	5125 8400 5125 8275
Wire Wire Line
	5125 8275 4500 8275
Wire Wire Line
	4500 8275 4500 8300
$Comp
L Device:LED_Small_ALT D?
U 1 1 5F80A98F
P 4500 8400
F 0 "D?" V 4546 8330 50  0000 R CNN
F 1 "IDLE" V 4455 8330 50  0000 R CNN
F 2 "" V 4500 8400 50  0001 C CNN
F 3 "~" V 4500 8400 50  0001 C CNN
	1    4500 8400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F80EDBC
P 4500 8500
AR Path="/5EB2C18C/5F80EDBC" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F80EDBC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4500 8250 50  0001 C CNN
F 1 "GND" H 4500 8350 50  0000 C CNN
F 2 "" H 4500 8500 50  0001 C CNN
F 3 "" H 4500 8500 50  0001 C CNN
	1    4500 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 8500 5550 8500
Wire Wire Line
	5550 8500 5550 8575
$Comp
L power:GND #PWR?
U 1 1 5F80FA66
P 5550 8575
AR Path="/5EB2C18C/5F80FA66" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F80FA66" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5550 8325 50  0001 C CNN
F 1 "GND" H 5550 8425 50  0000 C CNN
F 2 "" H 5550 8575 50  0001 C CNN
F 3 "" H 5550 8575 50  0001 C CNN
	1    5550 8575
	1    0    0    -1  
$EndComp
NoConn ~ 5650 8600
NoConn ~ 6800 8600
Text Label 8125 8400 2    50   ~ 0
LED_IDLE
Wire Wire Line
	8125 8400 7425 8400
Wire Wire Line
	8700 9675 8725 9675
Wire Wire Line
	8850 10150 8850 10275
Wire Wire Line
	8850 10275 9075 10275
Wire Wire Line
	9075 10275 9075 10575
Wire Wire Line
	8025 9950 8050 9950
Wire Wire Line
	8050 9950 8050 9775
Wire Wire Line
	8725 9675 8725 9300
Wire Wire Line
	8725 9300 7150 9300
Wire Wire Line
	7150 9300 7150 10050
Wire Wire Line
	7150 10050 7425 10050
Connection ~ 8725 9675
Wire Wire Line
	8725 9675 8950 9675
Wire Wire Line
	9625 9850 9575 9850
Wire Wire Line
	9575 10925 7850 10925
Wire Wire Line
	9575 10925 9575 9850
$Comp
L 4xxx:4020 U24
U 1 1 5F8F350B
P 4525 10100
F 0 "U24" H 4225 10875 50  0000 C CNN
F 1 "4020" H 4275 10775 50  0000 C CNN
F 2 "" H 4525 10100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4020bms-24bms-40bms.pdf" H 4525 10100 50  0001 C CNN
	1    4525 10100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F8F6161
P 4525 9050
AR Path="/5EB2C18C/5F8F6161" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F8F6161" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4525 8900 50  0001 C CNN
F 1 "+5V" H 4525 9200 50  0000 C CNN
F 2 "" H 4525 9050 50  0001 C CNN
F 3 "" H 4525 9050 50  0001 C CNN
	1    4525 9050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F8F616F
P 4700 9200
AR Path="/5EB2C18C/5F8F616F" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F8F616F" Ref="C?"  Part="1" 
F 0 "C?" H 4792 9246 50  0000 L CNN
F 1 "22n" H 4792 9155 50  0000 L CNN
F 2 "" H 4700 9200 50  0001 C CNN
F 3 "~" H 4700 9200 50  0001 C CNN
	1    4700 9200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F8F617C
P 4700 9300
AR Path="/5EB2C18C/5F8F617C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F8F617C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4700 9050 50  0001 C CNN
F 1 "GND" H 4700 9150 50  0000 C CNN
F 2 "" H 4700 9300 50  0001 C CNN
F 3 "" H 4700 9300 50  0001 C CNN
	1    4700 9300
	1    0    0    -1  
$EndComp
Connection ~ 4525 9075
Wire Wire Line
	4525 9075 4525 9300
Wire Wire Line
	4525 9075 4700 9075
Wire Wire Line
	4525 9050 4525 9075
Wire Wire Line
	4700 9075 4700 9100
$Comp
L power:GND #PWR?
U 1 1 5F8F7C5C
P 4525 11000
AR Path="/5EB2C18C/5F8F7C5C" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F8F7C5C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4525 10750 50  0001 C CNN
F 1 "GND" H 4525 10850 50  0000 C CNN
F 2 "" H 4525 11000 50  0001 C CNN
F 3 "" H 4525 11000 50  0001 C CNN
	1    4525 11000
	1    0    0    -1  
$EndComp
Text Label 2300 10625 0    50   ~ 0
WritePulse
Wire Wire Line
	2700 10825 2375 10825
Wire Wire Line
	2700 10625 2300 10625
Text Label 2375 10825 0    50   ~ 0
~CS_U7
Text Label 3825 10725 2    50   ~ 0
CS_U7_Pulse
Wire Wire Line
	3825 10725 3300 10725
Text Label 3500 9800 0    50   ~ 0
CS_U7_Pulse
Wire Wire Line
	3500 9800 4025 9800
Text Label 3725 9600 0    50   ~ 0
PH2bb
Wire Wire Line
	4025 9600 3725 9600
Text Label 5075 9175 0    50   ~ 0
CS_U7_Pulse
Wire Wire Line
	5250 9600 5225 9600
Connection ~ 5750 9075
$Comp
L 4xxx:4020 U23
U 1 1 5F908244
P 5750 10100
F 0 "U23" H 5450 10875 50  0000 C CNN
F 1 "4020" H 5500 10775 50  0000 C CNN
F 2 "" H 5750 10100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4020bms-24bms-40bms.pdf" H 5750 10100 50  0001 C CNN
	1    5750 10100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 9075 5925 9075
$Comp
L power:GND #PWR?
U 1 1 5F908252
P 5925 9300
AR Path="/5EB2C18C/5F908252" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F908252" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5925 9050 50  0001 C CNN
F 1 "GND" H 5925 9150 50  0000 C CNN
F 2 "" H 5925 9300 50  0001 C CNN
F 3 "" H 5925 9300 50  0001 C CNN
	1    5925 9300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5925 9075 5925 9100
$Comp
L power:+5V #PWR?
U 1 1 5F908260
P 5750 9050
AR Path="/5EB2C18C/5F908260" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F908260" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5750 8900 50  0001 C CNN
F 1 "+5V" H 5750 9200 50  0000 C CNN
F 2 "" H 5750 9050 50  0001 C CNN
F 3 "" H 5750 9050 50  0001 C CNN
	1    5750 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 9075 5750 9300
$Comp
L power:GND #PWR?
U 1 1 5F90826E
P 5750 11000
AR Path="/5EB2C18C/5F90826E" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F90826E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5750 10750 50  0001 C CNN
F 1 "GND" H 5750 10850 50  0000 C CNN
F 2 "" H 5750 11000 50  0001 C CNN
F 3 "" H 5750 11000 50  0001 C CNN
	1    5750 11000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F90827C
P 5925 9200
AR Path="/5EB2C18C/5F90827C" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F90827C" Ref="C?"  Part="1" 
F 0 "C?" H 6017 9246 50  0000 L CNN
F 1 "22n" H 6017 9155 50  0000 L CNN
F 2 "" H 5925 9200 50  0001 C CNN
F 3 "~" H 5925 9200 50  0001 C CNN
	1    5925 9200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 9050 5750 9075
Wire Wire Line
	5025 10700 5225 10700
Wire Wire Line
	5225 10700 5225 9600
Wire Wire Line
	6250 9800 6375 9800
Text Label 11000 9950 2    50   ~ 0
~RST
Wire Wire Line
	11000 9950 10825 9950
Wire Wire Line
	9475 10050 9625 10050
Text Label 6500 10025 1    50   ~ 0
PLUP12
$Comp
L Jumper:Jumper_2_Open JP?
U 1 1 5E90175F
P 6500 10425
F 0 "JP?" V 6546 10337 50  0000 R CNN
F 1 "Jumper_2_Open" V 6455 10337 50  0000 R CNN
F 2 "" H 6500 10425 50  0001 C CNN
F 3 "~" H 6500 10425 50  0001 C CNN
	1    6500 10425
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E903033
P 6500 10625
AR Path="/5EB2C18C/5E903033" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E903033" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6500 10375 50  0001 C CNN
F 1 "GND" H 6500 10475 50  0000 C CNN
F 2 "" H 6500 10625 50  0001 C CNN
F 3 "" H 6500 10625 50  0001 C CNN
	1    6500 10625
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 10025 6500 10175
Connection ~ 6500 10175
Wire Wire Line
	6500 10175 6500 10225
Wire Wire Line
	5075 9175 5075 9800
Wire Wire Line
	5075 9800 5250 9800
Wire Wire Line
	7050 11025 7250 11025
Wire Wire Line
	6250 9900 6300 9900
Wire Wire Line
	6300 9900 6300 10925
Wire Wire Line
	6300 10925 6450 10925
Wire Wire Line
	6375 11125 6450 11125
Wire Wire Line
	6375 9800 6375 11125
Wire Wire Line
	6550 9700 6550 9975
Wire Wire Line
	6550 9975 6575 9975
Wire Wire Line
	6250 9700 6550 9700
$Comp
L 74xx:74LS132 U22
U 3 1 5F23EFE8
P 6875 10075
F 0 "U22" H 6875 10400 50  0000 C CNN
F 1 "74LS132" H 6875 10309 50  0000 C CNN
F 2 "" H 6875 10075 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 6875 10075 50  0001 C CNN
	3    6875 10075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 10175 6575 10175
Wire Wire Line
	7250 10825 7200 10825
Wire Wire Line
	7200 10825 7200 10075
Wire Wire Line
	7200 10075 7175 10075
Text Label 3825 3650 1    50   ~ 0
PLUP13
Wire Wire Line
	3825 3650 3825 3750
Wire Wire Line
	3825 3750 3850 3750
Wire Wire Line
	3850 3950 3725 3950
Wire Wire Line
	2825 3950 3725 3950
Text Label 3725 3650 1    50   ~ 0
PLUP16
Wire Wire Line
	3725 3650 3725 3950
Connection ~ 3725 3950
Wire Wire Line
	6475 5875 7300 5875
Wire Wire Line
	7300 5875 7300 6075
Wire Wire Line
	7300 6075 6500 6075
Wire Wire Line
	6500 6075 6500 6575
Wire Wire Line
	6500 6575 6750 6575
Text Notes 6975 5875 0    50   ~ 0
never low
Text Notes 6550 7125 0    50   ~ 0
n.p.
Wire Wire Line
	6550 7125 6725 7125
Connection ~ 9425 8725
Wire Wire Line
	9275 8725 9425 8725
Text Notes 9275 8725 0    50   ~ 0
n.p.
Connection ~ 9500 8425
Wire Wire Line
	9275 8425 9500 8425
Text Notes 9275 8425 0    50   ~ 0
n.p.
Wire Wire Line
	9250 9025 9775 9025
Wire Wire Line
	8425 9125 8650 9125
Text Label 8425 9125 0    50   ~ 0
SYNb
Wire Wire Line
	10775 8325 11225 8325
Wire Wire Line
	11225 8225 10775 8225
Wire Wire Line
	10775 8125 11225 8125
Text Label 11225 8225 2    50   ~ 0
IRQ_SRC1l
Text Label 11225 8125 2    50   ~ 0
IRQ_SRC0l
Text Label 11225 8325 2    50   ~ 0
IRQ_SRC2l
Wire Wire Line
	8850 8325 9775 8325
Wire Wire Line
	8850 8225 9775 8225
Wire Wire Line
	8850 8125 9775 8125
Text Label 8850 8225 0    50   ~ 0
IRQ_SRC1
Text Label 8850 8125 0    50   ~ 0
IRQ_SRC0
Text Label 8850 8325 0    50   ~ 0
IRQ_SRC2
Text Label 9575 8825 0    50   ~ 0
PB0
Wire Wire Line
	9775 8825 9575 8825
Wire Wire Line
	10775 8625 11050 8625
Wire Wire Line
	10775 8525 11050 8525
Text Label 11050 8825 2    50   ~ 0
DL7
Text Label 11050 8625 2    50   ~ 0
DL2
Text Label 11050 8525 2    50   ~ 0
DL1
Wire Wire Line
	10775 8725 11050 8725
Text Label 11050 8425 2    50   ~ 0
DL0
Wire Wire Line
	10775 8825 11050 8825
Text Label 11050 8725 2    50   ~ 0
DL3
Wire Wire Line
	10775 8425 11050 8425
$Comp
L power:GND #PWR?
U 1 1 5F15D8B1
P 10450 7825
AR Path="/5EB2C18C/5F15D8B1" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F15D8B1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10450 7575 50  0001 C CNN
F 1 "GND" H 10450 7675 50  0000 C CNN
F 2 "" H 10450 7825 50  0001 C CNN
F 3 "" H 10450 7825 50  0001 C CNN
	1    10450 7825
	1    0    0    -1  
$EndComp
Connection ~ 10275 7600
Wire Wire Line
	10275 7600 10450 7600
Wire Wire Line
	10275 7575 10275 7600
$Comp
L Device:C_Small C?
U 1 1 5F15D8A1
P 10450 7725
AR Path="/5EB2C18C/5F15D8A1" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F15D8A1" Ref="C?"  Part="1" 
F 0 "C?" H 10542 7771 50  0000 L CNN
F 1 "22n" H 10542 7680 50  0000 L CNN
F 2 "" H 10450 7725 50  0001 C CNN
F 3 "~" H 10450 7725 50  0001 C CNN
	1    10450 7725
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 7600 10450 7625
Wire Wire Line
	10275 7600 10275 7825
$Comp
L power:+5V #PWR?
U 1 1 5F15D891
P 10275 7575
AR Path="/5EB2C18C/5F15D891" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F15D891" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10275 7425 50  0001 C CNN
F 1 "+5V" H 10275 7725 50  0000 C CNN
F 2 "" H 10275 7575 50  0001 C CNN
F 3 "" H 10275 7575 50  0001 C CNN
	1    10275 7575
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 8925 8650 8925
Text Label 8400 8925 0    50   ~ 0
PH2bb
$Comp
L 74xx:74LS00 U26
U 3 1 5F13941E
P 8950 9025
F 0 "U26" H 8950 9350 50  0000 C CNN
F 1 "74LS00" H 8950 9259 50  0000 C CNN
F 2 "" H 8950 9025 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 8950 9025 50  0001 C CNN
	3    8950 9025
	1    0    0    -1  
$EndComp
Text Label 9475 9125 0    50   ~ 0
~RSTbb
Wire Wire Line
	9775 9125 9475 9125
$Comp
L power:GND #PWR?
U 1 1 5F12D213
P 10275 9425
AR Path="/5EB2C18C/5F12D213" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F12D213" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10275 9175 50  0001 C CNN
F 1 "GND" H 10275 9275 50  0000 C CNN
F 2 "" H 10275 9425 50  0001 C CNN
F 3 "" H 10275 9425 50  0001 C CNN
	1    10275 9425
	1    0    0    -1  
$EndComp
Text Notes 9275 8525 0    50   ~ 0
n.p.
Wire Wire Line
	9775 8525 9275 8525
Wire Wire Line
	9425 8725 9775 8725
Wire Wire Line
	9425 7750 9425 8725
Wire Wire Line
	9500 8425 9500 7750
Wire Wire Line
	9775 8425 9500 8425
Text Label 9425 7750 3    50   ~ 0
PLUP10
Text Label 9500 7750 3    50   ~ 0
PLUP11
$Comp
L 74xx:74LS273 U15
U 1 1 5F06D9BC
P 10275 8625
F 0 "U15" H 9900 9400 50  0000 C CNN
F 1 "74LS273" H 10025 9300 50  0000 C CNN
F 2 "" H 10275 8625 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS273" H 10275 8625 50  0001 C CNN
	1    10275 8625
	1    0    0    -1  
$EndComp
Wire Wire Line
	9775 8625 9175 8625
Wire Wire Line
	9175 8625 9175 8400
Wire Wire Line
	9175 8400 8675 8400
Wire Wire Line
	8675 8400 8675 8100
Wire Wire Line
	7425 8100 7425 7025
Wire Wire Line
	7425 7025 7325 7025
Wire Wire Line
	7425 8100 8675 8100
$Comp
L power:+5V #PWR?
U 1 1 5E9E2896
P 12650 8350
AR Path="/5EB2C18C/5E9E2896" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9E2896" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12650 8200 50  0001 C CNN
F 1 "+5V" H 12650 8500 50  0000 C CNN
F 2 "" H 12650 8350 50  0001 C CNN
F 3 "" H 12650 8350 50  0001 C CNN
	1    12650 8350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9E22A2
P 12650 9750
AR Path="/5EB2C18C/5E9E22A2" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9E22A2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12650 9500 50  0001 C CNN
F 1 "GND" H 12650 9600 50  0000 C CNN
F 2 "" H 12650 9750 50  0001 C CNN
F 3 "" H 12650 9750 50  0001 C CNN
	1    12650 9750
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 8650 14625 8650
Text Label 15975 8750 2    50   ~ 0
~NMI
Wire Wire Line
	15975 8750 15825 8750
Text Label 10875 9250 0    50   ~ 0
CS_U7_Pulse
Wire Wire Line
	10875 9250 11375 9250
Wire Wire Line
	11850 7800 12450 7800
Text Label 11850 7800 0    50   ~ 0
~FFFA_Pulse
Wire Wire Line
	12375 7400 13925 7400
Wire Wire Line
	12375 8000 12450 8000
Wire Wire Line
	12375 7400 12375 8000
Wire Wire Line
	13925 7400 13925 7800
Wire Wire Line
	13050 7900 13250 7900
Connection ~ 13925 7800
Wire Wire Line
	13850 7800 13925 7800
Text Label 13000 8000 0    50   ~ 0
DL6
Text Label 13000 8100 0    50   ~ 0
DL3
Text Label 13000 8200 0    50   ~ 0
DL2
Text Notes 14825 8100 2    50   ~ 0
F8C8-F8CF
Text Label 14150 7900 0    50   ~ 0
~CS_U16
Text Notes 12525 9650 2    50   ~ 0
F8D0-F8DF
Text Notes 13525 8850 0    50   ~ 0
P17/U13
Wire Wire Line
	13150 8850 13525 8850
Wire Wire Line
	13800 8750 13150 8750
Wire Wire Line
	11975 9250 12150 9250
Text Label 11850 9450 0    50   ~ 0
~RSTbb
Wire Wire Line
	12150 9450 11850 9450
Wire Wire Line
	11850 8850 12150 8850
Wire Wire Line
	11850 8650 12150 8650
Text Label 11850 8850 0    50   ~ 0
A0
Wire Wire Line
	11850 8950 12150 8950
Text Label 11850 9050 0    50   ~ 0
A2
Wire Wire Line
	11850 9050 12150 9050
Text Label 11850 8950 0    50   ~ 0
A1
Text Label 11850 8650 0    50   ~ 0
A3
$Comp
L 74xx:74LS259 U7
U 1 1 5F1DF88C
P 12650 9050
F 0 "U7" H 12300 9700 50  0000 C CNN
F 1 "74LS259" H 12425 9625 50  0000 C CNN
F 2 "" H 12650 9050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 12650 9050 50  0001 C CNN
	1    12650 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 8000 13250 8000
Text Label 13000 7800 0    50   ~ 0
DL5
Text Label 13000 7700 0    50   ~ 0
DL7
Text Label 13000 7600 0    50   ~ 0
DL1
Text Label 13000 7500 0    50   ~ 0
DL0
Wire Wire Line
	13250 8200 13000 8200
Wire Wire Line
	13250 8100 13000 8100
Wire Wire Line
	13250 7800 13000 7800
Wire Wire Line
	13250 7700 13000 7700
Wire Wire Line
	13250 7600 13000 7600
Wire Wire Line
	13250 7500 13000 7500
$Comp
L 74xx:74LS30 U19
U 1 1 5F19CF3B
P 13550 7800
F 0 "U19" H 13550 8025 50  0000 C CNN
F 1 "74LS30" H 13550 7800 50  0000 C CNN
F 2 "" H 13550 7800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS30" H 13550 7800 50  0001 C CNN
	1    13550 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	13925 7800 14425 7800
Wire Wire Line
	13925 8850 13925 7800
Wire Wire Line
	14625 8850 13925 8850
$Comp
L 74xx:74LS08 U25
U 1 1 5F17EABC
P 14925 8750
F 0 "U25" H 14925 9075 50  0000 C CNN
F 1 "74LS08" H 14925 8984 50  0000 C CNN
F 2 "" H 14925 8750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 14925 8750 50  0001 C CNN
	1    14925 8750
	1    0    0    -1  
$EndComp
Text Label 14150 7600 0    50   ~ 0
DL7
Text Label 14150 7500 0    50   ~ 0
DL6
Text Label 14150 7400 0    50   ~ 0
DL5
Text Label 14150 7300 0    50   ~ 0
DL4
Text Label 14150 7200 0    50   ~ 0
DL3
Text Label 14150 7100 0    50   ~ 0
DL2
Text Label 14150 7000 0    50   ~ 0
DL1
Text Label 14150 6900 0    50   ~ 0
DL0
Wire Wire Line
	14425 7900 14150 7900
Wire Wire Line
	14425 7600 14150 7600
Wire Wire Line
	14425 7500 14150 7500
Wire Wire Line
	14425 7400 14150 7400
Wire Wire Line
	14425 7300 14150 7300
Wire Wire Line
	14425 7200 14150 7200
Wire Wire Line
	14425 7100 14150 7100
Wire Wire Line
	14425 7000 14150 7000
Wire Wire Line
	14425 6900 14150 6900
Text Label 15725 7600 2    50   ~ 0
D7
Text Label 15725 7500 2    50   ~ 0
D6
Text Label 15725 7400 2    50   ~ 0
D5
Text Label 15725 7300 2    50   ~ 0
D4
Text Label 15725 7200 2    50   ~ 0
D3
Text Label 15725 7100 2    50   ~ 0
D2
Text Label 15725 7000 2    50   ~ 0
D1
Text Label 15725 6900 2    50   ~ 0
D0
Wire Wire Line
	15425 7000 15725 7000
Wire Wire Line
	15425 6900 15725 6900
Wire Wire Line
	15425 7200 15725 7200
Wire Wire Line
	15425 7300 15725 7300
Wire Wire Line
	15425 7600 15725 7600
Wire Wire Line
	15425 7100 15725 7100
Wire Wire Line
	15425 7400 15725 7400
Wire Wire Line
	15425 7500 15725 7500
$Comp
L power:GND #PWR?
U 1 1 5F15EDD2
P 15100 6600
AR Path="/5EB2C18C/5F15EDD2" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F15EDD2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15100 6350 50  0001 C CNN
F 1 "GND" H 15100 6450 50  0000 C CNN
F 2 "" H 15100 6600 50  0001 C CNN
F 3 "" H 15100 6600 50  0001 C CNN
	1    15100 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	15100 6375 15100 6400
Connection ~ 14925 6375
Wire Wire Line
	14925 6375 15100 6375
$Comp
L Device:C_Small C?
U 1 1 5F15EDC2
P 15100 6500
AR Path="/5EB2C18C/5F15EDC2" Ref="C?"  Part="1" 
AR Path="/5E890F69/5F15EDC2" Ref="C?"  Part="1" 
F 0 "C?" H 15192 6546 50  0000 L CNN
F 1 "22n" H 15192 6455 50  0000 L CNN
F 2 "" H 15100 6500 50  0001 C CNN
F 3 "~" H 15100 6500 50  0001 C CNN
	1    15100 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	14925 6375 14925 6600
Wire Wire Line
	14925 6350 14925 6375
$Comp
L power:GND #PWR?
U 1 1 5F15D31E
P 14925 8200
AR Path="/5EB2C18C/5F15D31E" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5F15D31E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14925 7950 50  0001 C CNN
F 1 "GND" H 14925 8050 50  0000 C CNN
F 2 "" H 14925 8200 50  0001 C CNN
F 3 "" H 14925 8200 50  0001 C CNN
	1    14925 8200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS374 U16
U 1 1 5F15CC04
P 14925 7400
F 0 "U16" H 14575 8175 50  0000 C CNN
F 1 "74LS374" H 14675 8075 50  0000 C CNN
F 2 "" H 14925 7400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS374" H 14925 7400 50  0001 C CNN
	1    14925 7400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U26
U 4 1 5F13A1EF
P 12750 7900
F 0 "U26" H 12750 8225 50  0000 C CNN
F 1 "74LS00" H 12750 8134 50  0000 C CNN
F 2 "" H 12750 7900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 12750 7900 50  0001 C CNN
	4    12750 7900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS06 U27
U 3 1 5F124F3A
P 11675 9250
F 0 "U27" H 11675 9567 50  0000 C CNN
F 1 "74LS06" H 11675 9476 50  0000 C CNN
F 2 "" H 11675 9250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 11675 9250 50  0001 C CNN
	3    11675 9250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS06 U27
U 2 1 5F124856
P 15525 8750
F 0 "U27" H 15525 9067 50  0000 C CNN
F 1 "74LS06" H 15525 8976 50  0000 C CNN
F 2 "" H 15525 8750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS06" H 15525 8750 50  0001 C CNN
	2    15525 8750
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 10700 11100 10700
Wire Wire Line
	11100 10700 11100 10900
Wire Wire Line
	11150 10900 11100 10900
Connection ~ 11100 10900
Wire Wire Line
	11100 10900 11100 10975
$Comp
L power:GND #PWR?
U 1 1 5EA8082D
P 11100 10975
AR Path="/5EB2C18C/5EA8082D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EA8082D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11100 10725 50  0001 C CNN
F 1 "GND" H 11100 10825 50  0000 C CNN
F 2 "" H 11100 10975 50  0001 C CNN
F 3 "" H 11100 10975 50  0001 C CNN
	1    11100 10975
	1    0    0    -1  
$EndComp
Text Label 3125 3850 2    50   ~ 0
3B
Text Label 3125 3750 2    50   ~ 0
4B
Text Label 3125 3650 2    50   ~ 0
5B
Text Label 3125 3550 2    50   ~ 0
6B
Text Label 13425 9250 2    50   ~ 0
4B
Text Label 13425 9150 2    50   ~ 0
5B
Text Label 13425 9350 2    50   ~ 0
3B
Text Label 13425 9050 2    50   ~ 0
6B
Wire Wire Line
	13150 9050 13425 9050
Wire Wire Line
	13150 9150 13425 9150
Wire Wire Line
	13150 9250 13425 9250
Wire Wire Line
	13150 9350 13425 9350
NoConn ~ 13150 8950
Text Label 3775 6150 0    50   ~ 0
D0
Text Label 3775 6850 0    50   ~ 0
D1
Text Label 3775 6250 0    50   ~ 0
D2
Text Label 3775 6750 0    50   ~ 0
D3
Text Label 3775 6650 0    50   ~ 0
D4
Text Label 3775 6350 0    50   ~ 0
D5
Text Label 3775 6550 0    50   ~ 0
D6
Text Label 3775 6450 0    50   ~ 0
D7
Text Label 2325 8750 0    50   ~ 0
D0
Text Label 2325 9050 0    50   ~ 0
D7
Text Label 2325 9250 0    50   ~ 0
D4
Text Label 2325 8950 0    50   ~ 0
D5
Text Label 2325 9150 0    50   ~ 0
D6
Text Label 2325 8850 0    50   ~ 0
D2
Text Label 2325 9450 0    50   ~ 0
D1
Text Label 2325 9350 0    50   ~ 0
D3
Text Label 13800 8750 2    50   ~ 0
LED_IDLE
Text Notes 5375 1750 2    50   ~ 0
fixed IRQ6
Text Notes 7225 2200 0    50   ~ 0
<-
Text Notes 8225 2900 0    50   ~ 0
->
Text Notes 8225 3000 0    50   ~ 0
->
Text Notes 7625 3100 0    50   ~ 0
->
Text Notes 7625 2500 0    50   ~ 0
<-
Text Notes 7625 2600 0    50   ~ 0
<-
Text Notes 7625 2700 0    50   ~ 0
<-
Text Notes 7625 2800 0    50   ~ 0
<-
Text Notes 7625 3200 0    50   ~ 0
->
Text Label 3025 3950 0    50   ~ 0
2B
Text HLabel 4100 2925 0    50   Input ~ 0
2B
Text HLabel 4100 3025 0    50   Input ~ 0
~USR7
Wire Wire Line
	4100 3025 4400 3025
Text Label 4400 3025 2    50   ~ 0
~USR7
Text Label 4375 2925 2    50   ~ 0
2B
Wire Wire Line
	4375 2925 4100 2925
Text HLabel 4100 3125 0    50   Input ~ 0
~RSTb
Wire Wire Line
	4100 3125 4400 3125
Text Label 4400 3125 2    50   ~ 0
~RSTb
Text Notes 5225 10700 0    50   ~ 0
61Hz
Text Notes 8325 2200 0    50   ~ 0
50Hz
Text Notes 8775 9575 0    50   ~ 0
PB3 is low @ SINGLE
Text Notes 9750 10450 0    50   ~ 0
There is a 23us /RST Pulse\nif switched from MULTI to\nSINGLE and a 78us Pulse from \nSINGLE to MULTI
Wire Notes Line
	9525 10475 9525 10075
Wire Notes Line
	9525 10075 9500 10175
Wire Notes Line
	9525 10075 9550 10175
Wire Notes Line
	11050 10475 11050 10125
Wire Notes Line
	9525 10475 11050 10475
Wire Notes Line
	11050 10125 10900 10125
Wire Notes Line
	10900 10125 10900 9975
Wire Notes Line
	10900 9975 10875 10075
Wire Notes Line
	10900 9975 10925 10075
$EndSCHEMATC
