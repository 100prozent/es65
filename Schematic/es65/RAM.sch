EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 16
Title "es65 RAM Board"
Date "2020-03-31"
Rev "v0.1"
Comp "Offner Robert"
Comment1 "v0.1: Initial"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E83E00F
P 7450 2075
AR Path="/5E83BCE9/5E83E00F" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E83E00F" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E83E00F" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E83E00F" Ref="U?"  Part="1" 
F 0 "U?" H 7075 3100 50  0000 C CNN
F 1 "TMM2016" H 7200 3025 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 7450 2075 50  0001 C CNN
F 3 "" H 7450 2075 50  0001 C CNN
	1    7450 2075
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E8413D8
P 7575 875
AR Path="/5E83BCE9/5E8413D8" Ref="C?"  Part="1" 
AR Path="/5EA29723/5E8413D8" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5E8413D8" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5E8413D8" Ref="C?"  Part="1" 
F 0 "C?" H 7667 921 50  0000 L CNN
F 1 "C_Small" H 7667 830 50  0000 L CNN
F 2 "" H 7575 875 50  0001 C CNN
F 3 "~" H 7575 875 50  0001 C CNN
	1    7575 875 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EB2C5CE
P 7575 975
AR Path="/5E83BCE9/5EB2C5CE" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5CE" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5CE" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5CE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7575 725 50  0001 C CNN
F 1 "GND" H 7575 825 50  0000 C CNN
F 2 "" H 7575 975 50  0001 C CNN
F 3 "" H 7575 975 50  0001 C CNN
	1    7575 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7575 775  7575 750 
Wire Wire Line
	7575 750  7450 750 
Wire Wire Line
	7450 750  7450 1075
Wire Wire Line
	7450 750  7450 700 
Connection ~ 7450 750 
$Comp
L power:+5V #PWR?
U 1 1 5EAA2306
P 7450 700
AR Path="/5E83BCE9/5EAA2306" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2306" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2306" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2306" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 550 50  0001 C CNN
F 1 "+5V" H 7465 873 50  0000 C CNN
F 2 "" H 7450 700 50  0001 C CNN
F 3 "" H 7450 700 50  0001 C CNN
	1    7450 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EB2C5D0
P 7450 2975
AR Path="/5E83BCE9/5EB2C5D0" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5D0" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5D0" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5D0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 2725 50  0001 C CNN
F 1 "GND" H 7450 2825 50  0000 C CNN
F 2 "" H 7450 2975 50  0001 C CNN
F 3 "" H 7450 2975 50  0001 C CNN
	1    7450 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1275 8050 1275
Wire Wire Line
	7850 1375 8050 1375
Wire Wire Line
	7850 1475 8050 1475
Wire Wire Line
	7850 1575 8050 1575
Wire Wire Line
	7850 1675 8050 1675
Wire Wire Line
	7850 1775 8050 1775
Wire Wire Line
	7850 1875 8050 1875
Wire Wire Line
	7850 1975 8050 1975
Wire Wire Line
	6850 1875 7050 1875
Wire Wire Line
	6850 1375 7050 1375
Wire Wire Line
	6850 1675 7050 1675
Wire Wire Line
	6850 1975 7050 1975
Wire Wire Line
	6850 1575 7050 1575
Wire Wire Line
	6850 1775 7050 1775
Wire Wire Line
	6850 1475 7050 1475
Wire Wire Line
	6850 1275 7050 1275
Wire Wire Line
	6850 2075 7050 2075
Wire Wire Line
	6850 2175 7050 2175
Wire Wire Line
	6850 2275 7050 2275
Wire Wire Line
	6850 2425 7050 2425
Wire Wire Line
	6850 2525 7050 2525
Wire Wire Line
	6850 2625 7050 2625
Text Label 8050 1275 2    50   ~ 0
D0
Text Label 8050 1375 2    50   ~ 0
D1
Text Label 8050 1475 2    50   ~ 0
D2
Text Label 8050 1575 2    50   ~ 0
D3
Text Label 8050 1675 2    50   ~ 0
D4
Text Label 8050 1775 2    50   ~ 0
D5
Text Label 8050 1875 2    50   ~ 0
D6
Text Label 8050 1975 2    50   ~ 0
D7
Text Label 6850 1275 0    50   ~ 0
A0
Text Label 6850 1375 0    50   ~ 0
A1
Text Label 6850 1475 0    50   ~ 0
A2
Text Label 6850 1575 0    50   ~ 0
A3
Text Label 6850 1675 0    50   ~ 0
A4
Text Label 6850 1775 0    50   ~ 0
A5
Text Label 6850 1875 0    50   ~ 0
A6
Text Label 6850 1975 0    50   ~ 0
A7
Text Label 6850 2075 0    50   ~ 0
A8
Text Label 6850 2175 0    50   ~ 0
A9
Text Label 6850 2275 0    50   ~ 0
A10
$Comp
L 74xx:74HC245 U3
U 1 1 5EAA2308
P 5600 1525
AR Path="/5E83BCE9/5EAA2308" Ref="U3"  Part="1" 
AR Path="/5EA29723/5EAA2308" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA2308" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA2308" Ref="U?"  Part="1" 
F 0 "U3" H 5200 2250 50  0000 C CNN
F 1 "74HC245" H 5325 2175 50  0000 C CNN
F 2 "" H 5600 1525 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 5600 1525 50  0001 C CNN
	1    5600 1525
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA230F
P 5600 2325
AR Path="/5E83BCE9/5EAA230F" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA230F" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA230F" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA230F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 2075 50  0001 C CNN
F 1 "GND" H 5600 2175 50  0000 C CNN
F 2 "" H 5600 2325 50  0001 C CNN
F 3 "" H 5600 2325 50  0001 C CNN
	1    5600 2325
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA2310
P 5600 725
AR Path="/5E83BCE9/5EAA2310" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2310" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2310" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2310" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 575 50  0001 C CNN
F 1 "+5V" H 5615 898 50  0000 C CNN
F 2 "" H 5600 725 50  0001 C CNN
F 3 "" H 5600 725 50  0001 C CNN
	1    5600 725 
	1    0    0    -1  
$EndComp
Text Label 6300 1725 2    50   ~ 0
D7
Text Label 6300 1525 2    50   ~ 0
D5
Text Label 6300 1625 2    50   ~ 0
D6
Wire Wire Line
	6100 1625 6300 1625
Text Label 6300 1425 2    50   ~ 0
D4
Text Label 6300 1325 2    50   ~ 0
D3
Text Label 6300 1225 2    50   ~ 0
D2
Text Label 6300 1125 2    50   ~ 0
D1
Wire Wire Line
	6100 1125 6300 1125
Wire Wire Line
	6100 1425 6300 1425
Wire Wire Line
	6100 1725 6300 1725
Wire Wire Line
	6100 1325 6300 1325
Text Label 6300 1025 2    50   ~ 0
D0
Wire Wire Line
	6100 1525 6300 1525
Wire Wire Line
	6100 1225 6300 1225
Wire Wire Line
	6100 1025 6300 1025
$Comp
L es65-rescue:6331-my_ics U14
U 1 1 5EAA22FD
P 12575 1575
AR Path="/5E83BCE9/5EAA22FD" Ref="U14"  Part="1" 
AR Path="/5EA29723/5EAA22FD" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA22FD" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA22FD" Ref="U?"  Part="1" 
F 0 "U14" H 12275 2100 50  0000 C CNN
F 1 "6331" H 12325 2025 50  0000 C CNN
F 2 "" H 12275 1675 50  0001 C CNN
F 3 "" H 12275 1675 50  0001 C CNN
	1    12575 1575
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC245 U2
U 1 1 5E83EF16
P 5825 3250
AR Path="/5E83BCE9/5E83EF16" Ref="U2"  Part="1" 
AR Path="/5EA29723/5E83EF16" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E83EF16" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E83EF16" Ref="U?"  Part="1" 
F 0 "U2" H 5425 3975 50  0000 C CNN
F 1 "74HC245" H 5550 3900 50  0000 C CNN
F 2 "" H 5825 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 5825 3250 50  0001 C CNN
	1    5825 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EB2C5C8
P 5825 4050
AR Path="/5E83BCE9/5EB2C5C8" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5C8" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5C8" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5C8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5825 3800 50  0001 C CNN
F 1 "GND" H 5825 3900 50  0000 C CNN
F 2 "" H 5825 4050 50  0001 C CNN
F 3 "" H 5825 4050 50  0001 C CNN
	1    5825 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB2C5C9
P 5825 2450
AR Path="/5E83BCE9/5EB2C5C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5C9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5825 2300 50  0001 C CNN
F 1 "+5V" H 5840 2623 50  0000 C CNN
F 2 "" H 5825 2450 50  0001 C CNN
F 3 "" H 5825 2450 50  0001 C CNN
	1    5825 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6325 3350 6525 3350
Wire Wire Line
	6325 2850 6525 2850
Wire Wire Line
	6325 3150 6525 3150
Wire Wire Line
	6325 3450 6525 3450
Wire Wire Line
	6325 3050 6525 3050
Wire Wire Line
	6325 3250 6525 3250
Wire Wire Line
	6325 2950 6525 2950
Wire Wire Line
	6325 2750 6525 2750
Wire Wire Line
	6525 4600 6725 4600
Wire Wire Line
	6525 5000 6725 5000
Wire Wire Line
	6525 4900 6725 4900
$Comp
L 74xx:74HC245 U1
U 1 1 5EB2C5CA
P 6025 5000
AR Path="/5E83BCE9/5EB2C5CA" Ref="U1"  Part="1" 
AR Path="/5EA29723/5EB2C5CA" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EB2C5CA" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EB2C5CA" Ref="U?"  Part="1" 
F 0 "U1" H 5625 5725 50  0000 C CNN
F 1 "74HC245" H 5750 5650 50  0000 C CNN
F 2 "" H 6025 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 6025 5000 50  0001 C CNN
	1    6025 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6525 5100 6725 5100
Wire Wire Line
	6525 5200 6725 5200
Wire Wire Line
	6525 4700 6725 4700
Wire Wire Line
	6525 4500 6725 4500
Wire Wire Line
	6525 4800 6725 4800
$Comp
L power:+5V #PWR?
U 1 1 5EB2C5CB
P 6025 4200
AR Path="/5E83BCE9/5EB2C5CB" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5CB" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5CB" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5CB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6025 4050 50  0001 C CNN
F 1 "+5V" H 6040 4373 50  0000 C CNN
F 2 "" H 6025 4200 50  0001 C CNN
F 3 "" H 6025 4200 50  0001 C CNN
	1    6025 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA2303
P 6025 5800
AR Path="/5E83BCE9/5EAA2303" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2303" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2303" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2303" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6025 5550 50  0001 C CNN
F 1 "GND" H 6025 5650 50  0000 C CNN
F 2 "" H 6025 5800 50  0001 C CNN
F 3 "" H 6025 5800 50  0001 C CNN
	1    6025 5800
	1    0    0    -1  
$EndComp
Text Label 6525 3350 2    50   ~ 0
A14
Text Label 6525 2950 2    50   ~ 0
A10
Text Label 6725 4600 2    50   ~ 0
A1
Text Label 6725 4900 2    50   ~ 0
A4
Text Label 6725 4700 2    50   ~ 0
A2
Text Label 6725 5200 2    50   ~ 0
A7
Text Label 6525 3050 2    50   ~ 0
A11
Text Label 6525 2750 2    50   ~ 0
A8
Text Label 6725 5100 2    50   ~ 0
A6
Text Label 6525 3150 2    50   ~ 0
A12
Text Label 6525 2850 2    50   ~ 0
A9
Text Label 6525 3250 2    50   ~ 0
A13
Text Label 6725 4800 2    50   ~ 0
A3
Text Label 6725 5000 2    50   ~ 0
A5
Text Label 6725 4500 2    50   ~ 0
A0
Text Label 6525 3450 2    50   ~ 0
A15
Text Label 6850 2425 0    50   ~ 0
~WE
Text Label 6850 2625 0    50   ~ 0
~OE
Text Label 6850 2525 0    50   ~ 0
~CE1
Text Label 8100 2525 0    50   ~ 0
~CE2
Text Label 9350 2525 0    50   ~ 0
~CE3
Text Label 10600 2525 0    50   ~ 0
~CE4
Text Label 6825 5200 0    50   ~ 0
~CE5
Text Label 8075 5200 0    50   ~ 0
~CE6
Text Label 9325 5200 0    50   ~ 0
~CE7
Text Label 10575 5200 0    50   ~ 0
~CE8
Text Label 6800 7875 0    50   ~ 0
~CE9
Text Label 8050 7875 0    50   ~ 0
~CE10
Text Label 9300 7875 0    50   ~ 0
~CE11
Text Label 10550 7875 0    50   ~ 0
~CE12
Text Label 6775 10550 0    50   ~ 0
~CE13
$Comp
L power:+5V #PWR?
U 1 1 5EAA2309
P 12575 1075
AR Path="/5E83BCE9/5EAA2309" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2309" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2309" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2309" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12575 925 50  0001 C CNN
F 1 "+5V" H 12590 1248 50  0000 C CNN
F 2 "" H 12575 1075 50  0001 C CNN
F 3 "" H 12575 1075 50  0001 C CNN
	1    12575 1075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E846939
P 12575 2175
AR Path="/5E83BCE9/5E846939" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E846939" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E846939" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E846939" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12575 1925 50  0001 C CNN
F 1 "GND" H 12575 2025 50  0000 C CNN
F 2 "" H 12575 2175 50  0001 C CNN
F 3 "" H 12575 2175 50  0001 C CNN
	1    12575 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	8825 775  8825 750 
Wire Wire Line
	8825 750  8700 750 
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E847B5C
P 8700 2075
AR Path="/5E83BCE9/5E847B5C" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E847B5C" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E847B5C" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E847B5C" Ref="U?"  Part="1" 
F 0 "U?" H 8325 3100 50  0000 C CNN
F 1 "TMM2016" H 8450 3025 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 8700 2075 50  0001 C CNN
F 3 "" H 8700 2075 50  0001 C CNN
	1    8700 2075
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EB2C5D4
P 8825 875
AR Path="/5E83BCE9/5EB2C5D4" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EB2C5D4" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EB2C5D4" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EB2C5D4" Ref="C?"  Part="1" 
F 0 "C?" H 8917 921 50  0000 L CNN
F 1 "C_Small" H 8917 830 50  0000 L CNN
F 2 "" H 8825 875 50  0001 C CNN
F 3 "~" H 8825 875 50  0001 C CNN
	1    8825 875 
	1    0    0    -1  
$EndComp
Connection ~ 8700 750 
$Comp
L power:GND #PWR?
U 1 1 5EB2C5D5
P 8825 975
AR Path="/5E83BCE9/5EB2C5D5" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5D5" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5D5" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5D5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8825 725 50  0001 C CNN
F 1 "GND" H 8825 825 50  0000 C CNN
F 2 "" H 8825 975 50  0001 C CNN
F 3 "" H 8825 975 50  0001 C CNN
	1    8825 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 750  8700 1075
Wire Wire Line
	8700 750  8700 700 
$Comp
L power:+5V #PWR?
U 1 1 5EB2C5D6
P 8700 700
AR Path="/5E83BCE9/5EB2C5D6" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5D6" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5D6" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5D6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8700 550 50  0001 C CNN
F 1 "+5V" H 8715 873 50  0000 C CNN
F 2 "" H 8700 700 50  0001 C CNN
F 3 "" H 8700 700 50  0001 C CNN
	1    8700 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA230E
P 8700 2975
AR Path="/5E83BCE9/5EAA230E" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA230E" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA230E" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA230E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8700 2725 50  0001 C CNN
F 1 "GND" H 8700 2825 50  0000 C CNN
F 2 "" H 8700 2975 50  0001 C CNN
F 3 "" H 8700 2975 50  0001 C CNN
	1    8700 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1275 9300 1275
Wire Wire Line
	9100 1375 9300 1375
Wire Wire Line
	9100 1475 9300 1475
Wire Wire Line
	9100 1575 9300 1575
Wire Wire Line
	9100 1675 9300 1675
Wire Wire Line
	9100 1775 9300 1775
Wire Wire Line
	9100 1875 9300 1875
Wire Wire Line
	9100 1975 9300 1975
Wire Wire Line
	8100 1875 8300 1875
Wire Wire Line
	8100 1375 8300 1375
Wire Wire Line
	8100 1675 8300 1675
Wire Wire Line
	8100 1975 8300 1975
Wire Wire Line
	8100 1575 8300 1575
Wire Wire Line
	8100 1775 8300 1775
Wire Wire Line
	8100 1475 8300 1475
Wire Wire Line
	8100 1275 8300 1275
Wire Wire Line
	8100 2075 8300 2075
Wire Wire Line
	8100 2175 8300 2175
Wire Wire Line
	8100 2275 8300 2275
Wire Wire Line
	8100 2425 8300 2425
Wire Wire Line
	8100 2525 8300 2525
Wire Wire Line
	8100 2625 8300 2625
Text Label 9300 1275 2    50   ~ 0
D0
Text Label 9300 1375 2    50   ~ 0
D1
Text Label 9300 1475 2    50   ~ 0
D2
Text Label 9300 1575 2    50   ~ 0
D3
Text Label 9300 1675 2    50   ~ 0
D4
Text Label 9300 1775 2    50   ~ 0
D5
Text Label 9300 1875 2    50   ~ 0
D6
Text Label 9300 1975 2    50   ~ 0
D7
Text Label 8100 1275 0    50   ~ 0
A0
Text Label 8100 1375 0    50   ~ 0
A1
Text Label 8100 1475 0    50   ~ 0
A2
Text Label 8100 1575 0    50   ~ 0
A3
Text Label 8100 1675 0    50   ~ 0
A4
Text Label 8100 1775 0    50   ~ 0
A5
Text Label 8100 1875 0    50   ~ 0
A6
Text Label 8100 1975 0    50   ~ 0
A7
Text Label 8100 2075 0    50   ~ 0
A8
Text Label 8100 2175 0    50   ~ 0
A9
Text Label 8100 2275 0    50   ~ 0
A10
Text Label 8100 2625 0    50   ~ 0
~OE
Text Label 8100 2425 0    50   ~ 0
~WE
Text Label 10600 2175 0    50   ~ 0
A9
Text Label 10600 2275 0    50   ~ 0
A10
Wire Wire Line
	10075 775  10075 750 
Wire Wire Line
	10075 750  9950 750 
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E851891
P 9950 2075
AR Path="/5E83BCE9/5E851891" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E851891" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E851891" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E851891" Ref="U?"  Part="1" 
F 0 "U?" H 9575 3100 50  0000 C CNN
F 1 "TMM2016" H 9700 3025 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 9950 2075 50  0001 C CNN
F 3 "" H 9950 2075 50  0001 C CNN
	1    9950 2075
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EAA2311
P 10075 875
AR Path="/5E83BCE9/5EAA2311" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2311" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2311" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2311" Ref="C?"  Part="1" 
F 0 "C?" H 10167 921 50  0000 L CNN
F 1 "C_Small" H 10167 830 50  0000 L CNN
F 2 "" H 10075 875 50  0001 C CNN
F 3 "~" H 10075 875 50  0001 C CNN
	1    10075 875 
	1    0    0    -1  
$EndComp
Connection ~ 9950 750 
$Comp
L power:GND #PWR?
U 1 1 5EAA2312
P 10075 975
AR Path="/5E83BCE9/5EAA2312" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2312" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2312" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2312" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10075 725 50  0001 C CNN
F 1 "GND" H 10075 825 50  0000 C CNN
F 2 "" H 10075 975 50  0001 C CNN
F 3 "" H 10075 975 50  0001 C CNN
	1    10075 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 750  9950 1075
Wire Wire Line
	9950 750  9950 700 
$Comp
L power:+5V #PWR?
U 1 1 5EAA2313
P 9950 700
AR Path="/5E83BCE9/5EAA2313" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2313" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2313" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2313" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9950 550 50  0001 C CNN
F 1 "+5V" H 9965 873 50  0000 C CNN
F 2 "" H 9950 700 50  0001 C CNN
F 3 "" H 9950 700 50  0001 C CNN
	1    9950 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E8518C9
P 9950 2975
AR Path="/5E83BCE9/5E8518C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8518C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8518C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8518C9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9950 2725 50  0001 C CNN
F 1 "GND" H 9950 2825 50  0000 C CNN
F 2 "" H 9950 2975 50  0001 C CNN
F 3 "" H 9950 2975 50  0001 C CNN
	1    9950 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 1275 10550 1275
Wire Wire Line
	10350 1375 10550 1375
Wire Wire Line
	10350 1475 10550 1475
Wire Wire Line
	10350 1575 10550 1575
Wire Wire Line
	10350 1675 10550 1675
Wire Wire Line
	10350 1775 10550 1775
Wire Wire Line
	10350 1875 10550 1875
Wire Wire Line
	10350 1975 10550 1975
Wire Wire Line
	9350 1875 9550 1875
Wire Wire Line
	9350 1375 9550 1375
Wire Wire Line
	9350 1675 9550 1675
Wire Wire Line
	9350 1975 9550 1975
Wire Wire Line
	9350 1575 9550 1575
Wire Wire Line
	9350 1775 9550 1775
Wire Wire Line
	9350 1475 9550 1475
Wire Wire Line
	9350 1275 9550 1275
Wire Wire Line
	9350 2075 9550 2075
Wire Wire Line
	9350 2175 9550 2175
Wire Wire Line
	9350 2275 9550 2275
Wire Wire Line
	9350 2425 9550 2425
Wire Wire Line
	9350 2525 9550 2525
Wire Wire Line
	9350 2625 9550 2625
Text Label 10550 1275 2    50   ~ 0
D0
Text Label 10550 1375 2    50   ~ 0
D1
Text Label 10550 1475 2    50   ~ 0
D2
Text Label 10550 1575 2    50   ~ 0
D3
Text Label 10550 1675 2    50   ~ 0
D4
Text Label 10550 1775 2    50   ~ 0
D5
Text Label 10550 1875 2    50   ~ 0
D6
Text Label 10550 1975 2    50   ~ 0
D7
Text Label 9350 1275 0    50   ~ 0
A0
Text Label 9350 1375 0    50   ~ 0
A1
Text Label 9350 1475 0    50   ~ 0
A2
Text Label 9350 1575 0    50   ~ 0
A3
Text Label 9350 1675 0    50   ~ 0
A4
Text Label 9350 1775 0    50   ~ 0
A5
Text Label 9350 1875 0    50   ~ 0
A6
Text Label 9350 1975 0    50   ~ 0
A7
Text Label 9350 2075 0    50   ~ 0
A8
Text Label 9350 2175 0    50   ~ 0
A9
Text Label 9350 2275 0    50   ~ 0
A10
Wire Wire Line
	11325 750  11200 750 
Text Label 10600 1775 0    50   ~ 0
A5
Text Label 10600 2625 0    50   ~ 0
~OE
Wire Wire Line
	11200 750  11200 1075
Wire Wire Line
	11600 1375 11800 1375
Wire Wire Line
	11600 1975 11800 1975
Wire Wire Line
	10600 1475 10800 1475
Wire Wire Line
	10600 1975 10800 1975
Wire Wire Line
	10600 1875 10800 1875
Wire Wire Line
	10600 1675 10800 1675
Wire Wire Line
	10600 2625 10800 2625
Wire Wire Line
	10600 2075 10800 2075
Wire Wire Line
	10600 2425 10800 2425
Wire Wire Line
	11600 1675 11800 1675
Text Label 11800 1675 2    50   ~ 0
D4
Text Label 10600 1275 0    50   ~ 0
A0
Text Label 10600 2425 0    50   ~ 0
~WE
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E851928
P 11200 2075
AR Path="/5E83BCE9/5E851928" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E851928" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E851928" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E851928" Ref="U?"  Part="1" 
F 0 "U?" H 10825 3100 50  0000 C CNN
F 1 "TMM2016" H 10950 3025 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 11200 2075 50  0001 C CNN
F 3 "" H 11200 2075 50  0001 C CNN
	1    11200 2075
	1    0    0    -1  
$EndComp
Text Label 10600 2075 0    50   ~ 0
A8
Wire Wire Line
	11600 1475 11800 1475
Wire Wire Line
	10600 1375 10800 1375
Text Label 10600 1875 0    50   ~ 0
A6
Wire Wire Line
	11600 1775 11800 1775
Wire Wire Line
	11325 775  11325 750 
Wire Wire Line
	10600 2525 10800 2525
Wire Wire Line
	11600 1275 11800 1275
Text Label 11800 1275 2    50   ~ 0
D0
Text Label 10600 1475 0    50   ~ 0
A2
Wire Wire Line
	10600 2275 10800 2275
Wire Wire Line
	11600 1875 11800 1875
$Comp
L power:GND #PWR?
U 1 1 5E851941
P 11200 2975
AR Path="/5E83BCE9/5E851941" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E851941" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E851941" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E851941" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11200 2725 50  0001 C CNN
F 1 "GND" H 11200 2825 50  0000 C CNN
F 2 "" H 11200 2975 50  0001 C CNN
F 3 "" H 11200 2975 50  0001 C CNN
	1    11200 2975
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E85194E
P 11325 975
AR Path="/5E83BCE9/5E85194E" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E85194E" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E85194E" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E85194E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11325 725 50  0001 C CNN
F 1 "GND" H 11325 825 50  0000 C CNN
F 2 "" H 11325 975 50  0001 C CNN
F 3 "" H 11325 975 50  0001 C CNN
	1    11325 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	11600 1575 11800 1575
Text Label 10600 1575 0    50   ~ 0
A3
Text Label 11800 1375 2    50   ~ 0
D1
$Comp
L power:+5V #PWR?
U 1 1 5E85195E
P 11200 700
AR Path="/5E83BCE9/5E85195E" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E85195E" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E85195E" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E85195E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11200 550 50  0001 C CNN
F 1 "+5V" H 11215 873 50  0000 C CNN
F 2 "" H 11200 700 50  0001 C CNN
F 3 "" H 11200 700 50  0001 C CNN
	1    11200 700 
	1    0    0    -1  
$EndComp
Text Label 11800 1975 2    50   ~ 0
D7
Wire Wire Line
	10600 1575 10800 1575
Text Label 11800 1775 2    50   ~ 0
D5
Text Label 11800 1475 2    50   ~ 0
D2
Text Label 11800 1875 2    50   ~ 0
D6
Wire Wire Line
	10600 2175 10800 2175
Text Label 9350 2625 0    50   ~ 0
~OE
Text Label 10600 1375 0    50   ~ 0
A1
Wire Wire Line
	10600 1775 10800 1775
Wire Wire Line
	11200 750  11200 700 
Text Label 10600 1675 0    50   ~ 0
A4
Text Label 9350 2425 0    50   ~ 0
~WE
Wire Wire Line
	10600 1275 10800 1275
$Comp
L Device:C_Small C?
U 1 1 5EAA2318
P 11325 875
AR Path="/5E83BCE9/5EAA2318" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2318" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2318" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2318" Ref="C?"  Part="1" 
F 0 "C?" H 11417 921 50  0000 L CNN
F 1 "C_Small" H 11417 830 50  0000 L CNN
F 2 "" H 11325 875 50  0001 C CNN
F 3 "~" H 11325 875 50  0001 C CNN
	1    11325 875 
	1    0    0    -1  
$EndComp
Text Label 11800 1575 2    50   ~ 0
D3
Text Label 10600 1975 0    50   ~ 0
A7
Connection ~ 11200 750 
Text Label 8075 4850 0    50   ~ 0
A9
Text Label 8075 4950 0    50   ~ 0
A10
Wire Wire Line
	7550 3450 7550 3425
Wire Wire Line
	7550 3425 7425 3425
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E86F285
P 7425 4750
AR Path="/5E83BCE9/5E86F285" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E86F285" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E86F285" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E86F285" Ref="U?"  Part="1" 
F 0 "U?" H 7050 5775 50  0000 C CNN
F 1 "TMM2016" H 7175 5700 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 7425 4750 50  0001 C CNN
F 3 "" H 7425 4750 50  0001 C CNN
	1    7425 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EAA2319
P 7550 3550
AR Path="/5E83BCE9/5EAA2319" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2319" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2319" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2319" Ref="C?"  Part="1" 
F 0 "C?" H 7642 3596 50  0000 L CNN
F 1 "C_Small" H 7642 3505 50  0000 L CNN
F 2 "" H 7550 3550 50  0001 C CNN
F 3 "~" H 7550 3550 50  0001 C CNN
	1    7550 3550
	1    0    0    -1  
$EndComp
Connection ~ 7425 3425
$Comp
L power:GND #PWR?
U 1 1 5E86F2A1
P 7550 3650
AR Path="/5E83BCE9/5E86F2A1" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F2A1" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F2A1" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F2A1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7550 3400 50  0001 C CNN
F 1 "GND" H 7550 3500 50  0000 C CNN
F 2 "" H 7550 3650 50  0001 C CNN
F 3 "" H 7550 3650 50  0001 C CNN
	1    7550 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7425 3425 7425 3750
Wire Wire Line
	7425 3425 7425 3375
$Comp
L power:+5V #PWR?
U 1 1 5EB2C5E4
P 7425 3375
AR Path="/5E83BCE9/5EB2C5E4" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5E4" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5E4" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5E4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7425 3225 50  0001 C CNN
F 1 "+5V" H 7440 3548 50  0000 C CNN
F 2 "" H 7425 3375 50  0001 C CNN
F 3 "" H 7425 3375 50  0001 C CNN
	1    7425 3375
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E86F2BD
P 7425 5650
AR Path="/5E83BCE9/5E86F2BD" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F2BD" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F2BD" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F2BD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7425 5400 50  0001 C CNN
F 1 "GND" H 7425 5500 50  0000 C CNN
F 2 "" H 7425 5650 50  0001 C CNN
F 3 "" H 7425 5650 50  0001 C CNN
	1    7425 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7825 3950 8025 3950
Wire Wire Line
	7825 4050 8025 4050
Wire Wire Line
	7825 4150 8025 4150
Wire Wire Line
	7825 4250 8025 4250
Wire Wire Line
	7825 4350 8025 4350
Wire Wire Line
	7825 4450 8025 4450
Wire Wire Line
	7825 4550 8025 4550
Wire Wire Line
	7825 4650 8025 4650
Wire Wire Line
	6825 4550 7025 4550
Wire Wire Line
	6825 4050 7025 4050
Wire Wire Line
	6825 4350 7025 4350
Wire Wire Line
	6825 4650 7025 4650
Wire Wire Line
	6825 4250 7025 4250
Wire Wire Line
	6825 4450 7025 4450
Wire Wire Line
	6825 4150 7025 4150
Wire Wire Line
	6825 3950 7025 3950
Wire Wire Line
	6825 4750 7025 4750
Wire Wire Line
	6825 4850 7025 4850
Wire Wire Line
	6825 4950 7025 4950
Wire Wire Line
	6825 5100 7025 5100
Wire Wire Line
	6825 5200 7025 5200
Wire Wire Line
	6825 5300 7025 5300
Text Label 8025 3950 2    50   ~ 0
D0
Text Label 8025 4050 2    50   ~ 0
D1
Text Label 8025 4150 2    50   ~ 0
D2
Text Label 8025 4250 2    50   ~ 0
D3
Text Label 8025 4350 2    50   ~ 0
D4
Text Label 8025 4450 2    50   ~ 0
D5
Text Label 8025 4550 2    50   ~ 0
D6
Text Label 8025 4650 2    50   ~ 0
D7
Text Label 6825 3950 0    50   ~ 0
A0
Text Label 6825 4050 0    50   ~ 0
A1
Text Label 6825 4150 0    50   ~ 0
A2
Text Label 6825 4250 0    50   ~ 0
A3
Text Label 6825 4350 0    50   ~ 0
A4
Text Label 6825 4450 0    50   ~ 0
A5
Text Label 6825 4550 0    50   ~ 0
A6
Text Label 6825 4650 0    50   ~ 0
A7
Text Label 6825 4750 0    50   ~ 0
A8
Text Label 6825 4850 0    50   ~ 0
A9
Text Label 6825 4950 0    50   ~ 0
A10
Connection ~ 9925 3425
Wire Wire Line
	8800 3425 8675 3425
Text Label 8075 4450 0    50   ~ 0
A5
Text Label 8075 5300 0    50   ~ 0
~OE
Wire Wire Line
	8675 3425 8675 3750
Wire Wire Line
	9075 4050 9275 4050
Wire Wire Line
	9075 4650 9275 4650
Wire Wire Line
	11175 3425 11175 3375
Wire Wire Line
	8075 4150 8275 4150
Wire Wire Line
	8075 4650 8275 4650
Wire Wire Line
	8075 4550 8275 4550
Wire Wire Line
	10575 5300 10775 5300
Wire Wire Line
	10575 4150 10775 4150
Text Label 10525 4150 2    50   ~ 0
D2
Wire Wire Line
	9925 3425 9925 3375
Wire Wire Line
	8075 4350 8275 4350
Wire Wire Line
	10575 4650 10775 4650
Wire Wire Line
	9325 4150 9525 4150
Wire Wire Line
	11575 4650 11775 4650
Text Label 9325 5100 0    50   ~ 0
~WE
Wire Wire Line
	9325 4750 9525 4750
Wire Wire Line
	8075 5300 8275 5300
Wire Wire Line
	8075 4750 8275 4750
Wire Wire Line
	8075 5100 8275 5100
Wire Wire Line
	9325 4050 9525 4050
Wire Wire Line
	9075 4350 9275 4350
Text Label 9275 4350 2    50   ~ 0
D4
Text Label 8075 3950 0    50   ~ 0
A0
Wire Wire Line
	9325 3950 9525 3950
Wire Wire Line
	9325 5100 9525 5100
Wire Wire Line
	9325 4850 9525 4850
Text Label 8075 5100 0    50   ~ 0
~WE
Text Label 10575 4550 0    50   ~ 0
A6
Wire Wire Line
	11575 4450 11775 4450
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E86F32D
P 8675 4750
AR Path="/5E83BCE9/5E86F32D" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E86F32D" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E86F32D" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E86F32D" Ref="U?"  Part="1" 
F 0 "U?" H 8300 5775 50  0000 C CNN
F 1 "TMM2016" H 8425 5700 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 8675 4750 50  0001 C CNN
F 3 "" H 8675 4750 50  0001 C CNN
	1    8675 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9325 4450 9525 4450
Wire Wire Line
	10325 3950 10525 3950
Text Label 10575 4650 0    50   ~ 0
A7
Text Label 8075 4750 0    50   ~ 0
A8
Wire Wire Line
	10325 4450 10525 4450
Text Label 11775 4250 2    50   ~ 0
D3
Wire Wire Line
	9075 4150 9275 4150
Wire Wire Line
	8075 4050 8275 4050
Text Label 10575 5100 0    50   ~ 0
~WE
Wire Wire Line
	10575 4050 10775 4050
Text Label 8075 4550 0    50   ~ 0
A6
Wire Wire Line
	10325 4550 10525 4550
Text Label 10575 4750 0    50   ~ 0
A8
$Comp
L Device:C_Small C?
U 1 1 5EAA231D
P 11300 3550
AR Path="/5E83BCE9/5EAA231D" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA231D" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA231D" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA231D" Ref="C?"  Part="1" 
F 0 "C?" H 11392 3596 50  0000 L CNN
F 1 "C_Small" H 11392 3505 50  0000 L CNN
F 2 "" H 11300 3550 50  0001 C CNN
F 3 "~" H 11300 3550 50  0001 C CNN
	1    11300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9075 4450 9275 4450
Wire Wire Line
	8800 3450 8800 3425
Wire Wire Line
	8075 5200 8275 5200
Text Label 10525 4050 2    50   ~ 0
D1
Text Label 10575 4050 0    50   ~ 0
A1
Wire Wire Line
	10575 4450 10775 4450
$Comp
L power:+5V #PWR?
U 1 1 5E86F35B
P 9925 3375
AR Path="/5E83BCE9/5E86F35B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F35B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F35B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F35B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9925 3225 50  0001 C CNN
F 1 "+5V" H 9940 3548 50  0000 C CNN
F 2 "" H 9925 3375 50  0001 C CNN
F 3 "" H 9925 3375 50  0001 C CNN
	1    9925 3375
	1    0    0    -1  
$EndComp
Wire Wire Line
	9075 3950 9275 3950
Text Label 11775 4350 2    50   ~ 0
D4
Wire Wire Line
	10325 4150 10525 4150
Text Label 9275 3950 2    50   ~ 0
D0
Text Label 8075 4150 0    50   ~ 0
A2
Wire Wire Line
	11300 3450 11300 3425
Wire Wire Line
	9325 4350 9525 4350
Wire Wire Line
	8075 4950 8275 4950
Text Label 10525 4450 2    50   ~ 0
D5
Text Label 10575 4450 0    50   ~ 0
A5
Text Label 9325 3950 0    50   ~ 0
A0
Wire Wire Line
	9325 4250 9525 4250
$Comp
L Device:C_Small C?
U 1 1 5E86F375
P 10050 3550
AR Path="/5E83BCE9/5E86F375" Ref="C?"  Part="1" 
AR Path="/5EA29723/5E86F375" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5E86F375" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5E86F375" Ref="C?"  Part="1" 
F 0 "C?" H 10142 3596 50  0000 L CNN
F 1 "C_Small" H 10142 3505 50  0000 L CNN
F 2 "" H 10050 3550 50  0001 C CNN
F 3 "~" H 10050 3550 50  0001 C CNN
	1    10050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 3450 10050 3425
Wire Wire Line
	9075 4550 9275 4550
Text Label 9325 4750 0    50   ~ 0
A8
$Comp
L power:GND #PWR?
U 1 1 5E86F385
P 8675 5650
AR Path="/5E83BCE9/5E86F385" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F385" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F385" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F385" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8675 5400 50  0001 C CNN
F 1 "GND" H 8675 5500 50  0000 C CNN
F 2 "" H 8675 5650 50  0001 C CNN
F 3 "" H 8675 5650 50  0001 C CNN
	1    8675 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E86F392
P 8800 3650
AR Path="/5E83BCE9/5E86F392" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F392" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F392" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F392" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8800 3400 50  0001 C CNN
F 1 "GND" H 8800 3500 50  0000 C CNN
F 2 "" H 8800 3650 50  0001 C CNN
F 3 "" H 8800 3650 50  0001 C CNN
	1    8800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10325 4350 10525 4350
Text Label 9325 4250 0    50   ~ 0
A3
Wire Wire Line
	9325 4550 9525 4550
Wire Wire Line
	10575 5100 10775 5100
Connection ~ 11175 3425
Text Label 9325 4650 0    50   ~ 0
A7
Wire Wire Line
	10325 4050 10525 4050
Wire Wire Line
	9075 4250 9275 4250
Wire Wire Line
	11575 4050 11775 4050
Text Label 11775 4550 2    50   ~ 0
D6
Wire Wire Line
	10575 4850 10775 4850
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E86F3C2
P 9925 4750
AR Path="/5E83BCE9/5E86F3C2" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E86F3C2" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E86F3C2" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E86F3C2" Ref="U?"  Part="1" 
F 0 "U?" H 9550 5775 50  0000 C CNN
F 1 "TMM2016" H 9675 5700 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 9925 4750 50  0001 C CNN
F 3 "" H 9925 4750 50  0001 C CNN
	1    9925 4750
	1    0    0    -1  
$EndComp
Text Label 8075 4250 0    50   ~ 0
A3
Text Label 9275 4050 2    50   ~ 0
D1
Text Label 10525 4550 2    50   ~ 0
D6
Wire Wire Line
	10575 4950 10775 4950
Wire Wire Line
	10325 4250 10525 4250
$Comp
L power:GND #PWR?
U 1 1 5E86F3D4
P 9925 5650
AR Path="/5E83BCE9/5E86F3D4" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F3D4" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F3D4" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F3D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9925 5400 50  0001 C CNN
F 1 "GND" H 9925 5500 50  0000 C CNN
F 2 "" H 9925 5650 50  0001 C CNN
F 3 "" H 9925 5650 50  0001 C CNN
	1    9925 5650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E86F3E1
P 8675 3375
AR Path="/5E83BCE9/5E86F3E1" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F3E1" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F3E1" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F3E1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8675 3225 50  0001 C CNN
F 1 "+5V" H 8690 3548 50  0000 C CNN
F 2 "" H 8675 3375 50  0001 C CNN
F 3 "" H 8675 3375 50  0001 C CNN
	1    8675 3375
	1    0    0    -1  
$EndComp
Text Label 9325 4950 0    50   ~ 0
A10
Text Label 9325 4050 0    50   ~ 0
A1
Wire Wire Line
	10575 4350 10775 4350
Text Label 10525 3950 2    50   ~ 0
D0
Text Label 9325 4350 0    50   ~ 0
A4
Wire Wire Line
	11575 3950 11775 3950
Text Label 9325 5300 0    50   ~ 0
~OE
Text Label 11775 3950 2    50   ~ 0
D0
Text Label 10575 4150 0    50   ~ 0
A2
Text Label 9275 4650 2    50   ~ 0
D7
Text Label 10575 4350 0    50   ~ 0
A4
Wire Wire Line
	10575 3950 10775 3950
Text Label 9325 4550 0    50   ~ 0
A6
$Comp
L power:GND #PWR?
U 1 1 5E86F3FB
P 10050 3650
AR Path="/5E83BCE9/5E86F3FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F3FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F3FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F3FB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10050 3400 50  0001 C CNN
F 1 "GND" H 10050 3500 50  0000 C CNN
F 2 "" H 10050 3650 50  0001 C CNN
F 3 "" H 10050 3650 50  0001 C CNN
	1    10050 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 4250 8275 4250
Wire Wire Line
	9325 4650 9525 4650
Text Label 10575 5300 0    50   ~ 0
~OE
Wire Wire Line
	11175 3425 11175 3750
Text Label 9275 4450 2    50   ~ 0
D5
Wire Wire Line
	11575 4250 11775 4250
Text Label 9275 4150 2    50   ~ 0
D2
Wire Wire Line
	10575 5200 10775 5200
Text Label 9275 4550 2    50   ~ 0
D6
$Comp
L power:GND #PWR?
U 1 1 5E86F411
P 11175 5650
AR Path="/5E83BCE9/5E86F411" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E86F411" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E86F411" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E86F411" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11175 5400 50  0001 C CNN
F 1 "GND" H 11175 5500 50  0000 C CNN
F 2 "" H 11175 5650 50  0001 C CNN
F 3 "" H 11175 5650 50  0001 C CNN
	1    11175 5650
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E86F436
P 11175 4750
AR Path="/5E83BCE9/5E86F436" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E86F436" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E86F436" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E86F436" Ref="U?"  Part="1" 
F 0 "U?" H 10800 5775 50  0000 C CNN
F 1 "TMM2016" H 10925 5700 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 11175 4750 50  0001 C CNN
F 3 "" H 11175 4750 50  0001 C CNN
	1    11175 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 4850 8275 4850
Text Label 9325 4450 0    50   ~ 0
A5
Wire Wire Line
	11575 4350 11775 4350
Text Label 10575 4950 0    50   ~ 0
A10
Text Label 6825 5300 0    50   ~ 0
~OE
Wire Wire Line
	9325 4950 9525 4950
Text Label 8075 4050 0    50   ~ 0
A1
Wire Wire Line
	9325 5300 9525 5300
Wire Wire Line
	11300 3425 11175 3425
Text Label 10575 4850 0    50   ~ 0
A9
Wire Wire Line
	8075 4450 8275 4450
Wire Wire Line
	8675 3425 8675 3375
Text Label 8075 4350 0    50   ~ 0
A4
Text Label 10575 3950 0    50   ~ 0
A0
Wire Wire Line
	11575 4150 11775 4150
Wire Wire Line
	10575 4750 10775 4750
Text Label 10525 4250 2    50   ~ 0
D3
Text Label 6825 5100 0    50   ~ 0
~WE
$Comp
L power:GND #PWR?
U 1 1 5EAA2326
P 11300 3650
AR Path="/5E83BCE9/5EAA2326" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2326" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2326" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2326" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11300 3400 50  0001 C CNN
F 1 "GND" H 11300 3500 50  0000 C CNN
F 2 "" H 11300 3650 50  0001 C CNN
F 3 "" H 11300 3650 50  0001 C CNN
	1    11300 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB2C5F0
P 11175 3375
AR Path="/5E83BCE9/5EB2C5F0" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5F0" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5F0" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5F0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11175 3225 50  0001 C CNN
F 1 "+5V" H 11190 3548 50  0000 C CNN
F 2 "" H 11175 3375 50  0001 C CNN
F 3 "" H 11175 3375 50  0001 C CNN
	1    11175 3375
	1    0    0    -1  
$EndComp
Text Label 11775 4050 2    50   ~ 0
D1
Text Label 10575 4250 0    50   ~ 0
A3
Text Label 11775 4650 2    50   ~ 0
D7
Text Label 10525 4650 2    50   ~ 0
D7
Text Label 9325 4850 0    50   ~ 0
A9
Text Label 10525 4350 2    50   ~ 0
D4
Wire Wire Line
	10050 3425 9925 3425
Wire Wire Line
	8075 3950 8275 3950
$Comp
L Device:C_Small C?
U 1 1 5EB2C5F1
P 8800 3550
AR Path="/5E83BCE9/5EB2C5F1" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EB2C5F1" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EB2C5F1" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EB2C5F1" Ref="C?"  Part="1" 
F 0 "C?" H 8892 3596 50  0000 L CNN
F 1 "C_Small" H 8892 3505 50  0000 L CNN
F 2 "" H 8800 3550 50  0001 C CNN
F 3 "~" H 8800 3550 50  0001 C CNN
	1    8800 3550
	1    0    0    -1  
$EndComp
Text Label 11775 4450 2    50   ~ 0
D5
Text Label 11775 4150 2    50   ~ 0
D2
Text Label 9325 4150 0    50   ~ 0
A2
Text Label 9275 4250 2    50   ~ 0
D3
Text Label 8075 4650 0    50   ~ 0
A7
Wire Wire Line
	10325 4650 10525 4650
Wire Wire Line
	10575 4550 10775 4550
Wire Wire Line
	10575 4250 10775 4250
Wire Wire Line
	9925 3425 9925 3750
Wire Wire Line
	9325 5200 9525 5200
Wire Wire Line
	11575 4550 11775 4550
Connection ~ 8675 3425
Wire Wire Line
	9275 10450 9475 10450
Wire Wire Line
	9275 10200 9475 10200
Text Label 8050 7525 0    50   ~ 0
A9
Text Label 8050 7625 0    50   ~ 0
A10
Text Label 7975 9400 2    50   ~ 0
D1
Wire Wire Line
	6775 9600 6975 9600
Wire Wire Line
	7525 6125 7525 6100
Wire Wire Line
	7525 6100 7400 6100
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876C29
P 7400 7425
AR Path="/5E83BCE9/5E876C29" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876C29" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876C29" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876C29" Ref="U?"  Part="1" 
F 0 "U?" H 7025 8450 50  0000 C CNN
F 1 "TMM2016" H 7150 8375 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 7400 7425 50  0001 C CNN
F 3 "" H 7400 7425 50  0001 C CNN
	1    7400 7425
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EAA2329
P 7525 6225
AR Path="/5E83BCE9/5EAA2329" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2329" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2329" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2329" Ref="C?"  Part="1" 
F 0 "C?" H 7617 6271 50  0000 L CNN
F 1 "C_Small" H 7617 6180 50  0000 L CNN
F 2 "" H 7525 6225 50  0001 C CNN
F 3 "~" H 7525 6225 50  0001 C CNN
	1    7525 6225
	1    0    0    -1  
$EndComp
Connection ~ 7400 6100
$Comp
L power:GND #PWR?
U 1 1 5EAA232A
P 7525 6325
AR Path="/5E83BCE9/5EAA232A" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA232A" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA232A" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA232A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7525 6075 50  0001 C CNN
F 1 "GND" H 7525 6175 50  0000 C CNN
F 2 "" H 7525 6325 50  0001 C CNN
F 3 "" H 7525 6325 50  0001 C CNN
	1    7525 6325
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 6100 7400 6425
Wire Wire Line
	7400 6100 7400 6050
$Comp
L power:+5V #PWR?
U 1 1 5EAA232B
P 7400 6050
AR Path="/5E83BCE9/5EAA232B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA232B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA232B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA232B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7400 5900 50  0001 C CNN
F 1 "+5V" H 7415 6223 50  0000 C CNN
F 2 "" H 7400 6050 50  0001 C CNN
F 3 "" H 7400 6050 50  0001 C CNN
	1    7400 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EB2C5F5
P 7400 8325
AR Path="/5E83BCE9/5EB2C5F5" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5F5" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5F5" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5F5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7400 8075 50  0001 C CNN
F 1 "GND" H 7400 8175 50  0000 C CNN
F 2 "" H 7400 8325 50  0001 C CNN
F 3 "" H 7400 8325 50  0001 C CNN
	1    7400 8325
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 6625 8000 6625
Wire Wire Line
	7800 6725 8000 6725
Wire Wire Line
	7800 6825 8000 6825
Wire Wire Line
	7800 6925 8000 6925
Wire Wire Line
	7800 7025 8000 7025
Wire Wire Line
	7800 7125 8000 7125
Wire Wire Line
	7800 7225 8000 7225
Wire Wire Line
	7800 7325 8000 7325
Wire Wire Line
	6800 7225 7000 7225
Wire Wire Line
	6800 6725 7000 6725
Wire Wire Line
	6800 7025 7000 7025
Wire Wire Line
	6800 7325 7000 7325
Wire Wire Line
	6800 6925 7000 6925
Wire Wire Line
	6800 7125 7000 7125
Wire Wire Line
	6800 6825 7000 6825
Wire Wire Line
	6800 6625 7000 6625
Wire Wire Line
	6800 7425 7000 7425
Wire Wire Line
	6800 7525 7000 7525
Wire Wire Line
	6800 7625 7000 7625
Wire Wire Line
	6800 7775 7000 7775
Wire Wire Line
	6800 7875 7000 7875
Wire Wire Line
	6800 7975 7000 7975
Text Label 8000 6625 2    50   ~ 0
D0
Text Label 8000 6725 2    50   ~ 0
D1
Text Label 8000 6825 2    50   ~ 0
D2
Text Label 8000 6925 2    50   ~ 0
D3
Text Label 8000 7025 2    50   ~ 0
D4
Text Label 8000 7125 2    50   ~ 0
D5
Text Label 8000 7225 2    50   ~ 0
D6
Text Label 8000 7325 2    50   ~ 0
D7
Text Label 6800 6625 0    50   ~ 0
A0
Text Label 6800 6725 0    50   ~ 0
A1
Text Label 6800 6825 0    50   ~ 0
A2
Text Label 6800 6925 0    50   ~ 0
A3
Text Label 6800 7025 0    50   ~ 0
A4
Text Label 6800 7125 0    50   ~ 0
A5
Text Label 6800 7225 0    50   ~ 0
A6
Text Label 6800 7325 0    50   ~ 0
A7
Text Label 6800 7425 0    50   ~ 0
A8
Text Label 6800 7525 0    50   ~ 0
A9
Text Label 6800 7625 0    50   ~ 0
A10
Text Label 9275 9700 0    50   ~ 0
A4
Connection ~ 9900 6100
Wire Wire Line
	8775 6100 8650 6100
Text Label 8050 7125 0    50   ~ 0
A5
Text Label 8050 7975 0    50   ~ 0
~OE
Wire Wire Line
	8650 6100 8650 6425
Wire Wire Line
	9050 6725 9250 6725
Wire Wire Line
	9050 7325 9250 7325
Wire Wire Line
	11150 6100 11150 6050
Wire Wire Line
	9875 8775 9875 8725
Wire Wire Line
	8025 9700 8225 9700
Wire Wire Line
	8050 6825 8250 6825
Wire Wire Line
	8050 7325 8250 7325
Wire Wire Line
	9025 9900 9225 9900
Text Label 9275 10100 0    50   ~ 0
A8
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876CBE
P 9875 10100
AR Path="/5E83BCE9/5E876CBE" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876CBE" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876CBE" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876CBE" Ref="U?"  Part="1" 
F 0 "U?" H 9500 11125 50  0000 C CNN
F 1 "TMM2016" H 9625 11050 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 9875 10100 50  0001 C CNN
F 3 "" H 9875 10100 50  0001 C CNN
	1    9875 10100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 7225 8250 7225
Wire Wire Line
	10550 7975 10750 7975
$Comp
L power:+5V #PWR?
U 1 1 5E876CCD
P 8625 8725
AR Path="/5E83BCE9/5E876CCD" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876CCD" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876CCD" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876CCD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8625 8575 50  0001 C CNN
F 1 "+5V" H 8640 8898 50  0000 C CNN
F 2 "" H 8625 8725 50  0001 C CNN
F 3 "" H 8625 8725 50  0001 C CNN
	1    8625 8725
	1    0    0    -1  
$EndComp
Text Label 6775 9700 0    50   ~ 0
A4
Text Label 6775 9800 0    50   ~ 0
A5
Wire Wire Line
	10550 6825 10750 6825
Text Label 10500 6825 2    50   ~ 0
D2
Wire Wire Line
	9900 6100 9900 6050
$Comp
L power:GND #PWR?
U 1 1 5E876CDF
P 7500 9000
AR Path="/5E83BCE9/5E876CDF" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876CDF" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876CDF" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876CDF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7500 8750 50  0001 C CNN
F 1 "GND" H 7500 8850 50  0000 C CNN
F 2 "" H 7500 9000 50  0001 C CNN
F 3 "" H 7500 9000 50  0001 C CNN
	1    7500 9000
	1    0    0    -1  
$EndComp
Text Label 7975 10000 2    50   ~ 0
D7
Text Label 6775 9300 0    50   ~ 0
A0
Wire Wire Line
	8050 7025 8250 7025
Wire Wire Line
	10550 7325 10750 7325
Connection ~ 8625 8775
Connection ~ 7375 8775
Wire Wire Line
	6775 10200 6975 10200
Text Label 6775 10000 0    50   ~ 0
A7
Text Label 10525 10300 0    50   ~ 0
A10
Text Label 6775 10650 0    50   ~ 0
~OE
Wire Wire Line
	9300 6825 9500 6825
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876D0F
P 11125 10100
AR Path="/5E83BCE9/5E876D0F" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876D0F" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876D0F" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876D0F" Ref="U?"  Part="1" 
F 0 "U?" H 10750 11125 50  0000 C CNN
F 1 "TMM2016" H 10875 11050 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 11125 10100 50  0001 C CNN
F 3 "" H 11125 10100 50  0001 C CNN
	1    11125 10100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8025 10200 8225 10200
Wire Wire Line
	11550 7325 11750 7325
Text Label 9300 7775 0    50   ~ 0
~WE
$Comp
L power:GND #PWR?
U 1 1 5E876D1F
P 11125 11000
AR Path="/5E83BCE9/5E876D1F" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876D1F" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876D1F" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876D1F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11125 10750 50  0001 C CNN
F 1 "GND" H 11125 10850 50  0000 C CNN
F 2 "" H 11125 11000 50  0001 C CNN
F 3 "" H 11125 11000 50  0001 C CNN
	1    11125 11000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9275 10650 9475 10650
Wire Wire Line
	8750 8775 8625 8775
Wire Wire Line
	8625 8775 8625 9100
Wire Wire Line
	9300 7425 9500 7425
$Comp
L power:+5V #PWR?
U 1 1 5E876D30
P 7375 8725
AR Path="/5E83BCE9/5E876D30" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876D30" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876D30" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876D30" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7375 8575 50  0001 C CNN
F 1 "+5V" H 7390 8898 50  0000 C CNN
F 2 "" H 7375 8725 50  0001 C CNN
F 3 "" H 7375 8725 50  0001 C CNN
	1    7375 8725
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E876D3E
P 7500 8900
AR Path="/5E83BCE9/5E876D3E" Ref="C?"  Part="1" 
AR Path="/5EA29723/5E876D3E" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5E876D3E" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5E876D3E" Ref="C?"  Part="1" 
F 0 "C?" H 7592 8946 50  0000 L CNN
F 1 "C_Small" H 7592 8855 50  0000 L CNN
F 2 "" H 7500 8900 50  0001 C CNN
F 3 "~" H 7500 8900 50  0001 C CNN
	1    7500 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7775 9300 7975 9300
Wire Wire Line
	8050 7975 8250 7975
$Comp
L power:GND #PWR?
U 1 1 5E876D4D
P 11250 9000
AR Path="/5E83BCE9/5E876D4D" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876D4D" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876D4D" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876D4D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11250 8750 50  0001 C CNN
F 1 "GND" H 11250 8850 50  0000 C CNN
F 2 "" H 11250 9000 50  0001 C CNN
F 3 "" H 11250 9000 50  0001 C CNN
	1    11250 9000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 7425 8250 7425
Wire Wire Line
	8050 7775 8250 7775
Text Label 9275 10200 0    50   ~ 0
A9
Text Label 10475 9700 2    50   ~ 0
D4
Wire Wire Line
	10525 10200 10725 10200
Text Label 7975 9800 2    50   ~ 0
D5
Wire Wire Line
	9300 6725 9500 6725
Wire Wire Line
	10525 10650 10725 10650
Text Label 10475 9500 2    50   ~ 0
D2
Wire Wire Line
	9050 7025 9250 7025
Text Label 8025 9700 0    50   ~ 0
A4
Text Label 10525 9300 0    50   ~ 0
A0
Wire Wire Line
	7775 9900 7975 9900
Text Label 9250 7025 2    50   ~ 0
D4
Text Label 8050 6625 0    50   ~ 0
A0
Wire Wire Line
	10525 9900 10725 9900
Wire Wire Line
	10525 9600 10725 9600
Wire Wire Line
	7775 9700 7975 9700
Text Label 11725 9600 2    50   ~ 0
D3
Wire Wire Line
	9025 9500 9225 9500
Text Label 8025 9300 0    50   ~ 0
A0
Wire Wire Line
	9275 9300 9475 9300
Text Label 8025 9900 0    50   ~ 0
A6
Text Label 10525 10650 0    50   ~ 0
~OE
Wire Wire Line
	11125 8775 11125 9100
Wire Wire Line
	9300 6625 9500 6625
Wire Wire Line
	10525 9500 10725 9500
Wire Wire Line
	9025 9300 9225 9300
Wire Wire Line
	9300 7775 9500 7775
Wire Wire Line
	6775 9700 6975 9700
Wire Wire Line
	9300 7525 9500 7525
Text Label 8050 7775 0    50   ~ 0
~WE
Text Label 10550 7225 0    50   ~ 0
A6
Wire Wire Line
	11550 7125 11750 7125
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876D94
P 8650 7425
AR Path="/5E83BCE9/5E876D94" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876D94" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876D94" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876D94" Ref="U?"  Part="1" 
F 0 "U?" H 8275 8450 50  0000 C CNN
F 1 "TMM2016" H 8400 8375 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 8650 7425 50  0001 C CNN
F 3 "" H 8650 7425 50  0001 C CNN
	1    8650 7425
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 7125 9500 7125
Wire Wire Line
	10300 6625 10500 6625
Text Label 10550 7325 0    50   ~ 0
A7
Text Label 8050 7425 0    50   ~ 0
A8
Wire Wire Line
	10300 7125 10500 7125
Text Label 10475 9600 2    50   ~ 0
D3
Text Label 11750 6925 2    50   ~ 0
D3
Wire Wire Line
	9050 6825 9250 6825
Wire Wire Line
	8050 6725 8250 6725
Text Label 10550 7775 0    50   ~ 0
~WE
Wire Wire Line
	10550 6725 10750 6725
Text Label 8050 7225 0    50   ~ 0
A6
Wire Wire Line
	10300 7225 10500 7225
Text Label 10550 7425 0    50   ~ 0
A8
Text Label 9225 9900 2    50   ~ 0
D6
$Comp
L Device:C_Small C?
U 1 1 5E876DB1
P 11275 6225
AR Path="/5E83BCE9/5E876DB1" Ref="C?"  Part="1" 
AR Path="/5EA29723/5E876DB1" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5E876DB1" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5E876DB1" Ref="C?"  Part="1" 
F 0 "C?" H 11367 6271 50  0000 L CNN
F 1 "C_Small" H 11367 6180 50  0000 L CNN
F 2 "" H 11275 6225 50  0001 C CNN
F 3 "~" H 11275 6225 50  0001 C CNN
	1    11275 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 7125 9250 7125
Wire Wire Line
	8775 6125 8775 6100
Wire Wire Line
	8050 7875 8250 7875
Text Label 10500 6725 2    50   ~ 0
D1
Text Label 10550 6725 0    50   ~ 0
A1
Wire Wire Line
	10550 7125 10750 7125
$Comp
L power:+5V #PWR?
U 1 1 5E876DC4
P 9900 6050
AR Path="/5E83BCE9/5E876DC4" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876DC4" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876DC4" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876DC4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9900 5900 50  0001 C CNN
F 1 "+5V" H 9915 6223 50  0000 C CNN
F 2 "" H 9900 6050 50  0001 C CNN
F 3 "" H 9900 6050 50  0001 C CNN
	1    9900 6050
	1    0    0    -1  
$EndComp
Text Label 7975 9300 2    50   ~ 0
D0
Wire Wire Line
	9050 6625 9250 6625
Text Label 7975 9500 2    50   ~ 0
D2
Wire Wire Line
	6775 10550 6975 10550
Wire Wire Line
	8025 9800 8225 9800
Wire Wire Line
	8625 8775 8625 8725
Text Label 11750 7025 2    50   ~ 0
D4
Wire Wire Line
	10300 6825 10500 6825
Text Label 9250 6625 2    50   ~ 0
D0
Text Label 8050 6825 0    50   ~ 0
A2
$Comp
L power:GND #PWR?
U 1 1 5E876DDB
P 8625 11000
AR Path="/5E83BCE9/5E876DDB" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876DDB" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876DDB" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876DDB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8625 10750 50  0001 C CNN
F 1 "GND" H 8625 10850 50  0000 C CNN
F 2 "" H 8625 11000 50  0001 C CNN
F 3 "" H 8625 11000 50  0001 C CNN
	1    8625 11000
	1    0    0    -1  
$EndComp
Wire Wire Line
	11275 6125 11275 6100
Wire Wire Line
	8025 9600 8225 9600
Wire Wire Line
	9275 10000 9475 10000
Wire Wire Line
	9300 7025 9500 7025
Text Label 9275 9400 0    50   ~ 0
A1
Wire Wire Line
	10525 9700 10725 9700
Connection ~ 11125 8775
Wire Wire Line
	8050 7625 8250 7625
Wire Wire Line
	10275 9900 10475 9900
Text Label 10525 10100 0    50   ~ 0
A8
Text Label 10500 7125 2    50   ~ 0
D5
Wire Wire Line
	6775 9500 6975 9500
Text Label 10550 7125 0    50   ~ 0
A5
Text Label 9300 6625 0    50   ~ 0
A0
Wire Wire Line
	7775 9800 7975 9800
Wire Wire Line
	9300 6925 9500 6925
$Comp
L Device:C_Small C?
U 1 1 5EAA2336
P 10025 6225
AR Path="/5E83BCE9/5EAA2336" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2336" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2336" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2336" Ref="C?"  Part="1" 
F 0 "C?" H 10117 6271 50  0000 L CNN
F 1 "C_Small" H 10117 6180 50  0000 L CNN
F 2 "" H 10025 6225 50  0001 C CNN
F 3 "~" H 10025 6225 50  0001 C CNN
	1    10025 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	10025 6125 10025 6100
Wire Wire Line
	9050 7225 9250 7225
Wire Wire Line
	11250 8775 11125 8775
Text Label 10525 10200 0    50   ~ 0
A9
Wire Wire Line
	6775 9400 6975 9400
Text Label 9300 7425 0    50   ~ 0
A8
$Comp
L power:GND #PWR?
U 1 1 5EAA2337
P 8650 8325
AR Path="/5E83BCE9/5EAA2337" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2337" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2337" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2337" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8650 8075 50  0001 C CNN
F 1 "GND" H 8650 8175 50  0000 C CNN
F 2 "" H 8650 8325 50  0001 C CNN
F 3 "" H 8650 8325 50  0001 C CNN
	1    8650 8325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E876E19
P 8775 6325
AR Path="/5E83BCE9/5E876E19" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876E19" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876E19" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876E19" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8775 6075 50  0001 C CNN
F 1 "GND" H 8775 6175 50  0000 C CNN
F 2 "" H 8775 6325 50  0001 C CNN
F 3 "" H 8775 6325 50  0001 C CNN
	1    8775 6325
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 7025 10500 7025
Text Label 9300 6925 0    50   ~ 0
A3
Wire Wire Line
	10525 9800 10725 9800
$Comp
L power:+5V #PWR?
U 1 1 5E876E29
P 9875 8725
AR Path="/5E83BCE9/5E876E29" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876E29" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876E29" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876E29" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9875 8575 50  0001 C CNN
F 1 "+5V" H 9890 8898 50  0000 C CNN
F 2 "" H 9875 8725 50  0001 C CNN
F 3 "" H 9875 8725 50  0001 C CNN
	1    9875 8725
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 7225 9500 7225
Wire Wire Line
	10550 7775 10750 7775
Connection ~ 11150 6100
Text Label 6775 10450 0    50   ~ 0
~WE
Wire Wire Line
	8750 8800 8750 8775
Wire Wire Line
	8025 10550 8225 10550
Text Label 9300 7325 0    50   ~ 0
A7
Wire Wire Line
	10300 6725 10500 6725
Text Label 9275 9800 0    50   ~ 0
A5
Wire Wire Line
	11525 9700 11725 9700
Text Label 11725 9400 2    50   ~ 0
D1
Text Label 11725 9500 2    50   ~ 0
D2
Connection ~ 9875 8775
Wire Wire Line
	9050 6925 9250 6925
Wire Wire Line
	11550 6725 11750 6725
Text Label 11750 7225 2    50   ~ 0
D6
Wire Wire Line
	10550 7525 10750 7525
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876E5F
P 9900 7425
AR Path="/5E83BCE9/5E876E5F" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876E5F" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876E5F" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876E5F" Ref="U?"  Part="1" 
F 0 "U?" H 9525 8450 50  0000 C CNN
F 1 "TMM2016" H 9650 8375 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 9900 7425 50  0001 C CNN
F 3 "" H 9900 7425 50  0001 C CNN
	1    9900 7425
	1    0    0    -1  
$EndComp
Text Label 8050 6925 0    50   ~ 0
A3
Text Label 9250 6725 2    50   ~ 0
D1
Text Label 9275 10450 0    50   ~ 0
~WE
Text Label 10500 7225 2    50   ~ 0
D6
Wire Wire Line
	10550 7625 10750 7625
Wire Wire Line
	10300 6925 10500 6925
Wire Wire Line
	6775 10650 6975 10650
Wire Wire Line
	11525 9400 11725 9400
Text Label 11725 9900 2    50   ~ 0
D6
$Comp
L power:GND #PWR?
U 1 1 5EB2C5FA
P 9900 8325
AR Path="/5E83BCE9/5EB2C5FA" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5FA" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5FA" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5FA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9900 8075 50  0001 C CNN
F 1 "GND" H 9900 8175 50  0000 C CNN
F 2 "" H 9900 8325 50  0001 C CNN
F 3 "" H 9900 8325 50  0001 C CNN
	1    9900 8325
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB2C5FB
P 8650 6050
AR Path="/5E83BCE9/5EB2C5FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5FB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8650 5900 50  0001 C CNN
F 1 "+5V" H 8665 6223 50  0000 C CNN
F 2 "" H 8650 6050 50  0001 C CNN
F 3 "" H 8650 6050 50  0001 C CNN
	1    8650 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EAA233C
P 11250 8900
AR Path="/5E83BCE9/5EAA233C" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA233C" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA233C" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA233C" Ref="C?"  Part="1" 
F 0 "C?" H 11342 8946 50  0000 L CNN
F 1 "C_Small" H 11342 8855 50  0000 L CNN
F 2 "" H 11250 8900 50  0001 C CNN
F 3 "~" H 11250 8900 50  0001 C CNN
	1    11250 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9025 9800 9225 9800
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876EB6
P 7375 10100
AR Path="/5E83BCE9/5E876EB6" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876EB6" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876EB6" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876EB6" Ref="U?"  Part="1" 
F 0 "U?" H 7000 11125 50  0000 C CNN
F 1 "TMM2016" H 7125 11050 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 7375 10100 50  0001 C CNN
F 3 "" H 7375 10100 50  0001 C CNN
	1    7375 10100
	1    0    0    -1  
$EndComp
Text Label 9300 7625 0    50   ~ 0
A10
Text Label 9300 6725 0    50   ~ 0
A1
Wire Wire Line
	10550 7025 10750 7025
Text Label 6775 10200 0    50   ~ 0
A9
Text Label 10500 6625 2    50   ~ 0
D0
Wire Wire Line
	7500 8775 7375 8775
Wire Wire Line
	8025 10000 8225 10000
Wire Wire Line
	8025 9900 8225 9900
Wire Wire Line
	11125 8775 11125 8725
Wire Wire Line
	8025 9500 8225 9500
Wire Wire Line
	9875 8775 9875 9100
Wire Wire Line
	9275 10550 9475 10550
Wire Wire Line
	11525 9900 11725 9900
Wire Wire Line
	9025 9400 9225 9400
Wire Wire Line
	9025 10000 9225 10000
Text Label 8025 9800 0    50   ~ 0
A5
Text Label 8025 10650 0    50   ~ 0
~OE
Wire Wire Line
	8025 10450 8225 10450
Wire Wire Line
	9275 9400 9475 9400
Text Label 9225 9600 2    50   ~ 0
D3
Text Label 8025 10000 0    50   ~ 0
A7
Text Label 9275 9500 0    50   ~ 0
A2
Wire Wire Line
	9025 9700 9225 9700
Text Label 9225 9700 2    50   ~ 0
D4
Text Label 9300 7025 0    50   ~ 0
A4
$Comp
L power:GND #PWR?
U 1 1 5E876EDC
P 10000 9000
AR Path="/5E83BCE9/5E876EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876EDC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10000 8750 50  0001 C CNN
F 1 "GND" H 10000 8850 50  0000 C CNN
F 2 "" H 10000 9000 50  0001 C CNN
F 3 "" H 10000 9000 50  0001 C CNN
	1    10000 9000
	1    0    0    -1  
$EndComp
Text Label 10475 9400 2    50   ~ 0
D1
Text Label 10525 9400 0    50   ~ 0
A1
Wire Wire Line
	11550 6625 11750 6625
Text Label 9300 7975 0    50   ~ 0
~OE
Text Label 11750 6625 2    50   ~ 0
D0
Text Label 10550 6825 0    50   ~ 0
A2
Text Label 9250 7325 2    50   ~ 0
D7
$Comp
L power:+5V #PWR?
U 1 1 5E876EF0
P 11125 8725
AR Path="/5E83BCE9/5E876EF0" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E876EF0" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E876EF0" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E876EF0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11125 8575 50  0001 C CNN
F 1 "+5V" H 11140 8898 50  0000 C CNN
F 2 "" H 11125 8725 50  0001 C CNN
F 3 "" H 11125 8725 50  0001 C CNN
	1    11125 8725
	1    0    0    -1  
$EndComp
Wire Wire Line
	8025 9300 8225 9300
Text Label 10550 7025 0    50   ~ 0
A4
Wire Wire Line
	10550 6625 10750 6625
Wire Wire Line
	10275 9600 10475 9600
$Comp
L power:GND #PWR?
U 1 1 5EAA233F
P 9875 11000
AR Path="/5E83BCE9/5EAA233F" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA233F" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA233F" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA233F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9875 10750 50  0001 C CNN
F 1 "GND" H 9875 10850 50  0000 C CNN
F 2 "" H 9875 11000 50  0001 C CNN
F 3 "" H 9875 11000 50  0001 C CNN
	1    9875 11000
	1    0    0    -1  
$EndComp
Text Label 9300 7225 0    50   ~ 0
A6
$Comp
L power:GND #PWR?
U 1 1 5EB2C5FC
P 10025 6325
AR Path="/5E83BCE9/5EB2C5FC" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5FC" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5FC" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5FC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10025 6075 50  0001 C CNN
F 1 "GND" H 10025 6175 50  0000 C CNN
F 2 "" H 10025 6325 50  0001 C CNN
F 3 "" H 10025 6325 50  0001 C CNN
	1    10025 6325
	1    0    0    -1  
$EndComp
Text Label 6775 9400 0    50   ~ 0
A1
Wire Wire Line
	8050 6925 8250 6925
Wire Wire Line
	9300 7325 9500 7325
Text Label 9225 10000 2    50   ~ 0
D7
Text Label 10525 9700 0    50   ~ 0
A4
Wire Wire Line
	7375 8775 7375 9100
$Comp
L Device:C_Small C?
U 1 1 5E876F23
P 8750 8900
AR Path="/5E83BCE9/5E876F23" Ref="C?"  Part="1" 
AR Path="/5EA29723/5E876F23" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5E876F23" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5E876F23" Ref="C?"  Part="1" 
F 0 "C?" H 8842 8946 50  0000 L CNN
F 1 "C_Small" H 8842 8855 50  0000 L CNN
F 2 "" H 8750 8900 50  0001 C CNN
F 3 "~" H 8750 8900 50  0001 C CNN
	1    8750 8900
	1    0    0    -1  
$EndComp
Text Label 11725 9800 2    50   ~ 0
D5
Text Label 6775 9500 0    50   ~ 0
A2
Text Label 6775 9600 0    50   ~ 0
A3
Text Label 10550 7975 0    50   ~ 0
~OE
Wire Wire Line
	11150 6100 11150 6425
Text Label 9250 7125 2    50   ~ 0
D5
Wire Wire Line
	11550 6925 11750 6925
Text Label 9250 6825 2    50   ~ 0
D2
Wire Wire Line
	10550 7875 10750 7875
Text Label 9250 7225 2    50   ~ 0
D6
Text Label 10525 10450 0    50   ~ 0
~WE
Wire Wire Line
	10525 9400 10725 9400
Text Label 7975 9700 2    50   ~ 0
D4
Wire Wire Line
	7775 10000 7975 10000
$Comp
L power:GND #PWR?
U 1 1 5EAA2342
P 11150 8325
AR Path="/5E83BCE9/5EAA2342" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2342" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2342" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2342" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11150 8075 50  0001 C CNN
F 1 "GND" H 11150 8175 50  0000 C CNN
F 2 "" H 11150 8325 50  0001 C CNN
F 3 "" H 11150 8325 50  0001 C CNN
	1    11150 8325
	1    0    0    -1  
$EndComp
Text Label 10525 9600 0    50   ~ 0
A3
Text Label 11725 10000 2    50   ~ 0
D7
Text Label 7975 9900 2    50   ~ 0
D6
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876F66
P 11150 7425
AR Path="/5E83BCE9/5E876F66" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876F66" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876F66" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876F66" Ref="U?"  Part="1" 
F 0 "U?" H 10775 8450 50  0000 C CNN
F 1 "TMM2016" H 10900 8375 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 11150 7425 50  0001 C CNN
F 3 "" H 11150 7425 50  0001 C CNN
	1    11150 7425
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 7525 8250 7525
Text Label 9300 7125 0    50   ~ 0
A5
Wire Wire Line
	11550 7025 11750 7025
Text Label 10550 7625 0    50   ~ 0
A10
Text Label 8025 10450 0    50   ~ 0
~WE
Text Label 10525 9900 0    50   ~ 0
A6
Text Label 11725 9300 2    50   ~ 0
D0
Text Label 10525 9500 0    50   ~ 0
A2
Text Label 6800 7975 0    50   ~ 0
~OE
Text Label 10475 9300 2    50   ~ 0
D0
Wire Wire Line
	9300 7625 9500 7625
Wire Wire Line
	6775 9900 6975 9900
Wire Wire Line
	7775 9500 7975 9500
Text Label 6775 10300 0    50   ~ 0
A10
Text Label 8050 6725 0    50   ~ 0
A1
Wire Wire Line
	9300 7975 9500 7975
Text Label 7975 9600 2    50   ~ 0
D3
Text Label 8025 10200 0    50   ~ 0
A9
Wire Wire Line
	10525 9300 10725 9300
Text Label 9275 9900 0    50   ~ 0
A6
Wire Wire Line
	11275 6100 11150 6100
Wire Wire Line
	6775 10300 6975 10300
Text Label 10550 7525 0    50   ~ 0
A9
Wire Wire Line
	8050 7125 8250 7125
Text Label 6775 10100 0    50   ~ 0
A8
Wire Wire Line
	8650 6100 8650 6050
Wire Wire Line
	6775 10100 6975 10100
Text Label 8050 7025 0    50   ~ 0
A4
Text Label 10550 6625 0    50   ~ 0
A0
Wire Wire Line
	11550 6825 11750 6825
Wire Wire Line
	7375 8775 7375 8725
Wire Wire Line
	11525 9500 11725 9500
Wire Wire Line
	10525 10100 10725 10100
Wire Wire Line
	10000 8775 9875 8775
$Comp
L power:GND #PWR?
U 1 1 5EAA2343
P 8750 9000
AR Path="/5E83BCE9/5EAA2343" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2343" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2343" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2343" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8750 8750 50  0001 C CNN
F 1 "GND" H 8750 8850 50  0000 C CNN
F 2 "" H 8750 9000 50  0001 C CNN
F 3 "" H 8750 9000 50  0001 C CNN
	1    8750 9000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 7425 10750 7425
Text Label 10500 6925 2    50   ~ 0
D3
Wire Wire Line
	6775 9800 6975 9800
Text Label 11725 9700 2    50   ~ 0
D4
Text Label 8025 9600 0    50   ~ 0
A3
Wire Wire Line
	10525 10300 10725 10300
Wire Wire Line
	10275 9500 10475 9500
Text Label 9225 9300 2    50   ~ 0
D0
Wire Wire Line
	10525 10000 10725 10000
Wire Wire Line
	9275 9500 9475 9500
Text Label 9225 9400 2    50   ~ 0
D1
Text Label 10475 9900 2    50   ~ 0
D6
Text Label 9275 10300 0    50   ~ 0
A10
Text Label 8025 9500 0    50   ~ 0
A2
Wire Wire Line
	11250 8800 11250 8775
Wire Wire Line
	11525 9800 11725 9800
$Comp
L es65-rescue:TMM2016-my_ics U?
U 1 1 5E876FCA
P 8625 10100
AR Path="/5E83BCE9/5E876FCA" Ref="U?"  Part="1" 
AR Path="/5EA29723/5E876FCA" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E876FCA" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E876FCA" Ref="U?"  Part="1" 
F 0 "U?" H 8250 11125 50  0000 C CNN
F 1 "TMM2016" H 8375 11050 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 8625 10100 50  0001 C CNN
F 3 "" H 8625 10100 50  0001 C CNN
	1    8625 10100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9275 9700 9475 9700
Wire Wire Line
	8025 10300 8225 10300
Text Label 10475 9800 2    50   ~ 0
D5
Text Label 10525 9800 0    50   ~ 0
A5
Text Label 9275 9300 0    50   ~ 0
A0
Wire Wire Line
	9275 9600 9475 9600
Wire Wire Line
	11525 9300 11725 9300
Text Label 9275 10650 0    50   ~ 0
~OE
Text Label 8025 10100 0    50   ~ 0
A8
Wire Wire Line
	8025 9400 8225 9400
Text Label 10525 10000 0    50   ~ 0
A7
Wire Wire Line
	10275 9800 10475 9800
Wire Wire Line
	9275 9800 9475 9800
Wire Wire Line
	10275 9300 10475 9300
Text Label 10475 10000 2    50   ~ 0
D7
Wire Wire Line
	10275 10000 10475 10000
Text Label 6800 7775 0    50   ~ 0
~WE
Text Label 9225 9500 2    50   ~ 0
D2
Wire Wire Line
	10525 10550 10725 10550
Wire Wire Line
	7500 8800 7500 8775
Wire Wire Line
	11525 10000 11725 10000
Wire Wire Line
	9275 10100 9475 10100
$Comp
L power:GND #PWR?
U 1 1 5EAA2344
P 11275 6325
AR Path="/5E83BCE9/5EAA2344" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2344" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2344" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2344" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11275 6075 50  0001 C CNN
F 1 "GND" H 11275 6175 50  0000 C CNN
F 2 "" H 11275 6325 50  0001 C CNN
F 3 "" H 11275 6325 50  0001 C CNN
	1    11275 6325
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA2345
P 11150 6050
AR Path="/5E83BCE9/5EAA2345" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2345" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2345" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2345" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11150 5900 50  0001 C CNN
F 1 "+5V" H 11165 6223 50  0000 C CNN
F 2 "" H 11150 6050 50  0001 C CNN
F 3 "" H 11150 6050 50  0001 C CNN
	1    11150 6050
	1    0    0    -1  
$EndComp
Text Label 11750 6725 2    50   ~ 0
D1
Text Label 10550 6925 0    50   ~ 0
A3
Wire Wire Line
	7775 9600 7975 9600
Text Label 9225 9800 2    50   ~ 0
D5
Wire Wire Line
	11525 9600 11725 9600
Text Label 11750 7325 2    50   ~ 0
D7
Wire Wire Line
	8025 10650 8225 10650
Wire Wire Line
	8025 10100 8225 10100
Wire Wire Line
	6775 9300 6975 9300
Text Label 6775 9900 0    50   ~ 0
A6
$Comp
L Device:C_Small C?
U 1 1 5EAA2346
P 10000 8900
AR Path="/5E83BCE9/5EAA2346" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2346" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2346" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2346" Ref="C?"  Part="1" 
F 0 "C?" H 10092 8946 50  0000 L CNN
F 1 "C_Small" H 10092 8855 50  0000 L CNN
F 2 "" H 10000 8900 50  0001 C CNN
F 3 "~" H 10000 8900 50  0001 C CNN
	1    10000 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 8800 10000 8775
Text Label 8025 10300 0    50   ~ 0
A10
Text Label 10500 7325 2    50   ~ 0
D7
Wire Wire Line
	9275 10300 9475 10300
Text Label 8025 9400 0    50   ~ 0
A1
Text Label 9300 7525 0    50   ~ 0
A9
Text Label 10500 7025 2    50   ~ 0
D4
Wire Wire Line
	10025 6100 9900 6100
Wire Wire Line
	8050 6625 8250 6625
$Comp
L Device:C_Small C?
U 1 1 5EAA2347
P 8775 6225
AR Path="/5E83BCE9/5EAA2347" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EAA2347" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EAA2347" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EAA2347" Ref="C?"  Part="1" 
F 0 "C?" H 8867 6271 50  0000 L CNN
F 1 "C_Small" H 8867 6180 50  0000 L CNN
F 2 "" H 8775 6225 50  0001 C CNN
F 3 "~" H 8775 6225 50  0001 C CNN
	1    8775 6225
	1    0    0    -1  
$EndComp
Text Label 11750 7125 2    50   ~ 0
D5
Text Label 11750 6825 2    50   ~ 0
D2
Wire Wire Line
	6775 10000 6975 10000
Text Label 9300 6825 0    50   ~ 0
A2
Wire Wire Line
	10275 9400 10475 9400
Wire Wire Line
	9025 9600 9225 9600
Text Label 9250 6925 2    50   ~ 0
D3
Text Label 8050 7325 0    50   ~ 0
A7
Wire Wire Line
	6775 10450 6975 10450
Wire Wire Line
	10300 7325 10500 7325
Wire Wire Line
	10550 7225 10750 7225
Wire Wire Line
	10550 6925 10750 6925
Wire Wire Line
	10525 10450 10725 10450
Text Label 9275 10000 0    50   ~ 0
A7
Wire Wire Line
	9900 6100 9900 6425
Wire Wire Line
	10275 9700 10475 9700
Text Label 9275 9600 0    50   ~ 0
A3
Wire Wire Line
	9275 9900 9475 9900
$Comp
L power:GND #PWR?
U 1 1 5EAA2348
P 7375 11000
AR Path="/5E83BCE9/5EAA2348" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2348" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2348" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2348" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7375 10750 50  0001 C CNN
F 1 "GND" H 7375 10850 50  0000 C CNN
F 2 "" H 7375 11000 50  0001 C CNN
F 3 "" H 7375 11000 50  0001 C CNN
	1    7375 11000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7775 9400 7975 9400
Wire Wire Line
	9300 7875 9500 7875
Wire Wire Line
	11550 7225 11750 7225
Connection ~ 8650 6100
Text Label 8025 10550 0    50   ~ 0
~CE14
Text Label 9275 10550 0    50   ~ 0
~CE15
Text Label 10525 10550 0    50   ~ 0
~CE16
Text Label 13125 1275 2    50   ~ 0
~CE1
Text Label 13125 1375 2    50   ~ 0
~CE2
Text Label 13125 1475 2    50   ~ 0
~CE3
Text Label 13125 1575 2    50   ~ 0
~CE4
Text Label 13100 3950 2    50   ~ 0
~CE5
Text Label 13100 4050 2    50   ~ 0
~CE6
Text Label 13100 4150 2    50   ~ 0
~CE7
Text Label 13100 4250 2    50   ~ 0
~CE8
Text Label 13125 6150 2    50   ~ 0
~CE9
Text Label 13125 6250 2    50   ~ 0
~CE10
Text Label 13125 6350 2    50   ~ 0
~CE11
Text Label 13125 6450 2    50   ~ 0
~CE12
Text Label 13100 8825 2    50   ~ 0
~CE13
Text Label 13100 8925 2    50   ~ 0
~CE14
Text Label 13100 9025 2    50   ~ 0
~CE15
Text Label 13100 9125 2    50   ~ 0
~CE16
Wire Wire Line
	12925 1375 13125 1375
Wire Wire Line
	12925 1275 13125 1275
Wire Wire Line
	12925 1575 13125 1575
Wire Wire Line
	12925 1475 13125 1475
NoConn ~ 12925 1675
NoConn ~ 12925 1775
NoConn ~ 12925 1875
Text Label 11975 1375 0    50   ~ 0
A12
Text Label 11975 1275 0    50   ~ 0
A11
Wire Wire Line
	12175 1575 11975 1575
Text Label 11975 1575 0    50   ~ 0
A14
Wire Wire Line
	12175 1475 11975 1475
Wire Wire Line
	12175 1675 11975 1675
Wire Wire Line
	12175 1275 11975 1275
Wire Wire Line
	12175 1375 11975 1375
Text Label 11975 1475 0    50   ~ 0
A13
$Comp
L power:+5V #PWR?
U 1 1 5E8B4FFF
P 12550 3750
AR Path="/5E83BCE9/5E8B4FFF" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8B4FFF" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8B4FFF" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8B4FFF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12550 3600 50  0001 C CNN
F 1 "+5V" H 12565 3923 50  0000 C CNN
F 2 "" H 12550 3750 50  0001 C CNN
F 3 "" H 12550 3750 50  0001 C CNN
	1    12550 3750
	1    0    0    -1  
$EndComp
NoConn ~ 12900 4550
NoConn ~ 12900 4450
NoConn ~ 12900 4350
Wire Wire Line
	12900 4250 13100 4250
Wire Wire Line
	12150 4150 11950 4150
Wire Wire Line
	12150 3950 11950 3950
Wire Wire Line
	12150 4250 11950 4250
$Comp
L es65-rescue:6331-my_ics U13
U 1 1 5EAA234B
P 12550 4250
AR Path="/5E83BCE9/5EAA234B" Ref="U13"  Part="1" 
AR Path="/5EA29723/5EAA234B" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA234B" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA234B" Ref="U?"  Part="1" 
F 0 "U13" H 12250 4775 50  0000 C CNN
F 1 "6331" H 12300 4700 50  0000 C CNN
F 2 "" H 12250 4350 50  0001 C CNN
F 3 "" H 12250 4350 50  0001 C CNN
	1    12550 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA234C
P 12550 4850
AR Path="/5E83BCE9/5EAA234C" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA234C" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA234C" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA234C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12550 4600 50  0001 C CNN
F 1 "GND" H 12550 4700 50  0000 C CNN
F 2 "" H 12550 4850 50  0001 C CNN
F 3 "" H 12550 4850 50  0001 C CNN
	1    12550 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 4050 11950 4050
Wire Wire Line
	12150 4350 11950 4350
Wire Wire Line
	12900 3950 13100 3950
Wire Wire Line
	12900 4150 13100 4150
Wire Wire Line
	12900 4050 13100 4050
$Comp
L power:+5V #PWR?
U 1 1 5EAA234D
P 12575 5950
AR Path="/5E83BCE9/5EAA234D" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA234D" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA234D" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA234D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12575 5800 50  0001 C CNN
F 1 "+5V" H 12590 6123 50  0000 C CNN
F 2 "" H 12575 5950 50  0001 C CNN
F 3 "" H 12575 5950 50  0001 C CNN
	1    12575 5950
	1    0    0    -1  
$EndComp
NoConn ~ 12925 6750
Wire Wire Line
	12150 9025 11950 9025
Wire Wire Line
	12150 9225 11950 9225
NoConn ~ 12925 6650
NoConn ~ 12925 6550
Wire Wire Line
	12150 8925 11950 8925
Wire Wire Line
	12900 9025 13100 9025
Wire Wire Line
	12900 8925 13100 8925
NoConn ~ 12900 9325
Wire Wire Line
	12925 6450 13125 6450
Wire Wire Line
	12175 6350 11975 6350
$Comp
L es65-rescue:6331-my_ics U11
U 1 1 5EAA234E
P 12550 9125
AR Path="/5E83BCE9/5EAA234E" Ref="U11"  Part="1" 
AR Path="/5EA29723/5EAA234E" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA234E" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA234E" Ref="U?"  Part="1" 
F 0 "U11" H 12250 9650 50  0000 C CNN
F 1 "6331" H 12300 9575 50  0000 C CNN
F 2 "" H 12250 9225 50  0001 C CNN
F 3 "" H 12250 9225 50  0001 C CNN
	1    12550 9125
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA234F
P 12550 9725
AR Path="/5E83BCE9/5EAA234F" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA234F" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA234F" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA234F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12550 9475 50  0001 C CNN
F 1 "GND" H 12550 9575 50  0000 C CNN
F 2 "" H 12550 9725 50  0001 C CNN
F 3 "" H 12550 9725 50  0001 C CNN
	1    12550 9725
	1    0    0    -1  
$EndComp
Wire Wire Line
	12175 6150 11975 6150
Wire Wire Line
	12175 6450 11975 6450
NoConn ~ 12900 9225
NoConn ~ 12900 9425
$Comp
L es65-rescue:6331-my_ics U12
U 1 1 5EAA2350
P 12575 6450
AR Path="/5E83BCE9/5EAA2350" Ref="U12"  Part="1" 
AR Path="/5EA29723/5EAA2350" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA2350" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA2350" Ref="U?"  Part="1" 
F 0 "U12" H 12275 6975 50  0000 C CNN
F 1 "6331" H 12325 6900 50  0000 C CNN
F 2 "" H 12275 6550 50  0001 C CNN
F 3 "" H 12275 6550 50  0001 C CNN
	1    12575 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA2351
P 12575 7050
AR Path="/5E83BCE9/5EAA2351" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2351" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2351" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2351" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12575 6800 50  0001 C CNN
F 1 "GND" H 12575 6900 50  0000 C CNN
F 2 "" H 12575 7050 50  0001 C CNN
F 3 "" H 12575 7050 50  0001 C CNN
	1    12575 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	12175 6250 11975 6250
Wire Wire Line
	12175 6550 11975 6550
Wire Wire Line
	12925 6150 13125 6150
Wire Wire Line
	12925 6350 13125 6350
$Comp
L power:+5V #PWR?
U 1 1 5E8B9FD9
P 12550 8625
AR Path="/5E83BCE9/5E8B9FD9" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8B9FD9" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8B9FD9" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8B9FD9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12550 8475 50  0001 C CNN
F 1 "+5V" H 12565 8798 50  0000 C CNN
F 2 "" H 12550 8625 50  0001 C CNN
F 3 "" H 12550 8625 50  0001 C CNN
	1    12550 8625
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 9125 11950 9125
Wire Wire Line
	12150 8825 11950 8825
Wire Wire Line
	12900 8825 13100 8825
Wire Wire Line
	12900 9125 13100 9125
Wire Wire Line
	12925 6250 13125 6250
$Comp
L power:+5V #PWR?
U 1 1 5E8CB702
P 15175 2175
AR Path="/5E83BCE9/5E8CB702" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8CB702" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8CB702" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8CB702" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15175 2025 50  0001 C CNN
F 1 "+5V" H 15190 2348 50  0000 C CNN
F 2 "" H 15175 2175 50  0001 C CNN
F 3 "" H 15175 2175 50  0001 C CNN
	1    15175 2175
	1    0    0    -1  
$EndComp
Text Label 14350 3600 1    50   ~ 0
~CE1
Wire Wire Line
	14650 3400 14650 3600
Text Label 14450 3600 1    50   ~ 0
~CE2
Text Label 14650 3600 1    50   ~ 0
~CE4
Text Label 14550 3600 1    50   ~ 0
~CE3
Wire Wire Line
	14350 3400 14350 3600
Wire Wire Line
	14550 3400 14550 3600
Wire Wire Line
	14450 3400 14450 3600
$Comp
L power:+5V #PWR?
U 1 1 5E8CC09F
P 15200 3025
AR Path="/5E83BCE9/5E8CC09F" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8CC09F" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8CC09F" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8CC09F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15200 2875 50  0001 C CNN
F 1 "+5V" H 15215 3198 50  0000 C CNN
F 2 "" H 15200 3025 50  0001 C CNN
F 3 "" H 15200 3025 50  0001 C CNN
	1    15200 3025
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Network05 RN?
U 1 1 5E8CC0B1
P 15400 3225
AR Path="/5E83BCE9/5E8CC0B1" Ref="RN?"  Part="1" 
AR Path="/5EA29723/5E8CC0B1" Ref="RN?"  Part="1" 
AR Path="/5EA2979B/5E8CC0B1" Ref="RN?"  Part="1" 
AR Path="/5EA297F2/5E8CC0B1" Ref="RN?"  Part="1" 
F 0 "RN?" H 15688 3271 50  0000 L CNN
F 1 "470E" H 15688 3180 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 15775 3225 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 15400 3225 50  0001 C CNN
	1    15400 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	15400 3425 15400 3625
Wire Wire Line
	15300 3425 15300 3625
Text Label 15400 3625 1    50   ~ 0
~CE7
Text Label 15300 3625 1    50   ~ 0
~CE6
Text Label 15200 3625 1    50   ~ 0
~CE5
Text Label 15500 3625 1    50   ~ 0
~CE8
Wire Wire Line
	15200 3425 15200 3625
Wire Wire Line
	15500 3425 15500 3625
$Comp
L Device:R_Network05 RN?
U 1 1 5EB2C60A
P 14550 2350
AR Path="/5E83BCE9/5EB2C60A" Ref="RN?"  Part="1" 
AR Path="/5EA29723/5EB2C60A" Ref="RN?"  Part="1" 
AR Path="/5EA2979B/5EB2C60A" Ref="RN?"  Part="1" 
AR Path="/5EA297F2/5EB2C60A" Ref="RN?"  Part="1" 
F 0 "RN?" H 14838 2396 50  0000 L CNN
F 1 "470E" H 14838 2305 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 14925 2350 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 14550 2350 50  0001 C CNN
	1    14550 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB2C60B
P 14350 2150
AR Path="/5E83BCE9/5EB2C60B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C60B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C60B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C60B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14350 2000 50  0001 C CNN
F 1 "+5V" H 14365 2323 50  0000 C CNN
F 2 "" H 14350 2150 50  0001 C CNN
F 3 "" H 14350 2150 50  0001 C CNN
	1    14350 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 2550 14350 2750
Wire Wire Line
	14550 2550 14550 2750
Wire Wire Line
	14650 2550 14650 2750
Text Label 14550 2750 1    50   ~ 0
~CE11
Wire Wire Line
	14450 2550 14450 2750
Text Label 14650 2750 1    50   ~ 0
~CE12
Text Label 14450 2750 1    50   ~ 0
~CE10
Text Label 14350 2750 1    50   ~ 0
~CE9
$Comp
L Device:R_Network05 RN?
U 1 1 5EAA235A
P 14550 3200
AR Path="/5E83BCE9/5EAA235A" Ref="RN?"  Part="1" 
AR Path="/5EA29723/5EAA235A" Ref="RN?"  Part="1" 
AR Path="/5EA2979B/5EAA235A" Ref="RN?"  Part="1" 
AR Path="/5EA297F2/5EAA235A" Ref="RN?"  Part="1" 
F 0 "RN?" H 14838 3246 50  0000 L CNN
F 1 "470E" H 14838 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 14925 3200 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 14550 3200 50  0001 C CNN
	1    14550 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA235B
P 14350 3000
AR Path="/5E83BCE9/5EAA235B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA235B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA235B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA235B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14350 2850 50  0001 C CNN
F 1 "+5V" H 14365 3173 50  0000 C CNN
F 2 "" H 14350 3000 50  0001 C CNN
F 3 "" H 14350 3000 50  0001 C CNN
	1    14350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	15375 2575 15375 2775
Wire Wire Line
	15175 2575 15175 2775
Wire Wire Line
	15475 2575 15475 2775
Text Label 15375 2775 1    50   ~ 0
~CE15
Text Label 15275 2775 1    50   ~ 0
~CE14
Text Label 15475 2775 1    50   ~ 0
~CE16
Wire Wire Line
	15275 2575 15275 2775
Text Label 15175 2775 1    50   ~ 0
~CE13
$Comp
L es65-rescue:6301-my_ics U10
U 1 1 5E8EBA92
P 1300 7900
AR Path="/5E83BCE9/5E8EBA92" Ref="U10"  Part="1" 
AR Path="/5EA29723/5E8EBA92" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E8EBA92" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E8EBA92" Ref="U?"  Part="1" 
F 0 "U10" H 1000 8425 50  0000 C CNN
F 1 "6301" H 1050 8350 50  0000 C CNN
F 2 "" H 1000 8000 50  0001 C CNN
F 3 "" H 1000 8000 50  0001 C CNN
	1    1300 7900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E8EC5F7
P 1300 7400
AR Path="/5E83BCE9/5E8EC5F7" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8EC5F7" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8EC5F7" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8EC5F7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1300 7250 50  0001 C CNN
F 1 "+5V" H 1315 7573 50  0000 C CNN
F 2 "" H 1300 7400 50  0001 C CNN
F 3 "" H 1300 7400 50  0001 C CNN
	1    1300 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E8EF1BF
P 1300 8700
AR Path="/5E83BCE9/5E8EF1BF" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8EF1BF" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8EF1BF" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8EF1BF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1300 8450 50  0001 C CNN
F 1 "GND" H 1300 8550 50  0000 C CNN
F 2 "" H 1300 8700 50  0001 C CNN
F 3 "" H 1300 8700 50  0001 C CNN
	1    1300 8700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS20 U5
U 3 1 5EAA2361
P 15675 9250
AR Path="/5E83BCE9/5EAA2361" Ref="U5"  Part="3" 
AR Path="/5EA29723/5EAA2361" Ref="U?"  Part="3" 
AR Path="/5EA2979B/5EAA2361" Ref="U?"  Part="3" 
AR Path="/5EA297F2/5EAA2361" Ref="U?"  Part="3" 
F 0 "U5" H 15600 9300 50  0000 L CNN
F 1 "74LS20" H 15550 9225 50  0000 L CNN
F 2 "" H 15675 9250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS20" H 15675 9250 50  0001 C CNN
	3    15675 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1950 1350 1950
Wire Wire Line
	1050 3450 1350 3450
Wire Wire Line
	1050 2150 1350 2150
Wire Wire Line
	1050 2450 1350 2450
NoConn ~ 1925 2150
Wire Wire Line
	1050 3850 1350 3850
NoConn ~ 1050 1050
NoConn ~ 1050 3950
Wire Wire Line
	2825 2050 3125 2050
Wire Wire Line
	1050 2350 1350 2350
Wire Wire Line
	2825 1950 3125 1950
Wire Wire Line
	1925 2750 2225 2750
NoConn ~ 2825 3050
Text Label 1350 3750 2    50   ~ 0
AD6
Text Label 1350 3850 2    50   ~ 0
AD7
Text Label 1350 3650 2    50   ~ 0
AD5
Text Label 1350 3550 2    50   ~ 0
AD4
Text Label 1350 3450 2    50   ~ 0
AD3
Text Label 1350 3350 2    50   ~ 0
AD2
Text Label 1350 3250 2    50   ~ 0
AD1
Text Label 1350 3150 2    50   ~ 0
AD0
Text Label 1350 2650 2    50   ~ 0
AA15
Text Label 1350 2150 2    50   ~ 0
AA10
Text Label 1350 2350 2    50   ~ 0
AA12
Text Label 1350 2250 2    50   ~ 0
AA11
Text Label 1350 2550 2    50   ~ 0
AA14
Text Label 1350 2450 2    50   ~ 0
AA13
Text Label 1350 1950 2    50   ~ 0
AA8
Text Label 1350 1550 2    50   ~ 0
AA4
Text Label 1350 2050 2    50   ~ 0
AA9
Text Label 1350 1650 2    50   ~ 0
AA5
Text Label 1350 1850 2    50   ~ 0
AA7
Text Label 1350 1750 2    50   ~ 0
AA6
Text Label 2225 2750 2    50   ~ 0
PH2b
Text Label 1350 1250 2    50   ~ 0
AA1
Text Label 1350 1350 2    50   ~ 0
AA2
Text Label 1350 1450 2    50   ~ 0
AA3
Text Label 1350 1150 2    50   ~ 0
AA0
NoConn ~ 2825 2950
NoConn ~ 2825 2850
NoConn ~ 2825 2750
NoConn ~ 1925 3050
NoConn ~ 1050 2750
Wire Wire Line
	1050 2050 1350 2050
Wire Wire Line
	1050 1450 1350 1450
Wire Wire Line
	1050 950  1350 950 
Wire Wire Line
	1050 3150 1350 3150
NoConn ~ 1925 1850
Wire Wire Line
	1100 4050 1100 4100
Wire Wire Line
	1050 1250 1350 1250
NoConn ~ 1925 1750
Wire Wire Line
	2225 950  2225 825 
NoConn ~ 1050 2850
NoConn ~ 1925 1050
Wire Wire Line
	1050 4050 1100 4050
Wire Wire Line
	1350 950  1350 825 
Wire Wire Line
	1050 3250 1350 3250
NoConn ~ 1925 3250
Wire Wire Line
	1050 1550 1350 1550
NoConn ~ 1925 1650
Wire Wire Line
	1050 1350 1350 1350
Wire Wire Line
	1050 1150 1350 1150
NoConn ~ 1925 2350
NoConn ~ 1925 2250
Wire Wire Line
	1925 4050 1975 4050
NoConn ~ 1925 2450
Wire Wire Line
	1975 4050 1975 4100
NoConn ~ 1925 3150
Wire Wire Line
	1050 2550 1350 2550
NoConn ~ 1925 1550
Wire Wire Line
	1050 3550 1350 3550
Wire Wire Line
	1925 950  2225 950 
Wire Wire Line
	1050 3350 1350 3350
NoConn ~ 1925 3450
NoConn ~ 1925 3550
Wire Wire Line
	1050 1850 1350 1850
NoConn ~ 1925 2550
NoConn ~ 1925 1450
Wire Wire Line
	1050 2650 1350 2650
Wire Wire Line
	1050 2250 1350 2250
NoConn ~ 1925 2050
Wire Wire Line
	1050 1650 1350 1650
Wire Wire Line
	1050 1750 1350 1750
Wire Wire Line
	1050 3650 1350 3650
Wire Wire Line
	1050 3750 1350 3750
NoConn ~ 1925 1950
NoConn ~ 2825 2250
NoConn ~ 2825 2350
NoConn ~ 2825 2450
NoConn ~ 2825 2650
NoConn ~ 2825 1250
NoConn ~ 2825 1350
NoConn ~ 2825 950 
NoConn ~ 2825 1650
NoConn ~ 2825 1750
NoConn ~ 2825 4050
NoConn ~ 2825 2550
NoConn ~ 2825 3550
NoConn ~ 2825 1450
NoConn ~ 2825 1550
Wire Wire Line
	2825 2150 3125 2150
NoConn ~ 2825 3950
$Comp
L power:GND #PWR?
U 1 1 5EB2C5FE
P 1975 4100
AR Path="/5E84A03F/5EB2C5FE" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EB2C5FE" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EB2C5FE" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C5FE" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C5FE" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C5FE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1975 3850 50  0001 C CNN
F 1 "GND" H 1975 3950 50  0000 C CNN
F 2 "" H 1975 4100 50  0001 C CNN
F 3 "" H 1975 4100 50  0001 C CNN
	1    1975 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB2C605
P 2225 825
AR Path="/5E84A03F/5EB2C605" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EB2C605" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EB2C605" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C605" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C605" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C605" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2225 675 50  0001 C CNN
F 1 "+5V" H 2240 998 50  0000 C CNN
F 2 "" H 2225 825 50  0001 C CNN
F 3 "" H 2225 825 50  0001 C CNN
	1    2225 825 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA2362
P 1350 825
AR Path="/5E84A03F/5EAA2362" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EAA2362" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EAA2362" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2362" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2362" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2362" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1350 675 50  0001 C CNN
F 1 "+5V" H 1365 998 50  0000 C CNN
F 2 "" H 1350 825 50  0001 C CNN
F 3 "" H 1350 825 50  0001 C CNN
	1    1350 825 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EA2E398
P 1100 4100
AR Path="/5E84A03F/5EA2E398" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EA2E398" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EA2E398" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EA2E398" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EA2E398" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EA2E398" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1100 3850 50  0001 C CNN
F 1 "GND" H 1100 3950 50  0000 C CNN
F 2 "" H 1100 4100 50  0001 C CNN
F 3 "" H 1100 4100 50  0001 C CNN
	1    1100 4100
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 1 1 5EAA235C
P 850 2500
AR Path="/5E84A03F/5EAA235C" Ref="P?"  Part="1" 
AR Path="/5EAB9D7B/5EAA235C" Ref="P?"  Part="1" 
AR Path="/5E83BCE9/5EAA235C" Ref="P?"  Part="1" 
AR Path="/5EA29723/5EAA235C" Ref="P?"  Part="1" 
AR Path="/5EA2979B/5EAA235C" Ref="P?"  Part="1" 
AR Path="/5EA297F2/5EAA235C" Ref="P?"  Part="1" 
AR Path="/5EAA235C" Ref="P?"  Part="1" 
F 0 "P?" H 767 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 767 816 50  0000 C CNN
F 2 "" H 850 2500 50  0000 C CNN
F 3 "" H 850 2500 50  0000 C CNN
	1    850  2500
	-1   0    0    1   
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 3 1 5EA2E395
P 1725 2500
AR Path="/5E84A03F/5EA2E395" Ref="P?"  Part="3" 
AR Path="/5EAB9D7B/5EA2E395" Ref="P?"  Part="2" 
AR Path="/5E83BCE9/5EA2E395" Ref="P?"  Part="3" 
AR Path="/5EA29723/5EA2E395" Ref="P?"  Part="3" 
AR Path="/5EA2979B/5EA2E395" Ref="P?"  Part="3" 
AR Path="/5EA297F2/5EA2E395" Ref="P?"  Part="3" 
AR Path="/5EA2E395" Ref="P?"  Part="3" 
F 0 "P?" H 1642 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 1642 816 50  0000 C CNN
F 2 "" H 1725 2500 50  0000 C CNN
F 3 "" H 1725 2500 50  0000 C CNN
	3    1725 2500
	-1   0    0    1   
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 2 1 5EA2E396
P 2625 2500
AR Path="/5E84A03F/5EA2E396" Ref="P?"  Part="2" 
AR Path="/5EAB9D7B/5EA2E396" Ref="P?"  Part="3" 
AR Path="/5E83BCE9/5EA2E396" Ref="P?"  Part="2" 
AR Path="/5EA29723/5EA2E396" Ref="P?"  Part="2" 
AR Path="/5EA2979B/5EA2E396" Ref="P?"  Part="2" 
AR Path="/5EA297F2/5EA2E396" Ref="P?"  Part="2" 
AR Path="/5EA2E396" Ref="P?"  Part="2" 
F 0 "P?" H 2542 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 2542 816 50  0000 C CNN
F 2 "" H 2625 2500 50  0000 C CNN
F 3 "" H 2625 2500 50  0000 C CNN
	2    2625 2500
	-1   0    0    1   
$EndComp
NoConn ~ 2825 1050
NoConn ~ 2825 1150
Wire Wire Line
	2825 1850 3125 1850
NoConn ~ 2825 3650
NoConn ~ 2825 3750
NoConn ~ 2825 3850
Text Label 4850 1725 0    50   ~ 0
AD7
Text Label 4850 1425 0    50   ~ 0
AD4
Text Label 4850 1225 0    50   ~ 0
AD2
Text Label 4850 1125 0    50   ~ 0
AD1
Text Label 4850 1025 0    50   ~ 0
AD0
Text Label 4850 1625 0    50   ~ 0
AD6
Text Label 4850 1325 0    50   ~ 0
AD3
Text Label 4850 1525 0    50   ~ 0
AD5
Wire Wire Line
	4850 1025 5100 1025
Wire Wire Line
	4850 1125 5100 1125
Wire Wire Line
	4850 1225 5100 1225
Wire Wire Line
	4850 1325 5100 1325
Wire Wire Line
	4850 1425 5100 1425
Wire Wire Line
	4850 1525 5100 1525
Wire Wire Line
	4850 1625 5100 1625
Wire Wire Line
	4850 1725 5100 1725
Wire Wire Line
	4750 2025 5100 2025
Wire Wire Line
	5075 2950 5325 2950
Wire Wire Line
	5075 3450 5325 3450
Wire Wire Line
	5075 3150 5325 3150
Wire Wire Line
	5075 2850 5325 2850
Wire Wire Line
	5075 3350 5325 3350
Wire Wire Line
	5075 3050 5325 3050
Wire Wire Line
	5075 2750 5325 2750
Wire Wire Line
	5075 3250 5325 3250
Wire Wire Line
	5275 5200 5525 5200
Wire Wire Line
	5275 4800 5525 4800
Wire Wire Line
	5275 4500 5525 4500
Wire Wire Line
	5275 5000 5525 5000
Wire Wire Line
	5275 4700 5525 4700
Wire Wire Line
	5275 4900 5525 4900
Wire Wire Line
	5275 4600 5525 4600
Wire Wire Line
	5275 5100 5525 5100
Text Label 5275 4500 0    50   ~ 0
AA0
Text Label 5275 5200 0    50   ~ 0
AA7
Text Label 5075 2950 0    50   ~ 0
AA10
Text Label 5275 4700 0    50   ~ 0
AA2
Text Label 5075 3050 0    50   ~ 0
AA11
Text Label 5275 5100 0    50   ~ 0
AA6
Text Label 5075 3150 0    50   ~ 0
AA12
Text Label 5275 5000 0    50   ~ 0
AA5
Text Label 5275 4900 0    50   ~ 0
AA4
Text Label 5075 2850 0    50   ~ 0
AA9
Text Label 5275 4800 0    50   ~ 0
AA3
Text Label 5075 2750 0    50   ~ 0
AA8
Text Label 5075 3450 0    50   ~ 0
AA15
Text Label 5275 4600 0    50   ~ 0
AA1
Text Label 5075 3250 0    50   ~ 0
AA13
Text Label 5075 3350 0    50   ~ 0
AA14
$Comp
L power:GND #PWR?
U 1 1 5EAA2364
P 5300 3800
AR Path="/5E83BCE9/5EAA2364" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2364" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2364" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2364" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5300 3550 50  0001 C CNN
F 1 "GND" H 5300 3650 50  0000 C CNN
F 2 "" H 5300 3800 50  0001 C CNN
F 3 "" H 5300 3800 50  0001 C CNN
	1    5300 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3800 5300 3750
Wire Wire Line
	5300 3750 5325 3750
$Comp
L power:GND #PWR?
U 1 1 5EAA2365
P 5500 5550
AR Path="/5E83BCE9/5EAA2365" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2365" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2365" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2365" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5500 5300 50  0001 C CNN
F 1 "GND" H 5500 5400 50  0000 C CNN
F 2 "" H 5500 5550 50  0001 C CNN
F 3 "" H 5500 5550 50  0001 C CNN
	1    5500 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 5550 5500 5500
Wire Wire Line
	5500 5500 5525 5500
$Comp
L power:+5V #PWR?
U 1 1 5EB2C61C
P 4925 3550
AR Path="/5E83BCE9/5EB2C61C" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C61C" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C61C" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C61C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4925 3400 50  0001 C CNN
F 1 "+5V" H 4940 3723 50  0000 C CNN
F 2 "" H 4925 3550 50  0001 C CNN
F 3 "" H 4925 3550 50  0001 C CNN
	1    4925 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4925 3550 4925 3650
Wire Wire Line
	4925 3650 5325 3650
$Comp
L power:+5V #PWR?
U 1 1 5EAA2367
P 5150 5275
AR Path="/5E83BCE9/5EAA2367" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2367" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2367" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2367" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5150 5125 50  0001 C CNN
F 1 "+5V" H 5165 5448 50  0000 C CNN
F 2 "" H 5150 5275 50  0001 C CNN
F 3 "" H 5150 5275 50  0001 C CNN
	1    5150 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5275 5150 5400
Wire Wire Line
	5150 5400 5525 5400
$Comp
L 74xx:74LS30 U9
U 1 1 5EAA2368
P 14425 5450
AR Path="/5E83BCE9/5EAA2368" Ref="U9"  Part="1" 
AR Path="/5EA29723/5EAA2368" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA2368" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA2368" Ref="U?"  Part="1" 
F 0 "U9" H 14425 5975 50  0000 C CNN
F 1 "74LS30" H 14425 5884 50  0000 C CNN
F 2 "" H 14425 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS30" H 14425 5450 50  0001 C CNN
	1    14425 5450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS30 U9
U 2 1 5EAA2369
P 14775 9250
AR Path="/5E83BCE9/5EAA2369" Ref="U9"  Part="2" 
AR Path="/5EA29723/5EAA2369" Ref="U?"  Part="2" 
AR Path="/5EA2979B/5EAA2369" Ref="U?"  Part="2" 
AR Path="/5EA297F2/5EAA2369" Ref="U?"  Part="2" 
F 0 "U9" H 14700 9300 50  0000 L CNN
F 1 "74LS30" H 14650 9225 50  0000 L CNN
F 2 "" H 14775 9250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS30" H 14775 9250 50  0001 C CNN
	2    14775 9250
	1    0    0    -1  
$EndComp
Connection ~ 14775 8425
Wire Wire Line
	14775 8425 14775 8375
Wire Wire Line
	14900 8425 14775 8425
Wire Wire Line
	14775 8425 14775 8750
Wire Wire Line
	14900 8450 14900 8425
$Comp
L power:GND #PWR?
U 1 1 5EAA236A
P 14900 8650
AR Path="/5E83BCE9/5EAA236A" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA236A" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA236A" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA236A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14900 8400 50  0001 C CNN
F 1 "GND" H 14900 8500 50  0000 C CNN
F 2 "" H 14900 8650 50  0001 C CNN
F 3 "" H 14900 8650 50  0001 C CNN
	1    14900 8650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA236B
P 14775 8375
AR Path="/5E83BCE9/5EAA236B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA236B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA236B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA236B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14775 8225 50  0001 C CNN
F 1 "+5V" H 14790 8548 50  0000 C CNN
F 2 "" H 14775 8375 50  0001 C CNN
F 3 "" H 14775 8375 50  0001 C CNN
	1    14775 8375
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EB2C622
P 14900 8550
AR Path="/5E83BCE9/5EB2C622" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EB2C622" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EB2C622" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EB2C622" Ref="C?"  Part="1" 
F 0 "C?" H 14992 8596 50  0000 L CNN
F 1 "22n" H 14992 8505 50  0000 L CNN
F 2 "" H 14900 8550 50  0001 C CNN
F 3 "~" H 14900 8550 50  0001 C CNN
	1    14900 8550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EB2C623
P 14775 9750
AR Path="/5E83BCE9/5EB2C623" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C623" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C623" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C623" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14775 9500 50  0001 C CNN
F 1 "GND" H 14775 9600 50  0000 C CNN
F 2 "" H 14775 9750 50  0001 C CNN
F 3 "" H 14775 9750 50  0001 C CNN
	1    14775 9750
	1    0    0    -1  
$EndComp
Wire Wire Line
	12925 1975 13200 1975
Wire Wire Line
	13200 1975 13200 5650
Wire Wire Line
	13200 5650 14125 5650
Wire Wire Line
	12900 4650 13300 4650
Wire Wire Line
	13300 4650 13300 5550
Wire Wire Line
	13300 5550 14125 5550
Wire Wire Line
	14125 5350 13375 5350
Wire Wire Line
	13375 6850 12925 6850
Wire Wire Line
	13375 5350 13375 6850
Wire Wire Line
	14125 5450 13450 5450
Wire Wire Line
	13450 5450 13450 9525
Wire Wire Line
	13450 9525 12900 9525
Wire Wire Line
	14125 5850 13550 5850
Wire Wire Line
	13550 5850 13550 5750
Wire Wire Line
	13550 5250 14125 5250
Wire Wire Line
	13550 5250 13550 5000
Connection ~ 13550 5250
$Comp
L power:+5V #PWR?
U 1 1 5EAA236E
P 13550 5000
AR Path="/5E83BCE9/5EAA236E" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA236E" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA236E" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA236E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13550 4850 50  0001 C CNN
F 1 "+5V" H 13565 5173 50  0000 C CNN
F 2 "" H 13550 5000 50  0001 C CNN
F 3 "" H 13550 5000 50  0001 C CNN
	1    13550 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	14125 5750 13550 5750
Connection ~ 13550 5750
Wire Wire Line
	13550 5750 13550 5250
$Comp
L 74xx:74LS00 U4
U 2 1 5EAA2370
P 15100 5450
AR Path="/5E83BCE9/5EAA2370" Ref="U4"  Part="2" 
AR Path="/5EA29723/5EAA2370" Ref="U?"  Part="2" 
AR Path="/5EA2979B/5EAA2370" Ref="U?"  Part="2" 
AR Path="/5EA297F2/5EAA2370" Ref="U?"  Part="2" 
F 0 "U4" H 15100 5775 50  0000 C CNN
F 1 "74LS00" H 15100 5684 50  0000 C CNN
F 2 "" H 15100 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 15100 5450 50  0001 C CNN
	2    15100 5450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U4
U 5 1 5EAA2373
P 15225 9250
AR Path="/5E83BCE9/5EAA2373" Ref="U4"  Part="5" 
AR Path="/5EA29723/5EAA2373" Ref="U?"  Part="5" 
AR Path="/5EA2979B/5EAA2373" Ref="U?"  Part="5" 
AR Path="/5EA297F2/5EAA2373" Ref="U?"  Part="5" 
F 0 "U4" H 15150 9300 50  0000 L CNN
F 1 "74LS00" H 15075 9225 50  0000 L CNN
F 2 "" H 15225 9250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 15225 9250 50  0001 C CNN
	5    15225 9250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EFEECD2
P 15350 8650
AR Path="/5E83BCE9/5EFEECD2" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EFEECD2" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EFEECD2" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EFEECD2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15350 8400 50  0001 C CNN
F 1 "GND" H 15350 8500 50  0000 C CNN
F 2 "" H 15350 8650 50  0001 C CNN
F 3 "" H 15350 8650 50  0001 C CNN
	1    15350 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	15350 8450 15350 8425
Wire Wire Line
	15350 8425 15225 8425
Wire Wire Line
	15225 8425 15225 8375
Wire Wire Line
	15225 8425 15225 8750
$Comp
L power:+5V #PWR?
U 1 1 5EB2C62B
P 15225 8375
AR Path="/5E83BCE9/5EB2C62B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C62B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C62B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C62B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15225 8225 50  0001 C CNN
F 1 "+5V" H 15240 8548 50  0000 C CNN
F 2 "" H 15225 8375 50  0001 C CNN
F 3 "" H 15225 8375 50  0001 C CNN
	1    15225 8375
	1    0    0    -1  
$EndComp
Connection ~ 15225 8425
$Comp
L power:GND #PWR?
U 1 1 5EB2C62C
P 15225 9750
AR Path="/5E83BCE9/5EB2C62C" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB2C62C" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB2C62C" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB2C62C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15225 9500 50  0001 C CNN
F 1 "GND" H 15225 9600 50  0000 C CNN
F 2 "" H 15225 9750 50  0001 C CNN
F 3 "" H 15225 9750 50  0001 C CNN
	1    15225 9750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFEECFF
P 15350 8550
AR Path="/5E83BCE9/5EFEECFF" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EFEECFF" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EFEECFF" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EFEECFF" Ref="C?"  Part="1" 
F 0 "C?" H 15442 8596 50  0000 L CNN
F 1 "22n" H 15442 8505 50  0000 L CNN
F 2 "" H 15350 8550 50  0001 C CNN
F 3 "~" H 15350 8550 50  0001 C CNN
	1    15350 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	14800 5350 14775 5350
Wire Wire Line
	14775 5350 14775 5450
Wire Wire Line
	14775 5550 14800 5550
Connection ~ 14775 5450
Wire Wire Line
	14775 5450 14775 5550
Wire Wire Line
	15400 5450 15750 5450
Text Label 15750 5450 2    50   ~ 0
~CE~_DATA
Text Label 4750 2025 0    50   ~ 0
~CE~_DATA
Wire Wire Line
	15800 8425 15675 8425
Wire Wire Line
	15800 8450 15800 8425
$Comp
L power:GND #PWR?
U 1 1 5EAA2378
P 15675 9750
AR Path="/5E83BCE9/5EAA2378" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2378" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2378" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2378" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15675 9500 50  0001 C CNN
F 1 "GND" H 15675 9600 50  0000 C CNN
F 2 "" H 15675 9750 50  0001 C CNN
F 3 "" H 15675 9750 50  0001 C CNN
	1    15675 9750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAA2379
P 15800 8650
AR Path="/5E83BCE9/5EAA2379" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA2379" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA2379" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA2379" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15800 8400 50  0001 C CNN
F 1 "GND" H 15800 8500 50  0000 C CNN
F 2 "" H 15800 8650 50  0001 C CNN
F 3 "" H 15800 8650 50  0001 C CNN
	1    15800 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	15675 8425 15675 8375
Wire Wire Line
	15675 8425 15675 8750
Connection ~ 15675 8425
$Comp
L Device:C_Small C?
U 1 1 5EB2C630
P 15800 8550
AR Path="/5E83BCE9/5EB2C630" Ref="C?"  Part="1" 
AR Path="/5EA29723/5EB2C630" Ref="C?"  Part="1" 
AR Path="/5EA2979B/5EB2C630" Ref="C?"  Part="1" 
AR Path="/5EA297F2/5EB2C630" Ref="C?"  Part="1" 
F 0 "C?" H 15892 8596 50  0000 L CNN
F 1 "22n" H 15892 8505 50  0000 L CNN
F 2 "" H 15800 8550 50  0001 C CNN
F 3 "~" H 15800 8550 50  0001 C CNN
	1    15800 8550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA237B
P 15675 8375
AR Path="/5E83BCE9/5EAA237B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EAA237B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EAA237B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EAA237B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15675 8225 50  0001 C CNN
F 1 "+5V" H 15690 8548 50  0000 C CNN
F 2 "" H 15675 8375 50  0001 C CNN
F 3 "" H 15675 8375 50  0001 C CNN
	1    15675 8375
	1    0    0    -1  
$EndComp
Text Label 1900 7600 2    50   ~ 0
~SW_EN
Wire Wire Line
	1900 7600 1650 7600
Wire Wire Line
	900  7600 700  7600
Wire Wire Line
	900  7700 700  7700
Wire Wire Line
	900  7800 700  7800
Wire Wire Line
	900  7900 700  7900
Wire Wire Line
	900  8000 700  8000
Wire Wire Line
	900  8100 700  8100
Wire Wire Line
	900  8200 700  8200
Wire Wire Line
	900  8300 700  8300
Wire Wire Line
	900  8400 875  8400
Wire Wire Line
	900  8500 875  8500
Text Label 700  8200 0    50   ~ 0
A14
Text Label 700  7800 0    50   ~ 0
A10
Text Label 700  7900 0    50   ~ 0
A11
Text Label 700  7600 0    50   ~ 0
A8
Text Label 700  8000 0    50   ~ 0
A12
Text Label 700  7700 0    50   ~ 0
A9
Text Label 700  8100 0    50   ~ 0
A13
Text Label 700  8300 0    50   ~ 0
A15
Wire Wire Line
	875  8400 875  8500
Connection ~ 875  8500
Wire Wire Line
	12175 1775 11900 1775
Text Label 11900 1775 0    50   ~ 0
~BANK0
Text Label 11875 4450 0    50   ~ 0
~BANK1
Wire Wire Line
	12150 4450 11875 4450
Text Label 3125 1950 2    50   ~ 0
~BANK1
Text Label 3125 1850 2    50   ~ 0
~BANK0
Text Label 3125 2050 2    50   ~ 0
~BANK2
Text Label 3125 2150 2    50   ~ 0
~BANK3
Text Label 11900 6650 0    50   ~ 0
~BANK2
Text Label 11875 9325 0    50   ~ 0
~BANK3
Wire Wire Line
	11900 6650 12175 6650
Wire Wire Line
	11875 9325 12150 9325
$Comp
L Device:R_Network07 RN?
U 1 1 5EAA2354
P 15475 2375
AR Path="/5E83BCE9/5EAA2354" Ref="RN?"  Part="1" 
AR Path="/5EA29723/5EAA2354" Ref="RN?"  Part="1" 
AR Path="/5EA2979B/5EAA2354" Ref="RN?"  Part="1" 
AR Path="/5EA297F2/5EAA2354" Ref="RN?"  Part="1" 
F 0 "RN?" H 15863 2421 50  0000 L CNN
F 1 "1k" H 15863 2330 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP8" V 15950 2375 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 15475 2375 50  0001 C CNN
	1    15475 2375
	1    0    0    -1  
$EndComp
Text Label 15575 2850 1    50   ~ 0
~SW_EN
Wire Wire Line
	15575 2850 15575 2575
Text Label 13025 6850 0    50   ~ 0
PLUP1
Text Label 15675 2850 1    50   ~ 0
PLUP1
Wire Wire Line
	15675 2575 15675 2850
Text Label 13050 9525 0    50   ~ 0
PLUP2
Text Label 15775 2850 1    50   ~ 0
PLUP2
Wire Wire Line
	15775 2850 15775 2575
Text Label 12925 4650 0    50   ~ 0
PLUP3
Text Label 15600 3675 1    50   ~ 0
PLUP3
Text Label 12950 1975 0    50   ~ 0
PLUP4
Text Label 14750 3650 1    50   ~ 0
PLUP4
Wire Wire Line
	15600 3425 15600 3675
Wire Wire Line
	14750 3400 14750 3650
NoConn ~ 14750 2550
Text Label 550  8500 0    50   ~ 0
~BANK3
Wire Wire Line
	550  8500 875  8500
NoConn ~ 1650 7700
NoConn ~ 1650 7800
NoConn ~ 1650 7900
Wire Wire Line
	14725 5450 14775 5450
Wire Wire Line
	13775 5150 14125 5150
Text Label 13775 5150 0    50   ~ 0
~SW_EN
Text Label 4750 1925 0    50   ~ 0
~R~Wb
Wire Wire Line
	4750 1925 5100 1925
Text Notes 14975 5475 0    50   ~ 0
NAND\n
Text Notes 14300 5475 0    50   ~ 0
NAND\n
Text Label 3625 5075 0    50   ~ 0
L3
Text Label 3625 4675 0    50   ~ 0
L7
Text Label 3625 4975 0    50   ~ 0
L4
Text Label 3625 5375 0    50   ~ 0
L0
Text Label 3625 5275 0    50   ~ 0
L1
Text Label 3625 5175 0    50   ~ 0
L2
Text Label 5025 4675 2    50   ~ 0
LED7
Wire Wire Line
	4775 4775 5025 4775
Wire Wire Line
	4775 4675 5025 4675
Text Label 5025 4875 2    50   ~ 0
LED5
Text Label 5025 5375 2    50   ~ 0
LED0
Text Label 5025 4975 2    50   ~ 0
LED4
Wire Wire Line
	4775 5175 5025 5175
Wire Wire Line
	4775 4875 5025 4875
Text Label 5025 5075 2    50   ~ 0
LED3
Wire Wire Line
	4775 4975 5025 4975
Text Label 3625 4875 0    50   ~ 0
L5
$Comp
L es65-rescue:DM81LS95-my_ics U7
U 1 1 5E8AA364
P 4275 5175
AR Path="/5E83BCE9/5E8AA364" Ref="U7"  Part="1" 
AR Path="/5EA29723/5E8AA364" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E8AA364" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E8AA364" Ref="U?"  Part="1" 
F 0 "U7" H 3775 5925 50  0000 C CNN
F 1 "DM81LS95" H 3925 5850 50  0000 C CNN
F 2 "" H 4275 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT240.pdf" H 4275 5175 50  0001 C CNN
	1    4275 5175
	1    0    0    -1  
$EndComp
Text Label 3625 4775 0    50   ~ 0
L6
Wire Wire Line
	4775 5375 5025 5375
Wire Wire Line
	4775 5275 5025 5275
Text Label 5025 5175 2    50   ~ 0
LED2
Text Label 5025 4775 2    50   ~ 0
LED6
Wire Wire Line
	4775 5075 5025 5075
Text Label 5025 5275 2    50   ~ 0
LED1
Connection ~ 3750 5675
Wire Wire Line
	3750 5575 3750 5675
Wire Wire Line
	3775 5575 3750 5575
Wire Wire Line
	3625 5375 3775 5375
Wire Wire Line
	3625 4975 3775 4975
Wire Wire Line
	3625 5275 3775 5275
Wire Wire Line
	3750 5675 3775 5675
$Comp
L power:GND #PWR?
U 1 1 5F047DF4
P 3750 5725
AR Path="/5E83BCE9/5F047DF4" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F047DF4" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F047DF4" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F047DF4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3750 5475 50  0001 C CNN
F 1 "GND" H 3750 5575 50  0000 C CNN
F 2 "" H 3750 5725 50  0001 C CNN
F 3 "" H 3750 5725 50  0001 C CNN
	1    3750 5725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5725 3750 5675
Wire Wire Line
	3625 5075 3775 5075
Wire Wire Line
	3625 4675 3775 4675
Wire Wire Line
	3625 5175 3775 5175
Wire Wire Line
	3625 4775 3775 4775
$Comp
L power:GND #PWR?
U 1 1 5F047DDD
P 4275 5975
AR Path="/5E83BCE9/5F047DDD" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F047DDD" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F047DDD" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F047DDD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4275 5725 50  0001 C CNN
F 1 "GND" H 4275 5825 50  0000 C CNN
F 2 "" H 4275 5975 50  0001 C CNN
F 3 "" H 4275 5975 50  0001 C CNN
	1    4275 5975
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F047DD0
P 4275 4375
AR Path="/5E83BCE9/5F047DD0" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F047DD0" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F047DD0" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F047DD0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4275 4225 50  0001 C CNN
F 1 "+5V" H 4290 4548 50  0000 C CNN
F 2 "" H 4275 4375 50  0001 C CNN
F 3 "" H 4275 4375 50  0001 C CNN
	1    4275 4375
	1    0    0    -1  
$EndComp
Wire Wire Line
	3625 4875 3775 4875
Text Label 1500 5350 0    50   ~ 0
~R~Wb
Wire Wire Line
	650  6325 900  6325
Wire Wire Line
	1500 5900 1725 5900
Text Label 1725 5900 2    50   ~ 0
~R~Wb
Wire Wire Line
	1500 5350 1650 5350
Text Label 1700 6950 2    50   ~ 0
~OE
Wire Wire Line
	1700 6950 1500 6950
Text Label 1700 6425 2    50   ~ 0
~WE
Wire Wire Line
	1700 6425 1500 6425
Text Label 625  6850 0    50   ~ 0
PH2b
Wire Wire Line
	1500 5250 1650 5250
Wire Wire Line
	1500 5150 1650 5150
Text Label 625  7050 0    50   ~ 0
R{slash}Wb
Text Label 1500 5150 0    50   ~ 0
A7
Text Notes 1075 6450 0    50   ~ 0
NAND\n
Text Notes 1075 5275 0    50   ~ 0
NAND\n
Text Notes 1850 5300 0    50   ~ 0
NAND\n
Wire Wire Line
	625  6850 900  6850
Wire Wire Line
	625  6525 900  6525
Text Notes 1075 5925 0    50   ~ 0
NAND\n
Text Label 625  6525 0    50   ~ 0
PH2b
Wire Wire Line
	1350 5450 1650 5450
Text Label 1350 5450 0    50   ~ 0
PH2b
Text Notes 1075 6975 0    50   ~ 0
NAND\n
Text Label 650  6325 0    50   ~ 0
~R~Wb
Text Label 575  5100 0    50   ~ 0
~SW_EN
Wire Wire Line
	675  5800 850  5800
Text Label 675  5800 0    50   ~ 0
R{slash}Wb
Wire Wire Line
	625  7050 900  7050
Connection ~ 850  5800
Wire Wire Line
	575  5100 850  5100
Connection ~ 850  5100
Wire Wire Line
	850  5100 850  5200
Wire Wire Line
	900  5200 850  5200
Wire Wire Line
	850  5300 850  5400
Connection ~ 850  5200
Connection ~ 850  5300
$Comp
L 74xx:74LS20 U5
U 1 1 5EAA235F
P 1200 5250
AR Path="/5E83BCE9/5EAA235F" Ref="U5"  Part="1" 
AR Path="/5EA29723/5EAA235F" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA235F" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA235F" Ref="U?"  Part="1" 
F 0 "U5" H 1200 5625 50  0000 C CNN
F 1 "74LS20" H 1200 5534 50  0000 C CNN
F 2 "" H 1200 5250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS20" H 1200 5250 50  0001 C CNN
	1    1200 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  5200 850  5300
Wire Wire Line
	900  5300 850  5300
Wire Wire Line
	900  5100 850  5100
Wire Wire Line
	850  5400 900  5400
Wire Wire Line
	850  6000 900  6000
Wire Wire Line
	850  5800 850  6000
Wire Wire Line
	900  5800 850  5800
$Comp
L 74xx:74LS00 U4
U 4 1 5EB2C628
P 1200 6425
AR Path="/5E83BCE9/5EB2C628" Ref="U4"  Part="4" 
AR Path="/5EA29723/5EB2C628" Ref="U?"  Part="4" 
AR Path="/5EA2979B/5EB2C628" Ref="U?"  Part="4" 
AR Path="/5EA297F2/5EB2C628" Ref="U?"  Part="4" 
F 0 "U4" H 1200 6750 50  0000 C CNN
F 1 "74LS00" H 1200 6659 50  0000 C CNN
F 2 "" H 1200 6425 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1200 6425 50  0001 C CNN
	4    1200 6425
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U4
U 3 1 5EAA2371
P 1200 6950
AR Path="/5E83BCE9/5EAA2371" Ref="U4"  Part="3" 
AR Path="/5EA29723/5EAA2371" Ref="U?"  Part="3" 
AR Path="/5EA2979B/5EAA2371" Ref="U?"  Part="3" 
AR Path="/5EA297F2/5EAA2371" Ref="U?"  Part="3" 
F 0 "U4" H 1200 7275 50  0000 C CNN
F 1 "74LS00" H 1200 7184 50  0000 C CNN
F 2 "" H 1200 6950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1200 6950 50  0001 C CNN
	3    1200 6950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U4
U 1 1 5EAA236F
P 1200 5900
AR Path="/5E83BCE9/5EAA236F" Ref="U4"  Part="1" 
AR Path="/5EA29723/5EAA236F" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5EAA236F" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5EAA236F" Ref="U?"  Part="1" 
F 0 "U4" H 1200 6225 50  0000 C CNN
F 1 "74LS00" H 1200 6134 50  0000 C CNN
F 2 "" H 1200 5900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1200 5900 50  0001 C CNN
	1    1200 5900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS20 U5
U 2 1 5EAA2360
P 1950 5300
AR Path="/5E83BCE9/5EAA2360" Ref="U5"  Part="2" 
AR Path="/5EA29723/5EAA2360" Ref="U?"  Part="2" 
AR Path="/5EA2979B/5EAA2360" Ref="U?"  Part="2" 
AR Path="/5EA297F2/5EAA2360" Ref="U?"  Part="2" 
F 0 "U5" H 1950 5675 50  0000 C CNN
F 1 "74LS20" H 1950 5584 50  0000 C CNN
F 2 "" H 1950 5300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS20" H 1950 5300 50  0001 C CNN
	2    1950 5300
	1    0    0    -1  
$EndComp
Text Label 2300 9750 2    50   ~ 0
D0
Text Label 2300 9850 2    50   ~ 0
D1
Text Label 2300 9650 2    50   ~ 0
D7
Wire Wire Line
	2100 9950 2300 9950
Text Label 850  10650 0    50   ~ 0
A7
Wire Wire Line
	2100 10150 2300 10150
Wire Wire Line
	2100 10250 2300 10250
Text Label 2300 10350 2    50   ~ 0
D6
Wire Wire Line
	1100 10150 850  10150
Wire Wire Line
	1100 9750 850  9750
Wire Wire Line
	2100 9650 2300 9650
Wire Wire Line
	1100 10250 850  10250
Text Label 850  9650 0    50   ~ 0
SW7
Text Label 850  9950 0    50   ~ 0
SW4
Text Label 850  9850 0    50   ~ 0
SW1
Wire Wire Line
	1100 9950 850  9950
Wire Wire Line
	1100 10050 850  10050
Text Label 2300 10050 2    50   ~ 0
D3
Wire Wire Line
	850  10650 1100 10650
Text Label 2300 9950 2    50   ~ 0
D4
Wire Wire Line
	850  10550 1100 10550
Text Label 850  10550 0    50   ~ 0
~SW_EN
Wire Wire Line
	2100 10350 2300 10350
Wire Wire Line
	2100 9750 2300 9750
$Comp
L es65-rescue:DM81LS95-my_ics U8
U 1 1 5E8A5E47
P 1600 10150
AR Path="/5E83BCE9/5E8A5E47" Ref="U8"  Part="1" 
AR Path="/5EA29723/5E8A5E47" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5E8A5E47" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5E8A5E47" Ref="U?"  Part="1" 
F 0 "U8" H 1100 10900 50  0000 C CNN
F 1 "DM81LS95" H 1250 10825 50  0000 C CNN
F 2 "" H 1600 10150 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74HC_HCT240.pdf" H 1600 10150 50  0001 C CNN
	1    1600 10150
	1    0    0    -1  
$EndComp
Text Label 850  9750 0    50   ~ 0
SW0
Text Label 2300 10150 2    50   ~ 0
D2
Text Label 2300 10250 2    50   ~ 0
D5
Text Label 850  10050 0    50   ~ 0
SW3
Wire Wire Line
	1100 9650 850  9650
Wire Wire Line
	1100 9850 850  9850
Text Label 850  10250 0    50   ~ 0
SW5
Wire Wire Line
	1100 10350 850  10350
Text Label 850  10350 0    50   ~ 0
SW6
Text Label 850  10150 0    50   ~ 0
SW2
Wire Wire Line
	2100 9850 2300 9850
Wire Wire Line
	2100 10050 2300 10050
$Comp
L power:GND #PWR?
U 1 1 5F0435C3
P 1600 10950
AR Path="/5E83BCE9/5F0435C3" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F0435C3" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F0435C3" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F0435C3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1600 10700 50  0001 C CNN
F 1 "GND" H 1600 10800 50  0000 C CNN
F 2 "" H 1600 10950 50  0001 C CNN
F 3 "" H 1600 10950 50  0001 C CNN
	1    1600 10950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F042A2C
P 1600 9350
AR Path="/5E83BCE9/5F042A2C" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F042A2C" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F042A2C" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F042A2C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1600 9200 50  0001 C CNN
F 1 "+5V" H 1615 9523 50  0000 C CNN
F 2 "" H 1600 9350 50  0001 C CNN
F 3 "" H 1600 9350 50  0001 C CNN
	1    1600 9350
	1    0    0    -1  
$EndComp
Text HLabel 4075 3275 0    50   Input ~ 0
R{slash}Wb
Wire Wire Line
	4075 3275 4375 3275
Wire Wire Line
	4075 3175 4375 3175
Text Label 4375 3175 2    50   ~ 0
PH2b
Text HLabel 4075 3175 0    50   Input ~ 0
PH2b
Wire Wire Line
	4075 3500 4375 3500
Text Label 4375 3700 2    50   ~ 0
~BANK3
Text Label 4375 3400 2    50   ~ 0
~BANK0
Text HLabel 4075 3700 0    50   Input ~ 0
BANK3
Text HLabel 4075 3400 0    50   Input ~ 0
BANK0
Text Label 4375 3600 2    50   ~ 0
~BANK2
Text HLabel 4075 3500 0    50   Input ~ 0
BANK1
Text HLabel 4075 3600 0    50   Input ~ 0
BANK2
Text Label 4375 3500 2    50   ~ 0
~BANK1
Wire Wire Line
	4075 3400 4375 3400
Wire Wire Line
	4075 3700 4375 3700
Wire Wire Line
	4075 3600 4375 3600
Text Label 4350 2875 2    50   ~ 0
AD6
Text HLabel 4050 875  0    50   Input ~ 0
AA2
Text Label 4350 1975 2    50   ~ 0
AA13
Text Label 4350 2575 2    50   ~ 0
AD3
Text Label 4350 675  2    50   ~ 0
AA0
Text Label 4350 2075 2    50   ~ 0
AA14
Text Label 4350 2675 2    50   ~ 0
AD4
Text Label 4350 1375 2    50   ~ 0
AA7
Text Label 4350 975  2    50   ~ 0
AA3
Text Label 4350 875  2    50   ~ 0
AA2
Text Label 4350 775  2    50   ~ 0
AA1
Text Label 4350 1075 2    50   ~ 0
AA4
Wire Wire Line
	4050 1375 4350 1375
Wire Wire Line
	4050 1575 4350 1575
Wire Wire Line
	4050 2075 4350 2075
Wire Wire Line
	4050 1875 4350 1875
Wire Wire Line
	4050 2175 4350 2175
Text Label 4350 1475 2    50   ~ 0
AA8
Text Label 4350 2775 2    50   ~ 0
AD5
Text Label 4350 2175 2    50   ~ 0
AA15
Wire Wire Line
	4050 775  4350 775 
Wire Wire Line
	4050 675  4350 675 
Wire Wire Line
	4050 1675 4350 1675
Wire Wire Line
	4050 1775 4350 1775
Text Label 4350 1875 2    50   ~ 0
AA12
Wire Wire Line
	4050 1975 4350 1975
Wire Wire Line
	4050 1075 4350 1075
Wire Wire Line
	4050 1475 4350 1475
Text HLabel 4050 675  0    50   Input ~ 0
AA0
Text Label 4350 2275 2    50   ~ 0
AD0
Text Label 4350 1775 2    50   ~ 0
AA11
Text Label 4350 2375 2    50   ~ 0
AD1
Wire Wire Line
	4050 1175 4350 1175
Wire Wire Line
	4050 975  4350 975 
Text Label 4350 1675 2    50   ~ 0
AA10
Wire Wire Line
	4050 2875 4350 2875
Wire Wire Line
	4050 2975 4350 2975
Text HLabel 4050 775  0    50   Input ~ 0
AA1
Wire Wire Line
	4050 2275 4350 2275
Text Label 4350 1175 2    50   ~ 0
AA5
Wire Wire Line
	4050 2675 4350 2675
Wire Wire Line
	4050 875  4350 875 
Wire Wire Line
	4050 2575 4350 2575
Wire Wire Line
	4050 2775 4350 2775
Wire Wire Line
	4050 2375 4350 2375
Wire Wire Line
	4050 2475 4350 2475
Wire Wire Line
	4050 1275 4350 1275
Text HLabel 4050 1075 0    50   Input ~ 0
AA4
Text HLabel 4050 1175 0    50   Input ~ 0
AA5
Text HLabel 4050 1275 0    50   Input ~ 0
AA6
Text HLabel 4050 2275 0    50   Input ~ 0
AD0
Text HLabel 4050 1375 0    50   Input ~ 0
AA7
Text HLabel 4050 1775 0    50   Input ~ 0
AA11
Text HLabel 4050 1675 0    50   Input ~ 0
AA10
Text HLabel 4050 1875 0    50   Input ~ 0
AA12
Text HLabel 4050 2075 0    50   Input ~ 0
AA14
Text HLabel 4050 2375 0    50   Input ~ 0
AD1
Text HLabel 4050 2175 0    50   Input ~ 0
AA15
Text HLabel 4050 1475 0    50   Input ~ 0
AA8
Text HLabel 4050 2875 0    50   Input ~ 0
AD6
Text HLabel 4050 2975 0    50   Input ~ 0
AD7
Text HLabel 4050 2775 0    50   Input ~ 0
AD5
Text Label 4350 2475 2    50   ~ 0
AD2
Text Label 4350 1575 2    50   ~ 0
AA9
Text HLabel 4050 2675 0    50   Input ~ 0
AD4
Text Label 4350 1275 2    50   ~ 0
AA6
Text Label 4350 2975 2    50   ~ 0
AD7
Text HLabel 4050 1975 0    50   Input ~ 0
AA13
Text HLabel 4050 2575 0    50   Input ~ 0
AD3
Text HLabel 4050 1575 0    50   Input ~ 0
AA9
Text HLabel 4050 975  0    50   Input ~ 0
AA3
Text HLabel 4050 2475 0    50   Input ~ 0
AD2
$Comp
L 74xx:74LS259 U6
U 1 1 5F049CBA
P 2900 5100
AR Path="/5E83BCE9/5F049CBA" Ref="U6"  Part="1" 
AR Path="/5EA29723/5F049CBA" Ref="U?"  Part="1" 
AR Path="/5EA2979B/5F049CBA" Ref="U?"  Part="1" 
AR Path="/5EA297F2/5F049CBA" Ref="U?"  Part="1" 
F 0 "U6" H 2500 5775 50  0000 C CNN
F 1 "74LS259" H 2625 5675 50  0000 C CNN
F 2 "" H 2900 5100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 2900 5100 50  0001 C CNN
	1    2900 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04BDF3
P 2900 5800
AR Path="/5E83BCE9/5F04BDF3" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F04BDF3" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F04BDF3" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F04BDF3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2900 5550 50  0001 C CNN
F 1 "GND" H 2900 5650 50  0000 C CNN
F 2 "" H 2900 5800 50  0001 C CNN
F 3 "" H 2900 5800 50  0001 C CNN
	1    2900 5800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F04BE00
P 2900 4400
AR Path="/5E83BCE9/5F04BE00" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F04BE00" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F04BE00" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F04BE00" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2900 4250 50  0001 C CNN
F 1 "+5V" H 2915 4573 50  0000 C CNN
F 2 "" H 2900 4400 50  0001 C CNN
F 3 "" H 2900 4400 50  0001 C CNN
	1    2900 4400
	1    0    0    -1  
$EndComp
Text Label 2275 5100 0    50   ~ 0
A2
Wire Wire Line
	3400 5300 3550 5300
Text Label 3550 4800 2    50   ~ 0
L5
Wire Wire Line
	2275 4900 2400 4900
Wire Wire Line
	3400 4700 3550 4700
Wire Wire Line
	3400 5100 3550 5100
Wire Wire Line
	3400 5400 3550 5400
Wire Wire Line
	2275 5000 2400 5000
Wire Wire Line
	3400 4900 3550 4900
Wire Wire Line
	3400 5200 3550 5200
Wire Wire Line
	2275 4700 2400 4700
Wire Wire Line
	3400 5000 3550 5000
Text Label 2275 4700 0    50   ~ 0
A3
Wire Wire Line
	3400 4800 3550 4800
Text Label 3550 5300 2    50   ~ 0
L2
Wire Wire Line
	2350 5500 2400 5500
Text Label 2275 5000 0    50   ~ 0
A1
Wire Wire Line
	2350 5475 2350 5500
$Comp
L power:+5V #PWR?
U 1 1 5E8F9272
P 2350 5475
AR Path="/5E83BCE9/5E8F9272" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E8F9272" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E8F9272" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E8F9272" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2350 5325 50  0001 C CNN
F 1 "+5V" H 2350 5600 50  0000 C CNN
F 2 "" H 2350 5475 50  0001 C CNN
F 3 "" H 2350 5475 50  0001 C CNN
	1    2350 5475
	1    0    0    -1  
$EndComp
Text Label 3550 4700 2    50   ~ 0
L4
Text Label 3550 5000 2    50   ~ 0
L7
Text Label 3550 5400 2    50   ~ 0
L3
Text Label 3550 5200 2    50   ~ 0
L1
Text Label 3550 4900 2    50   ~ 0
L6
Text Label 3550 5100 2    50   ~ 0
L0
Text Label 2275 4900 0    50   ~ 0
A0
Wire Wire Line
	2250 5300 2400 5300
Wire Wire Line
	2275 5100 2400 5100
Wire Wire Line
	1925 2950 2225 2950
Text Label 2225 2950 2    50   ~ 0
R{slash}Wb
NoConn ~ 1925 2850
NoConn ~ 1925 2650
NoConn ~ 1925 3350
NoConn ~ 1925 3650
NoConn ~ 1925 3750
NoConn ~ 1925 3850
NoConn ~ 1925 1150
NoConn ~ 1925 1250
NoConn ~ 1925 1350
NoConn ~ 1050 2950
NoConn ~ 1050 3050
Text Label 4375 3275 2    50   ~ 0
~R~Wb
NoConn ~ 2825 3150
NoConn ~ 2825 3250
NoConn ~ 2825 3350
NoConn ~ 2825 3450
NoConn ~ 1925 3950
Text Notes 13575 1850 0    50   ~ 0
Inhalt des Decoder PROMs U14 (6131):\nAufschrift: 0xxx/1xxx\n0x00: 7e\n0x01: 7d\n0x02: 7b\n0x03: 77\nsonst alles 0xff
Text Notes 7425 2150 0    50   ~ 0
0x0000
Text Notes 8675 2150 0    50   ~ 0
0x0800
Text Notes 13600 4500 0    50   ~ 0
Inhalt des Decoder PROMs U13 (6131):\nAufschrift: 2xxx/3xxx\n0x04: 7e\n0x05: 7d\n0x06: 7b\n0x07: 77\nsonst alles 0xff
Text Notes 13650 6775 0    50   ~ 0
Inhalt des Decoder PROMs U12 (6131):\nAufschrift: 4xxx/5xxx\n0x08: 7e\n0x09: 7d\n0x0a: 7b\n0x0b: 77\nsonst alles 0xff
Text Notes 9925 2175 0    50   ~ 0
0x1000
Text Notes 11175 2175 0    50   ~ 0
0x1800
Text Notes 7375 4900 0    50   ~ 0
0x2000
Text Notes 8625 4875 0    50   ~ 0
0x2800
Text Notes 7325 10175 0    50   ~ 0
0x6000
Text Notes 8575 10175 0    50   ~ 0
0x6800
Text Notes 9825 10200 0    50   ~ 0
0x7000
Text Notes 11100 10225 0    50   ~ 0
0x7800
Text Notes 13575 8500 0    50   ~ 0
Inhalt des Decoder PROMs U11 (6131):\nAufschrift: 6xxx/7xxx\n0x0c: 7e\n0x0d: 7d\n0x0e: 7b\n0x0f: 77\nsonst alles 0xff
Text Notes 12025 2700 0    50   ~ 0
8k pro Bank\n32k pro User
Text Notes 2000 7600 0    50   ~ 0
0xFA00
Text Label 11975 1675 0    50   ~ 0
A15
Text Label 11950 8925 0    50   ~ 0
A12
Text Label 11950 8825 0    50   ~ 0
A11
Text Label 11950 9125 0    50   ~ 0
A14
Text Label 11950 9025 0    50   ~ 0
A13
Text Label 11950 9225 0    50   ~ 0
A15
Text Label 11950 4350 0    50   ~ 0
A15
Text Label 11950 4250 0    50   ~ 0
A14
Text Label 11950 4150 0    50   ~ 0
A13
Text Label 11950 3950 0    50   ~ 0
A11
Text Label 11950 4050 0    50   ~ 0
A12
Text Label 11975 6550 0    50   ~ 0
A15
Text Label 11975 6450 0    50   ~ 0
A14
Text Label 11975 6350 0    50   ~ 0
A13
Text Label 11975 6150 0    50   ~ 0
A11
Text Label 11975 6250 0    50   ~ 0
A12
Text Notes 9875 4875 0    50   ~ 0
0x3000
Text Notes 11150 4900 0    50   ~ 0
0x3800
Text Notes 7375 7525 0    50   ~ 0
0x4000
Text Notes 8600 7525 0    50   ~ 0
0x4800
Text Notes 9875 7500 0    50   ~ 0
0x5000
Text Notes 11075 7500 0    50   ~ 0
0x5800
Text Notes 1175 4775 0    50   ~ 0
0xfa80..0xfa87: LED Off\n0xfa88..0xfa8f: LED On
Text Notes 550  10550 0    50   ~ 0
0xFA00
Text Notes 575  5000 0    50   ~ 0
0xFA00
$Comp
L power:+5V #PWR?
U 1 1 5F05321D
P 2675 8075
AR Path="/5E83BCE9/5F053071/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F05321D" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F05321D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2675 7925 50  0001 C CNN
F 1 "+5V" H 2690 8248 50  0000 C CNN
F 2 "" H 2675 8075 50  0001 C CNN
F 3 "" H 2675 8075 50  0001 C CNN
	1    2675 8075
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2675 8075 2675 8150
Wire Wire Line
	2675 8150 2850 8150
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5F053A4C
P 3050 8250
AR Path="/5E83BCE9/5F053071/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053071/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053071/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053071/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5E83BCE9/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053A4C" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053A4C" Ref="J?"  Part="1" 
F 0 "J?" H 2968 7925 50  0000 C CNN
F 1 "Conn_01x03" H 2968 8016 50  0000 C CNN
F 2 "" H 3050 8250 50  0001 C CNN
F 3 "~" H 3050 8250 50  0001 C CNN
	1    3050 8250
	1    0    0    1   
$EndComp
Wire Wire Line
	2850 8250 2675 8250
Wire Wire Line
	2675 8250 2675 8300
$Comp
L power:GND #PWR?
U 1 1 5F054CB8
P 2675 8300
AR Path="/5E83BCE9/5F053071/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F054CB8" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F054CB8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2675 8050 50  0001 C CNN
F 1 "GND" H 2680 8127 50  0000 C CNN
F 2 "" H 2675 8300 50  0001 C CNN
F 3 "" H 2675 8300 50  0001 C CNN
	1    2675 8300
	-1   0    0    -1  
$EndComp
Text Notes 13775 5050 0    50   ~ 0
0xFA00
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 5E8691FD
P 3600 7350
AR Path="/5E83BCE9/5F053071/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053071/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5E83BCE9/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5EA29723/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5E8691FD" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5E8691FD" Ref="J?"  Part="1" 
F 0 "J?" H 3518 7867 50  0000 C CNN
F 1 "Conn_01x08" H 3518 7776 50  0000 C CNN
F 2 "" H 3600 7350 50  0001 C CNN
F 3 "~" H 3600 7350 50  0001 C CNN
	1    3600 7350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3800 7050 4075 7050
Wire Wire Line
	3800 7150 4075 7150
Wire Wire Line
	3800 7250 4075 7250
Wire Wire Line
	3800 7350 4075 7350
Wire Wire Line
	3800 7450 4075 7450
Wire Wire Line
	3800 7550 4075 7550
Wire Wire Line
	3800 7650 4075 7650
Wire Wire Line
	3800 7750 4075 7750
Wire Wire Line
	3775 9375 4075 9375
Wire Wire Line
	3775 8875 4075 8875
Wire Wire Line
	3775 8975 4075 8975
Wire Wire Line
	3775 9275 4075 9275
Wire Wire Line
	3775 9075 4075 9075
Wire Wire Line
	3775 8775 4075 8775
Wire Wire Line
	3775 8675 4075 8675
Wire Wire Line
	3775 9175 4075 9175
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 5E86AA15
P 3575 8975
AR Path="/5E83BCE9/5F053071/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053071/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5E83BCE9/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5EA29723/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5E86AA15" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5E86AA15" Ref="J?"  Part="1" 
F 0 "J?" H 3493 9492 50  0000 C CNN
F 1 "Conn_01x08" H 3493 9401 50  0000 C CNN
F 2 "" H 3575 8975 50  0001 C CNN
F 3 "~" H 3575 8975 50  0001 C CNN
	1    3575 8975
	-1   0    0    -1  
$EndComp
Text Label 4075 7050 2    50   ~ 0
SW1
Text Label 4075 7150 2    50   ~ 0
SW2
Text Label 4075 7250 2    50   ~ 0
SW3
Text Label 4075 7350 2    50   ~ 0
SW4
Text Label 4075 7450 2    50   ~ 0
SW5
Text Label 4075 7550 2    50   ~ 0
SW6
Text Label 4075 7650 2    50   ~ 0
SW7
Text Label 4075 7750 2    50   ~ 0
SW8
Text Label 4075 8675 2    50   ~ 0
LED1
Text Label 4075 8775 2    50   ~ 0
LED2
Text Label 4075 8875 2    50   ~ 0
LED3
Text Label 4075 8975 2    50   ~ 0
LED4
Text Label 4075 9075 2    50   ~ 0
LED5
Text Label 4075 9175 2    50   ~ 0
LED6
Text Label 4075 9275 2    50   ~ 0
LED7
Text Label 4075 9375 2    50   ~ 0
LED8
$Comp
L Device:R_Network08 RN?
U 1 1 5E88CB61
P 4275 7450
AR Path="/5E83BCE9/5F053071/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5EA29723/5F053071/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5E83BCE9/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5EA29723/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5EA297F2/5E88CB61" Ref="RN?"  Part="1" 
AR Path="/5EA2979B/5E88CB61" Ref="RN?"  Part="1" 
F 0 "RN?" V 3658 7450 50  0000 C CNN
F 1 "R_Network08" V 3749 7450 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 4750 7450 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 4275 7450 50  0001 C CNN
	1    4275 7450
	0    1    1    0   
$EndComp
Wire Wire Line
	4475 7050 4525 7050
Wire Wire Line
	4525 7050 4525 6875
$Comp
L power:+5V #PWR?
U 1 1 5E88E9DC
P 4525 6875
AR Path="/5E83BCE9/5F053071/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E88E9DC" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E88E9DC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4525 6725 50  0001 C CNN
F 1 "+5V" H 4540 7048 50  0000 C CNN
F 2 "" H 4525 6875 50  0001 C CNN
F 3 "" H 4525 6875 50  0001 C CNN
	1    4525 6875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 7350 2575 7350
Wire Wire Line
	2850 7550 2575 7550
Text Label 2575 7250 0    50   ~ 0
SW3
Wire Wire Line
	2850 7250 2575 7250
Wire Wire Line
	2850 7450 2575 7450
Wire Wire Line
	2850 7150 2575 7150
Wire Wire Line
	2850 7750 2575 7750
Text Label 2575 7350 0    50   ~ 0
SW4
Wire Wire Line
	2850 7650 2575 7650
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 5EB659BA
P 3050 7350
AR Path="/5E83BCE9/5F053071/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053071/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053071/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053071/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5E83BCE9/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5EA29723/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5EB659BA" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5EB659BA" Ref="J?"  Part="1" 
F 0 "J?" H 2968 7867 50  0000 C CNN
F 1 "Conn_01x08" H 2968 7776 50  0000 C CNN
F 2 "" H 3050 7350 50  0001 C CNN
F 3 "~" H 3050 7350 50  0001 C CNN
	1    3050 7350
	1    0    0    -1  
$EndComp
Text Label 2575 7550 0    50   ~ 0
SW6
Text Label 2575 7150 0    50   ~ 0
SW2
Text Label 2575 7450 0    50   ~ 0
SW5
Text Label 2575 7050 0    50   ~ 0
SW1
Text Label 2575 7650 0    50   ~ 0
SW7
Text Label 2575 7750 0    50   ~ 0
SW8
Wire Wire Line
	2850 7050 2575 7050
Wire Wire Line
	3150 7050 3500 7050
Wire Wire Line
	3150 7150 3500 7150
Wire Wire Line
	3150 7250 3500 7250
Wire Wire Line
	3150 7350 3500 7350
Wire Wire Line
	3150 7450 3500 7450
Wire Wire Line
	3150 7550 3500 7550
Wire Wire Line
	3150 7650 3500 7650
Wire Wire Line
	3150 7750 3500 7750
Text Notes 3325 7750 2    50   ~ 0
wht
Text Notes 3325 7650 2    50   ~ 0
brn
Text Notes 3325 7550 2    50   ~ 0
grn
Text Notes 3325 7350 2    50   ~ 0
gry
Text Notes 3325 7250 2    50   ~ 0
pnk
Text Notes 3325 7150 2    50   ~ 0
blu
Text Notes 3325 7050 2    50   ~ 0
red
Text Notes 3325 7450 2    50   ~ 0
yel
Wire Wire Line
	3950 8250 3950 8300
Wire Wire Line
	3950 8075 3950 8150
Wire Wire Line
	3950 8150 3775 8150
Wire Wire Line
	3775 8250 3950 8250
$Comp
L power:GND #PWR?
U 1 1 5EB68EC0
P 3950 8300
AR Path="/5E83BCE9/5F053071/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB68EC0" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB68EC0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 8050 50  0001 C CNN
F 1 "GND" H 3955 8127 50  0000 C CNN
F 2 "" H 3950 8300 50  0001 C CNN
F 3 "" H 3950 8300 50  0001 C CNN
	1    3950 8300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5EB68ECF
P 3575 8250
AR Path="/5E83BCE9/5F053071/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053071/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053071/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053071/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5E83BCE9/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5EA29723/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5EB68ECF" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5EB68ECF" Ref="J?"  Part="1" 
F 0 "J?" H 3493 7925 50  0000 C CNN
F 1 "Conn_01x03" H 3493 8016 50  0000 C CNN
F 2 "" H 3575 8250 50  0001 C CNN
F 3 "~" H 3575 8250 50  0001 C CNN
	1    3575 8250
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB68EDC
P 3950 8075
AR Path="/5E83BCE9/5F053071/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5EB68EDC" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5EB68EDC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 7925 50  0001 C CNN
F 1 "+5V" H 3965 8248 50  0000 C CNN
F 2 "" H 3950 8075 50  0001 C CNN
F 3 "" H 3950 8075 50  0001 C CNN
	1    3950 8075
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 8150 3475 8150
Wire Wire Line
	3150 8250 3475 8250
Text Notes 3300 8150 2    50   ~ 0
red
Text Notes 3300 8250 2    50   ~ 0
blu
Wire Wire Line
	3125 9375 3475 9375
Wire Wire Line
	3125 9175 3475 9175
Wire Wire Line
	3125 8775 3475 8775
Wire Wire Line
	3125 8675 3475 8675
Text Notes 3300 9275 2    50   ~ 0
brn
Text Notes 3300 8875 2    50   ~ 0
pnk
Text Notes 3300 8775 2    50   ~ 0
blu
Wire Wire Line
	3125 9275 3475 9275
Wire Wire Line
	3125 9075 3475 9075
Text Notes 3300 9075 2    50   ~ 0
yel
Text Notes 3300 9375 2    50   ~ 0
wht
Wire Wire Line
	3125 8975 3475 8975
Text Notes 3300 9175 2    50   ~ 0
grn
Wire Wire Line
	3125 8875 3475 8875
Text Notes 3300 8675 2    50   ~ 0
red
Text Notes 3300 8975 2    50   ~ 0
gry
Wire Wire Line
	2825 9275 2525 9275
Wire Wire Line
	2825 8975 2525 8975
Wire Wire Line
	2825 8675 2525 8675
Text Label 2525 9175 0    50   ~ 0
LED6
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 5EB71D11
P 3025 8975
AR Path="/5E83BCE9/5F053071/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5EA29723/5F053071/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5F053071/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5F053071/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5E83BCE9/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5EA29723/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5EA297F2/5EB71D11" Ref="J?"  Part="1" 
AR Path="/5EA2979B/5EB71D11" Ref="J?"  Part="1" 
F 0 "J?" H 2943 9492 50  0000 C CNN
F 1 "Conn_01x08" H 2943 9401 50  0000 C CNN
F 2 "" H 3025 8975 50  0001 C CNN
F 3 "~" H 3025 8975 50  0001 C CNN
	1    3025 8975
	1    0    0    -1  
$EndComp
Text Label 2525 8875 0    50   ~ 0
LED3
Wire Wire Line
	2825 8875 2525 8875
Wire Wire Line
	2825 9375 2525 9375
Wire Wire Line
	2825 8775 2525 8775
Text Label 2525 9075 0    50   ~ 0
LED5
Text Label 2525 8775 0    50   ~ 0
LED2
Text Label 2525 8975 0    50   ~ 0
LED4
Wire Wire Line
	2825 9075 2525 9075
Text Label 2525 8675 0    50   ~ 0
LED1
Wire Wire Line
	2825 9175 2525 9175
Text Label 2525 9375 0    50   ~ 0
LED8
Text Label 2525 9275 0    50   ~ 0
LED7
Wire Notes Line
	3375 11200 6700 11200
Wire Notes Line
	6700 11200 6700 6475
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E872399
P 6300 6750
AR Path="/5E83BCE9/5F053071/5E872399" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E872399" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E872399" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E872399" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E872399" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E872399" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E872399" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E872399" Ref="D?"  Part="1" 
F 0 "D?" H 6300 6925 50  0000 C CNN
F 1 "RED" H 6275 6850 50  0000 C CNN
F 2 "" V 6300 6750 50  0001 C CNN
F 3 "~" V 6300 6750 50  0001 C CNN
	1    6300 6750
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E872A35
P 6100 6750
AR Path="/5E83BCE9/5F053071/5E872A35" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E872A35" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E872A35" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E872A35" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E872A35" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E872A35" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E872A35" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E872A35" Ref="R?"  Part="1" 
F 0 "R?" V 5925 6750 50  0000 C CNN
F 1 "220E" V 5995 6750 50  0000 C CNN
F 2 "" H 6100 6750 50  0001 C CNN
F 3 "~" H 6100 6750 50  0001 C CNN
	1    6100 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 6750 5750 6750
Wire Wire Line
	6400 6750 6525 6750
Wire Wire Line
	6400 7025 6525 7025
Wire Wire Line
	6000 7025 5750 7025
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E874A73
P 6300 7025
AR Path="/5E83BCE9/5F053071/5E874A73" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E874A73" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E874A73" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E874A73" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E874A73" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E874A73" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E874A73" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E874A73" Ref="D?"  Part="1" 
F 0 "D?" H 6300 7200 50  0000 C CNN
F 1 "RED" H 6275 7125 50  0000 C CNN
F 2 "" V 6300 7025 50  0001 C CNN
F 3 "~" V 6300 7025 50  0001 C CNN
	1    6300 7025
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E874A81
P 6100 7025
AR Path="/5E83BCE9/5F053071/5E874A81" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E874A81" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E874A81" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E874A81" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E874A81" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E874A81" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E874A81" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E874A81" Ref="R?"  Part="1" 
F 0 "R?" V 5925 7025 50  0000 C CNN
F 1 "220E" V 5995 7025 50  0000 C CNN
F 2 "" H 6100 7025 50  0001 C CNN
F 3 "~" H 6100 7025 50  0001 C CNN
	1    6100 7025
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 7575 6525 7575
Wire Wire Line
	6400 7300 6525 7300
$Comp
L Device:R_Small R?
U 1 1 5E875B60
P 6100 7575
AR Path="/5E83BCE9/5F053071/5E875B60" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E875B60" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E875B60" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E875B60" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E875B60" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E875B60" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E875B60" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E875B60" Ref="R?"  Part="1" 
F 0 "R?" V 5925 7575 50  0000 C CNN
F 1 "220E" V 5995 7575 50  0000 C CNN
F 2 "" H 6100 7575 50  0001 C CNN
F 3 "~" H 6100 7575 50  0001 C CNN
	1    6100 7575
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 7575 5750 7575
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E875B70
P 6300 7575
AR Path="/5E83BCE9/5F053071/5E875B70" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E875B70" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E875B70" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E875B70" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E875B70" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E875B70" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E875B70" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E875B70" Ref="D?"  Part="1" 
F 0 "D?" H 6300 7750 50  0000 C CNN
F 1 "RED" H 6275 7675 50  0000 C CNN
F 2 "" V 6300 7575 50  0001 C CNN
F 3 "~" V 6300 7575 50  0001 C CNN
	1    6300 7575
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6000 7300 5750 7300
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E875B7F
P 6300 7300
AR Path="/5E83BCE9/5F053071/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E875B7F" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E875B7F" Ref="D?"  Part="1" 
F 0 "D?" H 6300 7475 50  0000 C CNN
F 1 "RED" H 6275 7400 50  0000 C CNN
F 2 "" V 6300 7300 50  0001 C CNN
F 3 "~" V 6300 7300 50  0001 C CNN
	1    6300 7300
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E875B8D
P 6100 7300
AR Path="/5E83BCE9/5F053071/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E875B8D" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E875B8D" Ref="R?"  Part="1" 
F 0 "R?" V 5925 7300 50  0000 C CNN
F 1 "220E" V 5995 7300 50  0000 C CNN
F 2 "" H 6100 7300 50  0001 C CNN
F 3 "~" H 6100 7300 50  0001 C CNN
	1    6100 7300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E877E13
P 6100 8400
AR Path="/5E83BCE9/5F053071/5E877E13" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E13" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E13" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E13" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E877E13" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E877E13" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E877E13" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E877E13" Ref="R?"  Part="1" 
F 0 "R?" V 5925 8400 50  0000 C CNN
F 1 "220E" V 5995 8400 50  0000 C CNN
F 2 "" H 6100 8400 50  0001 C CNN
F 3 "~" H 6100 8400 50  0001 C CNN
	1    6100 8400
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 8675 5750 8675
Wire Wire Line
	6400 8125 6525 8125
$Comp
L Device:R_Small R?
U 1 1 5E877E23
P 6100 8125
AR Path="/5E83BCE9/5F053071/5E877E23" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E23" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E23" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E23" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E877E23" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E877E23" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E877E23" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E877E23" Ref="R?"  Part="1" 
F 0 "R?" V 5925 8125 50  0000 C CNN
F 1 "220E" V 5995 8125 50  0000 C CNN
F 2 "" H 6100 8125 50  0001 C CNN
F 3 "~" H 6100 8125 50  0001 C CNN
	1    6100 8125
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 8400 5750 8400
Wire Wire Line
	6400 7850 6525 7850
Wire Wire Line
	6000 8125 5750 8125
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E877E34
P 6300 8675
AR Path="/5E83BCE9/5F053071/5E877E34" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E34" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E34" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E34" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E877E34" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E877E34" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E877E34" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E877E34" Ref="D?"  Part="1" 
F 0 "D?" H 6300 8850 50  0000 C CNN
F 1 "RED" H 6275 8775 50  0000 C CNN
F 2 "" V 6300 8675 50  0001 C CNN
F 3 "~" V 6300 8675 50  0001 C CNN
	1    6300 8675
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E877E42
P 6100 8675
AR Path="/5E83BCE9/5F053071/5E877E42" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E42" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E42" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E42" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E877E42" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E877E42" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E877E42" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E877E42" Ref="R?"  Part="1" 
F 0 "R?" V 5925 8675 50  0000 C CNN
F 1 "220E" V 5995 8675 50  0000 C CNN
F 2 "" H 6100 8675 50  0001 C CNN
F 3 "~" H 6100 8675 50  0001 C CNN
	1    6100 8675
	0    1    1    0   
$EndComp
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E877E50
P 6300 8400
AR Path="/5E83BCE9/5F053071/5E877E50" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E50" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E50" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E50" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E877E50" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E877E50" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E877E50" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E877E50" Ref="D?"  Part="1" 
F 0 "D?" H 6300 8575 50  0000 C CNN
F 1 "RED" H 6275 8500 50  0000 C CNN
F 2 "" V 6300 8400 50  0001 C CNN
F 3 "~" V 6300 8400 50  0001 C CNN
	1    6300 8400
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E877E5E
P 6300 8125
AR Path="/5E83BCE9/5F053071/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E877E5E" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E877E5E" Ref="D?"  Part="1" 
F 0 "D?" H 6300 8300 50  0000 C CNN
F 1 "RED" H 6275 8225 50  0000 C CNN
F 2 "" V 6300 8125 50  0001 C CNN
F 3 "~" V 6300 8125 50  0001 C CNN
	1    6300 8125
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6400 8400 6525 8400
Wire Wire Line
	6000 7850 5750 7850
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E877E6E
P 6300 7850
AR Path="/5E83BCE9/5F053071/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5E83BCE9/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5EA29723/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5EA297F2/5E877E6E" Ref="D?"  Part="1" 
AR Path="/5EA2979B/5E877E6E" Ref="D?"  Part="1" 
F 0 "D?" H 6300 8025 50  0000 C CNN
F 1 "RED" H 6275 7950 50  0000 C CNN
F 2 "" V 6300 7850 50  0001 C CNN
F 3 "~" V 6300 7850 50  0001 C CNN
	1    6300 7850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6400 8675 6525 8675
$Comp
L Device:R_Small R?
U 1 1 5E877E7D
P 6100 7850
AR Path="/5E83BCE9/5F053071/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5EA29723/5F053071/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5E83BCE9/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5EA29723/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5EA297F2/5E877E7D" Ref="R?"  Part="1" 
AR Path="/5EA2979B/5E877E7D" Ref="R?"  Part="1" 
F 0 "R?" V 5925 7850 50  0000 C CNN
F 1 "220E" V 5995 7850 50  0000 C CNN
F 2 "" H 6100 7850 50  0001 C CNN
F 3 "~" H 6100 7850 50  0001 C CNN
	1    6100 7850
	0    1    1    0   
$EndComp
Text Label 5750 8675 0    50   ~ 0
LED8
Text Label 5750 8400 0    50   ~ 0
LED7
Text Label 5750 8125 0    50   ~ 0
LED6
Text Label 5750 7025 0    50   ~ 0
LED2
Text Label 5750 7850 0    50   ~ 0
LED5
Text Label 5750 7300 0    50   ~ 0
LED3
Text Label 5750 7575 0    50   ~ 0
LED4
Text Label 5750 6750 0    50   ~ 0
LED1
Wire Wire Line
	6525 6750 6525 7025
Connection ~ 6525 7025
Wire Wire Line
	6525 7025 6525 7300
Connection ~ 6525 7300
Wire Wire Line
	6525 7300 6525 7575
Connection ~ 6525 7575
Wire Wire Line
	6525 7575 6525 7850
Connection ~ 6525 7850
Wire Wire Line
	6525 7850 6525 8125
Connection ~ 6525 8125
Wire Wire Line
	6525 8125 6525 8400
Connection ~ 6525 8400
Wire Wire Line
	6525 8400 6525 8675
Connection ~ 6525 8675
Wire Wire Line
	6525 8675 6525 8725
$Comp
L power:GND #PWR?
U 1 1 5E87D66B
P 6525 8725
AR Path="/5E83BCE9/5F053071/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E87D66B" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E87D66B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6525 8475 50  0001 C CNN
F 1 "GND" H 6530 8552 50  0000 C CNN
F 2 "" H 6525 8725 50  0001 C CNN
F 3 "" H 6525 8725 50  0001 C CNN
	1    6525 8725
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E87E070
P 5200 9100
AR Path="/5E83BCE9/5F053071/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E87E070" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E87E070" Ref="SW?"  Part="1" 
F 0 "SW?" H 5200 9385 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 5200 9294 50  0000 C CNN
F 2 "" H 5200 9100 50  0001 C CNN
F 3 "~" H 5200 9100 50  0001 C CNN
	1    5200 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 9000 5550 9000
Wire Wire Line
	5400 9200 5550 9200
Wire Wire Line
	5000 9100 4725 9100
Wire Wire Line
	5000 9650 4725 9650
Wire Wire Line
	5400 9550 5550 9550
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E87F556
P 5200 9650
AR Path="/5E83BCE9/5F053071/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E87F556" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E87F556" Ref="SW?"  Part="1" 
F 0 "SW?" H 5200 9935 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 5200 9844 50  0000 C CNN
F 2 "" H 5200 9650 50  0001 C CNN
F 3 "~" H 5200 9650 50  0001 C CNN
	1    5200 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 9750 5550 9750
Wire Wire Line
	5400 10300 5550 10300
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E880BEB
P 5200 10200
AR Path="/5E83BCE9/5F053071/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E880BEB" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E880BEB" Ref="SW?"  Part="1" 
F 0 "SW?" H 5200 10485 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 5200 10394 50  0000 C CNN
F 2 "" H 5200 10200 50  0001 C CNN
F 3 "~" H 5200 10200 50  0001 C CNN
	1    5200 10200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 10200 4725 10200
Wire Wire Line
	5400 10100 5550 10100
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E8819F8
P 5200 10750
AR Path="/5E83BCE9/5F053071/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E8819F8" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E8819F8" Ref="SW?"  Part="1" 
F 0 "SW?" H 5200 11035 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 5200 10944 50  0000 C CNN
F 2 "" H 5200 10750 50  0001 C CNN
F 3 "~" H 5200 10750 50  0001 C CNN
	1    5200 10750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 10850 5550 10850
Wire Wire Line
	5400 10650 5550 10650
Wire Wire Line
	5000 10750 4725 10750
Wire Wire Line
	5550 9000 5550 9200
Connection ~ 5550 9200
Wire Wire Line
	5550 9200 5550 9550
Connection ~ 5550 9550
Wire Wire Line
	5550 9550 5550 9750
Connection ~ 5550 9750
Wire Wire Line
	5550 9750 5550 10100
Connection ~ 5550 10100
Wire Wire Line
	5550 10100 5550 10300
Connection ~ 5550 10300
Wire Wire Line
	5550 10300 5550 10650
Connection ~ 5550 10650
Wire Wire Line
	5550 10650 5550 10850
Connection ~ 5550 10850
Wire Wire Line
	5550 10850 5550 10950
$Comp
L power:GND #PWR?
U 1 1 5E883A60
P 5550 10950
AR Path="/5E83BCE9/5F053071/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E883A60" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E883A60" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5550 10700 50  0001 C CNN
F 1 "GND" H 5555 10777 50  0000 C CNN
F 2 "" H 5550 10950 50  0001 C CNN
F 3 "" H 5550 10950 50  0001 C CNN
	1    5550 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6525 9200 6525 9550
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E883F1A
P 6175 10750
AR Path="/5E83BCE9/5F053071/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E883F1A" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E883F1A" Ref="SW?"  Part="1" 
F 0 "SW?" H 6175 11035 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 6175 10944 50  0000 C CNN
F 2 "" H 6175 10750 50  0001 C CNN
F 3 "~" H 6175 10750 50  0001 C CNN
	1    6175 10750
	1    0    0    -1  
$EndComp
Connection ~ 6525 10300
Wire Wire Line
	6375 10850 6525 10850
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E883F2A
P 6175 10200
AR Path="/5E83BCE9/5F053071/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E883F2A" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E883F2A" Ref="SW?"  Part="1" 
F 0 "SW?" H 6175 10485 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 6175 10394 50  0000 C CNN
F 2 "" H 6175 10200 50  0001 C CNN
F 3 "~" H 6175 10200 50  0001 C CNN
	1    6175 10200
	1    0    0    -1  
$EndComp
Connection ~ 6525 9200
Connection ~ 6525 10850
Wire Wire Line
	6525 9000 6525 9200
Connection ~ 6525 9750
Wire Wire Line
	6525 9750 6525 10100
Connection ~ 6525 10650
Wire Wire Line
	6525 10300 6525 10650
Wire Wire Line
	6375 10300 6525 10300
$Comp
L power:GND #PWR?
U 1 1 5E883F40
P 6525 10950
AR Path="/5E83BCE9/5F053071/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5F053071/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5EA29723/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5EA297F2/5E883F40" Ref="#PWR?"  Part="1" 
AR Path="/5EA2979B/5E883F40" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6525 10700 50  0001 C CNN
F 1 "GND" H 6530 10777 50  0000 C CNN
F 2 "" H 6525 10950 50  0001 C CNN
F 3 "" H 6525 10950 50  0001 C CNN
	1    6525 10950
	1    0    0    -1  
$EndComp
Connection ~ 6525 10100
Connection ~ 6525 9550
Wire Wire Line
	6525 10850 6525 10950
Wire Wire Line
	6375 10650 6525 10650
Wire Wire Line
	6375 9750 6525 9750
Wire Wire Line
	6375 10100 6525 10100
Wire Wire Line
	6525 10650 6525 10850
Wire Wire Line
	5975 9100 5700 9100
Wire Wire Line
	6525 9550 6525 9750
Wire Wire Line
	6525 10100 6525 10300
Wire Wire Line
	5975 10200 5700 10200
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E883F5A
P 6175 9650
AR Path="/5E83BCE9/5F053071/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E883F5A" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E883F5A" Ref="SW?"  Part="1" 
F 0 "SW?" H 6175 9935 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 6175 9844 50  0000 C CNN
F 2 "" H 6175 9650 50  0001 C CNN
F 3 "~" H 6175 9650 50  0001 C CNN
	1    6175 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5975 9650 5700 9650
Wire Wire Line
	5975 10750 5700 10750
Wire Wire Line
	6375 9550 6525 9550
Wire Wire Line
	6375 9000 6525 9000
$Comp
L Switch:SW_SPDT_MSM SW?
U 1 1 5E883F6D
P 6175 9100
AR Path="/5E83BCE9/5F053071/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5F053071/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5F053071/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5F053071/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5E83BCE9/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5EA29723/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5EA297F2/5E883F6D" Ref="SW?"  Part="1" 
AR Path="/5EA2979B/5E883F6D" Ref="SW?"  Part="1" 
F 0 "SW?" H 6175 9385 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 6175 9294 50  0000 C CNN
F 2 "" H 6175 9100 50  0001 C CNN
F 3 "~" H 6175 9100 50  0001 C CNN
	1    6175 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6375 9200 6525 9200
Text Label 5700 9650 0    50   ~ 0
SW6
Text Label 4725 10200 0    50   ~ 0
SW3
Text Label 4725 9100 0    50   ~ 0
SW1
Text Label 4725 10750 0    50   ~ 0
SW4
Text Label 5700 9100 0    50   ~ 0
SW5
Text Label 4725 9650 0    50   ~ 0
SW2
Text Label 5700 10750 0    50   ~ 0
SW8
Text Label 5700 10200 0    50   ~ 0
SW7
Wire Notes Line
	6700 6475 3375 6475
Wire Notes Line
	3375 6475 3375 11200
Text Notes 3900 6650 2    50   ~ 0
Front Panel
$EndSCHEMATC
