EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 10 16
Title "es65 Serial Board"
Date "2020-03-31"
Rev "v0.1"
Comp "Offner Robert"
Comment1 "v0.1: Initial"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1050 1950 1350 1950
Wire Wire Line
	1050 3450 1350 3450
Wire Wire Line
	1050 2150 1350 2150
Wire Wire Line
	1050 2450 1350 2450
Wire Wire Line
	1050 3850 1350 3850
NoConn ~ 1050 1050
NoConn ~ 1050 3950
Wire Wire Line
	1050 2350 1350 2350
Wire Wire Line
	1925 2750 2225 2750
Text Label 1350 3750 2    50   ~ 0
AD6
Text Label 1350 3850 2    50   ~ 0
AD7
Text Label 1350 3650 2    50   ~ 0
AD5
Text Label 1350 3550 2    50   ~ 0
AD4
Text Label 1350 3450 2    50   ~ 0
AD3
Text Label 1350 3350 2    50   ~ 0
AD2
Text Label 1350 3250 2    50   ~ 0
AD1
Text Label 1350 3150 2    50   ~ 0
AD0
Text Label 1350 2650 2    50   ~ 0
AA15
Text Label 1350 2150 2    50   ~ 0
AA10
Text Label 1350 2350 2    50   ~ 0
AA12
Text Label 1350 2250 2    50   ~ 0
AA11
Text Label 1350 2550 2    50   ~ 0
AA14
Text Label 1350 2450 2    50   ~ 0
AA13
Text Label 1350 1950 2    50   ~ 0
AA8
Text Label 1350 1550 2    50   ~ 0
AA4
Text Label 1350 2050 2    50   ~ 0
AA9
Text Label 1350 1650 2    50   ~ 0
AA5
Text Label 1350 1850 2    50   ~ 0
AA7
Text Label 1350 1750 2    50   ~ 0
AA6
Text Label 2225 2750 2    50   ~ 0
PH2b
Text Label 1350 1250 2    50   ~ 0
AA1
Text Label 1350 1350 2    50   ~ 0
AA2
Text Label 1350 1450 2    50   ~ 0
AA3
Text Label 1350 1150 2    50   ~ 0
AA0
NoConn ~ 1050 2750
Wire Wire Line
	1050 2050 1350 2050
Wire Wire Line
	1050 1450 1350 1450
Wire Wire Line
	1050 950  1350 950 
Wire Wire Line
	1050 3150 1350 3150
Wire Wire Line
	1100 4050 1100 4100
Wire Wire Line
	1050 1250 1350 1250
Wire Wire Line
	2225 950  2225 825 
NoConn ~ 1050 2850
Wire Wire Line
	1050 4050 1100 4050
Wire Wire Line
	1350 950  1350 825 
Wire Wire Line
	1050 3250 1350 3250
Wire Wire Line
	1050 1550 1350 1550
Wire Wire Line
	1050 1350 1350 1350
Wire Wire Line
	1050 1150 1350 1150
Wire Wire Line
	1925 4050 1975 4050
Wire Wire Line
	1975 4050 1975 4100
Wire Wire Line
	1050 2550 1350 2550
Wire Wire Line
	1050 3550 1350 3550
Wire Wire Line
	1925 950  2225 950 
Wire Wire Line
	1050 3350 1350 3350
Wire Wire Line
	1050 1850 1350 1850
Wire Wire Line
	1050 2650 1350 2650
Wire Wire Line
	1050 2250 1350 2250
Wire Wire Line
	1050 1650 1350 1650
Wire Wire Line
	1050 1750 1350 1750
Wire Wire Line
	1050 3650 1350 3650
Wire Wire Line
	1050 3750 1350 3750
$Comp
L power:GND #PWR?
U 1 1 5E9B305D
P 1975 4100
AR Path="/5E84A03F/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B305D" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B305D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1975 3850 50  0001 C CNN
F 1 "GND" H 1975 3950 50  0000 C CNN
F 2 "" H 1975 4100 50  0001 C CNN
F 3 "" H 1975 4100 50  0001 C CNN
	1    1975 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EAA2353
P 2225 825
AR Path="/5E84A03F/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAA2353" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EAA2353" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2225 675 50  0001 C CNN
F 1 "+5V" H 2240 998 50  0000 C CNN
F 2 "" H 2225 825 50  0001 C CNN
F 3 "" H 2225 825 50  0001 C CNN
	1    2225 825 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9AC891
P 1350 825
AR Path="/5E84A03F/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC891" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC891" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1350 675 50  0001 C CNN
F 1 "+5V" H 1365 998 50  0000 C CNN
F 2 "" H 1350 825 50  0001 C CNN
F 3 "" H 1350 825 50  0001 C CNN
	1    1350 825 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9B3066
P 1100 4100
AR Path="/5E84A03F/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3066" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3066" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1100 3850 50  0001 C CNN
F 1 "GND" H 1100 3950 50  0000 C CNN
F 2 "" H 1100 4100 50  0001 C CNN
F 3 "" H 1100 4100 50  0001 C CNN
	1    1100 4100
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 1 1 5E9AC889
P 850 2500
AR Path="/5E84A03F/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5EAB9D7B/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E83BCE9/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5EA2DD9E/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5EA9FEC9/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5EB2C18C/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E890F69/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E985316/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E9856B7/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E985745/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E985791/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E9AC39B/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC889" Ref="P?"  Part="1" 
AR Path="/5E9AC889" Ref="P?"  Part="1" 
F 0 "P?" H 767 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 767 816 50  0000 C CNN
F 2 "" H 850 2500 50  0000 C CNN
F 3 "" H 850 2500 50  0000 C CNN
	1    850  2500
	-1   0    0    1   
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 3 1 5E9B305F
P 1725 2500
AR Path="/5E84A03F/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5EAB9D7B/5E9B305F" Ref="P?"  Part="2" 
AR Path="/5E83BCE9/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5EA2DD9E/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5EA9FEC9/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5EB2C18C/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E890F69/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E985316/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E9856B7/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E985745/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E985791/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E9AC39B/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E9AC3F3/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E9B2B8D/5E9B305F" Ref="P?"  Part="3" 
AR Path="/5E9B305F" Ref="P?"  Part="3" 
F 0 "P?" H 1642 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 1642 816 50  0000 C CNN
F 2 "" H 1725 2500 50  0000 C CNN
F 3 "" H 1725 2500 50  0000 C CNN
	3    1725 2500
	-1   0    0    1   
$EndComp
Text HLabel 4075 3275 0    50   Input ~ 0
R{slash}Wb
Wire Wire Line
	4075 3275 4375 3275
Wire Wire Line
	4075 3175 4375 3175
Text Label 4375 3175 2    50   ~ 0
PH2b
Text HLabel 4075 3175 0    50   Input ~ 0
PH2b
Text Label 4350 2875 2    50   ~ 0
AD6
Text HLabel 4050 875  0    50   Input ~ 0
AA2
Text Label 4350 1975 2    50   ~ 0
AA13
Text Label 4350 2575 2    50   ~ 0
AD3
Text Label 4350 675  2    50   ~ 0
AA0
Text Label 4350 2075 2    50   ~ 0
AA14
Text Label 4350 2675 2    50   ~ 0
AD4
Text Label 4350 1375 2    50   ~ 0
AA7
Text Label 4350 975  2    50   ~ 0
AA3
Text Label 4350 875  2    50   ~ 0
AA2
Text Label 4350 775  2    50   ~ 0
AA1
Text Label 4350 1075 2    50   ~ 0
AA4
Wire Wire Line
	4050 1375 4350 1375
Wire Wire Line
	4050 1575 4350 1575
Wire Wire Line
	4050 2075 4350 2075
Wire Wire Line
	4050 1875 4350 1875
Wire Wire Line
	4050 2175 4350 2175
Text Label 4350 1475 2    50   ~ 0
AA8
Text Label 4350 2775 2    50   ~ 0
AD5
Text Label 4350 2175 2    50   ~ 0
AA15
Wire Wire Line
	4050 775  4350 775 
Wire Wire Line
	4050 675  4350 675 
Wire Wire Line
	4050 1675 4350 1675
Wire Wire Line
	4050 1775 4350 1775
Text Label 4350 1875 2    50   ~ 0
AA12
Wire Wire Line
	4050 1975 4350 1975
Wire Wire Line
	4050 1075 4350 1075
Wire Wire Line
	4050 1475 4350 1475
Text HLabel 4050 675  0    50   Input ~ 0
AA0
Text Label 4350 2275 2    50   ~ 0
AD0
Text Label 4350 1775 2    50   ~ 0
AA11
Text Label 4350 2375 2    50   ~ 0
AD1
Wire Wire Line
	4050 1175 4350 1175
Wire Wire Line
	4050 975  4350 975 
Text Label 4350 1675 2    50   ~ 0
AA10
Wire Wire Line
	4050 2875 4350 2875
Wire Wire Line
	4050 2975 4350 2975
Text HLabel 4050 775  0    50   Input ~ 0
AA1
Wire Wire Line
	4050 2275 4350 2275
Text Label 4350 1175 2    50   ~ 0
AA5
Wire Wire Line
	4050 2675 4350 2675
Wire Wire Line
	4050 875  4350 875 
Wire Wire Line
	4050 2575 4350 2575
Wire Wire Line
	4050 2775 4350 2775
Wire Wire Line
	4050 2375 4350 2375
Wire Wire Line
	4050 2475 4350 2475
Wire Wire Line
	4050 1275 4350 1275
Text HLabel 4050 1075 0    50   Input ~ 0
AA4
Text HLabel 4050 1175 0    50   Input ~ 0
AA5
Text HLabel 4050 1275 0    50   Input ~ 0
AA6
Text HLabel 4050 2275 0    50   Input ~ 0
AD0
Text HLabel 4050 1375 0    50   Input ~ 0
AA7
Text HLabel 4050 1775 0    50   Input ~ 0
AA11
Text HLabel 4050 1675 0    50   Input ~ 0
AA10
Text HLabel 4050 1875 0    50   Input ~ 0
AA12
Text HLabel 4050 2075 0    50   Input ~ 0
AA14
Text HLabel 4050 2375 0    50   Input ~ 0
AD1
Text HLabel 4050 2175 0    50   Input ~ 0
AA15
Text HLabel 4050 1475 0    50   Input ~ 0
AA8
Text HLabel 4050 2875 0    50   Input ~ 0
AD6
Text HLabel 4050 2975 0    50   Input ~ 0
AD7
Text HLabel 4050 2775 0    50   Input ~ 0
AD5
Text Label 4350 2475 2    50   ~ 0
AD2
Text Label 4350 1575 2    50   ~ 0
AA9
Text HLabel 4050 2675 0    50   Input ~ 0
AD4
Text Label 4350 1275 2    50   ~ 0
AA6
Text Label 4350 2975 2    50   ~ 0
AD7
Text HLabel 4050 1975 0    50   Input ~ 0
AA13
Text HLabel 4050 2575 0    50   Input ~ 0
AD3
Text HLabel 4050 1575 0    50   Input ~ 0
AA9
Text HLabel 4050 975  0    50   Input ~ 0
AA3
Text HLabel 4050 2475 0    50   Input ~ 0
AD2
Wire Wire Line
	1925 2950 2225 2950
Text Label 2225 2950 2    50   ~ 0
R{slash}Wb
NoConn ~ 1050 2950
NoConn ~ 1925 1050
NoConn ~ 1925 1150
NoConn ~ 1925 1250
NoConn ~ 1925 1350
NoConn ~ 1925 2250
NoConn ~ 1925 2650
Wire Wire Line
	1925 1450 2200 1450
Wire Wire Line
	1925 1550 2200 1550
Wire Wire Line
	1925 1650 2200 1650
Wire Wire Line
	1925 1750 2200 1750
Wire Wire Line
	1925 1850 2200 1850
Wire Wire Line
	1925 1950 2200 1950
Wire Wire Line
	1925 2050 2200 2050
Wire Wire Line
	1925 2150 2200 2150
NoConn ~ 1925 3050
NoConn ~ 1925 3250
NoConn ~ 1925 3350
NoConn ~ 1925 3450
NoConn ~ 1925 3550
Wire Wire Line
	4100 3575 4400 3575
Text Label 4400 3575 2    50   ~ 0
~RSTb
Text HLabel 4100 3575 0    50   Input ~ 0
~RSTb
$Comp
L power:+5V #PWR?
U 1 1 5EBD6BA3
P 1050 7150
AR Path="/5E890F69/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EBD6BA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EBD6BA3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1050 7000 50  0001 C CNN
F 1 "+5V" H 1050 7300 50  0000 C CNN
F 2 "" H 1050 7150 50  0001 C CNN
F 3 "" H 1050 7150 50  0001 C CNN
	1    1050 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 7175 1050 7400
Wire Wire Line
	1225 7175 1225 7200
$Comp
L Device:C_Small C?
U 1 1 5EBD6BB3
P 1225 7300
AR Path="/5E890F69/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E985316/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E985745/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E985791/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5EBD6BB3" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5EBD6BB3" Ref="C?"  Part="1" 
F 0 "C?" H 1317 7346 50  0000 L CNN
F 1 "22n" H 1317 7255 50  0000 L CNN
F 2 "" H 1225 7300 50  0001 C CNN
F 3 "~" H 1225 7300 50  0001 C CNN
	1    1225 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 7150 1050 7175
Wire Wire Line
	1050 7175 1225 7175
Connection ~ 1050 7175
$Comp
L power:GND #PWR?
U 1 1 5E9B3063
P 1225 7400
AR Path="/5E890F69/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3063" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3063" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1225 7150 50  0001 C CNN
F 1 "GND" H 1225 7250 50  0000 C CNN
F 2 "" H 1225 7400 50  0001 C CNN
F 3 "" H 1225 7400 50  0001 C CNN
	1    1225 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EBD843B
P 1050 8400
AR Path="/5E890F69/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EBD843B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EBD843B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1050 8150 50  0001 C CNN
F 1 "GND" H 1050 8250 50  0000 C CNN
F 2 "" H 1050 8400 50  0001 C CNN
F 3 "" H 1050 8400 50  0001 C CNN
	1    1050 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1925 3950 2225 3950
Text Label 2225 3950 2    50   ~ 0
~RSTb
$Comp
L power:+5V #PWR?
U 1 1 5E9AC885
P 1800 7150
AR Path="/5E890F69/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC885" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC885" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1800 7000 50  0001 C CNN
F 1 "+5V" H 1800 7300 50  0000 C CNN
F 2 "" H 1800 7150 50  0001 C CNN
F 3 "" H 1800 7150 50  0001 C CNN
	1    1800 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 7175 1800 7400
Wire Wire Line
	1975 7175 1975 7200
Wire Wire Line
	1800 7150 1800 7175
$Comp
L Device:C_Small C?
U 1 1 5E9B303B
P 1975 7300
AR Path="/5E890F69/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B303B" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9B303B" Ref="C?"  Part="1" 
F 0 "C?" H 2067 7346 50  0000 L CNN
F 1 "22n" H 2067 7255 50  0000 L CNN
F 2 "" H 1975 7300 50  0001 C CNN
F 3 "~" H 1975 7300 50  0001 C CNN
	1    1975 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 7175 1975 7175
Connection ~ 1800 7175
$Comp
L power:GND #PWR?
U 1 1 5E9AC887
P 1975 7400
AR Path="/5E890F69/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC887" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC887" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1975 7150 50  0001 C CNN
F 1 "GND" H 1975 7250 50  0000 C CNN
F 2 "" H 1975 7400 50  0001 C CNN
F 3 "" H 1975 7400 50  0001 C CNN
	1    1975 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9AC888
P 1800 8400
AR Path="/5E890F69/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC888" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC888" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1800 8150 50  0001 C CNN
F 1 "GND" H 1800 8250 50  0000 C CNN
F 2 "" H 1800 8400 50  0001 C CNN
F 3 "" H 1800 8400 50  0001 C CNN
	1    1800 8400
	1    0    0    -1  
$EndComp
NoConn ~ 1925 3650
NoConn ~ 1925 3850
$Comp
L power:+12V #PWR?
U 1 1 5E9AC893
P 2275 2275
AR Path="/5E890F69/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC893" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC893" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2275 2125 50  0001 C CNN
F 1 "+12V" H 2290 2448 50  0000 C CNN
F 2 "" H 2275 2275 50  0001 C CNN
F 3 "" H 2275 2275 50  0001 C CNN
	1    2275 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 2275 2275 2550
Wire Wire Line
	1925 2550 2275 2550
Wire Wire Line
	2350 2450 2350 2500
Wire Wire Line
	1925 2450 2350 2450
$Comp
L power:GNDA #PWR?
U 1 1 5E9AC894
P 2350 2500
AR Path="/5E890F69/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9AC894" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9AC894" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2350 2250 50  0001 C CNN
F 1 "GNDA" H 2355 2327 50  0000 C CNN
F 2 "" H 2350 2500 50  0001 C CNN
F 3 "" H 2350 2500 50  0001 C CNN
	1    2350 2500
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5EEA325B
P 2500 2600
AR Path="/5E890F69/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EEA325B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EEA325B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2500 2700 50  0001 C CNN
F 1 "-12V" H 2515 2773 50  0000 C CNN
F 2 "" H 2500 2600 50  0001 C CNN
F 3 "" H 2500 2600 50  0001 C CNN
	1    2500 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 2600 2500 2350
Wire Wire Line
	1925 2350 2500 2350
NoConn ~ 1925 3150
Wire Wire Line
	1925 2850 2225 2850
$Comp
L es65-rescue:DS8T28N-my_ics U3
U 1 1 5E8AD701
P 3850 5500
AR Path="/5E890F69/5E8AD701" Ref="U3"  Part="1" 
AR Path="/5E985316/5E8AD701" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E8AD701" Ref="U?"  Part="1" 
AR Path="/5E985745/5E8AD701" Ref="U?"  Part="1" 
AR Path="/5E985791/5E8AD701" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E8AD701" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E8AD701" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E8AD701" Ref="U?"  Part="1" 
F 0 "U3" H 3500 6025 50  0000 C CNN
F 1 "DS8T28N" H 3625 5950 50  0000 C CNN
F 2 "" H 3850 5500 50  0001 C CNN
F 3 "" H 3850 5500 50  0001 C CNN
	1    3850 5500
	1    0    0    -1  
$EndComp
Text Label 3050 7375 0    50   ~ 0
AD0
Wire Wire Line
	3350 5300 3050 5300
Text Label 3050 7175 0    50   ~ 0
AD2
Text Label 3050 7075 0    50   ~ 0
AD3
Text Label 3050 5500 0    50   ~ 0
AD4
Text Label 3050 5400 0    50   ~ 0
AD5
Text Label 3050 5200 0    50   ~ 0
AD7
Text Label 3050 5300 0    50   ~ 0
AD6
Wire Wire Line
	3350 5500 3050 5500
Wire Wire Line
	3350 5400 3050 5400
Text Label 3050 7275 0    50   ~ 0
AD1
Wire Wire Line
	3350 5200 3050 5200
Wire Wire Line
	3850 4675 3850 4900
$Comp
L power:+5V #PWR?
U 1 1 5E8B0FF1
P 3850 4650
AR Path="/5E890F69/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8B0FF1" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8B0FF1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 4500 50  0001 C CNN
F 1 "+5V" H 3850 4800 50  0000 C CNN
F 2 "" H 3850 4650 50  0001 C CNN
F 3 "" H 3850 4650 50  0001 C CNN
	1    3850 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E8B0FFF
P 4025 4800
AR Path="/5E890F69/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E985316/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E985745/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E985791/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E8B0FFF" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E8B0FFF" Ref="C?"  Part="1" 
F 0 "C?" H 4117 4846 50  0000 L CNN
F 1 "22n" H 4117 4755 50  0000 L CNN
F 2 "" H 4025 4800 50  0001 C CNN
F 3 "~" H 4025 4800 50  0001 C CNN
	1    4025 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4025 4675 4025 4700
Connection ~ 3850 4675
$Comp
L power:GND #PWR?
U 1 1 5E8B100E
P 4025 4900
AR Path="/5E890F69/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8B100E" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8B100E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4025 4650 50  0001 C CNN
F 1 "GND" H 4025 4750 50  0000 C CNN
F 2 "" H 4025 4900 50  0001 C CNN
F 3 "" H 4025 4900 50  0001 C CNN
	1    4025 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4650 3850 4675
Wire Wire Line
	3850 4675 4025 4675
$Comp
L power:GND #PWR?
U 1 1 5E8B4C1A
P 3850 6150
AR Path="/5E890F69/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8B4C1A" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8B4C1A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 5900 50  0001 C CNN
F 1 "GND" H 3850 6000 50  0000 C CNN
F 2 "" H 3850 6150 50  0001 C CNN
F 3 "" H 3850 6150 50  0001 C CNN
	1    3850 6150
	1    0    0    -1  
$EndComp
Connection ~ 3850 6550
Wire Wire Line
	3850 6550 4025 6550
Wire Wire Line
	3350 7275 3050 7275
Wire Wire Line
	3850 6525 3850 6550
$Comp
L es65-rescue:DS8T28N-my_ics U6
U 1 1 5E9B3043
P 3850 7375
AR Path="/5E890F69/5E9B3043" Ref="U6"  Part="1" 
AR Path="/5E985316/5E9B3043" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9B3043" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9B3043" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9B3043" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9B3043" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3043" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3043" Ref="U?"  Part="1" 
F 0 "U6" H 3500 7900 50  0000 C CNN
F 1 "DS8T28N" H 3625 7825 50  0000 C CNN
F 2 "" H 3850 7375 50  0001 C CNN
F 3 "" H 3850 7375 50  0001 C CNN
	1    3850 7375
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9B3044
P 3850 6525
AR Path="/5E890F69/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3044" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3044" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 6375 50  0001 C CNN
F 1 "+5V" H 3850 6675 50  0000 C CNN
F 2 "" H 3850 6525 50  0001 C CNN
F 3 "" H 3850 6525 50  0001 C CNN
	1    3850 6525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4025 6550 4025 6575
Wire Wire Line
	3350 7075 3050 7075
$Comp
L power:GND #PWR?
U 1 1 5E8B56BD
P 4025 6775
AR Path="/5E890F69/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8B56BD" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8B56BD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4025 6525 50  0001 C CNN
F 1 "GND" H 4025 6625 50  0000 C CNN
F 2 "" H 4025 6775 50  0001 C CNN
F 3 "" H 4025 6775 50  0001 C CNN
	1    4025 6775
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9B3046
P 4025 6675
AR Path="/5E890F69/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3046" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3046" Ref="C?"  Part="1" 
F 0 "C?" H 4117 6721 50  0000 L CNN
F 1 "22n" H 4117 6630 50  0000 L CNN
F 2 "" H 4025 6675 50  0001 C CNN
F 3 "~" H 4025 6675 50  0001 C CNN
	1    4025 6675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9B3047
P 3850 8025
AR Path="/5E890F69/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3047" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3047" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 7775 50  0001 C CNN
F 1 "GND" H 3850 7875 50  0000 C CNN
F 2 "" H 3850 8025 50  0001 C CNN
F 3 "" H 3850 8025 50  0001 C CNN
	1    3850 8025
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 6550 3850 6775
Wire Wire Line
	3350 7375 3050 7375
Wire Wire Line
	3350 7175 3050 7175
Wire Wire Line
	3350 5700 3300 5700
Wire Wire Line
	3300 5700 3300 5800
Wire Wire Line
	3300 5800 3350 5800
Wire Wire Line
	3300 7575 3300 7675
Connection ~ 3300 7675
Wire Wire Line
	3300 7675 2925 7675
Wire Wire Line
	3300 7675 3350 7675
Wire Wire Line
	3350 7575 3300 7575
Wire Wire Line
	4350 5200 4400 5200
Wire Wire Line
	4400 5200 4400 5600
Wire Wire Line
	4400 5600 4350 5600
Wire Wire Line
	4350 5300 4450 5300
Wire Wire Line
	4450 5300 4450 5700
Wire Wire Line
	4450 5700 4350 5700
Wire Wire Line
	4350 5400 4500 5400
Wire Wire Line
	4500 5400 4500 5800
Wire Wire Line
	4500 5800 4350 5800
Wire Wire Line
	4550 5500 4550 5900
Wire Wire Line
	4550 5900 4350 5900
Wire Wire Line
	4550 5900 4850 5900
Connection ~ 4550 5900
Wire Wire Line
	4500 5800 4850 5800
Connection ~ 4500 5800
Wire Wire Line
	4450 5700 4850 5700
Connection ~ 4450 5700
Wire Wire Line
	4350 5500 4550 5500
Wire Wire Line
	4400 5600 4850 5600
Connection ~ 4400 5600
Wire Wire Line
	4400 7075 4400 7475
Connection ~ 4400 7475
Wire Wire Line
	4450 7575 4350 7575
Wire Wire Line
	4500 7675 4350 7675
Wire Wire Line
	4450 7575 4850 7575
Wire Wire Line
	4550 7775 4350 7775
Connection ~ 4450 7575
Wire Wire Line
	4400 7475 4350 7475
Wire Wire Line
	4400 7475 4850 7475
Wire Wire Line
	4350 7075 4400 7075
Wire Wire Line
	4350 7375 4550 7375
Wire Wire Line
	4350 7275 4500 7275
Wire Wire Line
	4550 7375 4550 7775
Wire Wire Line
	4350 7175 4450 7175
Connection ~ 4550 7775
Wire Wire Line
	4450 7175 4450 7575
Wire Wire Line
	4550 7775 4850 7775
Wire Wire Line
	4500 7675 4850 7675
Wire Wire Line
	4500 7275 4500 7675
Connection ~ 4500 7675
Text Label 4850 7775 2    50   ~ 0
D0
Text Label 4850 7675 2    50   ~ 0
D1
Text Label 4850 7575 2    50   ~ 0
D2
Text Label 4850 7475 2    50   ~ 0
D3
Text Label 4850 5900 2    50   ~ 0
D4
Text Label 4850 5800 2    50   ~ 0
D5
Text Label 4850 5700 2    50   ~ 0
D6
Text Label 4850 5600 2    50   ~ 0
D7
$Comp
L 74xx:74LS00 U5
U 5 1 5E8E9EAD
P 1050 7900
AR Path="/5E890F69/5E8E9EAD" Ref="U5"  Part="5" 
AR Path="/5E985316/5E8E9EAD" Ref="U?"  Part="5" 
AR Path="/5E9856B7/5E8E9EAD" Ref="U?"  Part="5" 
AR Path="/5E985745/5E8E9EAD" Ref="U?"  Part="5" 
AR Path="/5E985791/5E8E9EAD" Ref="U?"  Part="5" 
AR Path="/5E9AC39B/5E8E9EAD" Ref="U?"  Part="5" 
AR Path="/5E9AC3F3/5E8E9EAD" Ref="U?"  Part="5" 
AR Path="/5E9B2B8D/5E8E9EAD" Ref="U?"  Part="5" 
F 0 "U5" H 1280 7946 50  0000 L CNN
F 1 "74LS00" H 1280 7855 50  0000 L CNN
F 2 "" H 1050 7900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1050 7900 50  0001 C CNN
	5    1050 7900
	1    0    0    -1  
$EndComp
$Comp
L 65xx:6551 U?
U 1 1 5E9163D3
P 7450 5375
AR Path="/5E890F69/5E9163D3" Ref="U?"  Part="1" 
AR Path="/5E985316/5E9163D3" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9163D3" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9163D3" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9163D3" Ref="U9"  Part="1" 
AR Path="/5E9AC39B/5E9163D3" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9163D3" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9163D3" Ref="U?"  Part="1" 
F 0 "U?" H 7100 6800 50  0000 C CNN
F 1 "6551" H 7150 6700 50  0000 C CIB
F 2 "" H 7450 5525 50  0001 C CNN
F 3 "http://www.6502.org/documents/datasheets/mos/mos_6551_acia.pdf" H 7450 5525 50  0001 C CNN
	1    7450 5375
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS367 U7
U 1 1 5E91D91C
P 3850 9375
AR Path="/5E890F69/5E91D91C" Ref="U7"  Part="1" 
AR Path="/5E985316/5E91D91C" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E91D91C" Ref="U?"  Part="1" 
AR Path="/5E985745/5E91D91C" Ref="U?"  Part="1" 
AR Path="/5E985791/5E91D91C" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E91D91C" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E91D91C" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E91D91C" Ref="U?"  Part="1" 
F 0 "U7" H 3525 10025 50  0000 C CNN
F 1 "N8T97" H 3600 9950 50  0000 C CNN
F 2 "" H 3850 9375 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS367" H 3850 9375 50  0001 C CNN
	1    3850 9375
	1    0    0    -1  
$EndComp
Connection ~ 3850 8450
Wire Wire Line
	3850 8425 3850 8450
Wire Wire Line
	3850 8450 3850 8675
Wire Wire Line
	3850 8450 4025 8450
Wire Wire Line
	4025 8450 4025 8475
$Comp
L Device:C_Small C?
U 1 1 5E922CAA
P 4025 8575
AR Path="/5E890F69/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E985316/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E985745/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E985791/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E922CAA" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E922CAA" Ref="C?"  Part="1" 
F 0 "C?" H 4117 8621 50  0000 L CNN
F 1 "22n" H 4117 8530 50  0000 L CNN
F 2 "" H 4025 8575 50  0001 C CNN
F 3 "~" H 4025 8575 50  0001 C CNN
	1    4025 8575
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E922CB7
P 3850 8425
AR Path="/5E890F69/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E922CB7" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E922CB7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 8275 50  0001 C CNN
F 1 "+5V" H 3850 8575 50  0000 C CNN
F 2 "" H 3850 8425 50  0001 C CNN
F 3 "" H 3850 8425 50  0001 C CNN
	1    3850 8425
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E922CC4
P 4025 8675
AR Path="/5E890F69/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E922CC4" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E922CC4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4025 8425 50  0001 C CNN
F 1 "GND" H 4025 8525 50  0000 C CNN
F 2 "" H 4025 8675 50  0001 C CNN
F 3 "" H 4025 8675 50  0001 C CNN
	1    4025 8675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E923A0E
P 3850 10075
AR Path="/5E890F69/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E923A0E" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E923A0E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 9825 50  0001 C CNN
F 1 "GND" H 3850 9925 50  0000 C CNN
F 2 "" H 3850 10075 50  0001 C CNN
F 3 "" H 3850 10075 50  0001 C CNN
	1    3850 10075
	1    0    0    -1  
$EndComp
Text Label 3050 9075 0    50   ~ 0
PH2b
Wire Wire Line
	3350 9075 3050 9075
Text Label 4750 9075 2    50   ~ 0
PH2bb
Wire Wire Line
	4350 9075 4750 9075
Text Label 6550 4375 0    50   ~ 0
PH2bb
Wire Wire Line
	6850 4375 6550 4375
Connection ~ 7450 3700
$Comp
L power:GND #PWR?
U 1 1 5E9B304A
P 7625 3925
AR Path="/5E890F69/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B304A" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B304A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7625 3675 50  0001 C CNN
F 1 "GND" H 7625 3775 50  0000 C CNN
F 2 "" H 7625 3925 50  0001 C CNN
F 3 "" H 7625 3925 50  0001 C CNN
	1    7625 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3700 7625 3700
Wire Wire Line
	7625 3700 7625 3725
$Comp
L power:+5V #PWR?
U 1 1 5E9B304B
P 7450 3675
AR Path="/5E890F69/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B304B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B304B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 3525 50  0001 C CNN
F 1 "+5V" H 7450 3825 50  0000 C CNN
F 2 "" H 7450 3675 50  0001 C CNN
F 3 "" H 7450 3675 50  0001 C CNN
	1    7450 3675
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9B304C
P 7625 3825
AR Path="/5E890F69/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B304C" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9B304C" Ref="C?"  Part="1" 
F 0 "C?" H 7717 3871 50  0000 L CNN
F 1 "22n" H 7717 3780 50  0000 L CNN
F 2 "" H 7625 3825 50  0001 C CNN
F 3 "~" H 7625 3825 50  0001 C CNN
	1    7625 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3675 7450 3700
Wire Wire Line
	7450 3700 7450 3925
$Comp
L es65-rescue:6301-my_ics U1
U 1 1 5E9B304D
P 5600 1625
AR Path="/5E890F69/5E9B304D" Ref="U1"  Part="1" 
AR Path="/5E985316/5E9B304D" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9B304D" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9B304D" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9B304D" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9B304D" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9B304D" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9B304D" Ref="U?"  Part="1" 
F 0 "U1" H 5315 2155 50  0000 C CNN
F 1 "6301" H 5365 2080 50  0000 C CNN
F 2 "" H 5300 1725 50  0001 C CNN
F 3 "" H 5300 1725 50  0001 C CNN
	1    5600 1625
	1    0    0    -1  
$EndComp
Connection ~ 5600 800 
$Comp
L power:GND #PWR?
U 1 1 5E95F466
P 5775 1025
AR Path="/5E890F69/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E95F466" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E95F466" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5775 775 50  0001 C CNN
F 1 "GND" H 5775 875 50  0000 C CNN
F 2 "" H 5775 1025 50  0001 C CNN
F 3 "" H 5775 1025 50  0001 C CNN
	1    5775 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 800  5775 800 
Wire Wire Line
	5775 800  5775 825 
$Comp
L power:+5V #PWR?
U 1 1 5E95F475
P 5600 775
AR Path="/5E890F69/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E95F475" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E95F475" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 625 50  0001 C CNN
F 1 "+5V" H 5600 925 50  0000 C CNN
F 2 "" H 5600 775 50  0001 C CNN
F 3 "" H 5600 775 50  0001 C CNN
	1    5600 775 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9B3050
P 5775 925
AR Path="/5E890F69/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3050" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3050" Ref="C?"  Part="1" 
F 0 "C?" H 5867 971 50  0000 L CNN
F 1 "22n" H 5867 880 50  0000 L CNN
F 2 "" H 5775 925 50  0001 C CNN
F 3 "~" H 5775 925 50  0001 C CNN
	1    5775 925 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 775  5600 800 
Wire Wire Line
	5600 800  5600 1125
$Comp
L power:GND #PWR?
U 1 1 5E96236E
P 5600 2425
AR Path="/5E890F69/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E96236E" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E96236E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 2175 50  0001 C CNN
F 1 "GND" H 5600 2275 50  0000 C CNN
F 2 "" H 5600 2425 50  0001 C CNN
F 3 "" H 5600 2425 50  0001 C CNN
	1    5600 2425
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E96298B
P 5150 2275
AR Path="/5E890F69/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E96298B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E96298B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5150 2025 50  0001 C CNN
F 1 "GND" H 5150 2125 50  0000 C CNN
F 2 "" H 5150 2275 50  0001 C CNN
F 3 "" H 5150 2275 50  0001 C CNN
	1    5150 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2275 5150 2225
Wire Wire Line
	5150 2125 5200 2125
Wire Wire Line
	5200 2225 5150 2225
Connection ~ 5150 2225
Wire Wire Line
	5150 2225 5150 2125
Wire Wire Line
	5200 1325 4925 1325
Wire Wire Line
	5200 1425 4925 1425
Wire Wire Line
	5200 1525 4925 1525
Wire Wire Line
	5200 1625 4925 1625
Wire Wire Line
	5200 1725 4925 1725
Wire Wire Line
	5200 1825 4925 1825
Wire Wire Line
	5200 2025 4925 2025
Wire Wire Line
	5950 1325 6300 1325
Wire Wire Line
	5950 1625 6475 1625
$Comp
L Device:C_Small C?
U 1 1 5E9B3053
P 5775 2950
AR Path="/5E890F69/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3053" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3053" Ref="C?"  Part="1" 
F 0 "C?" H 5867 2996 50  0000 L CNN
F 1 "22n" H 5867 2905 50  0000 L CNN
F 2 "" H 5775 2950 50  0001 C CNN
F 3 "~" H 5775 2950 50  0001 C CNN
	1    5775 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3850 4925 3850
$Comp
L power:+5V #PWR?
U 1 1 5E9B3054
P 5600 2800
AR Path="/5E890F69/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3054" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3054" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 2650 50  0001 C CNN
F 1 "+5V" H 5600 2950 50  0000 C CNN
F 2 "" H 5600 2800 50  0001 C CNN
F 3 "" H 5600 2800 50  0001 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2825 5600 3150
Wire Wire Line
	5600 2825 5775 2825
$Comp
L power:GND #PWR?
U 1 1 5E9B3055
P 5600 4450
AR Path="/5E890F69/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3055" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3055" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 4200 50  0001 C CNN
F 1 "GND" H 5600 4300 50  0000 C CNN
F 2 "" H 5600 4450 50  0001 C CNN
F 3 "" H 5600 4450 50  0001 C CNN
	1    5600 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 4925 3550
Connection ~ 5150 4250
Wire Wire Line
	5150 4250 5150 4150
Wire Wire Line
	5600 2800 5600 2825
Wire Wire Line
	5200 3950 4925 3950
Wire Wire Line
	5200 3450 4925 3450
$Comp
L es65-rescue:6301-my_ics U2
U 1 1 5E9B3056
P 5600 3650
AR Path="/5E890F69/5E9B3056" Ref="U2"  Part="1" 
AR Path="/5E985316/5E9B3056" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9B3056" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9B3056" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9B3056" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9B3056" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3056" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9B3056" Ref="U?"  Part="1" 
F 0 "U2" H 5315 4180 50  0000 C CNN
F 1 "6301" H 5365 4105 50  0000 C CNN
F 2 "" H 5300 3750 50  0001 C CNN
F 3 "" H 5300 3750 50  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3650 4925 3650
$Comp
L power:GND #PWR?
U 1 1 5E9641BF
P 5150 4300
AR Path="/5E890F69/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9641BF" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9641BF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5150 4050 50  0001 C CNN
F 1 "GND" H 5150 4150 50  0000 C CNN
F 2 "" H 5150 4300 50  0001 C CNN
F 3 "" H 5150 4300 50  0001 C CNN
	1    5150 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4250 5150 4250
$Comp
L power:GND #PWR?
U 1 1 5E9641CD
P 5775 3050
AR Path="/5E890F69/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9641CD" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9641CD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5775 2800 50  0001 C CNN
F 1 "GND" H 5775 2900 50  0000 C CNN
F 2 "" H 5775 3050 50  0001 C CNN
F 3 "" H 5775 3050 50  0001 C CNN
	1    5775 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4300 5150 4250
Wire Wire Line
	5150 4150 5200 4150
Wire Wire Line
	5200 3350 4925 3350
Wire Wire Line
	5200 3750 4925 3750
Wire Wire Line
	5950 3650 6225 3650
Wire Wire Line
	5950 3350 6225 3350
Wire Wire Line
	5775 2825 5775 2850
Connection ~ 5600 2825
Wire Wire Line
	5950 3550 6225 3550
Wire Wire Line
	5950 3450 6225 3450
Wire Wire Line
	5200 4050 4925 4050
Text Label 4925 1525 0    50   ~ 0
AA4
Text Label 4925 1325 0    50   ~ 0
AA2
Text Label 4925 3550 0    50   ~ 0
AA10
Text Label 4925 1825 0    50   ~ 0
AA7
Text Label 4925 3650 0    50   ~ 0
AA11
Text Label 4925 3750 0    50   ~ 0
AA12
Text Label 4925 3450 0    50   ~ 0
AA9
Text Label 4925 1625 0    50   ~ 0
AA5
Text Label 4925 3850 0    50   ~ 0
AA13
Text Label 4925 3950 0    50   ~ 0
AA14
Text Label 4925 1725 0    50   ~ 0
AA6
Text Label 4925 1425 0    50   ~ 0
AA3
Wire Wire Line
	4800 1925 4800 2600
Wire Wire Line
	4800 1925 5200 1925
Wire Wire Line
	6225 2600 6225 3350
Wire Wire Line
	4800 2600 6225 2600
Text Label 4925 3350 0    50   ~ 0
AA8
Text Label 4925 4050 0    50   ~ 0
AA15
Text Label 4925 2025 0    50   ~ 0
R{slash}Wb
Text Label 6325 4775 0    50   ~ 0
CS1_6551
Wire Wire Line
	6850 4775 6325 4775
Text Label 6325 4875 0    50   ~ 0
CS2_6551
Wire Wire Line
	6850 4875 6325 4875
Wire Wire Line
	5950 1525 6475 1525
Text Label 6475 1525 2    50   ~ 0
CS2_6551
Wire Wire Line
	6850 5175 6325 5175
Wire Wire Line
	6850 5075 6325 5075
Wire Wire Line
	4350 9175 4750 9175
Wire Wire Line
	4350 9275 4750 9275
$Comp
L power:GND #PWR?
U 1 1 5E98E825
P 7450 6825
AR Path="/5E890F69/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E98E825" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E98E825" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 6575 50  0001 C CNN
F 1 "GND" H 7450 6675 50  0000 C CNN
F 2 "" H 7450 6825 50  0001 C CNN
F 3 "" H 7450 6825 50  0001 C CNN
	1    7450 6825
	1    0    0    -1  
$EndComp
Text Label 6325 8400 0    50   ~ 0
CS1_6551
Wire Wire Line
	6850 8300 6325 8300
Wire Wire Line
	6850 8400 6325 8400
$Comp
L Device:Crystal_Small Y?
U 1 1 5E98F718
P 8350 4275
AR Path="/5E890F69/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E985316/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E9856B7/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E985745/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E985791/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E9AC39B/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E9AC3F3/5E98F718" Ref="Y?"  Part="1" 
AR Path="/5E9B2B8D/5E98F718" Ref="Y?"  Part="1" 
F 0 "Y?" H 8350 4500 50  0000 C CNN
F 1 "1M8432" H 8350 4409 50  0000 C CNN
F 2 "" H 8350 4275 50  0001 C CNN
F 3 "~" H 8350 4275 50  0001 C CNN
	1    8350 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4275 8050 4275
Wire Wire Line
	8050 4375 8475 4375
Wire Wire Line
	8475 4375 8475 4275
Wire Wire Line
	8475 4275 8450 4275
NoConn ~ 8050 4575
$Comp
L 74xx:74LS07 U8
U 1 1 5E990B47
P 10200 8350
AR Path="/5E890F69/5E990B47" Ref="U8"  Part="1" 
AR Path="/5E985316/5E990B47" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E990B47" Ref="U?"  Part="1" 
AR Path="/5E985745/5E990B47" Ref="U?"  Part="1" 
AR Path="/5E985791/5E990B47" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E990B47" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E990B47" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E990B47" Ref="U?"  Part="1" 
F 0 "U8" H 10200 8667 50  0000 C CNN
F 1 "74LS07" H 10200 8576 50  0000 C CNN
F 2 "" H 10200 8350 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 10200 8350 50  0001 C CNN
	1    10200 8350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS07 U8
U 2 1 5E991F78
P 10175 3225
AR Path="/5E890F69/5E991F78" Ref="U8"  Part="2" 
AR Path="/5E985316/5E991F78" Ref="U?"  Part="2" 
AR Path="/5E9856B7/5E991F78" Ref="U?"  Part="2" 
AR Path="/5E985745/5E991F78" Ref="U?"  Part="2" 
AR Path="/5E985791/5E991F78" Ref="U?"  Part="2" 
AR Path="/5E9AC39B/5E991F78" Ref="U?"  Part="2" 
AR Path="/5E9AC3F3/5E991F78" Ref="U?"  Part="2" 
AR Path="/5E9B2B8D/5E991F78" Ref="U?"  Part="2" 
F 0 "U8" H 10175 3542 50  0000 C CNN
F 1 "74LS07" H 10175 3451 50  0000 C CNN
F 2 "" H 10175 3225 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 10175 3225 50  0001 C CNN
	2    10175 3225
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS07 U8
U 3 1 5E992644
P 10200 4350
AR Path="/5E890F69/5E992644" Ref="U8"  Part="3" 
AR Path="/5E985316/5E992644" Ref="U?"  Part="3" 
AR Path="/5E9856B7/5E992644" Ref="U?"  Part="3" 
AR Path="/5E985745/5E992644" Ref="U?"  Part="3" 
AR Path="/5E985791/5E992644" Ref="U?"  Part="3" 
AR Path="/5E9AC39B/5E992644" Ref="U?"  Part="3" 
AR Path="/5E9AC3F3/5E992644" Ref="U?"  Part="3" 
AR Path="/5E9B2B8D/5E992644" Ref="U?"  Part="3" 
F 0 "U8" H 10200 4667 50  0000 C CNN
F 1 "74LS07" H 10200 4576 50  0000 C CNN
F 2 "" H 10200 4350 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 10200 4350 50  0001 C CNN
	3    10200 4350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS07 U8
U 6 1 5E9948C0
P 10200 7225
AR Path="/5E890F69/5E9948C0" Ref="U8"  Part="6" 
AR Path="/5E985316/5E9948C0" Ref="U?"  Part="6" 
AR Path="/5E9856B7/5E9948C0" Ref="U?"  Part="6" 
AR Path="/5E985745/5E9948C0" Ref="U?"  Part="6" 
AR Path="/5E985791/5E9948C0" Ref="U?"  Part="6" 
AR Path="/5E9AC39B/5E9948C0" Ref="U?"  Part="6" 
AR Path="/5E9AC3F3/5E9948C0" Ref="U?"  Part="6" 
AR Path="/5E9B2B8D/5E9948C0" Ref="U?"  Part="6" 
F 0 "U8" H 10200 7542 50  0000 C CNN
F 1 "74LS07" H 10200 7451 50  0000 C CNN
F 2 "" H 10200 7225 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 10200 7225 50  0001 C CNN
	6    10200 7225
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS07 U8
U 7 1 5E9954D3
P 1800 7900
AR Path="/5E890F69/5E9954D3" Ref="U8"  Part="7" 
AR Path="/5E985316/5E9954D3" Ref="U?"  Part="7" 
AR Path="/5E9856B7/5E9954D3" Ref="U?"  Part="7" 
AR Path="/5E985745/5E9954D3" Ref="U?"  Part="7" 
AR Path="/5E985791/5E9954D3" Ref="U?"  Part="7" 
AR Path="/5E9AC39B/5E9954D3" Ref="U?"  Part="7" 
AR Path="/5E9AC3F3/5E9954D3" Ref="U?"  Part="7" 
AR Path="/5E9B2B8D/5E9954D3" Ref="U?"  Part="7" 
F 0 "U8" H 2030 7946 50  0000 L CNN
F 1 "74LS07" H 2030 7855 50  0000 L CNN
F 2 "" H 1800 7900 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 1800 7900 50  0001 C CNN
	7    1800 7900
	1    0    0    -1  
$EndComp
$Comp
L Isolator:6N135 U12
U 1 1 5E999DC5
P 11025 2250
AR Path="/5E890F69/5E999DC5" Ref="U12"  Part="1" 
AR Path="/5E985316/5E999DC5" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E999DC5" Ref="U?"  Part="1" 
AR Path="/5E985745/5E999DC5" Ref="U?"  Part="1" 
AR Path="/5E985791/5E999DC5" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E999DC5" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E999DC5" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E999DC5" Ref="U?"  Part="1" 
F 0 "U12" H 11025 2675 50  0000 C CNN
F 1 "6N135" H 11025 2584 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10825 1950 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11025 2250 50  0001 L CNN
	1    11025 2250
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E99B415
P 10675 2000
AR Path="/5E890F69/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E99B415" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E99B415" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10675 1850 50  0001 C CNN
F 1 "+5V" H 10675 2150 50  0000 C CNN
F 2 "" H 10675 2000 50  0001 C CNN
F 3 "" H 10675 2000 50  0001 C CNN
	1    10675 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10675 2000 10675 2050
Wire Wire Line
	10675 2150 10725 2150
Wire Wire Line
	10725 2050 10675 2050
Connection ~ 10675 2050
Wire Wire Line
	10675 2050 10675 2150
Wire Wire Line
	10725 2450 10675 2450
Wire Wire Line
	10675 2450 10675 2525
$Comp
L power:GND #PWR?
U 1 1 5E99BEE2
P 10675 2525
AR Path="/5E890F69/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E99BEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E99BEE2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10675 2275 50  0001 C CNN
F 1 "GND" H 10675 2375 50  0000 C CNN
F 2 "" H 10675 2525 50  0001 C CNN
F 3 "" H 10675 2525 50  0001 C CNN
	1    10675 2525
	1    0    0    -1  
$EndComp
Wire Wire Line
	10725 2350 10425 2350
Text Label 10425 2350 0    50   ~ 0
~CTS1
Text Label 8300 5475 2    50   ~ 0
~CTS1
Wire Wire Line
	8300 5475 8050 5475
Wire Wire Line
	9875 3225 9625 3225
Text Label 9625 3225 0    50   ~ 0
~RTS1
Text Label 8300 5375 2    50   ~ 0
~RTS1
Wire Wire Line
	8050 5375 8300 5375
NoConn ~ 8050 5675
Wire Wire Line
	8050 5075 8300 5075
Wire Wire Line
	8050 5175 8300 5175
Text Label 8300 5075 2    50   ~ 0
TXD1
Wire Wire Line
	9900 4350 9650 4350
Text Label 9650 4350 0    50   ~ 0
TXD1
Connection ~ 10675 1150
$Comp
L power:GND #PWR?
U 1 1 5E9A2300
P 10675 1625
AR Path="/5E890F69/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9A2300" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9A2300" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10675 1375 50  0001 C CNN
F 1 "GND" H 10675 1475 50  0000 C CNN
F 2 "" H 10675 1625 50  0001 C CNN
F 3 "" H 10675 1625 50  0001 C CNN
	1    10675 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	10725 1550 10675 1550
Wire Wire Line
	10675 1550 10675 1625
$Comp
L power:+5V #PWR?
U 1 1 5E9A230F
P 10675 1100
AR Path="/5E890F69/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9A230F" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9A230F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10675 950 50  0001 C CNN
F 1 "+5V" H 10675 1250 50  0000 C CNN
F 2 "" H 10675 1100 50  0001 C CNN
F 3 "" H 10675 1100 50  0001 C CNN
	1    10675 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10675 1250 10725 1250
Wire Wire Line
	10675 1100 10675 1150
Wire Wire Line
	10675 1150 10675 1250
Wire Wire Line
	10725 1450 10425 1450
$Comp
L Isolator:6N135 U11
U 1 1 5E9A2327
P 11025 1350
AR Path="/5E890F69/5E9A2327" Ref="U11"  Part="1" 
AR Path="/5E985316/5E9A2327" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9A2327" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9A2327" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9A2327" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9A2327" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9A2327" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9A2327" Ref="U?"  Part="1" 
F 0 "U11" H 11025 1775 50  0000 C CNN
F 1 "6N135" H 11025 1684 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10825 1050 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11025 1350 50  0001 L CNN
	1    11025 1350
	-1   0    0    -1  
$EndComp
Text Label 10425 1450 0    50   ~ 0
RXD1
Wire Wire Line
	10725 1150 10675 1150
Text Label 8300 5175 2    50   ~ 0
RXD1
Wire Wire Line
	8050 5775 8100 5775
Wire Wire Line
	8100 5775 8100 5975
Wire Wire Line
	8100 5975 8050 5975
$Comp
L power:GND #PWR?
U 1 1 5E9A82A1
P 8100 6025
AR Path="/5E890F69/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9A82A1" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9A82A1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8100 5775 50  0001 C CNN
F 1 "GND" H 8100 5875 50  0000 C CNN
F 2 "" H 8100 6025 50  0001 C CNN
F 3 "" H 8100 6025 50  0001 C CNN
	1    8100 6025
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 6025 8100 5975
Connection ~ 8100 5975
Wire Wire Line
	6850 5775 6625 5775
Wire Wire Line
	6850 5875 6625 5875
Wire Wire Line
	6850 5975 6625 5975
Wire Wire Line
	6850 6075 6625 6075
Wire Wire Line
	6850 6175 6625 6175
Wire Wire Line
	6850 6275 6625 6275
Wire Wire Line
	6850 6375 6625 6375
Wire Wire Line
	6850 6475 6625 6475
Text Label 6625 6175 0    50   ~ 0
D4
Text Label 6625 5975 0    50   ~ 0
D2
Text Label 6625 5875 0    50   ~ 0
D1
Text Label 6625 6275 0    50   ~ 0
D5
Text Label 6625 6075 0    50   ~ 0
D3
Text Label 6625 6375 0    50   ~ 0
D6
Text Label 6625 6475 0    50   ~ 0
D7
Text Label 6625 5775 0    50   ~ 0
D0
Wire Wire Line
	6325 5575 6850 5575
Wire Wire Line
	3350 8975 3050 8975
Wire Wire Line
	3050 9175 3350 9175
Wire Wire Line
	3350 9275 3050 9275
Wire Wire Line
	3350 9375 3050 9375
Wire Wire Line
	3350 9475 3325 9475
Wire Wire Line
	4750 8975 4350 8975
Wire Wire Line
	3350 9675 3325 9675
Wire Wire Line
	3350 9775 3325 9775
Wire Wire Line
	4750 9375 4350 9375
Text Label 3050 9375 0    50   ~ 0
~RSTb
Text Label 4750 9375 2    50   ~ 0
~RSTbb
Text Label 6550 4275 0    50   ~ 0
~RSTbb
Wire Wire Line
	6550 4275 6850 4275
Text Label 3050 9175 0    50   ~ 0
AA1
Text Label 3050 9275 0    50   ~ 0
AA0
Wire Wire Line
	6850 4575 6325 4575
Text Label 6325 4575 0    50   ~ 0
~IRQ~_6551_1
Text Label 2475 5525 2    50   ~ 0
~IRQ~_6551_1
Wire Wire Line
	2475 5525 1975 5525
$Comp
L Connector_Generic:Conn_01x09 J?
U 1 1 5E9B507F
P 1700 5125
AR Path="/5E890F69/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E985316/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E985745/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E985791/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E9AC39B/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E9B507F" Ref="J?"  Part="1" 
AR Path="/5E9B2B8D/5E9B507F" Ref="J?"  Part="1" 
F 0 "J?" H 1618 5742 50  0000 C CNN
F 1 "Conn_01x09" H 1618 5651 50  0000 C CNN
F 2 "" H 1700 5125 50  0001 C CNN
F 3 "~" H 1700 5125 50  0001 C CNN
	1    1700 5125
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 4725 1975 4725
Wire Wire Line
	1900 4825 1975 4825
Wire Wire Line
	1900 4925 1975 4925
Wire Wire Line
	1900 5025 1975 5025
Wire Wire Line
	1900 5125 1975 5125
Wire Wire Line
	1900 5225 1975 5225
Wire Wire Line
	1900 5325 1975 5325
Wire Wire Line
	1900 5425 1975 5425
Wire Wire Line
	1900 5525 1975 5525
Wire Wire Line
	1975 4725 1975 4825
Connection ~ 1975 4825
Wire Wire Line
	1975 4825 1975 4925
Connection ~ 1975 4925
Wire Wire Line
	1975 4925 1975 5025
Connection ~ 1975 5025
Wire Wire Line
	1975 5025 1975 5125
Connection ~ 1975 5125
Wire Wire Line
	1975 5125 1975 5225
Connection ~ 1975 5225
Wire Wire Line
	1975 5225 1975 5325
Connection ~ 1975 5325
Wire Wire Line
	1975 5325 1975 5425
Connection ~ 1975 5425
Wire Wire Line
	1975 5425 1975 5525
Connection ~ 1975 5525
Text Label 2475 6625 2    50   ~ 0
~IRQ~_6551_2
Wire Wire Line
	2475 6625 1975 6625
Wire Wire Line
	1975 5825 1975 5925
Connection ~ 1975 6025
Connection ~ 1975 6125
Connection ~ 1975 5925
Connection ~ 1975 6225
Wire Wire Line
	1975 6025 1975 6125
Wire Wire Line
	1900 5825 1975 5825
Wire Wire Line
	1975 6525 1975 6625
Wire Wire Line
	1975 6325 1975 6425
Wire Wire Line
	1900 6025 1975 6025
$Comp
L Connector_Generic:Conn_01x09 J?
U 1 1 5E9BA007
P 1700 6225
AR Path="/5E890F69/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E985316/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E985745/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E985791/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E9AC39B/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E9BA007" Ref="J?"  Part="1" 
AR Path="/5E9B2B8D/5E9BA007" Ref="J?"  Part="1" 
F 0 "J?" H 1618 6842 50  0000 C CNN
F 1 "Conn_01x09" H 1618 6751 50  0000 C CNN
F 2 "" H 1700 6225 50  0001 C CNN
F 3 "~" H 1700 6225 50  0001 C CNN
	1    1700 6225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 6125 1975 6125
Wire Wire Line
	1900 6625 1975 6625
Wire Wire Line
	1975 6125 1975 6225
Wire Wire Line
	1975 5925 1975 6025
Connection ~ 1975 6625
Wire Wire Line
	1900 6525 1975 6525
Connection ~ 1975 6425
Connection ~ 1975 6525
Connection ~ 1975 6325
Wire Wire Line
	1900 6325 1975 6325
Wire Wire Line
	1975 6225 1975 6325
Wire Wire Line
	1900 6425 1975 6425
Wire Wire Line
	1900 5925 1975 5925
Wire Wire Line
	1975 6425 1975 6525
Wire Wire Line
	1900 6225 1975 6225
$Comp
L Connector_Generic:Conn_01x09 J?
U 1 1 5E9BE189
P 1350 6225
AR Path="/5E890F69/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E985316/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E985745/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E985791/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E9AC39B/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E9BE189" Ref="J?"  Part="1" 
AR Path="/5E9B2B8D/5E9BE189" Ref="J?"  Part="1" 
F 0 "J?" H 1268 6842 50  0000 C CNN
F 1 "Conn_01x09" H 1268 6751 50  0000 C CNN
F 2 "" H 1350 6225 50  0001 C CNN
F 3 "~" H 1350 6225 50  0001 C CNN
	1    1350 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5825 850  5825
Wire Wire Line
	1150 5925 850  5925
Wire Wire Line
	1150 6025 850  6025
Wire Wire Line
	1150 6125 850  6125
Wire Wire Line
	1150 6225 850  6225
Wire Wire Line
	1150 6325 850  6325
Wire Wire Line
	1150 6425 850  6425
Wire Wire Line
	1150 6525 850  6525
Text Label 850  5825 0    50   ~ 0
~IRQ0
Text Label 850  5925 0    50   ~ 0
~IRQ1
Text Label 850  6025 0    50   ~ 0
~IRQ2
Text Label 850  6125 0    50   ~ 0
~IRQ3
Text Label 850  6225 0    50   ~ 0
~IRQ4
Text Label 850  6325 0    50   ~ 0
~IRQ5
Text Label 850  6425 0    50   ~ 0
~IRQ6
Text Label 850  6525 0    50   ~ 0
~IRQ7
Text Label 2200 1550 2    50   ~ 0
~IRQ1
Text Label 2200 1950 2    50   ~ 0
~IRQ5
Text Label 2200 1850 2    50   ~ 0
~IRQ4
Text Label 2200 1750 2    50   ~ 0
~IRQ3
Text Label 2200 2150 2    50   ~ 0
~IRQ7
Text Label 2200 1650 2    50   ~ 0
~IRQ2
Text Label 2200 2050 2    50   ~ 0
~IRQ6
Text Label 2200 1450 2    50   ~ 0
~IRQ0
NoConn ~ 1150 6625
Wire Notes Line
	750  4450 2625 4450
Wire Notes Line
	2625 4450 2625 6750
Wire Notes Line
	2625 6750 750  6750
Wire Notes Line
	750  6750 750  4450
Text Notes 875  4650 0    50   ~ 0
IRQ Select
Wire Wire Line
	8050 8700 8300 8700
Wire Wire Line
	8050 8900 8300 8900
Wire Wire Line
	8100 9550 8100 9500
Wire Wire Line
	8100 9300 8100 9500
Wire Wire Line
	6850 8600 6325 8600
Text Label 6325 8100 0    50   ~ 0
~IRQ~_6551_2
Wire Wire Line
	6850 7900 6550 7900
Wire Wire Line
	6325 9100 6850 9100
Wire Wire Line
	7450 7225 7450 7450
Wire Wire Line
	6850 9400 6625 9400
$Comp
L power:GND #PWR?
U 1 1 5E9CC9E5
P 7450 10350
AR Path="/5E890F69/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9CC9E5" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9CC9E5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 10100 50  0001 C CNN
F 1 "GND" H 7450 10200 50  0000 C CNN
F 2 "" H 7450 10350 50  0001 C CNN
F 3 "" H 7450 10350 50  0001 C CNN
	1    7450 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 7800 8050 7800
Wire Wire Line
	6550 7800 6850 7800
Wire Wire Line
	6850 8100 6325 8100
$Comp
L power:+5V #PWR?
U 1 1 5E9CC9F5
P 7450 7200
AR Path="/5E890F69/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9CC9F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9CC9F5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 7050 50  0001 C CNN
F 1 "+5V" H 7450 7350 50  0000 C CNN
F 2 "" H 7450 7200 50  0001 C CNN
F 3 "" H 7450 7200 50  0001 C CNN
	1    7450 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 7225 7625 7225
Wire Wire Line
	8475 7900 8475 7800
Wire Wire Line
	7450 7200 7450 7225
Connection ~ 7450 7225
Wire Wire Line
	6850 9700 6625 9700
Text Label 6625 9400 0    50   ~ 0
D1
Text Label 8300 8700 2    50   ~ 0
RXD2
Wire Wire Line
	8100 9500 8050 9500
Text Label 6625 10000 0    50   ~ 0
D7
Connection ~ 8100 9500
Text Label 6550 7900 0    50   ~ 0
PH2bb
Text Label 8300 8900 2    50   ~ 0
~RTS2
Text Label 6625 9500 0    50   ~ 0
D2
Wire Wire Line
	6850 9900 6625 9900
$Comp
L power:GND #PWR?
U 1 1 5E9CCA12
P 8100 9550
AR Path="/5E890F69/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9CCA12" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9CCA12" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8100 9300 50  0001 C CNN
F 1 "GND" H 8100 9400 50  0000 C CNN
F 2 "" H 8100 9550 50  0001 C CNN
F 3 "" H 8100 9550 50  0001 C CNN
	1    8100 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 9300 8100 9300
Wire Wire Line
	6850 8700 6325 8700
Text Label 6625 9300 0    50   ~ 0
D0
NoConn ~ 8050 9200
Wire Wire Line
	8050 8600 8300 8600
Text Label 6625 9600 0    50   ~ 0
D3
Text Label 8300 9000 2    50   ~ 0
~CTS2
Wire Wire Line
	8300 9000 8050 9000
Wire Wire Line
	6850 9500 6625 9500
$Comp
L Device:C_Small C?
U 1 1 5E9CCA2A
P 7625 7350
AR Path="/5E890F69/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9CCA2A" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9CCA2A" Ref="C?"  Part="1" 
F 0 "C?" H 7717 7396 50  0000 L CNN
F 1 "22n" H 7717 7305 50  0000 L CNN
F 2 "" H 7625 7350 50  0001 C CNN
F 3 "~" H 7625 7350 50  0001 C CNN
	1    7625 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 9300 6625 9300
Text Label 6625 9700 0    50   ~ 0
D4
Wire Wire Line
	6850 10000 6625 10000
Wire Wire Line
	6850 9600 6625 9600
NoConn ~ 8050 8100
Text Label 6550 7800 0    50   ~ 0
~RSTbb
Text Label 6625 9800 0    50   ~ 0
D5
Wire Wire Line
	6850 9800 6625 9800
Wire Wire Line
	7625 7225 7625 7250
Wire Wire Line
	8475 7800 8450 7800
$Comp
L power:GND #PWR?
U 1 1 5E9CCA43
P 7625 7450
AR Path="/5E890F69/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9CCA43" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9CCA43" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7625 7200 50  0001 C CNN
F 1 "GND" H 7625 7300 50  0000 C CNN
F 2 "" H 7625 7450 50  0001 C CNN
F 3 "" H 7625 7450 50  0001 C CNN
	1    7625 7450
	1    0    0    -1  
$EndComp
$Comp
L 65xx:6551 U?
U 1 1 5E9CCA6B
P 7450 8900
AR Path="/5E890F69/5E9CCA6B" Ref="U?"  Part="1" 
AR Path="/5E985316/5E9CCA6B" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9CCA6B" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9CCA6B" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9CCA6B" Ref="U10"  Part="1" 
AR Path="/5E9AC39B/5E9CCA6B" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9CCA6B" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9CCA6B" Ref="U?"  Part="1" 
F 0 "U?" H 7100 10325 50  0000 C CNN
F 1 "6551" H 7150 10225 50  0000 C CIB
F 2 "" H 7450 9050 50  0001 C CNN
F 3 "http://www.6502.org/documents/datasheets/mos/mos_6551_acia.pdf" H 7450 9050 50  0001 C CNN
	1    7450 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 7900 8475 7900
Text Label 8300 8600 2    50   ~ 0
TXD2
Text Label 6625 9900 0    50   ~ 0
D6
$Comp
L Device:Crystal_Small Y?
U 1 1 5E9CCA7C
P 8350 7800
AR Path="/5E890F69/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E985316/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E9856B7/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E985745/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E985791/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E9AC39B/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E9AC3F3/5E9CCA7C" Ref="Y?"  Part="1" 
AR Path="/5E9B2B8D/5E9CCA7C" Ref="Y?"  Part="1" 
F 0 "Y?" H 8350 8025 50  0000 C CNN
F 1 "1M8432" H 8350 7934 50  0000 C CNN
F 2 "" H 8350 7800 50  0001 C CNN
F 3 "~" H 8350 7800 50  0001 C CNN
	1    8350 7800
	1    0    0    -1  
$EndComp
Text Label 6325 8300 0    50   ~ 0
CS2_6551
Text Label 6475 1625 2    50   ~ 0
CS1_6551
Wire Wire Line
	10700 4975 10700 5025
Text Label 10450 5325 0    50   ~ 0
RXD2
Wire Wire Line
	10700 5125 10750 5125
Wire Wire Line
	10750 5025 10700 5025
Connection ~ 10700 5925
Connection ~ 10700 5025
$Comp
L power:GND #PWR?
U 1 1 5E9E30F4
P 10700 6400
AR Path="/5E890F69/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9E30F4" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9E30F4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10700 6150 50  0001 C CNN
F 1 "GND" H 10700 6250 50  0000 C CNN
F 2 "" H 10700 6400 50  0001 C CNN
F 3 "" H 10700 6400 50  0001 C CNN
	1    10700 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 6325 10700 6325
Text Label 9650 7225 0    50   ~ 0
TXD2
Wire Wire Line
	9900 7225 9650 7225
$Comp
L Isolator:6N135 U13
U 1 1 5E9E310B
P 11050 5225
AR Path="/5E890F69/5E9E310B" Ref="U13"  Part="1" 
AR Path="/5E985316/5E9E310B" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9E310B" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9E310B" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9E310B" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9E310B" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9E310B" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9E310B" Ref="U?"  Part="1" 
F 0 "U13" H 11050 5650 50  0000 C CNN
F 1 "6N135" H 11050 5559 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10850 4925 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11050 5225 50  0001 L CNN
	1    11050 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10700 5425 10700 5500
$Comp
L power:GND #PWR?
U 1 1 5E9E3119
P 10700 5500
AR Path="/5E890F69/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9E3119" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9E3119" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10700 5250 50  0001 C CNN
F 1 "GND" H 10700 5350 50  0000 C CNN
F 2 "" H 10700 5500 50  0001 C CNN
F 3 "" H 10700 5500 50  0001 C CNN
	1    10700 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 5425 10700 5425
Wire Wire Line
	10700 6325 10700 6400
Text Label 9650 8350 0    50   ~ 0
~RTS2
$Comp
L power:+5V #PWR?
U 1 1 5E9E3129
P 10700 5875
AR Path="/5E890F69/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9E3129" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9E3129" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10700 5725 50  0001 C CNN
F 1 "+5V" H 10700 6025 50  0000 C CNN
F 2 "" H 10700 5875 50  0001 C CNN
F 3 "" H 10700 5875 50  0001 C CNN
	1    10700 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 8350 9650 8350
Wire Wire Line
	10750 5325 10450 5325
Wire Wire Line
	10700 5025 10700 5125
Wire Wire Line
	10700 6025 10750 6025
Wire Wire Line
	10700 5875 10700 5925
Wire Wire Line
	10700 5925 10700 6025
Wire Wire Line
	10750 6225 10450 6225
$Comp
L Isolator:6N135 U14
U 1 1 5E9E3160
P 11050 6125
AR Path="/5E890F69/5E9E3160" Ref="U14"  Part="1" 
AR Path="/5E985316/5E9E3160" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9E3160" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9E3160" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9E3160" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9E3160" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9E3160" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9E3160" Ref="U?"  Part="1" 
F 0 "U14" H 11050 6550 50  0000 C CNN
F 1 "6N135" H 11050 6459 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10850 5825 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11050 6125 50  0001 L CNN
	1    11050 6125
	-1   0    0    -1  
$EndComp
Text Label 10450 6225 0    50   ~ 0
~CTS2
Wire Wire Line
	10750 5925 10700 5925
$Comp
L power:+5V #PWR?
U 1 1 5E9E316F
P 10700 4975
AR Path="/5E890F69/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9E316F" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9E316F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10700 4825 50  0001 C CNN
F 1 "+5V" H 10700 5125 50  0000 C CNN
F 2 "" H 10700 4975 50  0001 C CNN
F 3 "" H 10700 4975 50  0001 C CNN
	1    10700 4975
	1    0    0    -1  
$EndComp
$Comp
L Isolator:6N135 U15
U 1 1 5E9F6610
P 11025 4250
AR Path="/5E890F69/5E9F6610" Ref="U15"  Part="1" 
AR Path="/5E985316/5E9F6610" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9F6610" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9F6610" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9F6610" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9F6610" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9F6610" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9F6610" Ref="U?"  Part="1" 
F 0 "U15" H 11025 4675 50  0000 C CNN
F 1 "6N135" H 11025 4584 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10825 3950 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11025 4250 50  0001 L CNN
	1    11025 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E9F9732
P 11375 4500
AR Path="/5E890F69/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9F9732" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9F9732" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11375 4250 50  0001 C CNN
F 1 "GNDD" H 11379 4345 50  0000 C CNN
F 2 "" H 11375 4500 50  0001 C CNN
F 3 "" H 11375 4500 50  0001 C CNN
	1    11375 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11375 4500 11375 4450
Wire Wire Line
	11375 4450 11325 4450
$Comp
L power:+5C #PWR?
U 1 1 5E9F9B58
P 11450 4000
AR Path="/5E890F69/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9F9B58" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9F9B58" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11450 3850 50  0001 C CNN
F 1 "+5C" H 11465 4173 50  0000 C CNN
F 2 "" H 11450 4000 50  0001 C CNN
F 3 "" H 11450 4000 50  0001 C CNN
	1    11450 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	11450 4000 11450 4050
Wire Wire Line
	11450 4150 11325 4150
Wire Wire Line
	11325 4050 11450 4050
Connection ~ 11450 4050
Wire Wire Line
	11450 4050 11450 4150
$Comp
L Regulator_Linear:LM7805_TO220 U?
U 1 1 5E9FA1CD
P 15125 775
AR Path="/5E890F69/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E985316/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9FA1CD" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E9FA1CD" Ref="U?"  Part="1" 
F 0 "U?" H 15125 1017 50  0000 C CNN
F 1 "LM7805_TO220" H 15125 926 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 15125 1000 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 15125 725 50  0001 C CNN
	1    15125 775 
	1    0    0    -1  
$EndComp
Wire Wire Line
	15125 1075 15125 1125
$Comp
L power:GNDD #PWR?
U 1 1 5E9FD902
P 15125 1200
AR Path="/5E890F69/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9FD902" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9FD902" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15125 950 50  0001 C CNN
F 1 "GNDD" H 15129 1045 50  0000 C CNN
F 2 "" H 15125 1200 50  0001 C CNN
F 3 "" H 15125 1200 50  0001 C CNN
	1    15125 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	15125 1200 15125 1125
Connection ~ 15125 1125
$Comp
L Device:C_Small C?
U 1 1 5E9FE04C
P 14675 975
AR Path="/5E890F69/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9FE04C" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9FE04C" Ref="C?"  Part="1" 
F 0 "C?" H 14767 1021 50  0000 L CNN
F 1 "100n" H 14767 930 50  0000 L CNN
F 2 "" H 14675 975 50  0001 C CNN
F 3 "~" H 14675 975 50  0001 C CNN
	1    14675 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	15125 1125 14675 1125
Wire Wire Line
	14675 1125 14675 1075
Wire Wire Line
	14675 875  14675 775 
Wire Wire Line
	14675 775  14825 775 
Wire Wire Line
	15775 1025 15775 1125
$Comp
L Device:CP_Small C?
U 1 1 5E9FBA80
P 15475 925
AR Path="/5E890F69/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9FBA80" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9FBA80" Ref="C?"  Part="1" 
F 0 "C?" H 15563 971 50  0000 L CNN
F 1 "33u" H 15563 880 50  0000 L CNN
F 2 "" H 15475 925 50  0001 C CNN
F 3 "~" H 15475 925 50  0001 C CNN
	1    15475 925 
	1    0    0    -1  
$EndComp
Wire Wire Line
	15425 775  15475 775 
Wire Wire Line
	15475 1125 15475 1025
Connection ~ 15475 1125
Wire Wire Line
	15475 775  15775 775 
Wire Wire Line
	15125 1125 15475 1125
Wire Wire Line
	15775 1125 15475 1125
Wire Wire Line
	15775 775  15775 725 
Wire Wire Line
	15475 775  15475 825 
Connection ~ 15475 775 
$Comp
L Device:C_Small C?
U 1 1 5E9FC34D
P 15775 925
AR Path="/5E890F69/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9FC34D" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E9FC34D" Ref="C?"  Part="1" 
F 0 "C?" H 15867 971 50  0000 L CNN
F 1 "100n" H 15867 880 50  0000 L CNN
F 2 "" H 15775 925 50  0001 C CNN
F 3 "~" H 15775 925 50  0001 C CNN
	1    15775 925 
	1    0    0    -1  
$EndComp
Wire Wire Line
	15775 825  15775 775 
Connection ~ 15775 775 
$Comp
L power:+5C #PWR?
U 1 1 5E9FD0F5
P 15775 725
AR Path="/5E890F69/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9FD0F5" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9FD0F5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15775 575 50  0001 C CNN
F 1 "+5C" H 15790 898 50  0000 C CNN
F 2 "" H 15775 725 50  0001 C CNN
F 3 "" H 15775 725 50  0001 C CNN
	1    15775 725 
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5EA0900B
P 14675 700
AR Path="/5E890F69/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA0900B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA0900B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14675 550 50  0001 C CNN
F 1 "+12V" H 14690 873 50  0000 C CNN
F 2 "" H 14675 700 50  0001 C CNN
F 3 "" H 14675 700 50  0001 C CNN
	1    14675 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	14675 700  14675 775 
Connection ~ 14675 775 
$Comp
L Device:R_Network05 RN?
U 1 1 5EA0ED92
P 14900 1575
AR Path="/5E890F69/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E985316/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E9856B7/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E985745/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E985791/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E9AC39B/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E9AC3F3/5EA0ED92" Ref="RN?"  Part="1" 
AR Path="/5E9B2B8D/5EA0ED92" Ref="RN?"  Part="1" 
F 0 "RN?" H 15188 1621 50  0000 L CNN
F 1 "470E" H 15188 1530 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 15275 1575 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 14900 1575 50  0001 C CNN
	1    14900 1575
	1    0    0    -1  
$EndComp
$Comp
L power:+5C #PWR?
U 1 1 5EA0F0A4
P 14700 1375
AR Path="/5E890F69/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA0F0A4" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA0F0A4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14700 1225 50  0001 C CNN
F 1 "+5C" H 14715 1548 50  0000 C CNN
F 2 "" H 14700 1375 50  0001 C CNN
F 3 "" H 14700 1375 50  0001 C CNN
	1    14700 1375
	1    0    0    -1  
$EndComp
Wire Wire Line
	14700 1775 14700 2050
Text Label 14700 2050 1    50   ~ 0
PLUP1
Text Label 14800 2050 1    50   ~ 0
PLUP2
Text Label 15000 2050 1    50   ~ 0
PLUP3
Text Label 15100 2050 1    50   ~ 0
PLUP4
Wire Wire Line
	14800 1775 14800 2050
Wire Wire Line
	15000 1775 15000 2050
Wire Wire Line
	15100 1775 15100 2050
Text Label 10450 4150 0    50   ~ 0
PLUP1
Wire Wire Line
	10725 4150 10450 4150
Wire Wire Line
	10500 4350 10725 4350
Text Label 10425 3025 0    50   ~ 0
PLUP2
Wire Wire Line
	10475 3225 10700 3225
Connection ~ 11425 2925
$Comp
L Isolator:6N135 U16
U 1 1 5EA25575
P 11000 3125
AR Path="/5E890F69/5EA25575" Ref="U16"  Part="1" 
AR Path="/5E985316/5EA25575" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EA25575" Ref="U?"  Part="1" 
AR Path="/5E985745/5EA25575" Ref="U?"  Part="1" 
AR Path="/5E985791/5EA25575" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5EA25575" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EA25575" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5EA25575" Ref="U?"  Part="1" 
F 0 "U16" H 11000 3550 50  0000 C CNN
F 1 "6N135" H 11000 3459 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10800 2825 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11000 3125 50  0001 L CNN
	1    11000 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	11350 3375 11350 3325
Wire Wire Line
	11350 3325 11300 3325
Wire Wire Line
	11425 2925 11425 3025
Wire Wire Line
	11300 2925 11425 2925
$Comp
L power:+5C #PWR?
U 1 1 5EA25586
P 11425 2875
AR Path="/5E890F69/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA25586" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA25586" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11425 2725 50  0001 C CNN
F 1 "+5C" H 11440 3048 50  0000 C CNN
F 2 "" H 11425 2875 50  0001 C CNN
F 3 "" H 11425 2875 50  0001 C CNN
	1    11425 2875
	1    0    0    -1  
$EndComp
Wire Wire Line
	11425 2875 11425 2925
Wire Wire Line
	11425 3025 11300 3025
Wire Wire Line
	10700 3025 10425 3025
$Comp
L power:GNDD #PWR?
U 1 1 5EA25596
P 11350 3375
AR Path="/5E890F69/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA25596" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA25596" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11350 3125 50  0001 C CNN
F 1 "GNDD" H 11354 3220 50  0000 C CNN
F 2 "" H 11350 3375 50  0001 C CNN
F 3 "" H 11350 3375 50  0001 C CNN
	1    11350 3375
	1    0    0    -1  
$EndComp
Connection ~ 11450 6925
Wire Wire Line
	11325 6925 11450 6925
Wire Wire Line
	10500 7225 10725 7225
Wire Wire Line
	11450 6875 11450 6925
Wire Wire Line
	11375 7375 11375 7325
Text Label 10450 7025 0    50   ~ 0
PLUP3
Wire Wire Line
	11450 6925 11450 7025
$Comp
L power:GNDD #PWR?
U 1 1 5EA27EFB
P 11375 7375
AR Path="/5E890F69/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA27EFB" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA27EFB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11375 7125 50  0001 C CNN
F 1 "GNDD" H 11379 7220 50  0000 C CNN
F 2 "" H 11375 7375 50  0001 C CNN
F 3 "" H 11375 7375 50  0001 C CNN
	1    11375 7375
	1    0    0    -1  
$EndComp
Wire Wire Line
	10725 7025 10450 7025
$Comp
L Isolator:6N135 U17
U 1 1 5EA27F10
P 11025 7125
AR Path="/5E890F69/5EA27F10" Ref="U17"  Part="1" 
AR Path="/5E985316/5EA27F10" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EA27F10" Ref="U?"  Part="1" 
AR Path="/5E985745/5EA27F10" Ref="U?"  Part="1" 
AR Path="/5E985791/5EA27F10" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5EA27F10" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EA27F10" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5EA27F10" Ref="U?"  Part="1" 
F 0 "U17" H 11025 7550 50  0000 C CNN
F 1 "6N135" H 11025 7459 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10825 6825 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11025 7125 50  0001 L CNN
	1    11025 7125
	1    0    0    -1  
$EndComp
Wire Wire Line
	11450 7025 11325 7025
$Comp
L power:+5C #PWR?
U 1 1 5EA27F1E
P 11450 6875
AR Path="/5E890F69/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA27F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA27F1E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11450 6725 50  0001 C CNN
F 1 "+5C" H 11465 7048 50  0000 C CNN
F 2 "" H 11450 6875 50  0001 C CNN
F 3 "" H 11450 6875 50  0001 C CNN
	1    11450 6875
	1    0    0    -1  
$EndComp
Wire Wire Line
	11375 7325 11325 7325
$Comp
L Isolator:6N135 U18
U 1 1 5EA30FB6
P 11025 8250
AR Path="/5E890F69/5EA30FB6" Ref="U18"  Part="1" 
AR Path="/5E985316/5EA30FB6" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EA30FB6" Ref="U?"  Part="1" 
AR Path="/5E985745/5EA30FB6" Ref="U?"  Part="1" 
AR Path="/5E985791/5EA30FB6" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5EA30FB6" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EA30FB6" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5EA30FB6" Ref="U?"  Part="1" 
F 0 "U18" H 11025 8675 50  0000 C CNN
F 1 "6N135" H 11025 8584 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10825 7950 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 11025 8250 50  0001 L CNN
	1    11025 8250
	1    0    0    -1  
$EndComp
$Comp
L power:+5C #PWR?
U 1 1 5EA30FC3
P 11450 8000
AR Path="/5E890F69/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA30FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA30FC3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11450 7850 50  0001 C CNN
F 1 "+5C" H 11465 8173 50  0000 C CNN
F 2 "" H 11450 8000 50  0001 C CNN
F 3 "" H 11450 8000 50  0001 C CNN
	1    11450 8000
	1    0    0    -1  
$EndComp
Wire Wire Line
	11450 8050 11450 8150
$Comp
L power:GNDD #PWR?
U 1 1 5EA30FD1
P 11375 8500
AR Path="/5E890F69/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA30FD1" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA30FD1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11375 8250 50  0001 C CNN
F 1 "GNDD" H 11379 8345 50  0000 C CNN
F 2 "" H 11375 8500 50  0001 C CNN
F 3 "" H 11375 8500 50  0001 C CNN
	1    11375 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11375 8500 11375 8450
Wire Wire Line
	11450 8000 11450 8050
Connection ~ 11450 8050
Wire Wire Line
	11450 8150 11325 8150
Text Label 10450 8150 0    50   ~ 0
PLUP4
Wire Wire Line
	10725 8150 10450 8150
Wire Wire Line
	11375 8450 11325 8450
Wire Wire Line
	10500 8350 10725 8350
Wire Wire Line
	11325 8050 11450 8050
Wire Notes Line
	2800 4450 4950 4450
Wire Notes Line
	4950 4450 4950 10275
Wire Notes Line
	2800 10275 2800 4450
Text Notes 2900 4625 0    50   ~ 0
Buffer
Wire Wire Line
	7850 1075 7850 1350
Wire Wire Line
	7500 2550 7775 2550
Wire Wire Line
	6675 2750 6675 2500
Wire Wire Line
	7100 1550 7025 1550
Wire Wire Line
	7025 1250 7025 1350
Wire Wire Line
	7025 1650 7025 1750
Connection ~ 7025 1350
Connection ~ 8450 1950
Wire Wire Line
	7100 1950 7025 1950
Wire Wire Line
	7100 1650 7025 1650
Wire Wire Line
	7025 1550 7025 1650
Text Label 9725 1250 2    50   ~ 0
D0
Wire Wire Line
	8450 1950 8500 1950
Wire Wire Line
	7100 1350 7025 1350
Connection ~ 8350 1850
Wire Wire Line
	9500 1350 9725 1350
Connection ~ 7025 1650
Text Label 9725 1950 2    50   ~ 0
D7
$Comp
L 74xx:74LS373 U4
U 1 1 5E8C40C2
P 9000 1750
AR Path="/5E890F69/5E8C40C2" Ref="U4"  Part="1" 
AR Path="/5E985316/5E8C40C2" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E8C40C2" Ref="U?"  Part="1" 
AR Path="/5E985745/5E8C40C2" Ref="U?"  Part="1" 
AR Path="/5E985791/5E8C40C2" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E8C40C2" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E8C40C2" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E8C40C2" Ref="U?"  Part="1" 
F 0 "U4" H 8625 2500 50  0000 C CNN
F 1 "74LS373" H 8750 2425 50  0000 C CNN
F 2 "" H 9000 1750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS373" H 9000 1750 50  0001 C CNN
	1    9000 1750
	1    0    0    -1  
$EndComp
Connection ~ 8150 1650
Wire Wire Line
	7025 1450 7025 1550
Wire Wire Line
	8150 1650 8150 1075
Wire Wire Line
	7850 1350 8500 1350
Wire Wire Line
	7025 1350 7025 1450
Text Label 9725 1850 2    50   ~ 0
D6
Wire Wire Line
	8350 1850 8500 1850
Wire Wire Line
	7750 1250 8500 1250
Wire Wire Line
	7950 1450 8500 1450
Wire Wire Line
	7100 1250 7025 1250
Connection ~ 7025 1750
Wire Wire Line
	7025 1850 7025 1950
Wire Wire Line
	9500 1450 9725 1450
Text Label 9725 1550 2    50   ~ 0
D3
Wire Wire Line
	7025 1950 7025 2050
Connection ~ 7025 1550
Connection ~ 7025 1850
Wire Wire Line
	8450 2650 8450 2250
Wire Wire Line
	9000 725  9000 950 
Wire Wire Line
	8250 1075 8250 1750
$Comp
L power:+5V #PWR?
U 1 1 5E8D88ED
P 8450 675
AR Path="/5E890F69/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8D88ED" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8D88ED" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8450 525 50  0001 C CNN
F 1 "+5V" H 8450 825 50  0000 C CNN
F 2 "" H 8450 675 50  0001 C CNN
F 3 "" H 8450 675 50  0001 C CNN
	1    8450 675 
	-1   0    0    -1  
$EndComp
Text Label 9725 1750 2    50   ~ 0
D5
$Comp
L power:GND #PWR?
U 1 1 5E8D614A
P 7025 2050
AR Path="/5E890F69/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8D614A" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8D614A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7025 1800 50  0001 C CNN
F 1 "GND" H 7025 1900 50  0000 C CNN
F 2 "" H 7025 2050 50  0001 C CNN
F 3 "" H 7025 2050 50  0001 C CNN
	1    7025 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8250 1750 8500 1750
Wire Wire Line
	8050 1550 8500 1550
$Comp
L Device:R_Network08 RN?
U 1 1 5E8D6A2C
P 8050 875
AR Path="/5E890F69/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E985316/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E9856B7/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E985745/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E985791/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E9AC39B/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E9AC3F3/5E8D6A2C" Ref="RN?"  Part="1" 
AR Path="/5E9B2B8D/5E8D6A2C" Ref="RN?"  Part="1" 
F 0 "RN?" H 8438 921 50  0000 L CNN
F 1 "10k" H 8438 830 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP9" V 8525 875 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 8050 875 50  0001 C CNN
	1    8050 875 
	-1   0    0    -1  
$EndComp
Connection ~ 7950 1450
Wire Wire Line
	7350 2650 7500 2650
$Comp
L 74xx:74LS00 U5
U 1 1 5E8E4A87
P 7050 2650
AR Path="/5E890F69/5E8E4A87" Ref="U5"  Part="1" 
AR Path="/5E985316/5E8E4A87" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E8E4A87" Ref="U?"  Part="1" 
AR Path="/5E985745/5E8E4A87" Ref="U?"  Part="1" 
AR Path="/5E985791/5E8E4A87" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E8E4A87" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E8E4A87" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E8E4A87" Ref="U?"  Part="1" 
F 0 "U5" H 7050 2975 50  0000 C CNN
F 1 "74LS00" H 7050 2884 50  0000 C CNN
F 2 "" H 7050 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 7050 2650 50  0001 C CNN
	1    7050 2650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U5
U 2 1 5E8E7842
P 8075 2650
AR Path="/5E890F69/5E8E7842" Ref="U5"  Part="2" 
AR Path="/5E985316/5E8E7842" Ref="U?"  Part="2" 
AR Path="/5E9856B7/5E8E7842" Ref="U?"  Part="2" 
AR Path="/5E985745/5E8E7842" Ref="U?"  Part="2" 
AR Path="/5E985791/5E8E7842" Ref="U?"  Part="2" 
AR Path="/5E9AC39B/5E8E7842" Ref="U?"  Part="2" 
AR Path="/5E9AC3F3/5E8E7842" Ref="U?"  Part="2" 
AR Path="/5E9B2B8D/5E8E7842" Ref="U?"  Part="2" 
F 0 "U5" H 8075 2975 50  0000 C CNN
F 1 "74LS00" H 8075 2884 50  0000 C CNN
F 2 "" H 8075 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 8075 2650 50  0001 C CNN
	2    8075 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2650 7500 2550
Text Label 7475 2750 0    50   ~ 0
PH2bb
Wire Wire Line
	6750 2750 6675 2750
$Comp
L power:+5V #PWR?
U 1 1 5E93CEE2
P 6675 2500
AR Path="/5E890F69/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E93CEE2" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E93CEE2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6675 2350 50  0001 C CNN
F 1 "+5V" H 6675 2650 50  0000 C CNN
F 2 "" H 6675 2500 50  0001 C CNN
F 3 "" H 6675 2500 50  0001 C CNN
	1    6675 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1850 8350 1075
Connection ~ 7850 1350
Wire Wire Line
	7750 1075 7750 1250
Wire Wire Line
	9500 1750 9725 1750
Connection ~ 7025 1450
Connection ~ 8050 1550
Wire Wire Line
	7775 2750 7475 2750
Wire Wire Line
	9000 700  9000 725 
Text Label 9725 1350 2    50   ~ 0
D1
Wire Wire Line
	8450 1075 8450 1950
$Comp
L Device:C_Small C?
U 1 1 5E8C5629
P 9175 850
AR Path="/5E890F69/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E985316/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E985745/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E985791/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E9AC39B/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E8C5629" Ref="C?"  Part="1" 
AR Path="/5E9B2B8D/5E8C5629" Ref="C?"  Part="1" 
F 0 "C?" H 9267 896 50  0000 L CNN
F 1 "22n" H 9267 805 50  0000 L CNN
F 2 "" H 9175 850 50  0001 C CNN
F 3 "~" H 9175 850 50  0001 C CNN
	1    9175 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2250 8500 2250
Wire Wire Line
	8150 1650 8500 1650
$Comp
L power:+5V #PWR?
U 1 1 5E8C560D
P 9000 700
AR Path="/5E890F69/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8C560D" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8C560D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9000 550 50  0001 C CNN
F 1 "+5V" H 9000 850 50  0000 C CNN
F 2 "" H 9000 700 50  0001 C CNN
F 3 "" H 9000 700 50  0001 C CNN
	1    9000 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9175 725  9175 750 
Wire Wire Line
	7100 1450 7025 1450
Wire Wire Line
	9000 725  9175 725 
Wire Wire Line
	9500 1550 9725 1550
Text Label 9725 1450 2    50   ~ 0
D2
Wire Wire Line
	7100 1750 7025 1750
Connection ~ 7025 1950
Wire Wire Line
	7950 1450 7950 1075
Wire Wire Line
	7100 1850 7025 1850
Connection ~ 8250 1750
Wire Wire Line
	8375 2650 8450 2650
$Comp
L power:GND #PWR?
U 1 1 5E8C561B
P 9175 950
AR Path="/5E890F69/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8C561B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8C561B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9175 700 50  0001 C CNN
F 1 "GND" H 9175 800 50  0000 C CNN
F 2 "" H 9175 950 50  0001 C CNN
F 3 "" H 9175 950 50  0001 C CNN
	1    9175 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 1650 9725 1650
Wire Wire Line
	8050 1075 8050 1550
Wire Wire Line
	9500 1250 9725 1250
Connection ~ 9000 725 
Text Label 9725 1650 2    50   ~ 0
D4
$Comp
L power:GND #PWR?
U 1 1 5E8D5C6A
P 9000 2550
AR Path="/5E890F69/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8D5C6A" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8D5C6A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9000 2300 50  0001 C CNN
F 1 "GND" H 9000 2400 50  0000 C CNN
F 2 "" H 9000 2550 50  0001 C CNN
F 3 "" H 9000 2550 50  0001 C CNN
	1    9000 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2550 6550 2550
Wire Wire Line
	9500 1850 9725 1850
Connection ~ 7750 1250
Wire Wire Line
	7025 1750 7025 1850
Wire Wire Line
	9500 1950 9725 1950
Wire Wire Line
	5950 1425 6550 1425
Wire Wire Line
	6550 2550 6550 1425
Wire Wire Line
	11325 1250 11600 1250
Text Label 15450 2050 1    50   ~ 0
PLUP5
Wire Wire Line
	15750 1775 15750 2050
$Comp
L power:+5C #PWR?
U 1 1 5EA87215
P 15450 1375
AR Path="/5E890F69/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA87215" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA87215" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15450 1225 50  0001 C CNN
F 1 "+5C" H 15465 1548 50  0000 C CNN
F 2 "" H 15450 1375 50  0001 C CNN
F 3 "" H 15450 1375 50  0001 C CNN
	1    15450 1375
	1    0    0    -1  
$EndComp
Wire Wire Line
	15550 1775 15550 2050
Wire Wire Line
	15450 1775 15450 2050
Text Label 15650 2050 1    50   ~ 0
PLUP7
Wire Wire Line
	15650 1775 15650 2050
Text Label 15550 2050 1    50   ~ 0
PLUP6
$Comp
L Device:R_Network05 RN?
U 1 1 5EA8722C
P 15650 1575
AR Path="/5E890F69/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E985316/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E9856B7/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E985745/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E985791/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E9AC39B/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E9AC3F3/5EA8722C" Ref="RN?"  Part="1" 
AR Path="/5E9B2B8D/5EA8722C" Ref="RN?"  Part="1" 
F 0 "RN?" H 15938 1621 50  0000 L CNN
F 1 "470E" H 15938 1530 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 16025 1575 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 15650 1575 50  0001 C CNN
	1    15650 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	15850 1775 15850 2050
Text Label 15750 2050 1    50   ~ 0
PLUP8
NoConn ~ 14900 1775
Text Label 11600 1250 2    50   ~ 0
PLUP5
Text Label 11600 2150 2    50   ~ 0
PLUP6
Wire Wire Line
	11325 2150 11600 2150
Wire Wire Line
	11350 5125 11625 5125
Text Label 11625 5125 2    50   ~ 0
PLUP7
Wire Wire Line
	11350 6025 11625 6025
Text Label 11625 6025 2    50   ~ 0
PLUP8
Wire Wire Line
	11325 1450 11600 1450
Wire Wire Line
	11325 2350 11600 2350
Wire Wire Line
	11300 3225 11575 3225
Wire Wire Line
	11325 4350 11600 4350
Wire Wire Line
	11350 5325 11625 5325
Wire Wire Line
	11350 6225 11625 6225
Wire Wire Line
	11325 7225 11600 7225
Wire Wire Line
	11325 8350 11600 8350
Wire Wire Line
	725  10325 775  10325
$Comp
L 74xx:74LS07 U8
U 5 1 5E993D6C
P 1850 10325
AR Path="/5E890F69/5E993D6C" Ref="U8"  Part="5" 
AR Path="/5E985316/5E993D6C" Ref="U?"  Part="5" 
AR Path="/5E9856B7/5E993D6C" Ref="U?"  Part="5" 
AR Path="/5E985745/5E993D6C" Ref="U?"  Part="5" 
AR Path="/5E985791/5E993D6C" Ref="U?"  Part="5" 
AR Path="/5E9AC39B/5E993D6C" Ref="U?"  Part="5" 
AR Path="/5E9AC3F3/5E993D6C" Ref="U?"  Part="5" 
AR Path="/5E9B2B8D/5E993D6C" Ref="U?"  Part="5" 
F 0 "U8" H 1850 10642 50  0000 C CNN
F 1 "74LS07" H 1850 10551 50  0000 C CNN
F 2 "" H 1850 10325 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 1850 10325 50  0001 C CNN
	5    1850 10325
	1    0    0    -1  
$EndComp
Wire Notes Line
	4950 10275 2800 10275
$Comp
L 74xx:74LS07 U8
U 4 1 5E993104
P 1075 10325
AR Path="/5E890F69/5E993104" Ref="U8"  Part="4" 
AR Path="/5E985316/5E993104" Ref="U?"  Part="4" 
AR Path="/5E9856B7/5E993104" Ref="U?"  Part="4" 
AR Path="/5E985745/5E993104" Ref="U?"  Part="4" 
AR Path="/5E985791/5E993104" Ref="U?"  Part="4" 
AR Path="/5E9AC39B/5E993104" Ref="U?"  Part="4" 
AR Path="/5E9AC3F3/5E993104" Ref="U?"  Part="4" 
AR Path="/5E9B2B8D/5E993104" Ref="U?"  Part="4" 
F 0 "U8" H 1075 10642 50  0000 C CNN
F 1 "74LS07" H 1075 10551 50  0000 C CNN
F 2 "" H 1075 10325 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/sn74ls07.pdf" H 1075 10325 50  0001 C CNN
	4    1075 10325
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U5
U 3 1 5E8E8993
P 1175 10950
AR Path="/5E890F69/5E8E8993" Ref="U5"  Part="3" 
AR Path="/5E985316/5E8E8993" Ref="U?"  Part="3" 
AR Path="/5E9856B7/5E8E8993" Ref="U?"  Part="3" 
AR Path="/5E985745/5E8E8993" Ref="U?"  Part="3" 
AR Path="/5E985791/5E8E8993" Ref="U?"  Part="3" 
AR Path="/5E9AC39B/5E8E8993" Ref="U?"  Part="3" 
AR Path="/5E9AC3F3/5E8E8993" Ref="U?"  Part="3" 
AR Path="/5E9B2B8D/5E8E8993" Ref="U?"  Part="3" 
F 0 "U5" H 1175 11275 50  0000 C CNN
F 1 "74LS00" H 1175 11184 50  0000 C CNN
F 2 "" H 1175 10950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1175 10950 50  0001 C CNN
	3    1175 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1680 11070 1605 11070
Wire Wire Line
	1680 10870 1605 10870
$Comp
L power:+5V #PWR?
U 1 1 5EA97462
P 800 10800
AR Path="/5E890F69/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA97462" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA97462" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 800 10650 50  0001 C CNN
F 1 "+5V" H 800 10950 50  0000 C CNN
F 2 "" H 800 10800 50  0001 C CNN
F 3 "" H 800 10800 50  0001 C CNN
	1    800  10800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 10325 1550 10325
Wire Wire Line
	875  10850 800  10850
$Comp
L power:+5V #PWR?
U 1 1 5EA903EC
P 725 10225
AR Path="/5E890F69/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA903EC" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA903EC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 725 10075 50  0001 C CNN
F 1 "+5V" H 725 10375 50  0000 C CNN
F 2 "" H 725 10225 50  0001 C CNN
F 3 "" H 725 10225 50  0001 C CNN
	1    725  10225
	1    0    0    -1  
$EndComp
Connection ~ 800  10850
$Comp
L 74xx:74LS00 U5
U 4 1 5E8E8E3E
P 1980 10970
AR Path="/5E890F69/5E8E8E3E" Ref="U5"  Part="4" 
AR Path="/5E985316/5E8E8E3E" Ref="U?"  Part="4" 
AR Path="/5E9856B7/5E8E8E3E" Ref="U?"  Part="4" 
AR Path="/5E985745/5E8E8E3E" Ref="U?"  Part="4" 
AR Path="/5E985791/5E8E8E3E" Ref="U?"  Part="4" 
AR Path="/5E9AC39B/5E8E8E3E" Ref="U?"  Part="4" 
AR Path="/5E9AC3F3/5E8E8E3E" Ref="U?"  Part="4" 
AR Path="/5E9B2B8D/5E8E8E3E" Ref="U?"  Part="4" 
F 0 "U5" H 1980 11295 50  0000 C CNN
F 1 "74LS00" H 1980 11204 50  0000 C CNN
F 2 "" H 1980 10970 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 1980 10970 50  0001 C CNN
	4    1980 10970
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 10225 1500 10325
Wire Wire Line
	875  11050 800  11050
$Comp
L power:+5V #PWR?
U 1 1 5EA909B9
P 1500 10225
AR Path="/5E890F69/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA909B9" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EA909B9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1500 10075 50  0001 C CNN
F 1 "+5V" H 1500 10375 50  0000 C CNN
F 2 "" H 1500 10225 50  0001 C CNN
F 3 "" H 1500 10225 50  0001 C CNN
	1    1500 10225
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  10850 800  10800
Connection ~ 1605 10870
Wire Wire Line
	1605 10870 1605 10820
$Comp
L power:+5V #PWR?
U 1 1 5E9455C9
P 1605 10820
AR Path="/5E890F69/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9455C9" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9455C9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1605 10670 50  0001 C CNN
F 1 "+5V" H 1605 10970 50  0000 C CNN
F 2 "" H 1605 10820 50  0001 C CNN
F 3 "" H 1605 10820 50  0001 C CNN
	1    1605 10820
	1    0    0    -1  
$EndComp
Wire Wire Line
	1605 11070 1605 10870
Wire Wire Line
	725  10225 725  10325
Wire Wire Line
	800  11050 800  10850
Wire Notes Line
	11050 625  11050 10150
Text Label 11600 1450 2    50   ~ 0
RXD1i
Text Label 11775 3250 0    50   ~ 0
RXD1i
Text Label 11600 2350 2    50   ~ 0
~CTS1i
Text Label 11925 4700 0    50   ~ 0
~CTS1i
Text Label 11575 3225 2    50   ~ 0
~RTS1i
Text Label 11600 4350 2    50   ~ 0
TXD1i
Text Label 12250 2800 0    50   ~ 0
TXD1i
Text Label 11625 5325 2    50   ~ 0
RXD2i
Text Label 11825 8550 0    50   ~ 0
RXD2i
Text Label 11625 6225 2    50   ~ 0
~CTS2i
Text Label 11875 7225 0    50   ~ 0
~CTS2i
Text Label 11600 7225 2    50   ~ 0
TXD2i
Text Label 12125 9550 0    50   ~ 0
TXD2i
Text Label 11600 8350 2    50   ~ 0
~RTS2i
Text Label 12100 8200 0    50   ~ 0
~RTS2i
$Comp
L Connector_Generic:Conn_02x25_Top_Bottom J?
U 1 1 5EAC0867
P 14025 5500
AR Path="/5E890F69/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E985316/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E985745/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E985791/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E9AC39B/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5EAC0867" Ref="J?"  Part="1" 
AR Path="/5E9B2B8D/5EAC0867" Ref="J?"  Part="1" 
F 0 "J?" H 14075 6917 50  0000 C CNN
F 1 "Conn_02x25_Top_Bottom" H 14075 6826 50  0000 C CNN
F 2 "" H 14025 5500 50  0001 C CNN
F 3 "~" H 14025 5500 50  0001 C CNN
	1    14025 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	13825 5800 13750 5800
Wire Wire Line
	13825 5900 13750 5900
Wire Wire Line
	13825 6000 13750 6000
Wire Wire Line
	13825 6100 13750 6100
Wire Wire Line
	13825 6200 13750 6200
Wire Wire Line
	13825 6300 13750 6300
Wire Wire Line
	13825 6400 13750 6400
Wire Wire Line
	13825 6500 13750 6500
Wire Wire Line
	13825 6600 13750 6600
Wire Wire Line
	13825 6700 13750 6700
Wire Wire Line
	13750 5800 13750 5900
Connection ~ 13750 5900
Wire Wire Line
	13750 5900 13750 6000
Connection ~ 13750 6000
Wire Wire Line
	13750 6000 13750 6100
Connection ~ 13750 6100
Wire Wire Line
	13750 6100 13750 6200
Connection ~ 13750 6200
Wire Wire Line
	13750 6200 13750 6300
Connection ~ 13750 6300
Wire Wire Line
	13750 6300 13750 6400
Connection ~ 13750 6400
Wire Wire Line
	13750 6400 13750 6500
Connection ~ 13750 6500
Wire Wire Line
	13750 6500 13750 6600
Connection ~ 13750 6600
Wire Wire Line
	13750 6600 13750 6700
Connection ~ 13750 6700
Wire Wire Line
	13750 6700 13750 6800
$Comp
L power:GNDD #PWR?
U 1 1 5EAC4C0F
P 13750 6800
AR Path="/5E890F69/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAC4C0F" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EAC4C0F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13750 6550 50  0001 C CNN
F 1 "GNDD" H 13754 6645 50  0000 C CNN
F 2 "" H 13750 6800 50  0001 C CNN
F 3 "" H 13750 6800 50  0001 C CNN
	1    13750 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	13825 4300 13750 4300
Wire Wire Line
	13750 4300 13750 4800
Connection ~ 13750 5800
Wire Wire Line
	13825 4800 13750 4800
Connection ~ 13750 4800
Wire Wire Line
	13750 4800 13750 4900
Wire Wire Line
	13825 4900 13750 4900
Connection ~ 13750 4900
Wire Wire Line
	13750 4900 13750 5000
Wire Wire Line
	13825 5000 13750 5000
Connection ~ 13750 5000
Wire Wire Line
	13750 5000 13750 5100
Wire Wire Line
	13825 5100 13750 5100
Connection ~ 13750 5100
Wire Wire Line
	13750 5100 13750 5200
Wire Wire Line
	13825 5200 13750 5200
Connection ~ 13750 5200
Wire Wire Line
	13750 5200 13750 5300
Wire Wire Line
	13825 5300 13750 5300
Connection ~ 13750 5300
Wire Wire Line
	13750 5300 13750 5600
Wire Wire Line
	13825 5600 13750 5600
Connection ~ 13750 5600
Wire Wire Line
	13750 5600 13750 5700
Wire Wire Line
	13825 5700 13750 5700
Connection ~ 13750 5700
Wire Wire Line
	13750 5700 13750 5800
$Comp
L power:GNDD #PWR?
U 1 1 5EACB898
P 14425 6800
AR Path="/5E890F69/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EACB898" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EACB898" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14425 6550 50  0001 C CNN
F 1 "GNDD" H 14429 6645 50  0000 C CNN
F 2 "" H 14425 6800 50  0001 C CNN
F 3 "" H 14425 6800 50  0001 C CNN
	1    14425 6800
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5EACC29C
P 14350 7025
AR Path="/5E890F69/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EACC29C" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EACC29C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14350 7125 50  0001 C CNN
F 1 "-12V" H 14350 7175 50  0000 C CNN
F 2 "" H 14350 7025 50  0001 C CNN
F 3 "" H 14350 7025 50  0001 C CNN
	1    14350 7025
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5EAD2924
P 14600 6575
AR Path="/5E890F69/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAD2924" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EAD2924" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14600 6425 50  0001 C CNN
F 1 "+12V" H 14615 6748 50  0000 C CNN
F 2 "" H 14600 6575 50  0001 C CNN
F 3 "" H 14600 6575 50  0001 C CNN
	1    14600 6575
	1    0    0    -1  
$EndComp
Wire Wire Line
	14600 6700 14600 6575
Wire Wire Line
	14325 6700 14600 6700
Wire Wire Line
	14325 6100 14425 6100
Wire Wire Line
	14425 6100 14425 6200
Wire Wire Line
	14325 6200 14425 6200
Connection ~ 14425 6200
Wire Wire Line
	14425 6200 14425 6300
Wire Wire Line
	14325 6300 14425 6300
Connection ~ 14425 6300
Wire Wire Line
	14425 6300 14425 6400
Wire Wire Line
	14325 6400 14425 6400
Connection ~ 14425 6400
Wire Wire Line
	14425 6400 14425 6500
Wire Wire Line
	14325 6500 14425 6500
Connection ~ 14425 6500
Wire Wire Line
	14325 6000 14425 6000
Wire Wire Line
	14425 6000 14425 6100
Connection ~ 14425 6100
Wire Wire Line
	14325 4300 14425 4300
Wire Wire Line
	14425 4300 14425 4400
Connection ~ 14425 6000
Wire Wire Line
	14325 4400 14425 4400
Connection ~ 14425 4400
Wire Wire Line
	14425 4400 14425 4500
Wire Wire Line
	14325 4500 14425 4500
Connection ~ 14425 4500
Wire Wire Line
	14425 4500 14425 4600
Wire Wire Line
	14325 4600 14425 4600
Connection ~ 14425 4600
Wire Wire Line
	14425 4600 14425 4700
Wire Wire Line
	14325 4700 14425 4700
Connection ~ 14425 4700
Wire Wire Line
	14425 4700 14425 4800
Wire Wire Line
	14325 4800 14425 4800
Connection ~ 14425 4800
Wire Wire Line
	14425 4800 14425 4900
Wire Wire Line
	14325 4900 14425 4900
Connection ~ 14425 4900
Wire Wire Line
	14425 4900 14425 5000
Wire Wire Line
	14325 5000 14425 5000
Connection ~ 14425 5000
Wire Wire Line
	14425 5000 14425 5100
Wire Wire Line
	14325 5100 14425 5100
Connection ~ 14425 5100
Wire Wire Line
	14425 5100 14425 5200
Wire Wire Line
	14325 5200 14425 5200
Connection ~ 14425 5200
Wire Wire Line
	14425 5200 14425 5300
Wire Wire Line
	14325 5300 14425 5300
Connection ~ 14425 5300
Wire Wire Line
	14425 5300 14425 5400
Wire Wire Line
	14325 5400 14425 5400
Connection ~ 14425 5400
Wire Wire Line
	14425 5400 14425 5500
Wire Wire Line
	14325 5500 14425 5500
Connection ~ 14425 5500
Wire Wire Line
	14425 5500 14425 6000
Wire Wire Line
	13425 5400 13825 5400
$Comp
L power:+12V #PWR?
U 1 1 5EADB7FA
P 13600 5325
AR Path="/5E890F69/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EADB7FA" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EADB7FA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13600 5175 50  0001 C CNN
F 1 "+12V" H 13615 5498 50  0000 C CNN
F 2 "" H 13600 5325 50  0001 C CNN
F 3 "" H 13600 5325 50  0001 C CNN
	1    13600 5325
	1    0    0    -1  
$EndComp
Wire Wire Line
	13600 5325 13600 5500
Wire Wire Line
	13600 5500 13825 5500
Wire Wire Line
	14425 6500 14425 6800
Wire Wire Line
	14325 6600 14350 6600
Wire Wire Line
	14350 6600 14350 7025
$Comp
L power:-12V #PWR?
U 1 1 5EADCA07
P 13425 5475
AR Path="/5E890F69/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EADCA07" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EADCA07" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13425 5575 50  0001 C CNN
F 1 "-12V" H 13440 5648 50  0000 C CNN
F 2 "" H 13425 5475 50  0001 C CNN
F 3 "" H 13425 5475 50  0001 C CNN
	1    13425 5475
	-1   0    0    1   
$EndComp
Wire Wire Line
	13425 5475 13425 5400
$Comp
L es65-rescue:N8T15-my_ics U21
U 1 1 5E881E08
P 12825 2650
AR Path="/5E890F69/5E881E08" Ref="U21"  Part="1" 
AR Path="/5E985316/5E881E08" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E881E08" Ref="U?"  Part="1" 
AR Path="/5E985745/5E881E08" Ref="U?"  Part="1" 
AR Path="/5E985791/5E881E08" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E881E08" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E881E08" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E881E08" Ref="U?"  Part="1" 
F 0 "U21" H 12825 3025 50  0000 C CNN
F 1 "N8T15" H 12825 2934 50  0000 C CNN
F 2 "" H 12825 2650 50  0001 C CNN
F 3 "" H 12825 2650 50  0001 C CNN
	1    12825 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5E88824C
P 14000 775
AR Path="/5E890F69/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E88824C" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E88824C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14000 625 50  0001 C CNN
F 1 "+12V" H 14015 948 50  0000 C CNN
F 2 "" H 14000 775 50  0001 C CNN
F 3 "" H 14000 775 50  0001 C CNN
	1    14000 775 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E888837
P 13900 1775
AR Path="/5E890F69/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E888837" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E888837" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13900 1525 50  0001 C CNN
F 1 "GNDD" H 13904 1620 50  0000 C CNN
F 2 "" H 13900 1775 50  0001 C CNN
F 3 "" H 13900 1775 50  0001 C CNN
	1    13900 1775
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5E888E1F
P 14100 1825
AR Path="/5E890F69/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E888E1F" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E888E1F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14100 1925 50  0001 C CNN
F 1 "-12V" H 14115 1998 50  0000 C CNN
F 2 "" H 14100 1825 50  0001 C CNN
F 3 "" H 14100 1825 50  0001 C CNN
	1    14100 1825
	-1   0    0    1   
$EndComp
Wire Wire Line
	14100 1825 14100 1775
$Comp
L power:+12V #PWR?
U 1 1 5E88E93D
P 13275 750
AR Path="/5E890F69/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E88E93D" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E88E93D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13275 600 50  0001 C CNN
F 1 "+12V" H 13290 923 50  0000 C CNN
F 2 "" H 13275 750 50  0001 C CNN
F 3 "" H 13275 750 50  0001 C CNN
	1    13275 750 
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5E88E94A
P 13375 1800
AR Path="/5E890F69/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E88E94A" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E88E94A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13375 1900 50  0001 C CNN
F 1 "-12V" H 13390 1973 50  0000 C CNN
F 2 "" H 13375 1800 50  0001 C CNN
F 3 "" H 13375 1800 50  0001 C CNN
	1    13375 1800
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E88E957
P 13175 1750
AR Path="/5E890F69/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E88E957" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E88E957" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13175 1500 50  0001 C CNN
F 1 "GNDD" H 13179 1595 50  0000 C CNN
F 2 "" H 13175 1750 50  0001 C CNN
F 3 "" H 13175 1750 50  0001 C CNN
	1    13175 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	13375 1800 13375 1750
$Comp
L es65-rescue:N8T15-my_ics U21
U 3 1 5E88E967
P 13275 1250
AR Path="/5E890F69/5E88E967" Ref="U21"  Part="3" 
AR Path="/5E985316/5E88E967" Ref="U?"  Part="3" 
AR Path="/5E9856B7/5E88E967" Ref="U?"  Part="3" 
AR Path="/5E985745/5E88E967" Ref="U?"  Part="3" 
AR Path="/5E985791/5E88E967" Ref="U?"  Part="3" 
AR Path="/5E9AC39B/5E88E967" Ref="U?"  Part="3" 
AR Path="/5E9AC3F3/5E88E967" Ref="U?"  Part="3" 
AR Path="/5E9B2B8D/5E88E967" Ref="U?"  Part="3" 
F 0 "U21" H 13505 1296 50  0000 L CNN
F 1 "N8T15" H 13505 1205 50  0000 L CNN
F 2 "" H 13275 1250 50  0001 C CNN
F 3 "" H 13275 1250 50  0001 C CNN
	3    13275 1250
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T15-my_ics U22
U 3 1 5E883B40
P 14000 1275
AR Path="/5E890F69/5E883B40" Ref="U22"  Part="3" 
AR Path="/5E985316/5E883B40" Ref="U?"  Part="3" 
AR Path="/5E9856B7/5E883B40" Ref="U?"  Part="3" 
AR Path="/5E985745/5E883B40" Ref="U?"  Part="3" 
AR Path="/5E985791/5E883B40" Ref="U?"  Part="3" 
AR Path="/5E9AC39B/5E883B40" Ref="U?"  Part="3" 
AR Path="/5E9AC3F3/5E883B40" Ref="U?"  Part="3" 
AR Path="/5E9B2B8D/5E883B40" Ref="U?"  Part="3" 
F 0 "U22" H 14230 1321 50  0000 L CNN
F 1 "N8T15" H 14230 1230 50  0000 L CNN
F 2 "" H 14000 1275 50  0001 C CNN
F 3 "" H 14000 1275 50  0001 C CNN
	3    14000 1275
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T15-my_ics U22
U 2 1 5E893EB5
P 12700 8050
AR Path="/5E890F69/5E893EB5" Ref="U22"  Part="2" 
AR Path="/5E985316/5E893EB5" Ref="U?"  Part="2" 
AR Path="/5E9856B7/5E893EB5" Ref="U?"  Part="2" 
AR Path="/5E985745/5E893EB5" Ref="U?"  Part="2" 
AR Path="/5E985791/5E893EB5" Ref="U?"  Part="2" 
AR Path="/5E9AC39B/5E893EB5" Ref="U?"  Part="2" 
AR Path="/5E9AC3F3/5E893EB5" Ref="U?"  Part="2" 
AR Path="/5E9B2B8D/5E893EB5" Ref="U?"  Part="2" 
F 0 "U22" H 12700 8425 50  0000 C CNN
F 1 "N8T15" H 12700 8334 50  0000 C CNN
F 2 "" H 12700 8050 50  0001 C CNN
F 3 "" H 12700 8050 50  0001 C CNN
	2    12700 8050
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T15-my_ics U22
U 1 1 5E893EC6
P 12700 9400
AR Path="/5E890F69/5E893EC6" Ref="U22"  Part="1" 
AR Path="/5E985316/5E893EC6" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E893EC6" Ref="U?"  Part="1" 
AR Path="/5E985745/5E893EC6" Ref="U?"  Part="1" 
AR Path="/5E985791/5E893EC6" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E893EC6" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E893EC6" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E893EC6" Ref="U?"  Part="1" 
F 0 "U22" H 12700 9775 50  0000 C CNN
F 1 "N8T15" H 12700 9684 50  0000 C CNN
F 2 "" H 12700 9400 50  0001 C CNN
F 3 "" H 12700 9400 50  0001 C CNN
	1    12700 9400
	1    0    0    -1  
$EndComp
Wire Wire Line
	12400 7900 12375 7900
Wire Wire Line
	12375 7900 12375 8000
Wire Wire Line
	12375 8200 12400 8200
Wire Wire Line
	12400 8000 12375 8000
Connection ~ 12375 8000
Wire Wire Line
	12400 8100 12375 8100
Wire Wire Line
	12375 8000 12375 8100
Connection ~ 12375 8100
Wire Wire Line
	12375 8100 12375 8200
Wire Wire Line
	12525 2600 12500 2600
Wire Wire Line
	12500 2500 12500 2600
Connection ~ 12500 2600
Wire Wire Line
	12525 2500 12500 2500
Wire Wire Line
	12500 2800 12525 2800
Wire Wire Line
	12500 2700 12500 2800
Wire Wire Line
	12525 2700 12500 2700
Connection ~ 12500 2700
Wire Wire Line
	12500 2600 12500 2700
Wire Wire Line
	12375 9450 12375 9550
Wire Wire Line
	12400 9250 12375 9250
Wire Wire Line
	12400 9350 12375 9350
Wire Wire Line
	12375 9250 12375 9350
Connection ~ 12375 9450
Wire Wire Line
	12375 9550 12400 9550
Wire Wire Line
	12400 9450 12375 9450
Connection ~ 12375 9350
Wire Wire Line
	12375 9350 12375 9450
Wire Wire Line
	12125 9550 12375 9550
Connection ~ 12375 9550
Wire Wire Line
	12100 8200 12375 8200
Connection ~ 12375 8200
Wire Wire Line
	12250 2800 12500 2800
Connection ~ 12500 2800
Wire Wire Line
	13125 2650 13600 2650
Wire Wire Line
	13600 2650 13600 4400
Wire Wire Line
	13600 4400 13825 4400
$Comp
L es65-rescue:N8T16-my_ics U19
U 1 1 5E8DCCE0
P 12700 3250
AR Path="/5E890F69/5E8DCCE0" Ref="U19"  Part="1" 
AR Path="/5E985316/5E8DCCE0" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E8DCCE0" Ref="U?"  Part="1" 
AR Path="/5E985745/5E8DCCE0" Ref="U?"  Part="1" 
AR Path="/5E985791/5E8DCCE0" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E8DCCE0" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E8DCCE0" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E8DCCE0" Ref="U?"  Part="1" 
F 0 "U19" H 13050 3500 50  0000 C CNN
F 1 "N8T16" H 13025 3425 50  0000 C CNN
F 2 "" H 12700 3250 50  0001 C CNN
F 3 "" H 12700 3250 50  0001 C CNN
	1    12700 3250
	-1   0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T16-my_ics U19
U 2 1 5E8DE7B9
P 12825 4700
AR Path="/5E890F69/5E8DE7B9" Ref="U19"  Part="2" 
AR Path="/5E985316/5E8DE7B9" Ref="U?"  Part="2" 
AR Path="/5E9856B7/5E8DE7B9" Ref="U?"  Part="2" 
AR Path="/5E985745/5E8DE7B9" Ref="U?"  Part="2" 
AR Path="/5E985791/5E8DE7B9" Ref="U?"  Part="2" 
AR Path="/5E9AC39B/5E8DE7B9" Ref="U?"  Part="2" 
AR Path="/5E9AC3F3/5E8DE7B9" Ref="U?"  Part="2" 
AR Path="/5E9B2B8D/5E8DE7B9" Ref="U?"  Part="2" 
F 0 "U19" H 13150 4950 50  0000 C CNN
F 1 "N8T16" H 13125 4875 50  0000 C CNN
F 2 "" H 12825 4700 50  0001 C CNN
F 3 "" H 12825 4700 50  0001 C CNN
	2    12825 4700
	-1   0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T16-my_ics U20
U 3 1 5E8DF3BE
P 12600 1250
AR Path="/5E890F69/5E8DF3BE" Ref="U20"  Part="3" 
AR Path="/5E985316/5E8DF3BE" Ref="U?"  Part="3" 
AR Path="/5E9856B7/5E8DF3BE" Ref="U?"  Part="3" 
AR Path="/5E985745/5E8DF3BE" Ref="U?"  Part="3" 
AR Path="/5E985791/5E8DF3BE" Ref="U?"  Part="3" 
AR Path="/5E9AC39B/5E8DF3BE" Ref="U?"  Part="3" 
AR Path="/5E9AC3F3/5E8DF3BE" Ref="U?"  Part="3" 
AR Path="/5E9B2B8D/5E8DF3BE" Ref="U?"  Part="3" 
F 0 "U20" H 12828 1296 50  0000 L CNN
F 1 "N8T16" H 12828 1205 50  0000 L CNN
F 2 "" H 12600 1250 50  0001 C CNN
F 3 "" H 12600 1250 50  0001 C CNN
	3    12600 1250
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5E8E1A15
P 12600 750
AR Path="/5E890F69/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8E1A15" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8E1A15" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12600 600 50  0001 C CNN
F 1 "+12V" H 12615 923 50  0000 C CNN
F 2 "" H 12600 750 50  0001 C CNN
F 3 "" H 12600 750 50  0001 C CNN
	1    12600 750 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E8E219B
P 12600 1750
AR Path="/5E890F69/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8E219B" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8E219B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12600 1500 50  0001 C CNN
F 1 "GNDD" H 12604 1595 50  0000 C CNN
F 2 "" H 12600 1750 50  0001 C CNN
F 3 "" H 12600 1750 50  0001 C CNN
	1    12600 1750
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T16-my_ics U19
U 3 1 5E8E27A8
P 11925 1225
AR Path="/5E890F69/5E8E27A8" Ref="U19"  Part="3" 
AR Path="/5E985316/5E8E27A8" Ref="U?"  Part="3" 
AR Path="/5E9856B7/5E8E27A8" Ref="U?"  Part="3" 
AR Path="/5E985745/5E8E27A8" Ref="U?"  Part="3" 
AR Path="/5E985791/5E8E27A8" Ref="U?"  Part="3" 
AR Path="/5E9AC39B/5E8E27A8" Ref="U?"  Part="3" 
AR Path="/5E9AC3F3/5E8E27A8" Ref="U?"  Part="3" 
AR Path="/5E9B2B8D/5E8E27A8" Ref="U?"  Part="3" 
F 0 "U19" H 12153 1271 50  0000 L CNN
F 1 "N8T16" H 12153 1180 50  0000 L CNN
F 2 "" H 11925 1225 50  0001 C CNN
F 3 "" H 11925 1225 50  0001 C CNN
	3    11925 1225
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E8E27B5
P 11925 1725
AR Path="/5E890F69/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8E27B5" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8E27B5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11925 1475 50  0001 C CNN
F 1 "GNDD" H 11929 1570 50  0000 C CNN
F 2 "" H 11925 1725 50  0001 C CNN
F 3 "" H 11925 1725 50  0001 C CNN
	1    11925 1725
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5E8E27C2
P 11925 725
AR Path="/5E890F69/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8E27C2" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8E27C2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 11925 575 50  0001 C CNN
F 1 "+12V" H 11940 898 50  0000 C CNN
F 2 "" H 11925 725 50  0001 C CNN
F 3 "" H 11925 725 50  0001 C CNN
	1    11925 725 
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T16-my_ics U20
U 2 1 5E8E6C72
P 12775 7225
AR Path="/5E890F69/5E8E6C72" Ref="U20"  Part="2" 
AR Path="/5E985316/5E8E6C72" Ref="U?"  Part="2" 
AR Path="/5E9856B7/5E8E6C72" Ref="U?"  Part="2" 
AR Path="/5E985745/5E8E6C72" Ref="U?"  Part="2" 
AR Path="/5E985791/5E8E6C72" Ref="U?"  Part="2" 
AR Path="/5E9AC39B/5E8E6C72" Ref="U?"  Part="2" 
AR Path="/5E9AC3F3/5E8E6C72" Ref="U?"  Part="2" 
AR Path="/5E9B2B8D/5E8E6C72" Ref="U?"  Part="2" 
F 0 "U20" H 13125 7475 50  0000 C CNN
F 1 "N8T16" H 13100 7400 50  0000 C CNN
F 2 "" H 12775 7225 50  0001 C CNN
F 3 "" H 12775 7225 50  0001 C CNN
	2    12775 7225
	-1   0    0    -1  
$EndComp
$Comp
L es65-rescue:N8T16-my_ics U20
U 1 1 5E8E6C83
P 12775 8550
AR Path="/5E890F69/5E8E6C83" Ref="U20"  Part="1" 
AR Path="/5E985316/5E8E6C83" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E8E6C83" Ref="U?"  Part="1" 
AR Path="/5E985745/5E8E6C83" Ref="U?"  Part="1" 
AR Path="/5E985791/5E8E6C83" Ref="U?"  Part="1" 
AR Path="/5E9AC39B/5E8E6C83" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E8E6C83" Ref="U?"  Part="1" 
AR Path="/5E9B2B8D/5E8E6C83" Ref="U?"  Part="1" 
F 0 "U20" H 13100 8800 50  0000 C CNN
F 1 "N8T16" H 13075 8725 50  0000 C CNN
F 2 "" H 12775 8550 50  0001 C CNN
F 3 "" H 12775 8550 50  0001 C CNN
	1    12775 8550
	-1   0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5E8E9D23
P 13125 3600
AR Path="/5E890F69/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8E9D23" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8E9D23" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13125 3350 50  0001 C CNN
F 1 "GNDD" H 13129 3445 50  0000 C CNN
F 2 "" H 13125 3600 50  0001 C CNN
F 3 "" H 13125 3600 50  0001 C CNN
	1    13125 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	13125 3600 13125 3350
Wire Wire Line
	13125 3150 13050 3150
Wire Wire Line
	13050 3350 13125 3350
Connection ~ 13125 3350
Wire Wire Line
	13125 3350 13125 3150
Wire Wire Line
	13250 4800 13250 4600
Wire Wire Line
	13250 5050 13250 4800
$Comp
L power:GNDD #PWR?
U 1 1 5E8EB4C6
P 13250 5050
AR Path="/5E890F69/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8EB4C6" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8EB4C6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13250 4800 50  0001 C CNN
F 1 "GNDD" H 13254 4895 50  0000 C CNN
F 2 "" H 13250 5050 50  0001 C CNN
F 3 "" H 13250 5050 50  0001 C CNN
	1    13250 5050
	-1   0    0    -1  
$EndComp
Connection ~ 13250 4800
Wire Wire Line
	13250 4600 13175 4600
Wire Wire Line
	13175 4800 13250 4800
Wire Wire Line
	13200 7125 13125 7125
Wire Wire Line
	13200 7575 13200 7325
Wire Wire Line
	13200 8650 13200 8450
Wire Wire Line
	13200 8900 13200 8650
Connection ~ 13200 7325
$Comp
L power:GNDD #PWR?
U 1 1 5E8EBC23
P 13200 8900
AR Path="/5E890F69/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8EBC23" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8EBC23" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13200 8650 50  0001 C CNN
F 1 "GNDD" H 13204 8745 50  0000 C CNN
F 2 "" H 13200 8900 50  0001 C CNN
F 3 "" H 13200 8900 50  0001 C CNN
	1    13200 8900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	13200 7325 13200 7125
$Comp
L power:GNDD #PWR?
U 1 1 5E8EBC31
P 13200 7575
AR Path="/5E890F69/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8EBC31" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E8EBC31" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13200 7325 50  0001 C CNN
F 1 "GNDD" H 13204 7420 50  0000 C CNN
F 2 "" H 13200 7575 50  0001 C CNN
F 3 "" H 13200 7575 50  0001 C CNN
	1    13200 7575
	-1   0    0    -1  
$EndComp
Connection ~ 13200 8650
Wire Wire Line
	13200 8450 13125 8450
Wire Wire Line
	13125 7325 13200 7325
Wire Wire Line
	13125 8650 13200 8650
Text Label 15850 2050 1    50   ~ 0
PLUP9
Text Label 13425 3550 2    50   ~ 0
PLUP9
Wire Wire Line
	13425 3550 13050 3550
Text Label 13550 5000 2    50   ~ 0
PLUP9
Wire Wire Line
	13550 5000 13175 5000
Text Label 13500 8850 2    50   ~ 0
PLUP9
Wire Wire Line
	13500 7525 13125 7525
Text Label 13500 7525 2    50   ~ 0
PLUP9
Wire Wire Line
	13500 8850 13125 8850
Wire Wire Line
	12050 3250 11775 3250
Text Label 12150 4300 0    50   ~ 0
~RTS1i
Wire Wire Line
	12425 4100 12425 4200
Connection ~ 12425 4200
Wire Wire Line
	12450 4200 12425 4200
Wire Wire Line
	12425 4200 12425 4300
Wire Wire Line
	12425 4300 12450 4300
$Comp
L es65-rescue:N8T15-my_ics U21
U 2 1 5E8830FD
P 12750 4150
AR Path="/5E890F69/5E8830FD" Ref="U21"  Part="2" 
AR Path="/5E985316/5E8830FD" Ref="U?"  Part="2" 
AR Path="/5E9856B7/5E8830FD" Ref="U?"  Part="2" 
AR Path="/5E985745/5E8830FD" Ref="U?"  Part="2" 
AR Path="/5E985791/5E8830FD" Ref="U?"  Part="2" 
AR Path="/5E9AC39B/5E8830FD" Ref="U?"  Part="2" 
AR Path="/5E9AC3F3/5E8830FD" Ref="U?"  Part="2" 
AR Path="/5E9B2B8D/5E8830FD" Ref="U?"  Part="2" 
F 0 "U21" H 12750 4525 50  0000 C CNN
F 1 "N8T15" H 12750 4434 50  0000 C CNN
F 2 "" H 12750 4150 50  0001 C CNN
F 3 "" H 12750 4150 50  0001 C CNN
	2    12750 4150
	1    0    0    -1  
$EndComp
Connection ~ 12425 4300
Wire Wire Line
	12450 4000 12425 4000
Connection ~ 12425 4100
Wire Wire Line
	12425 4300 12150 4300
Wire Wire Line
	12425 4000 12425 4100
Wire Wire Line
	12450 4100 12425 4100
Wire Wire Line
	13050 3250 13475 3250
Wire Wire Line
	13475 3250 13475 4500
Wire Wire Line
	13475 4500 13825 4500
Wire Wire Line
	13050 4150 13350 4150
Wire Wire Line
	13350 4150 13350 4600
Wire Wire Line
	13350 4600 13825 4600
Wire Wire Line
	13175 4700 13650 4700
Wire Wire Line
	12175 4700 11925 4700
Wire Wire Line
	12125 7225 11875 7225
Wire Wire Line
	11825 8550 12125 8550
Wire Wire Line
	13125 7225 14725 7225
Wire Wire Line
	14725 7225 14725 5900
Wire Wire Line
	14325 5900 14725 5900
Wire Wire Line
	13000 8050 14850 8050
Wire Wire Line
	14850 8050 14850 5800
Wire Wire Line
	14325 5800 14850 5800
Wire Wire Line
	13125 8550 14975 8550
Wire Wire Line
	14975 8550 14975 5700
Wire Wire Line
	14325 5700 14975 5700
Wire Wire Line
	15125 9400 15125 5600
Wire Wire Line
	13000 9400 15125 9400
Wire Wire Line
	14325 5600 15125 5600
Wire Wire Line
	3300 700  3575 700 
Wire Wire Line
	3300 800  3575 800 
Wire Wire Line
	3300 900  3575 900 
Wire Wire Line
	3300 1000 3575 1000
Wire Wire Line
	3300 1100 3575 1100
Wire Wire Line
	3300 1200 3575 1200
Wire Wire Line
	3300 1300 3575 1300
Wire Wire Line
	3300 1400 3575 1400
Text Label 3575 800  2    50   ~ 0
~IRQ1
Text Label 3575 1200 2    50   ~ 0
~IRQ5
Text Label 3575 1100 2    50   ~ 0
~IRQ4
Text Label 3575 1000 2    50   ~ 0
~IRQ3
Text Label 3575 1400 2    50   ~ 0
~IRQ7
Text Label 3575 900  2    50   ~ 0
~IRQ2
Text Label 3575 1300 2    50   ~ 0
~IRQ6
Text Label 3575 700  2    50   ~ 0
~IRQ0
Text HLabel 3300 700  0    50   Input ~ 0
IRQ0
Text HLabel 3300 800  0    50   Input ~ 0
IRQ1
Text HLabel 3300 900  0    50   Input ~ 0
IRQ2
Text HLabel 3300 1000 0    50   Input ~ 0
IRQ3
Text HLabel 3300 1100 0    50   Input ~ 0
IRQ4
Text HLabel 3300 1200 0    50   Input ~ 0
IRQ5
Text HLabel 3300 1300 0    50   Input ~ 0
IRQ6
Text HLabel 3300 1400 0    50   Input ~ 0
IRQ7
NoConn ~ 1050 3050
Text Label 4375 3275 2    50   ~ 0
R{slash}Wb
Text Label 2225 2850 2    50   ~ 0
PH1b
Text Label 3050 8975 0    50   ~ 0
PH1b
Text Label 4750 8975 2    50   ~ 0
PH1bb
Text Label 8200 2150 0    50   ~ 0
PH1bb
Wire Wire Line
	8200 2150 8500 2150
Wire Wire Line
	4075 3075 4375 3075
Text Label 4375 3075 2    50   ~ 0
PH1b
Text HLabel 4075 3075 0    50   Input ~ 0
PH1b
$Comp
L es65-rescue:SUB-D25-my_conn J?
U 1 1 5E98CEB1
P 14075 3100
AR Path="/5E890F69/5E98CEB1" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E98CEB1" Ref="J?"  Part="1" 
AR Path="/5E985745/5E98CEB1" Ref="J?"  Part="1" 
AR Path="/5E985791/5E98CEB1" Ref="J?"  Part="1" 
AR Path="/5E9AC39B/5E98CEB1" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E98CEB1" Ref="J?"  Part="1" 
AR Path="/5E9B2B8D/5E98CEB1" Ref="J?"  Part="1" 
F 0 "J?" H 14075 3875 50  0000 C CNN
F 1 "SUB-D25" H 14075 3784 50  0000 C CNN
F 2 "" H 14075 3100 50  0001 C CNN
F 3 "" H 14075 3100 50  0001 C CNN
	1    14075 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	13875 2550 13750 2550
$Comp
L power:GNDD #PWR?
U 1 1 5E98E0DF
P 13750 3875
AR Path="/5E890F69/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E98E0DF" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E98E0DF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13750 3625 50  0001 C CNN
F 1 "GNDD" H 13754 3720 50  0000 C CNN
F 2 "" H 13750 3875 50  0001 C CNN
F 3 "" H 13750 3875 50  0001 C CNN
	1    13750 3875
	1    0    0    -1  
$EndComp
Wire Wire Line
	13875 3050 13750 3050
Connection ~ 13750 3050
Wire Wire Line
	13750 3050 13750 3150
Wire Wire Line
	13875 3150 13750 3150
Connection ~ 13750 3150
Wire Wire Line
	13750 3150 13750 3250
Wire Wire Line
	13875 3250 13750 3250
Connection ~ 13750 3250
Wire Wire Line
	13750 3250 13750 3350
Wire Wire Line
	13875 3350 13750 3350
Connection ~ 13750 3350
Wire Wire Line
	13750 3350 13750 3450
Wire Wire Line
	13875 3450 13750 3450
Connection ~ 13750 3450
Wire Wire Line
	13750 3450 13750 3550
Wire Wire Line
	13875 3550 13750 3550
Connection ~ 13750 3550
Wire Wire Line
	13750 3550 13750 3875
Wire Wire Line
	13750 2550 13750 3050
Wire Wire Line
	14275 2600 14375 2600
Wire Wire Line
	14375 2600 14375 2700
$Comp
L power:GNDD #PWR?
U 1 1 5E9914E7
P 14375 3850
AR Path="/5E890F69/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9914E7" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E9914E7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14375 3600 50  0001 C CNN
F 1 "GNDD" H 14379 3695 50  0000 C CNN
F 2 "" H 14375 3850 50  0001 C CNN
F 3 "" H 14375 3850 50  0001 C CNN
	1    14375 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	14275 2700 14375 2700
Connection ~ 14375 2700
Wire Wire Line
	14375 2700 14375 2800
Wire Wire Line
	14275 2800 14375 2800
Connection ~ 14375 2800
Wire Wire Line
	14375 2800 14375 2900
Wire Wire Line
	14275 2900 14375 2900
Connection ~ 14375 2900
Wire Wire Line
	14375 2900 14375 3000
Wire Wire Line
	14275 3000 14375 3000
Connection ~ 14375 3000
Wire Wire Line
	14375 3000 14375 3100
Wire Wire Line
	14275 3100 14375 3100
Connection ~ 14375 3100
Wire Wire Line
	14375 3100 14375 3200
Wire Wire Line
	14275 3200 14375 3200
Connection ~ 14375 3200
Wire Wire Line
	14375 3200 14375 3300
Wire Wire Line
	14275 3300 14375 3300
Connection ~ 14375 3300
Wire Wire Line
	14375 3300 14375 3400
Wire Wire Line
	14275 3400 14375 3400
Connection ~ 14375 3400
Wire Wire Line
	14375 3400 14375 3500
Wire Wire Line
	14275 3500 14375 3500
Connection ~ 14375 3500
Wire Wire Line
	14375 3500 14375 3600
Wire Wire Line
	14275 3600 14375 3600
Connection ~ 14375 3600
Wire Wire Line
	14375 3600 14375 3700
Wire Wire Line
	14275 3700 14375 3700
Connection ~ 14375 3700
Wire Wire Line
	14375 3700 14375 3850
Wire Wire Line
	13875 2650 13600 2650
Connection ~ 13600 2650
Wire Wire Line
	13875 2750 13475 2750
Wire Wire Line
	13475 2750 13475 3250
Connection ~ 13475 3250
Wire Wire Line
	13875 2850 13550 2850
Wire Wire Line
	13550 2850 13550 4150
Wire Wire Line
	13550 4150 13350 4150
Connection ~ 13350 4150
Wire Wire Line
	13875 2950 13650 2950
Wire Wire Line
	13650 2950 13650 4700
Connection ~ 13650 4700
Wire Wire Line
	13650 4700 13825 4700
Wire Wire Line
	15850 6550 15950 6550
Wire Wire Line
	15850 6650 15950 6650
Connection ~ 15325 6100
Connection ~ 15325 6200
Wire Wire Line
	15850 6350 15950 6350
Wire Wire Line
	15950 5550 15950 5650
Wire Wire Line
	15850 5650 15950 5650
Connection ~ 15950 6650
Connection ~ 15950 6550
Wire Wire Line
	15850 6450 15950 6450
Connection ~ 15950 5750
Wire Wire Line
	15950 5750 15950 5850
Wire Wire Line
	15850 6250 15950 6250
Connection ~ 15950 5950
Wire Wire Line
	15850 5750 15950 5750
Wire Wire Line
	15325 5500 15325 6000
Wire Wire Line
	15850 5950 15950 5950
Wire Wire Line
	15950 6450 15950 6550
Wire Wire Line
	15950 5950 15950 6050
Connection ~ 15950 6250
Wire Wire Line
	15950 6050 15950 6150
Connection ~ 15950 5650
Connection ~ 15950 6450
Connection ~ 15950 6150
Wire Wire Line
	15850 5850 15950 5850
Wire Wire Line
	15950 5850 15950 5950
Wire Wire Line
	15950 6550 15950 6650
$Comp
L power:GNDD #PWR?
U 1 1 5E99F2EE
P 15950 6800
AR Path="/5E890F69/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E99F2EE" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E99F2EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15950 6550 50  0001 C CNN
F 1 "GNDD" H 15954 6645 50  0000 C CNN
F 2 "" H 15950 6800 50  0001 C CNN
F 3 "" H 15950 6800 50  0001 C CNN
	1    15950 6800
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:SUB-D25-my_conn J?
U 1 1 5E99F313
P 15650 6050
AR Path="/5E890F69/5E99F313" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E99F313" Ref="J?"  Part="1" 
AR Path="/5E985745/5E99F313" Ref="J?"  Part="1" 
AR Path="/5E985791/5E99F313" Ref="J?"  Part="1" 
AR Path="/5E9AC39B/5E99F313" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E99F313" Ref="J?"  Part="1" 
AR Path="/5E9B2B8D/5E99F313" Ref="J?"  Part="1" 
F 0 "J?" H 15650 6825 50  0000 C CNN
F 1 "SUB-D25" H 15650 6734 50  0000 C CNN
F 2 "" H 15650 6050 50  0001 C CNN
F 3 "" H 15650 6050 50  0001 C CNN
	1    15650 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	15450 6100 15325 6100
Wire Wire Line
	15325 6400 15325 6500
Wire Wire Line
	15325 6300 15325 6400
Wire Wire Line
	15450 5500 15325 5500
Wire Wire Line
	15325 6200 15325 6300
Wire Wire Line
	15950 6650 15950 6800
Wire Wire Line
	15850 6050 15950 6050
Connection ~ 15950 6050
Wire Wire Line
	15850 5550 15950 5550
Wire Wire Line
	15850 6150 15950 6150
Wire Wire Line
	15950 6350 15950 6450
Wire Wire Line
	15950 6250 15950 6350
Wire Wire Line
	15450 6400 15325 6400
$Comp
L power:GNDD #PWR?
U 1 1 5E99F32E
P 15325 6825
AR Path="/5E890F69/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E99F32E" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5E99F32E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15325 6575 50  0001 C CNN
F 1 "GNDD" H 15329 6670 50  0000 C CNN
F 2 "" H 15325 6825 50  0001 C CNN
F 3 "" H 15325 6825 50  0001 C CNN
	1    15325 6825
	1    0    0    -1  
$EndComp
Connection ~ 15325 6300
Wire Wire Line
	15950 5650 15950 5750
Wire Wire Line
	15450 6300 15325 6300
Connection ~ 15950 5850
Wire Wire Line
	15450 6500 15325 6500
Wire Wire Line
	15325 6000 15325 6100
Wire Wire Line
	15325 6500 15325 6825
Connection ~ 15325 6500
Connection ~ 15950 6350
Wire Wire Line
	15325 6100 15325 6200
Wire Wire Line
	15450 6200 15325 6200
Wire Wire Line
	15950 6150 15950 6250
Connection ~ 15325 6000
Connection ~ 15325 6400
Wire Wire Line
	15450 6000 15325 6000
Wire Wire Line
	14725 5900 15450 5900
Connection ~ 14725 5900
Wire Wire Line
	15450 5800 14850 5800
Connection ~ 14850 5800
Wire Wire Line
	14975 5700 15450 5700
Connection ~ 14975 5700
Wire Wire Line
	15450 5600 15125 5600
Connection ~ 15125 5600
Text Notes 5950 2075 0    50   ~ 0
SIF 40x\nSIF 30x\nSIF 20x\nSIF 10x
Text Notes 5875 3750 0    50   ~ 0
F8xx
Text Notes 4625 1925 0    50   ~ 0
~0xF800
$Comp
L power:GND #PWR?
U 1 1 5EEC4994
P 3325 9825
AR Path="/5E890F69/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC39B/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EEC4994" Ref="#PWR?"  Part="1" 
AR Path="/5E9B2B8D/5EEC4994" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3325 9575 50  0001 C CNN
F 1 "GND" H 3325 9675 50  0000 C CNN
F 2 "" H 3325 9825 50  0001 C CNN
F 3 "" H 3325 9825 50  0001 C CNN
	1    3325 9825
	1    0    0    -1  
$EndComp
Wire Wire Line
	3325 9825 3325 9775
Wire Wire Line
	3325 9775 3325 9675
Connection ~ 3325 9775
Wire Wire Line
	3325 9675 3325 9475
Connection ~ 3325 9675
NoConn ~ 4350 9475
Text Label 4750 9175 2    50   ~ 0
AA1b
Text Label 6325 8700 0    50   ~ 0
AA1b
Text Label 6325 5175 0    50   ~ 0
AA1b
Text Label 4750 9275 2    50   ~ 0
AA0b
Text Label 6325 8600 0    50   ~ 0
AA0b
Text Label 6325 5075 0    50   ~ 0
AA0b
Text Label 6950 2675 0    50   ~ 0
NAND
Text Notes 2650 3275 0    50   ~ 0
USER 1:\n0xf818:      ~Switches Read\n0xf810:     ~CS2_6551\n0xf814:     ~CS1_6551\n\nUSER 2:\n0xf828:      ~Switches Read\n0xf820:     ~CS2_6551\n0xf824:     ~CS1_6551\n\nUSER 3:\n0xf838:      ~Switches Read\n0xf830:     ~CS2_6551\n0xf834:     ~CS1_6551 \n\nUSER 4:\n0xf848:      ~Switches Read\n0xf840:     ~CS2_6551\n0xf844:     ~CS1_6551\n
Text Label 6325 5575 0    50   ~ 0
RD{slash}~WR
Text Label 6300 1325 2    50   ~ 0
RD{slash}~WR
Text Label 6325 9100 0    50   ~ 0
RD{slash}~WR
Text Label 2925 7675 0    50   ~ 0
RD{slash}~WR
Text Label 2925 5800 0    50   ~ 0
RD{slash}~WR
Connection ~ 3300 5800
Wire Wire Line
	3300 5800 2925 5800
Text Notes 1400 5525 2    50   ~ 0
USR SER IRQ\n1    1    2\n      2    5\n2    1    3\n      2    5\n3    1    4\n      2    5\n4    1    2\n      2    5\n
NoConn ~ 1925 3750
Text Notes 9550 1075 0    50   ~ 0
Data is inverted:\nSetting: 5e
Wire Wire Line
	8450 1950 7700 1950
Wire Wire Line
	7700 1850 8350 1850
Wire Wire Line
	8250 1750 7700 1750
Wire Wire Line
	7700 1650 8150 1650
Wire Wire Line
	8050 1550 7700 1550
Wire Wire Line
	7700 1450 7950 1450
Wire Wire Line
	7850 1350 7700 1350
Wire Wire Line
	7750 1250 7700 1250
Text Notes 6950 1975 2    50   ~ 0
Switch Setting:\nOn\nOff\nOff\nOff\n\nOff\nOn\nOff\nOn
$Comp
L Switch:SW_DIP_x08 SW?
U 1 1 5E8D2880
P 7400 1550
AR Path="/5E890F69/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E985316/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E9856B7/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E985745/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E985791/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E9AC39B/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E9AC3F3/5E8D2880" Ref="SW?"  Part="1" 
AR Path="/5E9B2B8D/5E8D2880" Ref="SW?"  Part="1" 
F 0 "SW?" H 7400 2217 50  0000 C CNN
F 1 "SW_DIP_x08" H 7400 2126 50  0000 C CNN
F 2 "" H 7400 1550 50  0001 C CNN
F 3 "~" H 7400 1550 50  0001 C CNN
	1    7400 1550
	-1   0    0    1   
$EndComp
$EndSCHEMATC
