EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 14 16
Title "es65 Printer Board"
Date "2020-03-31"
Rev "v0.1"
Comp "Offner Robert"
Comment1 "v0.1: Initial"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1050 1950 1350 1950
Wire Wire Line
	1050 3450 1350 3450
Wire Wire Line
	1050 2150 1350 2150
Wire Wire Line
	1050 2450 1350 2450
Wire Wire Line
	1050 3850 1350 3850
NoConn ~ 1050 1050
NoConn ~ 1050 3950
Wire Wire Line
	1050 2350 1350 2350
Wire Wire Line
	1925 2750 2225 2750
Text Label 1350 3750 2    50   ~ 0
AD6
Text Label 1350 3850 2    50   ~ 0
AD7
Text Label 1350 3650 2    50   ~ 0
AD5
Text Label 1350 3550 2    50   ~ 0
AD4
Text Label 1350 3450 2    50   ~ 0
AD3
Text Label 1350 3350 2    50   ~ 0
AD2
Text Label 1350 3250 2    50   ~ 0
AD1
Text Label 1350 3150 2    50   ~ 0
AD0
Text Label 1350 2650 2    50   ~ 0
AA15
Text Label 1350 2150 2    50   ~ 0
AA10
Text Label 1350 2350 2    50   ~ 0
AA12
Text Label 1350 2250 2    50   ~ 0
AA11
Text Label 1350 2550 2    50   ~ 0
AA14
Text Label 1350 2450 2    50   ~ 0
AA13
Text Label 1350 1950 2    50   ~ 0
AA8
Text Label 1350 1550 2    50   ~ 0
AA4
Text Label 1350 2050 2    50   ~ 0
AA9
Text Label 1350 1650 2    50   ~ 0
AA5
Text Label 1350 1850 2    50   ~ 0
AA7
Text Label 1350 1750 2    50   ~ 0
AA6
Text Label 2225 2750 2    50   ~ 0
PH2b
Text Label 1350 1250 2    50   ~ 0
AA1
Text Label 1350 1350 2    50   ~ 0
AA2
Text Label 1350 1450 2    50   ~ 0
AA3
Text Label 1350 1150 2    50   ~ 0
AA0
NoConn ~ 1050 2750
Wire Wire Line
	1050 2050 1350 2050
Wire Wire Line
	1050 1450 1350 1450
Wire Wire Line
	1050 950  1350 950 
Wire Wire Line
	1050 3150 1350 3150
Wire Wire Line
	1100 4050 1100 4100
Wire Wire Line
	1050 1250 1350 1250
Wire Wire Line
	2225 950  2225 825 
NoConn ~ 1050 2850
Wire Wire Line
	1050 4050 1100 4050
Wire Wire Line
	1350 950  1350 825 
Wire Wire Line
	1050 3250 1350 3250
Wire Wire Line
	1050 1550 1350 1550
Wire Wire Line
	1050 1350 1350 1350
Wire Wire Line
	1050 1150 1350 1150
Wire Wire Line
	1925 4050 1975 4050
Wire Wire Line
	1975 4050 1975 4100
Wire Wire Line
	1050 2550 1350 2550
Wire Wire Line
	1050 3550 1350 3550
Wire Wire Line
	1925 950  2225 950 
Wire Wire Line
	1050 3350 1350 3350
Wire Wire Line
	1050 1850 1350 1850
Wire Wire Line
	1050 2650 1350 2650
Wire Wire Line
	1050 2250 1350 2250
Wire Wire Line
	1050 1650 1350 1650
Wire Wire Line
	1050 1750 1350 1750
Wire Wire Line
	1050 3650 1350 3650
Wire Wire Line
	1050 3750 1350 3750
$Comp
L power:GND #PWR?
U 1 1 5EAA2349
P 1975 4100
AR Path="/5E84A03F/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAA2349" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAA2349" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1975 3850 50  0001 C CNN
F 1 "GND" H 1975 3950 50  0000 C CNN
F 2 "" H 1975 4100 50  0001 C CNN
F 3 "" H 1975 4100 50  0001 C CNN
	1    1975 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9B305E
P 2225 825
AR Path="/5E84A03F/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B305E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B305E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2225 675 50  0001 C CNN
F 1 "+5V" H 2240 998 50  0000 C CNN
F 2 "" H 2225 825 50  0001 C CNN
F 3 "" H 2225 825 50  0001 C CNN
	1    2225 825 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E9B3065
P 1350 825
AR Path="/5E84A03F/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3065" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3065" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1350 675 50  0001 C CNN
F 1 "+5V" H 1365 998 50  0000 C CNN
F 2 "" H 1350 825 50  0001 C CNN
F 3 "" H 1350 825 50  0001 C CNN
	1    1350 825 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ED165B7
P 1100 4100
AR Path="/5E84A03F/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5EAB9D7B/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E83BCE9/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5EA2DD9E/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5EA9FEC9/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5EB2C18C/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E890F69/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ED165B7" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ED165B7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1100 3850 50  0001 C CNN
F 1 "GND" H 1100 3950 50  0000 C CNN
F 2 "" H 1100 4100 50  0001 C CNN
F 3 "" H 1100 4100 50  0001 C CNN
	1    1100 4100
	1    0    0    -1  
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 1 1 5E8E37C6
P 850 2500
AR Path="/5E84A03F/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5EAB9D7B/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E83BCE9/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5EA2DD9E/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5EA9FEC9/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5EB2C18C/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E890F69/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E985316/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E9856B7/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E985745/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E985791/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E9AC3F3/5E8E37C6" Ref="P?"  Part="1" 
AR Path="/5E8E37C6" Ref="P?"  Part="1" 
F 0 "P?" H 767 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 767 816 50  0000 C CNN
F 2 "" H 850 2500 50  0000 C CNN
F 3 "" H 850 2500 50  0000 C CNN
	1    850  2500
	-1   0    0    1   
$EndComp
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 3 1 5EAA235D
P 1725 2500
AR Path="/5E84A03F/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5EAB9D7B/5EAA235D" Ref="P?"  Part="2" 
AR Path="/5E83BCE9/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5EA2DD9E/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5EA9FEC9/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5EB2C18C/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5E890F69/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5E985316/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5E9856B7/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5E985745/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5E985791/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5E9AC3F3/5EAA235D" Ref="P?"  Part="3" 
AR Path="/5EAA235D" Ref="P?"  Part="3" 
F 0 "P?" H 1642 725 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 1642 816 50  0000 C CNN
F 2 "" H 1725 2500 50  0000 C CNN
F 3 "" H 1725 2500 50  0000 C CNN
	3    1725 2500
	-1   0    0    1   
$EndComp
Text HLabel 4350 3275 0    50   Input ~ 0
R{slash}Wb
Wire Wire Line
	4350 3275 4650 3275
Wire Wire Line
	4350 3175 4650 3175
Text Label 4650 3175 2    50   ~ 0
PH2b
Text HLabel 4350 3175 0    50   Input ~ 0
PH2b
Text Label 4625 2875 2    50   ~ 0
AD6
Text HLabel 4325 875  0    50   Input ~ 0
AA2
Text Label 4625 1975 2    50   ~ 0
AA13
Text Label 4625 2575 2    50   ~ 0
AD3
Text Label 4625 675  2    50   ~ 0
AA0
Text Label 4625 2075 2    50   ~ 0
AA14
Text Label 4625 2675 2    50   ~ 0
AD4
Text Label 4625 1375 2    50   ~ 0
AA7
Text Label 4625 975  2    50   ~ 0
AA3
Text Label 4625 875  2    50   ~ 0
AA2
Text Label 4625 775  2    50   ~ 0
AA1
Text Label 4625 1075 2    50   ~ 0
AA4
Wire Wire Line
	4325 1375 4625 1375
Wire Wire Line
	4325 1575 4625 1575
Wire Wire Line
	4325 2075 4625 2075
Wire Wire Line
	4325 1875 4625 1875
Wire Wire Line
	4325 2175 4625 2175
Text Label 4625 1475 2    50   ~ 0
AA8
Text Label 4625 2775 2    50   ~ 0
AD5
Text Label 4625 2175 2    50   ~ 0
AA15
Wire Wire Line
	4325 775  4625 775 
Wire Wire Line
	4325 675  4625 675 
Wire Wire Line
	4325 1675 4625 1675
Wire Wire Line
	4325 1775 4625 1775
Text Label 4625 1875 2    50   ~ 0
AA12
Wire Wire Line
	4325 1975 4625 1975
Wire Wire Line
	4325 1075 4625 1075
Wire Wire Line
	4325 1475 4625 1475
Text HLabel 4325 675  0    50   Input ~ 0
AA0
Text Label 4625 2275 2    50   ~ 0
AD0
Text Label 4625 1775 2    50   ~ 0
AA11
Text Label 4625 2375 2    50   ~ 0
AD1
Wire Wire Line
	4325 1175 4625 1175
Wire Wire Line
	4325 975  4625 975 
Text Label 4625 1675 2    50   ~ 0
AA10
Wire Wire Line
	4325 2875 4625 2875
Wire Wire Line
	4325 2975 4625 2975
Text HLabel 4325 775  0    50   Input ~ 0
AA1
Wire Wire Line
	4325 2275 4625 2275
Text Label 4625 1175 2    50   ~ 0
AA5
Wire Wire Line
	4325 2675 4625 2675
Wire Wire Line
	4325 875  4625 875 
Wire Wire Line
	4325 2575 4625 2575
Wire Wire Line
	4325 2775 4625 2775
Wire Wire Line
	4325 2375 4625 2375
Wire Wire Line
	4325 2475 4625 2475
Wire Wire Line
	4325 1275 4625 1275
Text HLabel 4325 1075 0    50   Input ~ 0
AA4
Text HLabel 4325 1175 0    50   Input ~ 0
AA5
Text HLabel 4325 1275 0    50   Input ~ 0
AA6
Text HLabel 4325 2275 0    50   Input ~ 0
AD0
Text HLabel 4325 1375 0    50   Input ~ 0
AA7
Text HLabel 4325 1775 0    50   Input ~ 0
AA11
Text HLabel 4325 1675 0    50   Input ~ 0
AA10
Text HLabel 4325 1875 0    50   Input ~ 0
AA12
Text HLabel 4325 2075 0    50   Input ~ 0
AA14
Text HLabel 4325 2375 0    50   Input ~ 0
AD1
Text HLabel 4325 2175 0    50   Input ~ 0
AA15
Text HLabel 4325 1475 0    50   Input ~ 0
AA8
Text HLabel 4325 2875 0    50   Input ~ 0
AD6
Text HLabel 4325 2975 0    50   Input ~ 0
AD7
Text HLabel 4325 2775 0    50   Input ~ 0
AD5
Text Label 4625 2475 2    50   ~ 0
AD2
Text Label 4625 1575 2    50   ~ 0
AA9
Text HLabel 4325 2675 0    50   Input ~ 0
AD4
Text Label 4625 1275 2    50   ~ 0
AA6
Text Label 4625 2975 2    50   ~ 0
AD7
Text HLabel 4325 1975 0    50   Input ~ 0
AA13
Text HLabel 4325 2575 0    50   Input ~ 0
AD3
Text HLabel 4325 1575 0    50   Input ~ 0
AA9
Text HLabel 4325 975  0    50   Input ~ 0
AA3
Text HLabel 4325 2475 0    50   Input ~ 0
AD2
Wire Wire Line
	1925 2950 2225 2950
Text Label 2225 2950 2    50   ~ 0
R{slash}Wb
NoConn ~ 1050 2950
NoConn ~ 1925 1050
NoConn ~ 1925 1150
NoConn ~ 1925 1250
NoConn ~ 1925 1350
NoConn ~ 1925 2250
NoConn ~ 1925 2650
Wire Wire Line
	1925 1450 2200 1450
Wire Wire Line
	1925 1550 2200 1550
Wire Wire Line
	1925 1650 2200 1650
Wire Wire Line
	1925 1750 2200 1750
Wire Wire Line
	1925 1850 2200 1850
Wire Wire Line
	1925 1950 2200 1950
Wire Wire Line
	1925 2050 2200 2050
Wire Wire Line
	1925 2150 2200 2150
NoConn ~ 1925 3050
NoConn ~ 1925 3250
NoConn ~ 1925 3350
NoConn ~ 1925 3450
NoConn ~ 1925 3550
Wire Wire Line
	4375 3575 4675 3575
Text Label 4675 3575 2    50   ~ 0
~RSTb
Text HLabel 4375 3575 0    50   Input ~ 0
~RSTb
$Comp
L power:+5V #PWR?
U 1 1 5E9B3061
P 1050 7150
AR Path="/5E890F69/5E9B3061" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3061" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3061" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3061" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3061" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3061" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1050 7000 50  0001 C CNN
F 1 "+5V" H 1050 7300 50  0000 C CNN
F 2 "" H 1050 7150 50  0001 C CNN
F 3 "" H 1050 7150 50  0001 C CNN
	1    1050 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 7175 1050 7400
Wire Wire Line
	1225 7175 1225 7200
$Comp
L Device:C_Small C?
U 1 1 5E9B3062
P 1225 7300
AR Path="/5E890F69/5E9B3062" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B3062" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B3062" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B3062" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B3062" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3062" Ref="C?"  Part="1" 
F 0 "C?" H 1317 7346 50  0000 L CNN
F 1 "22n" H 1317 7255 50  0000 L CNN
F 2 "" H 1225 7300 50  0001 C CNN
F 3 "~" H 1225 7300 50  0001 C CNN
	1    1225 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 7150 1050 7175
Wire Wire Line
	1050 7175 1225 7175
Connection ~ 1050 7175
$Comp
L power:GND #PWR?
U 1 1 5EBD6BC3
P 1225 7400
AR Path="/5E890F69/5EBD6BC3" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EBD6BC3" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EBD6BC3" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EBD6BC3" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EBD6BC3" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EBD6BC3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1225 7150 50  0001 C CNN
F 1 "GND" H 1225 7250 50  0000 C CNN
F 2 "" H 1225 7400 50  0001 C CNN
F 3 "" H 1225 7400 50  0001 C CNN
	1    1225 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9B3064
P 1050 8400
AR Path="/5E890F69/5E9B3064" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3064" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3064" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3064" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3064" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3064" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1050 8150 50  0001 C CNN
F 1 "GND" H 1050 8250 50  0000 C CNN
F 2 "" H 1050 8400 50  0001 C CNN
F 3 "" H 1050 8400 50  0001 C CNN
	1    1050 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1925 3950 2225 3950
Text Label 2225 3950 2    50   ~ 0
~RSTb
$Comp
L power:+5V #PWR?
U 1 1 5E9B303A
P 1800 7150
AR Path="/5E890F69/5E9B303A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B303A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B303A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B303A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B303A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B303A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1800 7000 50  0001 C CNN
F 1 "+5V" H 1800 7300 50  0000 C CNN
F 2 "" H 1800 7150 50  0001 C CNN
F 3 "" H 1800 7150 50  0001 C CNN
	1    1800 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 7175 1800 7400
Wire Wire Line
	1975 7175 1975 7200
Wire Wire Line
	1800 7150 1800 7175
$Comp
L Device:C_Small C?
U 1 1 5E87BA48
P 1975 7300
AR Path="/5E890F69/5E87BA48" Ref="C?"  Part="1" 
AR Path="/5E985316/5E87BA48" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E87BA48" Ref="C?"  Part="1" 
AR Path="/5E985745/5E87BA48" Ref="C?"  Part="1" 
AR Path="/5E985791/5E87BA48" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E87BA48" Ref="C?"  Part="1" 
F 0 "C?" H 2067 7346 50  0000 L CNN
F 1 "22n" H 2067 7255 50  0000 L CNN
F 2 "" H 1975 7300 50  0001 C CNN
F 3 "~" H 1975 7300 50  0001 C CNN
	1    1975 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 7175 1975 7175
Connection ~ 1800 7175
$Comp
L power:GND #PWR?
U 1 1 5E87BA57
P 1975 7400
AR Path="/5E890F69/5E87BA57" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E87BA57" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E87BA57" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E87BA57" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E87BA57" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E87BA57" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1975 7150 50  0001 C CNN
F 1 "GND" H 1975 7250 50  0000 C CNN
F 2 "" H 1975 7400 50  0001 C CNN
F 3 "" H 1975 7400 50  0001 C CNN
	1    1975 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E87BA64
P 1800 8400
AR Path="/5E890F69/5E87BA64" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E87BA64" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E87BA64" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E87BA64" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E87BA64" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E87BA64" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1800 8150 50  0001 C CNN
F 1 "GND" H 1800 8250 50  0000 C CNN
F 2 "" H 1800 8400 50  0001 C CNN
F 3 "" H 1800 8400 50  0001 C CNN
	1    1800 8400
	1    0    0    -1  
$EndComp
NoConn ~ 1925 3650
NoConn ~ 1925 3850
$Comp
L power:+12V #PWR?
U 1 1 5EEA2642
P 2275 2275
AR Path="/5E890F69/5EEA2642" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EEA2642" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EEA2642" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EEA2642" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EEA2642" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EEA2642" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2275 2125 50  0001 C CNN
F 1 "+12V" H 2290 2448 50  0000 C CNN
F 2 "" H 2275 2275 50  0001 C CNN
F 3 "" H 2275 2275 50  0001 C CNN
	1    2275 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 2275 2275 2550
Wire Wire Line
	1925 2550 2275 2550
Wire Wire Line
	2350 2450 2350 2500
Wire Wire Line
	1925 2450 2350 2450
$Comp
L power:GNDA #PWR?
U 1 1 5EEA2FA3
P 2350 2500
AR Path="/5E890F69/5EEA2FA3" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EEA2FA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EEA2FA3" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EEA2FA3" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EEA2FA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EEA2FA3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2350 2250 50  0001 C CNN
F 1 "GNDA" H 2355 2327 50  0000 C CNN
F 2 "" H 2350 2500 50  0001 C CNN
F 3 "" H 2350 2500 50  0001 C CNN
	1    2350 2500
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5E9B3069
P 2500 2600
AR Path="/5E890F69/5E9B3069" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3069" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3069" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3069" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3069" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3069" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2500 2700 50  0001 C CNN
F 1 "-12V" H 2515 2773 50  0000 C CNN
F 2 "" H 2500 2600 50  0001 C CNN
F 3 "" H 2500 2600 50  0001 C CNN
	1    2500 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 2600 2500 2350
Wire Wire Line
	1925 2350 2500 2350
NoConn ~ 1925 3150
$Comp
L es65-rescue:DS8T28N-my_ics U3
U 1 1 5E9B303E
P 3850 5500
AR Path="/5E890F69/5E9B303E" Ref="U3"  Part="1" 
AR Path="/5E985316/5E9B303E" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9B303E" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9B303E" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9B303E" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9B303E" Ref="U5"  Part="1" 
F 0 "U5" H 3500 6025 50  0000 C CNN
F 1 "DS8T28N" H 3625 5950 50  0000 C CNN
F 2 "" H 3850 5500 50  0001 C CNN
F 3 "" H 3850 5500 50  0001 C CNN
	1    3850 5500
	1    0    0    -1  
$EndComp
Text Label 3050 7375 0    50   ~ 0
AD0
Wire Wire Line
	3350 5300 3050 5300
Text Label 3050 7175 0    50   ~ 0
AD2
Text Label 3050 7075 0    50   ~ 0
AD3
Text Label 3050 5500 0    50   ~ 0
AD4
Text Label 3050 5400 0    50   ~ 0
AD5
Text Label 3050 5200 0    50   ~ 0
AD7
Text Label 3050 5300 0    50   ~ 0
AD6
Wire Wire Line
	3350 5500 3050 5500
Wire Wire Line
	3350 5400 3050 5400
Text Label 3050 7275 0    50   ~ 0
AD1
Wire Wire Line
	3350 5200 3050 5200
Wire Wire Line
	3850 4675 3850 4900
$Comp
L power:+5V #PWR?
U 1 1 5E9B303F
P 3850 4650
AR Path="/5E890F69/5E9B303F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B303F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B303F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B303F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B303F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B303F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 4500 50  0001 C CNN
F 1 "+5V" H 3850 4800 50  0000 C CNN
F 2 "" H 3850 4650 50  0001 C CNN
F 3 "" H 3850 4650 50  0001 C CNN
	1    3850 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9B3040
P 4025 4800
AR Path="/5E890F69/5E9B3040" Ref="C?"  Part="1" 
AR Path="/5E985316/5E9B3040" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E9B3040" Ref="C?"  Part="1" 
AR Path="/5E985745/5E9B3040" Ref="C?"  Part="1" 
AR Path="/5E985791/5E9B3040" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3040" Ref="C?"  Part="1" 
F 0 "C?" H 4117 4846 50  0000 L CNN
F 1 "22n" H 4117 4755 50  0000 L CNN
F 2 "" H 4025 4800 50  0001 C CNN
F 3 "~" H 4025 4800 50  0001 C CNN
	1    4025 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4025 4675 4025 4700
Connection ~ 3850 4675
$Comp
L power:GND #PWR?
U 1 1 5E9B3041
P 4025 4900
AR Path="/5E890F69/5E9B3041" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3041" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3041" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3041" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3041" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3041" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4025 4650 50  0001 C CNN
F 1 "GND" H 4025 4750 50  0000 C CNN
F 2 "" H 4025 4900 50  0001 C CNN
F 3 "" H 4025 4900 50  0001 C CNN
	1    4025 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4650 3850 4675
Wire Wire Line
	3850 4675 4025 4675
$Comp
L power:GND #PWR?
U 1 1 5E9B3042
P 3850 6150
AR Path="/5E890F69/5E9B3042" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3042" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3042" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3042" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3042" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3042" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 5900 50  0001 C CNN
F 1 "GND" H 3850 6000 50  0000 C CNN
F 2 "" H 3850 6150 50  0001 C CNN
F 3 "" H 3850 6150 50  0001 C CNN
	1    3850 6150
	1    0    0    -1  
$EndComp
Connection ~ 3850 6550
Wire Wire Line
	3850 6550 4025 6550
Wire Wire Line
	3350 7275 3050 7275
Wire Wire Line
	3850 6525 3850 6550
$Comp
L es65-rescue:DS8T28N-my_ics U6
U 1 1 5E8B56A1
P 3850 7375
AR Path="/5E890F69/5E8B56A1" Ref="U6"  Part="1" 
AR Path="/5E985316/5E8B56A1" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E8B56A1" Ref="U?"  Part="1" 
AR Path="/5E985745/5E8B56A1" Ref="U?"  Part="1" 
AR Path="/5E985791/5E8B56A1" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E8B56A1" Ref="U4"  Part="1" 
F 0 "U4" H 3500 7900 50  0000 C CNN
F 1 "DS8T28N" H 3625 7825 50  0000 C CNN
F 2 "" H 3850 7375 50  0001 C CNN
F 3 "" H 3850 7375 50  0001 C CNN
	1    3850 7375
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E8B56AE
P 3850 6525
AR Path="/5E890F69/5E8B56AE" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8B56AE" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8B56AE" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8B56AE" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8B56AE" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8B56AE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 6375 50  0001 C CNN
F 1 "+5V" H 3850 6675 50  0000 C CNN
F 2 "" H 3850 6525 50  0001 C CNN
F 3 "" H 3850 6525 50  0001 C CNN
	1    3850 6525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4025 6550 4025 6575
Wire Wire Line
	3350 7075 3050 7075
$Comp
L power:GND #PWR?
U 1 1 5E9B3045
P 4025 6775
AR Path="/5E890F69/5E9B3045" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3045" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3045" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3045" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3045" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3045" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4025 6525 50  0001 C CNN
F 1 "GND" H 4025 6625 50  0000 C CNN
F 2 "" H 4025 6775 50  0001 C CNN
F 3 "" H 4025 6775 50  0001 C CNN
	1    4025 6775
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E8B56CC
P 4025 6675
AR Path="/5E890F69/5E8B56CC" Ref="C?"  Part="1" 
AR Path="/5E985316/5E8B56CC" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E8B56CC" Ref="C?"  Part="1" 
AR Path="/5E985745/5E8B56CC" Ref="C?"  Part="1" 
AR Path="/5E985791/5E8B56CC" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E8B56CC" Ref="C?"  Part="1" 
F 0 "C?" H 4117 6721 50  0000 L CNN
F 1 "22n" H 4117 6630 50  0000 L CNN
F 2 "" H 4025 6675 50  0001 C CNN
F 3 "~" H 4025 6675 50  0001 C CNN
	1    4025 6675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E8B56DA
P 3850 8025
AR Path="/5E890F69/5E8B56DA" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E8B56DA" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E8B56DA" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E8B56DA" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E8B56DA" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E8B56DA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 7775 50  0001 C CNN
F 1 "GND" H 3850 7875 50  0000 C CNN
F 2 "" H 3850 8025 50  0001 C CNN
F 3 "" H 3850 8025 50  0001 C CNN
	1    3850 8025
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 6550 3850 6775
Wire Wire Line
	3350 7375 3050 7375
Wire Wire Line
	3350 7175 3050 7175
Wire Wire Line
	3350 5700 3300 5700
Wire Wire Line
	3300 5700 3300 5800
Wire Wire Line
	3300 5800 3350 5800
Wire Wire Line
	3300 5800 2925 5800
Connection ~ 3300 5800
Text Label 2925 5800 0    50   ~ 0
~CE_DATA
Wire Wire Line
	3300 7575 3300 7675
Connection ~ 3300 7675
Wire Wire Line
	3300 7675 2925 7675
Text Label 2925 7675 0    50   ~ 0
~CE_DATA
Wire Wire Line
	3300 7675 3350 7675
Wire Wire Line
	3350 7575 3300 7575
Wire Wire Line
	4350 5200 4400 5200
Wire Wire Line
	4400 5200 4400 5600
Wire Wire Line
	4400 5600 4350 5600
Wire Wire Line
	4350 5300 4450 5300
Wire Wire Line
	4450 5300 4450 5700
Wire Wire Line
	4450 5700 4350 5700
Wire Wire Line
	4350 5400 4500 5400
Wire Wire Line
	4500 5400 4500 5800
Wire Wire Line
	4500 5800 4350 5800
Wire Wire Line
	4550 5500 4550 5900
Wire Wire Line
	4550 5900 4350 5900
Wire Wire Line
	4550 5900 4850 5900
Connection ~ 4550 5900
Wire Wire Line
	4500 5800 4850 5800
Connection ~ 4500 5800
Wire Wire Line
	4450 5700 4850 5700
Connection ~ 4450 5700
Wire Wire Line
	4350 5500 4550 5500
Wire Wire Line
	4400 5600 4850 5600
Connection ~ 4400 5600
Wire Wire Line
	4400 7075 4400 7475
Connection ~ 4400 7475
Wire Wire Line
	4450 7575 4350 7575
Wire Wire Line
	4500 7675 4350 7675
Wire Wire Line
	4450 7575 4850 7575
Wire Wire Line
	4550 7775 4350 7775
Connection ~ 4450 7575
Wire Wire Line
	4400 7475 4350 7475
Wire Wire Line
	4400 7475 4850 7475
Wire Wire Line
	4350 7075 4400 7075
Wire Wire Line
	4350 7375 4550 7375
Wire Wire Line
	4350 7275 4500 7275
Wire Wire Line
	4550 7375 4550 7775
Wire Wire Line
	4350 7175 4450 7175
Connection ~ 4550 7775
Wire Wire Line
	4450 7175 4450 7575
Wire Wire Line
	4550 7775 4850 7775
Wire Wire Line
	4500 7675 4850 7675
Wire Wire Line
	4500 7275 4500 7675
Connection ~ 4500 7675
Text Label 4850 7775 2    50   ~ 0
D0
Text Label 4850 7675 2    50   ~ 0
D1
Text Label 4850 7575 2    50   ~ 0
D2
Text Label 4850 7475 2    50   ~ 0
D3
Text Label 4850 5900 2    50   ~ 0
D4
Text Label 4850 5800 2    50   ~ 0
D5
Text Label 4850 5700 2    50   ~ 0
D6
Text Label 4850 5600 2    50   ~ 0
D7
Connection ~ 7450 3700
$Comp
L power:GND #PWR?
U 1 1 5E93A6B1
P 7625 3925
AR Path="/5E890F69/5E93A6B1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E93A6B1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E93A6B1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E93A6B1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E93A6B1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E93A6B1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7625 3675 50  0001 C CNN
F 1 "GND" H 7625 3775 50  0000 C CNN
F 2 "" H 7625 3925 50  0001 C CNN
F 3 "" H 7625 3925 50  0001 C CNN
	1    7625 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3700 7625 3700
Wire Wire Line
	7625 3700 7625 3725
$Comp
L power:+5V #PWR?
U 1 1 5E93A6C0
P 7450 3675
AR Path="/5E890F69/5E93A6C0" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E93A6C0" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E93A6C0" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E93A6C0" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E93A6C0" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E93A6C0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 3525 50  0001 C CNN
F 1 "+5V" H 7450 3825 50  0000 C CNN
F 2 "" H 7450 3675 50  0001 C CNN
F 3 "" H 7450 3675 50  0001 C CNN
	1    7450 3675
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E93A6CE
P 7625 3825
AR Path="/5E890F69/5E93A6CE" Ref="C?"  Part="1" 
AR Path="/5E985316/5E93A6CE" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E93A6CE" Ref="C?"  Part="1" 
AR Path="/5E985745/5E93A6CE" Ref="C?"  Part="1" 
AR Path="/5E985791/5E93A6CE" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E93A6CE" Ref="C?"  Part="1" 
F 0 "C?" H 7717 3871 50  0000 L CNN
F 1 "22n" H 7717 3780 50  0000 L CNN
F 2 "" H 7625 3825 50  0001 C CNN
F 3 "~" H 7625 3825 50  0001 C CNN
	1    7625 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3675 7450 3700
Wire Wire Line
	7450 3700 7450 3925
$Comp
L es65-rescue:6301-my_ics U1
U 1 1 5E95D3D8
P 5600 1625
AR Path="/5E890F69/5E95D3D8" Ref="U1"  Part="1" 
AR Path="/5E985316/5E95D3D8" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E95D3D8" Ref="U?"  Part="1" 
AR Path="/5E985745/5E95D3D8" Ref="U?"  Part="1" 
AR Path="/5E985791/5E95D3D8" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E95D3D8" Ref="U1"  Part="1" 
F 0 "U1" H 5315 2155 50  0000 C CNN
F 1 "6301" H 5365 2080 50  0000 C CNN
F 2 "" H 5300 1725 50  0001 C CNN
F 3 "" H 5300 1725 50  0001 C CNN
	1    5600 1625
	1    0    0    -1  
$EndComp
Connection ~ 5600 800 
$Comp
L power:GND #PWR?
U 1 1 5E9B304E
P 5775 1025
AR Path="/5E890F69/5E9B304E" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B304E" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B304E" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B304E" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B304E" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B304E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5775 775 50  0001 C CNN
F 1 "GND" H 5775 875 50  0000 C CNN
F 2 "" H 5775 1025 50  0001 C CNN
F 3 "" H 5775 1025 50  0001 C CNN
	1    5775 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 800  5775 800 
Wire Wire Line
	5775 800  5775 825 
$Comp
L power:+5V #PWR?
U 1 1 5E9B304F
P 5600 775
AR Path="/5E890F69/5E9B304F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B304F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B304F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B304F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B304F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B304F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 625 50  0001 C CNN
F 1 "+5V" H 5600 925 50  0000 C CNN
F 2 "" H 5600 775 50  0001 C CNN
F 3 "" H 5600 775 50  0001 C CNN
	1    5600 775 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E95F483
P 5775 925
AR Path="/5E890F69/5E95F483" Ref="C?"  Part="1" 
AR Path="/5E985316/5E95F483" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E95F483" Ref="C?"  Part="1" 
AR Path="/5E985745/5E95F483" Ref="C?"  Part="1" 
AR Path="/5E985791/5E95F483" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E95F483" Ref="C?"  Part="1" 
F 0 "C?" H 5867 971 50  0000 L CNN
F 1 "22n" H 5867 880 50  0000 L CNN
F 2 "" H 5775 925 50  0001 C CNN
F 3 "~" H 5775 925 50  0001 C CNN
	1    5775 925 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 775  5600 800 
Wire Wire Line
	5600 800  5600 1125
$Comp
L power:GND #PWR?
U 1 1 5E9B3051
P 5600 2425
AR Path="/5E890F69/5E9B3051" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3051" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3051" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3051" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3051" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3051" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 2175 50  0001 C CNN
F 1 "GND" H 5600 2275 50  0000 C CNN
F 2 "" H 5600 2425 50  0001 C CNN
F 3 "" H 5600 2425 50  0001 C CNN
	1    5600 2425
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9B3052
P 5150 2275
AR Path="/5E890F69/5E9B3052" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3052" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3052" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3052" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3052" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3052" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5150 2025 50  0001 C CNN
F 1 "GND" H 5150 2125 50  0000 C CNN
F 2 "" H 5150 2275 50  0001 C CNN
F 3 "" H 5150 2275 50  0001 C CNN
	1    5150 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2275 5150 2225
Wire Wire Line
	5150 2125 5200 2125
Wire Wire Line
	5200 2225 5150 2225
Connection ~ 5150 2225
Wire Wire Line
	5150 2225 5150 2125
Wire Wire Line
	5200 1525 4925 1525
Wire Wire Line
	5200 1625 4925 1625
Wire Wire Line
	5200 1725 4925 1725
Wire Wire Line
	5200 1825 4925 1825
Wire Wire Line
	5200 2025 4925 2025
$Comp
L Device:C_Small C?
U 1 1 5E964172
P 5775 2950
AR Path="/5E890F69/5E964172" Ref="C?"  Part="1" 
AR Path="/5E985316/5E964172" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5E964172" Ref="C?"  Part="1" 
AR Path="/5E985745/5E964172" Ref="C?"  Part="1" 
AR Path="/5E985791/5E964172" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5E964172" Ref="C?"  Part="1" 
F 0 "C?" H 5867 2996 50  0000 L CNN
F 1 "22n" H 5867 2905 50  0000 L CNN
F 2 "" H 5775 2950 50  0001 C CNN
F 3 "~" H 5775 2950 50  0001 C CNN
	1    5775 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3850 4925 3850
$Comp
L power:+5V #PWR?
U 1 1 5E964180
P 5600 2800
AR Path="/5E890F69/5E964180" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E964180" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E964180" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E964180" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E964180" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E964180" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 2650 50  0001 C CNN
F 1 "+5V" H 5600 2950 50  0000 C CNN
F 2 "" H 5600 2800 50  0001 C CNN
F 3 "" H 5600 2800 50  0001 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2825 5600 3150
Wire Wire Line
	5600 2825 5775 2825
$Comp
L power:GND #PWR?
U 1 1 5E96418F
P 5600 4450
AR Path="/5E890F69/5E96418F" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E96418F" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E96418F" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E96418F" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E96418F" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E96418F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 4200 50  0001 C CNN
F 1 "GND" H 5600 4300 50  0000 C CNN
F 2 "" H 5600 4450 50  0001 C CNN
F 3 "" H 5600 4450 50  0001 C CNN
	1    5600 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 4925 3550
Connection ~ 5150 4250
Wire Wire Line
	5150 4250 5150 4150
Wire Wire Line
	5600 2800 5600 2825
Wire Wire Line
	5200 3950 4925 3950
Wire Wire Line
	5200 3450 4925 3450
$Comp
L es65-rescue:6301-my_ics U2
U 1 1 5E9641B1
P 5600 3650
AR Path="/5E890F69/5E9641B1" Ref="U2"  Part="1" 
AR Path="/5E985316/5E9641B1" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9641B1" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9641B1" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9641B1" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9641B1" Ref="U?"  Part="1" 
F 0 "U?" H 5315 4180 50  0000 C CNN
F 1 "6301" H 5365 4105 50  0000 C CNN
F 2 "" H 5300 3750 50  0001 C CNN
F 3 "" H 5300 3750 50  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3650 4925 3650
$Comp
L power:GND #PWR?
U 1 1 5E9B3057
P 5150 4300
AR Path="/5E890F69/5E9B3057" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3057" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3057" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3057" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3057" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3057" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5150 4050 50  0001 C CNN
F 1 "GND" H 5150 4150 50  0000 C CNN
F 2 "" H 5150 4300 50  0001 C CNN
F 3 "" H 5150 4300 50  0001 C CNN
	1    5150 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4250 5150 4250
$Comp
L power:GND #PWR?
U 1 1 5E9B3058
P 5775 3050
AR Path="/5E890F69/5E9B3058" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3058" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3058" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3058" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3058" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3058" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5775 2800 50  0001 C CNN
F 1 "GND" H 5775 2900 50  0000 C CNN
F 2 "" H 5775 3050 50  0001 C CNN
F 3 "" H 5775 3050 50  0001 C CNN
	1    5775 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4300 5150 4250
Wire Wire Line
	5150 4150 5200 4150
Wire Wire Line
	5200 3350 4925 3350
Wire Wire Line
	5200 3750 4925 3750
Wire Wire Line
	5950 3350 6225 3350
Wire Wire Line
	5775 2825 5775 2850
Connection ~ 5600 2825
Wire Wire Line
	5200 4050 4925 4050
Text Label 4925 1525 0    50   ~ 0
AA4
Text Label 4925 3550 0    50   ~ 0
AA10
Text Label 4925 1825 0    50   ~ 0
AA7
Text Label 4925 3650 0    50   ~ 0
AA11
Text Label 4925 3750 0    50   ~ 0
AA12
Text Label 4925 3450 0    50   ~ 0
AA9
Text Label 4925 1625 0    50   ~ 0
AA5
Text Label 4925 3850 0    50   ~ 0
AA13
Text Label 4925 3950 0    50   ~ 0
AA14
Text Label 4925 1725 0    50   ~ 0
AA6
Wire Wire Line
	4800 1925 4800 2600
Wire Wire Line
	4800 1925 5200 1925
Wire Wire Line
	6225 2600 6225 3350
Wire Wire Line
	4800 2600 6225 2600
Text Label 4925 3350 0    50   ~ 0
AA8
Text Label 4925 4050 0    50   ~ 0
AA15
Text Label 4925 2025 0    50   ~ 0
R{slash}Wb
Text Label 2475 6625 2    50   ~ 0
~IRQ~_6522
Wire Wire Line
	2475 6625 1975 6625
Wire Wire Line
	1975 5825 1975 5925
Connection ~ 1975 6025
Connection ~ 1975 6125
Connection ~ 1975 5925
Connection ~ 1975 6225
Wire Wire Line
	1975 6025 1975 6125
Wire Wire Line
	1900 5825 1975 5825
Wire Wire Line
	1975 6525 1975 6625
Wire Wire Line
	1975 6325 1975 6425
Wire Wire Line
	1900 6025 1975 6025
$Comp
L Connector_Generic:Conn_01x09 J?
U 1 1 5E9B3059
P 1700 6225
AR Path="/5E890F69/5E9B3059" Ref="J?"  Part="1" 
AR Path="/5E985316/5E9B3059" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E9B3059" Ref="J?"  Part="1" 
AR Path="/5E985745/5E9B3059" Ref="J?"  Part="1" 
AR Path="/5E985791/5E9B3059" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3059" Ref="J?"  Part="1" 
F 0 "J?" H 1618 6842 50  0000 C CNN
F 1 "Conn_01x09" H 1618 6751 50  0000 C CNN
F 2 "" H 1700 6225 50  0001 C CNN
F 3 "~" H 1700 6225 50  0001 C CNN
	1    1700 6225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 6125 1975 6125
Wire Wire Line
	1900 6625 1975 6625
Wire Wire Line
	1975 6125 1975 6225
Wire Wire Line
	1975 5925 1975 6025
Connection ~ 1975 6625
Wire Wire Line
	1900 6525 1975 6525
Connection ~ 1975 6425
Connection ~ 1975 6525
Connection ~ 1975 6325
Wire Wire Line
	1900 6325 1975 6325
Wire Wire Line
	1975 6225 1975 6325
Wire Wire Line
	1900 6425 1975 6425
Wire Wire Line
	1900 5925 1975 5925
Wire Wire Line
	1975 6425 1975 6525
Wire Wire Line
	1900 6225 1975 6225
$Comp
L Connector_Generic:Conn_01x09 J?
U 1 1 5E9B305A
P 1350 6225
AR Path="/5E890F69/5E9B305A" Ref="J?"  Part="1" 
AR Path="/5E985316/5E9B305A" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E9B305A" Ref="J?"  Part="1" 
AR Path="/5E985745/5E9B305A" Ref="J?"  Part="1" 
AR Path="/5E985791/5E9B305A" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E9B305A" Ref="J?"  Part="1" 
F 0 "J?" H 1268 6842 50  0000 C CNN
F 1 "Conn_01x09" H 1268 6751 50  0000 C CNN
F 2 "" H 1350 6225 50  0001 C CNN
F 3 "~" H 1350 6225 50  0001 C CNN
	1    1350 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5825 850  5825
Wire Wire Line
	1150 5925 850  5925
Wire Wire Line
	1150 6025 850  6025
Wire Wire Line
	1150 6125 850  6125
Wire Wire Line
	1150 6225 850  6225
Wire Wire Line
	1150 6325 850  6325
Wire Wire Line
	1150 6425 850  6425
Wire Wire Line
	1150 6525 850  6525
Text Label 850  5825 0    50   ~ 0
~IRQ0
Text Label 850  5925 0    50   ~ 0
~IRQ1
Text Label 850  6025 0    50   ~ 0
~IRQ2
Text Label 850  6125 0    50   ~ 0
~IRQ3
Text Label 850  6225 0    50   ~ 0
~IRQ4
Text Label 850  6325 0    50   ~ 0
~IRQ5
Text Label 850  6425 0    50   ~ 0
~IRQ6
Text Label 850  6525 0    50   ~ 0
~IRQ7
Text Label 2200 1550 2    50   ~ 0
~IRQ1
Text Label 2200 1950 2    50   ~ 0
~IRQ5
Text Label 2200 1850 2    50   ~ 0
~IRQ4
Text Label 2200 1750 2    50   ~ 0
~IRQ3
Text Label 2200 2150 2    50   ~ 0
~IRQ7
Text Label 2200 1650 2    50   ~ 0
~IRQ2
Text Label 2200 2050 2    50   ~ 0
~IRQ6
Text Label 2200 1450 2    50   ~ 0
~IRQ0
NoConn ~ 1150 6625
Wire Notes Line
	750  4450 2625 4450
Wire Notes Line
	2625 4450 2625 6750
Wire Notes Line
	2625 6750 750  6750
Wire Notes Line
	750  6750 750  4450
Text Notes 875  4650 0    50   ~ 0
IRQ Select
Text Notes 2900 4625 0    50   ~ 0
Buffer
Wire Wire Line
	1650 4650 1925 4650
Wire Wire Line
	1650 4750 1925 4750
Wire Wire Line
	1650 4850 1925 4850
Wire Wire Line
	1650 4950 1925 4950
Wire Wire Line
	1650 5050 1925 5050
Wire Wire Line
	1650 5150 1925 5150
Wire Wire Line
	1650 5250 1925 5250
Wire Wire Line
	1650 5350 1925 5350
Text Label 1925 4750 2    50   ~ 0
~IRQ1
Text Label 1925 5150 2    50   ~ 0
~IRQ5
Text Label 1925 5050 2    50   ~ 0
~IRQ4
Text Label 1925 4950 2    50   ~ 0
~IRQ3
Text Label 1925 5350 2    50   ~ 0
~IRQ7
Text Label 1925 4850 2    50   ~ 0
~IRQ2
Text Label 1925 5250 2    50   ~ 0
~IRQ6
Text Label 1925 4650 2    50   ~ 0
~IRQ0
Text HLabel 1650 4650 0    50   Input ~ 0
IRQ0
Text HLabel 1650 4750 0    50   Input ~ 0
IRQ1
Text HLabel 1650 4850 0    50   Input ~ 0
IRQ2
Text HLabel 1650 4950 0    50   Input ~ 0
IRQ3
Text HLabel 1650 5050 0    50   Input ~ 0
IRQ4
Text HLabel 1650 5150 0    50   Input ~ 0
IRQ5
Text HLabel 1650 5250 0    50   Input ~ 0
IRQ6
Text HLabel 1650 5350 0    50   Input ~ 0
IRQ7
NoConn ~ 1050 3050
Text Label 4650 3275 2    50   ~ 0
R{slash}Wb
Text Label 4650 3075 2    50   ~ 0
PH1b
Text Label 6400 4575 0    50   ~ 0
~IRQ~_6522
$Comp
L 65xx:6522 U6
U 1 1 5E9B1303
P 7450 5375
F 0 "U6" H 7050 6800 50  0000 C CNN
F 1 "6522" H 7100 6700 50  0000 C CIB
F 2 "" H 7450 5525 50  0001 C CNN
F 3 "http://www.6502.org/documents/datasheets/mos/mos_6522_preliminary_nov_1977.pdf" H 7450 5525 50  0001 C CNN
	1    7450 5375
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4575 6850 4575
Wire Wire Line
	6850 4275 6400 4275
Wire Wire Line
	6850 4375 6400 4375
Wire Wire Line
	8275 4375 8050 4375
Wire Wire Line
	8275 4475 8050 4475
Wire Wire Line
	6850 4775 6400 4775
Wire Wire Line
	6850 4875 6400 4875
Wire Wire Line
	8275 4575 8050 4575
Wire Wire Line
	6850 5075 6400 5075
Wire Wire Line
	6850 5175 6400 5175
Wire Wire Line
	6850 5275 6400 5275
Wire Wire Line
	6850 5375 6400 5375
Wire Wire Line
	8275 4675 8050 4675
Wire Wire Line
	6850 5575 6400 5575
Wire Wire Line
	8275 4275 8050 4275
Wire Wire Line
	6850 5775 6400 5775
Wire Wire Line
	6850 5875 6400 5875
Wire Wire Line
	6850 5975 6400 5975
Wire Wire Line
	6850 6175 6400 6175
Wire Wire Line
	6850 6275 6400 6275
Wire Wire Line
	6850 6375 6400 6375
Wire Wire Line
	6850 6475 6400 6475
Wire Wire Line
	8050 4775 8275 4775
Wire Wire Line
	8050 4875 8275 4875
Wire Wire Line
	8050 4975 8275 4975
Wire Wire Line
	8050 5175 8275 5175
Wire Wire Line
	8050 5275 8275 5275
Wire Wire Line
	8050 5475 8275 5475
Wire Wire Line
	8050 5575 8275 5575
Wire Wire Line
	8050 5675 8275 5675
Wire Wire Line
	8050 5775 8275 5775
Wire Wire Line
	8050 5875 8275 5875
Wire Wire Line
	8050 5975 8275 5975
Wire Wire Line
	8050 6075 8275 6075
Wire Wire Line
	8050 6175 8275 6175
Wire Wire Line
	8050 6375 8275 6375
Wire Wire Line
	8050 6475 8275 6475
$Comp
L power:GND #PWR?
U 1 1 5E9C5AD2
P 7450 6825
AR Path="/5E890F69/5E9C5AD2" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9C5AD2" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9C5AD2" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9C5AD2" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9C5AD2" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9C5AD2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 6575 50  0001 C CNN
F 1 "GND" H 7450 6675 50  0000 C CNN
F 2 "" H 7450 6825 50  0001 C CNN
F 3 "" H 7450 6825 50  0001 C CNN
	1    7450 6825
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E9CC0E1
P 4800 1525
AR Path="/5E890F69/5E9CC0E1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9CC0E1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9CC0E1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9CC0E1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9CC0E1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9CC0E1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4800 1275 50  0001 C CNN
F 1 "GND" H 4800 1375 50  0000 C CNN
F 2 "" H 4800 1525 50  0001 C CNN
F 3 "" H 4800 1525 50  0001 C CNN
	1    4800 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1525 4800 1425
Wire Wire Line
	4800 1325 5200 1325
Wire Wire Line
	4800 1425 5200 1425
Connection ~ 4800 1425
Wire Wire Line
	4800 1425 4800 1325
Wire Notes Line
	2700 4450 4975 4450
Wire Notes Line
	4975 4450 4975 8250
Wire Notes Line
	4975 8250 2700 8250
Wire Notes Line
	2700 8250 2700 4450
$Comp
L 74xx:74LS32 U3
U 1 1 5E9D28A2
P 6975 1325
F 0 "U3" H 6975 1650 50  0000 C CNN
F 1 "74LS32" H 6975 1559 50  0000 C CNN
F 2 "" H 6975 1325 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 6975 1325 50  0001 C CNN
	1    6975 1325
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 2 1 5E9D5C69
P 8100 2400
F 0 "U3" H 8100 2725 50  0000 C CNN
F 1 "74LS32" H 8100 2634 50  0000 C CNN
F 2 "" H 8100 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8100 2400 50  0001 C CNN
	2    8100 2400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 3 1 5E9D6B64
P 7175 2300
F 0 "U3" H 7175 2625 50  0000 C CNN
F 1 "74LS32" H 7175 2534 50  0000 C CNN
F 2 "" H 7175 2300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7175 2300 50  0001 C CNN
	3    7175 2300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 4 1 5E9D773B
P 7175 3025
F 0 "U3" H 7175 3350 50  0000 C CNN
F 1 "74LS32" H 7175 3259 50  0000 C CNN
F 2 "" H 7175 3025 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7175 3025 50  0001 C CNN
	4    7175 3025
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U3
U 5 1 5E9D84B5
P 1050 7900
F 0 "U3" H 1280 7946 50  0000 L CNN
F 1 "74LS32" H 1280 7855 50  0000 L CNN
F 2 "" H 1050 7900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 1050 7900 50  0001 C CNN
	5    1050 7900
	1    0    0    -1  
$EndComp
Text Notes 7095 3045 0    50   ~ 0
OR
Text Notes 6900 1350 0    50   ~ 0
OR
Text Notes 8025 2425 0    50   ~ 0
OR
Text Notes 7100 2325 0    50   ~ 0
OR
$Comp
L power:+5V #PWR?
U 1 1 5E9E1071
P 6600 1175
AR Path="/5E890F69/5E9E1071" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9E1071" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9E1071" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9E1071" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9E1071" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9E1071" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6600 1025 50  0001 C CNN
F 1 "+5V" H 6600 1325 50  0000 C CNN
F 2 "" H 6600 1175 50  0001 C CNN
F 3 "" H 6600 1175 50  0001 C CNN
	1    6600 1175
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 1175 6600 1225
Wire Wire Line
	6600 1225 6675 1225
Wire Wire Line
	5950 1425 6675 1425
Wire Wire Line
	5950 1325 6325 1325
Text Label 6325 1325 2    50   ~ 0
~CE_DATA
Wire Wire Line
	5950 1625 6325 1625
Text Label 6325 1625 2    50   ~ 0
CS1
Text Label 6400 4775 0    50   ~ 0
CS1
Text Label 6400 4875 0    50   ~ 0
~CS2
Text Label 6325 1525 2    50   ~ 0
~CS2
Wire Wire Line
	6325 1525 5950 1525
Text Label 6400 5375 0    50   ~ 0
AA3
Text Label 6400 5275 0    50   ~ 0
AA2
Text Label 6400 5175 0    50   ~ 0
AA1
Text Label 6400 5075 0    50   ~ 0
AA0
Text Label 6400 4375 0    50   ~ 0
PH2b
Text Label 6400 4275 0    50   ~ 0
~RSTb
Text Label 6400 6475 0    50   ~ 0
D7
Text Label 6400 6175 0    50   ~ 0
D4
Text Label 6400 6275 0    50   ~ 0
D5
Text Label 6400 6375 0    50   ~ 0
D6
Text Label 6400 5775 0    50   ~ 0
D0
Text Label 6400 6075 0    50   ~ 0
D3
Text Label 6400 5875 0    50   ~ 0
D1
Text Label 6400 5975 0    50   ~ 0
D2
Text Label 6400 5575 0    50   ~ 0
~CE_DATA
Text Notes 7400 1325 0    50   ~ 0
not populated
NoConn ~ 5950 3450
NoConn ~ 5950 3550
NoConn ~ 5950 3650
$Comp
L 74xx:74LS04 U7
U 7 1 5EA1C253
P 1800 7900
F 0 "U7" H 2030 7946 50  0000 L CNN
F 1 "74LS04" H 2030 7855 50  0000 L CNN
F 2 "" H 1800 7900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1800 7900 50  0001 C CNN
	7    1800 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7275 1325 7925 1325
Wire Wire Line
	7800 2300 7475 2300
Wire Wire Line
	3475 9225 3475 9650
$Comp
L Device:R_Network05 RN?
U 1 1 5E9B305B
P 3375 9025
AR Path="/5E890F69/5E9B305B" Ref="RN?"  Part="1" 
AR Path="/5E985316/5E9B305B" Ref="RN?"  Part="1" 
AR Path="/5E9856B7/5E9B305B" Ref="RN?"  Part="1" 
AR Path="/5E985745/5E9B305B" Ref="RN?"  Part="1" 
AR Path="/5E985791/5E9B305B" Ref="RN?"  Part="1" 
AR Path="/5E9AC3F3/5E9B305B" Ref="RN?"  Part="1" 
F 0 "RN?" H 3325 9325 50  0000 L CNN
F 1 "470E" H 3300 9200 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 3450 9475 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 3450 9100 50  0001 C CNN
	1    3375 9025
	1    0    0    -1  
$EndComp
Wire Wire Line
	3175 9225 3175 9650
Wire Wire Line
	3375 9225 3375 9650
Wire Wire Line
	3275 9225 3275 9650
Wire Wire Line
	3575 9225 3575 9650
$Comp
L power:+5V #PWR?
U 1 1 5EA42BD1
P 3175 8825
AR Path="/5E890F69/5EA42BD1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA42BD1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA42BD1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA42BD1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA42BD1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA42BD1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3250 9300 50  0001 C CNN
F 1 "+5V" H 3175 8975 50  0000 C CNN
F 2 "" H 3100 9300 50  0001 C CNN
F 3 "" H 3100 9300 50  0001 C CNN
	1    3175 8825
	1    0    0    -1  
$EndComp
Text Label 3175 9650 1    50   ~ 0
PLUP1
Text Label 3275 9650 1    50   ~ 0
PLUP2
Text Label 3375 9650 1    50   ~ 0
PLUP3
Text Label 3475 9650 1    50   ~ 0
PLUP4
Text Label 3575 9650 1    50   ~ 0
PLUP5
Text Label 7650 2125 0    50   ~ 0
PLUP4
Wire Wire Line
	7650 2125 7650 2500
Wire Wire Line
	7650 2500 7800 2500
Wire Wire Line
	7650 2500 7650 2675
Wire Wire Line
	7650 2675 7325 2675
Connection ~ 7650 2500
Wire Wire Line
	6875 2925 6800 2925
Wire Wire Line
	6800 2925 6800 2775
Wire Wire Line
	6800 2400 6875 2400
Wire Wire Line
	6875 2200 6800 2200
Wire Wire Line
	6800 2200 6800 1900
Text Label 6800 1900 0    50   ~ 0
PLUP3
$Comp
L 74xx:74LS04 U8
U 2 1 5EA62F87
P 2200 9975
F 0 "U8" H 2200 10292 50  0000 C CNN
F 1 "74LS04" H 2200 10201 50  0000 C CNN
F 2 "" H 2200 9975 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2200 9975 50  0001 C CNN
	2    2200 9975
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U8
U 1 1 5EA62F95
P 2200 9450
F 0 "U8" H 2200 9767 50  0000 C CNN
F 1 "74LS04" H 2200 9676 50  0000 C CNN
F 2 "" H 2200 9450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2200 9450 50  0001 C CNN
	1    2200 9450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EA6B913
P 1050 8800
AR Path="/5E890F69/5EA6B913" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA6B913" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA6B913" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA6B913" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA6B913" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA6B913" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1050 8650 50  0001 C CNN
F 1 "+5V" H 1050 8950 50  0000 C CNN
F 2 "" H 1050 8800 50  0001 C CNN
F 3 "" H 1050 8800 50  0001 C CNN
	1    1050 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 8800 1050 8825
Wire Wire Line
	1050 8825 1050 9050
$Comp
L power:GND #PWR?
U 1 1 5EA6B922
P 1050 10050
AR Path="/5E890F69/5EA6B922" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA6B922" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA6B922" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA6B922" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA6B922" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA6B922" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1050 9800 50  0001 C CNN
F 1 "GND" H 1050 9900 50  0000 C CNN
F 2 "" H 1050 10050 50  0001 C CNN
F 3 "" H 1050 10050 50  0001 C CNN
	1    1050 10050
	1    0    0    -1  
$EndComp
Connection ~ 1050 8825
$Comp
L power:GND #PWR?
U 1 1 5EA6B930
P 1225 9050
AR Path="/5E890F69/5EA6B930" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EA6B930" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EA6B930" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EA6B930" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EA6B930" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EA6B930" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1225 8800 50  0001 C CNN
F 1 "GND" H 1225 8900 50  0000 C CNN
F 2 "" H 1225 9050 50  0001 C CNN
F 3 "" H 1225 9050 50  0001 C CNN
	1    1225 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 8825 1225 8825
Wire Wire Line
	1225 8825 1225 8850
$Comp
L Device:C_Small C?
U 1 1 5EA6B940
P 1225 8950
AR Path="/5E890F69/5EA6B940" Ref="C?"  Part="1" 
AR Path="/5E985316/5EA6B940" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5EA6B940" Ref="C?"  Part="1" 
AR Path="/5E985745/5EA6B940" Ref="C?"  Part="1" 
AR Path="/5E985791/5EA6B940" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5EA6B940" Ref="C?"  Part="1" 
F 0 "C?" H 1317 8996 50  0000 L CNN
F 1 "22n" H 1317 8905 50  0000 L CNN
F 2 "" H 1225 8950 50  0001 C CNN
F 3 "~" H 1225 8950 50  0001 C CNN
	1    1225 8950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U8
U 7 1 5EA6B94E
P 1050 9550
F 0 "U8" H 1280 9596 50  0000 L CNN
F 1 "74LS04" H 1280 9505 50  0000 L CNN
F 2 "" H 1050 9550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1050 9550 50  0001 C CNN
	7    1050 9550
	1    0    0    -1  
$EndComp
Text Label 8275 4275 2    50   ~ 0
PA0
Text Label 8275 4375 2    50   ~ 0
PA1
Text Label 8275 4475 2    50   ~ 0
PA2
Text Label 8275 4575 2    50   ~ 0
PA3
Text Label 8275 4675 2    50   ~ 0
PA4
Text Label 8275 4775 2    50   ~ 0
PA5
Text Label 8275 4875 2    50   ~ 0
PA6
Text Label 8275 4975 2    50   ~ 0
PA7
Text Label 8275 5475 2    50   ~ 0
PB0
Text Label 8275 5575 2    50   ~ 0
PB1
Text Label 8275 5675 2    50   ~ 0
PB2
Text Label 8275 5775 2    50   ~ 0
PB3
Text Label 8275 5875 2    50   ~ 0
PB4
Text Label 8275 5975 2    50   ~ 0
PB5
Text Label 8275 6075 2    50   ~ 0
PB6
Text Label 8275 6175 2    50   ~ 0
PB7
Text Label 8275 5175 2    50   ~ 0
CA1
Text Label 8275 5275 2    50   ~ 0
CA2
Text Label 8275 6375 2    50   ~ 0
CB1
Text Label 8275 6475 2    50   ~ 0
CB2
$Comp
L es65-rescue:CONN_DIN41612_32x3-my_conn P?
U 2 1 5EAC3C86
P 2675 2525
F 0 "P?" H 2592 750 50  0000 C CNN
F 1 "CONN_DIN41612_32x3" H 2592 841 50  0000 C CNN
F 2 "" H 2675 2525 50  0000 C CNN
F 3 "" H 2675 2525 50  0000 C CNN
	2    2675 2525
	-1   0    0    1   
$EndComp
Wire Wire Line
	2875 2675 3200 2675
Wire Wire Line
	2875 2775 3200 2775
Wire Wire Line
	2875 2875 3200 2875
Wire Wire Line
	2875 2975 3200 2975
Wire Wire Line
	2875 3075 3200 3075
Wire Wire Line
	2875 3175 3200 3175
Wire Wire Line
	2875 3275 3200 3275
Wire Wire Line
	2875 3375 3200 3375
Wire Wire Line
	2875 3475 3200 3475
Wire Wire Line
	2875 3575 3200 3575
Text Label 3200 3575 2    50   ~ 0
PB0
Text Label 3200 3175 2    50   ~ 0
PB4
Text Label 3200 2875 2    50   ~ 0
PB7
Text Label 3200 2975 2    50   ~ 0
PB6
Text Label 3200 3275 2    50   ~ 0
PB3
Text Label 3200 3075 2    50   ~ 0
PB5
Text Label 3200 3375 2    50   ~ 0
PB2
Text Label 3200 3475 2    50   ~ 0
PB1
Text Label 3200 2775 2    50   ~ 0
CB1
Text Label 3200 2675 2    50   ~ 0
CB2
$Comp
L power:GND #PWR?
U 1 1 5EAF7E96
P 1825 10100
AR Path="/5E890F69/5EAF7E96" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAF7E96" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAF7E96" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAF7E96" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAF7E96" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAF7E96" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1825 9850 50  0001 C CNN
F 1 "GND" H 1825 9950 50  0000 C CNN
F 2 "" H 1825 10100 50  0001 C CNN
F 3 "" H 1825 10100 50  0001 C CNN
	1    1825 10100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1825 10100 1825 9975
Wire Wire Line
	1825 9450 1900 9450
Wire Wire Line
	1900 9975 1825 9975
Connection ~ 1825 9975
Wire Wire Line
	1825 9975 1825 9450
$Comp
L Device:R_Network05 RN?
U 1 1 5EB0044C
P 3925 9050
AR Path="/5E890F69/5EB0044C" Ref="RN?"  Part="1" 
AR Path="/5E985316/5EB0044C" Ref="RN?"  Part="1" 
AR Path="/5E9856B7/5EB0044C" Ref="RN?"  Part="1" 
AR Path="/5E985745/5EB0044C" Ref="RN?"  Part="1" 
AR Path="/5E985791/5EB0044C" Ref="RN?"  Part="1" 
AR Path="/5E9AC3F3/5EB0044C" Ref="RN?"  Part="1" 
F 0 "RN?" H 3875 9350 50  0000 L CNN
F 1 "470E" H 3850 9225 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 4000 9500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 4000 9125 50  0001 C CNN
	1    3925 9050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EB00432
P 3725 8850
AR Path="/5E890F69/5EB00432" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB00432" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB00432" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB00432" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB00432" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB00432" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3800 9325 50  0001 C CNN
F 1 "+5V" H 3725 9000 50  0000 C CNN
F 2 "" H 3650 9325 50  0001 C CNN
F 3 "" H 3650 9325 50  0001 C CNN
	1    3725 8850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4025 9250 4025 9675
Wire Wire Line
	3925 9250 3925 9675
Wire Wire Line
	4125 9250 4125 9675
Wire Wire Line
	3825 9250 3825 9675
Wire Wire Line
	3725 9250 3725 9675
Text Label 3725 9675 1    50   ~ 0
PLUP6
Text Label 3825 9675 1    50   ~ 0
PLUP7
Text Label 3925 9675 1    50   ~ 0
PLUP8
Text Label 4025 9675 1    50   ~ 0
PLUP9
Text Label 4125 9675 1    50   ~ 0
PLUP10
Wire Wire Line
	4300 9250 4300 9675
Wire Wire Line
	4400 9250 4400 9675
Wire Wire Line
	4700 9250 4700 9675
Wire Wire Line
	4500 9250 4500 9675
Wire Wire Line
	4600 9250 4600 9675
$Comp
L power:+5V #PWR?
U 1 1 5EB0B83D
P 4300 8850
AR Path="/5E890F69/5EB0B83D" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB0B83D" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB0B83D" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB0B83D" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB0B83D" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB0B83D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4375 9325 50  0001 C CNN
F 1 "+5V" H 4300 9000 50  0000 C CNN
F 2 "" H 4225 9325 50  0001 C CNN
F 3 "" H 4225 9325 50  0001 C CNN
	1    4300 8850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Network05 RN?
U 1 1 5EB0B850
P 4500 9050
AR Path="/5E890F69/5EB0B850" Ref="RN?"  Part="1" 
AR Path="/5E985316/5EB0B850" Ref="RN?"  Part="1" 
AR Path="/5E9856B7/5EB0B850" Ref="RN?"  Part="1" 
AR Path="/5E985745/5EB0B850" Ref="RN?"  Part="1" 
AR Path="/5E985791/5EB0B850" Ref="RN?"  Part="1" 
AR Path="/5E9AC3F3/5EB0B850" Ref="RN?"  Part="1" 
F 0 "RN?" H 4450 9350 50  0000 L CNN
F 1 "470E" H 4425 9225 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP6" V 4575 9500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 4575 9125 50  0001 C CNN
	1    4500 9050
	1    0    0    -1  
$EndComp
Text Label 4400 9675 1    50   ~ 0
PLUP12
Text Label 4700 9675 1    50   ~ 0
PLUP15
Text Label 4300 9675 1    50   ~ 0
PLUP11
Text Label 4600 9675 1    50   ~ 0
PLUP14
Text Label 4500 9675 1    50   ~ 0
PLUP13
$Comp
L 74xx:74LS04 U18
U 7 1 5EB64EB2
P 14750 1500
F 0 "U18" H 14980 1546 50  0000 L CNN
F 1 "74LS04" H 14980 1455 50  0000 L CNN
F 2 "" H 14750 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 14750 1500 50  0001 C CNN
	7    14750 1500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5EB661B1
P 14750 700
F 0 "#PWR?" H 14750 550 50  0001 C CNN
F 1 "VCC" H 14765 873 50  0000 C CNN
F 2 "" H 14750 700 50  0001 C CNN
F 3 "" H 14750 700 50  0001 C CNN
	1    14750 700 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EB6791E
P 14900 900
AR Path="/5E890F69/5EB6791E" Ref="C?"  Part="1" 
AR Path="/5E985316/5EB6791E" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5EB6791E" Ref="C?"  Part="1" 
AR Path="/5E985745/5EB6791E" Ref="C?"  Part="1" 
AR Path="/5E985791/5EB6791E" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5EB6791E" Ref="C?"  Part="1" 
F 0 "C?" H 14992 946 50  0000 L CNN
F 1 "22n" H 14992 855 50  0000 L CNN
F 2 "" H 14900 900 50  0001 C CNN
F 3 "~" H 14900 900 50  0001 C CNN
	1    14900 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EB682FD
P 14900 1000
AR Path="/5E890F69/5EB682FD" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB682FD" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB682FD" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB682FD" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB682FD" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB682FD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14900 750 50  0001 C CNN
F 1 "GNDD" H 14904 845 50  0000 C CNN
F 2 "" H 14900 1000 50  0001 C CNN
F 3 "" H 14900 1000 50  0001 C CNN
	1    14900 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	14900 800  14900 775 
Wire Wire Line
	14900 775  14750 775 
Wire Wire Line
	14750 775  14750 700 
Wire Wire Line
	14750 775  14750 1000
Connection ~ 14750 775 
$Comp
L power:GNDD #PWR?
U 1 1 5EB69CD7
P 14750 2000
AR Path="/5E890F69/5EB69CD7" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB69CD7" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB69CD7" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB69CD7" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB69CD7" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB69CD7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14750 1750 50  0001 C CNN
F 1 "GNDD" H 14754 1845 50  0000 C CNN
F 2 "" H 14750 2000 50  0001 C CNN
F 3 "" H 14750 2000 50  0001 C CNN
	1    14750 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	15625 800  15475 800 
$Comp
L Device:C_Small C?
U 1 1 5EB6A300
P 15625 925
AR Path="/5E890F69/5EB6A300" Ref="C?"  Part="1" 
AR Path="/5E985316/5EB6A300" Ref="C?"  Part="1" 
AR Path="/5E9856B7/5EB6A300" Ref="C?"  Part="1" 
AR Path="/5E985745/5EB6A300" Ref="C?"  Part="1" 
AR Path="/5E985791/5EB6A300" Ref="C?"  Part="1" 
AR Path="/5E9AC3F3/5EB6A300" Ref="C?"  Part="1" 
F 0 "C?" H 15717 971 50  0000 L CNN
F 1 "22n" H 15717 880 50  0000 L CNN
F 2 "" H 15625 925 50  0001 C CNN
F 3 "~" H 15625 925 50  0001 C CNN
	1    15625 925 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U19
U 7 1 5EB6A30E
P 15475 1525
F 0 "U19" H 15705 1571 50  0000 L CNN
F 1 "74LS04" H 15705 1480 50  0000 L CNN
F 2 "" H 15475 1525 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 15475 1525 50  0001 C CNN
	7    15475 1525
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5EB6A31B
P 15475 725
F 0 "#PWR?" H 15475 575 50  0001 C CNN
F 1 "VCC" H 15490 898 50  0000 C CNN
F 2 "" H 15475 725 50  0001 C CNN
F 3 "" H 15475 725 50  0001 C CNN
	1    15475 725 
	1    0    0    -1  
$EndComp
Wire Wire Line
	15475 800  15475 725 
Wire Wire Line
	15625 825  15625 800 
Connection ~ 15475 800 
$Comp
L power:GNDD #PWR?
U 1 1 5EB6A32B
P 15475 2025
AR Path="/5E890F69/5EB6A32B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB6A32B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB6A32B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB6A32B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB6A32B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB6A32B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15475 1775 50  0001 C CNN
F 1 "GNDD" H 15479 1870 50  0000 C CNN
F 2 "" H 15475 2025 50  0001 C CNN
F 3 "" H 15475 2025 50  0001 C CNN
	1    15475 2025
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EB6A338
P 15625 1025
AR Path="/5E890F69/5EB6A338" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB6A338" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB6A338" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB6A338" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB6A338" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB6A338" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 15625 775 50  0001 C CNN
F 1 "GNDD" H 15629 870 50  0000 C CNN
F 2 "" H 15625 1025 50  0001 C CNN
F 3 "" H 15625 1025 50  0001 C CNN
	1    15625 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	15475 800  15475 1025
$Comp
L 74xx:74LS04 U19
U 3 1 5EB74A3B
P 15025 2825
F 0 "U19" H 15025 3142 50  0000 C CNN
F 1 "74LS04" H 15025 3051 50  0000 C CNN
F 2 "" H 15025 2825 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 15025 2825 50  0001 C CNN
	3    15025 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	14725 2825 14700 2825
$Comp
L 74xx:74LS04 U19
U 4 1 5EB74A78
P 15025 3325
F 0 "U19" H 15025 3642 50  0000 C CNN
F 1 "74LS04" H 15025 3551 50  0000 C CNN
F 2 "" H 15025 3325 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 15025 3325 50  0001 C CNN
	4    15025 3325
	1    0    0    -1  
$EndComp
Wire Wire Line
	14725 3325 14700 3325
Wire Wire Line
	14700 2825 14700 3325
Connection ~ 14700 3325
Wire Wire Line
	14700 3325 14700 3475
$Comp
L power:GNDD #PWR?
U 1 1 5EC2E831
P 14700 3475
AR Path="/5E890F69/5EC2E831" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EC2E831" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EC2E831" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EC2E831" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EC2E831" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EC2E831" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 14700 3225 50  0001 C CNN
F 1 "GNDD" H 14704 3320 50  0000 C CNN
F 2 "" H 14700 3475 50  0001 C CNN
F 3 "" H 14700 3475 50  0001 C CNN
	1    14700 3475
	1    0    0    -1  
$EndComp
Wire Notes Line
	10250 900  10250 9000
Connection ~ 10800 3000
Wire Wire Line
	10650 6525 10650 5800
$Comp
L Device:R_Small R?
U 1 1 5EC14BE4
P 10825 7925
F 0 "R?" H 10884 7971 50  0000 L CNN
F 1 "470E" H 10884 7880 50  0000 L CNN
F 2 "" H 10825 7925 50  0001 C CNN
F 3 "~" H 10825 7925 50  0001 C CNN
	1    10825 7925
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 5800 10650 5075
Wire Wire Line
	10550 5800 10650 5800
Wire Wire Line
	10800 3725 10800 4450
Wire Wire Line
	10650 7250 10650 6525
Connection ~ 10800 5175
Wire Wire Line
	11825 6825 11825 4600
Connection ~ 10600 3300
Connection ~ 10800 2275
Connection ~ 10600 6925
Wire Wire Line
	11575 7550 11900 7550
Wire Wire Line
	10975 7550 10550 7550
Wire Wire Line
	11575 4650 11625 4650
Wire Wire Line
	10550 5075 10650 5075
Wire Wire Line
	10800 5175 10550 5175
Wire Wire Line
	12825 4900 12050 4900
$Comp
L 74xx:74LS04 U18
U 5 1 5EB63FFD
P 11275 6825
F 0 "U18" H 11275 7142 50  0000 C CNN
F 1 "74LS04" H 11275 7051 50  0000 C CNN
F 2 "" H 11275 6825 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 6825 50  0001 C CNN
	5    11275 6825
	1    0    0    -1  
$EndComp
Wire Wire Line
	11675 4400 12825 4400
Wire Wire Line
	10800 2275 10800 3000
Wire Wire Line
	10550 7250 10650 7250
Wire Wire Line
	10650 2900 10650 2175
Wire Wire Line
	10600 6925 10600 7650
Wire Wire Line
	12825 4700 11900 4700
$Comp
L 74xx:74LS04 U18
U 4 1 5EB6390C
P 11275 7550
F 0 "U18" H 11275 7867 50  0000 C CNN
F 1 "74LS04" H 11275 7776 50  0000 C CNN
F 2 "" H 11275 7550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 7550 50  0001 C CNN
	4    11275 7550
	1    0    0    -1  
$EndComp
Connection ~ 10650 1450
Wire Wire Line
	10600 5475 10600 6200
Wire Wire Line
	10550 2175 10650 2175
Wire Wire Line
	10550 2900 10650 2900
Wire Wire Line
	10800 6625 10550 6625
Wire Wire Line
	10600 3300 10600 4025
Wire Wire Line
	10600 4750 10600 5475
Connection ~ 10800 6625
Wire Wire Line
	10650 4350 10650 3625
Wire Wire Line
	12550 6800 12525 6800
Wire Wire Line
	10600 7650 10600 7775
Wire Wire Line
	9900 8375 9950 8375
Wire Wire Line
	9125 7550 8900 7550
Text Label 8900 3200 0    50   ~ 0
PA1
$Comp
L 74xx:74LS04 U7
U 6 1 5EA1BE03
P 9425 4650
F 0 "U7" H 9550 4850 50  0000 C CNN
F 1 "74LS04" H 9600 4775 50  0000 C CNN
F 2 "" H 9425 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 4650 50  0001 C CNN
	6    9425 4650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U7
U 5 1 5EA1B662
P 9425 6100
F 0 "U7" H 9525 6300 50  0000 C CNN
F 1 "74LS04" H 9600 6225 50  0000 C CNN
F 2 "" H 9425 6100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 6100 50  0001 C CNN
	5    9425 6100
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_3_Open J3
U 1 1 5EBF0873
P 12275 6800
F 0 "J3" H 12275 7024 50  0000 C CNN
F 1 "ACK - BUSY " H 12275 6933 50  0000 C CNN
F 2 "" H 12275 6800 50  0001 C CNN
F 3 "~" H 12275 6800 50  0001 C CNN
	1    12275 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	12100 1750 12100 3900
Wire Wire Line
	10825 8025 10825 8075
Wire Wire Line
	11600 8275 12275 8275
Wire Wire Line
	12100 3900 12825 3900
Wire Wire Line
	11575 2475 12000 2475
Wire Wire Line
	9950 8075 9675 8075
Wire Wire Line
	12825 4800 11975 4800
Wire Wire Line
	10550 6825 10975 6825
Wire Wire Line
	11975 6800 12025 6800
Wire Wire Line
	10600 4025 10600 4750
Wire Wire Line
	12200 5600 12825 5600
Wire Wire Line
	12050 4900 12050 6525
Wire Wire Line
	11575 3200 11900 3200
Wire Wire Line
	10800 2275 10550 2275
Wire Wire Line
	11575 3925 11775 3925
Text Label 8900 7550 0    50   ~ 0
PA7
Wire Wire Line
	9125 1750 8900 1750
$Comp
L 74xx:74LS04 U8
U 6 1 5EA62FB1
P 9475 8275
F 0 "U8" H 9750 8475 50  0000 C CNN
F 1 "74LS04" H 9675 8400 50  0000 C CNN
F 2 "" H 9475 8275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9475 8275 50  0001 C CNN
	6    9475 8275
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10600 2575 10600 3300
$Comp
L power:VCC #PWR?
U 1 1 5EB5F039
P 12200 5475
F 0 "#PWR?" H 12200 5325 50  0001 C CNN
F 1 "VCC" H 12215 5648 50  0000 C CNN
F 2 "" H 12200 5475 50  0001 C CNN
F 3 "" H 12200 5475 50  0001 C CNN
	1    12200 5475
	1    0    0    -1  
$EndComp
Connection ~ 10650 2900
Wire Wire Line
	11900 3200 11900 4100
$Comp
L power:VCC #PWR?
U 1 1 5EC1585C
P 10825 7825
F 0 "#PWR?" H 10825 7675 50  0001 C CNN
F 1 "VCC" H 10840 7998 50  0000 C CNN
F 2 "" H 10825 7825 50  0001 C CNN
F 3 "" H 10825 7825 50  0001 C CNN
	1    10825 7825
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 1450 10650 1275
$Comp
L power:VCC #PWR?
U 1 1 5EC1CFC2
P 10650 1275
F 0 "#PWR?" H 10650 1125 50  0001 C CNN
F 1 "VCC" H 10665 1448 50  0000 C CNN
F 2 "" H 10650 1275 50  0001 C CNN
F 3 "" H 10650 1275 50  0001 C CNN
	1    10650 1275
	1    0    0    -1  
$EndComp
$Comp
L Isolator:6N135 U16
U 1 1 5E9B305C
P 10250 6725
AR Path="/5E890F69/5E9B305C" Ref="U16"  Part="1" 
AR Path="/5E985316/5E9B305C" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5E9B305C" Ref="U?"  Part="1" 
AR Path="/5E985745/5E9B305C" Ref="U?"  Part="1" 
AR Path="/5E985791/5E9B305C" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5E9B305C" Ref="U9"  Part="1" 
F 0 "U9" H 10250 7150 50  0000 C CNN
F 1 "6N135" H 10250 7059 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 6425 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 6725 50  0001 L CNN
	1    10250 6725
	1    0    0    -1  
$EndComp
Wire Wire Line
	9725 6825 9950 6825
Wire Wire Line
	9950 6625 9675 6625
Text Label 9675 1550 0    50   ~ 0
PLUP14
Text Label 9675 5175 0    50   ~ 0
PLUP8
Text Label 8900 1750 0    50   ~ 0
CA2
Text Label 9675 3000 0    50   ~ 0
PLUP11
Wire Wire Line
	9725 5375 9950 5375
Wire Wire Line
	10650 2175 10650 1450
Connection ~ 10650 3625
Wire Wire Line
	10550 1550 10800 1550
Wire Wire Line
	11575 6100 11750 6100
Wire Wire Line
	10975 1750 10550 1750
$Comp
L 74xx:74LS04 U19
U 2 1 5EB74A4A
P 11300 8275
F 0 "U19" H 11300 8592 50  0000 C CNN
F 1 "74LS04" H 11300 8501 50  0000 C CNN
F 2 "" H 11300 8275 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11300 8275 50  0001 C CNN
	2    11300 8275
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12050 6525 12550 6525
Wire Wire Line
	11575 6825 11825 6825
Connection ~ 10600 6200
Wire Wire Line
	10550 8275 11000 8275
Wire Wire Line
	10550 2475 10975 2475
Wire Wire Line
	10550 1450 10650 1450
$Comp
L 74xx:74LS04 U19
U 5 1 5EB74A86
P 11275 2475
F 0 "U19" H 11275 2792 50  0000 C CNN
F 1 "74LS04" H 11275 2701 50  0000 C CNN
F 2 "" H 11275 2475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 2475 50  0001 C CNN
	5    11275 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 4450 10550 4450
$Comp
L power:VCC #PWR?
U 1 1 5EC1DB39
P 10800 1275
F 0 "#PWR?" H 10800 1125 50  0001 C CNN
F 1 "VCC" H 10815 1448 50  0000 C CNN
F 2 "" H 10800 1275 50  0001 C CNN
F 3 "" H 10800 1275 50  0001 C CNN
	1    10800 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 6625 10800 7350
Wire Wire Line
	10800 3725 10550 3725
Wire Wire Line
	10800 7350 10550 7350
$Comp
L Device:R_Small R?
U 1 1 5EC1DB48
P 10800 1375
F 0 "R?" H 10859 1421 50  0000 L CNN
F 1 "470E" H 10859 1330 50  0000 L CNN
F 2 "" H 10800 1375 50  0001 C CNN
F 3 "~" H 10800 1375 50  0001 C CNN
	1    10800 1375
	1    0    0    -1  
$EndComp
Text Label 8900 6825 0    50   ~ 0
PA6
Text Label 8950 8275 0    50   ~ 0
CA1
Text Label 8900 5375 0    50   ~ 0
PA4
Wire Wire Line
	10975 5375 10550 5375
Connection ~ 10800 5900
Connection ~ 10600 4750
Wire Wire Line
	10600 6925 10550 6925
Wire Wire Line
	10550 2575 10600 2575
$Comp
L Connector_Generic:Conn_02x25_Top_Bottom J?
U 1 1 5E9B3060
P 13025 5100
AR Path="/5E890F69/5E9B3060" Ref="J?"  Part="1" 
AR Path="/5E985316/5E9B3060" Ref="J?"  Part="1" 
AR Path="/5E9856B7/5E9B3060" Ref="J?"  Part="1" 
AR Path="/5E985745/5E9B3060" Ref="J?"  Part="1" 
AR Path="/5E985791/5E9B3060" Ref="J?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3060" Ref="J?"  Part="1" 
F 0 "J?" H 13075 6517 50  0000 C CNN
F 1 "Conn_02x25_Top_Bottom" H 13075 6426 50  0000 C CNN
F 2 "" H 13025 5100 50  0001 C CNN
F 3 "~" H 13025 5100 50  0001 C CNN
	1    13025 5100
	1    0    0    -1  
$EndComp
Text Label 9675 5900 0    50   ~ 0
PLUP7
Wire Wire Line
	9725 7550 9950 7550
Wire Wire Line
	9725 6100 9950 6100
Wire Wire Line
	9900 8425 9900 8375
Wire Wire Line
	11900 4100 12825 4100
Connection ~ 10650 5800
Text Label 8900 6100 0    50   ~ 0
PA5
Wire Wire Line
	9125 5375 8900 5375
$Comp
L power:GNDD #PWR?
U 1 1 5E9B3048
P 10600 7775
AR Path="/5E890F69/5E9B3048" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5E9B3048" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5E9B3048" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5E9B3048" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5E9B3048" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5E9B3048" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10600 7525 50  0001 C CNN
F 1 "GNDD" H 10604 7620 50  0000 C CNN
F 2 "" H 10600 7775 50  0001 C CNN
F 3 "" H 10600 7775 50  0001 C CNN
	1    10600 7775
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U8
U 4 1 5EA62F6B
P 9425 3200
F 0 "U8" H 9525 3375 50  0000 C CNN
F 1 "74LS04" H 9600 3300 50  0000 C CNN
F 2 "" H 9425 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 3200 50  0001 C CNN
	4    9425 3200
	1    0    0    -1  
$EndComp
Text Label 9675 8075 0    50   ~ 0
PLUP13
Wire Wire Line
	10800 1550 10800 2275
Text Label 8900 4650 0    50   ~ 0
PA3
Wire Wire Line
	9125 2475 8900 2475
Wire Wire Line
	10975 3200 10550 3200
Connection ~ 10600 7650
Wire Wire Line
	10800 5900 10550 5900
Wire Wire Line
	9125 6825 8900 6825
Wire Wire Line
	9125 6100 8900 6100
Text Label 8900 2475 0    50   ~ 0
PA0
Wire Wire Line
	10550 4750 10600 4750
$Comp
L Isolator:6N135 U?
U 1 1 5EB4E838
P 10250 8175
AR Path="/5E890F69/5EB4E838" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB4E838" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB4E838" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB4E838" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB4E838" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB4E838" Ref="U20"  Part="1" 
F 0 "U20" H 10250 8600 50  0000 C CNN
F 1 "6N135" H 10250 8509 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 7875 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 8175 50  0001 L CNN
	1    10250 8175
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9125 3200 8900 3200
Text Label 9675 7350 0    50   ~ 0
PLUP15
Wire Wire Line
	9950 2275 9675 2275
Wire Wire Line
	9725 3200 9950 3200
Wire Wire Line
	10550 4025 10600 4025
Wire Wire Line
	9725 3925 9950 3925
Wire Wire Line
	9950 3725 9675 3725
$Comp
L Isolator:6N135 U?
U 1 1 5EB20BA2
P 10250 3825
AR Path="/5E890F69/5EB20BA2" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB20BA2" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB20BA2" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB20BA2" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB20BA2" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB20BA2" Ref="U13"  Part="1" 
F 0 "U13" H 10250 4250 50  0000 C CNN
F 1 "6N135" H 10250 4159 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 3525 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 3825 50  0001 L CNN
	1    10250 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 1550 9675 1550
Wire Wire Line
	10550 1850 10600 1850
$Comp
L Isolator:6N135 U?
U 1 1 5EB20B5E
P 10250 3100
AR Path="/5E890F69/5EB20B5E" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB20B5E" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB20B5E" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB20B5E" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB20B5E" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB20B5E" Ref="U14"  Part="1" 
F 0 "U14" H 10250 3525 50  0000 C CNN
F 1 "6N135" H 10250 3434 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 2800 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 3100 50  0001 L CNN
	1    10250 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 3000 9675 3000
$Comp
L Isolator:6N135 U?
U 1 1 5EB20B72
P 10250 2375
AR Path="/5E890F69/5EB20B72" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB20B72" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB20B72" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB20B72" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB20B72" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB20B72" Ref="U15"  Part="1" 
F 0 "U15" H 10250 2800 50  0000 C CNN
F 1 "6N135" H 10250 2709 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 2075 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 2375 50  0001 L CNN
	1    10250 2375
	1    0    0    -1  
$EndComp
$Comp
L Isolator:6N135 U?
U 1 1 5EB1E4EC
P 10250 4550
AR Path="/5E890F69/5EB1E4EC" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB1E4EC" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB1E4EC" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB1E4EC" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB1E4EC" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB1E4EC" Ref="U12"  Part="1" 
F 0 "U12" H 10250 4975 50  0000 C CNN
F 1 "6N135" H 10250 4884 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 4250 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 4550 50  0001 L CNN
	1    10250 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 5900 9675 5900
Wire Wire Line
	9725 4650 9950 4650
$Comp
L power:GND #PWR?
U 1 1 5EB53260
P 9900 8425
AR Path="/5E890F69/5EB53260" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB53260" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB53260" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB53260" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB53260" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB53260" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9900 8175 50  0001 C CNN
F 1 "GND" H 9900 8275 50  0000 C CNN
F 2 "" H 9900 8425 50  0001 C CNN
F 3 "" H 9900 8425 50  0001 C CNN
	1    9900 8425
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 5475 10600 5475
Wire Wire Line
	9775 8275 9950 8275
$Comp
L Isolator:6N135 U?
U 1 1 5EB4B0E0
P 10250 7450
AR Path="/5E890F69/5EB4B0E0" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB4B0E0" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB4B0E0" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB4B0E0" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB4B0E0" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB4B0E0" Ref="U17"  Part="1" 
F 0 "U17" H 10250 7875 50  0000 C CNN
F 1 "6N135" H 10250 7784 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 7150 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 7450 50  0001 L CNN
	1    10250 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 7650 10600 7650
Wire Wire Line
	9125 3925 8900 3925
$Comp
L 74xx:74LS04 U7
U 1 1 5EA188D7
P 9425 3925
F 0 "U7" H 9550 4100 50  0000 C CNN
F 1 "74LS04" H 9625 4025 50  0000 C CNN
F 2 "" H 9425 3925 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 3925 50  0001 C CNN
	1    9425 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	9175 8275 8950 8275
Wire Wire Line
	10975 4650 10550 4650
Wire Wire Line
	12000 4000 12825 4000
Wire Wire Line
	11575 1750 12100 1750
Wire Wire Line
	11775 3925 11775 4200
$Comp
L 74xx:74LS04 U8
U 3 1 5EA62F79
P 9425 2475
F 0 "U8" H 9500 2650 50  0000 C CNN
F 1 "74LS04" H 9575 2575 50  0000 C CNN
F 2 "" H 9425 2475 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 2475 50  0001 C CNN
	3    9425 2475
	1    0    0    -1  
$EndComp
Connection ~ 10650 4350
Wire Wire Line
	11625 4300 12825 4300
Wire Wire Line
	9725 2475 9950 2475
$Comp
L Isolator:6N135 U?
U 1 1 5EB20B88
P 10250 1650
AR Path="/5E890F69/5EB20B88" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB20B88" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB20B88" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB20B88" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB20B88" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB20B88" Ref="U16"  Part="1" 
F 0 "U16" H 10250 2075 50  0000 C CNN
F 1 "6N135" H 10250 1984 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 1350 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 1650 50  0001 L CNN
	1    10250 1650
	1    0    0    -1  
$EndComp
$Comp
L Isolator:6N135 U?
U 1 1 5EB1E505
P 10250 5275
AR Path="/5E890F69/5EB1E505" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB1E505" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB1E505" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB1E505" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB1E505" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB1E505" Ref="U11"  Part="1" 
F 0 "U11" H 10250 5700 50  0000 C CNN
F 1 "6N135" H 10250 5609 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 4975 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 5275 50  0001 L CNN
	1    10250 5275
	1    0    0    -1  
$EndComp
$Comp
L Isolator:6N135 U?
U 1 1 5EB1C604
P 10250 6000
AR Path="/5E890F69/5EB1C604" Ref="U?"  Part="1" 
AR Path="/5E985316/5EB1C604" Ref="U?"  Part="1" 
AR Path="/5E9856B7/5EB1C604" Ref="U?"  Part="1" 
AR Path="/5E985745/5EB1C604" Ref="U?"  Part="1" 
AR Path="/5E985791/5EB1C604" Ref="U?"  Part="1" 
AR Path="/5E9AC3F3/5EB1C604" Ref="U10"  Part="1" 
F 0 "U10" H 10250 6425 50  0000 C CNN
F 1 "6N135" H 10250 6334 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 10050 5700 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 10250 6000 50  0001 L CNN
	1    10250 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 3000 10800 3725
Wire Wire Line
	10800 5900 10800 6625
Wire Wire Line
	10975 3925 10550 3925
Wire Wire Line
	12000 2475 12000 4000
Wire Wire Line
	9950 7350 9675 7350
Wire Wire Line
	10550 3300 10600 3300
Wire Wire Line
	11575 5375 11675 5375
Connection ~ 10650 6525
Wire Wire Line
	11750 6100 11750 4500
$Comp
L 74xx:74LS04 U18
U 6 1 5EB645C3
P 11275 6100
F 0 "U18" H 11275 6417 50  0000 C CNN
F 1 "74LS04" H 11275 6326 50  0000 C CNN
F 2 "" H 11275 6100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 6100 50  0001 C CNN
	6    11275 6100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U19
U 1 1 5EB74A58
P 11275 1750
F 0 "U19" H 11275 2067 50  0000 C CNN
F 1 "74LS04" H 11275 1976 50  0000 C CNN
F 2 "" H 11275 1750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 1750 50  0001 C CNN
	1    11275 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	11750 4500 12825 4500
$Comp
L 74xx:74LS04 U18
U 1 1 5EB60218
P 11275 5375
F 0 "U18" H 11275 5692 50  0000 C CNN
F 1 "74LS04" H 11275 5601 50  0000 C CNN
F 2 "" H 11275 5375 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 5375 50  0001 C CNN
	1    11275 5375
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 6525 10650 6525
Wire Wire Line
	12275 8275 12275 6950
Wire Wire Line
	10550 8075 10825 8075
Wire Wire Line
	12200 5475 12200 5600
Wire Wire Line
	10550 6200 10600 6200
$Comp
L power:+5V #PWR?
U 1 1 5EB5388B
P 9900 7875
AR Path="/5E890F69/5EB5388B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EB5388B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EB5388B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EB5388B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EB5388B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EB5388B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9900 7725 50  0001 C CNN
F 1 "+5V" H 9900 8025 50  0000 C CNN
F 2 "" H 9900 7875 50  0001 C CNN
F 3 "" H 9900 7875 50  0001 C CNN
	1    9900 7875
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U19
U 6 1 5EB74A66
P 11275 3200
F 0 "U19" H 11275 3517 50  0000 C CNN
F 1 "74LS04" H 11275 3426 50  0000 C CNN
F 2 "" H 11275 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 3200 50  0001 C CNN
	6    11275 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	11825 4600 12825 4600
Wire Wire Line
	10800 1475 10800 1550
Wire Wire Line
	9900 7875 9900 7975
$Comp
L 74xx:74LS04 U7
U 2 1 5EA1A999
P 9425 5375
F 0 "U7" H 9525 5575 50  0000 C CNN
F 1 "74LS04" H 9600 5500 50  0000 C CNN
F 2 "" H 9425 5375 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 5375 50  0001 C CNN
	2    9425 5375
	1    0    0    -1  
$EndComp
Connection ~ 10650 5075
Wire Wire Line
	10550 3625 10650 3625
Wire Wire Line
	10650 5075 10650 4350
Wire Wire Line
	10800 3000 10550 3000
Wire Wire Line
	9725 1750 9950 1750
$Comp
L 74xx:74LS04 U7
U 3 1 5EA1AF58
P 9425 6825
F 0 "U7" H 9525 7000 50  0000 C CNN
F 1 "74LS04" H 9600 6925 50  0000 C CNN
F 2 "" H 9425 6825 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 6825 50  0001 C CNN
	3    9425 6825
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U7
U 4 1 5EA1B2DD
P 9425 7550
F 0 "U7" H 9525 7750 50  0000 C CNN
F 1 "74LS04" H 9600 7675 50  0000 C CNN
F 2 "" H 9425 7550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 7550 50  0001 C CNN
	4    9425 7550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U8
U 5 1 5EA62FA3
P 9425 1750
F 0 "U8" H 9575 1925 50  0000 C CNN
F 1 "74LS04" H 9625 1850 50  0000 C CNN
F 2 "" H 9425 1750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 9425 1750 50  0001 C CNN
	5    9425 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 1850 10600 2575
Wire Wire Line
	10800 4450 10800 5175
Connection ~ 10800 1550
Wire Wire Line
	10550 4350 10650 4350
Connection ~ 10800 3725
Connection ~ 10650 2175
Wire Wire Line
	10800 5175 10800 5900
Wire Wire Line
	10650 3625 10650 2900
Wire Wire Line
	9125 4650 8900 4650
Wire Wire Line
	9950 5175 9675 5175
Text Label 9675 2275 0    50   ~ 0
PLUP12
Text Label 9675 3725 0    50   ~ 0
PLUP10
Text Label 9675 6625 0    50   ~ 0
PLUP6
Text Label 9675 4450 0    50   ~ 0
PLUP9
Wire Wire Line
	9950 4450 9675 4450
Wire Wire Line
	9900 7975 9950 7975
$Comp
L 74xx:74LS04 U18
U 2 1 5EB6204F
P 11275 4650
F 0 "U18" H 11275 4967 50  0000 C CNN
F 1 "74LS04" H 11275 4876 50  0000 C CNN
F 2 "" H 11275 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 4650 50  0001 C CNN
	2    11275 4650
	1    0    0    -1  
$EndComp
Connection ~ 10800 4450
Connection ~ 10600 2575
Wire Wire Line
	11625 4650 11625 4300
Wire Wire Line
	11775 4200 12825 4200
Connection ~ 10600 5475
Wire Wire Line
	10975 6100 10550 6100
$Comp
L 74xx:74LS04 U18
U 3 1 5EB62EF9
P 11275 3925
F 0 "U18" H 11275 4242 50  0000 C CNN
F 1 "74LS04" H 11275 4151 50  0000 C CNN
F 2 "" H 11275 3925 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 11275 3925 50  0001 C CNN
	3    11275 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	11900 4700 11900 7550
Wire Wire Line
	11675 5375 11675 4400
Wire Wire Line
	12550 6525 12550 6800
Connection ~ 10600 4025
Wire Wire Line
	10600 6200 10600 6925
Wire Wire Line
	11975 4800 11975 6800
Text Label 8900 3925 0    50   ~ 0
PA2
NoConn ~ 13325 5000
NoConn ~ 13325 5100
NoConn ~ 13325 5200
NoConn ~ 13325 5400
NoConn ~ 13325 5500
NoConn ~ 13325 5600
NoConn ~ 13325 5700
NoConn ~ 13325 5800
NoConn ~ 13325 5900
NoConn ~ 13325 6000
NoConn ~ 13325 6100
NoConn ~ 13325 6200
NoConn ~ 13325 6300
NoConn ~ 12825 5000
NoConn ~ 12825 5100
NoConn ~ 12825 5300
NoConn ~ 12825 5500
NoConn ~ 12825 5700
NoConn ~ 12825 5800
NoConn ~ 12825 5900
NoConn ~ 12825 6000
NoConn ~ 12825 6100
NoConn ~ 12825 6200
NoConn ~ 12825 6300
Wire Wire Line
	12825 5200 12600 5200
Wire Wire Line
	12600 5200 12600 5400
Wire Wire Line
	12825 5400 12600 5400
Connection ~ 12600 5400
Wire Wire Line
	12600 5400 12600 5725
$Comp
L power:GNDD #PWR?
U 1 1 5ECA20A8
P 12600 5725
AR Path="/5E890F69/5ECA20A8" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ECA20A8" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ECA20A8" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ECA20A8" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ECA20A8" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ECA20A8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 12600 5475 50  0001 C CNN
F 1 "GNDD" H 12604 5570 50  0000 C CNN
F 2 "" H 12600 5725 50  0001 C CNN
F 3 "" H 12600 5725 50  0001 C CNN
	1    12600 5725
	1    0    0    -1  
$EndComp
Wire Wire Line
	13325 3900 13475 3900
Wire Wire Line
	13475 3900 13475 4000
$Comp
L power:GNDD #PWR?
U 1 1 5ECA8DAF
P 13475 6525
AR Path="/5E890F69/5ECA8DAF" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ECA8DAF" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ECA8DAF" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ECA8DAF" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ECA8DAF" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ECA8DAF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 13475 6275 50  0001 C CNN
F 1 "GNDD" H 13479 6370 50  0000 C CNN
F 2 "" H 13475 6525 50  0001 C CNN
F 3 "" H 13475 6525 50  0001 C CNN
	1    13475 6525
	1    0    0    -1  
$EndComp
Wire Wire Line
	13325 4000 13475 4000
Connection ~ 13475 4000
Wire Wire Line
	13475 4000 13475 4100
Wire Wire Line
	13325 4100 13475 4100
Connection ~ 13475 4100
Wire Wire Line
	13475 4100 13475 4200
Wire Wire Line
	13325 4200 13475 4200
Connection ~ 13475 4200
Wire Wire Line
	13475 4200 13475 4300
Wire Wire Line
	13325 4300 13475 4300
Connection ~ 13475 4300
Wire Wire Line
	13475 4300 13475 4400
Wire Wire Line
	13325 4400 13475 4400
Connection ~ 13475 4400
Wire Wire Line
	13475 4400 13475 4500
Wire Wire Line
	13325 4500 13475 4500
Connection ~ 13475 4500
Wire Wire Line
	13475 4500 13475 4600
Wire Wire Line
	13325 4600 13475 4600
Connection ~ 13475 4600
Wire Wire Line
	13475 4600 13475 4700
Wire Wire Line
	13325 4700 13475 4700
Connection ~ 13475 4700
Wire Wire Line
	13475 4700 13475 4800
Wire Wire Line
	13325 4800 13475 4800
Connection ~ 13475 4800
Wire Wire Line
	13475 4800 13475 4900
Wire Wire Line
	13325 4900 13475 4900
Connection ~ 13475 4900
Wire Wire Line
	13475 4900 13475 5300
Wire Wire Line
	13325 5300 13475 5300
Connection ~ 13475 5300
Wire Wire Line
	13475 5300 13475 6525
NoConn ~ 1925 3750
NoConn ~ 1925 2850
Wire Wire Line
	3725 975  4050 975 
Text Label 4050 1375 2    50   ~ 0
PB7
Text Label 4050 1075 2    50   ~ 0
PB4
Text Label 4050 1275 2    50   ~ 0
PB6
Text Label 4050 975  2    50   ~ 0
PB3
Wire Wire Line
	3725 1475 4050 1475
Text Label 4050 1475 2    50   ~ 0
CB1
Wire Wire Line
	3725 875  4050 875 
Wire Wire Line
	3725 1275 4050 1275
Text Label 4050 875  2    50   ~ 0
PB2
Wire Wire Line
	3725 1175 4050 1175
Text Label 4050 1175 2    50   ~ 0
PB5
Wire Wire Line
	3725 1375 4050 1375
Wire Wire Line
	3725 775  4050 775 
Text Label 4050 675  2    50   ~ 0
PB0
Wire Wire Line
	3725 1575 4050 1575
Text Label 4050 1575 2    50   ~ 0
CB2
Wire Wire Line
	3725 675  4050 675 
Wire Wire Line
	3725 1075 4050 1075
Text Label 4050 775  2    50   ~ 0
PB1
Text HLabel 3725 675  0    50   Input ~ 0
PB0
Text HLabel 3725 775  0    50   Input ~ 0
PB1
Text HLabel 3725 875  0    50   Input ~ 0
PB2
Text HLabel 3725 975  0    50   Input ~ 0
PB3
Text HLabel 3725 1075 0    50   Input ~ 0
PB4
Text HLabel 3725 1175 0    50   Input ~ 0
PB5
Text HLabel 3725 1275 0    50   Input ~ 0
PB6
Text HLabel 3725 1375 0    50   Input ~ 0
PB7
Text HLabel 3725 1475 0    50   Input ~ 0
CB1
Text HLabel 3725 1575 0    50   Input ~ 0
CB2
NoConn ~ 2875 975 
NoConn ~ 2875 1175
NoConn ~ 2875 1275
NoConn ~ 2875 1375
NoConn ~ 2875 1475
NoConn ~ 2875 1575
NoConn ~ 2875 1675
NoConn ~ 2875 1775
NoConn ~ 2875 1875
NoConn ~ 2875 1975
NoConn ~ 2875 2075
NoConn ~ 2875 2175
NoConn ~ 2875 2275
NoConn ~ 2875 2375
NoConn ~ 2875 2475
NoConn ~ 2875 2575
NoConn ~ 2875 3675
NoConn ~ 2875 3775
NoConn ~ 2875 3875
NoConn ~ 2875 3975
NoConn ~ 2875 4075
Wire Wire Line
	2875 1075 3200 1075
Wire Wire Line
	6175 9800 5900 9800
Wire Wire Line
	6175 9900 6025 9900
Wire Wire Line
	6175 10300 6125 10300
$Comp
L power:GND #PWR?
U 1 1 5ECF2D58
P 6125 10375
AR Path="/5E890F69/5ECF2D58" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ECF2D58" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ECF2D58" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ECF2D58" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ECF2D58" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ECF2D58" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6125 10125 50  0001 C CNN
F 1 "GND" H 6125 10225 50  0000 C CNN
F 2 "" H 6125 10375 50  0001 C CNN
F 3 "" H 6125 10375 50  0001 C CNN
	1    6125 10375
	1    0    0    -1  
$EndComp
Wire Wire Line
	6125 10375 6125 10300
$Comp
L power:+5V #PWR?
U 1 1 5ECF3BEF
P 5625 10050
AR Path="/5E890F69/5ECF3BEF" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ECF3BEF" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ECF3BEF" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ECF3BEF" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ECF3BEF" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ECF3BEF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5700 10525 50  0001 C CNN
F 1 "+5V" H 5625 10200 50  0000 C CNN
F 2 "" H 5550 10525 50  0001 C CNN
F 3 "" H 5550 10525 50  0001 C CNN
	1    5625 10050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5625 10050 5625 10200
Wire Wire Line
	5625 10200 6175 10200
$Comp
L power:+12V #PWR?
U 1 1 5ECF4C56
P 5750 9950
AR Path="/5E890F69/5ECF4C56" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ECF4C56" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ECF4C56" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ECF4C56" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ECF4C56" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ECF4C56" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5750 9800 50  0001 C CNN
F 1 "+12V" H 5765 10123 50  0000 C CNN
F 2 "" H 5750 9950 50  0001 C CNN
F 3 "" H 5750 9950 50  0001 C CNN
	1    5750 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 9950 5750 10100
Wire Wire Line
	5750 10100 6175 10100
$Comp
L power:-12V #PWR?
U 1 1 5ECF636C
P 5925 10275
AR Path="/5E890F69/5ECF636C" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5ECF636C" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5ECF636C" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5ECF636C" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5ECF636C" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5ECF636C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5925 10375 50  0001 C CNN
F 1 "-12V" H 5940 10448 50  0000 C CNN
F 2 "" H 5925 10275 50  0001 C CNN
F 3 "" H 5925 10275 50  0001 C CNN
	1    5925 10275
	-1   0    0    1   
$EndComp
Wire Wire Line
	5925 10275 5925 10000
Wire Wire Line
	5925 10000 6175 10000
Text Label 3200 1075 2    50   ~ 0
B31
Text Label 6025 9900 0    50   ~ 0
B31
Text Notes 6475 9900 0    50   ~ 0
gry\n
Text Notes 6475 10000 0    50   ~ 0
yel
Text Notes 6475 10100 0    50   ~ 0
grn
Text Notes 6475 10200 0    50   ~ 0
brn
Text Notes 6475 10300 0    50   ~ 0
wht
Text Notes 6475 9800 0    50   ~ 0
pnk
Text Label 5900 9600 1    50   ~ 0
PLUP3
Wire Wire Line
	5900 9600 5900 9800
Wire Wire Line
	6800 2775 8450 2775
Wire Wire Line
	8450 2775 8450 2400
Wire Wire Line
	8450 2400 8400 2400
Connection ~ 6800 2775
Wire Wire Line
	6800 2775 6800 2400
Text Label 4800 1925 0    50   ~ 0
~0xF800
Text Notes 3050 1750 0    50   ~ 0
0xf870-0xf87f: 6522 
$Comp
L Device:LED_Small_ALT D?
U 1 1 5ECE4931
P 8725 10200
AR Path="/5E9AC3F3/5ECE4579/5ECE4931" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE4931" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ECE4931" Ref="D?"  Part="1" 
F 0 "D?" V 8771 10130 50  0000 R CNN
F 1 "RED" V 8680 10130 50  0000 R CNN
F 2 "" V 8725 10200 50  0001 C CNN
F 3 "~" V 8725 10200 50  0001 C CNN
	1    8725 10200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8725 10300 8725 10325
$Comp
L Device:R_Small R?
U 1 1 5ECE5695
P 8725 10000
AR Path="/5E9AC3F3/5ECE4579/5ECE5695" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE5695" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ECE5695" Ref="R?"  Part="1" 
F 0 "R?" H 8784 10046 50  0000 L CNN
F 1 "1k" H 8784 9955 50  0000 L CNN
F 2 "" H 8725 10000 50  0001 C CNN
F 3 "~" H 8725 10000 50  0001 C CNN
	1    8725 10000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8725 10325 8250 10325
Wire Wire Line
	8725 9875 8725 9900
$Comp
L Device:LED_Small_ALT D?
U 1 1 5ECE5B80
P 9200 10200
AR Path="/5E9AC3F3/5ECE4579/5ECE5B80" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE5B80" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ECE5B80" Ref="D?"  Part="1" 
F 0 "D?" V 9246 10130 50  0000 R CNN
F 1 "RED +12V FL" V 9155 10130 50  0000 R CNN
F 2 "" V 9200 10200 50  0001 C CNN
F 3 "~" V 9200 10200 50  0001 C CNN
	1    9200 10200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8725 10325 9200 10325
$Comp
L Device:R_Small R?
U 1 1 5ECE5B8E
P 9200 10000
AR Path="/5E9AC3F3/5ECE4579/5ECE5B8E" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE5B8E" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ECE5B8E" Ref="R?"  Part="1" 
F 0 "R?" H 9259 10046 50  0000 L CNN
F 1 "1k" H 9259 9955 50  0000 L CNN
F 2 "" H 9200 10000 50  0001 C CNN
F 3 "~" H 9200 10000 50  0001 C CNN
	1    9200 10000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small_ALT D?
U 1 1 5ECE68AC
P 9925 10500
AR Path="/5E9AC3F3/5ECE4579/5ECE68AC" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE68AC" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ECE68AC" Ref="D?"  Part="1" 
F 0 "D?" V 9971 10430 50  0000 R CNN
F 1 "RED -12V" V 9880 10430 50  0000 R CNN
F 2 "" V 9925 10500 50  0001 C CNN
F 3 "~" V 9925 10500 50  0001 C CNN
	1    9925 10500
	0    -1   1    0   
$EndComp
$Comp
L Device:LED_Small_ALT D?
U 1 1 5ECE68C8
P 9925 10200
AR Path="/5E9AC3F3/5ECE4579/5ECE68C8" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE68C8" Ref="D?"  Part="1" 
AR Path="/5E9AC3F3/5ECE68C8" Ref="D?"  Part="1" 
F 0 "D?" V 9971 10130 50  0000 R CNN
F 1 "RED +5V" V 9880 10130 50  0000 R CNN
F 2 "" V 9925 10200 50  0001 C CNN
F 3 "~" V 9925 10200 50  0001 C CNN
	1    9925 10200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5ECE68D6
P 9925 10000
AR Path="/5E9AC3F3/5ECE4579/5ECE68D6" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE68D6" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ECE68D6" Ref="R?"  Part="1" 
F 0 "R?" H 9984 10046 50  0000 L CNN
F 1 "1k" H 9984 9955 50  0000 L CNN
F 2 "" H 9925 10000 50  0001 C CNN
F 3 "~" H 9925 10000 50  0001 C CNN
	1    9925 10000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 10325 9200 10300
Connection ~ 8725 10325
Connection ~ 9200 10325
Wire Wire Line
	9925 9825 9925 9900
$Comp
L Device:Net-Tie_2 NT?
U 1 1 5ECFAB96
P 8250 10025
AR Path="/5E9AC3F3/5ECE4579/5ECFAB96" Ref="NT?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECFAB96" Ref="NT?"  Part="1" 
AR Path="/5E9AC3F3/5ECFAB96" Ref="NT?"  Part="1" 
F 0 "NT?" V 8204 10069 50  0000 L CNN
F 1 "Net-Tie_2" V 8295 10069 50  0000 L CNN
F 2 "" H 8250 10025 50  0001 C CNN
F 3 "~" H 8250 10025 50  0001 C CNN
	1    8250 10025
	0    1    1    0   
$EndComp
Connection ~ 8250 10325
Wire Wire Line
	8250 10125 8250 10325
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 5EAB741F
P 6375 10000
F 0 "J?" H 6325 10425 50  0000 L CNN
F 1 "Conn_01x06" H 6050 10325 50  0000 L CNN
F 2 "" H 6375 10000 50  0001 C CNN
F 3 "~" H 6375 10000 50  0001 C CNN
	1    6375 10000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAB9CDC
P 8250 10375
AR Path="/5E890F69/5EAB9CDC" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAB9CDC" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAB9CDC" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAB9CDC" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAB9CDC" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAB9CDC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8250 10125 50  0001 C CNN
F 1 "GND" H 8250 10225 50  0000 C CNN
F 2 "" H 8250 10375 50  0001 C CNN
F 3 "" H 8250 10375 50  0001 C CNN
	1    8250 10375
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 10375 8250 10325
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 5EABA06F
P 7225 9975
F 0 "J?" H 7143 10392 50  0000 C CNN
F 1 "Conn_01x06" H 7100 10300 50  0000 C CNN
F 2 "" H 7225 9975 50  0001 C CNN
F 3 "~" H 7225 9975 50  0001 C CNN
	1    7225 9975
	-1   0    0    -1  
$EndComp
Text Notes 6775 10550 2    50   ~ 0
to Front Panel
Wire Wire Line
	6475 9800 7100 9800
Wire Wire Line
	6475 9900 7100 9900
Wire Wire Line
	6475 10000 7100 10000
Wire Wire Line
	6475 10100 7100 10100
Wire Wire Line
	6475 10200 7100 10200
Wire Wire Line
	6475 10300 7100 10300
$Comp
L power:GND #PWR?
U 1 1 5EABE982
P 7475 10350
AR Path="/5E890F69/5EABE982" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EABE982" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EABE982" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EABE982" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EABE982" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EABE982" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7475 10100 50  0001 C CNN
F 1 "GND" H 7475 10200 50  0000 C CNN
F 2 "" H 7475 10350 50  0001 C CNN
F 3 "" H 7475 10350 50  0001 C CNN
	1    7475 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7475 10350 7475 10275
Wire Wire Line
	7475 10275 7425 10275
$Comp
L power:+5V #PWR?
U 1 1 5EABEF40
P 7950 10050
AR Path="/5E890F69/5EABEF40" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EABEF40" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EABEF40" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EABEF40" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EABEF40" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EABEF40" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8025 10525 50  0001 C CNN
F 1 "+5V" H 7950 10200 50  0000 C CNN
F 2 "" H 7875 10525 50  0001 C CNN
F 3 "" H 7875 10525 50  0001 C CNN
	1    7950 10050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 10050 7950 10175
Wire Wire Line
	7950 10175 7425 10175
$Comp
L power:+5V #PWR?
U 1 1 5EABF70B
P 9925 9825
AR Path="/5E890F69/5EABF70B" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EABF70B" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EABF70B" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EABF70B" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EABF70B" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EABF70B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10000 10300 50  0001 C CNN
F 1 "+5V" H 9925 9975 50  0000 C CNN
F 2 "" H 9850 10300 50  0001 C CNN
F 3 "" H 9850 10300 50  0001 C CNN
	1    9925 9825
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5EAC0342
P 7825 10250
AR Path="/5E890F69/5EAC0342" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAC0342" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAC0342" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAC0342" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAC0342" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAC0342" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7825 10350 50  0001 C CNN
F 1 "-12V" H 7840 10423 50  0000 C CNN
F 2 "" H 7825 10250 50  0001 C CNN
F 3 "" H 7825 10250 50  0001 C CNN
	1    7825 10250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7825 10250 7825 9975
Wire Wire Line
	7825 9975 7425 9975
$Comp
L power:+12V #PWR?
U 1 1 5EAC083A
P 7750 9675
AR Path="/5E890F69/5EAC083A" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAC083A" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAC083A" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAC083A" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAC083A" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAC083A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7750 9525 50  0001 C CNN
F 1 "+12V" H 7765 9848 50  0000 C CNN
F 2 "" H 7750 9675 50  0001 C CNN
F 3 "" H 7750 9675 50  0001 C CNN
	1    7750 9675
	1    0    0    -1  
$EndComp
Wire Wire Line
	7425 10075 7750 10075
Wire Wire Line
	7750 10075 7750 9675
$Comp
L power:+12V #PWR?
U 1 1 5EAC2CE9
P 8725 9875
AR Path="/5E890F69/5EAC2CE9" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAC2CE9" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAC2CE9" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAC2CE9" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAC2CE9" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAC2CE9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8725 9725 50  0001 C CNN
F 1 "+12V" H 8740 10048 50  0000 C CNN
F 2 "" H 8725 9875 50  0001 C CNN
F 3 "" H 8725 9875 50  0001 C CNN
	1    8725 9875
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5EAC37C1
P 9925 10850
AR Path="/5E890F69/5EAC37C1" Ref="#PWR?"  Part="1" 
AR Path="/5E985316/5EAC37C1" Ref="#PWR?"  Part="1" 
AR Path="/5E9856B7/5EAC37C1" Ref="#PWR?"  Part="1" 
AR Path="/5E985745/5EAC37C1" Ref="#PWR?"  Part="1" 
AR Path="/5E985791/5EAC37C1" Ref="#PWR?"  Part="1" 
AR Path="/5E9AC3F3/5EAC37C1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9925 10950 50  0001 C CNN
F 1 "-12V" H 9940 11023 50  0000 C CNN
F 2 "" H 9925 10850 50  0001 C CNN
F 3 "" H 9925 10850 50  0001 C CNN
	1    9925 10850
	-1   0    0    1   
$EndComp
Wire Wire Line
	9200 10325 9925 10325
Wire Wire Line
	9925 10325 9925 10400
Connection ~ 9925 10325
Wire Wire Line
	9925 10800 9925 10850
Wire Wire Line
	9925 10300 9925 10325
$Comp
L Device:R_Small R?
U 1 1 5ECE68BA
P 9925 10700
AR Path="/5E9AC3F3/5ECE4579/5ECE68BA" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ED0AA5D/5ECE68BA" Ref="R?"  Part="1" 
AR Path="/5E9AC3F3/5ECE68BA" Ref="R?"  Part="1" 
F 0 "R?" H 9984 10746 50  0000 L CNN
F 1 "1k" H 9984 10655 50  0000 L CNN
F 2 "" H 9925 10700 50  0001 C CNN
F 3 "~" H 9925 10700 50  0001 C CNN
	1    9925 10700
	1    0    0    1   
$EndComp
Text Label 7575 9875 2    50   ~ 0
B31
Wire Wire Line
	7575 9875 7425 9875
Text Label 9400 9825 2    50   ~ 0
B31
Wire Wire Line
	9400 9825 9200 9825
Wire Wire Line
	9200 9825 9200 9900
Text Label 8250 9550 1    50   ~ 0
PLUP3
Wire Wire Line
	8250 9550 8250 9925
Wire Wire Line
	7425 9775 7600 9775
Wire Wire Line
	7600 9775 7600 9600
Text Label 7600 9600 1    50   ~ 0
PLUP3
Wire Notes Line
	6775 9150 10450 9150
Wire Notes Line
	10450 9150 10450 11125
Wire Notes Line
	10450 11125 6775 11125
Wire Notes Line
	6775 11125 6775 9150
Text Notes 6850 9300 0    50   ~ 0
Front Panel
Text Notes 12750 7025 2    50   ~ 0
 Set to ACK
Text Notes 1775 5550 2    50   ~ 0
set to IRQ5
Wire Wire Line
	6850 6075 6400 6075
$EndSCHEMATC
