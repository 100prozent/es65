; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/MUC_Board_Beranek/MUC_Beranek_lo.bin
; Page:       1


        .setcpu "6502"

_INL0           := $00F0
_INL1           := $00F1
_ADRLO0         := $00F2
_ADRLO1         := $00F3
CBLOCK_Bank_F601:= $F601
CBLOCK_DestL_F602:= $F602
CBLOCK_DestH_F603:= $F603
CBLOCK_Blocks_F604:= $F604
Floppy_DMA_Read_in_RAM_0xF7A0:= $F7A0           ; 0xf7a0
FNMRAD          := $F7A8
FNMRAD_p1       := $F7A9
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
NMI_Read_Vector := $F8D2
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
NMI_Write_Vector:= $F8DA
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
        jmp     Vector_Reset_Bootstrap

; 0xFC03
Floppy_DMA_Read_to_RAM:
        pha
        sta     NMI_DISABLE
LFC07:
        lda     FloppyDAT_03
        sta     ResetVector0+1
        inc     FNMRAD
        .byte   $D0
; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
Floppy_DriveSelect:
        .byte   $03
        inc     FNMRAD_p1
LFC15:
        lda     VIAMUC_VIAORB
        lsr     a
        bcc     LFC07
        lsr     a
        bcc     LFC15
        lda     NMI0_Vector+1+3
        sta     NMI_ENABLE
        pla
        rti

; 0xFC26
Vector_Reset_Bootstrap:
        ldx     #$FF
        txs
        lda     #$FE
        sta     OUTUMAP_F8E8
        lda     #$30
        sta     VIAMUC_DRB
        sta     VIAMUC_VIAORB
        lda     NMI0_Vector+1+3
        cld
        jsr     Wait
        ldx     #$00
LFC3F:
        lda     Floppy_DMA_Read_to_RAM,x
        sta     Floppy_DMA_Read_in_RAM_0xF7A0,x
        inx
        cpx     #$23
        bne     LFC3F
LFC4A:
        lda     VIAMUC_VIAORB
        sta     LED_IDLE_OFF
        and     #$02
        bne     LFC4A
        lda     FloppySCR_00
        lda     #$80
        sta     Floppy_MotorSel
        lda     #$F6
        sta     Floppy_MotorSel
        lda     #$80
        sta     Floppy_MotorSel
        lda     #$88
        sta     Floppy_MotorSel
        lda     #$C8
        sta     Floppy_MotorSel
        lda     #$C0
        sta     Floppy_MotorSel
        lda     #$0A
        sta     _INL0
        sta     NMI_ENABLE
LFC7C:
        lda     FloppySCR_00
        sta     LED_IDLE_OFF
        bmi     LFC7C
        jsr     Wait
        sta     LED_IDLE_ON
        lda     #$0D
        sta     FloppySCR_00
LFC8F:
        lda     VIAMUC_VIAORB
        sta     LED_IDLE_ON
        and     #$02
        bne     LFC8F
        lda     FloppySCR_00
LFC9C:
        lda     #$00
        ldy     #$F6
        sta     FNMRAD
        sty     FNMRAD_p1
        jsr     ReadNextSector
        lda     $F600
        cmp     #$45
        beq     LFCD8
        cmp     #$43
        beq     LFCB9
        lda     #$00
        jmp     LFD51

LFCB9:
        lda     CBLOCK_Bank_F601
        beq     LFCC1
        sta     OUTMAP_F8E0
LFCC1:
        lda     CBLOCK_DestL_F602
        ldy     CBLOCK_DestH_F603
        sta     FNMRAD
        sty     FNMRAD_p1
LFCCD:
        jsr     ReadNextSector
        dec     CBLOCK_Blocks_F604
        bne     LFCCD
        jmp     LFC9C

LFCD8:
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        lda     #$80
        sta     Floppy_MotorSel
        lda     #$C0
        sta     Floppy_MotorSel
        lda     #$80
        sta     Floppy_MotorSel
        sta     LED_IDLE_OFF
        jmp     LFF00

; 0xFCF9
Wait:
        ldx     #$00
        ldy     #$00
; 0xFCFD
Wait1:
        nop
        sta     NMI_Read_Vector
        nop
        inx
        bne     Wait1
        iny
        bne     Wait1
        rts

; 0xFD09
ReadNextSector:
        inc     _INL0
        lda     _INL0
        cmp     #$0B
        bcc     LFD30
        lda     #$5D
        sta     FloppySCR_00
LFD16:
        lda     VIAMUC_VIAORB
        sta     LED_IDLE_ON
        and     #$02
        bne     LFD16
        lda     FloppySCR_00
        and     #$D8
        beq     LFD2C
        lda     #$01
        jmp     LFD51

LFD2C:
        lda     #$01
        sta     _INL0
LFD30:
        lda     _INL0
        sta     FloppySEC_02
        lda     #$84
        sta     FloppySCR_00
LFD3A:
        lda     VIAMUC_VIAORB
        sta     LED_IDLE_ON
        and     #$02
        bne     LFD3A
        lda     FloppySCR_00
        and     #$BC
        beq     LFD50
        lda     #$02
        jmp     LFD51

LFD50:
        rts

LFD51:
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        lda     #$80
        sta     Floppy_MotorSel
        lda     #$C0
        sta     Floppy_MotorSel
        lda     #$80
        sta     Floppy_MotorSel
; 0xFD72
Error1:
        sta     LED_IDLE_OFF
        jsr     Wait
        sta     LED_IDLE_ON
        jsr     Wait
        jmp     Error1

; 0xFD81
SKIP:
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF
; 0xFE00
START:
        jmp     Vector_Koppelprogramm

; 0xFE03
VTABHX:
        .byte   "0123456789ABCDEF"

; 0xFE13
Vector_Koppelprogramm:
        ldx     #$FF
        .byte   $9A
        lda     #$FE
        sta     OUTUMAP_F8E8
        sta     OUTMAP_F8E0
        cld
        lda     #$9E
        sta     Serial_0U0_ControlReg
        lda     #$6B
        sta     Serial_0U0_CommandReg
        ldy     #$00
LFE2B:
        lda     #$12
        jsr     SENDCH
        jsr     RECCH
        cmp     #$0C
        bne     LFE2B
; 0xFE37
LOOK:
        jsr     RECCH
        cmp     #$47
        beq     GOEX
        cmp     #$4D
        beq     MODIFY
        cmp     #$52
        beq     READLO
        cmp     #$11
        bne     LOOK
        lda     #$13
        jsr     SENDCH
        jmp     LOOK

; 0xFE52
READLO:
        jsr     RECAD
        lda     (_ADRLO0),y
        jsr     SENDBY
        jmp     LOOK

; 0xFE5D
MODIFY:
        jsr     RECAD
        jsr     RECBYT
        sta     (_ADRLO0),y
        lda     (_ADRLO0),y
        jsr     SENDBY
        jmp     LOOK

; 0xFE6D
GOEX:
        jsr     RECAD
        ldx     #$FF
        txs
        lda     a:_ADRLO0
        ora     a:_ADRLO1
        beq     LFE81
        sta     LED_IDLE_OFF
        jmp     (_ADRLO0)

LFE81:
        sta     LED_IDLE_OFF
        jmp     LFF00

; 0xFE87
SENDCH:
        pha
LFE88:
        lda     Serial_0U0_StatusReg
        sta     LED_IDLE_ON
        and     #$10
        beq     LFE88
        pla
        sta     Serial_0U0_DataReg
        rts

; 0xFE97
SENDBY:
        pha
        pha
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        jsr     SENDHE
        pla
        and     #$0F
        jsr     SENDHE
        pla
        rts

; 0xFEA8
SENDHE:
        tax
        lda     VTABHX,x
        jmp     SENDCH

; 0xFEBAF
RECBYT:
        jsr     RECCH
        jsr     PACK
        jsr     RECCH
        jsr     PACK
        lda     _INL0
        rts

; 0xFEBE
RECAD:
        jsr     RECBYT
        jsr     RECBYT
        lda     _INL0
        sta     _ADRLO0
        lda     _INL1
        sta     _ADRLO1
        rts

; 0xFECD
RECCH:
        lda     Serial_0U0_StatusReg
        sta     LED_IDLE_ON
        and     #$08
        beq     RECCH
        lda     Serial_0U0_DataReg
        and     #$7F
        rts

; 0xFEDD
PACK:
        ldx     #$0F
; 0xFEDF
PACK1:
        cmp     VTABHX,x
        beq     PACK2
        dex
        bpl     PACK1
; 0xFEE7
ENDE:
        jmp     ENDE

; 0xFEEA
PACK2:
        txa
        rol     a
        rol     a
        rol     a
        rol     a
        ldx     #$04
; 0xFEF1
PACK3:
        rol     a
        rol     _INL0
        rol     _INL1
        dex
        bne     PACK3
        rts

        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
LFF00:
        sta     LED_IDLE_OFF
        jmp     LFF00

; 0xFF06
Vector_not_used:
        sei
        sta     NMI_DISABLE
LFF0A:
        jmp     LFF0A

; 0xFF0D
SKIP:
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF
; 0xffe0
IRQ7_Vector:
        .addr   Vector_not_used
; 0xffe2
IRQ6_Vector:
        .addr   Vector_not_used
; 0xffe4
IRQ5_Vector:
        .addr   Vector_not_used
; 0xffe6
IRQ4_Vector:
        .addr   Vector_not_used
; 0xffe8
IRQ3_Vector:
        .addr   Vector_not_used
; 0xffea
IRQ2_Vector:
        .addr   Vector_not_used
; 0xffec
IRQ1_Vector:
        .addr   Vector_not_used
; 0xffee
IRQ0_Vector:
        .addr   Vector_not_used
; 0xfff0
NMI3_Vector:
        .addr   Floppy_DMA_Read_in_RAM_0xF7A0
; 0xfff2
NMI2_Vector:
        .addr   Vector_not_used
; 0xfff4
NMI1_Vector:
        .addr   Vector_not_used
; 0xfff6
NMI0_Vector:
        .addr   Vector_not_used
        .addr   Vector_not_used
        .addr   Vector_not_used
; 0xfffc
ResetVector1:
        .addr   Vector_Koppelprogramm
; 0xfffe
ResetVector0:
        .addr   Vector_Reset_Bootstrap
