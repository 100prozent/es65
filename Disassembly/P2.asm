; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/ROM_Board/P2.bin
; Page:       1


        .setcpu "6502"

_ZERO_          := $0000                        ; 0x0000
L0008           := $0008
L000C           := $000C
L00A0           := $00A0
POIN_F0         := $00F0
POINC1_F4       := $00F4
POINC2_F6       := $00F6
POINI_FA        := $00FA
InterruptControl_Reg:= $00FE
StackTail_211   := $0211
JCSREG_0212     := $0212
JumpAddrL_221   := $0221
JumpAddrH_222   := $0222
BankTemp_22D    := $022D
BRK_TRAP_22F    := $022F
BrkSRC_ROM_23C_FF:= $023C
PANIC_SRCL_25F  := $025F
PANIC_SRCH_260  := $0260
Status_Serial2_Backup_03e5:= $03E5
UART_Flags_03e7 := $03E7
StoreA_in_SerialCommandReg:= $03E8
A_from_to_SerialRX_TX_Reg:= $03EC
SpecialCharacters_Copy:= $03F0
L06F0           := $06F0
FunctionByte_afterBRK_728:= $0728
L2020           := $2020
L38A8           := $38A8
L3A31           := $3A31
L424D           := $424D
L4445           := $4445
L4553           := $4553
L4556           := $4556
L4946           := $4946
L4E45           := $4E45
L4E49           := $4E49
L5244           := $5244
L5246           := $5246
L5341           := $5341
L5355           := $5355
L5449           := $5449
L7365           := $7365
Vector_CALL     := $801E                        ; 0x801e
Vector_CREATE   := $8021                        ; 0x8021
Vector_DISMOUNT := $8024                        ; 0x8024
Vector_DRIVE    := $8027                        ; 0x8027
Vector_FHIRQRST := $802A                        ; 0x802A
Vector_FHIRQRW  := $802D                        ; 0x802D
Vector_FHIRQSK  := $8030                        ; 0x8030
Vector_FHNMIADR := $8033                        ; 0x8033
Vector_FHNMIRW  := $8036                        ; 0x8036
Vector_LOCK     := $8039                        ; 0x8039
Vector_LOGON    := $803C                        ; 0x803c
Vector_LODPOIS2 := $803F                        ; 0x803F
Vector_MYINIT   := $8042                        ; 0x8042
Vector_OUTAUS   := $8045                        ; 0x8045
Vector_OUTVOR   := $8048                        ; 0x8048
Vector_PROGTERM := $804B                        ; 0x804b
Vector_SCHEDUL  := $804E                        ; 0x804e
Vector_SVC60    := $8051                        ; 0x8051
Vector_SVC61    := $8054                        ; 0x8054
Vector_SVC62    := $8057                        ; 0x8057
Vector_SVC63    := $805A                        ; 0x805a
Vector_SVC64    := $805D                        ; 0x805d
Vector_SVC65    := $8060                        ; 0x8060
Vector_TASKTERM := $8063                        ; 0x8063
Vector_THPRMPTE := $8066                        ; 0x8066
Vector_THWRITEE := $8069                        ; 0x8069
Vector_LOFRUP   := $806C                        ; 0x806c
Vector_SVC0D    := $806F                        ; 0x806f
PORT_B_Shadow_f3e8:= $F3E8
PH3TSK_F400_PRINTER_BUSY:= $F400
C3USAD_F41A     := $F41A
C3USAD_P3_F41D  := $F41D
C3ENT_F420      := $F420
Users_Installed_F421:= $F421                    ; 0xf421
C3USNR_F422     := $F422
SingleMultiMode_f423:= $F423
C3DATE_F424     := $F424                        ; 0xf424
C3TIME_F42E     := $F42E                        ; 0xf42e
C3TI20_F436     := $F436                        ; 0xf436
Bank_Shadow_F438:= $F438
C3LGNZ_F43C_USERS:= $F43C
MY3MAP_F448     := $F448
MY3MAP_P1_F449  := $F449
MY3MAP_P2_F44A  := $F44A
MY3MAP_P3_F44B  := $F44B
IPSW_F4AB       := $F4AB
IPANZ_F4AC_INIT_IPC:= $F4AC
IPADR_F4AE      := $F4AE
IPADR_P1_F4AF   := $F4AF
NMIURS_M1       := $F7FE
NMIURS          := $F7FF
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
Floppy_DriveSelect:= $FC11                      ; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
LFFFE           := $FFFE
LA000:
        jmp     LA0F1

        jmp     LA173

        jmp     LABE9

; 0xa009
Commands:
        .byte   "ED"
        .byte   $FE
LA00C:
        .byte   "IT"
        .byte   $FF
LA00F:
        .byte   "CO"
        .byte   $FE
        .byte   "PY"
        .byte   $FF
        .byte   "CERRFLAG"
        .byte   $FE,$FF
        .byte   "C"
LA020:
        .byte   $FE
        .byte   "ATALOG"
        .byte   $FF
        .byte   "DEL"
        .byte   $FE
        .byte   "ETE"
        .byte   $FF
        .byte   "PAUSE"
        .byte   $FE,$FF
        .byte   "REM"
        .byte   $FE
        .byte   "ARK"
        .byte   $FF
        .byte   "SI"
        .byte   $FE
        .byte   "N"
        .byte   $FF
        .byte   "ENTER"
        .byte   $FE,$FF
        .byte   "ESCAPE"
        .byte   $FE,$FF
        .byte   "E"
        .byte   $FE
        .byte   "XEC"
        .byte   $FF
        .byte   "XCMD"
        .byte   $FE,$FF
        .byte   "XD"
        .byte   $FE,$FF
        .byte   "XRQ"
        .byte   $FE,$FF
        .byte   "LOGON"
        .byte   $FE,$FF
        .byte   "LOGOFF"
        .byte   $FE,$FF
        .byte   "DATE"
        .byte   $FE,$FF
        .byte   "TIME"
        .byte   $FE,$FF
        .byte   "SC"
        .byte   $FE
        .byte   "OPY"
        .byte   $FF
        .byte   "STA"
        .byte   $FE
        .byte   "TUS"
        .byte   $FF
        .byte   "DEF"
        .byte   $FE
        .byte   "INE"
        .byte   $FF
        .byte   "DI"
        .byte   $FE
        .byte   "SMOUNT"
        .byte   $FF
        .byte   "M"
        .byte   $FE
        .byte   "OUNT"
        .byte   $FF,$FF
; 0xa0ac
CommandGranted:
        .byte   $01,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$01,$00,$00,$00,$01,$01
        .byte   $00,$00,$00,$00,$00,$00,$00
; 0xa0c3
Command_Jumptable:
        .addr   COMMAND_EDIT
        .addr   COMMAND_COPY
        .addr   COMMAND_CERRFLAG
        .addr   COMMAND_CATALOG
        .addr   COMMAND_DELETE
        .addr   COMMAND_PAUSE
        .addr   COMMAND_REMARK
        .addr   COMMAND_SIN
        .addr   COMMAND_ENTER
        .addr   COMMAND_ESCAPE
        .addr   COMMAND_EXEC
        .addr   COMMAND_XCMD
        .addr   COMMAND_XD
        .addr   COMMAND_XRQ
        .addr   COMMAND_LOGON
        .addr   COMMAND_LOGOFF
        .addr   COMMAND_DATE
        .addr   COMMAND_TIME
        .addr   COMMAND_SCOPY
        .addr   COMMAND_STATUS
        .addr   COMMAND_DEFINE
        .addr   COMMAND_DISMOUNT
        .addr   COMMAND_MOUNT
LA0F1:
        brk
        .byte   $96,$09,$A0
        bvc     FoundCommand
; Return with Error Code in A
ReturnError_a0f7:
        lda     #$A0
        bit     $FC00
        rts

; 0xa0fd
FoundCommand:
        tax
        ldy     CommandGranted,x
        asl     a
        tax
        lda     Command_Jumptable,x
        sta     POINC2_F6
        lda     Command_Jumptable+1,x
        sta     POINC2_F6+1
        cpy     #$00
        beq     CommExecGranted
        ldx     StackTail_211
        lda     $0107,x
        cmp     #$80
        bcc     ReturnError_a120
        lda     $0102,x
        beq     CommExecGranted
; Return with Error Code in A
ReturnError_a120:
        lda     #$A2
        bit     $FC00
        rts

; 0xa126
CommExecGranted:
        brk
        .byte   $11
        bvs     LA134
        cmp     #$20
        bne     ReturnError_a0f7
LA12E:
        lda     #$20
        brk
        .byte   $12
        bvc     LA12E
LA134:
        clv
        jmp     (POINC2_F6)

LA138:
        lda     #$20
        sta     $04A0
        rts

; 0xA13E
COMMAND_LOGON:
        lda     $04A0
        cmp     #$20
        beq     LA14B
        bit     $FC00
        lda     #$A2
        rts

LA14B:
        ldx     #$08
        brk
        .byte   $97,$A0
        .byte   $04
        bvs     LA138
        brk
        .byte   $1B
        bvs     LA138
        jsr     Vector_LOGON
        rts

; 0xA15B
COMMAND_LOGOFF:
        jsr     Check_isUserLoggedOn
        brk
        .byte   $1B
        bvc     LA165
        lda     #$A1
        rts

LA165:
        jsr     LA173
        dec     C3LGNZ_F43C_USERS
        brk
        .byte   $04
        ldx     #$FF
        txs
        jmp     Vector_CREATE

LA173:
        lda     $0204
        ldy     $0205
        brk
        .byte   $43
        lda     $0202
        ldy     $0203
        brk
        .byte   $43
        bit     $0207
        bvs     comm
        lda     $0200
        ldy     $0201
        brk
        .byte   $43
; 0xA190
comm:
        brk
        .byte   $B1
; 0xA192
comm:
        brk
        .byte   $00
        lda     PH3TSK_F400_PRINTER_BUSY
        cmp     $0207
        bne     LA1A1
        lda     #$00
        sta     PH3TSK_F400_PRINTER_BUSY
LA1A1:
        lda     #$04
        sta     $025E
LA1A6:
        lda     #$77
        ldy     #$F4
        jsr     Vector_LOCK
        lda     $025E
        ldx     #$FF
        jsr     Vector_DISMOUNT
        dec     $025E
        bpl     LA1A6
        brk
        .byte   $37
        lda     #$20
        sta     $04A0
        rts

; 0xA1C2
COMMAND_DATE:
        lda     #$0A
        jsr     ReadTextBuffer
        bvc     LA1CF
; Return with Error Code in A
ReturnError_a1c9:
        bit     $FC00
        lda     #$A1
        rts

LA1CF:
        lda     #$2D
        cmp     $0262
        bne     ReturnError_a1c9
        cmp     $0265
        bne     ReturnError_a1c9
        lda     #$30
        sta     $0262
        sta     $0265
        ldx     #$09
        jsr     CheckTimeDateFormat
        bvs     ReturnError_a1c9
        lda     #$2D
        sta     $0262
        sta     $0265
        ldx     #$09
        sei
LA1F5:
        lda     $025E,x
        sta     C3DATE_F424,x
        dex
        bpl     LA1F5
        cli
        clv
        rts

; 0xA201
ReadTextBuffer:
        sta     $0272
        ldx     #$00
; 0xA206
CopyTextBuffer:
        brk
        .byte   $11
        bvs     LA215
        sta     $025E,x
        inx
        cpx     $0272
        bne     CopyTextBuffer
        brk
        .byte   $1B
LA215:
        rts

; 0xA216
CheckTimeDateFormat:
        lda     $025E,x
        cmp     #$30
        bcc     LA226
        cmp     #$3A
        bcs     LA226
        dex
        bpl     CheckTimeDateFormat
        clv
        rts

LA226:
        bit     $FC00
        rts

; 0xA22A
COMMAND_TIME:
        lda     #$08
        jsr     ReadTextBuffer
        bvc     LA237
; Return with Error Code in A
ReturnError_a231:
        bit     $FC00
        lda     #$A1
        rts

LA237:
        lda     #$3A
        cmp     PANIC_SRCH_260
        bne     ReturnError_a231
        cmp     $0263
        bne     ReturnError_a231
        lda     #$30
        sta     PANIC_SRCH_260
        sta     $0263
        ldx     #$07
        jsr     CheckTimeDateFormat
        bvs     ReturnError_a231
        lda     #$3A
        sta     PANIC_SRCH_260
        sta     $0263
        ldx     #$07
        sei
LA25D:
        lda     $025E,x
        sta     C3TIME_F42E,x
        dex
        bpl     LA25D
        cli
        clv
        rts

; 0xA269
COMMAND_SCOPY:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        bit     $0207
        bvc     LA277
        lda     #$12
        rts

LA277:
        ldx     #$01
        brk
        bmi     LA2EC
        ora     $F485,x
        sty     POINC1_F4+1
        ldy     #$00
        lda     #$43
        sta     (POINC1_F4),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$02
        brk
        clc
        bvc     LA29B
LA291:
        ldx     #$01
        brk
        and     ($2C),y
        brk
        .byte   $FC
        lda     #$A1
        rts

LA29B:
        brk
        .byte   $1B
        bvs     LA291
        sta     $025E
        sty     PANIC_SRCL_25F
        lda     $0200
        ldy     $0201
        brk
        .byte   $43
        lda     $0200
        ldy     $0201
        ldx     #$01
        brk
        and     ($AD),y
        lsr     LAC02,x
        .byte   $5F
        .byte   $02
        sta     $0200
        sty     $0201
        sta     POINC1_F4
        sty     POINC1_F4+1
        ldy     #$41
        lda     #$D1
        sta     (POINC1_F4),y
        iny
        lda     #$02
        sta     (POINC1_F4),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$02
        brk
        .byte   $42
        bvc     LA2F4
        sta     $025E
        ldy     #$40
        lda     #$00
        sta     (POINC1_F4),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$02
        brk
LA2EC:
        .byte   $42
        lda     $025E
        bit     $FC00
        rts

LA2F4:
        clv
        rts

; 0xA2F6
COMMAND_STATUS:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
; 0xa2fc - 1b - ?
comm:
        brk
        .byte   $1B
        bvc     LA303
        lda     #$A1
        rts

LA303:
        jsr     Vector_OUTVOR
        lda     #$20
        ldy     SingleMultiMode_f423
        beq     LA30F
        lda     #$53
LA30F:
        ldy     $04AA
        beq     LA316
        lda     #$50
; 0xa316 - 1b - ?
LA316:
        brk
        .byte   $21
        brk
        .byte   $26
        .byte   " USER-ID   MEM  PRINTER  ENTER "



        .byte   " USER  DATE        TIME      SY"



        .byte   "S-MEMORY  CHA"

        .byte   $D2
        jsr     Vector_OUTAUS
        bvc     LA36E
        jmp     LA40F

LA36E:
        jsr     Vector_OUTVOR
; 0xa371 - 26 text
LA371:
        brk
        .byte   $26
        .byte   " "
        .byte   $A0
        ldx     #$08
; 0xa377 - 
LA377:
        brk
        .byte   $B5,$A0,$04
; 0xa37b - 
LA37b:
        brk
        .byte   $26
        .byte   " "
        .byte   $A0
        lda     $023A
; 0xa382 - 
LA382:
        brk
        .byte   $23
; 0xa384 - 
LA384:
        brk
        .byte   $26
        .byte   "K"
        .byte   $A0
        lda     PH3TSK_F400_PRINTER_BUSY
        jsr     LA410
        brk
        .byte   $2B
        lda     C3USNR_F422
        jsr     LA410
        lda     C3LGNZ_F43C_USERS
        brk
        and     $2600
        jsr     L2020
        jsr     L00A0
        and     #$00
        rol     $20
        ldy     #$00
        plp
        brk
        rol     $20
        ldy     #$AD
        sbc     #$F3
        ldy     #$00
        brk
        asl     $C0
        brk
        beq     LA3BB
        lda     #$99
LA3BB:
        brk
        .byte   $23
        brk
        rol     $25
        jsr     L5246
        eor     $C5
        brk
        rol     $20
        jsr     LA020
        lda     $04AB
        brk
        and     ($20,x)
        eor     $80
        jsr     Vector_OUTVOR
        jsr     Vector_OUTAUS
        jsr     Vector_OUTVOR
        ldx     #$01
LA3DE:
        brk
        rol     $20
        jsr     L5244
        eor     #$56
        eor     L00A0
        txa
        brk
        and     $2B00
        inx
        cpx     #$06
        bcc     LA3DE
        jsr     Vector_OUTAUS
        jsr     Vector_OUTVOR
        brk
        rol     $20
        ldy     #$A2
        brk
LA3FE:
        stx     $025E
        jsr     LA42A
        ldx     $025E
        inx
        cpx     #$05
        bcc     LA3FE
        jsr     Vector_OUTAUS
LA40F:
        rts

LA410:
        cmp     #$00
        beq     LA41F
        brk
        rol     $20
        .byte   $42
        eor     $53,x
        eor     L2020,y
        ldy     #$60
LA41F:
        brk
        rol     $20
        lsr     $52
        eor     $45
        jsr     LA020
        rts

LA42A:
        txa
        asl     a
        asl     a
        asl     a
        tay
        lda     $04BA,x
        cmp     #$20
        bne     LA457
        lda     $F700,y
        cmp     #$20
        beq     LA44A
        brk
        .byte   $26
        .byte   "-------- "

        .byte   $A0
        rts

LA44A:
        brk
        rol     $20
        jsr     L2020
        jsr     L2020
        jsr     LA020
        rts

LA457:
        cmp     #$2A
        bne     LA480
        lda     $F700,y
        cmp     #$25
        bne     LA473
        brk
LA463:
        rol     $AA
        ldx     #$08
LA467:
        lda     $F728,y
        brk
        and     ($C8,x)
        dex
        bne     LA467
        brk
        .byte   $2B
        rts

LA473:
        brk
        .byte   $26
        .byte   "******** "

        .byte   $A0
        rts

LA480:
        ldx     #$08
LA482:
        lda     $F700,y
        brk
        .byte   $21
        iny
        dex
        bne     LA482
        brk
        .byte   $26
        .byte   " "
        .byte   $A0
        rts

        .byte   "$T"
        .byte   $FE
        .byte   "ERM"
        .byte   $FF
        .byte   "$P"
        .byte   $FE
        .byte   "RINTER"
        .byte   $FF
        .byte   "CHAR="
        .byte   $FE,$FF
        .byte   "PARAM="
        .byte   $FE,$FF,$FF
LA4B1:
        .addr   LA00C
        .addr   LA00F
; 0xA4B5
COMMAND_DEFINE:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        brk
        .byte   $96
        bcc     LA463
        bvc     LA4C4
LA4C1:
        lda     #$A1
        rts

LA4C4:
        sta     $025E
        cmp     #$02
        beq     LA4EB
        cmp     #$03
        beq     LA4F7
        brk
        .byte   $1B
        bvc     LA4D9
        lda     #$2C
        brk
        .byte   $12
        bvs     LA4C1
LA4D9:
        lda     $025E
        asl     a
        tax
        lda     LA4B1+1,x
        tay
        lda     LA4B1,x
        ldx     #$01
        jsr     Vector_CALL
        rts

LA4EB:
        brk
        .byte   $11
        bvs     LA4C1
        brk
        .byte   $1B
        bvs     LA4C1
        sta     $04AB
        rts

LA4F7:
        brk
        .byte   $11
        bvs     LA4C1
        brk
        .byte   $1B
        bvs     LA4C1
        sta     $0248
        rts

; 0xA503
COMMAND_MOUNT:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        lda     #$00
        sta     $04CF
        lda     #$77
        ldy     #$F4
        jsr     Vector_LOCK
        jsr     LA793
        bvs     LA587
        lda     #$2C
        brk
        .byte   $12
        bvs     LA587
        lda     #$2A
        brk
        .byte   $12
        bvc     LA529
        jmp     LA621

LA529:
        sta     $025E
        lda     #$00
        sta     $0766
        brk
        .byte   $1B
        bvc     LA547
        lda     #$2C
        brk
        .byte   $12
        bvs     LA587
        ldx     #$08
        brk
        .byte   $97
        ror     $07
        bvs     LA587
        brk
        .byte   $1B
        bvs     LA587
LA547:
        jsr     LA68F
        bvs     LA589
        jsr     LA76D
        bvc     LA58C
        cmp     #$C3
        beq     LA560
        cmp     #$F4
        beq     LA560
        cmp     #$F5
        beq     LA560
        jmp     LA589

LA560:
        lda     $0766
        beq     LA56A
        lda     #$F7
        jmp     LA589

LA56A:
        lda     #$21
LA56C:
        ldy     $04C1
        sta     $F700,y
        ldx     $04BF
        lda     #$2A
        sta     $04BA,x
        lda     $F450
        sta     $F49C,x
        lda     #$00
        sta     $F477
        clv
        rts

LA587:
        lda     #$A1
LA589:
        jmp     LA5CC

LA58C:
        jsr     LA7A8
        lda     $0766
        bne     LA599
LA594:
        lda     #$22
        jmp     LA589

LA599:
        cmp     #$40
        bne     LA5A2
        lda     $04AA
        bne     LA5B1
LA5A2:
        ldx     #$07
        ldy     #$07
LA5A6:
        lda     (POINC2_F6),y
        cmp     $0766,x
        bne     LA594
        dey
        dex
        bpl     LA5A6
LA5B1:
        jsr     LA7A8
        ldx     #$00
        ldy     $04C1
LA5B9:
        lda     $0766,x
        sta     $F728,y
        iny
        inx
        cpx     #$08
        bcc     LA5B9
        lda     #$25
        jmp     LA56C

LA5CA:
        lda     #$A1
LA5CC:
        sta     $0767
        ldx     $04BF
        lda     $F750,x
        bne     LA5F8
        lda     #$5F
        ldy     #$F4
        jsr     Vector_LOCK
        sei
        lda     #$80
        sta     Floppy_MotorSel
        ldx     $04BF
        ora     Floppy_DriveSelect,x
        sta     Floppy_MotorSel
        and     #$F6
        sta     Floppy_MotorSel
        cli
        lda     #$00
        sta     $F45F
LA5F8:
        lda     $0207
        sta     $0765
        lda     #$08
        sta     $0207
        lda     $04CE
        ldy     $04CF
        beq     LA60F
        ldx     #$05
        brk
        .byte   $31
LA60F:
        lda     $0765
        sta     $0207
        lda     #$00
        sta     $F477
        lda     $0767
        bit     $FC00
        rts

LA621:
        ldx     #$08
        ldx     #$08
        brk
        .byte   $97
        lsr     $7002,x
        .byte   $9F
        brk
        .byte   $1B
        bvs     LA5CA
        jsr     LA68F
        bvs     LA5CC
        ldx     $04C1
        lda     $F700,x
        cmp     #$20
        beq     LA659
        jsr     LA744
LA641:
        lda     #$41
        ldx     $04BF
        sta     $04BA,x
        inc     $F750,x
        lda     $F450
        sta     $F49C,x
        lda     #$00
        sta     $F477
        clv
        rts

LA659:
        jsr     LA76D
        bvs     LA68C
        jsr     LA7A8
        ldx     #$07
        ldy     #$07
LA665:
        lda     (POINC2_F6),y
        cmp     $025E,x
        bne     LA68A
        dey
        dex
        bpl     LA665
        jsr     LA744
        jsr     LA7A8
        ldx     #$00
        ldy     $04C1
LA67B:
        lda     $025E,x
        .byte   $99
LA67F:
        brk
        .byte   $F7
        iny
        inx
        cpx     #$08
        bcc     LA67B
        jmp     LA641

LA68A:
        lda     #$22
LA68C:
        jmp     LA5CC

LA68F:
        ldx     $04BF
        lda     $04BA,x
        cmp     #$20
        beq     LA69F
        bit     $FC00
        lda     #$C0
        rts

LA69F:
        ldy     $04C1
        lda     $F700,y
        cmp     #$20
        beq     LA6C1
        ldx     #$00
LA6AB:
        lda     $025E,x
        cmp     $F700,y
        bne     LA6BB
        iny
        inx
        cpx     #$08
        bcc     LA6AB
        clv
        rts

LA6BB:
        lda     #$22
        bit     $FC00
        rts

LA6C1:
        lda     $0207
        sta     $0765
        lda     #$08
        sta     $0207
        ldx     #$05
        brk
        bmi     LA67F
        adc     $07
        stx     $0207
        bvc     LA6D9
        rts

LA6D9:
        sta     $04CE
        sty     $04CF
        ldx     $04C0
        sta     $F755,x
        tya
        sta     $F756,x
        lda     $04CE
        sta     $F782,x
        lda     $04CF
        sta     $F783,x
        inc     $F783,x
        inc     $F783,x
        clc
        cld
        lda     $04CE
        adc     #$70
        sta     $F75F,x
        lda     $04CE
        adc     #$30
        sta     $F76E,x
        lda     $04CF
        sta     $F760,x
        sta     $F76F,x
        lda     $F782,x
        sta     $F778,x
        sta     POINC2_F6
        lda     $F783,x
        sta     $F779,x
        sta     POINC2_F6+1
        ldy     #$7D
        lda     #$00
LA72A:
        sta     (POINC2_F6),y
        dey
        bpl     LA72A
        ldy     #$48
        lda     $04BF
        sta     (POINC2_F6),y
        lda     #$01
        ldy     #$45
        sta     (POINC2_F6),y
        lda     #$02
        ldy     #$2D
        sta     (POINC2_F6),y
        clv
        rts

LA744:
        jsr     LA7A8
        ldy     #$1B
        jsr     LA783
        ldy     #$2B
        jsr     LA783
        ldy     #$1D
        jsr     LA783
        bne     LA75B
        iny
        sta     (POINC2_F6),y
LA75B:
        ldy     #$2B
        sed
        clc
        lda     (POINC2_F6),y
        adc     #$01
        sta     (POINC2_F6),y
        iny
        lda     (POINC2_F6),y
        adc     #$00
        sta     (POINC2_F6),y
        rts

LA76D:
        inc     $F450
        lda     #$18
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        cmp     #$01
        beq     LA781
        bit     $FC00
        rts

LA781:
        clv
        rts

LA783:
        lda     (POINC2_F6),y
        cmp     #$E5
        bne     LA792
        lda     #$00
        sta     (POINC2_F6),y
        iny
        lda     #$00
        sta     (POINC2_F6),y
LA792:
        rts

LA793:
        lda     #$00
        ldx     #$01
        brk
        .byte   $1F
        pha
        sta     $04BF
        asl     a
        sta     $04C0
        asl     a
        asl     a
        sta     $04C1
        pla
        rts

LA7A8:
        ldx     $04C0
        lda     $F755,x
        sta     POINC2_F6
        lda     $F756,x
        sta     POINC2_F6+1
        rts

        .byte   ",KILL"
        .byte   $FE,$FF,$FF
; 0xA7BE
COMMAND_DISMOUNT:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        lda     #$77
        ldy     #$F4
        jsr     Vector_LOCK
        jsr     LA793
        bvc     LA7DB
LA7D0:
        lda     #$A1
        bit     $FC00
        ldx     #$00
        stx     $F477
        rts

LA7DB:
        brk
        .byte   $1B
        bvs     LA7E5
        ldx     #$00
        jsr     Vector_DISMOUNT
        rts

LA7E5:
        sta     $04BF
        brk
        stx     $B6,y
        .byte   $A7
        bvs     LA7D0
        brk
        .byte   $1B
        bvs     LA7D0
        lda     $04BF
        ldx     #$FF
        jsr     Vector_DISMOUNT
        rts

        .byte   "  FILENAME  ERROR SECTOR  MOUNT"



        .byte   "    VER# SECT FIRST USER      L"



        .byte   "AST USER"
        .byte   $FF
; 0xA842
COMMAND_CATALOG:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        ldx     #$43
        brk
        tya
        .byte   $1C
        asl     $50
        asl     $A9
        .byte   $A1
LA852:
        bit     $FC00
        rts

        .byte   $00,$1B
        .byte   "p"
        .byte   $F6,$A9,$1C,$A0,$06,$85,$F8,$84
        .byte   $F9
        .byte   " '"
        .byte   $80
        .byte   "p"
        .byte   $EB,$AD
        .byte   "d"
        .byte   $06,$10,$05,$A9
        .byte   "!LR"
        .byte   $A8,$8D
        .byte   "^"
        .byte   $02,$0A,$8D
        .byte   "c"
        .byte   $02
        .byte   " H"
        .byte   $80,$A0
        .byte   " "
        .byte   $AE
        .byte   "^"
        .byte   $02,$BD,$97,$F4,$F0,$02,$A0
        .byte   "R"
        tya
        brk
        and     (_ZERO_,x)
        rol     $20
        .byte   $44
        .byte   $52
        eor     #$56
        eor     L00A0
        ldx     $025E
        inx
        txa
        brk
        and     $4520
        .byte   $80
        jsr     Vector_OUTVOR
        brk
        ldx     #$FB
        .byte   $A7
        jsr     Vector_OUTAUS
        jsr     Vector_OUTVOR
        ldx     $0263
        lda     $F755,x
        sta     POINC1_F4
        lda     $F756,x
        sta     POINC1_F4+1
        brk
        rol     $20
        ldy     #$A0
        brk
LA8BD:
        lda     (POINC1_F4),y
        brk
        and     ($C8,x)
        cpy     #$08
        bcc     LA8BD
        brk
        rol     $20
        ldy     #$A0
        .byte   $1C
        lda     (POINC1_F4),y
        brk
        .byte   $23
        dey
        lda     (POINC1_F4),y
        brk
        .byte   $23
        brk
        rol     $20
        ldy     #$A0
        .byte   $1F
        lda     (POINC1_F4),y
        brk
        .byte   $23
        dey
        lda     (POINC1_F4),y
        brk
        .byte   $23
        dey
        lda     (POINC1_F4),y
        brk
        .byte   $23
        brk
        rol     $20
        ldy     #$A0
        bit     $F4B1
        brk
        .byte   $23
        dey
        lda     (POINC1_F4),y
        brk
        .byte   $23
        lda     #$20
        ldx     #$0A
        brk
        bit     a:$A9
        jsr     LAA3A
        brk
        and     _ZERO_
        .byte   $2B
        ldy     #$10
        jsr     LAA84
        brk
        .byte   $2B
        ldy     #$20
        jsr     LAA84
        jsr     Vector_OUTAUS
LA916:
        brk
        .byte   $07
        bvc     LA91F
        lda     #$02
        jmp     LA928

LA91F:
        ldx     $025E
        brk
        cmp     ($1C),y
        ora     $50
        .byte   $09
LA928:
        cmp     #$02
        beq     LA92F
        jmp     LA852

LA92F:
        clv
        rts

        lda     #$1C
        ldy     #$05
        sta     POINC1_F4
        sty     POINC1_F4+1
        lda     #$04
        sta     $0261
LA93E:
        ldy     #$00
        lda     (POINC1_F4),y
        cmp     #$20
        bne     LA949
LA946:
        jmp     LAA0C

LA949:
        ldy     #$20
        lda     (POINC1_F4),y
        sta     $0268
        lda     #$20
        sta     (POINC1_F4),y
        lda     $061C
        cmp     #$20
        beq     LA983
        lda     $068B
        bmi     LA991
        ldy     #$00
LA962:
        lda     (POINC1_F4),y
        jsr     LAA33
        beq     LA986
        cmp     $061C,y
        beq     LA97E
        lda     $061C,y
        cmp     #$20
        bne     LA946
        lda     $068B
        lsr     a
        bcs     LA983
        jmp     LA946

LA97E:
        iny
        cpy     #$20
        bcc     LA962
LA983:
        jmp     LA9C9

LA986:
        lda     $061C,y
        jsr     LAA33
        beq     LA983
        jmp     LAA0C

LA991:
        lda     #$20
        sta     $063C
        ldy     #$00
LA998:
        ldx     #$00
        sty     $076F
LA99D:
        lda     (POINC1_F4),y
        cmp     $061C,x
        beq     LA9B2
LA9A4:
        ldy     $076F
        iny
        lda     (POINC1_F4),y
        jsr     LAA33
        bne     LA998
        jmp     LAA0C

LA9B2:
        iny
        inx
        lda     $061C,x
        jsr     LAA33
        bne     LA99D
        lda     $068B
        lsr     a
        bcs     LA9C9
        lda     (POINC1_F4),y
        jsr     LAA33
        bne     LA9A4
LA9C9:
        ldy     #$20
        lda     $0268
        sta     (POINC1_F4),y
        jsr     Vector_OUTVOR
        brk
        rol     $20
        ldy     #$A0
        brk
LA9D9:
        lda     (POINC1_F4),y
        brk
        and     ($C8,x)
        cpy     #$20
        bne     LA9D9
        brk
        .byte   $2B
        ldy     #$2B
        lda     (POINC1_F4),y
        tax
        iny
        lda     (POINC1_F4),y
        tay
        txa
        brk
        and     _ZERO_
        .byte   $2B
        ldy     #$2D
        lda     (POINC1_F4),y
        jsr     LAA3A
        brk
        and     _ZERO_
        .byte   $2B
        ldy     #$20
        jsr     LAA84
        brk
        .byte   $2B
        ldy     #$30
        jsr     LAA84
        jsr     Vector_OUTAUS
LAA0C:
        clc
        cld
        lda     POINC1_F4
        adc     #$40
        sta     POINC1_F4
        lda     POINC1_F4+1
        adc     #$00
        sta     POINC1_F4+1
        dec     $0261
        beq     LAA22
        jmp     LA93E

LAA22:
        clc
        cld
        lda     $025E
        adc     #$08
        sta     $025E
        bcs     LAA31
        jmp     LA916

LAA31:
        clv
        rts

LAA33:
        cmp     #$20
        beq     LAA39
        cmp     #$3B
LAA39:
        rts

LAA3A:
        sta     $0262
        ldx     $0263
        lda     $F75F,x
        sta     POINC2_F6
        lda     $F760,x
        sta     POINC2_F6+1
        lda     #$00
        sta     $0264
        sta     $0265
        ldy     #$00
LAA54:
        jsr     LAA69
        bne     LAA54
        inc     POINC2_F6+1
LAA5B:
        jsr     LAA69
        cpy     #$90
        bcc     LAA5B
        lda     $0264
        ldy     $0265
        rts

LAA69:
        lda     (POINC2_F6),y
        cmp     $0262
        bne     LAA82
        clc
        sed
        lda     $0264
        adc     #$01
        sta     $0264
        lda     $0265
        adc     #$00
        sta     $0265
LAA82:
        iny
        rts

LAA84:
        ldx     #$08
LAA86:
        lda     (POINC1_F4),y
        brk
        and     ($C8,x)
        dex
        bne     LAA86
        brk
        .byte   $2B
        lda     (POINC1_F4),y
        iny
        brk
        .byte   $23
        lda     (POINC1_F4),y
        iny
        brk
        .byte   $23
        lda     (POINC1_F4),y
        brk
        .byte   $23
        rts

        bit     $4C41
        jmp     LFFFE

        .byte   $FF
; 0xAAA6
COMMAND_DELETE:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        ldx     #$C1
        brk
        tya
        .byte   $1C
        asl     $50
        .byte   $06
LAAB4:
        lda     #$A1
        bit     $FC00
        rts

        lda     $065C
        cmp     #$03
        bne     LAAB4
LAAC1:
        lda     $0664
        cmp     #$FE
        bne     LAACE
        lda     #$D2
        bit     $FC00
        rts

LAACE:
        brk
        .byte   $1B
        bvc     LAAEB
        lda     $0647
        cmp     #$FF
        bne     LAAB4
        brk
        stx     $9F,y
        tax
        bvs     LAAB4
        lda     #$00
        sta     $0647
        sta     $0648
        brk
        .byte   $1B
        bvs     LAAB4
LAAEB:
        brk
        .byte   $DB
        .byte   $1C
        asl     $60
; 0xaaf0
COMMAND_COPY:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        lda     #$12
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        rts

; 0xAB00
COMMAND_PAUSE:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        lda     $0236
        cmp     #$01
        beq     LAB13
LAB0D:
        brk
        .byte   $04
        brk
        .byte   $07
        bvc     LAB0D
LAB13:
        clv
        rts

; 0xab15
COMMAND_REMARK:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        clv
        rts

; 0xab1d
COMMAND_CERRFLAG:
        jsr     Check_isUserLoggedOn
        brk
        .byte   $1B
        bvc     LAB2A
        lda     #$A1
        bit     $FC00
        rts

LAB2A:
        lda     #$00
        sta     $0239
        clv
        rts

; 0xab31
COMMAND_SIN:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        ldx     #$02
        brk
        bmi     LAAC1
        .byte   $F4
        sty     POINC1_F4+1
        bvs     LAB53
        ldy     #$00
        lda     #$49
        sta     (POINC1_F4),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$01
        brk
        clc
        bvc     LAB61
LAB51:
        lda     #$A1
LAB53:
        pha
        ldx     #$02
        lda     POINC1_F4
        ldy     POINC1_F4+1
        brk
        and     ($2C),y
        brk
        .byte   $FC
        pla
        rts

LAB61:
        jsr     LABE9
        bvs     LAB53
        sta     POINC2_F6
        sty     POINC2_F6+1
        ldy     #$40
        lda     (POINC1_F4),y
        cmp     #$03
        bne     LAB51
        ldy     #$80
        lda     #$52
        sta     (POINC1_F4),y
        ldy     #$65
        lda     $0204
        sta     (POINC1_F4),y
        iny
        lda     $0205
        sta     (POINC1_F4),y
        iny
        lda     $0237
        sta     (POINC1_F4),y
        iny
        lda     $0238
        sta     (POINC1_F4),y
        iny
        lda     $0236
        sta     (POINC1_F4),y
        iny
        lda     $0248
        sta     (POINC1_F4),y
        iny
        lda     $0249
        sta     (POINC1_F4),y
        iny
        lda     $024A
        sta     (POINC1_F4),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$01
        brk
        .byte   $42
        bvs     LAB53
        sta     $0204
        sty     $0205
        lda     POINC2_F6
        ldy     POINC2_F6+1
        sta     $0249
        sty     $024A
        lda     #$21
        cpy     #$00
        bne     LABCB
        lda     #$FF
LABCB:
        sta     $0248
        clc
        cld
        lda     $0204
        adc     #$80
        sta     $0237
        lda     $0205
        adc     #$00
        sta     $0238
        ldy     #$40
        lda     (POINC1_F4),y
        sta     $0236
        clv
        rts

LABE9:
        brk
        .byte   $1B
        bvs     LABF2
        clv
        lda     #$00
        tay
        rts

LABF2:
        ldx     #$01
        brk
        bmi     LAC47
        ora     ($60,x)
        sta     POINC2_F6
        sty     POINC2_F6+1
        lda     #$00
        sta     $025E
LAC02:
        lda     #$14
        sta     PANIC_SRCL_25F
        lda     #$2C
        brk
        .byte   $12
        bvc     LAC1B
LAC0D:
        lda     POINC2_F6
        ldy     POINC2_F6+1
        ldx     #$01
        brk
        and     ($A9),y
        lda     ($2C,x)
        brk
        .byte   $FC
        rts

LAC1B:
        lda     $025E
        cmp     #$14
        bcs     LAC77
        lda     #$2C
        brk
        .byte   $12
        bvc     LAC6E
        cmp     #$0D
        beq     LAC6E
        ldx     #$20
        brk
        .byte   $97
        .byte   $1C
        asl     $50
        php
        ldx     #$20
        brk
        sta     $061C,y
        bvs     LAC0D
        stx     PANIC_SRCH_260
        ldy     $025E
        txa
        sta     (POINC2_F6),y
        iny
        .byte   $AD
LAC47:
        .byte   $5F
        .byte   $02
        sta     (POINC2_F6),y
        tay
        dey
        ldx     #$00
LAC4F:
        cpx     PANIC_SRCH_260
        beq     LAC60
        iny
        bmi     LAC0D
        lda     $061C,x
        sta     (POINC2_F6),y
        inx
        jmp     LAC4F

LAC60:
        iny
        sty     PANIC_SRCL_25F
        lda     #$2C
        brk
        .byte   $12
        bvc     LAC6E
        cmp     #$0D
        bne     LAC0D
LAC6E:
        inc     $025E
        inc     $025E
        jmp     LAC1B

LAC77:
        brk
        .byte   $1B
        bvs     LAC0D
        lda     POINC2_F6
        ldy     POINC2_F6+1
        clv
        rts

; 0xAC81
comm:
        .byte   "ED"
        .byte   $FE
        .byte   "IT"
        .byte   $FF
        .byte   "ASS"
        .byte   $FE
        .byte   "EMB"
        .byte   $FF,$FF
LAC90:
        .byte   $02,$03
LAC92:
        .addr   LB000
        .addr   LA000
        .addr   L4445
        .addr   L5449
        .addr   L2020
        .addr   L2020
        .addr   L5341
        .addr   L4553
        .addr   L424D
        .addr   L2020
; 0xaca6
COMMAND_EDIT:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        lda     LAC90
        sta     POINC2_F6
        lda     #$00
        sta     POINC2_F6+1
        lda     LAC92
        ldy     LAC92+1
        sta     POINC1_F4
        sty     POINC1_F4+1
        brk
        .byte   $1B
        bvc     LACF7
LACC3:
        lda     #$20
        brk
        .byte   $12
        bvs     LACF7
        bvc     LACC3
; 0xACCB
COMMAND_EXEC:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        brk
        .byte   $96
        sta     ($AC,x)
        bvs     LAD1B
        tax
        lda     LAC90,x
        sta     POINC2_F6
        txa
        asl     a
        sta     POINC2_F6+1
        tax
        lda     LAC92,x
        sta     POINC1_F4
        lda     LAC92+1,x
        sta     POINC1_F4+1
        jsr     LAD77
        bvc     LACF7
        lda     #$A1
LACF3:
        bit     $FC00
        rts

LACF7:
        brk
        .byte   $04
        ldx     #$FF
        txs
        lda     POINC2_F6+1
        asl     a
        asl     a
        tax
        ldy     #$00
LAD03:
        lda     LAC92+1+3,x
        sta     $0218,y
        inx
        iny
        cpy     #$08
        bcc     LAD03
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     POINC2_F6
        jsr     Vector_CALL
LAD18:
        jmp     LAD18

LAD1B:
        ldx     #$11
        brk
        ldy     $1C,x
        ora     $70
        bne     LAD44
        .byte   $77
        lda     $C970
        lda     #$FF
        sta     $05C6
        lda     #$00
        sta     $05C7
        lda     #$08
        sta     $05C8
        ldx     $0216
        dex
        stx     $05CA
        lda     #$FF
        sta     $05C9
        brk
LAD44:
        sty     $051C
        bvs     LACF3
        brk
        .byte   $04
        ldx     #$FF
        txs
        ldx     #$00
LAD50:
        lda     $051E,x
        sta     $0218,x
        inx
        cpx     #$08
        bcc     LAD50
        lda     $05CC
        ldy     $05CD
        sta     POINC2_F6
        sty     POINC2_F6+1
        ldx     $0564
        lda     VIAMUC_VIAORB
        and     #$CF
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        txa
        jmp     (POINC2_F6)

LAD77:
        brk
        .byte   $1B
        bvc     LAD85
        brk
        ora     ($C9),y
        bit     $04F0
        bit     $FC00
        rts

LAD85:
        clv
        rts

; 0xAD87
COMMAND_ENTER:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        sei
        lda     C3USNR_F422
        beq     LAD9A
        cli
        lda     #$10
        bit     $FC00
        rts

LAD9A:
        lda     #$FF
        sta     C3USNR_F422
        cli
        ldx     #$0C
        brk
        .byte   $30
        bvc     LADA9
        jmp     LAE34

LADA9:
        sta     $F440
        sty     $F441
        sta     POINC2_F6
        sty     POINC2_F6+1
        lda     $049C
        pha
        sta     POINC1_F4
        lda     $049D
        pha
        sta     POINC1_F4+1
        ldy     #$53
LADC1:
        lda     (POINC1_F4),y
        sta     $027E,y
        dey
        bne     LADC1
        lda     #$7E
        ldy     #$02
        sta     $049C
        sty     $049D
        lda     #$00
        ldy     #$02
        sta     POINC1_F4
        sty     POINC1_F4+1
        ldx     #$02
        ldy     #$00
LADDF:
        lda     (POINC1_F4),y
        sta     (POINC2_F6),y
        iny
        bne     LADDF
        inc     POINC1_F4+1
        inc     POINC2_F6+1
        inx
        cpx     #$08
        bcc     LADDF
        lda     #$00
        sta     $F43F
        lda     #$FF
        sta     $F442
        lda     #$00
        sta     $F43E
        brk
        .byte   $85
        .byte   $42
        .byte   $F4
        lda     $F443
        sta     $027F
        ldx     #$0C
        lda     $F440
        ldy     $F441
        brk
        .byte   $31
        pla
        sta     POINC1_F4+1
        sta     $049C
        pla
        sta     POINC1_F4+1
        sta     $049D
        ldy     #$01
        lda     $027E,y
        sta     (POINC1_F4),y
        lda     $F442
        cmp     #$01
        bne     LAE34
        lda     $0207
        sta     $F444
        clv
        rts

LAE34:
        ldx     #$00
        stx     C3USNR_F422
        bit     $FC00
        rts

; 0xAE3D
COMMAND_ESCAPE:
        jsr     Check_isUserLoggedOn
        jsr     Check_ErrorflagSet
        brk
        .byte   $1B,$50,$06
        lda     #$A1
LAE49:
        bit     $FC00
        rts

        bit     $0207
        bvc     LAE56
        lda     #$12
        bne     LAE49
LAE56:
        lda     $0236
        cmp     #$01
        beq     LAE61
        lda     #$08
        bne     LAE49
LAE61:
        lda     $F444
        cmp     $0207
        beq     LAE6D
        lda     #$A2
        bne     LAE49
LAE6D:
        lda     #$FF
        sta     $F43F
        clv
        rts

LAE74:
        bit     $FC00
        lda     #$A1
        rts

; 0xAE7A
COMMAND_XD:
        jsr     LAF23
        ldx     #$FF
        brk
        .byte   $95,$6E
        .byte   $02
        bvs     LAE74
        brk
        .byte   $1B
        bvs     LAE74
        lda     $026E
        and     #$F0
        sta     a:POINC1_F4
        lda     $026F
        sta     a:POINC1_F4+1
LAE97:
        ldy     #$00
LAE99:
        lda     (POINC1_F4),y
        sta     $025E,y
        iny
        .byte   $C0
LAEA0:
        bpl     $AE32
        inc     $20,x
        pha
        .byte   $80
        brk
        .byte   $26
        .byte   " "
        .byte   $A0
        ldy     a:POINC1_F4+1
        lda     a:POINC1_F4
        brk
        .byte   $25
        brk
        .byte   $26
        .byte   " :"
        .byte   $A0
        ldx     #$00
LAEB9:
        lda     $025E,x
        brk
        .byte   $23
        brk
        .byte   $2B
        inx
        txa
        and     #$03
        bne     LAEC8
        brk
        .byte   $2B
LAEC8:
        txa
        and     #$0F
        bne     LAEB9
        brk
        .byte   $2B
        ldx     #$00
LAED1:
        lda     $025E,x
        brk
        .byte   $21
        inx
        cpx     #$10
        bne     LAED1
        jsr     Vector_OUTAUS
        clc
        cld
        lda     a:POINC1_F4
        adc     #$10
        sta     a:POINC1_F4
        lda     a:POINC1_F4+1
        adc     #$00
        sta     a:POINC1_F4+1
        lda     a:POINC1_F4
        ora     a:POINC1_F4+1
        beq     LAF0C
        brk
        .byte   $07
        bvs     LAF0C
        sec
        cld
        lda     $0270
        sbc     a:POINC1_F4
        lda     $0271
        sbc     a:POINC1_F4+1
        bcs     LAE97
LAF0C:
        clv
        rts

; 0xAF0E
COMMAND_XRQ:
        jsr     Check_isUserLoggedOn
        jsr     LAF23
        brk
        .byte   $1B
        bvc     LAF1E
        lda     #$A1
LAF1A:
        bit     $FC00
        rts

LAF1E:
        brk
        .byte   $32
        bvs     LAF1A
        rts

LAF23:
        lda     $04AA
        beq     LAF29
        rts

LAF29:
        pla
        pla
        lda     #$A0
        bit     $FC00
        rts

; 0xAF31
COMMAND_XCMD:
        brk
        .byte   $1B
        bvs     LAF41
        lda     #$00
        sta     $04AA
LAF3A:
        lda     #$A0
        bit     $FC00
        clc
        rts

LAF41:
        ldx     #$08
        brk
        .byte   $99,$5E,$02
        bvs     LAF3A
        ldx     #$07
LAF4B:
        lda     $FF07,x
        cmp     $025E,x
        bne     LAF3A
        dex
        bpl     LAF4B
        lda     #$FF
        sta     $04AA
        jmp     LAF3A

; 0xAF5E
Check_isUserLoggedOn:
        lda     $04A0
        cmp     #$20
        beq     LAF66
        rts

LAF66:
        pla
        pla
        clc
        bit     $FC00
        rts

; 0xAF6D
Check_ErrorflagSet:
        lda     $0239
        bne     LAF73
        rts

LAF73:
        pla
        pla
        lda     #$06
        bit     $FC00
        clc
        rts

        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $F0
LB000:
        jmp     EditorSTART

; 0xB003
EditorCmd:
        .byte   "@L"
        .byte   $FE,$FF
        .byte   "C"
        .byte   $FE,$FF
        .byte   "P"
        .byte   $FE,$FF,$FF
        .byte   "A"
        .byte   $FE
        .byte   "LL"
        .byte   $FF,$FF
        .byte   "AOVERFLOW (@REN 1,1 ASSUMED"



        .byte   $A9
        .byte   "BNOT SAVED (USE @QU, @WRI OR @D"



        .byte   "EL ALL"
        .byte   $A9
        .byte   "CLINE# OVERFLO"

        .byte   $D7,$FF
        .byte   "S:"
        .byte   $FE,$FF
        .byte   "E"
        .byte   $FE
        .byte   "ND"
        .byte   $FF
        .byte   "P"
        .byte   $FE,$FF
        .byte   "DEL"
        .byte   $FE
        .byte   "ETE"
        .byte   $FF
        .byte   "STA"
        .byte   $FE
        .byte   "TUS"
        .byte   $FF
        .byte   "I"
        .byte   $FE
        .byte   "NSERT"
        .byte   $FF
        .byte   "D"
        .byte   $FE
        .byte   "ISPLAY"
        .byte   $FF
        .byte   "REN"
        .byte   $FE
        .byte   "UMBER"
        .byte   $FF
        .byte   "M"
        .byte   $FE
        .byte   "ODIFY"
        .byte   $FF
        .byte   "FILE"
        .byte   $FE,$FF
        .byte   "FI"
        .byte   $FE
        .byte   "ND"
        .byte   $FF
        .byte   "CH"
        .byte   $FE
        .byte   "ANGE"
        .byte   $FF
        .byte   "L"
        .byte   $FE
        .byte   "AST"
        .byte   $FF
        .byte   "WRI"
        .byte   $FE
        .byte   "TE"
        .byte   $FF
        .byte   "OV"
        .byte   $FE
        .byte   "ERWRITE"
        .byte   $FF
        .byte   "REA"
        .byte   $FE
        .byte   "D"
        .byte   $FF
        .byte   "QU"
        .byte   $FE
        .byte   "IT"
        .byte   $FF
        .byte   "H"
        .byte   $FE
        .byte   "ELP"
        .byte   $FF,$FF
LB0E4:
        .byte   "F"
LB0E5:
        .byte   $B4,$98,$B4,$BE,$B4
        .byte   "3"
        .byte   $B5,$8D,$B4,$0F,$B4,$BE,$B4
        .byte   "p"
        .byte   $B5,$F1,$B5,$FA,$BC
        .byte   "c"
        .byte   $BB,$B0,$BB,$1E,$B6
        .byte   "B"
        .byte   $BD
        .byte   "="
        .byte   $BD,$AD,$BD,$15,$BD
        .byte   "R"
        .byte   $B4,$00,$08,$06,$09,$83,$08,$06
        .byte   $09,$06,$09,$00,$00,$00,$00
; 0xB116
EditorSTART:
        lda     #$00
        sta     $0959
        lda     #$03
        sta     $0801
        lda     #$53
        sta     $0906
        lda     #$56
        ldy     #$0A
        sta     $19
        sty     $1A
        sta     $17
        sty     $18
        sta     $1F
        sty     $20
        lda     #$0D
        sta     $0A55
        lda     #$00
        sta     $34
        sta     $40
        brk
        .byte   $82
        eor     (_ZERO_,x)
        ldy     $52
        dey
        lda     #$7F
        sta     $1B
        sty     $1C
        jsr     LBA5B
        brk
        rol     $20
        jsr     L7365
        rol     $35,x
        and     L4445
        eor     #$54
        jsr     L4556
        .byte   $52
        .byte   $53
        rol     $2E42
        rol     $2E,x
        and     $20,x
        .byte   $4F
        lsr     a:L00A0
        and     #$00
        rol     $20
        eor     ($54,x)
        ldy     #$00
        plp
        jsr     LBA68
        lda     #$49
        sta     $27
        lda     #$00
        sta     a:$3B
        lda     #$10
        ldy     #$00
        sta     $1D
        sty     $1E
        sta     $25
        sty     $26
        lda     #$1F
        sta     $0A56
        brk
        ora     $1B00,x
        bvc     LB1D6
        jsr     LBAAE
        bvc     LB1AA
        jmp     LB1A3

LB1A1:
        lda     #$A1
LB1A3:
        jsr     LBA77
        lda     #$06
        brk
        .byte   $01
LB1AA:
        brk
        .byte   $1B
        bvs     LB1A1
        jsr     LBADA
        lda     #$00
        sta     a:$35
        sta     a:$36
        lda     #$99
        sta     a:$37
        sta     a:$38
        lda     #$FF
        sta     a:$3E
        jsr     LBF51
        bvs     LB1A3
        lda     #$00
        sta     a:$34
        jsr     LB831
        jmp     LB623

LB1D6:
        lda     a:$3B
        beq     LB1E6
        jsr     LBA5B
        jsr     LBA68
        lda     #$00
        sta     a:$3B
LB1E6:
        lda     #$4B
        sta     $0800
        brk
        .byte   $82
        eor     (_ZERO_,x)
        jsr     LB801
        ldx     #$47
        brk
        .byte   $89
        php
        lda     ($50),y
        .byte   $02
        brk
        .byte   $01
LB1FC:
        brk
        .byte   $82
        eor     (_ZERO_,x)
        lda     $51
        bne     LB206
        sta     $40
LB206:
        brk
        bcc     LB209
LB209:
        php
        lda     #$40
        brk
        .byte   $1C
        sta     $04
        lda     $58
        brk
        .byte   $12
        bvs     LB219
        jmp     LB388

LB219:
        lda     $40
        beq     LB225
        lda     #$06
        jsr     LBA77
        jmp     LB1D6

LB225:
        lda     #$FF
        sta     $34
        lda     $04
        beq     LB238
        jsr     LB326
        bvc     LB238
        jsr     LBA77
        jmp     LB1D6

LB238:
        lda     $27
        cmp     #$49
        bne     LB241
        jmp     LB2C9

LB241:
        lda     $04
        bne     LB25E
        lda     $1F
        ldy     $20
        sta     L0008
        sty     $09
        sta     _ZERO_
        sty     $01
        jsr     LB7B6
        sta     $02
        sty     $03
        jsr     LB662
        jmp     LB2AE

LB25E:
        lda     $33
        cmp     $04
        beq     LB29C
        bcc     LB285
        sec
        cld
        lda     $33
        sbc     $04
        sta     $2B
        clc
        lda     $1F
        sta     _ZERO_
        adc     $2B
        sta     $02
        lda     $20
        sta     $01
        adc     #$00
        sta     $03
        jsr     LB662
        jmp     LB29C

LB285:
        cld
        sec
        lda     $04
        sbc     $33
        sta     $04
        lda     #$00
        sta     $05
        lda     $1F
        ldy     $20
        sta     $06
        sty     $07
        jsr     LB6E0
LB29C:
        lda     $1F
        sta     L0008
        lda     $20
        sta     $09
        jsr     LB9E0
        jsr     LB7B6
        sta     $1F
        sty     $20
LB2AE:
        ldy     #$00
        lda     ($1F),y
        cmp     #$1F
        bne     LB2B9
        jmp     LB623

LB2B9:
        sta     $1E
        iny
        lda     ($1F),y
        sta     $1D
        jmp     LB1D6

LB2C3:
        jsr     LB34D
LB2C6:
        jmp     LB1D6

LB2C9:
        lda     $04
        beq     LB2C6
        ldx     #$03
LB2CF:
        lda     $1D,x
        sta     a:$21,x
        dex
        bpl     LB2CF
        jsr     LB34D
        lda     #$4B
        sta     $0883
        jsr     LBA5B
        lda     $27
        brk
        and     (_ZERO_,x)
        .byte   $2B
        lda     $1D
        ldy     $1E
        brk
        and     _ZERO_
        .byte   $2B
        ldx     #$C6
        brk
        .byte   $89
        .byte   $0C
        lda     ($50),y
        .byte   $02
        brk
        ora     (L00A0,x)
        .byte   $03
LB2FC:
        lda     $21,y
        sta     $1D,y
        dey
        bpl     LB2FC
        lda     a:$04
        jsr     LB9E0
        jsr     LB34D
        ldx     #$00
        brk
        .byte   $89
        .byte   $0C
        lda     ($50),y
        .byte   $02
        brk
        ora     ($A2,x)
        .byte   $83
LB31A:
        lda     $0882,x
        sta     $07FF,x
        dex
        bne     LB31A
        jmp     LB1FC

LB326:
        cld
        clc
        adc     $17
        adc     #$02
        sta     $2D
        lda     #$00
        adc     $18
        sta     $2E
        sec
        lda     $2D
        sbc     $1B
        lda     $2E
        sbc     $1C
        bcc     LB34B
        brk
        .byte   $32
        bvs     LB34C
        cld
        sec
        sbc     #$01
        sta     $1C
        sta     $52
LB34B:
        clv
LB34C:
        rts

LB34D:
        lda     #$00
        sta     a:$14
        clc
        sed
        lda     $1D
        adc     $25
        sta     $31
        lda     $1E
        adc     $26
        sta     $32
        bcc     LB372
        lda     #$43
        sta     a:$14
        jsr     LBA77
        lda     $1D
        sta     $31
        lda     $1E
        sta     $32
LB372:
        tay
        lda     $31
        jsr     LB745
        sta     $1F
        sty     $20
        lda     $31
        ldy     $32
        sta     $1D
        sty     $1E
        bit     a:$14
        rts

LB388:
        lda     $40
        beq     LB39F
        brk
        ora     $45A9,x
        brk
        .byte   $12
        bvs     LB397
        jmp     LB498

LB397:
        lda     #$06
        jsr     LBA77
        jmp     LB1D6

LB39F:
        lda     #$FF
        sta     a:$3B
        lda     $27
        cmp     #$4D
        bne     LB3B2
        lda     #$01
        sta     $25
        lda     #$00
        sta     $26
LB3B2:
        brk
        ora     $01A9,x
        ldy     #$00
        sta     L000C
        sty     $0D
        brk
        .byte   $1B
        bvs     LB3C9
        lda     $27
        cmp     #$49
        bne     LB420
LB3C6:
        jmp     LB5F5

LB3C9:
        lda     #$49
        sta     $27
        lda     $0801
        sta     $3C
        brk
        stx     $66,y
        bcs     LB447
        .byte   $37
        asl     a
        tax
        lda     LB0E4,x
        sta     L0008
        lda     LB0E5,x
        sta     $09
        cpx     #$00
        beq     LB40C
        cpx     #$04
        bne     LB3F8
        lda     $57
        bne     LB3F8
        lda     $3C
        sta     $0801
        jmp     LB40F

LB3F8:
        brk
        ora     ($70),y
        bpl     LB3C6
        jsr     L06F0
        bit     EditorCmd
        jsr     LB439
LB406:
        lda     #$20
        brk
        .byte   $12
        bvc     LB406
LB40C:
        jmp     (L0008)

LB40F:
        jsr     LB8A0
        jsr     LB439
        lda     $0E
        sta     $1D
        lda     $0F
        sta     $1E
        jsr     LB735
LB420:
        lda     L000C
        sta     $25
        lda     $0D
        sta     $26
        lda     #$49
        sta     $27
        lda     a:$1E
        cmp     #$1F
        bne     LB436
        jmp     LB623

LB436:
        jmp     LB1D6

LB439:
        bvs     LB43C
        rts

LB43C:
        pla
        pla
        lda     #$A1
        jsr     LBA77
        jmp     LB1D6

        .byte   $A2
LB447:
        .byte   $FF
        brk
        .byte   $03
        bvc     LB44F
        jsr     LBA77
LB44F:
        jmp     LB1D6

        brk
        .byte   $1B
        jsr     LB439
        brk
        ldy     #$00
        php
        brk
        bpl     LB45E
LB45E:
        .byte   $27
        brk
        rol     $43
        .byte   $4F
        jsr     L3A31
        pha
        rol     L4445
        eor     #$D4
LB46C:
        lda     #$00
        brk
        .byte   $1C
        brk
        .byte   $03
        bvc     LB48A
        cmp     #$21
        beq     LB47C
        cmp     #$B0
        bne     LB4AE
LB47C:
        inc     $0806
        lda     $0806
        cmp     #$36
        bcc     LB46C
        lda     #$B0
        bne     LB4AE
LB48A:
        jmp     LB1D6

        brk
        .byte   $1B
        jsr     LB439
        jsr     LB831
        jmp     LB1D6

LB498:
        brk
        .byte   $1B
        jsr     LB439
LB49D:
        lda     $51
        bne     LB4B4
        lda     $34
        beq     LB4B4
        lda     $0A56
        cmp     #$1F
        beq     LB4B4
        lda     #$42
LB4AE:
        jsr     LBA77
        jmp     LB1D6

LB4B4:
        lda     #$01
        ldx     $40
        beq     LB4BC
        lda     #$06
LB4BC:
        brk
        ora     ($A9,x)
        .byte   $FF
        sta     a:$3D
        jsr     LB971
        jsr     LB439
        brk
        .byte   $1B
        jsr     LB439
        jsr     LBB0F
LB4D1:
        jsr     LB528
        bcs     LB4DA
        brk
        .byte   $07
        bvc     LB4DD
LB4DA:
        jmp     LB1D6

LB4DD:
        jsr     LB4E3
        jmp     LB4D1

LB4E3:
        jsr     LBA5B
        ldy     #$00
        lda     (L0008),y
        cmp     $1E
        bne     LB4FC
        iny
        lda     (L0008),y
        cmp     $1D
        bne     LB4FC
        brk
        rol     $2A
        ldy     #$4C
        brk
        .byte   $B5
LB4FC:
        brk
        .byte   $2B
        brk
        .byte   $2B
        ldy     #$01
        lda     (L0008),y
        tax
        dey
        lda     (L0008),y
        tay
        txa
        brk
        and     _ZERO_
        .byte   $2B
        ldy     #$00
        jsr     LB7D4
        jsr     LB7D4
        lda     (L0008),y
        cmp     #$0D
        beq     LB521
        brk
        and     ($4C,x)
        .byte   $13
        .byte   $B5
LB521:
        jsr     LB7D4
        jsr     LBA68
        rts

LB528:
        cld
        sec
        lda     L0008
        sbc     $02
        lda     $09
        sbc     $03
        rts

        brk
        .byte   $1B
        bvs     LB53D
        bit     EditorCmd
        jsr     LB439
LB53D:
        lda     #$00
        sta     a:$3D
        jsr     LB971
        jsr     LB439
        brk
        .byte   $1B
        jsr     LB439
        lda     #$FF
        sta     $34
        jsr     LB662
        lda     $0A56
        cmp     #$1F
        beq     LB55E
        jmp     LB1D6

LB55E:
        lda     #$00
        sta     $0959
        brk
        .byte   $33
        cld
        sec
        sbc     #$01
        sta     $1C
        sta     $52
        jmp     LB623

        lda     #$10
        sta     $0E
        sta     L000C
        lda     #$00
        sta     $0F
        sta     $0D
        brk
        .byte   $1B
        bvc     LB5AF
        lda     #$04
        brk
        .byte   $1A
        jsr     LB439
        sta     $0E
        sty     $0F
        sta     L000C
        sty     $0D
        brk
        ora     ($70),y
        .byte   $1C
        cmp     #$2C
        beq     LB59D
LB597:
        bit     EditorCmd
        jsr     LB439
LB59D:
        lda     #$04
        brk
        .byte   $1A
        bvs     LB597
        sta     L000C
        sty     $0D
        ora     $0D
        beq     LB597
        brk
        .byte   $1B
        bvs     LB597
LB5AF:
        jsr     LB7AD
LB5B2:
        ldy     #$00
        lda     (L0008),y
        cmp     #$1F
        beq     LB5EA
        lda     $0F
        sta     (L0008),y
        iny
        lda     $0E
        sta     (L0008),y
        jsr     LB7B6
        clc
        sed
        lda     $0E
        adc     L000C
        sta     $0E
        lda     $0F
        adc     $0D
        sta     $0F
        bcc     LB5B2
        lda     #$41
        jsr     LBA77
        lda     #$01
        ldy     #$00
        sta     $0E
        sty     $0F
        sta     L000C
        sty     $0D
        jmp     LB5AF

LB5EA:
        lda     #$FF
        sta     $34
        jmp     LB623

        brk
        .byte   $1B
        bvs     LB604
LB5F5:
        lda     #$4D
        sta     $27
        lda     $1F
        ldy     $20
        sta     L0008
        sty     $09
        jmp     LB2AE

LB604:
        jsr     LB8EC
        jsr     LB439
        brk
        .byte   $1B
        jsr     LB439
        ldx     #$4D
        stx     $27
        sta     $1F
        sty     $20
        sta     L0008
        sta     $09
        jmp     LB2AE

        brk
        .byte   $1B
        jsr     LB439
LB623:
        lda     #$49
        sta     $27
        lda     $17
        ldy     $18
        cmp     $19
        bne     LB63C
        cpy     $1A
        bne     LB63C
        lda     #$00
        sta     $1D
        sta     $1E
        jmp     LB657

LB63C:
        dey
        sta     $2D
        sty     $2E
        ldy     #$FE
LB643:
        lda     ($2D),y
        cmp     #$0D
        beq     LB64D
        dey
        jmp     LB643

LB64D:
        iny
        lda     ($2D),y
        sta     $1E
        iny
        lda     ($2D),y
        sta     $1D
LB657:
        lda     #$10
        sta     $25
        lda     #$00
        sta     $26
        jmp     LB2C3

LB662:
        lda     _ZERO_
        ldy     $01
        sta     $2D
        sty     $2E
        lda     $02
        ldy     $03
        sta     $2F
        sty     $30
        ldy     #$00
LB674:
        lda     ($2F),y
        sta     ($2D),y
        cmp     #$1F
        beq     LB686
        iny
        bne     LB674
        inc     $2E
        inc     $30
        jmp     LB674

LB686:
        clc
        cld
        tya
        adc     $2D
        sta     $17
        lda     #$00
        adc     $2E
        sta     $18
        sec
        lda     _ZERO_
        sbc     $1F
        lda     $01
        sbc     $20
        bcs     LB6C2
        sec
        lda     $02
        sbc     $1F
        lda     $03
        sbc     $20
        bcc     LB6C3
        lda     _ZERO_
        ldy     $01
        sta     $1F
        sty     $20
LB6B1:
        lda     $27
        cmp     #$4D
        bne     LB6C2
        ldy     #$01
        lda     ($1F),y
        sta     $1D
        dey
        lda     ($1F),y
        sta     $1E
LB6C2:
        rts

LB6C3:
        sec
        lda     $02
        sbc     _ZERO_
        sta     $2D
        lda     $03
        sbc     $01
        sta     $2E
        sec
        lda     $1F
        sbc     $2D
        sta     $1F
        lda     $20
        sbc     $2E
        sta     $20
        jmp     LB6B1

LB6E0:
        cld
        clc
        lda     $17
        sta     $2D
        adc     $04
        sta     $2F
        sta     $17
        lda     $18
        sta     $2E
        adc     $05
        sta     $30
        sta     $18
        ldy     #$00
LB6F8:
        lda     ($2D),y
        sta     ($2F),y
        lda     $2D
        bne     LB702
        dec     $2E
LB702:
        dec     $2D
        lda     $2F
        bne     LB70A
        dec     $30
LB70A:
        dec     $2F
        sec
        lda     $06
        sbc     $2D
        lda     $07
        sbc     $2E
        bcc     LB6F8
        lda     ($2D),y
        sta     ($2F),y
        cld
        sec
        lda     $06
        sbc     $1F
        lda     $07
        sbc     $20
        bcs     LB734
        clc
        lda     $1F
        adc     $04
        sta     $1F
        lda     $20
        adc     $05
        sta     $20
LB734:
        rts

LB735:
        lda     $1D
        ldy     $1E
        sta     $0A
        sty     $0B
        jsr     LB774
        sta     $1F
        sty     $20
        rts

LB745:
        sta     $0A
        sty     $0B
        cmp     $1D
        bne     LB75C
        cpy     $1E
        bne     LB75C
        lda     $1F
        ldy     $20
        sta     L0008
        sty     $09
        jmp     LB794

LB75C:
        sed
        sec
        lda     $0A
        sbc     $1D
        lda     $0B
        sbc     $1E
        bcc     LB774
        lda     $1F
        ldy     $20
        sta     L0008
        sty     $09
        sed
        jmp     LB778

LB774:
        jsr     LB7AD
        sed
LB778:
        ldy     #$00
        lda     (L0008),y
        cmp     #$1F
        beq     LB7A9
        ldy     #$01
        sec
        lda     (L0008),y
        sbc     $0A
        dey
        lda     (L0008),y
        sbc     $0B
        bcs     LB794
        jsr     LB7B6
        jmp     LB778

LB794:
        ldy     #$00
        lda     (L0008),y
        cmp     $0B
        bne     LB7A9
        iny
        lda     (L0008),y
        cmp     $0A
        bne     LB7A9
        clc
LB7A4:
        lda     L0008
        ldy     $09
        rts

LB7A9:
        sec
        jmp     LB7A4

LB7AD:
        lda     $19
        sta     L0008
        lda     $1A
        sta     $09
        rts

LB7B6:
        ldy     #$00
        lda     (L0008),y
        cmp     #$1F
        beq     LB7CF
        jsr     LB7D4
LB7C1:
        jsr     LB7D4
        ldy     #$00
        lda     (L0008),y
        cmp     #$0D
        bne     LB7C1
        jsr     LB7D4
LB7CF:
        lda     L0008
        ldy     $09
        rts

LB7D4:
        inc     L0008
        bne     LB7DA
        inc     $09
LB7DA:
        rts

LB7DB:
        lda     #$00
        sta     $0A
        sta     $0B
        jsr     LB7AD
LB7E4:
        ldy     #$00
        lda     (L0008),y
        cmp     #$1F
        beq     LB800
        clc
        sed
        lda     $0A
        adc     #$01
        sta     $0A
        lda     $0B
        adc     #$00
        sta     $0B
        jsr     LB7B6
        jmp     LB7E4

LB800:
        rts

LB801:
        jsr     LBA5B
        lda     $27
        brk
        and     (_ZERO_,x)
        .byte   $2B
        lda     $1D
        ldy     $1E
        brk
        and     _ZERO_
        .byte   $2B
        brk
        ldy     #$00
        php
        brk
        .byte   $27
        lda     $27
        cmp     #$49
        beq     LB830
        ldy     #$02
LB820:
        lda     ($1F),y
        cmp     #$0D
        beq     LB82C
        brk
        and     ($C8,x)
        jmp     LB820

LB82C:
        dey
        dey
        sty     $33
LB830:
        rts

LB831:
        jsr     LB7DB
        jsr     LBA5B
        lda     #$20
        ldx     #$07
        brk
        bit     $0AA5
        ldy     $0B
        brk
        and     _ZERO_
        rol     $20
        jmp     L4E49

        eor     $53
        jsr     L38A8
        cld
        lda     $17
        sbc     $19
        lda     $18
        sbc     $1A
        tay
        iny
        tya
        ldy     #$00
        brk
        asl     $85
        .byte   $2B
        tya
        brk
        and     $2BA5
        brk
        .byte   $23
        brk
        rol     $20
        bvc     LB8AD
        .byte   $47
        eor     $53
        jsr     L5355
        eor     $44
        lda     #$AD
        eor     $F009,y
        .byte   $22
        brk
        rol     $20
        rol     $2E2E
        jsr     L4946
        jmp     L4E45

        eor     ($4D,x)
        eor     $20
        and     LAEA0,x
        lda     ($09,x)
        inx
        txa
        brk
        and     $2600
        tsx
        ldx     #$20
        brk
        lda     $59,x
        ora     #$20
        pla
        tsx
        rts

LB8A0:
        jsr     LB8EC
        bvs     LB8E8
        sta     $2D
        sty     $2E
        lda     $35
        cmp     #$FF
LB8AD:
        beq     LB8B8
        ldy     $36
        sta     $0E
        sty     $0F
        jmp     LB8C3

LB8B8:
        ldy     #$00
        lda     ($2D),y
        sta     $0F
        iny
        lda     ($2D),y
        sta     $0E
LB8C3:
        lda     #$01
        sta     L000C
        lda     #$00
        sta     $0D
        lda     #$2C
        brk
        .byte   $12
        bvc     LB8D8
        cmp     #$0D
        beq     LB8E6
        jmp     LB8E8

LB8D8:
        lda     #$04
        brk
        .byte   $1A
        bvs     LB8E8
        brk
        .byte   $1B
        bvs     LB8E8
        sta     L000C
        sty     $0D
LB8E6:
        clv
        rts

LB8E8:
        bit     EditorCmd
        rts

LB8EC:
        lda     #$FF
        sta     $35
        brk
        stx     $04,y
        bcs     LB965
        .byte   $5B
        tax
        beq     LB94A
        cpx     #$01
        bne     LB918
        ldy     #$00
        lda     ($1F),y
        cmp     a:$1E
        bne     LB911
        iny
        lda     ($1F),y
        cmp     a:$1D
        bne     LB911
        clc
        bcc     LB912
LB911:
        sec
LB912:
        lda     $1F
        ldy     $20
        clv
        rts

LB918:
        lda     $1F
        ldy     $20
        cmp     $19
        bne     LB927
        cpy     $1A
        bne     LB927
        clv
        sec
        rts

LB927:
        dey
        sta     $2D
        sty     $2E
        ldy     #$FE
LB92E:
        lda     ($2D),y
        cmp     #$0D
        beq     LB938
        dey
        jmp     LB92E

LB938:
        iny
        clc
        cld
        tya
        adc     $2D
        sta     $2D
        lda     $2E
        adc     #$00
        tay
        lda     $2D
        clv
        clc
        rts

LB94A:
        lda     $17
        ldy     $18
        sec
        clv
        rts

        lda     #$04
        brk
        .byte   $1A
        bvs     LB95F
        sta     $35
        sty     $36
        jsr     LB745
        clv
LB95F:
        rts

LB960:
        lda     $19
        ldy     $1A
        .byte   $85
LB965:
        brk
        sty     $01
        lda     $17
        ldy     $18
        sta     $02
        sty     $03
        rts

LB971:
        jsr     LB960
        brk
        stx     $0E,y
        bcs     LB9C9
        .byte   $54
        jsr     LB8EC
        bvs     LB9CE
        sta     _ZERO_
        sty     $01
        php
        pla
        sta     $2B
        brk
        .byte   $1B
        bvc     LB9B3
        lda     #$2D
        brk
        .byte   $12
        bvs     LB9B3
        lda     a:$3D
        beq     LB9A2
        lda     #$FF
        brk
        .byte   $12
        cmp     #$0D
        beq     LB9CE
        cmp     #$2C
        beq     LB9CE
LB9A2:
        jsr     LB8EC
        sta     $2C
        php
        pla
        sta     $2B
        lda     $2C
        bvc     LB9B7
LB9AF:
        bit     EditorCmd
        rts

LB9B3:
        lda     _ZERO_
        ldy     $01
LB9B7:
        sta     $02
        sty     $03
        lsr     $2B
        bcs     LB9CE
        sta     L0008
        sty     $09
        jsr     LB7B6
        lda     L0008
        .byte   $85
LB9C9:
        .byte   $02
        lda     $09
        sta     $03
LB9CE:
        lda     _ZERO_
        ldy     $01
        sec
        cld
        lda     $02
        sbc     _ZERO_
        lda     $03
        sbc     $01
        bcc     LB9AF
        clv
        rts

LB9E0:
        lda     $27
        cmp     #$4D
        beq     LBA3B
        lda     $1D
        sta     L000C
        lda     $1E
        sta     $0D
        lda     $1F
        sta     L0008
        lda     $20
        sta     $09
LB9F6:
        ldy     #$00
        lda     (L0008),y
        cmp     #$1F
        beq     LBA26
        cmp     $0D
        bne     LBA26
        iny
        lda     (L0008),y
        cmp     L000C
        bne     LBA26
        clc
        sed
        lda     L000C
        adc     #$01
        sta     L000C
        lda     $0D
        adc     #$00
        sta     $0D
        lda     L000C
        sta     (L0008),y
        dey
        lda     $0D
        sta     (L0008),y
        jsr     LB7B6
LBA23:
        jmp     LB9F6

LBA26:
        lda     $1F
        sta     $06
        lda     $20
        sta     $07
        lda     #$00
        sta     $05
        inc     $04
        inc     $04
        inc     $04
        jsr     LB6E0
LBA3B:
        ldy     #$00
        lda     $1E
        sta     ($1F),y
        iny
        lda     $1D
        sta     ($1F),y
        iny
        ldx     $0801
LBA4A:
        lda     $0800,x
        sta     ($1F),y
        iny
        inx
        cpx     $0802
        bcc     LBA4A
        lda     #$0D
        sta     ($1F),y
        rts

LBA5B:
        sty     $29
        brk
        ldy     #$06
        ora     #$00
        .byte   $27
        brk
        .byte   $2B
        ldy     $29
        rts

LBA68:
        sty     $29
        stx     $28
        ldx     #$45
        brk
LBA6F:
        txa
        bpl     LBA23
        ldy     $29
        ldx     $28
        rts

LBA77:
        cmp     #$A1
        bne     LBA9D
        jsr     LBA5B
        lda     #$20
        ldx     #$07
        brk
        bit     $01AD
        php
        cmp     #$03
        beq     LBA9B
        cld
        sec
        sbc     #$03
        tax
        lda     #$2E
        brk
        bit     $5EA9
        brk
        and     ($20,x)
        pla
        tsx
LBA9B:
        lda     #$A1
LBA9D:
        tax
        jsr     LBA5B
        brk
        tax
        .byte   $14
        bcs     LBAC6
        pla
        tsx
        lda     a:$51
        sta     $40
        rts

LBAAE:
        pha
        ldx     #$7D
        lda     #$00
LBAB3:
        sta     $09D7,x
        dex
        bpl     LBAB3
        pla
        tax
        brk
        tya
        .byte   $D7
        ora     #$70
        bpl     LBA6F
        .byte   $17
        asl     a
        cmp     #$03
LBAC6:
        bne     LBAD1
        lda     $0A1F
        cmp     #$FF
        beq     LBAD4
        clv
        rts

LBAD1:
        lda     #$A1
        rts

LBAD4:
        lda     #$21
        bit     EditorCmd
        rts

LBADA:
        lda     $0A02
        cmp     #$FF
        bne     LBAEE
        ldx     #$7D
LBAE3:
        lda     $09D7,x
        sta     $0959,x
        dex
        bpl     LBAE3
        clv
        rts

LBAEE:
        lda     #$00
        sta     $0959
        bit     EditorCmd
        rts

LBAF7:
        lda     $0959
        bne     LBB02
        lda     #$21
        bit     EditorCmd
        rts

LBB02:
        ldx     #$7D
LBB04:
        lda     $0959,x
        sta     $09D7,x
        dex
        bpl     LBB04
        clv
        rts

LBB0F:
        lda     a:_ZERO_
        sta     a:L0008
        lda     a:$01
        sta     a:$09
        rts

LBB1C:
        jsr     LBB0F
LBB1F:
        jsr     LB528
        bcs     LBB28
        brk
        .byte   $07
        bvc     LBB2B
LBB28:
        jmp     LB1D6

LBB2B:
        lda     L0008
        sta     $0A
        lda     $09
        sta     $0B
        jsr     LB7D4
LBB36:
        ldy     #$00
        jsr     LB7D4
        lda     $10
        beq     LBB5A
LBB3F:
        lda     (L0008),y
        cmp     #$0D
        bne     LBB50
LBB45:
        jsr     LB7D4
        cpy     #$00
        beq     LBB1F
        dey
        jmp     LBB45

LBB50:
        cmp     $69,y
        bne     LBB36
        iny
        cpy     $10
        bne     LBB3F
LBB5A:
        jsr     LBB60
        jmp     LBB1F

LBB60:
        jmp     (L000C)

        ldx     #$20
        brk
        sta     $69,y
        jsr     LB439
        stx     $10
        brk
        .byte   $1B
        bvc     LBB83
        lda     #$2C
        brk
        .byte   $12
        jsr     LB439
        brk
        .byte   $1B
        bvs     LBB83
        bit     EditorCmd
        jsr     LB439
LBB83:
        lda     #$FF
        sta     a:$3D
        jsr     LB971
        jsr     LB439
        brk
        .byte   $1B
        jsr     LB439
        jsr     LBCE8
        lda     #$A1
        sta     L000C
        lda     #$BB
        sta     $0D
        jmp     LBB1C

        lda     a:$0A
        sta     a:L0008
        lda     a:$0B
        sta     a:$09
        jmp     LB4E3

        lda     #$FD
        sta     $3F
        ldx     #$20
        brk
        sta     $69,y
LBBBA:
        jsr     LB439
        stx     $10
        bit     EditorCmd
        lda     #$2C
        brk
        .byte   $12
        jsr     LB439
        ldx     #$20
        brk
        sta     $89,y
        bvs     LBBBA
        stx     $11
        cpx     $10
        bcs     LBBE0
        cld
        sec
        txa
        sbc     $10
        sbc     #$03
        sta     $3F
LBBE0:
        brk
        .byte   $1B
        bvc     LBBF5
        lda     #$2C
        brk
        .byte   $12
        jsr     LB439
        brk
        .byte   $1B
        bvs     LBBF5
        bit     EditorCmd
        jsr     LB439
LBBF5:
        lda     #$00
        sta     a:$3D
        jsr     LB971
        jsr     LB439
        brk
        .byte   $1B
        jsr     LB439
        jsr     LBCE8
        lda     $02
        sta     $12
        lda     $03
        sta     $13
        lda     #$27
        sta     L000C
        lda     #$BC
        sta     $0D
        lda     #$FF
        sta     $34
        jmp     LBB1C

LBC1F:
        jsr     LBA77
        pla
        pla
        jmp     LB1D6

        lda     $3F
        jsr     LB326
        bvs     LBC1F
        lda     $0A
        sta     $15
        lda     $0B
        sta     $16
        lda     L0008
        sta     _ZERO_
        sta     $06
        lda     $09
        sta     $01
        sta     $07
        cld
        clc
        lda     $10
        adc     L0008
        sta     $02
        lda     $09
        adc     #$00
        sta     $03
        lda     $10
        beq     LBC57
        jsr     LB662
LBC57:
        sec
        cld
        lda     $12
        sbc     $10
        sta     $12
        lda     $13
        sbc     #$00
        sta     $13
        lda     $11
        beq     LBC93
        sta     $04
        lda     #$00
        sta     $05
        lda     L0008
        sta     _ZERO_
        lda     $09
        sta     $01
        jsr     LB6E0
        clc
        cld
        lda     $12
        adc     $11
        sta     $12
        lda     $13
        adc     #$00
        sta     $13
        ldy     $11
        dey
LBC8B:
        lda     $89,y
        sta     (_ZERO_),y
        dey
        bpl     LBC8B
LBC93:
        lda     $15
        sta     L0008
        lda     $16
        sta     $09
        lda     $12
        sta     $02
        lda     $13
        sta     $03
        ldy     #$02
        lda     (L0008),y
        cmp     #$0D
        bne     LBCE5
        jsr     LBA5B
        brk
        rol     $44
        ldy     #$A0
        ora     ($B1,x)
        php
        tax
        dey
        lda     (L0008),y
        tay
        txa
        brk
        and     $20
        pla
        tsx
        clc
        cld
        lda     L0008
        sta     _ZERO_
        adc     #$03
        sta     $02
        lda     $09
        sta     $01
        adc     #$00
        sta     $03
        jsr     LB662
        sec
        cld
        lda     $12
        sbc     #$03
        sta     $02
        lda     $13
        sbc     #$00
        sta     $03
        rts

LBCE5:
        jmp     LB4E3

LBCE8:
        ldx     #$1F
LBCEA:
        lda     $69,x
        and     #$7F
        sta     $69,x
        lda     $89,x
        and     #$7F
        sta     $89,x
        dex
        bpl     LBCEA
        rts

        lda     #$02
        jsr     LBAAE
        bvc     LBD07
        jsr     LBA77
        jmp     LB1D6

LBD07:
        brk
        .byte   $1B
        jsr     LB439
        jsr     LBADA
        jsr     LB439
        jmp     LB1D6

        brk
        .byte   $1B
        jsr     LB439
        jsr     LBAF7
        bvs     LBD2F
        lda     #$02
        sta     $2A
        lda     #$FF
        sta     $3A
        jsr     LB960
        jsr     LBE31
        bvc     LBD35
LBD2F:
        jsr     LBA77
        jmp     LB1D6

LBD35:
        lda     #$00
        sta     a:$34
        jmp     LB49D

        lda     #$FF
        jmp     LBD44

        lda     #$00
LBD44:
        sta     $3A
        lda     #$02
        sta     $2A
        brk
        .byte   $1B
        bvs     LBD69
        jsr     LBAF7
        bvs     LBD63
        jsr     LB960
        jsr     LBE31
        bvs     LBD63
        lda     #$00
        sta     a:$34
        jmp     LB1D6

LBD63:
        jsr     LBA77
        jmp     LB1D6

LBD69:
        lda     #$02
        jsr     LBAAE
        bvc     LBD76
        jsr     LBA77
        jmp     LB1D6

LBD76:
        lda     #$00
        sta     a:$39
        brk
        .byte   $1B
        bvc     LBD8C
        lda     a:$34
        sta     a:$39
        lda     #$2C
        brk
        .byte   $12
        jsr     LB439
LBD8C:
        lda     #$00
        sta     a:$3D
        jsr     LB971
        jsr     LB439
        brk
        .byte   $1B
        jsr     LB439
        jsr     LBE31
        bvc     LBDA4
        jsr     LBA77
LBDA4:
        lda     a:$39
        sta     a:$34
        jmp     LB1D6

        lda     #$00
        sta     $3A
        lda     #$01
        sta     $2A
        jsr     LBAAE
        bvc     LBDC0
        jsr     LBA77
        jmp     LB1D6

LBDC0:
        lda     #$FF
        sta     $3E
        lda     #$00
        sta     $35
        sta     $36
        lda     #$99
        sta     $37
        sta     $38
        brk
        .byte   $1B
        bvc     LBE05
        lda     #$2C
        brk
        .byte   $12
        jsr     LB439
        lda     #$04
        brk
        .byte   $1A
        jsr     LB439
        sta     $35
        sty     $36
        brk
        ora     ($C9),y
        and     $13D0
        lda     #$04
        brk
        .byte   $1A
        jsr     LB439
        sta     $37
        sty     $38
        lda     #$00
        sta     $3E
        brk
        .byte   $1B
        bvc     LBE05
        bit     EditorCmd
        jsr     LB439
LBE05:
        lda     $0A56
        cmp     #$1F
        beq     LBE1C
        jsr     LBE31
        bvc     LBE14
        jsr     LBA77
LBE14:
        lda     #$FF
        sta     a:$34
        jmp     LB1D6

LBE1C:
        jsr     LBF51
        bvc     LBE29
        jsr     LBA77
        lda     #$00
        sta     $0959
LBE29:
        lda     #$00
        sta     a:$34
        jmp     LB623

LBE31:
        lda     #$00
        sta     $0A18
        lda     #$08
        sta     $0A19
        lda     $2A
        cmp     #$01
        beq     LBE4F
        lda     #$01
        ldy     $09D7
        cpy     #$4C
        bne     LBE4C
        lda     #$02
LBE4C:
        sta     $0A05
LBE4F:
        lda     #$83
        sta     $0800
        lda     $0A45
        bne     LBE5E
        lda     $3A
        sta     $0A45
LBE5E:
        ldx     $2A
        brk
        .byte   $C2
        .byte   $D7
        ora     #$70
        eor     $2AA5,x
        cmp     #$01
        beq     LBEC7
        jsr     LBB0F
LBE6F:
        jsr     LB528
        bcs     LBEAC
        ldx     #$03
        ldy     #$00
        lda     $09D7
        cmp     #$4C
        bne     LBE85
        jsr     LB7D4
        jsr     LB7D4
LBE85:
        lda     (L0008),y
        cmp     #$0D
        beq     LBE99
        cpx     #$83
        bcs     LBE93
        sta     $0800,x
        inx
LBE93:
        jsr     LB7D4
        jmp     LBE85

LBE99:
        jsr     LB7D4
        stx     $0802
        brk
        cmp     $D7
        ora     #$70
        .byte   $02
        brk
        rti

        bvs     LBEB4
        jmp     LBE6F

LBEAC:
        brk
        .byte   $C3
        .byte   $D7
        ora     #$70
        .byte   $02
        clv
        rts

LBEB4:
        pha
        lda     #$00
        sta     $0A45
        brk
        .byte   $C3
        .byte   $D7
        ora     #$00
        .byte   $DB
        .byte   $D7
        ora     #$68
        bit     EditorCmd
        rts

LBEC7:
        lda     $0A05
        cmp     #$02
        beq     LBEDA
        lda     #$00
        sta     $3E
        lda     #$01
        sta     $0A44
        jmp     LBEE8

LBEDA:
        lda     #$02
        sta     $0A44
        lda     $3E
        bne     LBEE8
        lda     #$A1
LBEE5:
        jmp     LBF6D

LBEE8:
        brk
        cpy     $D7
        ora     #$70
        .byte   $02
        brk
        rti

        bvs     LBEE5
        brk
        bcc     LBEF5
LBEF5:
        php
        lda     #$00
        brk
        .byte   $1C
        cmp     #$00
        beq     LBEAC
        sta     $04
        lda     $3E
        bne     LBF37
        sed
        sec
        lda     $0804
        sbc     $35
        lda     $0803
        sbc     $36
        bcc     LBEE8
        sec
        lda     $37
        sbc     $0804
        lda     $38
        sbc     $0803
        bcc     LBEAC
        ldy     #$03
LBF21:
        lda     $0802,y
        sta     $0800,y
        iny
        cpy     $0802
        bcc     LBF21
        dec     $0802
        dec     $0802
        dec     $04
        dec     $04
LBF37:
        lda     $04
        jsr     LB326
        bvc     LBF41
        jmp     LBF6D

LBF41:
        jsr     LB9E0
        jsr     LB34D
        bvc     LBEE8
        lda     #$D7
        ldy     #$09
        brk
        .byte   $43
        clv
        rts

LBF51:
        lda     #$00
        ldy     #$08
        sta     $0A18
        sty     $0A19
        lda     #$01
        sta     $0A44
        lda     #$83
        sta     $0800
        ldx     #$01
        brk
        .byte   $C2
        .byte   $D7
        ora     #$50
        asl     a
LBF6D:
        pha
        brk
        .byte   $C3
        .byte   $D7
        ora     #$68
        bit     EditorCmd
        rts

        lda     $0A05
        cmp     #$02
        bne     LBF81
        jmp     LBEC7

LBF81:
        brk
        cpy     $D7
        ora     #$70
        .byte   $02
        brk
        rti

        bvs     LBF6D
        brk
        bcc     LBF8E
LBF8E:
        php
        lda     #$00
        brk
        .byte   $1C
        sta     $04
        cmp     #$00
        bne     LBF9E
LBF99:
        brk
        .byte   $C3
        .byte   $D7
        ora     #$60
LBF9E:
        sed
        sec
        lda     $0804
        sbc     $35
        lda     $0803
        sbc     $36
        bcc     LBF81
        sec
        lda     $37
        sbc     $0804
        lda     $38
        sbc     $0803
        bcc     LBF99
        dec     $04
        dec     $04
        lda     $04
        jsr     LB326
        bvs     LBF6D
        ldy     #$00
        ldx     $0801
LBFC9:
        lda     $0800,x
        sta     ($17),y
        iny
        inx
        cpx     $0802
        bcc     LBFC9
        lda     #$0D
        sta     ($17),y
        iny
        lda     #$1F
        sta     ($17),y
        clc
        cld
        tya
        adc     $17
        sta     $17
        lda     $18
        adc     #$00
        sta     $18
        jmp     LBF81

        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        rts

