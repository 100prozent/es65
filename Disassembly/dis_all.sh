#!/bin/bash

cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "P0.asm";
    INPUTNAME "../ROMS/ROM_Board/P0.bin";
    STARTADDR \$A000;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
cat P0.loc >> infile.in
cat common.loc >> infile.in
./da65 -i infile.in
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "P1.asm";
    INPUTNAME "../ROMS/ROM_Board/P1.bin";
    STARTADDR \$A000;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
cat P1.loc >> infile.in
cat common.loc >> infile.in
./da65 -i infile.in
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "P2.asm";
    INPUTNAME "../ROMS/ROM_Board/P2.bin";
    STARTADDR \$A000;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
cat P2.loc >> infile.in
cat common.loc >> infile.in
./da65 -i infile.in
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "P3.asm";
    INPUTNAME "../ROMS/ROM_Board/P3.bin";
    STARTADDR \$A000;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
cat P3.loc >> infile.in
cat common.loc >> infile.in
./da65 -i infile.in
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "tgm.asm";
    INPUTNAME "../ROMS/MUC_Board_TGM/TGM_lo.bin";
    STARTADDR \$FC00;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
#cat common.loc >> infile.in
cat TGM.loc >> infile.in
cat vectors.loc >> infile.in
./da65 -i infile.in
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "muc.asm";
    INPUTNAME "../ROMS/MUC_Board_Beranek/MUC_Beranek_hi.bin";
    STARTADDR \$FC00;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
#cat common.loc >> infile.in
cat MUC_Beranek.loc >> infile.in
cat vectors.loc >> infile.in
./da65 -i infile.in
#exit
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "muc_bootstrap.asm";
    INPUTNAME "../ROMS/MUC_Board_Beranek/MUC_Beranek_lo.bin";
    STARTADDR \$FC00;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
#cat common.loc >> infile.in
cat MUC_Bootstrap.loc >> infile.in
#cat vectors.loc >> infile.in
./da65 -i infile.in
#----------------------------------------------------------
cat > infile.in <<EOF
GLOBAL {
    OUTPUTNAME "V65_all.asm";
    INPUTNAME "../ROMS/RAM_ROM_Board/V65_all.bin";
    STARTADDR \$8000;
    PAGELENGTH 0;
    CPU "6502";
    LABELBREAK 0;
};
EOF
cat Peripherals.loc >> infile.in
cat v65.loc >> infile.in
cat common.loc >> infile.in
cat vectors.loc >> infile.in
./da65 -i infile.in

rm infile.in