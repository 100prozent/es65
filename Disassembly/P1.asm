; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/ROM_Board/P1.bin
; Page:       1


        .setcpu "6502"

_ZERO_          := $0000                        ; 0x0000
POIN_F0         := $00F0
POINC1_F4       := $00F4
POINC2_F6       := $00F6
POINI_FA        := $00FA
InterruptControl_Reg:= $00FE
StackTail_211   := $0211
JCSREG_0212     := $0212
JumpAddrL_221   := $0221
JumpAddrH_222   := $0222
BankTemp_22D    := $022D
BRK_TRAP_22F    := $022F
BrkSRC_ROM_23C_FF:= $023C
PANIC_SRCL_25F  := $025F
PANIC_SRCH_260  := $0260
Status_Serial2_Backup_03e5:= $03E5
UART_Flags_03e7 := $03E7
StoreA_in_SerialCommandReg:= $03E8
A_from_to_SerialRX_TX_Reg:= $03EC
SpecialCharacters_Copy:= $03F0
FunctionByte_afterBRK_728:= $0728
L072C           := $072C
L2020           := $2020
L2300           := $2300
L2620           := $2620
L2700           := $2700
L4F46           := $4F46
L5020           := $5020
L5526           := $5526
L5826           := $5826
Vector_CALL     := $801E                        ; 0x801e
Vector_CREATE   := $8021                        ; 0x8021
Vector_DISMOUNT := $8024                        ; 0x8024
Vector_DRIVE    := $8027                        ; 0x8027
Vector_FHIRQRST := $802A                        ; 0x802A
Vector_FHIRQRW  := $802D                        ; 0x802D
Vector_FHIRQSK  := $8030                        ; 0x8030
Vector_FHNMIADR := $8033                        ; 0x8033
Vector_FHNMIRW  := $8036                        ; 0x8036
Vector_LOCK     := $8039                        ; 0x8039
Vector_LOGON    := $803C                        ; 0x803c
Vector_LODPOIS2 := $803F                        ; 0x803F
Vector_MYINIT   := $8042                        ; 0x8042
Vector_OUTAUS   := $8045                        ; 0x8045
Vector_OUTVOR   := $8048                        ; 0x8048
Vector_PROGTERM := $804B                        ; 0x804b
Vector_SCHEDUL  := $804E                        ; 0x804e
Vector_SVC60    := $8051                        ; 0x8051
Vector_SVC61    := $8054                        ; 0x8054
Vector_SVC62    := $8057                        ; 0x8057
Vector_SVC63    := $805A                        ; 0x805a
Vector_SVC64    := $805D                        ; 0x805d
Vector_SVC65    := $8060                        ; 0x8060
Vector_TASKTERM := $8063                        ; 0x8063
Vector_THPRMPTE := $8066                        ; 0x8066
Vector_THWRITEE := $8069                        ; 0x8069
Vector_LOFRUP   := $806C                        ; 0x806c
Vector_SVC0D    := $806F                        ; 0x806f
LF085           := $F085
PORT_B_Shadow_f3e8:= $F3E8
PH3TSK_F400_PRINTER_BUSY:= $F400
C3USAD_F41A     := $F41A
C3USAD_P3_F41D  := $F41D
C3ENT_F420      := $F420
Users_Installed_F421:= $F421                    ; 0xf421
C3USNR_F422     := $F422
SingleMultiMode_f423:= $F423
C3DATE_F424     := $F424                        ; 0xf424
C3TIME_F42E     := $F42E                        ; 0xf42e
C3TI20_F436     := $F436                        ; 0xf436
Bank_Shadow_F438:= $F438
C3LGNZ_F43C_USERS:= $F43C
MY3MAP_F448     := $F448
MY3MAP_P1_F449  := $F449
MY3MAP_P2_F44A  := $F44A
MY3MAP_P3_F44B  := $F44B
LF47F           := $F47F
LF481           := $F481
IPSW_F4AB       := $F4AB
IPANZ_F4AC_INIT_IPC:= $F4AC
IPADR_F4AE      := $F4AE
IPADR_P1_F4AF   := $F4AF
NMIURS_M1       := $F7FE
NMIURS          := $F7FF
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
Floppy_DriveSelect:= $FC11                      ; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
LFFFE           := $FFFE
Vector_A000:
        jmp     LA02A

Vector_A003:
        jmp     INIT_TERMINAL

Vector_A006:
        jmp     INIT_PRINTER

Vector_A009:
        jmp     INIT_FLOPPY

Vector_A00c:
        jmp     LA400

Vector_A00f:
        jmp     LA9F8

Vector_A012:
        jmp     Command_Copy

Vector_A015:
        jmp     LB8FF

Vector_A018:
        jmp     LAFAE

Vector_A01b:
        jmp     LAFBA

Vector_A01e:
        jmp     LB25E

Vector_A021:
        jmp     LB35F

Vector_A024:
        jmp     LB5FC

Vector_A027:
        jmp     LB0E4

LA02A:
        ldy     #$40
        lda     ($F8),y
        asl     a
        tax
        lda     LA03D,x
        sta     POIN_F0
        lda     LA03D+1,x
        sta     POIN_F0+1
        jmp     (POIN_F0)

LA03D:
        .addr   LA045
        .addr   LA083
        .addr   LA491
        .addr   LAB3D
LA045:
        lda     #$77
        ldy     #$A0
LA049:
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     $075E
        lda     (POIN_F0),y
        tax
        iny
        lda     (POIN_F0),y
        sta     POIN_F0+1
        stx     POIN_F0
        clv
        jmp     (POIN_F0)

LA05E:
        lda     $075F
LA061:
        ldy     #$45
        sta     ($F8),y
        lda     #$01
LA067:
        ldy     #$46
        sta     ($F8),y
        clv
        rts

        lda     #$00
        jmp     LA061

        lda     #$02
        jmp     LA067

_A077:
        lsr     $6DA0,x
        ldy     #$72
        ldy     #$65
        ldy     #$72
        ldy     #$72
        .byte   $A0
LA083:
        lda     #$FA
        ldy     #$A2
        jmp     LA049

_A08a:
        asl     $03
        asl     $31
        rol     a
        jsr     L072C
        .byte   $02
        bvc     LA05E
        lda     #$12
        bne     LA067
        lda     #$00
LA09B:
        sta     FunctionByte_afterBRK_728
        lda     UART_Flags_03e7
        and     #$C0
        beq     LA0AA
        lda     #$C0
        jmp     LA067

LA0AA:
        lda     $0401
        sta     $0402
        lda     #$00
        sta     $0405
        sta     $040B
        jsr     LA1EA
        ldy     #$00
        lda     (POIN_F0),y
        cmp     #$51
        bcc     LA0C5
        lda     #$50
LA0C5:
        sta     $0323
        ldy     #$01
        lda     #$03
        sta     (POIN_F0),y
        lda     FunctionByte_afterBRK_728
        cmp     #$02
        beq     LA116
        lda     #$03
        sta     $0325
        sta     $0324
        lda     FunctionByte_afterBRK_728
        beq     LA0F0
LA0E2:
        ldy     #$43
        lda     ($F8),y
        sta     POIN_F0
        iny
        lda     ($F8),y
        sta     POIN_F0+1
        jmp     LA0F8

LA0F0:
        lda     #$8A
        sta     POIN_F0
        lda     #$A0
        sta     POIN_F0+1
LA0F8:
        jsr     LA2DB
        lda     #$66
        ldy     #$80
        jsr     LA1F6
        lda     $040D
        cmp     #$30
        bne     LA10C
        sta     $040A
LA10C:
        lda     UART_Flags_03e7
        and     #$FE
        sta     UART_Flags_03e7
        clv
        rts

LA116:
        ldy     #$02
        lda     (POIN_F0),y
        sta     $0325
        sta     $0324
        cmp     #$03
        beq     LA0E2
        ldy     #$03
LA126:
        lda     (POIN_F0),y
        and     #$7F
        sta     FunctionByte_afterBRK_728
        lda     $0323,y
        and     #$80
        ora     FunctionByte_afterBRK_728
        sta     $0323,y
        iny
        cpy     $0323
        bcs     LA143
        cpy     $0324
        bcc     LA126
LA143:
        sty     $0325
        jmp     LA0E2

        lda     #$01
        jmp     LA09B

        lda     #$02
        jmp     LA09B

        lda     UART_Flags_03e7
        and     #$C0
        beq     LA15F
        lda     #$C0
        jmp     LA067

LA15F:
        jsr     LA1EA
        lda     #$03
        sta     $040C
        ldy     #$2E
        lda     ($F8),y
        cmp     #$02
        bne     LA172
        jsr     LA2DB
LA172:
        lda     $0403
        bne     LA188
        ldy     #$02
        lda     (POIN_F0),y
        cmp     $040C
        bne     LA188
        lda     #$01
        ldy     #$46
        sta     ($F8),y
        clv
        rts

LA188:
        lda     $0404
        beq     LA1A3
        lda     $F435
        sta     $0407
LA193:
        brk
        .byte   $04
        lda     $F435
        cmp     $0407
        beq     LA193
        lda     $0401
        sta     $0402
LA1A3:
        lda     $0405
        beq     LA1BE
        lda     #$8D
        sta     A_from_to_SerialRX_TX_Reg
        lda     #$07
        jsr     A_from_to_SerialRX_TX_Reg
        lda     #$05
        ldy     #$04
        brk
        ora     $AD
        ora     ($04,x)
        sta     $0402
LA1BE:
        lda     $0402
        bmi     LA1D4
        lda     $040A
        bne     LA1D4
        dec     $0402
        bne     LA1D4
        lda     #$FF
        sta     $0405
        bne     LA1A3
LA1D4:
        jsr     LA1EA
        lda     #$69
        ldy     #$80
        jsr     LA1F6
        lda     $040D
        cmp     #$30
        bne     LA1E8
        sta     $040A
LA1E8:
        clv
        rts

LA1EA:
        ldy     #$41
        lda     ($F8),y
        sta     POIN_F0
        iny
        lda     ($F8),y
        sta     POIN_F0+1
        rts

LA1F6:
        sta     $03E1
        sty     $03E2
        lda     $F8
        ldy     $F9
        sta     $03E3
        sty     $03E4
        ldy     #$02
        lda     (POIN_F0),y
        sta     FunctionByte_afterBRK_728
        cld
        sec
        sbc     $040C
        cmp     #$4F
        bcc     LA21F
        lda     $040C
        clc
        adc     #$4F
        sta     FunctionByte_afterBRK_728
LA21F:
        ldx     #$03
        lda     #$0D
        sta     $037A,x
        inx
        lda     $040A
        bne     LA232
        lda     #$0A
        sta     $037A,x
        inx
LA232:
        lda     #$00
        sta     $040A
        lda     FunctionByte_afterBRK_728
        cmp     $040C
        beq     LA25B
        ldy     $040C
LA242:
        lda     (POIN_F0),y
        and     #$7F
        cmp     #$7F
        bne     LA24C
        lda     #$5F
LA24C:
        sta     $037A,x
        iny
        inx
        cpy     FunctionByte_afterBRK_728
        beq     LA25B
        cpx     $037A
        bcc     LA242
LA25B:
        stx     $037C
        lda     #$03
        sta     $037B
        lda     $0200
        ldy     $0201
        sta     $F2
        sty     $F3
        ldy     #$40
        lda     ($F2),y
        beq     LA2A5
        lda     $F8
        cmp     $0200
        bne     LA281
        lda     $F9
        cmp     $0201
        beq     LA2A5
LA281:
        ldx     $02D3
        cpx     $02D1
        bcs     LA2A5
        ldy     $040C
        cpy     FunctionByte_afterBRK_728
        beq     LA2A5
LA291:
        lda     (POIN_F0),y
        sta     $02D1,x
        inx
        iny
        cpy     FunctionByte_afterBRK_728
        .byte   $B0
LA29C:
        ora     $EC
        cmp     ($02),y
        bcc     LA291
LA2A2:
        stx     $02D3
LA2A5:
        ldy     #$46
        lda     #$FF
        sta     ($F8),y
        lda     #$8D
        sta     StoreA_in_SerialCommandReg
        lda     $020A
        sta     $03EA
        sta     $03EE
        clc
        cld
        lda     $0209
        adc     #$02
        sta     $03E9
        sei
        lda     UART_Flags_03e7
        ora     #$40
        sta     UART_Flags_03e7
        lda     $03E6
        and     #$F3
        ora     #$04
        sta     $03E6
        jsr     StoreA_in_SerialCommandReg
        cli
        rts

LA2DB:
        lda     #$04
        sta     $040C
        ldy     #$02
        lda     (POIN_F0),y
        cmp     #$04
        bcs     LA2F2
        lda     #$04
        sta     (POIN_F0),y
        lda     #$20
        ldy     #$03
        sta     (POIN_F0),y
LA2F2:
        ldy     #$03
        lda     (POIN_F0),y
        sta     $040D
        rts

        bcc     LA29C
        adc     $99A0
        ldy     #$53
        lda     ($49,x)
        lda     ($4E,x)
        .byte   $A1
LA306:
        ldx     #$16
LA308:
        lda     $FF12,x
        cmp     #$FF
        bne     LA312
        lda     $FC5E,x
LA312:
        sta     SpecialCharacters_Copy,x
        dex
        bpl     LA308
        rts

; 0xA319
INIT_TERMINAL:
        jsr     LA306
        lda     #$60
        sta     $03EB
        sta     $03EF
        lda     #$AD
        sta     StoreA_in_SerialCommandReg
        lda     $020D
        sta     $03E9
        lda     $020E
        sta     $03EA
        jsr     StoreA_in_SerialCommandReg
        sta     $0407
        lda     $020A
        sta     $03EA
        sta     $03EE
        lda     #$8D
        sta     StoreA_in_SerialCommandReg
        clc
        cld
        lda     $0209
        adc     #$00
        sta     $03ED
        lda     $0209
        adc     #$03
        sta     $03E9
        lda     $0407
        and     #$8F
        sta     $0408
        lda     $0407
        lsr     a
        and     #$20
        ora     #$10
        ora     $0408
        jsr     StoreA_in_SerialCommandReg
        clc
        lda     $0209
        adc     #$02
        sta     $03E9
        lda     $0407
        asl     a
        asl     a
        and     #$C0
        ora     #$23
        jsr     StoreA_in_SerialCommandReg
        and     #$FD
        sta     $03E6
        lda     #$00
        sta     UART_Flags_03e7
        sta     $040A
        rts

        lsr     LFFFE
        .byte   $53
        inc     $56FF,x
        and     LFFFE,x
        jmp     LFFFE

        eor     InterruptControl_Reg,x
        .byte   $FF
        rol     a
        inc     $FFFF,x
LA3A8:
        jsr     Vector_OUTVOR
        brk
        rol     $20
        ldy     #$A9
        lsr     $03AC
        .byte   $04
        beq     LA3B8
        lda     #$53
LA3B8:
        brk
        and     (_ZERO_,x)
        .byte   $2B
        lda     #$4C
        ldy     $0406
        beq     LA3C5
        lda     #$55
LA3C5:
        brk
        and     (_ZERO_,x)
        .byte   $2B
        ldy     $0401
        dey
        cpy     #$FE
        bne     LA3D3
        ldy     #$00
LA3D3:
        tya
        ldy     #$00
        brk
        asl     _ZERO_
        .byte   $23
        brk
        .byte   $2B
        brk
        .byte   $2B
        ldx     #$00
LA3E0:
        lda     SpecialCharacters_Copy,x
        brk
        .byte   $23
        brk
        .byte   $2B
        inx
        cpx     #$11
        bcc     LA3E0
        jsr     Vector_OUTAUS
        clc
        clv
        rts

LA3F2:
        ldy     #$00
        brk
        asl     $48
        tya
        brk
        and     a:$68
        .byte   $23
        brk
        .byte   $2B
        rts

LA400:
        brk
        .byte   $1B
        bvc     LA3A8
        lda     #$94
        ldy     #$A3
        brk
        asl     $70,x
        .byte   $3F
        cmp     #$02
        beq     LA42A
        brk
        .byte   $1B
        bvs     LA478
        cmp     #$02
        bcc     LA424
        cmp     #$05
        beq     LA445
        and     #$FC
        sta     $0406
        clc
        clv
        rts

LA424:
        sta     $0403
        clc
        clv
        rts

LA42A:
        lda     #$02
        brk
        .byte   $1A
        bvs     LA478
        brk
        .byte   $1B
        bvs     LA478
        brk
        php
        tax
        bne     LA43B
        ldx     #$FE
LA43B:
        inx
        stx     $0401
        stx     $0402
        clc
        clv
        rts

LA445:
        jsr     LA306
        clc
        clv
        rts

        ldx     #$10
LA44D:
        lda     SpecialCharacters_Copy,x
        sta     $025E,x
        dex
        bpl     LA44D
        ldx     #$00
        lda     #$00
        brk
        .byte   $12
        bvc     LA478
        cmp     #$2C
        beq     LA46D
        cmp     #$0D
        beq     LA46D
        brk
        .byte   $13
        bvs     LA478
        sta     $025E,x
LA46D:
        inx
        cpx     #$11
        bcs     LA47F
        brk
        ora     ($C9),y
        bit     $E0F0
LA478:
        bit     $FC00
        lda     #$A1
        clc
        rts

LA47F:
        brk
        .byte   $1B
        bvs     LA478
        ldx     #$10
LA485:
        lda     $025E,x
        sta     SpecialCharacters_Copy,x
        dex
        bpl     LA485
        clc
        clv
        rts

LA491:
        lda     #$AC
        ldy     #$A8
        jmp     LA049

        sei
        lda     PH3TSK_F400_PRINTER_BUSY
        beq     LA4A7
        cli
        lda     #$C0
LA4A1:
        ldy     #$46
        sta     ($F8),y
        clv
        rts

LA4A7:
        lda     $0207
        sta     PH3TSK_F400_PRINTER_BUSY
        cli
        lda     $075F
        cmp     #$02
        beq     LA4BF
        lda     #$00
        sta     PH3TSK_F400_PRINTER_BUSY
        lda     #$E2
        jmp     LA4A1

LA4BF:
        ldy     #$45
        sta     ($F8),y
        sei
        lda     $F87C
        ora     #$0F
        sta     $F87C
        cli
        lda     #$FF
        sta     $F873
        lda     #$05
        sta     $F407
        ldx     #$00
        lda     $0477,x
        jsr     LA55F
LA4DF:
        inx
        cpx     $0476
        bcs     LA4FB
        lda     $0477,x
        jsr     LA534
        bvc     LA4DF
        lda     #$00
        sta     PH3TSK_F400_PRINTER_BUSY
        ldy     #$45
        sta     ($F8),y
        lda     #$C1
        jmp     LA4A1

LA4FB:
        lda     $0417
        sta     $F405
        ldy     #$70
        lda     #$FF
        sta     ($F8),y
        ldy     #$61
        lda     #$00
        sta     ($F8),y
        iny
        lda     #$01
        sta     ($F8),y
        ldy     #$63
        lda     #$23
        sta     ($F8),y
        iny
        lda     #$04
        sta     ($F8),y
        lda     #$01
        sta     $F40A
        lda     #$0D
        sta     $F403
        jsr     LA534
        lda     #$82
        sta     $F87E
        lda     #$01
        jmp     LA4A1

LA534:
        pha
        lda     $F407
        sta     $F404
LA53B:
        lda     $F87D
        and     #$02
        bne     LA55A
        lda     POIN_F0
        pha
        lda     POIN_F0+1
        pha
        brk
        .byte   $04
        pla
        sta     POIN_F0+1
        pla
        sta     POIN_F0
        lda     $F404
        bpl     LA53B
        bit     $FC00
        pla
        rts

LA55A:
        clv
        lda     $F871
        pla
LA55F:
        sei
        pha
        sta     $F87F
        lda     $F87C
        and     #$FD
        sta     $F87C
        ora     #$02
        sta     $F87C
        cli
        pla
        rts

        lda     PH3TSK_F400_PRINTER_BUSY
        cmp     $0207
        beq     LA581
        lda     #$E1
        jmp     LA4A1

LA581:
        lda     #$00
        sta     PH3TSK_F400_PRINTER_BUSY
        ldy     #$45
        lda     #$00
        sta     ($F8),y
        lda     #$01
        jmp     LA4A1

        lda     #$F2
        jmp     LA4A1

        lda     $F403
        beq     LA5B1
        lda     #$64
        sta     $F404
LA5A0:
        brk
        .byte   $04
        lda     $F403
        beq     LA5B1
        lda     $F404
        bpl     LA5A0
        lda     #$C1
        jmp     LA749

LA5B1:
        lda     #$64
        sta     $F407
        lda     $0416
        beq     LA5C3
        lda     $F405
        cmp     $0417
        bcs     LA5C6
LA5C3:
        jmp     LA662

LA5C6:
        ldx     $0418
        bne     LA5D5
        lda     #$0C
        jsr     LA534
        bvs     LA61D
        jmp     LA60C

LA5D5:
        cmp     $0418
        bcs     LA5EA
        lda     #$0A
        jsr     LA534
        bvs     LA61D
        inc     $F405
        lda     $F405
        jmp     LA5D5

LA5EA:
        jsr     LA753
        bvs     LA61D
        ldx     #$00
LA5F1:
        lda     #$2D
        jsr     LA534
        bvs     LA61D
        inx
        cpx     $0414
        bcc     LA5F1
        lda     #$0D
        jsr     LA534
        bvs     LA61D
        lda     #$0A
        jsr     LA534
        bvs     LA61D
LA60C:
        lda     #$02
        sta     $F405
LA611:
        cmp     $0415
        bcs     LA629
        lda     #$0A
        jsr     LA534
        bvc     LA620
LA61D:
        jmp     LA74F

LA620:
        inc     $F405
        lda     $F405
        jmp     LA611

LA629:
        cmp     $0416
        beq     LA662
        jsr     LA753
        bvs     LA61D
        jsr     LA76D
        bvs     LA61D
        clc
        sed
        ldy     #$62
        lda     ($F8),y
        adc     #$01
        sta     ($F8),y
        dey
        lda     ($F8),y
        adc     #$00
        sta     ($F8),y
        lda     #$0D
        jsr     LA534
        bvs     LA61D
LA650:
        lda     #$0A
        jsr     LA534
        bvs     LA61D
        inc     $F405
        lda     $F405
        cmp     $0416
        bcc     LA650
LA662:
        ldy     #$41
        lda     ($F8),y
        sta     POIN_F0
        iny
        lda     ($F8),y
        sta     POIN_F0+1
        ldy     #$02
        lda     (POIN_F0),y
        sta     $F406
        cmp     #$03
        bne     LA680
        lda     #$20
        sta     $F40A
        jmp     LA6E3

LA680:
        ldy     #$2E
        lda     ($F8),y
        cmp     #$02
        beq     LA692
        lda     #$20
        sta     $F40A
        ldy     #$03
        jmp     LA6A8

LA692:
        ldy     #$03
        lda     (POIN_F0),y
        sta     $F40A
        cmp     #$30
        bne     LA6A7
        lda     $048B
        bne     LA6A7
        lda     #$01
        jmp     LA4A1

LA6A7:
        iny
LA6A8:
        ldx     #$00
        jsr     LA753
        bvs     LA6DC
LA6AF:
        cpy     $F406
        bcs     LA6E3
        cpx     $0414
        bcc     LA6D1
        lda     #$0D
        jsr     LA534
        bvs     LA6DC
        lda     #$0A
        jsr     LA534
        bvs     LA6DC
        jsr     LA753
        bvs     LA6DC
        inc     $F405
        ldx     #$00
LA6D1:
        lda     (POIN_F0),y
        cmp     #$7F
        bne     LA6D9
        lda     #$5F
LA6D9:
        jsr     LA534
LA6DC:
        bvs     LA74F
        inx
        iny
        jmp     LA6AF

LA6E3:
        lda     $F40A
        cmp     #$30
        bne     LA6F4
        lda     #$00
        sta     $F40A
        lda     #$00
        jmp     LA734

LA6F4:
        cmp     #$41
        bne     LA71D
        lda     $0416
        beq     LA71D
        sec
        cld
        lda     $0417
        sbc     $F405
        sta     $F40A
        lda     $0416
        bne     LA718
        lda     #$00
        sta     $F40A
        lda     $0417
        sta     $F405
LA718:
        lda     #$FF
        jmp     LA734

LA71D:
        lda     #$01
        sta     $F40A
        lda     #$00
        ldy     $0416
        beq     LA734
        ldy     $F405
        iny
        cpy     $0417
        bcc     LA734
        lda     #$FF
LA734:
        ldy     #$70
        sta     ($F8),y
LA738:
        lda     #$0D
        sta     $F403
        jsr     LA534
        bvs     LA74F
        lda     #$82
        sta     $F87E
        lda     #$01
LA749:
        ldy     #$46
        sta     ($F8),y
        clv
        rts

LA74F:
        lda     #$C1
        bne     LA749
LA753:
        pha
        stx     $041A
        ldx     $0419
LA75A:
        beq     LA767
        lda     #$20
        jsr     LA534
        bvs     LA768
        dex
        jmp     LA75A

LA767:
        clv
LA768:
        ldx     $041A
        pla
        rts

LA76D:
        ldx     #$01
LA76F:
        lda     $049C,x
        sta     $041B,x
        lda     POIN_F0,x
        sta     $041D,x
        lda     $F2,x
        sta     $041F,x
        dex
        bpl     LA76F
        ldy     #$63
        lda     ($F8),y
        tax
        iny
        lda     ($F8),y
        tay
        txa
        brk
        bpl     LA738
        brk
        brk
        .byte   $1C
        lda     $0414
        sta     $0421
        brk
        ora     ($70),y
        .byte   $12
        sta     $0422
LA79F:
        brk
        ora     ($70),y
        .byte   $0B
        cmp     $0422
        beq     LA7CA
LA7A8:
        jsr     LA856
        jmp     LA79F

        clv
LA7AF:
        ldx     #$01
LA7B1:
        lda     $041B,x
        sta     $049C,x
        lda     $041D,x
        sta     POIN_F0,x
        lda     $041F,x
        sta     $F2,x
        dex
        bpl     LA7B1
        rts

LA7C5:
        bit     $FC00
        bvs     LA7AF
LA7CA:
        brk
        ora     ($70),y
        cpx     #$C9
        .byte   $53
        bne     LA7D5
        jmp     LA86D

LA7D5:
        cmp     #$50
        beq     LA7F0
        cmp     #$55
        beq     LA812
        cmp     #$54
        beq     LA819
        cmp     #$44
        beq     LA820
        cmp     #$47
        beq     LA829
        cmp     #$58
        beq     LA830
        jmp     LA7A8

LA7F0:
        ldy     #$61
LA7F2:
        lda     ($F8),y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     $FC01,x
        jsr     LA856
        lda     ($F8),y
        and     #$0F
        tax
        lda     $FC01,x
        jsr     LA856
        iny
        cpy     #$63
        bcc     LA7F2
        jmp     LA79F

LA812:
        lda     #$A0
        ldy     #$04
        jmp     LA842

LA819:
        lda     #$2E
        ldy     #$F4
        jmp     LA842

LA820:
        lda     #$24
        ldy     #$F4
        ldx     #$0A
        jmp     LA844

LA829:
        lda     #$18
        ldy     #$02
        jmp     LA842

LA830:
        brk
        .byte   $14
        bvs     LA83F
        sta     InterruptControl_Reg
        sty     $FF
        brk
        .byte   $13
        bvs     LA83F
        tax
        bne     LA848
LA83F:
        jmp     LA79F

LA842:
        ldx     #$08
LA844:
        sta     InterruptControl_Reg
        sty     $FF
LA848:
        ldy     #$00
LA84A:
        lda     (InterruptControl_Reg),y
        jsr     LA856
        iny
        dex
        bne     LA84A
        jmp     LA79F

LA856:
        pha
        lda     $0421
        bne     LA85E
        pla
        rts

LA85E:
        dec     $0421
        pla
        jsr     LA534
        bvs     LA868
        rts

LA868:
        pla
        pla
        jmp     LA7C5

LA86D:
        ldx     #$00
LA86F:
        lda     $FF07,x
        bpl     LA876
        lda     #$20
LA876:
        jsr     LA856
        inx
        cpx     #$08
        bcc     LA86F
        lda     #$28
        jsr     LA856
        ldy     #$00
LA885:
        lda     $FF0F,y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     $FC01,x
        jsr     LA856
        lda     $FF0F,y
        and     #$0F
        tax
        lda     $FC01,x
        jsr     LA856
        iny
        cpy     #$02
        bcc     LA885
        lda     #$29
        jsr     LA856
        jmp     LA79F

        tya
        ldy     $74
        lda     $91
        lda     $96
        lda     $91
        lda     $91
        lda     $26
        jsr     L2620
        .byte   $47
        jsr     L4F46
        .byte   $52
        jsr     L5526
        jsr     L2620
        .byte   $53
        jsr     L2020
        jsr     L2020
        jsr     L2620
        .byte   $44
        jsr     L2620
        .byte   $54
        jsr     L5020
        eor     ($47,x)
        eor     $20
        rol     $50
        .byte   $FF
; 0xA8E0
INIT_PRINTER:
        ldx     #$00
        jsr     LA95F
        sta     $0414
        jsr     LA95F
        sta     $0419
        jsr     LA95F
        sta     $0418
        jsr     LA95F
        sta     $0415
        jsr     LA95F
        sta     $0416
        jsr     LA95F
        sta     $0417
        lda     $0415
        bne     LA90E
        lda     $0416
LA90E:
        sta     $0415
        lda     $0416
        clc
        cld
        adc     $0417
        sta     $0417
        ldx     $0418
        beq     LA922
        inx
LA922:
        stx     $0418
        lda     #$53
        sta     $0423
        lda     #$23
        ldy     #$04
        brk
        jsr     L2700
        brk
        ldx     #$B8
        tay
        lda     $FF33
        sta     $048B
        lda     $FF34
        cmp     #$FF
        bne     LA94E
        lda     #$7F
        sta     $0477
        lda     #$01
        sta     $0476
        rts

LA94E:
        ldx     #$13
LA950:
        lda     $FF34,x
        sta     $0477,x
        dex
        bpl     LA950
        lda     #$20
        sta     $0476
        rts

LA95F:
        lda     $FF29,x
        cmp     #$FF
        bne     LA969
        lda     $FC75,x
LA969:
        inx
        rts

LA96B:
        jsr     Vector_OUTVOR
        brk
        rol     $20
        ldy     #$AD
        .byte   $14
        .byte   $04
        jsr     LA3F2
        lda     $0419
        jsr     LA3F2
        lda     $0416
        bne     LA9AA
        lda     #$30
        brk
        .byte   $21
LA987:
        brk
        rol     $2D
        ldy     #$A9
        eor     $8BAE,y
        .byte   $04
        bne     LA994
        lda     #$4E
LA994:
        brk
        and     ($A2,x)
        brk
LA998:
        brk
        .byte   $2B
        lda     $0477,x
        brk
        .byte   $23
        inx
        cpx     $0476
        bcc     LA998
        jsr     Vector_OUTAUS
        clv
        rts

LA9AA:
        lda     $0418
        bne     LA9B8
        lda     #$48
        brk
        and     (_ZERO_,x)
        .byte   $2B
        jmp     LA9C0

LA9B8:
        ldy     $0418
        dey
        tya
        jsr     LA3F2
LA9C0:
        lda     $0415
        cmp     $0416
        bne     LA9CA
        lda     #$00
LA9CA:
        jsr     LA3F2
        lda     $0416
        jsr     LA3F2
        sec
        cld
        lda     $0417
        sbc     $0416
        jsr     LA3F2
        jmp     LA987

        rol     a
        inc     $4EFF,x
        inc     $49FF,x
        bit     LFFFE
        .byte   $FF
LA9EC:
        brk
        .byte   $1B
        bvc     LA9F3
        lda     #$A1
        rts

LA9F3:
        jsr     INIT_PRINTER
        clv
        rts

LA9F8:
        brk
        .byte   $1B
        bvs     LA9FF
        jmp     LA96B

LA9FF:
        lda     #$2A
        brk
        .byte   $12
        bvc     LA9EC
        lda     PH3TSK_F400_PRINTER_BUSY
        cmp     $0207
        bne     LAA13
        lda     #$C0
        bit     $FC00
        rts

LAA13:
        brk
        stx     $E1,y
        lda     #$70
        and     a:$C9,x
        beq     LA9EC
        cmp     #$01
        bne     LAA2C
        brk
        .byte   $1B
        bvs     LAA53
        lda     #$00
        sta     $048B
        clv
        rts

LAA2C:
        ldx     #$00
LAA2E:
        brk
        .byte   $13
        bvs     LAA49
        sta     $0477,x
        inx
        cpx     #$14
        bcs     LAA40
        lda     #$2C
        brk
        .byte   $12
        bvc     LAA2E
LAA40:
        brk
        .byte   $1B
        bvs     LAA49
        stx     $0476
        clv
        rts

LAA49:
        lda     #$01
        sta     $0476
        lda     #$7F
        sta     $0477
LAA53:
        jmp     LAABE

        jsr     LAB2E
        cmp     #$0A
        bcc     LAABE
        sta     $025E
        jsr     LAB1E
        jsr     LAB2E
        sta     PANIC_SRCL_25F
        jsr     LAB1E
        lda     #$48
        brk
        .byte   $12
        bvs     LAA7A
        lda     #$00
        sta     PANIC_SRCH_260
        jmp     LAA9E

LAA7A:
        jsr     LAB2E
        cmp     #$00
        bne     LAA98
        brk
        .byte   $1B
        bvs     LAABE
        lda     $025E
        sta     $0414
        lda     PANIC_SRCL_25F
        sta     $0419
        lda     #$00
        sta     $0416
        clv
        rts

LAA98:
        sta     PANIC_SRCH_260
        inc     PANIC_SRCH_260
LAA9E:
        jsr     LAB1E
        jsr     LAB2E
        sta     $0261
        jsr     LAB1E
        jsr     LAB2E
        cmp     #$02
        bcc     LAABE
        sta     $0262
        jsr     LAB1E
        jsr     LAB2E
        cmp     #$02
        bcs     LAAC4
LAABE:
        bit     $FC00
        lda     #$A1
        rts

LAAC4:
        clc
        cld
        adc     $0262
        sta     $0263
        lda     $0261
        cmp     #$00
        bne     LAADC
        lda     $0262
        sta     $0261
        jmp     LAAE8

LAADC:
        cmp     #$02
        bcc     LAABE
        sta     $0261
        cmp     $0262
        bcs     LAABE
LAAE8:
        brk
        .byte   $1B
        bvs     LAABE
        lda     $025E
        sta     $0414
        lda     PANIC_SRCL_25F
        sta     $0419
        lda     PANIC_SRCH_260
        sta     $0418
        lda     $0261
        sta     $0415
        lda     $0262
        sta     $0416
        lda     $0263
        sta     $0417
        lda     PANIC_SRCH_260
        sta     $0418
        lda     $0417
        sta     $F405
        clv
        rts

LAB1E:
        brk
        ora     ($C9),y
        bit     $02D0
        clv
        rts

LAB26:
        pla
        pla
        lda     #$A1
        bit     $FC00
        rts

LAB2E:
        lda     #$03
        brk
        .byte   $1A
        bvs     LAB26
        brk
        php
        bvs     LAB26
        cpy     #$00
        bne     LAB26
        rts

LAB3D:
        lda     #$F9
        ldy     #$AE
        .byte   $4C
        .byte   $49
LAB43:
        ldy     #$04
        .byte   $02
        lda     $F8
        ldy     $F9
        ldx     $075F
        brk
        .byte   $52
        bvc     LAB56
LAB51:
        ldy     #$46
        sta     ($F8),y
        rts

LAB56:
        ldx     StackTail_211
        lda     $0102,x
        sta     $075F
        cmp     #$01
        bne     LAB84
        ldy     #$2E
        lda     ($F8),y
        bpl     LAB74
LAB69:
        lda     $F8
        ldy     $F9
        brk
        .byte   $53
        lda     #$B1
        jmp     LAB51

LAB74:
        and     #$F0
        sta     $0762
        ldy     #$6D
        lda     ($F8),y
        and     #$F0
        cmp     $0762
        bne     LAB69
LAB84:
        ldy     #$5A
        lda     ($F8),y
        bne     LABAA
        ldx     $075F
        lda     LAB43,x
        tax
        brk
        bmi     LAC04
        ldy     $628C,x
        .byte   $07
        ldy     #$59
        sta     ($F8),y
        iny
        lda     $0762
        sta     ($F8),y
        ldy     #$60
        lda     ($F8),y
        ora     #$01
        sta     ($F8),y
LABAA:
        ldy     #$59
        lda     #$4C
        jsr     LAC64
        ldx     StackTail_211
        lda     $0102,x
        cmp     #$02
        bne     LABC6
        ldy     #$45
        lda     #$02
        sta     ($F8),y
        lda     #$01
        jmp     LAB51

LABC6:
        ldy     #$59
        lda     #$5B
        jsr     LAC64
        ldy     #$5C
        lda     ($F8),y
        tax
        inx
        txa
        sta     ($F8),y
        ldy     #$60
        lda     ($F8),y
        ora     #$80
        sta     ($F8),y
        ldy     #$45
        lda     #$01
        sta     ($F8),y
        ldy     #$4E
        lda     #$01
        sta     ($F8),y
        jsr     LAC40
        lda     #$01
        jmp     LAB51

        jsr     LAC4E
        ldy     #$59
        jsr     LAC5A
        ldy     #$02
        lda     (POIN_F0),y
        cmp     #$03
        bne     LAC22
        lda     #$01
LAC04:
        jsr     LAC7E
        bvc     LAC13
LAC09:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LAC13:
        lda     #$20
        jsr     LAC7E
        bvs     LAC09
LAC1A:
        ldy     #$46
        lda     #$01
        sta     ($F8),y
        clv
        rts

LAC22:
        sta     $04C7
        sec
        cld
        sbc     #$03
        jsr     LAC7E
        bvs     LAC09
        ldy     #$03
LAC30:
        lda     (POIN_F0),y
        jsr     LAC7E
        bvs     LAC09
        iny
        cpy     $04C7
        bcc     LAC30
        jmp     LAC1A

LAC40:
        lda     $F8
        ldy     $F9
        brk
        .byte   $54
        bvc     LAC4C
        ldy     #$49
        sta     ($F8),y
LAC4C:
        clv
        rts

LAC4E:
        ldy     #$41
        lda     ($F8),y
        sta     POIN_F0
        iny
        lda     ($F8),y
        sta     POIN_F0+1
        rts

LAC5A:
        lda     ($F8),y
        sta     $F2
        iny
        lda     ($F8),y
        sta     $F3
        rts

LAC64:
        sta     $0761
        inc     $0761
        lda     ($F8),y
        sta     $0762
        iny
        lda     ($F8),y
        ldy     $0761
        sta     ($F8),y
        dey
        lda     $0762
        sta     ($F8),y
        rts

LAC7E:
        sty     $04C8
        sta     $0762
        ldy     #$5F
        lda     ($F8),y
        tay
        lda     $0762
        sta     ($F2),y
        iny
        tya
        ldy     #$5F
        sta     ($F8),y
        tay
        beq     LAC9C
        clv
        ldy     $04C8
        rts

LAC9C:
        ldy     #$4E
        clc
        sed
        lda     ($F8),y
        adc     #$01
        sta     ($F8),y
        iny
        lda     ($F8),y
        adc     #$00
        sta     ($F8),y
        jsr     LACD2
        lda     $F8
        ldy     $F9
        brk
        bvc     LAD07
        .byte   $04
        cmp     #$E6
        bne     LACC9
        lda     $F8
        ldy     $F9
        brk
        .byte   $5A
        bvs     LACC9
        brk
        eor     $70,x
        ora     ($B8,x)
LACC9:
        pha
        jsr     LACDE
        ldy     $04C8
        pla
        rts

LACD2:
        ldy     #$03
LACD4:
        lda     POIN_F0,y
        sta     $04C9,y
        dey
        bpl     LACD4
        rts

LACDE:
        ldy     #$03
LACE0:
        lda     $04C9,y
        sta     POIN_F0,y
        dey
        bpl     LACE0
        rts

LACEA:
        jmp     LAC09

LACED:
        jsr     LAC4E
        ldy     #$5D
        jsr     LAC5A
        jsr     LADE7
        bvs     LACEA
        cmp     #$00
        bne     LAD0D
LACFE:
        ldy     #$02
        lda     #$03
        sta     (POIN_F0),y
LAD04:
        ldy     #$01
        .byte   $A9
LAD07:
        .byte   $03
        sta     (POIN_F0),y
        jmp     LAC1A

LAD0D:
        sta     $04CD
        clc
        cld
        adc     #$03
        sta     $04C7
        ldy     #$6D
        lda     ($F8),y
        and     #$0F
        sta     $0771
        ldy     #$2E
        lda     ($F8),y
        and     #$0F
        sta     $0770
        cmp     $0771
        beq     LADA3
        cmp     #$00
        beq     LAD5E
        cmp     #$02
        beq     LAD4D
        ldy     $04C7
        dey
        sty     $04C7
        jsr     LADE7
        bvc     LAD45
LAD42:
        jmp     LACEA

LAD45:
        dec     $04CD
        bne     LAD4D
LAD4A:
        jmp     LACFE

LAD4D:
        ldy     $04C7
        dey
        sty     $04C7
        jsr     LADE7
        bvs     LAD42
        dec     $04CD
        beq     LAD4A
LAD5E:
        ldy     #$03
        lda     $0771
        cmp     #$00
        beq     LADA5
        cmp     #$02
        beq     LAD92
        ldy     $04C7
        iny
        iny
        sty     $04C7
        sed
        clc
        ldy     #$61
        lda     ($F8),y
        adc     #$01
        sta     ($F8),y
        ldy     #$04
        sta     (POIN_F0),y
        ldy     #$62
        lda     ($F8),y
        adc     #$00
        sta     ($F8),y
        ldy     #$03
        sta     (POIN_F0),y
        ldy     #$05
        jmp     LADA5

LAD92:
        ldy     $04C7
        iny
        sty     $04C7
        ldy     #$03
        lda     #$20
        sta     (POIN_F0),y
        iny
        jmp     LADA5

LADA3:
        ldy     #$03
LADA5:
        sty     $0770
        jsr     LADD4
        ldy     $0770
LADAE:
        jsr     LADE7
        bvs     LAD42
        dec     $04CD
        sta     (POIN_F0),y
        iny
        cpy     $04C7
        bcc     LADAE
LADBE:
        lda     $04CD
        bne     LADC6
        jmp     LAD04

LADC6:
        jsr     LADE7
        bvc     LADCE
        jmp     LACEA

LADCE:
        dec     $04CD
        jmp     LADBE

LADD4:
        ldy     #$00
        lda     $04C7
        cmp     (POIN_F0),y
        bcc     LADDF
        lda     (POIN_F0),y
LADDF:
        sta     $04C7
        ldy     #$02
        sta     (POIN_F0),y
        rts

LADE7:
        sty     $04C8
        ldy     #$5F
        lda     ($F8),y
        bne     LAE5A
        jsr     LACD2
        lda     $F8
        ldy     $F9
        brk
        bvc     LAE4A
        .byte   $0C
        pha
        jsr     LACDE
        pla
        bit     $FC00
        ldy     $04C8
        rts

        ldy     #$60
        lda     ($F8),y
        bpl     LAE26
        ldy     #$59
        lda     #$5D
        jsr     LAC64
        ldy     #$5B
        lda     #$4C
        jsr     LAC64
        ldy     #$60
        lda     ($F8),y
        and     #$7F
        sta     ($F8),y
        jmp     LAE3C

LAE26:
        ldy     #$5B
        lda     #$5D
        jsr     LAC64
        ldy     #$59
        lda     #$4C
        jsr     LAC64
        ldy     #$60
        lda     ($F8),y
        ora     #$80
        sta     ($F8),y
LAE3C:
        ldy     #$4E
        clc
        sed
        lda     ($F8),y
        adc     #$01
        sta     ($F8),y
        iny
        lda     ($F8),y
        .byte   $69
LAE4A:
        brk
        sta     ($F8),y
        jsr     LAC40
        jsr     LACDE
        ldy     #$5D
        jsr     LAC5A
        lda     #$00
LAE5A:
        tay
        lda     ($F2),y
        pha
        iny
        tya
        ldy     #$5F
        sta     ($F8),y
        pla
        ldy     $04C8
        clv
        rts

        ldy     #$45
        .byte   $B1
LAE6D:
        sed
        cmp     #$02
        bne     LAEC1
        ldy     #$59
        jsr     LAC5A
LAE77:
        lda     #$00
        jsr     LAC7E
        bvc     LAE92
LAE7E:
        pha
        jsr     LAED2
        ldy     #$6E
        lda     #$00
        sta     ($F8),y
        lda     $F8
        ldy     $F9
        brk
        .byte   $53
        pla
        jmp     LAB51

LAE92:
        ldy     #$5F
        lda     ($F8),y
        bne     LAE77
        lda     $F8
        ldy     $F9
        brk
        bvc     LAEEF
        .byte   $04
        cmp     #$E6
        bne     LAE7E
        lda     $F8
        ldy     $F9
        brk
        .byte   $53
        bvs     LAE7E
LAEAC:
        jsr     LAED2
        bvs     LAE7E
        ldy     #$51
        lda     #$00
LAEB5:
        sta     ($F8),y
        iny
        cpy     #$61
        bcc     LAEB5
        lda     #$01
        jmp     LAE7E

LAEC1:
        lda     $F8
        ldy     $F9
        brk
        bvc     LAE6D
        sed
        ldy     $F9
        brk
        .byte   $53
        bvs     LAE7E
        jmp     LAEAC

LAED2:
        ldy     #$45
        lda     ($F8),y
        tax
        lda     LAB43,x
        sta     $0761
        lda     #$00
        sta     ($F8),y
        clv
        ldy     #$60
        lda     ($F8),y
        lsr     a
        bcc     LAEF8
        ldy     #$59
        lda     ($F8),y
        tax
        iny
LAEEF:
        lda     ($F8),y
        tay
        txa
        ldx     $0761
        brk
        .byte   $31
LAEF8:
        rts

        lsr     $AB
        ror     a
        ldx     LACED
        .byte   $F2
        .byte   $AB
        sbc     $EDAC
        .byte   $AC
; 0xAF05
INIT_FLOPPY:
        lda     #$20
        sta     $04BA
        sta     $04BB
        sta     $04BC
        sta     $04BD
        sta     $04BE
        rts

LAF17:
        sta     $F491
        sty     $F492
        lda     #$03
        sta     $04C5
        ldy     #$48
        lda     ($F8),y
        sta     $F48F
LAF29:
        ldx     $F48F
        lda     $F491
        ldy     $F492
        brk
        eor     ($50),y
        asl     a
        cmp     #$02
        beq     LAF3E
        bit     $FC00
        rts

LAF3E:
        clv
        rts

        lda     #$04
        sta     $F490
        lda     $F491
        sta     POIN_F0
        lda     $F492
        sta     POIN_F0+1
LAF4F:
        ldy     #$00
        lda     (POIN_F0),y
        cmp     #$20
        bne     LAF5D
        jsr     LAFA8
        jmp     LAF86

LAF5D:
        lda     (POIN_F0),y
        cmp     #$20
        beq     LAF79
        cmp     #$3B
        beq     LAF79
        sta     $0761
        lda     ($F8),y
        cmp     $0761
        bne     LAF86
        iny
        cpy     #$20
        bcc     LAF5D
        jmp     LAF83

LAF79:
        lda     ($F8),y
        cmp     #$20
        beq     LAF83
        cmp     #$3B
        bne     LAF86
LAF83:
        jsr     LAFAB
LAF86:
        clc
        cld
        lda     POIN_F0
        adc     #$40
        sta     POIN_F0
        lda     POIN_F0+1
        adc     #$00
        sta     POIN_F0+1
        inc     $04C5
        dec     $F490
        bne     LAF4F
        clc
        lda     $F48F
        adc     #$08
        sta     $F48F
        jmp     LAF29

LAFA8:
        jmp     (LF47F)

LAFAB:
        jmp     (LF481)

LAFAE:
        lda     #$5F
        ldy     #$F4
        jsr     Vector_LOCK
        lda     #$84
        jmp     LAFC3

LAFBA:
        lda     #$5F
        ldy     #$F4
        jsr     Vector_LOCK
        lda     #$A4
LAFC3:
        sta     $F47A
        jsr     Vector_FHNMIRW
        lda     $F437
        sta     $F47B
        lda     #$88
        sta     Floppy_MotorSel
        ldx     $04BF
        ora     Floppy_DriveSelect,x
        sta     Floppy_MotorSel
        and     #$F6
        sta     Floppy_MotorSel
        lda     FloppySCR_00
        bpl     LAFEF
        lda     #$C1
        ldx     #$00
        stx     $F45F
        rts

LAFEF:
        lda     #$2A
        ldy     #$80
        sta     $F460
        sty     $F461
        ldx     $04BF
        stx     $F4A1
        lda     #$00
        sta     $F769,x
        ldx     $04C0
        lda     $F755,x
        ldy     $F756,x
        jsr     Vector_FHNMIADR
        lda     #$05
        sta     $F47C
        lda     #$00
        sta     $F4A2
        ldx     $04C0
        lda     $F755,x
        sta     POIN_F0
        lda     $F756,x
        sta     POIN_F0+1
        ldy     #$1D
        sed
        clc
        lda     (POIN_F0),y
        adc     #$02
        sta     (POIN_F0),y
        iny
        lda     (POIN_F0),y
        adc     #$00
        sta     (POIN_F0),y
        iny
        lda     (POIN_F0),y
        adc     #$00
        sta     (POIN_F0),y
        lda     #$0C
        ora     $F495
        sta     FloppySCR_00
        lda     #$5F
        ldy     #$F4
        brk
        ora     $AD
        .byte   $5F
        .byte   $F4
        ldx     #$00
        stx     $F45F
        rts

LB056:
        sta     $0761
        ldx     #$00
        cld
LB05C:
        lda     $F500,x
        beq     LB06E
        clc
        txa
        adc     #$06
        tax
        cmp     #$FC
        bcc     LB05C
        bit     $FC00
        rts

LB06E:
        lda     $0761
        sta     $F500,x
        lda     $0207
        sta     $F501,x
        lda     $04C5
        sta     $F503,x
        lda     $04BF
        sta     $F502,x
        lda     $F8
        sta     $F504,x
        lda     $F9
        sta     $F505,x
        clv
        rts

LB092:
        ldx     #$00
        cld
LB095:
        lda     $F500,x
        beq     LB0B9
        lda     $0761
        beq     LB0A7
        lda     $F501,x
        cmp     $0207
        bne     LB0B9
LB0A7:
        lda     $F502,x
        cmp     $04BF
        bne     LB0B9
        lda     $F503,x
        cmp     $04C5
        bne     LB0B9
        clv
        rts

LB0B9:
        clc
        txa
        adc     #$06
        tax
        cmp     #$FC
        bcc     LB095
        bit     $FC00
        rts

LB0C6:
        lda     #$00
        sta     $0761
        jsr     LB092
        bvs     LB0D3
        lda     $F500,x
LB0D3:
        rts

LB0D4:
        lda     #$FF
        sta     $0761
        jsr     LB092
        bvs     LB0E3
        lda     #$00
        sta     $F500,x
LB0E3:
        rts

LB0E4:
        lda     $F437
        sta     $F47B
        lda     $F47A
        jsr     Vector_FHNMIRW
        lda     $F8
        ldy     $F9
        sta     $F475
        sty     $F476
        lda     FloppySCR_00
        bpl     LB10B
        lda     #$C1
LB101:
        ldy     #$49
        sta     ($F8),y
        lda     #$00
        sta     $F45F
        rts

LB10B:
        lda     $F47A
        cmp     #$F4
        bne     LB11E
        ldy     #$00
        lda     ($F8),y
        cmp     #$D9
        beq     LB11E
LB11A:
        lda     #$05
        bne     LB101
LB11E:
        lda     $F47A
        cmp     #$E4
        bne     LB12D
        ldy     #$00
        lda     ($F8),y
        cmp     #$D8
        bne     LB11A
LB12D:
        ldy     #$4A
        lda     ($F8),y
        sta     $F464
        iny
        lda     ($F8),y
        sta     $F465
        ora     $F464
        bne     LB143
LB13F:
        lda     #$F6
        bne     LB101
LB143:
        sec
        sed
        lda     $F464
        sbc     #$01
        sta     $F464
        lda     $F465
        sbc     #$00
        sta     $F465
        lda     $F464
        and     #$0F
        clc
        cld
        adc     #$01
        sta     $F462
        ldx     #$04
LB163:
        lsr     $F465
        ror     $F464
        dex
        bne     LB163
        lda     $F465
        bne     LB13F
        ldy     #$00
        ldx     #$00
        lda     $F464
        brk
        php
        bvs     LB13F
        cmp     #$28
        bcs     LB13F
        sta     $F463
        lda     $F462
        sta     FloppySEC_02
        ldy     #$48
        lda     ($F8),y
        sta     $F4A1
        tax
        lda     $F769,x
        sta     FloppyTRK_01
        lda     $F463
        sta     $F769,x
        lda     $F47A
        cmp     #$84
        beq     LB1BD
        cmp     #$A4
        bne     LB1C7
        ldy     #$4C
        lda     ($F8),y
        sta     POIN_F0
        iny
        lda     ($F8),y
        sta     POIN_F0+1
        ldy     #$00
LB1B5:
        lda     (POIN_F0),y
        sta     $F600,y
        iny
        bne     LB1B5
LB1BD:
        lda     #$00
        ldy     #$F6
        jsr     Vector_FHNMIADR
        jmp     LB1D4

LB1C7:
        ldy     #$4C
        lda     ($F8),y
        tax
        iny
        lda     ($F8),y
        tay
        txa
        jsr     Vector_FHNMIADR
LB1D4:
        ldy     #$49
        lda     #$FF
        sta     ($F8),y
        lda     #$05
        sta     $F47C
        lda     #$00
        sta     $F4A2
        ldy     #$48
        lda     ($F8),y
        asl     a
        tax
        lda     $F755,x
        sta     $F2
        lda     $F756,x
        sta     $F3
        ldy     #$1D
        sed
        clc
        lda     ($F2),y
        adc     #$01
        sta     ($F2),y
        iny
        lda     ($F2),y
        adc     #$00
        sta     ($F2),y
        iny
        lda     ($F2),y
        adc     #$00
        sta     ($F2),y
        lda     $F463
        cmp     FloppyTRK_01
        beq     LB242
        sta     FloppyDAT_03
        lda     #$2D
        ldy     #$80
        sta     $F478
        sty     $F479
        lda     #$30
        ldy     #$80
        sta     $F460
        sty     $F461
        ldy     #$1C
        lda     $F47A
        cmp     #$F4
        beq     LB238
        cmp     #$E4
        bne     LB23A
LB238:
        ldy     #$18
LB23A:
        tya
        ora     $F495
        sta     FloppySCR_00
        rts

LB242:
        lda     #$2D
        ldy     #$80
        sta     $F460
        sty     $F461
        php
        sei
        lda     VIAMUC_VIAORB
        and     #$BF
        sta     VIAMUC_VIAORB
        plp
        lda     $F47A
        sta     FloppySCR_00
        rts

LB25E:
        ldy     #$50
        lda     ($F8),y
        cmp     #$01
        beq     LB26D
        jsr     LAFBA
        cmp     #$01
        bne     LB2A6
LB26D:
        ldy     #$50
        lda     #$00
        sta     ($F8),y
        ldy     #$2D
        lda     ($F8),y
        sta     $04C5
        jsr     LB0D4
        bvc     LB284
        lda     #$CD
        jmp     LB2A6

LB284:
        lda     #$00
        sta     $F47E
        ldy     #$6E
        lda     ($F8),y
        beq     LB2A4
        ldy     #$2B
        lda     #$FF
        sta     ($F8),y
        iny
        sta     ($F8),y
        lda     $F8
        ldy     $F9
        brk
        .byte   $5B
        bvc     LB2A4
        cmp     #$B2
        bne     LB2A6
LB2A4:
        clv
        rts

LB2A6:
        ldx     StackTail_211
        sta     $0104,x
        lda     #$00
        sta     $F47E
        bit     $FC00
        rts

        lda     $F48C
        cmp     #$FF
        bne     LB2D2
        lda     $F48F
        sta     $F48C
        lda     POIN_F0
        sta     $F48A
        lda     POIN_F0+1
        sta     $F48B
        lda     $04C5
        sta     $F48D
LB2D2:
        rts

        ldy     #$2B
        lda     ($F8),y
        cmp     #$FF
        beq     LB342
        sta     $0761
        lda     (POIN_F0),y
        cmp     $0761
        bne     LB341
        iny
        lda     ($F8),y
        sta     $0761
        lda     (POIN_F0),y
        cmp     $0761
        bne     LB341
LB2F2:
        lda     $F48F
        sta     $F483
        lda     POIN_F0
        sta     $F484
        lda     POIN_F0+1
        sta     $F485
        ldy     #$2D
        lda     (POIN_F0),y
        sta     $F48E
        ldy     #$20
LB30B:
        lda     (POIN_F0),y
        sta     ($F8),y
        iny
        cpy     #$2B
        bcc     LB30B
        ldx     StackTail_211
        lda     $0102,x
        cmp     #$02
        bne     LB341
        ldy     #$00
LB320:
        lda     ($F8),y
        cmp     #$20
        beq     LB332
        cmp     #$3B
        beq     LB341
        iny
        cpy     #$20
        bcc     LB320
        jmp     LB341

LB332:
        lda     (POIN_F0),y
        cmp     #$20
        beq     LB341
LB338:
        lda     (POIN_F0),y
        sta     ($F8),y
        iny
        cpy     #$20
        bcc     LB338
LB341:
        rts

LB342:
        sec
        sed
        lda     $F488
        sbc     (POIN_F0),y
        iny
        lda     $F489
        sbc     (POIN_F0),y
        bcs     LB341
        lda     (POIN_F0),y
        sta     $F489
        dey
        lda     (POIN_F0),y
        sta     $F488
        jmp     LB2F2

LB35F:
        lda     #$FF
        sta     $F483
        sta     $F48C
        sta     $F48E
        lda     #$00
        sta     $F488
        sta     $F489
        lda     #$B5
        ldy     #$B2
        sta     LF47F
        sty     $F480
        lda     #$D3
        ldy     #$B2
        sta     LF481
        sty     $F482
        ldx     #$02
        brk
        bmi     LB3DB
        asl     a
LB38C:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

        sta     $F486
        sty     $F487
        ldx     StackTail_211
        lda     $0102,x
        cmp     #$02
        bne     LB3A9
        jmp     LB530

LB3A9:
        lda     $F486
        ldy     $F487
        jsr     LAF17
        bvs     LB3BD
        lda     $F483
        cmp     #$FF
        bne     LB3CF
        lda     #$B0
LB3BD:
        sta     $F483
        lda     $F486
        ldy     $F487
        ldx     #$02
        brk
        and     ($AD),y
        .byte   $83
        .byte   $F4
        bne     LB38C
LB3CF:
        tax
        lda     $F486
        ldy     $F487
        brk
        eor     ($70),y
        .byte   $E3
        .byte   $AD
LB3DB:
        sty     POINC1_F4
        ldy     $F485
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$2D
        lda     (POIN_F0),y
        sta     $04C5
        jsr     LB0C6
        bvs     LB40A
        sta     $0761
        and     #$40
        bne     LB406
        ldx     StackTail_211
        lda     $0102,x
        cmp     #$01
        beq     LB40A
        lda     $0761
        bpl     LB40A
LB406:
        lda     #$CB
        bne     LB3BD
LB40A:
        ldy     #$00
LB40C:
        lda     (POIN_F0),y
        sta     ($F8),y
        iny
        cpy     #$40
        bne     LB40C
        ldy     #$30
        jsr     LB5C9
        ldy     #$00
LB41C:
        lda     ($F8),y
        sta     (POIN_F0),y
        iny
        cpy     #$40
        bcc     LB41C
        ldx     StackTail_211
        lda     $0102,x
        cmp     #$01
        beq     LB440
        lda     $F483
LB432:
        tax
        lda     $F486
        ldy     $F487
        brk
        .byte   $5C
        bvc     LB440
        jmp     LB3BD

LB440:
        ldx     #$02
        lda     $F486
        ldy     $F487
        brk
        and     ($50),y
        .byte   $03
        jmp     LB38C

        ldy     #$2D
        lda     ($F8),y
        sta     $04C5
        ldy     #$80
        ldx     StackTail_211
        lda     $0102,x
        cmp     #$01
        beq     LB464
        ldy     #$40
LB464:
        tya
        jsr     LB056
        bvc     LB46F
        lda     #$CC
        jmp     LB38C

LB46F:
        ldy     #$50
        ldx     StackTail_211
        lda     $0102,x
        sta     ($F8),y
        ldx     $04C0
        lda     $F75F,x
        sta     $F2
        lda     $F760,x
        sta     $F3
        ldy     #$2D
        lda     ($F8),y
        sta     $04C5
        ldy     #$00
LB48F:
        lda     ($F2),y
        cmp     $04C5
        beq     LB4AF
        iny
        bne     LB48F
        inc     $F3
LB49B:
        lda     ($F2),y
        cmp     $04C5
        beq     LB4AF
        iny
        cpy     #$90
        bne     LB49B
        lda     #$00
        sta     $F2
        sta     $F3
        ldy     #$00
LB4AF:
        jsr     LB501
        lda     #$00
        sta     $04C3
        sta     $04C4
        ldx     $04C0
        lda     $F75F,x
        sta     $F2
        lda     $F760,x
        sta     $F3
        ldy     #$00
LB4C9:
        jsr     LB4E6
        bne     LB4C9
        inc     $F3
LB4D0:
        jsr     LB4E6
        cpy     #$90
        bne     LB4D0
        ldy     #$57
        lda     $04C3
        sta     ($F8),y
        iny
        lda     $04C4
        sta     ($F8),y
        clv
        rts

LB4E6:
        lda     ($F2),y
        cmp     $04C5
        bne     LB4FF
        clc
        sed
        lda     $04C3
        adc     #$01
        sta     $04C3
        lda     $04C4
        adc     #$00
        sta     $04C4
LB4FF:
        iny
        rts

LB501:
        clc
        cld
        tya
        adc     $F2
        sta     $F2
        lda     $F3
        adc     #$00
        sta     $F3
        ldy     #$51
        lda     $F2
        sta     ($F8),y
        iny
        lda     $F3
        sta     ($F8),y
        ldy     #$55
        lda     $F2
        sta     ($F8),y
        iny
        lda     $F3
        sta     ($F8),y
        ldy     #$53
        lda     #$01
        sta     ($F8),y
        iny
        lda     #$00
        sta     ($F8),y
        rts

LB530:
        ldy     #$2B
        lda     ($F8),y
        cmp     #$FF
        beq     LB53D
        lda     #$F0
LB53A:
        jmp     LB3BD

LB53D:
        ldy     #$20
        jsr     LB5C9
        lda     $F486
        ldy     $F487
        jsr     LAF17
        bvs     LB53A
        lda     $F48C
        cmp     #$FF
        bne     LB55D
        jsr     LB7F2
        bvc     LB55D
        lda     #$C9
        bne     LB53A
LB55D:
        clc
        sed
        lda     $F488
        adc     #$01
        sta     $F488
        lda     $F489
        adc     #$00
        sta     $F489
        bcc     LB575
        lda     #$C8
        bne     LB53A
LB575:
        ldy     #$2B
        lda     $F488
        sta     ($F8),y
        iny
        lda     $F489
        sta     ($F8),y
        ldy     #$2D
        lda     $F48D
        sta     ($F8),y
        lda     $F48E
        cmp     #$FF
        beq     LB59C
        sta     $04C5
        jsr     LB0C6
        bvs     LB59C
        lda     #$CB
        bne     LB53A
LB59C:
        ldy     #$30
        jsr     LB5C9
        ldx     $F48C
        lda     $F486
        ldy     $F487
        brk
        eor     ($70),y
        sty     $8AAD
        .byte   $F4
        ldy     $F48B
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$00
LB5BA:
        lda     ($F8),y
        sta     (POIN_F0),y
        iny
        cpy     #$40
        bne     LB5BA
        lda     $F48C
        jmp     LB432

LB5C9:
        ldx     #$00
LB5CB:
        lda     $04A0,x
        sta     ($F8),y
        iny
        inx
        cpx     #$08
        bne     LB5CB
        ldx     #$02
        jsr     LB5E6
        ldx     #$05
        jsr     LB5E6
        ldx     #$08
        jsr     LB5E6
        rts

LB5E6:
        lda     C3DATE_F424,x
        asl     a
        asl     a
        asl     a
        asl     a
        sta     $0761
        lda     $F425,x
        and     #$0F
        ora     $0761
        sta     ($F8),y
        iny
        rts

LB5FC:
        lda     $04C2
        bne     LB604
        jmp     LB75D

LB604:
        ldy     #$2D
        lda     ($F8),y
        sta     $04C5
        ldy     #$52
        lda     ($F8),y
        bne     LB61D
LB611:
        lda     #$02
LB613:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB61D:
        ldy     #$4E
        lda     ($F8),y
        sta     $04C3
        iny
        lda     ($F8),y
        sta     $04C4
        ldy     #$53
        sec
        sed
        lda     $04C3
        sbc     ($F8),y
        iny
        lda     $04C4
        sbc     ($F8),y
        bcs     LB656
        ldy     #$55
        lda     ($F8),y
        tax
        iny
        lda     ($F8),y
        ldy     #$52
        sta     ($F8),y
        dey
        txa
        sta     ($F8),y
        ldy     #$53
        lda     #$01
        sta     ($F8),y
        iny
        lda     #$00
        sta     ($F8),y
LB656:
        ldy     #$51
        lda     ($F8),y
        sta     POIN_F0
        iny
        lda     ($F8),y
        sta     POIN_F0+1
LB661:
        ldy     #$53
        lda     ($F8),y
        cmp     $04C3
        bne     LB6AE
        iny
        lda     ($F8),y
        cmp     $04C4
        bne     LB6AE
        sec
        cld
        ldy     #$51
        ldx     $04C0
        lda     ($F8),y
        sbc     $F75F,x
        sta     $04C3
        iny
        lda     ($F8),y
        sbc     $F760,x
        sta     $04C4
        ldy     $04C4
        lda     $04C3
        brk
        asl     $8D
        .byte   $C3
        .byte   $04
        sty     $04C4
        ldy     #$4A
        clc
        sed
        lda     $04C3
        adc     #$01
        sta     ($F8),y
        iny
        lda     $04C4
        adc     #$00
        sta     ($F8),y
        jmp     LB6F0

LB6AE:
        inc     POIN_F0
        bne     LB6B4
        inc     POIN_F0+1
LB6B4:
        ldx     $04C0
        sec
        cld
        lda     POIN_F0
        sbc     $F782,x
        lda     POIN_F0+1
        sbc     $F783,x
        bcc     LB6C8
        jmp     LB611

LB6C8:
        ldy     #$00
        lda     (POIN_F0),y
        cmp     $04C5
        bne     LB6AE
        ldy     #$51
        lda     POIN_F0
        sta     ($F8),y
        iny
        lda     POIN_F0+1
        sta     ($F8),y
        ldy     #$53
        sed
        clc
        lda     ($F8),y
        adc     #$01
        sta     ($F8),y
        iny
        lda     ($F8),y
        adc     #$00
        sta     ($F8),y
        jmp     LB661

LB6F0:
        lda     $04C2
        cmp     #$A4
        bne     LB730
        ldy     #$4A
        lda     ($F8),y
        tax
        iny
        lda     ($F8),y
        tay
        txa
        ldx     #$00
        brk
        php
        ldx     $04C0
        clc
        cld
        adc     $F75F,x
        sta     POIN_F0
        tya
        adc     $F760,x
        sta     POIN_F0+1
        sec
        lda     POIN_F0
        sbc     #$01
        sta     POIN_F0
        lda     POIN_F0+1
        sbc     #$00
        sta     POIN_F0+1
        ldy     #$00
        lda     (POIN_F0),y
        cmp     $04C5
        beq     LB730
        lda     #$CE
        jmp     LB613

LB730:
        jsr     LB73E
        lda     $04C2
        sta     $F47A
        jsr     LB0E4
        clv
        rts

LB73E:
        lda     #$5F
        ldy     #$F4
        jsr     Vector_LOCK
        ldy     #$48
        lda     ($F8),y
        tax
        sei
        lda     #$88
        sta     Floppy_MotorSel
        ora     Floppy_DriveSelect,x
        sta     Floppy_MotorSel
        and     #$F6
        sta     Floppy_MotorSel
        cli
        rts

LB75D:
        lda     #$7E
        ldy     #$F4
        jsr     Vector_LOCK
        ldy     #$2D
        lda     ($F8),y
        sta     $04C5
        jsr     LB7AC
        lda     $F3
        bne     LB783
        lda     #$C7
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
LB77D:
        ldx     #$00
        stx     $F47E
        rts

LB783:
        ldy     $0761
        lda     $04C5
        sta     ($F2),y
        ldy     #$57
        clc
        sed
        lda     ($F8),y
        sta     POIN_F0
        adc     #$01
        sta     ($F8),y
        iny
        lda     ($F8),y
        adc     #$00
        sta     ($F8),y
        ora     POIN_F0
        bne     LB7A8
        ldy     $0761
        jsr     LB501
LB7A8:
        clv
        jmp     LB77D

LB7AC:
        ldx     $04C0
        lda     $F75F,x
        sta     POIN_F0
        lda     $F760,x
        sta     POIN_F0+1
        lda     #$00
        sta     $F3
        inc     POIN_F0+1
        clv
        ldy     #$8F
LB7C2:
        jsr     LB7D8
        bvs     LB7D7
        bne     LB7C2
        jsr     LB7D8
        bvs     LB7D7
        dec     POIN_F0+1
LB7D0:
        jsr     LB7D8
        bvs     LB7D7
        bne     LB7D0
LB7D7:
        rts

LB7D8:
        lda     (POIN_F0),y
        bne     LB7E9
        sty     $0761
        lda     POIN_F0
        sta     $F2
        lda     POIN_F0+1
        sta     $F3
LB7E7:
        dey
        rts

LB7E9:
        cmp     $04C5
        bne     LB7E7
        bit     $FC00
        rts

LB7F2:
        lda     $04C5
        pha
        lda     #$02
        sta     $04C5
        jsr     LB7AC
        pla
        sta     $04C5
        bvs     LB808
LB804:
        bit     $FC00
        rts

LB808:
        lda     $F3
        beq     LB804
        jsr     LB89B
        ldy     #$3E
        lda     (POIN_F0),y
        cmp     #$FF
        bne     LB804
        ldy     $0761
        lda     #$02
        sta     ($F2),y
        ldx     $04C0
        sec
        cld
        lda     $F2
        adc     $0761
        sta     $0763
        lda     $F3
        adc     #$00
        sta     $0764
        sec
        lda     $0763
        sbc     $F75F,x
        sta     $0763
        lda     $0764
        sbc     $F760,x
        tay
        lda     $0763
        brk
        asl     $8D
        .byte   $63
        .byte   $07
        sty     $0764
        jsr     LB89B
        ldy     #$00
LB853:
        lda     (POIN_F0),y
        cmp     #$FF
        beq     LB85D
        iny
        iny
        bne     LB853
LB85D:
        lda     $0763
        sta     (POIN_F0),y
        iny
        lda     $0764
        sta     (POIN_F0),y
        jsr     LAFBA
        lda     $04C5
        sta     $F48D
        lda     $F48F
        sta     $F48C
        lda     $F486
        sta     $F48A
        sta     POIN_F0
        lda     $F487
        sta     $F48B
        sta     POIN_F0+1
        ldy     #$00
        lda     #$20
LB88B:
        sta     (POIN_F0),y
        iny
        bne     LB88B
        ldx     $F48C
        lda     POIN_F0
        ldy     POIN_F0+1
        brk
        .byte   $5C
        clv
        rts

LB89B:
        ldx     $04C0
        lda     $F76E,x
        sta     POIN_F0
        lda     $F76F,x
        sta     POIN_F0+1
        rts

        rts

        ldy     #$2B
        lda     ($F8),y
        cmp     #$FF
        beq     LB8CE
        cmp     (POIN_F0),y
        bne     LB8CD
        iny
        lda     ($F8),y
        cmp     (POIN_F0),y
        bne     LB8CD
LB8BD:
        lda     $F48F
        sta     $F483
        lda     POIN_F0
        sta     $F484
        lda     POIN_F0+1
        sta     $F485
LB8CD:
        rts

LB8CE:
        inc     $F48A
        sec
        sed
        lda     (POIN_F0),y
        sbc     $F488
        iny
        lda     (POIN_F0),y
        sbc     $F489
        bcs     LB8CD
        lda     (POIN_F0),y
        sta     $F489
        dey
        lda     (POIN_F0),y
        sta     $F488
        jmp     LB8BD

        inc     $F48A
        lda     $F48F
        sta     $F483
        jsr     LB986
        bvc     LB8FE
        pla
        pla
LB8FE:
        rts

LB8FF:
        lda     #$7E
        ldy     #$F4
        jsr     Vector_LOCK
        lda     #$00
        sta     $F48A
        lda     #$FF
        sta     $F483
        lda     #$99
        sta     $F488
        sta     $F489
        lda     #$A9
        ldy     #$B8
        sta     LF47F
        sty     $F480
        ldy     #$2B
        lda     ($F8),y
        iny
        ora     ($F8),y
        bne     LB92E
        jmp     LB9DE

LB92E:
        lda     #$AA
        ldy     #$B8
        sta     LF481
        sty     $F482
        lda     #$1C
        ldy     #$05
        jsr     LAF17
        bvc     LB94A
LB941:
        ldx     #$00
        stx     $F47E
        bit     $FC00
        rts

LB94A:
        lda     $F483
        cmp     #$FF
        bne     LB955
        lda     #$B0
        bne     LB941
LB955:
        tax
        lda     $F48A
        cmp     #$01
        bne     LB961
        lda     #$B2
        bne     LB941
LB961:
        lda     #$1C
        ldy     #$05
        brk
        eor     ($70),y
        cld
        lda     $F484
        ldy     $F485
        sta     POIN_F0
        sty     POIN_F0+1
        jsr     LB986
        bvs     LB941
        jsr     LAFBA
        cmp     #$01
        bne     LB941
        ldx     #$00
        stx     $F47E
        clv
        rts

LB986:
        ldy     #$2D
        lda     (POIN_F0),y
        sta     $04C5
        jsr     LB0C6
        bvs     LB998
        lda     #$CB
LB994:
        bit     $FC00
        rts

LB998:
        ldy     #$48
        lda     ($F8),y
        asl     a
        tax
        lda     $F75F,x
        sta     $F2
        lda     $F760,x
        sta     $F3
        ldy     #$00
LB9AA:
        jsr     LBA0B
        bne     LB9AA
        inc     $F3
LB9B1:
        jsr     LBA0B
        cpy     #$90
        bne     LB9B1
        ldy     #$00
        lda     #$20
LB9BC:
        sta     (POIN_F0),y
        iny
        cpy     #$40
        bcc     LB9BC
        lda     POIN_F0
        pha
        lda     POIN_F0+1
        pha
        ldx     $F483
        lda     #$1C
        ldy     #$05
        brk
        .byte   $5C
        tax
        pla
        sta     POIN_F0+1
        pla
        sta     POIN_F0
        txa
        bvs     LB994
        clv
        rts

LB9DE:
        lda     #$EE
        ldy     #$B8
        sta     LF481
        sty     $F482
        lda     #$1C
        ldy     #$05
        jsr     LAF17
        bvc     LB9F4
LB9F1:
        jmp     LB941

LB9F4:
        lda     $F48A
        bne     LB9FD
        lda     #$B0
        bne     LB9F1
LB9FD:
        jsr     LAFBA
        cmp     #$01
        bne     LB9F1
        ldx     #$00
        stx     $F47E
        clv
        rts

LBA0B:
        lda     ($F2),y
        cmp     $04C5
        bne     LBA16
        lda     #$00
        sta     ($F2),y
LBA16:
        iny
        rts

        bit     $FE4C
        eor     #$4E
        eor     $FF
        .byte   $FF
; 0xBA20
Command_Copy:
        ldx     #$04
        brk
        .byte   $30
        bvc     LBA2B
LBA26:
        bit     $FC00
        clc
        rts

LBA2B:
        sta     $025E
        sty     PANIC_SRCL_25F
        sta     POINC1_F4
        sty     POINC1_F4+1
        inc     POINC1_F4+1
        sta     POINC2_F6
        sty     POINC2_F6+1
        inc     POINC2_F6+1
        clc
        cld
        lda     POINC2_F6
        adc     #$80
        sta     POINC2_F6
        bvc     LBA49
        inc     POINC2_F6+1
LBA49:
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$01
        brk
        clc
        bvc     LBA69
        jmp     LBCF4

LBA56:
        lda     #$A1
LBA58:
        pha
        lda     $025E
        ldy     PANIC_SRCL_25F
        ldx     #$04
        brk
        and     ($68),y
        bne     LBA26
        clv
        clc
        rts

LBA69:
        ldy     #$40
        lda     (POINC1_F4),y
        cmp     #$03
        beq     LBA75
        cmp     #$01
        bne     LBA56
LBA75:
        brk
        ora     ($50),y
        asl     $01A9
        ldy     #$40
        sta     (POINC2_F6),y
        lda     #$00
        sta     $027D
        jmp     LBAA9

        cmp     #$2C
        bne     LBA56
        lda     POINC2_F6
        ldy     POINC2_F6+1
        ldx     #$02
        brk
        clc
        bvs     LBA56
        lda     #$00
        sta     $027D
        brk
        stx     $18,y
        tsx
        bvs     LBAA5
        lda     #$01
        sta     $027D
LBAA5:
        brk
        .byte   $1B
        bvs     LBA56
LBAA9:
        ldy     #$40
        lda     (POINC1_F4),y
        cmp     #$03
        bne     LBAB7
        lda     (POINC2_F6),y
        cmp     #$03
        beq     LBABA
LBAB7:
        jmp     LBBB5

LBABA:
        lda     $04AA
        bne     LBAE0
        ldy     #$00
LBAC1:
        lda     (POINC1_F4),y
        cmp     #$2E
        beq     LBAD7
        cmp     (POINC2_F6),y
        beq     LBACF
        lda     #$D4
        bne     LBA58
LBACF:
        iny
        cpy     #$20
        bcc     LBAC1
        jmp     LBA56

LBAD7:
        lda     (POINC2_F6),y
        cmp     #$2E
        beq     LBAE0
        jmp     LBA56

LBAE0:
        ldy     #$4C
        lda     $025E
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        iny
        lda     PANIC_SRCL_25F
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        ldy     #$4E
        lda     #$01
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        ldx     #$01
        brk
        .byte   $52
        bvc     LBB06
        jmp     LBA58

LBB06:
        ldy     #$2E
        lda     (POINC1_F4),y
        sta     (POINC2_F6),y
        lda     POINC2_F6
        ldy     POINC2_F6+1
        ldx     #$02
        brk
        .byte   $52
        bvc     LBB25
LBB16:
        sta     $0264
LBB19:
        lda     POINC1_F4
        ldy     POINC1_F4+1
        brk
        .byte   $53
        lda     $0264
        jmp     LBA58

LBB25:
        lda     POINC1_F4
        ldy     POINC1_F4+1
        brk
        .byte   $54
        bvs     LBB2F
        brk
        .byte   $50
LBB2F:
        bvc     LBB62
        cmp     #$02
        bne     LBB4A
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        .byte   $53
        bvs     LBB16
        lda     POINC1_F4
        ldy     POINC1_F4+1
        brk
        .byte   $53
        bvs     LBB47
        lda     #$00
LBB47:
        jmp     LBA58

LBB4A:
        sta     $0264
        lda     #$00
        ldy     #$6E
        sta     (POINC2_F6),y
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        .byte   $53
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        .byte   $5B
        jmp     LBB19

LBB62:
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        .byte   $5A
        bvs     LBB4A
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        eor     $70,x
        .byte   $02
        brk
        bvc     LBBE5
        .byte   $D4
        brk
        .byte   $07
        bvc     LBB7F
        lda     #$09
        jmp     LBB4A

LBB7F:
        ldy     #$4E
        clc
        sed
        lda     (POINC1_F4),y
        adc     #$01
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        iny
        lda     (POINC1_F4),y
        adc     #$00
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        jmp     LBB25

        rol     $20
        jsr     L5826
        bit     $FF
        jsr     L2020
        rol     $55
        jsr     L2620
        .byte   $44
        jsr     L2620
        .byte   $54
        jsr     L5020
        eor     ($47,x)
        eor     $20
        rol     $50
        .byte   $FF
LBBB5:
        ldy     #$41
        lda     $025E
        sta     (POINC1_F4),y
        clc
        cld
        adc     #$80
        sta     (POINC2_F6),y
        sta     PANIC_SRCH_260
        iny
        lda     PANIC_SRCL_25F
        adc     #$00
        sta     (POINC2_F6),y
        sta     $0261
        lda     PANIC_SRCL_25F
        sta     (POINC1_F4),y
        lda     POINC1_F4
        ldy     POINC1_F4+1
        sta     $0266
        sty     $0267
        ldx     #$01
        brk
        .byte   $42
        bvs     LBBF9
LBBE5:
        lda     POINC2_F6
        ldy     POINC2_F6+1
        ldx     #$02
        brk
        .byte   $42
        bvc     LBBFC
LBBEF:
        pha
        lda     $0266
        ldy     $0267
        brk
        .byte   $43
        pla
LBBF9:
        jmp     LBA58

LBBFC:
        lda     #$7E
        ldy     #$02
        brk
        jsr     L2700
        brk
        ldx     #$97
        .byte   $BB
        lda     POINC1_F4
        ldy     POINC1_F4+1
        brk
        and     $A9
        bit     _ZERO_
        and     ($A9,x)
        jsr     L2300
        brk
        ldx     #$9E
        .byte   $BB
        ldy     #$63
        lda     #$7E
        sta     (POINC2_F6),y
        iny
        lda     #$02
        sta     (POINC2_F6),y
        ldy     #$2E
        lda     (POINC1_F4),y
        cmp     #$02
        bne     LBC38
        sta     (POINC2_F6),y
        ldy     #$6D
        sta     (POINC1_F4),y
        lda     #$00
        jmp     LBC3F

LBC38:
        and     $027D
        ldy     #$6D
        sta     (POINC1_F4),y
LBC3F:
        sta     $0262
        lda     PANIC_SRCH_260
        ldy     $0261
        brk
        jsr     LF085
        sty     POIN_F0+1
        ldy     #$00
        lda     #$80
        sta     (POIN_F0),y
        lda     $025E
        ldy     PANIC_SRCL_25F
        sta     POINC1_F4
        sty     POINC1_F4+1
        ldy     #$00
        lda     #$80
        sta     (POINC1_F4),y
LBC64:
        lda     $0266
        ldy     $0267
        brk
        .byte   $44
        bvs     LBC70
        brk
        rti

LBC70:
        bvc     LBC87
LBC72:
        pha
        ldy     #$6E
        lda     #$00
        sta     (POINC2_F6),y
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        .byte   $43
        lda     POINC2_F6
        brk
        .byte   $5B
        pla
        jmp     LBBEF

LBC87:
        brk
        .byte   $27
        ldy     #$02
        lda     (POINC1_F4),y
        tax
        lda     #$00
        cpx     #$03
        beq     LBCCF
        stx     $0263
        ldy     #$03
        lda     $0262
        beq     LBCAE
        brk
        .byte   $2B
        brk
        .byte   $2B
        lda     (POINC1_F4),y
        brk
        .byte   $23
        iny
        lda     (POINC1_F4),y
        brk
        .byte   $23
        brk
        .byte   $2B
        iny
LBCAE:
        cpy     $0263
        bcs     LBCBA
        lda     (POINC1_F4),y
        iny
        brk
        and     ($50,x)
        .byte   $F4
LBCBA:
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        eor     $70
        .byte   $02
        brk
        rti

        bvs     LBC72
        brk
        .byte   $07
        bvc     LBC64
        lda     #$09
        jmp     LBC72

LBCCF:
        lda     POINC2_F6
        ldy     POINC2_F6+1
        brk
        .byte   $43
        bvc     LBCE0
        pha
        lda     POINC2_F6
        brk
        .byte   $5B
        pla
        jmp     LBBEF

LBCE0:
        lda     $0266
        ldy     $0267
        brk
        .byte   $43
        bvs     LBCEC
        lda     #$00
LBCEC:
        jmp     LBA58

LBCEF:
        lda     #$A1
LBCF1:
        jmp     LBA58

LBCF4:
        lda     #$2A
        ldx     #$01
        brk
        .byte   $1F
        bvs     LBCEF
        cmp     #$FF
        bne     LBD04
LBD00:
        lda     #$21
        bne     LBCF1
LBD04:
        ldy     #$48
        sta     (POINC1_F4),y
        asl     a
        asl     a
        asl     a
        tax
        lda     $F700,x
        cmp     #$25
        bne     LBD00
        lda     #$2C
        brk
        .byte   $12
        bvs     LBCEF
        lda     #$2A
        ldx     #$01
        brk
        .byte   $1F
        bvs     LBCEF
        cmp     #$FF
        beq     LBD00
        ldy     #$48
        cmp     (POINC1_F4),y
        beq     LBCEF
        sta     (POINC2_F6),y
        asl     a
        asl     a
        asl     a
        tax
        lda     $F700,x
        cmp     #$25
        bne     LBD00
        brk
        .byte   $1B
        bvs     LBCEF
        ldy     #$48
        lda     (POINC1_F4),y
        asl     a
        tax
        lda     $F755,x
        sta     POIN_F0
        lda     $F756,x
        sta     POIN_F0+1
        lda     (POINC2_F6),y
        asl     a
        tax
        lda     $F755,x
        sta     $F2
        lda     $F756,x
        sta     $F3
        ldy     #$00
LBD5C:
        lda     (POIN_F0),y
        cmp     ($F2),y
        bne     LBD84
        iny
        cpy     #$08
        bcc     LBD5C
        ldy     #$2B
        lda     ($F2),y
        cmp     #$E5
        beq     LBD89
        lda     (POIN_F0),y
        cmp     #$E5
        beq     LBD84
        ldy     #$2B
        sec
        sed
        lda     ($F2),y
        sbc     (POIN_F0),y
        iny
        lda     ($F2),y
        sbc     (POIN_F0),y
        bcc     LBD89
LBD84:
        lda     #$D1
        jmp     LBCF1

LBD89:
        lda     _ZERO_
        ldy     $01
        sta     PANIC_SRCH_260
        sty     $0261
        ldy     #$48
        lda     (POINC1_F4),y
        asl     a
        tax
        lda     $F75F,x
        sta     _ZERO_
        lda     $F760,x
        sta     $01
        ldy     #$4A
        lda     #$01
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        ldy     #$4C
        lda     $025E
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        iny
        lda     PANIC_SRCL_25F
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
LBDBC:
        ldy     #$00
        lda     (_ZERO_),y
        beq     LBDDE
        cmp     #$FF
        beq     LBDDE
        lda     POINC1_F4
        ldy     POINC1_F4+1
        brk
        lsr     $70,x
        .byte   $02
        brk
        bvc     LBE41
        and     $F6A5,x
        ldy     POINC2_F6+1
        brk
        .byte   $57
        bvs     LBDDC
        brk
        .byte   $50
LBDDC:
        bvs     LBE0F
LBDDE:
        inc     _ZERO_
        bne     LBDE4
        inc     $01
LBDE4:
        clc
        sed
        ldy     #$4A
        lda     (POINC1_F4),y
        adc     #$01
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        iny
        lda     (POINC1_F4),y
        adc     #$00
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        ldy     #$48
        lda     (POINC1_F4),y
        asl     a
        tax
        lda     $F782,x
        cmp     _ZERO_
        bne     LBDBC
        lda     $F783,x
        cmp     $01
        bne     LBDBC
        lda     #$00
LBE0F:
        pha
        lda     PANIC_SRCH_260
        ldy     $0261
        sta     _ZERO_
        sty     $01
        pla
        jmp     LBCF1

        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
LBE41:
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $D2
