; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/MUC_Board_TGM/TGM_lo.bin
; Page:       1


        .setcpu "6502"

Vector_Reset    := $8000                        ; 0x8000
Vector_IRQ7     := $8003                        ; 0x8003
Vector_IRQ6_MUC := $8006                        ; 0x8006
Vector_IRQ5_USR1234_SER2_PRINTER:= $8009        ; 0x8009
Vector_IRQ4_USR3_SER1:= $800C                   ; 0x800c
Vector_IRQ3_USR2_SER1:= $800F                   ; 0x800f
Vector_IRQ2_USR14_SER1:= $8012                  ; 0x8012
Vector_IRQ1_FLOPPY:= $8015                      ; 0x8015
Vector_IRQ0_BRK := $8018                        ; 0x8018
Vector_NMI0_1   := $801B                        ; 0x801b
Floppy_DMA_Read_in_RAM_0xF7A0:= $F7A0           ; 0xf7a0
Floppy_DMA_Write_in_RAM_0xF7D0:= $F7D0          ; 0xf7d0
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
; 0xfc00
dn:
        .byte   $40
; 0xfc01
HexConv:
        .byte   "0123456789ABCDEF"

; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
Floppy_DriveSelect:
        .byte   $40,$20,$10,$04,$02
; 0xfc16 - Low Bit Switches on Bank
Bank_Switch_Values_FC16:
        .byte   $FE,$FD,$FB,$F7,$EF,$DF,$BF,$7F
; 0xfc1e
Convert:
        .byte   $01,$02,$04,$08,$10,$20,$40,$80
; 0xfc26 goes to 206,207,209,20A,20B,20C,20D,20E  208=1
UserData_1:
        .byte   $7E,$01
        .addr   Serial_1U0_DataReg
        .addr   Serial_0U0_DataReg
        .addr   SerialDIPSwitchesU0
; 0xfc2e
UserData_2:
        .byte   $7D,$02
        .addr   Serial_1U1_DataReg
        .addr   Serial_0U1_DataReg
        .addr   SerialDIPSwitchesU1
; 0xfc36
UserData_3:
        .byte   $7B,$03
        .addr   Serial_1U2_DataReg
        .addr   Serial_0U2_DataReg
        .addr   SerialDIPSwitchesU2
; 0xfc3e
UserData_4:
        .byte   $77,$04
        .addr   Serial_1U3_DataReg
        .addr   Serial_0U3_DataReg
        .addr   SerialDIPSwitchesU3
; 0xfc46
UserData_5:
        .byte   $6F,$05
        .addr   Serial_1U4_DataReg
        .addr   Serial_0U4_DataReg
        .addr   SerialDIPSwitchesU4
; 0xfc4e
UserData_6:
        .byte   $5F,$06
        .addr   Serial_1U5_DataReg
        .addr   Serial_0U5_DataReg
        .addr   SerialDIPSwitchesU5
; 0xfc56
dn_fc56:
        .word   $473F,$FFFF,$FFFF,$FFFF
; SpecialCharacters WTHSFT 0xfc5e
BS:
        .byte   $15
CVOR:
        .byte   $06
DUP:
        .byte   $5F
ENTER:
        .byte   $0D
RESET:
        .byte   $0A
ENDL:
        .byte   $1A
CLEAR:
        .byte   $7F
DEL:
        .byte   $5B
INS:
        .byte   $5D
TAB:
        .byte   $5E
STAB:
        .byte   $14
CLTAB:
        .byte   $03
DUPTAB:
        .byte   $5C
SLOW:
        .byte   $13
ESCAPE:
        .byte   $1B
STOP:
        .byte   $20
START:
        .byte   $20
THVIDL:
        .byte   $15
THVIDC:
        .byte   $15
THSPAC:
        .byte   $FF
THSLOS:
        .byte   $00
THSTPS:
        .byte   $00
THUPCS:
        .byte   $00
; CHAR_per_LINE
WPHDFN:
        .byte   $50
SHIFT:
        .byte   $00
LINES_per_PAGE:
        .byte   $48
HEADLINE:
        .byte   $03
first_DATALINE:
        .byte   $07
ammount_DATALINE:
        .byte   $3E
; 12 msec Step-Time
VTSTEL:
        .byte   $FF
filler:
        .byte   $FF
; 0xfc7d
Clear_Pins_nB_HB:
Clear_Pins_nB_LB:= * + 1                        ; 0xfc7e
        .addr   Pin_6B_Low
        .addr   Pin_5B_Low
        .addr   Pin_4B_Low
        .addr   Pin_3B_Low
; 0xfc85
Set_Pins_nB_HB:
Set_Pins_nB_LB  := * + 1                        ; 0xfc86
        .addr   Pin_6B_High
        .addr   Pin_5B_High
        .addr   Pin_4B_High
        .addr   Pin_3B_High
; 0xfc8d
ErrorMsg:
        .byte   $02
        .byte   "END OF FIL"

        .byte   $C5,$03
        .byte   "STACK OVERFLO"

        .byte   $D7,$06
        .byte   "ERRFLAG SE"

        .byte   $D4,$07
        .byte   "MEMORY NOT AVAILABL"


        .byte   $C5,$09
        .byte   "ESCAPE"
        .byte   $C4,$10
        .byte   "ENTER BUS"

        .byte   $D9,$12
        .byte   "IN ENTER NOT ALLOWE"


        .byte   $C4,$13
        .byte   "IN MULTI-USER NOT ALLOWE"


        .byte   $C4
        .byte   "!NOT OR WRONG MOUNTE"


        .byte   $C4,$22
        .byte   "WRONG FLOPPY-NAM"

        .byte   $C5
        .byte   "0TSYS INO"

        .byte   $D0,$A0
        .byte   "ILLEGAL CM"

        .byte   $C4,$A1
        .byte   "ILLEGAL PARAM"

        .byte   $D3,$A4
        .byte   "OUT OF MEMOR"

        .byte   $D9,$A5
        .byte   "UNRESOLVED EXTERN"


        .byte   $D3,$A7
        .byte   "MODUL WITH ERROR"

        .byte   $D3,$A8
        .byte   "PC > $F"
        .byte   $C6,$A9
        .byte   "PC > $FFF"

        .byte   $C6,$AA
        .byte   "MEMORY ERRO"

        .byte   $D2,$B0
        .byte   "NOT IN CATALO"

        .byte   $C7,$B1
        .byte   "ILLEGAL FILE-TY"

        .byte   $D0,$B2
        .byte   "LAST VERSIO"

        .byte   $CE,$C0
        .byte   "DEVICE BUS"

        .byte   $D9,$C1
        .byte   "DEVICE INO"

        .byte   $D0,$C3
        .byte   "CRC ERRO"
        .byte   $D2,$C5
        .byte   "WRITE PROTECTE"

        .byte   $C4,$C7
        .byte   "FLOPPY FUL"

        .byte   $CC,$CB
        .byte   "FILE IS LOCKE"

        .byte   $C4,$D1
        .byte   "COPY ERRO"

        .byte   $D2,$D2
        .byte   "ERASE ERRO"

        .byte   $D2,$D3
        .byte   "DISMOUNT ERRO"

        .byte   $D2,$D4
        .byte   "PREFIX ERRO"

        .byte   $D2,$E9
        .byte   "SYS-MEMORY OVERFLO"


        .byte   $D7,$F4
        .byte   "SEEK ERRO"

        .byte   $D2,$F5
        .byte   "SECTOR NOT FOUN"

        .byte   $C4,$F7
        .byte   "FLOPPY NOT FORMATE"


        .byte   $C4
; 0xFE9C
SKIP:
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF
; 0xFEFF
CHECKSUM:
        .byte   $91
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
; 0xff07
ServerName:
        .byte   "ERTL    "
; 0xff0f
FirmwareVersion1:
        .byte   $02
; 0xff10
FirmwareVersion2:
        .byte   $65
; 0xFF11
SpecialCharacter:
        .byte   "@"
; 0xFF12
dn_FF12:
        .byte   $08,$15,$1F,$0D,$0C,$1A,$18,$06
        .byte   $04,$09,$1C,$1D,$1E,$13,$7F,$20
        .byte   $20,$15,$15,$00,$00,$00,$00,$84
        .byte   $00,$00,$03,$05,$41
; 0xFF2F
SKIP:
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF
; 0xffe0
IRQ7_Vector:
        .addr   Vector_IRQ7
; 0xffe2
IRQ6_Vector:
        .addr   Vector_IRQ6_MUC
; 0xffe4
IRQ5_Vector:
        .addr   Vector_IRQ5_USR1234_SER2_PRINTER
; 0xffe6
IRQ4_Vector:
        .addr   Vector_IRQ4_USR3_SER1
; 0xffe8
IRQ3_Vector:
        .addr   Vector_IRQ3_USR2_SER1
; 0xffea
IRQ2_Vector:
        .addr   Vector_IRQ2_USR14_SER1
; 0xffec
IRQ1_Vector:
        .addr   Vector_IRQ1_FLOPPY
; 0xffee
IRQ0_Vector:
        .addr   Vector_IRQ0_BRK
; 0xfff0
NMI3_Vector:
        .addr   Floppy_DMA_Read_in_RAM_0xF7A0
; 0xfff2
NMI2_Vector:
        .addr   Floppy_DMA_Write_in_RAM_0xF7D0
; 0xfff4
NMI1_Vector:
        .addr   Vector_NMI0_1
; 0xfff6
NMI0_Vector:
        .addr   Vector_NMI0_1
; 0xfff8
Dead_Vector:
        .addr   ResetVector1+1
; 0xfffa - not used
Ex_NMIVector:
        .addr   ResetVector1+1
; 0xfffc
ResetVector0:
        .addr   Vector_Reset
; 0xfffe
ResetVector1:
        .addr   Vector_Reset
