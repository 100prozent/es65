; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/ROM_Board/P0.bin
; Page:       1


        .setcpu "6502"

_ZERO_          := $0000                        ; 0x0000
L0026           := $0026
L00C6           := $00C6
L00CC           := $00CC
POIN_F0         := $00F0
POINC1_F4       := $00F4
POINC2_F6       := $00F6
POINI_FA        := $00FA
InterruptControl_Reg:= $00FE
StackTail_211   := $0211
JCSREG_0212     := $0212
JumpAddrL_221   := $0221
JumpAddrH_222   := $0222
BankTemp_22D    := $022D
BRK_TRAP_22F    := $022F
BrkSRC_ROM_23C_FF:= $023C
PANIC_SRCL_25F  := $025F
PANIC_SRCH_260  := $0260
Status_Serial2_Backup_03e5:= $03E5
UART_Flags_03e7 := $03E7
StoreA_in_SerialCommandReg:= $03E8
A_from_to_SerialRX_TX_Reg:= $03EC
SpecialCharacters_Copy:= $03F0
L05D2           := $05D2
FunctionByte_afterBRK_728:= $0728
L077A           := $077A
L2100           := $2100
L2700           := $2700
L37AD           := $37AD
L5953           := $5953
Vector_CALL     := $801E                        ; 0x801e
Vector_CREATE   := $8021                        ; 0x8021
Vector_DISMOUNT := $8024                        ; 0x8024
Vector_DRIVE    := $8027                        ; 0x8027
Vector_FHIRQRST := $802A                        ; 0x802A
Vector_FHIRQRW  := $802D                        ; 0x802D
Vector_FHIRQSK  := $8030                        ; 0x8030
Vector_FHNMIADR := $8033                        ; 0x8033
Vector_FHNMIRW  := $8036                        ; 0x8036
Vector_LOCK     := $8039                        ; 0x8039
Vector_LOGON    := $803C                        ; 0x803c
Vector_LODPOIS2 := $803F                        ; 0x803F
Vector_MYINIT   := $8042                        ; 0x8042
Vector_OUTAUS   := $8045                        ; 0x8045
Vector_OUTVOR   := $8048                        ; 0x8048
Vector_PROGTERM := $804B                        ; 0x804b
Vector_SCHEDUL  := $804E                        ; 0x804e
Vector_SVC60    := $8051                        ; 0x8051
Vector_SVC61    := $8054                        ; 0x8054
Vector_SVC62    := $8057                        ; 0x8057
Vector_SVC63    := $805A                        ; 0x805a
Vector_SVC64    := $805D                        ; 0x805d
Vector_SVC65    := $8060                        ; 0x8060
Vector_TASKTERM := $8063                        ; 0x8063
Vector_THPRMPTE := $8066                        ; 0x8066
Vector_THWRITEE := $8069                        ; 0x8069
Vector_LOFRUP   := $806C                        ; 0x806c
Vector_SVC0D    := $806F                        ; 0x806f
LC610           := $C610
PORT_B_Shadow_f3e8:= $F3E8
PH3TSK_F400_PRINTER_BUSY:= $F400
C3USAD_F41A     := $F41A
C3USAD_P3_F41D  := $F41D
C3ENT_F420      := $F420
Users_Installed_F421:= $F421                    ; 0xf421
C3USNR_F422     := $F422
SingleMultiMode_f423:= $F423
C3DATE_F424     := $F424                        ; 0xf424
C3TIME_F42E     := $F42E                        ; 0xf42e
C3TI20_F436     := $F436                        ; 0xf436
Bank_Shadow_F438:= $F438
C3LGNZ_F43C_USERS:= $F43C
MY3MAP_F448     := $F448
MY3MAP_P1_F449  := $F449
MY3MAP_P2_F44A  := $F44A
MY3MAP_P3_F44B  := $F44B
IPSW_F4AB       := $F4AB
IPANZ_F4AC_INIT_IPC:= $F4AC
IPADR_F4AE      := $F4AE
IPADR_P1_F4AF   := $F4AF
NMIURS_M1       := $F7FE
NMIURS          := $F7FF
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
Floppy_DriveSelect:= $FC11                      ; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
; 0xa000
Addr_A000:
        .addr   _ZERO_
        .addr   dont_know_01
        .addr   dont_know_02
        .addr   dont_know_03
        .addr   dont_know_04
        .addr   dont_know_05
        .addr   dont_know_06
        .addr   dont_know_07
        .addr   dont_know_08
        .addr   dont_know_09
        .addr   dont_know_0A
        .addr   dont_know_0B
        .addr   dont_know_0C
        .addr   Vector_SVC0D
        .addr   dont_know_0E
        .addr   _ZERO_
        .addr   dont_know_10
        .addr   Text_from_Console_11
        .addr   dont_know_12
        .addr   dont_know_13
        .addr   dont_know_14
        .addr   dont_know_15
        .addr   dont_know_16
        .addr   dont_know_17
        .addr   dont_know_18
        .addr   dont_know_19
        .addr   dont_know_1A
        .addr   dont_know_1B
        .addr   dont_know_1C
        .addr   dont_know_1D
        .addr   dont_know_1E
        .addr   dont_know_1F
        .addr   dont_know_20
        .addr   Character_to_Console_21
        .addr   dont_know_22
        .addr   OUTHEX_23
        .addr   OUTBIL_24
        .addr   Text_to_Console_25
        .addr   OUTEX1
        .addr   dont_know_27
        .addr   Time_to_Console_28
        .addr   Date_to_Console_29
        .addr   dont_know_2A
        .addr   dont_know_2B
        .addr   dont_know_2C
        .addr   OUTHEL_2D
        .addr   OUTHEH_2E
        .addr   OUTBIH_2F
        .addr   dont_know_30
        .addr   dont_know_31
        .addr   dont_know_32
        .addr   dont_know_33
        .addr   dont_know_34
        .addr   dont_know_35
        .addr   dont_know_36
        .addr   dont_know_37
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   dont_know_40
        .addr   dont_know_41
        .addr   OPEN_42
        .addr   CLOSE_43
        .addr   dont_know_44
        .addr   dont_know_45
        .addr   dont_know_46
        .addr   dont_know_47
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   dont_know_50
        .addr   dont_know_51
        .addr   dont_know_52
        .addr   dont_know_53
        .addr   dont_know_54
        .addr   dont_know_55
        .addr   dont_know_56
        .addr   dont_know_57
        .addr   dont_know_58
        .addr   dont_know_59
        .addr   dont_know_5A
        .addr   dont_know_5B
        .addr   dont_know_5C
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   Vector_SVC60
        .addr   Vector_SVC61
        .addr   Vector_SVC62
        .addr   Vector_SVC63
        .addr   Vector_SVC64
        .addr   Vector_SVC65
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
        .addr   _ZERO_
; 0xA0E0
GetAddress_afterFunction:
        ldx     StackTail_211
        lda     $0104,x
        sta     $F2
        lda     $0101,x
        sta     $F3
        rts

; 0xA0EE------------------------------
dont_know_01:
        ldx     StackTail_211
        lda     $0107,x
        sta     PANIC_SRCH_260
        lda     $0106,x
        sta     PANIC_SRCL_25F
        lda     $0104,x
        jmp     Vector_PROGTERM

; 0xA103------------------------------
dont_know_02:
        jsr     GetAddress_afterFunction
        ldy     #$00
        lda     $0207
        sta     ($F2),y
        ldx     #$07
        ldy     #$08
LA111:
        lda     $04A0,x
        sta     ($F2),y
        dey
        dex
        bpl     LA111
        ldy     #$09
        ldx     #$02
        jsr     LA1A3
        ldx     #$05
        jsr     LA1A3
        ldx     #$08
        jsr     LA1A3
        ldy     #$0C
        ldx     #$0A
        jsr     LA1A3
        ldx     #$0D
        jsr     LA1A3
        ldx     #$10
        jsr     LA1A3
        ldy     #$0F
        lda     SingleMultiMode_f423
        sta     ($F2),y
        ldy     #$10
        ldx     #$00
        lda     $0236
        cmp     #$01
        beq     LA150
        ldx     #$40
LA150:
        bit     $0207
        bvc     LA157
        ldx     #$80
LA157:
        txa
        sta     ($F2),y
        ldy     #$11
        lda     $0216
        sta     ($F2),y
        ldy     #$12
        lda     $049C
        sta     ($F2),y
        iny
        lda     $049D
        sta     ($F2),y
        ldy     #$14
        lda     $049E
        sta     ($F2),y
        iny
        lda     $049F
        sta     ($F2),y
        ldy     #$16
        lda     $04AA
        sta     ($F2),y
        ldy     #$17
        lda     $04AB
        sta     ($F2),y
        ldy     #$18
        lda     #$65
        sta     ($F2),y
        ldy     #$19
        lda     $F445
        sta     ($F2),y
        iny
        lda     $F446
        sta     ($F2),y
        iny
        lda     $F447
        sta     ($F2),y
        rts

LA1A3:
        lda     $F425,x
        and     #$0F
        sta     $0729
        lda     C3DATE_F424,x
        asl     a
        asl     a
        asl     a
        asl     a
        ora     $0729
        sta     ($F2),y
        iny
        rts

; 0xa1b9
Text_System:
        .byte   " SYSTEM "
; 0xA1C1------------------------------
dont_know_03:
        ldx     #$07
LA1C3:
        lda     $0218,x
        sta     $023E,x
        lda     Text_System,x
        sta     $0218,x
        dex
        bpl     LA1C3
        lda     $04A8
        pha
        lda     #$00
        sta     $04A8
        lda     #$00
        ldy     #$A0
        ldx     #$02
        jsr     Vector_CALL
        bvc     LA1EC
        ldx     StackTail_211
        sta     $0104,x
LA1EC:
        ldx     #$07
LA1EE:
        lda     $023E,x
        sta     $0218,x
        dex
        bpl     LA1EE
        pla
        sta     $04A8
        rts

; 0xA1FC------------------------------
dont_know_04:
        lda     #$00
        sei
        sta     $0213
LA202:
        pla
        pla
        pla
        sta     $0220
        pla
        pla
        pla
        sta     StackTail_211
        tsx
        stx     JCSREG_0212
        jmp     Vector_SCHEDUL

; 0xA215------------------------------
dont_know_05:
        lda     #$FF
        sei
        sta     $0213
        ldx     StackTail_211
        lda     $0104,x
        sta     $0214
        lda     $0101,x
        sta     $0215
        jmp     LA202

; 0xa22d
dna22d:
        .byte   $01,$16,$56,$96
; 0xa231
dna231:
        .byte   $00,$00,$02,$40
; 0xA235------------------------------
dont_know_06:
        ldx     StackTail_211
        lda     $0104,x
        sta     $072B
        lda     $0101,x
        sta     $072A
        lda     #$00
        sta     $072C
        sta     $072D
        sta     $072E
        sed
        ldx     #$00
LA252:
        lda     $072B
        and     #$0F
        tay
        lsr     $072A
        ror     $072B
        lsr     $072A
        ror     $072B
        lsr     $072A
        ror     $072B
        lsr     $072A
        ror     $072B
        cpy     #$00
        beq     LA292
LA274:
        clc
        lda     $072E
        adc     dna22d,x
        sta     $072E
        lda     $072D
        adc     dna231,x
        sta     $072D
        lda     $072C
        adc     #$00
        sta     $072C
        dey
        bne     LA274
LA292:
        inx
        cpx     #$04
        bne     LA252
        ldx     StackTail_211
        lda     $072C
        sta     $0102,x
        lda     $072D
        sta     $0101,x
        lda     $072E
        sta     $0104,x
        clv
        rts

; 0xA2AE------------------------------
dont_know_07:
        bit     $0207
        bvc     LA2BE
        sei
        bit     $F43F
        lda     #$00
        sta     $F43F
        cli
        rts

LA2BE:
        lda     UART_Flags_03e7
        and     #$01
        beq     LA2D2
        sei
        lda     UART_Flags_03e7
        and     #$FE
        sta     UART_Flags_03e7
        cli
        bit     $FC00
LA2D2:
        rts

; 0xA2D3------------------------------
dont_know_08:
        ldx     StackTail_211
        lda     $0102,x
        sta     $073B
        lda     $0101,x
        sta     $073C
        lda     $0104,x
        sta     $073D
        jsr     LA7E2
        bvs     LA2FC
        ldx     StackTail_211
        lda     $073C
        sta     $0101,x
        lda     $073D
        sta     $0104,x
LA2FC:
        rts

; 0xA2FD------------------------------
dont_know_09:
        lda     BankTemp_22D
        sta     $023D
LA303:
        jsr     GetAddress_afterFunction
        lda     $0204
        ldy     $0205
        sta     POIN_F0
        sty     POIN_F0+1
        lda     $023D
        sta     BankTemp_22D
        ldy     #$00
        jsr     Vector_LODPOIS2
        tax
        iny
        jsr     Vector_LODPOIS2
        ldy     #$42
        sta     (POIN_F0),y
        dey
        txa
        sta     (POIN_F0),y
        ldy     #$02
        jsr     Vector_LODPOIS2
        tax
        iny
        jsr     Vector_LODPOIS2
        ldy     #$44
        sta     (POIN_F0),y
        dey
        txa
        sta     (POIN_F0),y
        ldx     StackTail_211
        lda     $0102,x
        bne     LA345
        jmp     LA3D3

LA345:
        and     #$7F
        cmp     #$44
        bne     LA366
        lda     #$44
        sta     $0246
LA350:
        lda     $0204
        ldy     $0205
        brk
        .byte   $44
        bvc     LA3C9
        cmp     #$E5
        bne     LA3A6
        lda     $0204
        brk
        rti

        jmp     LA350

LA366:
        cmp     #$46
        bne     LA385
        lda     #$46
        sta     $0246
LA36F:
        lda     $0204
        ldy     $0205
; 0xa375 - 2e 
comm:
        brk
        .byte   $46
        bvc     LA3C9
        cmp     #$E5
        bne     LA3A6
        lda     $0204
        brk
        rti

        jmp     LA36F

LA385:
        cmp     #$47
        bne     LA3A4
        lda     #$47
        sta     $0246
LA38E:
        lda     $0204
        ldy     $0205
        brk
        .byte   $47
        bvc     LA3C9
        cmp     #$E5
        bne     LA3A6
        lda     $0204
        brk
        rti

        jmp     LA38E

LA3A4:
        lda     #$F2
LA3A6:
        pha
        jsr     LA571
        brk
        rol     $3E
        jsr     L5953
        .byte   $53
        eor     #$CE
        pla
        tax
        lda     #$00
        ldy     #$00
        brk
        rol     a
        .byte   $20
        .byte   $89
LA3BD:
        lda     $A9
        .byte   $37
        ldy     #$02
        ldx     #$45
        brk
        asl     a
        jmp     LA4C4

LA3C9:
        ldx     StackTail_211
        lda     $0102,x
        bpl     LA3D3
        clv
        rts

LA3D3:
        lda     $0204
        ldy     $0205
        brk
        rti

        bvs     LA3A6
        lda     $0236
        cmp     #$01
        bne     LA3E7
        jmp     LA4C2

LA3E7:
        jsr     LA571
        jsr     GetAddress_afterFunction
        lda     $023D
        sta     BankTemp_22D
        jsr     LA595
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$00
        lda     (POIN_F0),y
        clc
        cld
        adc     #$03
        bpl     LA406
        lda     #$80
LA406:
        sta     $07AE
        lda     POIN_F0
        ldy     POIN_F0+1
        brk
        bpl     LA3BD
        .byte   $37
        .byte   $02
        ldy     $0238
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$00
        lda     $07AE
        sta     (POIN_F0),y
        lda     #$00
        brk
        .byte   $1C
        pha
        brk
        asl     $25A9,x
        brk
        and     ($A9,x)
        jsr     L2100
LA42F:
        brk
        ora     ($70),y
        .byte   $57
        cmp     $0248
        bne     LA47E
        brk
        ora     ($70),y
        pha
        cmp     #$30
        bcc     LA472
        cmp     #$3A
        bcs     LA472
        and     #$0F
        sta     $07AE
        lda     $0249
        ldy     $024A
        sta     InterruptControl_Reg
        sty     $FF
        lda     $07AE
        asl     a
        tay
        lda     (InterruptControl_Reg),y
        sta     $07AE
        iny
        lda     (InterruptControl_Reg),y
        tay
LA461:
        lda     $07AE
        beq     LA42F
        lda     (InterruptControl_Reg),y
        jsr     LA54F
        dec     $07AE
        iny
        jmp     LA461

LA472:
        sta     $07AE
        lda     $0248
        jsr     LA54F
        lda     $07AE
LA47E:
        jsr     LA54F
        jmp     LA42F

        lda     $0248
        jsr     LA54F
        jsr     GetAddress_afterFunction
        lda     $023D
        sta     BankTemp_22D
        jsr     LA595
        brk
        jsr     L37AD
        .byte   $02
        ldy     $0238
        brk
        bpl     LA4A1
LA4A1:
        .byte   $27
        lda     #$00
        brk
        .byte   $1C
        brk
        ora     (_ZERO_),y
        ora     (_ZERO_),y
        ora     (_ZERO_),y
        ora     ($70),y
        .byte   $04
        brk
        and     ($50,x)
        sed
        jsr     LA589
        pla
        beq     LA4C4
        lda     #$37
        ldy     #$02
        ldx     #$45
        brk
        asl     a
LA4C2:
        clv
        rts

LA4C4:
        lda     $0204
        ldy     $0205
        sta     $F8
        sty     $F9
        ldy     #$65
        lda     ($F8),y
        iny
        ora     ($F8),y
        bne     LA4DA
        jmp     Vector_TASKTERM

LA4DA:
        lda     $0204
        ldy     $0205
        brk
        .byte   $43
        lda     $0204
        ldy     $0205
        sta     $F8
        sty     $F9
        ldy     #$65
        lda     ($F8),y
        sta     $0204
        iny
        lda     ($F8),y
        sta     $0205
        iny
        lda     ($F8),y
        sta     $0237
        iny
        lda     ($F8),y
        sta     $0238
        iny
        lda     ($F8),y
        sta     $0236
        cmp     #$01
        bne     LA514
        lda     #$00
        sta     $0239
LA514:
        lda     $0249
        sta     $F2
        lda     $024A
        sta     $F3
        iny
        lda     ($F8),y
        sta     $0248
        iny
        lda     ($F8),y
        sta     $0249
        iny
        lda     ($F8),y
        sta     $024A
        lda     $F2
        ldy     $F3
        beq     LA53A
        ldx     #$01
        brk
        .byte   $31
LA53A:
        lda     $F8
        ldy     $F9
        ldx     #$02
        brk
        and     ($B8),y
        ldx     StackTail_211
        lda     $0246
        sta     $0102,x
        jmp     LA303

LA54F:
        brk
        and     ($70,x)
        ora     ($60,x)
        pla
        pla
        lda     $0237
        ldy     $0238
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$02
        sec
        cld
        lda     (POIN_F0),y
        sbc     #$01
        sta     (POIN_F0),y
        lda     #$3E
        brk
        and     ($4C,x)
        txa
        .byte   $A4
LA571:
        ldx     #$03
LA573:
        lda     $049C,x
        sta     $0232,x
        dex
        bpl     LA573
        lda     $0237
        ldy     $0238
        brk
        jsr     L2700
        brk
        .byte   $2B
        rts

LA589:
        ldx     #$03
LA58B:
        lda     $0232,x
        sta     $049C,x
        dex
        bpl     LA58B
        rts

LA595:
        ldy     #$00
        jsr     Vector_LODPOIS2
        tax
        iny
        jsr     Vector_LODPOIS2
        tay
        txa
        rts

; 0xA5A2------------------------------
dont_know_0A:
        jsr     GetAddress_afterFunction
        lda     $0202
        ldy     $0203
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$00
        jsr     Vector_LODPOIS2
        tax
        iny
        jsr     Vector_LODPOIS2
        ldy     #$42
        sta     (POIN_F0),y
        dey
        txa
        sta     (POIN_F0),y
        ldx     StackTail_211
        lda     $0102,x
        beq     LA5FD
        and     #$7F
        cmp     #$45
        bne     LA5E5
LA5CF:
        lda     $0202
        ldy     $0203
        brk
        eor     $50
        .byte   $1A
        cmp     #$E5
        bne     LA5E7
        lda     $0202
        brk
        rti

        jmp     LA5CF

LA5E5:
        lda     #$F2
LA5E7:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        jmp     LA607

        ldx     StackTail_211
        lda     $0102,x
        bpl     LA5FD
        clv
        rts

LA5FD:
        lda     $0202
        .byte   $AC
        .byte   $03
LA602:
        .byte   $02
        brk
        rti

        bvs     LA5E7
LA607:
        bvs     LA60A
        rts

LA60A:
        php
        bit     $0207
        bvs     LA612
        plp
        rts

LA612:
        plp
        jmp     Vector_TASKTERM

; 0xA616------------------------------
dont_know_0B:
        lda     SingleMultiMode_f423
        bne     LA625
        lda     $04AA
        bne     LA625
        lda     #$13
LA622:
        jmp     LA5E7

LA625:
        lda     BrkSRC_ROM_23C_FF
        bne     LA62E
        lda     #$05
        bne     LA622
LA62E:
        ldx     StackTail_211
        lda     $0104,x
        sta     BRK_TRAP_22F
        lda     $0101,x
        sta     BRK_TRAP_22F+1
        rts

; 0xA63E------------------------------
dont_know_0E:
        ldx     StackTail_211
        lda     $0104,x
        beq     LA64F
        lda     $0103,x
        ora     #$10
        sta     $0103,x
        rts

LA64F:
        lda     $0103,x
        and     #$EF
        sta     $0103,x
        rts

LA658:
        bit     $FC00
        sty     $0735
        ldy     #$02
        lda     (POIN_F0),y
        sta     $0737
        ldy     #$01
        lda     (POIN_F0),y
        cmp     $0737
        bcc     LA673
        lda     #$0D
        jmp     LA690

LA673:
        tay
        lda     (POIN_F0),y
        and     #$7F
        pha
        iny
        tya
        ldy     #$01
        sta     (POIN_F0),y
        pla
        clv
        bit     $04A8
        bmi     LA690
        cmp     #$61
        bcc     LA690
        cmp     #$7B
        bcs     LA690
        and     #$DF
LA690:
        ldy     $0735
        rts

LA694:
        sty     $0735
        ldy     #$01
        lda     (POIN_F0),y
        tay
        dey
        tya
        ldy     #$01
        sta     (POIN_F0),y
        ldy     $0735
        rts

LA6A6:
        ldy     #$01
        lda     (POIN_F0),y
        sta     $0736
        rts

LA6AE:
        ldy     #$01
        lda     $0736
        sta     (POIN_F0),y
        rts

LA6B6:
        ldx     $04A8
        stx     $04A9
        ldx     #$80
        stx     $04A8
        rts

LA6C2:
        ldx     $04A9
        stx     $04A8
        rts

; 0xA6C9------------------------------
dont_know_10:
        ldx     StackTail_211
        lda     $0104,x
        sta     $049C
        lda     $0101,x
        sta     $049D
        rts

; 0xa6d9------------------------------
Text_from_Console_11:
        jsr     LA658
        ldx     StackTail_211
        sta     $0104,x
        rts

; 0xA6E3------------------------------
dont_know_12:
        jsr     LA658
        ldx     StackTail_211
        cmp     $0104,x
        beq     LA6FA
        sta     $0104,x
        bvs     LA6F6
        jsr     LA694
LA6F6:
        bit     $FC00
        rts

LA6FA:
        clv
        rts

; 0xA6FC------------------------------
dont_know_13:
        lda     #$00
        sta     $073B
        jsr     LA6A6
        jsr     LA658
        bvs     LA731
        cmp     #$27
        beq     LA738
        cmp     #$22
        bne     LA719
        lda     #$80
        sta     $073B
        jmp     LA738

LA719:
        jsr     LA694
        jsr     LA74E
        bvs     LA731
        lda     $073C
        bne     LA731
        ldx     StackTail_211
        lda     $073D
        sta     $0104,x
        clv
        rts

LA731:
        jsr     LA6AE
        bit     $FC00
        rts

LA738:
        jsr     LA6B6
        jsr     LA658
        jsr     LA6C2
        bvs     LA731
        ora     $073B
        ldx     StackTail_211
        sta     $0104,x
        clv
        rts

LA74E:
        lda     #$00
        sta     $073B
        sta     $073C
        sta     $073D
        jsr     LA658
        bvs     LA7B3
        cmp     #$24
        beq     LA770
        cmp     #$25
        beq     LA7B7
        jsr     LA694
        ldx     #$09
        ldy     #$03
        jmp     LA774

LA770:
        ldx     #$0F
        ldy     #$02
LA774:
        stx     $0738
        jsr     LA658
        bvs     LA7B3
        jsr     LA848
        bvs     LA7B3
        sta     $073D
LA784:
        jsr     LA658
        bvs     LA7AA
        jsr     LA848
        bvs     LA7A7
        asl     a
        asl     a
        asl     a
        asl     a
        ldx     #$03
LA794:
        rol     a
        rol     $073D
        rol     $073C
        rol     $073B
        dex
        bpl     LA794
        dey
        bpl     LA784
        jmp     LA7AA

LA7A7:
        jsr     LA694
LA7AA:
        lda     $0738
        cmp     #$09
        beq     LA7E2
        clv
        rts

LA7B3:
        bit     $FC00
        rts

LA7B7:
        ldx     #$01
        stx     $0738
        jsr     LA658
        bvs     LA7B3
        jsr     LA848
        bvs     LA7B3
        sta     $073D
        ldy     #$0E
LA7CB:
        jsr     LA658
        bvs     LA7AA
        jsr     LA848
        bvs     LA7A7
        lsr     a
        rol     $073D
        rol     $073C
        dey
        bpl     LA7CB
        jmp     LA7AA

LA7E2:
        lda     #$00
        sta     $0739
        sta     $073A
        sec
        sed
LA7EC:
        lda     $073D
        sbc     #$56
        sta     $073D
        lda     $073C
        sbc     #$02
        sta     $073C
        lda     $073B
        sbc     #$00
        sta     $073B
        bcc     LA80F
        inc     $0739
        bne     LA7EC
LA80B:
        bit     $FC00
        rts

LA80F:
        clc
        lda     $073D
        adc     #$56
        sta     $073D
        lda     $073C
        adc     #$02
        sta     $073C
        sec
LA821:
        lda     $073D
        sbc     #$01
        sta     $073D
        lda     $073C
        sbc     #$00
        sta     $073C
        bcc     LA83A
        inc     $073A
        bne     LA821
        beq     LA80B
LA83A:
        lda     $073A
        sta     $073D
        lda     $0739
        sta     $073C
        clv
        rts

LA848:
        clv
        ldx     $0738
        cmp     #$61
        bcc     LA856
        cmp     #$7B
        bcs     LA856
        and     #$DF
LA856:
        cmp     $FC01,x
        beq     LA861
        dex
        bpl     LA856
        bit     $FC00
LA861:
        txa
        rts

; 0xA863------------------------------
dont_know_14:
        jsr     LA6A6
        jsr     LA74E
        bvs     LA87C
        ldx     StackTail_211
        lda     $073D
        sta     $0104,x
        lda     $073C
        sta     $0101,x
        clv
        rts

LA87C:
        jsr     LA6AE
        bit     $FC00
        rts

; 0xA883------------------------------
dont_know_15:
        jsr     LA6A6
        lda     $0736
        sta     $073E
        ldx     StackTail_211
        lda     $0104,x
        sta     $F8
        lda     $0101,x
        sta     $F9
        lda     #$24
        brk
        .byte   $12
        bvc     LA8A2
        jmp     LA955

LA8A2:
        lda     #$00
        sta     $0740
        sta     $0741
        sta     $0742
        sta     $0743
        lda     #$0F
        sta     $0738
        ldy     #$02
        jsr     LA658
        bvc     LA8BF
LA8BC:
        jmp     LA9B3

LA8BF:
        sta     $073F
        jsr     LA848
        bvc     LA8D6
        lda     $073F
        cmp     #$58
        bne     LA8BC
        lda     #$0F
        sta     $0743
        jmp     LA8D9

LA8D6:
        sta     $0741
LA8D9:
        jsr     LA658
        bvs     LA927
        sta     $073F
        jsr     LA848
        bvc     LA900
        lda     $073F
        cmp     #$58
        bne     LA924
        lda     #$F0
        ldx     #$03
LA8F1:
        rol     a
        rol     $0743
        rol     $0742
        dex
        bpl     LA8F1
        lda     #$00
        jmp     LA90E

LA900:
        sta     $073F
        lda     $0742
        ora     $0743
        bne     LA924
        lda     $073F
LA90E:
        ldx     #$03
        asl     a
        asl     a
        asl     a
        asl     a
LA914:
        rol     a
        rol     $0741
        rol     $0740
        dex
        bpl     LA914
        dey
        bpl     LA8D9
        jmp     LA927

LA924:
        jsr     LA694
LA927:
        lda     $0742
        ora     $0743
        beq     LA955
        ldy     #$00
        lda     $0741
        sta     ($F8),y
        eor     $0743
        ldy     #$02
        sta     ($F8),y
        ldy     #$01
        lda     $0740
        sta     ($F8),y
        eor     $0742
        ldy     #$03
        sta     ($F8),y
LA94B:
        ldx     StackTail_211
        lda     #$00
        sta     $0102,x
        clv
        rts

LA955:
        lda     $073E
        sta     $0736
        jsr     LA6AE
        brk
        .byte   $14
        bvs     LA9B3
        sty     $073F
        ldy     #$00
        sta     ($F8),y
        ldy     #$02
        sta     ($F8),y
        lda     $073F
        ldy     #$01
        sta     ($F8),y
        ldy     #$03
        sta     ($F8),y
        lda     #$2D
        brk
        .byte   $12
        bvc     LA981
        jmp     LA94B

LA981:
        brk
        .byte   $14
        bvs     LA9C0
        pha
        ldx     StackTail_211
        lda     #$00
        sta     $0102,x
        pla
        sty     $073F
LA992:
        ldy     #$02
        sta     ($F8),y
        lda     $073F
        ldy     #$03
        sta     ($F8),y
        cld
        sec
        ldy     #$02
        lda     ($F8),y
        ldy     #$00
        sbc     ($F8),y
        ldy     #$03
        lda     ($F8),y
        ldy     #$01
        sbc     ($F8),y
        bcc     LA9B3
        clv
        rts

LA9B3:
        lda     $073E
        sta     $0736
        jsr     LA6AE
        bit     $FC00
        rts

LA9C0:
        ldx     StackTail_211
        lda     $0102,x
        beq     LA9B3
        lda     #$FF
        sta     $0102,x
        sta     $073F
        jmp     LA992

; 0xA9D3------------------------------
dont_know_16:
        jsr     LA6A6
        jsr     GetAddress_afterFunction
        lda     #$FF
        sta     $0745
        ldy     #$00
LA9E0:
        inc     $0745
        jsr     LA658
        bvs     LAA0F
        sta     $0744
        jsr     Vector_LODPOIS2
        cmp     $0744
        beq     LAA16
LA9F3:
        iny
        jsr     Vector_LODPOIS2
        cmp     #$FF
        bne     LA9F3
        iny
        jsr     Vector_LODPOIS2
        cmp     #$FF
        beq     LAA0F
        sty     $074C
        jsr     LA6AE
        ldy     $074C
        jmp     LA9E0

LAA0F:
        bit     $FC00
        jsr     LA6AE
        rts

LAA16:
        iny
        jsr     Vector_LODPOIS2
        cmp     #$FE
        beq     LAA31
        jsr     LA658
        bvs     LA9F3
        sta     $0744
        jsr     Vector_LODPOIS2
        cmp     $0744
        beq     LAA16
        jmp     LA9F3

LAA31:
        iny
        jsr     Vector_LODPOIS2
        cmp     #$FF
        beq     LAA4C
        jsr     LA658
        bvs     LAA4C
        sta     $0744
        jsr     Vector_LODPOIS2
        cmp     $0744
        beq     LAA31
        jsr     LA694
LAA4C:
        ldx     StackTail_211
        lda     $0745
        sta     $0104,x
        clv
        rts

; 0xAA57------------------------------
dont_know_17:
        jsr     LA6A6
        jsr     GetAddress_afterFunction
        lda     $0102,x
        sta     $0746
        ldy     #$00
        lda     #$20
LAA67:
        sta     ($F2),y
        iny
        cpy     $0746
        bne     LAA67
        dec     $0746
        dec     $0746
        jsr     LA658
        bvs     LAAA5
        jsr     LAAC8
        bvs     LAAA5
        ldy     #$00
        sta     ($F2),y
LAA83:
        iny
        jsr     LA658
        bvs     LAA9C
        jsr     LAAAC
        bvs     LAA99
        sta     ($F2),y
        dec     $0746
        bpl     LAA83
        iny
        jmp     LAA9C

LAA99:
        jsr     LA694
LAA9C:
        ldx     StackTail_211
        tya
        sta     $0102,x
        clv
        rts

LAAA5:
        jsr     LA6AE
        bit     $FC00
        rts

LAAAC:
        jsr     LAAC8
        bvc     LAAC6
        jsr     LAADE
        bvc     LAAC6
        cmp     #$2E
        beq     LAAC6
        cmp     #$2F
        beq     LAAC6
        cmp     #$23
        beq     LAAC6
        bit     $FC00
        rts

LAAC6:
        clv
        rts

LAAC8:
        cmp     #$40
        bcc     LAAD8
        cmp     #$5B
        bcc     LAADC
        cmp     #$61
        bcc     LAAD8
        cmp     #$7B
        bcc     LAADC
LAAD8:
        bit     $FC00
        rts

LAADC:
        clv
        rts

LAADE:
        cmp     #$30
        bcc     LAAE6
        cmp     #$3A
        bcc     LAAEA
LAAE6:
        bit     $FC00
        rts

LAAEA:
        clv
        rts

        .byte   "$N"
        .byte   $FE
        .byte   "ULL"
        .byte   $FF
        .byte   "$T"
        .byte   $FE
        .byte   "ERM"
        .byte   $FF
        .byte   "$P"
        .byte   $FE
        .byte   "RINTER"
        .byte   $FF
        .byte   "$F"
        .byte   $FE
        .byte   "LOPPY"
        .byte   $FF,$FF
        .byte   "OV"
        .byte   $FE
        .byte   "ERWRITE"
        .byte   $FF,$FF
        .byte   "1"
        .byte   $FE,$FF
        .byte   "2"
        .byte   $FE,$FF
        .byte   "3"
        .byte   $FE,$FF
        .byte   "4"
        .byte   $FE,$FF
        .byte   "5"
        .byte   $FE,$FF,$FF
LAB2A:
        .byte   $C9,$FF,$D0,$10,$AE
        eor     $3007
        .byte   $14
        ldx     #$00
LAB35:
        lda     $04BA,x
        cmp     $0748
        bne     LAB3F
        txa
        rts

LAB3F:
        inx
        cpx     #$05
        bcc     LAB35
        lda     #$FF
        rts

        ldx     #$00
LAB49:
        lda     $04BA,x
        cmp     $0748
        bne     LAB66
        txa
        pha
LAB53:
        inx
        cpx     #$05
        bcc     LAB5A
        pla
        rts

LAB5A:
        lda     $04BA,x
        cmp     $0748
        bne     LAB53
        pla
        lda     #$FE
        rts

LAB66:
        inx
        cpx     #$05
        bcc     LAB49
        lda     #$FF
        rts

; 0xAB6E------------------------------
dont_know_18:
        jsr     LA6A6
        lda     $0736
        sta     $0749
        ldx     StackTail_211
        lda     $0102,x
        sta     $074D
        and     #$3F
        sta     $0747
        lda     $0104,x
        sta     $F8
        lda     $0101,x
        sta     $F9
        lda     $0102,x
        and     #$40
        beq     LAB9F
        ldy     #$7D
        lda     #$00
LAB9A:
        sta     ($F8),y
        dey
        bpl     LAB9A
LAB9F:
        ldy     #$48
        lda     #$FF
        sta     ($F8),y
        ldy     #$40
        lda     #$03
        sta     ($F8),y
        brk
        stx     $EC,y
        tax
        bvs     LABD9
        ldy     #$40
        sta     ($F8),y
        cmp     #$00
        beq     LABCE
        cmp     #$02
        beq     LABCE
        cmp     #$03
        beq     LAC09
        lda     $0747
        cmp     #$03
        bne     LABCC
        bit     $FC00
        rts

LABCC:
        clv
        rts

LABCE:
        lda     $0747
        cmp     #$02
        beq     LABCC
        bit     $FC00
        rts

LABD9:
        lda     #$1A
        ldy     #$AB
        brk
        asl     $50,x
        .byte   $37
        lda     #$41
        sta     $0748
        lda     #$FF
        jsr     LAB2A
        ldy     #$48
        sta     ($F8),y
        jmp     LAC3F

LABF2:
        lda     $049C
        sta     POIN_F0
        lda     $049D
        sta     POIN_F0+1
        lda     $0749
        sta     $0736
        jsr     LA6AE
        bit     $FC00
        rts

LAC09:
        jsr     LA658
        cmp     #$3A
        bne     LABF2
        lda     #$1A
        ldy     #$AB
        brk
        asl     $70,x
        .byte   $DA
        ldx     #$41
        stx     $0748
        jsr     LAB2A
        ldy     #$48
        sta     ($F8),y
        jsr     LA658
        bvs     LAC30
        cmp     #$3A
        beq     LAC3F
        jsr     LA694
LAC30:
        lda     $0747
        cmp     #$03
        bne     LABF2
        ldy     #$00
        lda     #$20
        sta     ($F8),y
        clv
        rts

LAC3F:
        ldy     #$2B
        lda     #$FF
        sta     ($F8),y
        iny
        sta     ($F8),y
        lda     $0747
        cmp     #$03
        bne     LAC52
        jmp     LAD4A

LAC52:
        ldx     #$20
        ldy     #$00
        stx     $074A
        sty     $074B
LAC5C:
        lda     ($F8),y
        beq     LAC71
        cmp     #$20
        beq     LAC71
        cmp     #$2E
        beq     LAC71
        dex
        iny
        cpy     #$0A
        bcc     LAC5C
        jmp     LABF2

LAC71:
        lda     #$2E
        sta     ($F8),y
        cpx     #$20
        beq     LAC93
        iny
        dex
        sty     $074B
        stx     $074A
        lda     #$FE
        sta     ($F8),y
        iny
        lda     #$FF
        sta     ($F8),y
        iny
        sta     ($F8),y
        lda     $F8
        ldy     $F9
        brk
        .byte   $16
LAC93:
        ldx     $074A
        clc
        cld
        lda     $F8
        adc     $074B
        sta     $074C
        lda     $F9
        adc     #$00
        tay
        lda     $074C
        brk
        .byte   $17
        bvc     LACAF
LACAC:
        jmp     LABF2

LACAF:
        txa
        clc
        cld
        adc     $074B
        tax
        inx
        cpx     $074A
        dex
        bcs     LACE9
        jsr     LA658
        cmp     #$3B
        bne     LACEC
        lda     $0747
        cmp     #$02
        bne     LACAC
        txa
        tay
        lda     #$3B
        sta     ($F8),y
        inx
        stx     $074C
        sec
        cld
        lda     #$20
        sbc     $074C
        tax
        clc
        lda     $F8
        adc     $074C
        ldy     $F9
        brk
        .byte   $17
        bvs     LACAC
LACE9:
        jsr     LA658
LACEC:
        bvs     LACF5
        cmp     #$28
        beq     LAD07
        jsr     LA694
LACF5:
        ldy     #$01
LACF7:
        lda     ($F8),y
        cmp     #$2E
        beq     LAD05
        iny
        cpy     #$06
        bcc     LACF7
        jmp     LABF2

LAD05:
        clv
        rts

LAD07:
        lda     $0747
        cmp     #$02
        beq     LAD30
        lda     #$04
        brk
        .byte   $1A
        bvs     LACAC
        sty     $074C
        ldy     #$2B
        sta     ($F8),y
        ora     $074C
        beq     LACAC
        iny
        lda     $074C
        sta     ($F8),y
        jsr     LA658
        cmp     #$29
        bne     LAD47
        jmp     LACF5

LAD30:
        lda     #$0E
        ldy     #$AB
        brk
        asl     $70,x
        .byte   $0F
        lda     #$29
        brk
        .byte   $12
        bvs     LAD47
        lda     #$FF
        ldy     #$6E
        sta     ($F8),y
        jmp     LACF5

LAD47:
        jmp     LABF2

LAD4A:
        lda     #$2A
        brk
        .byte   $12
        bvs     LAD67
        ldy     #$6F
        lda     ($F8),y
        ora     #$80
        sta     ($F8),y
        ldy     #$01
        lda     (POIN_F0),y
        tay
        dey
        lda     #$41
        sta     (POIN_F0),y
        tya
        ldy     #$01
        sta     (POIN_F0),y
LAD67:
        lda     $F8
        ldy     $F9
        ldx     #$20
        brk
        .byte   $17
        bvs     LAD99
        ldy     #$6F
        lda     ($F8),y
        bpl     LAD89
        ldy     #$01
LAD79:
        lda     ($F8),y
        dey
        sta     ($F8),y
        iny
        iny
        cpy     #$20
        bcc     LAD79
        dey
        lda     #$20
        sta     ($F8),y
LAD89:
        lda     #$2A
        brk
        .byte   $12
        bvs     LAD97
        ldy     #$6F
        lda     ($F8),y
        ora     #$01
        sta     ($F8),y
LAD97:
        clv
        rts

LAD99:
        ldy     #$00
        lda     #$20
        sta     ($F8),y
        jmp     LAD89

; 0xADA2------------------------------
dont_know_19:
        jsr     GetAddress_afterFunction
        lda     $0102,x
        sta     $074F
        jsr     LA6A6
        jsr     LA6B6
        jsr     LA658
        bvc     LADC0
LADB6:
        jsr     LA6AE
        jsr     LA6C2
        bit     $FC00
        rts

LADC0:
        sta     $074E
        ldy     #$00
LADC5:
        jsr     LA658
        bvs     LADB6
        cmp     $074E
        beq     LADDF
        sta     ($F2),y
        iny
        dec     $074F
        bne     LADC5
        jsr     LA658
        cmp     $074E
        bne     LADB6
LADDF:
        ldx     StackTail_211
        tya
        sta     $0102,x
        jsr     LA6C2
        lda     $074E
        cmp     #$22
        bne     LADF7
        dey
        lda     ($F2),y
        ora     #$80
        sta     ($F2),y
LADF7:
        clv
        rts

; 0xADF9------------------------------
dont_know_1A:
        jsr     LA6A6
        ldx     StackTail_211
        lda     $0104,x
        beq     LAE69
        cmp     #$07
        bcs     LAE69
        tay
        dey
        dey
        lda     #$00
        sta     $0750
        sta     $0751
        sta     $0752
        lda     #$09
        sta     $0738
        jsr     LA658
        bvs     LAE69
        jsr     LA848
        bvs     LAE69
        sta     $0752
        cpy     #$FF
        beq     LAE52
LAE2C:
        jsr     LA658
        bvs     LAE52
        jsr     LA848
        bvs     LAE4F
        asl     a
        asl     a
        asl     a
        asl     a
        ldx     #$03
LAE3C:
        rol     a
        rol     $0752
        rol     $0751
        rol     $0750
        dex
        bpl     LAE3C
        dey
        bpl     LAE2C
        jmp     LAE52

LAE4F:
        jsr     LA694
LAE52:
        ldx     StackTail_211
        lda     $0750
        sta     $0102,x
        lda     $0751
        sta     $0101,x
        lda     $0752
        sta     $0104,x
        clv
        rts

LAE69:
        jsr     LA6AE
        bit     $FC00
        rts

; 0xAE70------------------------------
dont_know_1B:
        jsr     LA658
        bvs     LAE7C
        bit     $FC00
        jsr     LA694
        rts

LAE7C:
        clv
        rts

; 0xAE7E------------------------------
dont_know_1C:
        lda     #$80
        sta     $04A8
        ldy     #$02
        lda     (POIN_F0),y
        sta     $0753
        cmp     #$03
        bne     LAE9D
        lda     #$00
        ldx     StackTail_211
        sta     $0104,x
        ldy     #$01
        lda     #$03
        sta     (POIN_F0),y
        rts

LAE9D:
        ldy     #$03
        ldx     StackTail_211
        lda     $0104,x
        bpl     LAEBF
LAEA7:
        lda     (POIN_F0),y
        cmp     #$20
        bne     LAEBF
        iny
        cpy     $0753
        bcc     LAEA7
        dey
        tya
        ldy     #$01
        sta     (POIN_F0),y
        lda     #$01
        sta     $0104,x
        rts

LAEBF:
        tya
        ldy     #$01
        sta     (POIN_F0),y
        lda     $0104,x
        and     #$40
        beq     LAEE3
        ldy     $0753
        dey
LAECF:
        lda     (POIN_F0),y
        cmp     #$20
        bne     LAEDD
        cpy     #$04
        bcc     LAEDD
        dey
        jmp     LAECF

LAEDD:
        iny
        tya
        ldy     #$02
        sta     (POIN_F0),y
LAEE3:
        sec
        cld
        ldy     #$02
        lda     (POIN_F0),y
        ldy     #$01
        sbc     (POIN_F0),y
        sta     $0104,x
        rts

; 0xAEF1------------------------------
dont_know_1D:
        lda     #$00
LAEF3:
        sta     $04A8
        rts

; 0xAEF7------------------------------
dont_know_1E:
        lda     #$80
        bne     LAEF3
; 0xAEFB------------------------------
dont_know_1F:
        brk
        stx     $1A,y
        .byte   $AB
        bvc     LAF28
        ldx     StackTail_211
        lda     $0102,x
        beq     LAF0D
        bit     $FC00
        rts

LAF0D:
        ldx     StackTail_211
        lda     $0104,x
        sta     $0748
        lda     #$00
        sta     $074D
        lda     #$FF
        jsr     LAB2A
        ldx     StackTail_211
        sta     $0104,x
        clv
        rts

LAF28:
        tay
        ldx     StackTail_211
        lda     $0104,x
        beq     LAF36
        cmp     $04BA,y
        bne     LAF3C
LAF36:
        tya
        sta     $0104,x
        clv
        rts

LAF3C:
        lda     #$FF
        sta     $0104,x
        clv
        rts

LAF43:
        clv
        pha
        sty     $0754
        ldy     #$00
        lda     (POIN_F0),y
        sta     $0755
        ldy     #$02
        lda     (POIN_F0),y
        cmp     $0755
        bcs     LAF66
        tay
        pla
        sta     (POIN_F0),y
        iny
        tya
        ldy     #$02
        sta     (POIN_F0),y
LAF62:
        ldy     $0754
        rts

LAF66:
        pla
        bit     $FC00
        bvs     LAF62
LAF6C:
        ldx     StackTail_211
        lda     $0104,x
        sta     $F2
        lda     $0101,x
        sta     $F3
        rts

; 0xAF7A------------------------------
dont_know_20:
        ldx     StackTail_211
        lda     $0104,x
        sta     $049E
        lda     $0101,x
        sta     $049F
        rts

; 0xaf8a------------------------------
Character_to_Console_21:
        ldx     StackTail_211
        lda     $0104,x
        and     #$7F
        tax
        and     #$60
        bne     LAF99
        ldx     #$20
LAF99:
        txa
        cmp     #$7F
        bne     LAFA0
        lda     #$5F
LAFA0:
        jmp     LAF43

; 0xAFA3------------------------------
dont_know_22:
        jsr     LAF6C
        ldy     #$00
LAFA8:
        jsr     Vector_LODPOIS2
        cmp     #$FF
        beq     LAFBC
        and     #$7F
        jsr     LAF43
        jsr     Vector_LODPOIS2
        bmi     LAFBC
        iny
        bne     LAFA8
LAFBC:
        rts

; 0xAFBD------------------------------------------------------------
OUTHEX_23:
        ldx     StackTail_211
        lda     $0104,x
LAFC3:
        pha
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        jsr     LAFCC
        pla
LAFCC:
        and     #$0F
        tay
        lda     $FC01,y
        jmp     LAF43

; 0xAFD5
OUTBIL_24:
        ldx     StackTail_211
        lda     $0104,x
        asl     a
        asl     a
        asl     a
        asl     a
LAFDF:
        sta     $0756
        ldy     #$04
LAFE4:
        rol     $0756
        lda     #$30
        adc     #$00
        jsr     LAF43
        dey
        bne     LAFE4
        rts

; 0xaff2------------------------------
Text_to_Console_25:
        ldx     StackTail_211
        lda     $0101,x
        jsr     LAFC3
        lda     $0104,x
        jmp     LAFC3

; 0xb001------------------------------
OUTEX1:
        ldx     StackTail_211
        lda     $0106,x
        sta     $F2
        lda     $0107,x
        sta     $F3
        ldy     #$00
LB010:
        jsr     Vector_LODPOIS2
        inc     $0106,x
        bne     LB01B
        inc     $0107,x
LB01B:
        cmp     #$FF
        beq     LB02C
        and     #$7F
        jsr     LAF43
        jsr     Vector_LODPOIS2
        bmi     LB02C
        iny
        bne     LB010
LB02C:
        rts

; 0xB02D------------------------------
dont_know_27:
        ldy     #$02
        lda     #$03
        sta     (POIN_F0),y
        rts

; 0xB034------------------------------
Time_to_Console_28:
        ldx     #$00
        sei
LB037:
        lda     C3TIME_F42E,x
        jsr     LAF43
        inx
        cpx     #$08
        bne     LB037
        cli
        rts

; 0xB044------------------------------
Date_to_Console_29:
        ldx     #$00
        sei
LB047:
        lda     C3DATE_F424,x
        jsr     LAF43
        inx
        cpx     #$0A
        bne     LB047
        cli
        rts

        .byte   "> ERROR"
        .byte   $A0
; 0xB05C------------------------------
dont_know_2A:
        lda     BankTemp_22D
        pha
        lda     $049E
        ldy     $049F
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$02
        lda     (POIN_F0),y
        cmp     #$05
        bcc     LB079
        brk
        ldx     #$55
        bcs     LB0C3
        .byte   $7D
        .byte   $B0
LB079:
        brk
        ldx     #$54
        bcs     LB02C
        ora     ($02),y
        lda     $0102,x
        brk
        .byte   $23
        lda     #$20
        brk
        and     ($68,x)
        sta     BankTemp_22D
        lda     $0104,x
        ora     $0101,x
        beq     LB0A4
        lda     $0104,x
        sta     $F2
        lda     $0101,x
        sta     $F3
        jsr     LB0B5
        bcc     LB0B4
LB0A4:
        lda     #$8D
        ldy     #$FC
        sta     $F2
        sty     $F3
        lda     #$FE
        sta     BankTemp_22D
        jsr     LB0B5
LB0B4:
        rts

LB0B5:
        ldy     #$00
        ldx     StackTail_211
LB0BA:
        jsr     Vector_LODPOIS2
        cmp     #$FF
        beq     LB102
        .byte   $DD
        .byte   $02
LB0C3:
        ora     (POIN_F0,x)
        .byte   $0E
LB0C6:
        jsr     LB104
        jsr     Vector_LODPOIS2
        bpl     LB0C6
        jsr     LB104
        jmp     LB0BA

        jsr     LB104
LB0D7:
        jsr     Vector_LODPOIS2
        cmp     #$FF
        beq     LB100
        and     #$7F
        ldx     $F2
        stx     InterruptControl_Reg
        ldx     $F3
        stx     $FF
        ldx     BankTemp_22D
        brk
        and     ($8E,x)
        and     LA602
        inc     $F286,x
        ldx     $FF
        stx     $F3
        jsr     Vector_LODPOIS2
        bmi     LB100
        iny
        bne     LB0D7
LB100:
        clc
        rts

LB102:
        sec
        rts

LB104:
        inc     $F2
        bne     LB10A
        inc     $F3
LB10A:
        rts

; 0xB10B------------------------------
dont_know_2B:
        lda     #$20
        jmp     LAF43

; 0xB110------------------------------
dont_know_2C:
        ldx     StackTail_211
        lda     $0102,x
        tay
LB117:
        lda     $0104,x
        jsr     LAF43
        dey
        bne     LB117
        rts

; 0xB121------------------------------
OUTHEL_2D:
        ldx     StackTail_211
        lda     $0104,x
LB127:
        and     #$0F
        ora     #$30
; 0xB12b - 
comm:
        brk
        .byte   $21
        rts

; 0xB12e------------------------------
OUTHEH_2E:
        ldx     StackTail_211
        lda     $0104,x
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        jmp     LB127

; 0xB13B------------------------------
OUTBIH_2F:
        ldx     StackTail_211
        lda     $0104,x
        jmp     LAFDF

; 0xB144------------------------------
dont_know_35:
        lda     $049E
        ldy     $049F
        sta     POIN_F0
        sty     POIN_F0+1
        jsr     GetAddress_afterFunction
        lda     $0102,x
        tax
        ldy     #$00
LB157:
        jsr     Vector_LODPOIS2
        jsr     LAF43
        iny
        dex
        bne     LB157
        rts

; 0xB162------------------------------
dont_know_31:
        ldx     StackTail_211
        lda     $0104,x
        ora     $0101,x
        beq     LB1DA
        lda     $0104,x
        cmp     #$FF
        bne     LB17E
        lda     $0101,x
        cmp     #$FF
        bne     LB17E
        jmp     LB1FB

LB17E:
        lda     $0102,x
        beq     LB1CE
        tay
        sec
        cld
        lda     $0104,x
        sbc     #$00
        sta     $0758
        lda     $0101,x
        sbc     #$C0
        sta     $0759
        bcc     LB1CE
        lda     $0758
        and     #$7F
        bne     LB1CE
        ldx     #$06
LB1A1:
        lsr     $0759
        ror     $0758
        dex
        bpl     LB1A1
        ldx     $0758
        cpx     #$68
        bcs     LB1CE
LB1B1:
        lda     $F380,x
        and     #$EF
        cmp     $0207
        bne     LB1CE
        lda     #$00
        sta     $F380,x
        inc     $F3E9
        inx
        dey
        bne     LB1B1
        clv
        rts

LB1C9:
        lda     #$05
        jmp     LB1D0

LB1CE:
        lda     #$E4
LB1D0:
        bit     $FC00
        ldx     StackTail_211
        sta     $0104,x
        rts

LB1DA:
        lda     BrkSRC_ROM_23C_FF
        beq     LB1C9
        ldx     #$00
LB1E1:
        lda     $F380,x
        bmi     LB1F9
        and     #$EF
        cmp     $0207
        bne     LB1F5
        lda     #$00
        sta     $F380,x
        inc     $F3E9
LB1F5:
        inx
        jmp     LB1E1

LB1F9:
        clv
        rts

LB1FB:
        lda     BrkSRC_ROM_23C_FF
        beq     LB1C9
        ldx     #$00
LB202:
        lda     $F380,x
        bmi     LB21A
        eor     #$10
        cmp     $0207
        bne     LB216
        lda     #$00
        sta     $F380,x
        inc     $F3E9
LB216:
        inx
        jmp     LB202

LB21A:
        clv
        rts

; 0xB21C------------------------------
dont_know_30:
        ldx     StackTail_211
        lda     $0102,x
        beq     LB251
        sta     $0757
        ldy     #$10
        lda     BrkSRC_ROM_23C_FF
        beq     LB230
        ldy     #$00
LB230:
        tya
        ora     $0207
        sta     POIN_F0
        ldx     #$00
LB238:
        lda     $F380,x
        sei
        beq     LB25D
LB23E:
        cli
        bmi     LB245
        inx
        jmp     LB238

LB245:
        ldx     StackTail_211
        lda     #$E9
        sta     $0104,x
        bit     $FC00
        rts

LB251:
        bit     $FC00
        lda     #$E3
        ldx     StackTail_211
        sta     $0104,x
        rts

LB25D:
        stx     $075A
        ldy     #$00
LB262:
        iny
        cpy     $0757
        beq     LB271
        inx
        lda     $F380,x
        beq     LB262
        jmp     LB23E

LB271:
        ldx     $075A
        ldy     $0757
        lda     POIN_F0
LB279:
        sta     $F380,x
        dec     $F3E9
        inx
        dey
        bne     LB279
        cli
        lda     #$00
        sta     $0758
        lda     $075A
        sta     $0759
        lsr     $0759
        ror     $0758
        ldx     StackTail_211
        clc
        cld
        lda     #$00
        adc     $0758
        sta     POIN_F0
        sta     $0104,x
        lda     #$C0
        adc     $0759
        sta     $0101,x
        sta     POIN_F0+1
        ldx     $0757
LB2B1:
        lda     #$00
        ldy     #$00
LB2B5:
        sta     (POIN_F0),y
        iny
        bpl     LB2B5
        clc
        cld
        lda     POIN_F0
        adc     #$80
        sta     POIN_F0
        lda     POIN_F0+1
        adc     #$00
        sta     POIN_F0+1
        dex
        bne     LB2B1
        clv
        rts

; 0xB2CD------------------------------
dont_know_32:
        lda     $0216
        cmp     #$80
        bcc     LB2E0
LB2D4:
        lda     #$07
LB2D6:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB2E0:
        cmp     #$30
        beq     LB2E8
        cmp     #$60
        bne     LB2D4
LB2E8:
        ldy     #$00
        ldx     #$00
        cld
LB2ED:
        sei
        lda     $F44C,x
        cmp     #$FF
        beq     LB2FA
        cmp     $0216
        beq     LB306
LB2FA:
        cli
        iny
        iny
        iny
        inx
        cpx     #$04
        bcc     LB2ED
        jmp     LB2D4

LB306:
        lda     MY3MAP_F448,x
        bne     LB2FA
        lda     $0207
        sta     MY3MAP_F448,x
        stx     $075B
        sty     $075C
        txa
        asl     a
        tax
        lda     $FC7D,x
        sta     $0252,y
        lda     $FC7E,x
        sta     $0253,y
        cli
        brk
        .byte   $04
        lda     #$00
        sta     POIN_F0
        ldx     #$60
        ldy     #$24
        lda     $0216
        cmp     #$30
        beq     LB33C
        ldx     #$80
        ldy     #$32
LB33C:
        stx     $F2
        lda     $0216
        sta     POIN_F0+1
        sty     $F3
        ldy     #$00
LB347:
        lda     #$FF
        sta     (POIN_F0),y
        lda     (POIN_F0),y
        cmp     #$FF
        bne     LB376
        lda     #$00
        sta     (POIN_F0),y
        lda     (POIN_F0),y
        bne     LB376
        iny
        bne     LB347
        inc     POIN_F0+1
        lda     POIN_F0+1
        cmp     $F2
        bcc     LB347
        lda     $F2
        sta     $0216
        ldx     StackTail_211
        sta     $0104,x
        lda     $F3
        sta     $023A
        clv
        rts

LB376:
        ldx     $075B
        .byte   $AC
        .byte   $5C
LB37B:
        .byte   $07
        sei
        lda     #$FF
        sta     MY3MAP_F448,x
        txa
        asl     a
        tax
        lda     $FC85,x
        sta     $0252,y
        lda     $FC86,x
        sta     $0253,y
        cli
        lda     $075B
        ora     #$90
        jmp     LB2D6

; 0xB39A------------------------------
dont_know_33:
        jsr     Vector_MYINIT
        lda     $0216
        ldx     StackTail_211
        sta     $0104,x
        clv
        rts

; 0xB3A8------------------------------
dont_know_36:
        lda     #$AB
        ldy     #$F4
        jsr     Vector_LOCK
        lda     $0247
        beq     LB3B8
        lda     #$80
        bne     LB3D4
LB3B8:
        lda     IPADR_P1_F4AF
        bne     LB3EE
        lda     $0207
        sta     $F4AD
        lda     #$09
        sta     $0207
        ldx     #$01
        brk
        bmi     LB37B
        lda     $8EF4
        .byte   $07
        .byte   $02
        bvc     LB3E3
LB3D4:
        ldx     StackTail_211
        sta     $0104,x
        lda     #$00
        sta     IPSW_F4AB
        bit     $FC00
        rts

LB3E3:
        sta     IPADR_F4AE
        sty     IPADR_P1_F4AF
        lda     #$00
        sta     IPANZ_F4AC_INIT_IPC
LB3EE:
        ldx     StackTail_211
        lda     IPADR_F4AE
        sta     $0104,x
        lda     IPADR_P1_F4AF
        sta     $0101,x
        lda     #$FF
        sta     $0247
        inc     IPANZ_F4AC_INIT_IPC
        lda     #$00
        sta     IPSW_F4AB
        clv
        rts

; 0xB40C------------------------------
dont_know_37:
        lda     #$AB
        ldy     #$F4
        jsr     Vector_LOCK
        lda     $0247
        bne     LB41C
        lda     #$81
        bne     LB3D4
LB41C:
        dec     IPANZ_F4AC_INIT_IPC
        bne     LB443
        lda     $0207
        sta     $F4AD
        lda     #$09
        sta     $0207
        ldx     #$01
        lda     IPADR_F4AE
        ldy     IPADR_P1_F4AF
        brk
        and     ($AE),y
        lda     $8EF4
        .byte   $07
        .byte   $02
        bvs     LB3D4
        lda     #$00
        sta     IPADR_P1_F4AF
LB443:
        lda     #$00
        sta     $0247
        sta     IPSW_F4AB
        clv
        rts

; 0xB44D------------------------------
OPEN_42:
        lda     #$00
        sta     $075E
        jsr     LB4A6
        beq     LB463
LB457:
        lda     #$E5
LB459:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB463:
        lda     $0102,x
        bne     LB46C
LB468:
        lda     #$F0
        bne     LB459
LB46C:
        cmp     #$03
        bcs     LB468
        sta     $075F
        ldy     #$45
        lda     ($F8),y
        beq     LB47D
        lda     #$E0
        bne     LB459
LB47D:
        ldy     #$40
        lda     ($F8),y
        cmp     #$04
        bcc     LB489
        lda     #$F1
        bne     LB459
LB489:
        jsr     LB49C
        ldy     #$46
        lda     ($F8),y
        tax
        lda     #$00
        sta     ($F8),y
        txa
        cmp     #$01
        bne     LB459
        clv
        rts

LB49C:
        lda     #$00
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        rts

LB4A6:
        ldx     StackTail_211
        lda     $0104,x
        sta     $F8
        lda     $0101,x
        sta     $F9
        ldy     #$46
        lda     ($F8),y
        rts

; 0xB4B8------------------------------
CLOSE_43:
        lda     #$02
        sta     $075E
        jsr     LB4A6
        bne     LB457
        ldy     #$45
        lda     ($F8),y
        bne     LB47D
        lda     #$E1
        jmp     LB459

; 0xB4CD------------------------------
dont_know_44:
        lda     #$04
        ldy     #$01
LB4D1:
        sta     $075E
        sty     $0760
        jsr     LB4A6
        beq     LB4E8
        lda     #$E5
LB4DE:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB4E8:
        ldy     #$45
        lda     ($F8),y
        cmp     $0760
        beq     LB4F5
        lda     #$E7
        bne     LB4DE
LB4F5:
        ldy     #$40
        lda     ($F8),y
        cmp     #$04
        bcc     LB501
        lda     #$F1
        bne     LB4DE
LB501:
        jmp     LB49C

; 0xB504------------------------------
dont_know_45:
        lda     #$06
        ldy     #$02
        jmp     LB4D1

; 0xB50B------------------------------
dont_know_46:
        lda     #$08
        ldy     #$01
        jmp     LB4D1

; 0xB512------------------------------
dont_know_47:
        lda     #$0A
        ldy     #$01
        jmp     LB4D1

; 0xB519------------------------------
dont_know_40:
        ldx     StackTail_211
        lda     $0104,x
        ldy     $0101,x
        brk
        eor     ($70,x)
        .byte   $17
        ldx     StackTail_211
        clc
        cld
        lda     $0104,x
        adc     #$46
        pha
        lda     $0101,x
        adc     #$00
        tay
        pla
        brk
        ora     $4C
        ora     $C9B5,y
        ora     ($D0,x)
        .byte   $02
        clv
        rts

        ldx     StackTail_211
        sta     $0104,x
        rts

; 0xB54A------------------------------
dont_know_41:
        jsr     LB4A6
        cmp     #$FF
        bne     LB552
        rts

LB552:
        ldx     StackTail_211
        cmp     #$00
        bne     LB561
        lda     #$E6
        sta     $0104,x
LB55E:
        jmp     LB60E

LB561:
        sta     $0104,x
        lda     #$00
        sta     ($F8),y
        ldy     #$40
        lda     ($F8),y
        cmp     $0208
        bne     LB55E
        lda     $0200
        ldy     $0201
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$40
        lda     (POIN_F0),y
        beq     LB55E
        lda     $0200
        ldy     $0201
        cmp     $F8
        bne     LB58F
        cpy     $F9
        beq     LB60E
LB58F:
        lda     $040A
        bne     LB609
        lda     $0200
        ldy     $0201
        brk
        eor     $70
        .byte   $02
        brk
        rti

        bvc     LB609
        pha
        lda     $0200
        ldy     $0201
        brk
        .byte   $43
        lda     $0200
        ldy     $0201
        sta     POIN_F0
        sty     POIN_F0+1
        ldy     #$40
        lda     #$00
        sta     (POIN_F0),y
        .byte   $AD
LB5BC:
        brk
        .byte   $02
        ldy     $0201
        brk
        .byte   $42
        lda     $049E
        ldy     $049F
        sta     $0321
        sty     $0322
        lda     #$D1
        ldy     #$02
        brk
        jsr     L2700
        brk
        rol     $20
        rol     $5320,x
        eor     $4353,y
        .byte   $4F
        bvc     LB5BC
        pla
        tax
        lda     #$00
        ldy     #$00
        brk
        rol     a
        lda     $0321
        ldy     $0322
        sta     $049E
        sty     $049F
        lda     #$D1
        ldy     #$02
        sta     $0321
        sty     $0322
        lda     #$21
        ldy     #$03
        ldx     #$45
        brk
        asl     a
LB609:
        lda     #$03
        sta     $02D3
LB60E:
        bit     $FC00
        rts

; 0xB612------------------------------
dont_know_50:
        jsr     GetAddress_afterFunction
        ldy     #$49
        lda     ($F2),y
        cmp     #$FF
        bne     LB62F
        clc
        cld
        lda     $F2
        adc     #$49
        tax
        lda     $F3
        adc     #$00
        tay
        txa
        brk
        ora     $4C
        .byte   $12
        .byte   $B6
LB62F:
        clv
        cmp     #$01
        beq     LB643
        ldx     StackTail_211
        cmp     #$00
        bne     LB63D
        lda     #$E6
LB63D:
        sta     $0104,x
        bit     $FC00
LB643:
        lda     #$00
        sta     ($F2),y
        rts

; 0xB648------------------------------
dont_know_5C:
        lda     #$A4
        sta     $04C6
        lda     BrkSRC_ROM_23C_FF
        bne     LB65B
        lda     #$05
        bne     LB672
; 0xB656------------------------------
dont_know_51:
        lda     #$84
        sta     $04C6
LB65B:
        lda     $F9
        pha
        lda     $F8
        pha
        ldx     StackTail_211
        lda     $0102,x
        and     #$F8
        lsr     a
        lsr     a
        lsr     a
        cmp     #$20
        bcc     LB682
LB670:
        lda     #$02
LB672:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
LB67B:
        pla
        sta     $F8
        pla
        sta     $F9
        rts

LB682:
        lda     $0102,x
        and     #$07
        sta     $04BF
        asl     a
        sta     $04C0
        asl     a
        asl     a
        sta     $04C1
        ldy     $04BF
        lda     $04BA,y
        cmp     #$41
        beq     LB6A1
        lda     #$21
        bne     LB672
LB6A1:
        jsr     LB760
        lda     (POIN_F0),y
        cmp     #$FF
        beq     LB670
        iny
        lda     (POIN_F0),y
        bpl     LB6B9
        ldx     $04C6
        cpx     #$84
        bne     LB6B9
        jmp     LB750

LB6B9:
        and     #$7F
        sta     $04C4
        dey
        lda     (POIN_F0),y
        sta     $04C3
        ldx     $04C0
        lda     $F778,x
        sta     $F8
        lda     $F779,x
        sta     $F9
        lda     #$7D
        ldy     #$F4
        jsr     Vector_LOCK
        ldx     StackTail_211
        ldy     #$4C
        lda     $0104,x
        sta     ($F8),y
        iny
        lda     $0101,x
        sta     ($F8),y
        ldy     #$4A
        lda     $04C3
        sta     ($F8),y
        iny
        lda     $04C4
        sta     ($F8),y
        jsr     LB8A5
        lda     $04C6
        sta     $F47A
        lda     #$27
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        lda     $F8
        ldy     $F9
        brk
        bvc     LB75E
        ora     #$AE
        ora     ($02),y
        sta     $0104,x
        jmp     LB748

        jsr     LB4A6
        jsr     LB760
        sty     $076E
        ldy     #$00
        lda     ($F8),y
        ldy     #$40
        ora     ($F8),y
        ldy     #$80
        ora     ($F8),y
        ldy     #$C0
        ora     ($F8),y
        ldy     $076E
        iny
        cmp     #$20
        bne     LB742
        lda     (POIN_F0),y
        ora     #$80
        sta     (POIN_F0),y
        jmp     LB748

LB742:
        lda     (POIN_F0),y
        and     #$7F
        sta     (POIN_F0),y
LB748:
        lda     #$00
        sta     $F47D
        jmp     LB67B

LB750:
        jsr     LB4A6
        lda     #$20
        ldy     #$00
LB757:
        sta     ($F8),y
        iny
        bne     LB757
        clv
        .byte   $4C
LB75E:
        .byte   $7B
        .byte   $B6
LB760:
        lda     $0102,x
        and     #$F8
        lsr     a
        lsr     a
        tay
        ldx     $04C0
        lda     $F76E,x
        sta     POIN_F0
        lda     $F76F,x
        sta     POIN_F0+1
        rts

; 0xB776------------------------------
dont_know_52:
        jsr     LB4A6
        jsr     Vector_DRIVE
        bvs     LB78C
        ldy     #$46
        lda     ($F8),y
        bne     LB78A
        ldy     #$49
        lda     ($F8),y
        beq     LB796
LB78A:
        lda     #$E5
LB78C:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB796:
        ldx     StackTail_211
        lda     $0102,x
        bne     LB7A2
LB79E:
        lda     #$F0
        bne     LB78C
LB7A2:
        cmp     #$04
        bcs     LB79E
        cmp     #$01
        beq     LB7B6
        ldx     $04BF
        lda     $F497,x
        beq     LB7B6
        lda     #$C5
        bne     LB78C
LB7B6:
        ldy     #$50
        lda     ($F8),y
        beq     LB7C0
        lda     #$E0
        bne     LB78C
LB7C0:
        ldx     $04BF
        lda     $F49C,x
        ldy     #$71
        sta     ($F8),y
        lda     #$7E
        ldy     #$F4
        jsr     Vector_LOCK
        lda     #$21
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        lda     #$00
        sta     $F47E
        rts

; 0xB7E0------------------------------
dont_know_53:
        jsr     LB4A6
        jsr     LB90E
        bvs     LB7F0
        ldy     #$46
        lda     ($F8),y
        beq     LB7FA
LB7EE:
        lda     #$E5
LB7F0:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB7FA:
        ldy     #$49
        lda     ($F8),y
        bne     LB7EE
        ldy     #$50
        lda     ($F8),y
        bne     LB80A
        lda     #$E1
        bne     LB7F0
LB80A:
        lda     #$7E
        ldy     #$F4
        jsr     Vector_LOCK
        lda     #$1E
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        rts

; 0xB81B------------------------------
dont_know_54:
        lda     #$84
LB81D:
        sta     $04C2
        jsr     LB4A6
        jsr     LB90E
        bvs     LB830
        ldy     #$49
        lda     ($F8),y
        beq     LB83A
        lda     #$E5
LB830:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB83A:
        ldy     #$50
        lda     ($F8),y
        bne     LB844
        lda     #$E1
        bne     LB830
LB844:
        cmp     #$01
        bne     LB855
        lda     $04C2
        beq     LB851
        cmp     #$A4
        bne     LB855
LB851:
        lda     #$CA
        bne     LB830
LB855:
        lda     #$24
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        rts

; 0xB85F------------------------------
dont_know_55:
        lda     #$A4
        jmp     LB81D

; 0xB864------------------------------
dont_know_56:
        lda     #$84
LB866:
        sta     $04C2
        jsr     LB4A6
        ldy     #$49
        lda     ($F8),y
        beq     LB876
        lda     #$E5
        bne     LB88B
LB876:
        ldy     #$48
        lda     ($F8),y
        asl     a
        asl     a
        asl     a
        tax
        lda     $F700,x
        cmp     #$25
        beq     LB891
        cmp     #$21
        beq     LB891
        lda     #$21
LB88B:
        ldy     #$49
        sta     ($F8),y
        clv
        rts

LB891:
        jsr     LB8A5
        lda     $04C2
        sta     $F47A
        lda     #$27
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        clv
        rts

LB8A5:
        lda     #$5F
        ldy     #$F4
        jsr     Vector_LOCK
        ldy     #$48
        lda     ($F8),y
        tax
        sei
        lda     #$88
        sta     Floppy_MotorSel
        ora     Floppy_DriveSelect,x
        sta     Floppy_MotorSel
        and     #$F6
        sta     Floppy_MotorSel
        cli
        rts

; 0xB8C4------------------------------
dont_know_57:
        lda     #$A4
        jmp     LB866

; 0xB8C9------------------------------
dont_know_58:
        lda     #$E4
        jmp     LB866

; 0xB8CE------------------------------
dont_know_59:
        lda     #$F4
        jmp     LB866

; 0xB8D3------------------------------
dont_know_5A:
        lda     #$00
        jmp     LB81D

; 0xB8D8------------------------------
dont_know_5B:
        jsr     LB4A6
        ldy     #$40
        lda     ($F8),y
        cmp     #$03
        beq     LB8E7
        lda     #$F1
        bne     LB904
LB8E7:
        jsr     Vector_DRIVE
        bvs     LB904
        ldx     $04BF
        lda     $F497,x
        beq     LB8F8
        lda     #$C5
        bne     LB904
LB8F8:
        lda     #$15
        ldy     #$A0
        ldx     #$01
        jsr     Vector_CALL
        bvs     LB904
        rts

LB904:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LB90E:
        ldy     #$48
        lda     ($F8),y
        tax
        ldy     #$71
        lda     ($F8),y
        cmp     $F49C,x
        beq     LB922
        lda     #$CF
        bit     $FC00
        rts

LB922:
        clv
        jsr     Vector_DRIVE
        rts

LB927:
        .byte   "EVMO"
LB92B:
        .byte   $10
        .byte   "@0 "
LB92F:
        .byte   $19
        .byte   "-M"
        .byte   $8B
        .byte   "M="
        .byte   $FE,$FF
        .byte   "S="
        .byte   $FE,$FF
        .byte   "B="
        .byte   $FE,$FF
        .byte   "O="
        .byte   $FE,$FF
        .byte   "Z="
        .byte   $FE,$FF
        .byte   "D="
        .byte   $FE,$FF
        .byte   "P="
        .byte   $FE,$FF
        .byte   "S1="
        .byte   $FE,$FF
        .byte   "S2="
        .byte   $FE,$FF
        .byte   "S3="
        .byte   $FE,$FF
        .byte   "S4="
        .byte   $FE,$FF
        .byte   "S5="
        .byte   $FE,$FF
        .byte   "S6="
        .byte   $FE,$FF
        .byte   "S7="
        .byte   $FE,$FF
        .byte   "C1="
        .byte   $FE,$FF
        .byte   "C2="
        .byte   $FE,$FF
        .byte   "C3="
        .byte   $FE,$FF,$FF
LB982:
        .byte   $00,$AF,$A7,$A7,$80,$83,$86,$89
        .byte   $8C,$8F,$92,$95,$98,$9B,$9E,$A1
        .byte   $A4
LB993:
        .byte   $08,$02,$04,$02,$02,$02,$02,$02
        .byte   $02,$02,$02,$02,$02,$02,$02,$02
        .byte   $02
LB9A4:
        .byte   $10
        .byte   " @"
        .byte   $80
LB9A8:
        .byte   " EV M   O       "

; 0xB9B8------------------------------
dont_know_34:
        lda     $049C
        ldy     $049D
        sta     POIN_F0
        sty     POIN_F0+1
        jsr     LA6A6
        lda     $0736
        sta     $0774
        ldx     StackTail_211
        lda     $0102,x
        sta     $0773
        lda     $0104,x
        sta     $F8
        lda     $0101,x
        sta     $F9
        lda     $0773
        and     #$01
        beq     LBA34
        ldy     #$00
        tya
LB9E8:
        sta     ($F8),y
        iny
        cpy     #$FF
        bcc     LB9E8
        lda     $0773
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     LB9A8,x
        ldy     #$00
        sta     ($F8),y
        lda     $F8
        ldy     $F9
        ldx     #$01
        brk
        clc
        bvs     LBA1D
        ldy     #$48
        lda     ($F8),y
        cmp     #$FF
        bne     LBA15
        lda     #$21
        jmp     LBA1F

LBA15:
        ldy     #$40
        lda     ($F8),y
        cmp     #$03
        beq     LBA34
LBA1D:
        lda     #$A1
LBA1F:
        pha
        lda     $0774
        sta     $0736
        jsr     LA6AE
        pla
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

LBA34:
        ldy     #$01
        lda     ($F8),y
        cmp     #$2E
        beq     LBA40
LBA3C:
        lda     #$B1
        bne     LBA1F
LBA40:
        ldy     #$00
        lda     ($F8),y
        ldx     #$03
LBA46:
        cmp     LB927,x
        beq     LBA50
        dex
        bpl     LBA46
        bmi     LBA3C
LBA50:
        lda     LB92B,x
        ldy     #$6D
        sta     ($F8),y
        lda     $0773
        and     LB9A4,x
        beq     LBA3C
        lda     $0773
        and     LB92F,x
        sta     $0773
        and     #$0E
        beq     LBA7D
LBA6C:
        lda     #$2C
        brk
        .byte   $12
        bvs     LBA7D
        lda     #$33
        ldy     #$B9
        brk
        asl     $50,x
        ora     $20
        sty     $A6,x
LBA7D:
        clv
        rts

        tax
        lda     LB982,x
        bne     LBAC4
        lda     $0773
        and     LB993,x
        beq     LBA1D
        lda     $F8
        ldy     $F9
        sta     $0775
        sty     $0776
        clc
        cld
        lda     $F8
        adc     #$AB
        sta     $0772
        lda     $F9
        adc     #$00
        tay
        lda     $0772
        ldx     #$00
        brk
        ora     $AD,x
        adc     $07,x
        ldy     $0776
        sta     $F8
        sty     $F9
        bvc     LBABB
LBAB8:
        jmp     LBA1D

LBABB:
        ldy     #$AA
        lda     #$FF
        sta     ($F8),y
        jmp     LBA6C

LBAC4:
        tay
        lda     $0773
        and     LB993,x
        beq     LBAB8
        sty     $0772
        brk
        .byte   $14
        bvs     LBAB8
        sta     $0775
        sty     $0776
        ldy     $0772
        lda     #$FF
        sta     ($F8),y
        iny
        lda     $0775
        sta     ($F8),y
        iny
        lda     $0776
        sta     ($F8),y
        jmp     LBA6C

; 0xBAF0------------------------------
dont_know_0C:
        jsr     GetAddress_afterFunction
        ldy     #$00
LBAF5:
        lda     ($F2),y
        sta     $051C,y
        iny
        cpy     #$FF
        bcc     LBAF5
        jsr     LBEEB
        php
        pha
        jsr     GetAddress_afterFunction
        ldy     #$00
LBB09:
        lda     $051C,y
        sta     ($F2),y
        iny
        cpy     #$FF
        bcc     LBB09
        pla
        plp
        bvc     LBB1D
        ldx     StackTail_211
        sta     $0104,x
LBB1D:
        rts

        jsr     LBF40
        lda     $069F
        cmp     #$CC
        bne     LBB42
        jmp     LBB47

        jsr     LBF40
        lda     $069F
        cmp     #$BB
        bne     LBB42
        jmp     LBB47

        jsr     LBF40
        lda     $069F
        cmp     #$AA
        beq     LBB47
LBB42:
        lda     #$B1
        jmp     LBF10

LBB47:
        lda     $069E
        cmp     #$06
        bne     LBB42
        lda     $05CB
        bne     LBB6B
        lda     $051C
        cmp     #$4D
        beq     LBB6B
        lda     #$FF
        sta     $05CB
        lda     $06A0
        ldy     $06A1
        sta     $05CC
        sty     $05CD
LBB6B:
        jsr     LBF40
        lda     $05C3
        beq     LBB9A
        sec
        cld
        lda     $05C4
        sbc     $069F
        sta     $05C4
        lda     $05C5
        sbc     $06A0
        sta     $05C5
        clc
        lda     $05CC
        adc     $05C4
        sta     $05CC
        .byte   $AD
LBB92:
        cmp     $6D05
        cmp     $05
        sta     $05CD
LBB9A:
        lda     $069E
        cmp     #$03
        beq     LBBC5
        cmp     #$06
        bcc     LBB42
        lda     $069F
        .byte   $8D
        .byte   $7F
LBBAA:
        .byte   $07
        lda     $06A0
        sta     $0780
        .byte   $A2
LBBB2:
        .byte   $05
LBBB3:
        lda     $069C,x
        jsr     LBF77
        inx
LBBBA:
        cpx     $069E
        bcc     LBBB3
        jsr     LBF40
LBBC2:
        jmp     LBB9A

LBBC5:
        brk
        .byte   $C3
        .byte   $1C
        ora     $60
LBBCA:
        bmi     LBB92
        cpy     #$07
        .byte   $32
        .byte   $3C
        cpy     $48
        bpl     LBB9A
        cpy     #$07
        .byte   $34
        cpy     $48C4
LBBDA:
        rti

        stx     _ZERO_,y
        .byte   $07
        .byte   $32
        .byte   $3C
        .byte   $44
        pha
        bpl     LBBAA
        cpy     #$07
        .byte   $34
        cpy     $48C4
LBBEA:
        bmi     LBBB2
        bvc     LBBF5
        .byte   $32
        .byte   $3C
        .byte   $44
        pha
        bpl     LBBBA
        .byte   $A0
LBBF5:
        .byte   $07
        .byte   $34
        cpy     $48C4
        bmi     LBBC2
        bcs     LBC05
        .byte   $32
        .byte   $3C
LBC00:
        .byte   $44
        pha
        bpl     LBBCA
        .byte   $C0
LBC05:
        .byte   $07
        .byte   $34
        cpy     $48C4
        cpy     #$C6
        brk
        .byte   $07
        .byte   $3C
        .byte   $3C
        .byte   $44
        pha
        bpl     LBBDA
        brk
        .byte   $07
        .byte   $34
        .byte   $3C
        cpy     $C8
        jsr     L0026
        .byte   $07
        .byte   $32
        .byte   $3C
        .byte   $44
        pha
        bpl     LBBEA
        brk
        .byte   $07
        .byte   $34
        .byte   $3C
        .byte   $44
        pha
        jsr     L00C6
        .byte   $07
        .byte   $32
        .byte   $3C
        .byte   $44
        pha
        bpl     LBC00
        cpy     #$0C
        .byte   $34
        cpy     $4CC4
        jsr     L00CC
        .byte   $0C
        .byte   $32
        .byte   $3C
        .byte   $44
        jmp     LC610

        cpy     #$07
        .byte   $34
        cpy     $48C4
LBC4A:
        .byte   $76
LBC4B:
        ldy     $BC6A,x
        .byte   $64
        ldy     LBC64,x
        .byte   $9E
        ldy     $BCA1,x
        cmp     $F3BC,y
        ldy     LBD0D,x
        and     ($BD),y
        cmp     ($BC),y
        .byte   $EB
        ldy     LBD2C,x
LBC64:
        jsr     LBF77
        jmp     LBE9D

        jsr     LBF77
        jsr     LBF59
        jsr     LBF77
        jmp     LBE9D

        jsr     LBF77
        jsr     LBF59
        sta     $077E
        and     #$0F
        beq     LBC89
        jsr     LBF59
        jsr     LBF59
LBC89:
        lda     $077E
        lsr     a
        lsr     a
        lsr     a
        tax
        jsr     LBF59
        clc
        cld
        adc     $0781,x
        jsr     LBF77
        jmp     LBE9D

LBC9E:
        jsr     LBF77
        jsr     LBF59
        sta     $077E
        and     #$0F
        beq     LBCB1
        jsr     LBF59
        jsr     LBF59
LBCB1:
        lda     $077E
        lsr     a
        lsr     a
        lsr     a
        tax
        jsr     LBF59
        clc
        cld
        adc     $0781,x
        php
        jsr     LBF77
        jsr     LBF59
        plp
        adc     $0782,x
        jsr     LBF77
        jmp     LBE9D

        jsr     LBF59
        jsr     LBF59
        lda     #$F0
        lsr     a
        lsr     a
        lsr     a
        tax
        jsr     LBF59
        clc
        cld
        adc     $0781,x
        jsr     LBF77
        jmp     LBE9D

        jsr     LBF59
        jsr     LBF59
        lda     #$F0
        lsr     a
        lsr     a
        lsr     a
        tax
        jsr     LBF59
        clc
        cld
        adc     $0781,x
        php
        jsr     LBF59
        plp
        adc     $0782,x
        jsr     LBF77
        jmp     LBE9D

LBD0D:
        and     #$F0
        lsr     a
        lsr     a
        lsr     a
        tax
        jsr     LBF59
        clc
        cld
        adc     $0781,x
        sta     $077F
        php
        jsr     LBF59
        plp
        adc     $0782,x
        sta     $0780
        jmp     LBE9D

LBD2C:
        lda     #$A6
        jmp     LBF10

LBD31:
        jsr     LBF40
        lda     $069F
        cmp     #$45
        beq     LBD31
        cmp     #$49
        bne     LBD58
        lda     $05D1
        beq     LBD31
        ldx     #$0A
LBD46:
        lda     $06A0,x
        sta     $05D4,x
        dex
        bpl     LBD46
        jsr     LBD55
        jmp     LBD31

LBD55:
        jmp     (L05D2)

LBD58:
        cmp     #$53
        bne     LBD2C
        jsr     LBF59
        jsr     LBF59
        cmp     #$00
        beq     LBD90
        lda     $05CB
        bne     LBD90
        jsr     LBF59
        lsr     a
        lsr     a
        lsr     a
        tax
        jsr     LBF59
        clc
        cld
        adc     $0781,x
        sta     $05CC
        php
        jsr     LBF59
        plp
        adc     $0782,x
        sta     $05CD
        lda     #$FF
        sta     $05CB
        jmp     LBD99

LBD90:
        jsr     LBF59
        jsr     LBF59
        jsr     LBF59
LBD99:
        jsr     LBF59
        sta     $077E
        jsr     LBF59
        ora     $077E
        sta     $077E
        lda     $07A7
        beq     LBDB2
        lda     #$A5
        jmp     LBF10

LBDB2:
        lda     $077E
        beq     LBDBC
        lda     #$A7
        jmp     LBF10

LBDBC:
        brk
        .byte   $C3
        .byte   $1C
        ora     $60
        jsr     LBF40
        ldx     #$07
LBDC6:
        lda     $06A0,x
        sta     $05DF,x
        dex
        bpl     LBDC6
        lda     $06A9
        ora     $06AA
        sta     $07A7
        jsr     LBF40
        ldx     #$04
        ldy     #$00
        sty     $0778
        sty     $0779
LBDE5:
        ldy     $0779
        lda     $059C,y
        beq     LBE13
        lda     $059D,y
        sta     $077C
        lda     $059E,y
        sta     $077D
        ldy     $0778
        cld
        sec
        lda     $077C
        sbc     $069C,x
        sta     $0781,y
        lda     $077D
        sbc     $069D,x
        sta     $0782,y
        jmp     LBE1E

LBE13:
        ldy     $0778
        lda     #$00
        sta     $0781,y
        sta     $0782,y
LBE1E:
        clc
        cld
        lda     $069C,x
        adc     $0781,y
        sta     $077C
        lda     $069D,x
        adc     $0782,y
        sta     $077D
        ldy     $0779
        cpx     #$04
        bne     LBE58
        cmp     #$00
        bne     LBE53
        clc
        lda     $077C
        adc     $06B6,x
        sta     $059D,y
        lda     #$00
        sta     $059E,y
        bcc     LBE77
        lda     $059D,y
        beq     LBE77
LBE53:
        lda     #$A8
        jmp     LBF10

LBE58:
        clc
        lda     $077C
        adc     $06B6,x
        sta     $059D,y
        lda     $077D
        adc     $06B7,x
        sta     $059E,y
        bcc     LBE77
        ora     $059D,y
        beq     LBE77
        lda     #$A9
        jmp     LBF10

LBE77:
        inc     $0778
        inc     $0778
        inc     $0779
        inc     $0779
        inc     $0779
        inx
        inx
        cpx     #$1E
        bcs     LBE8F
        jmp     LBDE5

LBE8F:
        lda     #$00
        sta     $079F
        sta     $07A0
        jsr     LBF40
        inc     $069D
LBE9D:
        jsr     LBF59
        sta     $077E
        lsr     a
        tax
        lda     LBBCA,x
        bcs     LBEAE
        lsr     a
        lsr     a
        lsr     a
        lsr     a
LBEAE:
        and     #$0F
        asl     a
        tax
        lda     LBC4A,x
        sta     L077A
        lda     LBC4B,x
        sta     $077B
        lda     $077E
        .byte   $6C
LBEC2:
        .byte   $7A
LBEC3:
        .byte   $07
        sec
        .byte   $BB
        cmp     ($BD,x)
        .byte   $2B
        .byte   $BB
        asl     $85BB,x
        inc     $FF84,x
        txa
        ldy     #$00
        sta     (InterruptControl_Reg),y
        lda     (InterruptControl_Reg),y
        sta     InterruptControl_Reg
        cpx     InterruptControl_Reg
        bne     LBEDE
        rts

LBEDE:
        lda     #$AA
        bit     $FC00
        rts

LBEE4:
        ldx     $07A1
        txs
        jmp     LBF10

LBEEB:
        lda     $05CE
        bne     LBEFA
        lda     #$CC
        ldy     #$BE
        sta     $05CF
        sty     $05D0
LBEFA:
        ldx     #$01
        brk
        .byte   $C2
        .byte   $1C
        ora     $50
        ora     ($60,x)
        lda     $054A
        lsr     a
        lsr     a
        lsr     a
        and     #$1E
        tax
        bne     LBF1A
LBF0E:
        lda     #$B1
LBF10:
        pha
        brk
        .byte   $C3
        .byte   $1C
        ora     $68
        bit     $FC00
        rts

LBF1A:
        cpx     #$0A
        bcs     LBF0E
        lda     LBEC2,x
        sta     L077A
        lda     LBEC3,x
        sta     $077B
        lda     #$8F
        sta     $069C
        lda     #$9C
        ldy     #$06
        sta     $055D
        sty     $055E
        tsx
        stx     $07A1
        jmp     (L077A)

LBF40:
        sty     $07A4
        brk
        cpy     $1C
        ora     $70
        .byte   $02
        brk
        rti

        ldy     #$03
        sty     $069D
        ldy     $07A4
        bvs     LBF56
        rts

LBF56:
        jmp     LBEE4

LBF59:
        sty     $07A2
        ldy     $069D
        cpy     $069E
        bcc     LBF6A
        jsr     LBF40
        inc     $069D
LBF6A:
        ldy     $069D
        inc     $069D
        lda     $069C,y
        ldy     $07A2
        rts

LBF77:
        stx     $07A3
        sty     $07A2
        sta     $077E
        clc
        cld
        lda     $05C4
        adc     $077F
        sta     $07A5
        lda     $05C5
        adc     $0780
        sta     $07A6
        lda     $05C6
        beq     LBFB8
        cld
        sec
        lda     $07A5
        sbc     $05C7
        lda     $07A6
        sbc     $05C8
        bcc     LBFD9
        sec
        lda     $05C9
        sbc     $07A5
        lda     $05CA
        sbc     $07A6
        bcc     LBFD9
LBFB8:
        ldx     $077E
        lda     $07A5
        ldy     $07A6
        clv
        jsr     Vector_LOFRUP
        bvc     LBFCA
        jmp     LBEE4

LBFCA:
        inc     $077F
        bne     LBFD2
        inc     $0780
LBFD2:
        ldy     $07A2
        ldx     $07A3
        rts

LBFD9:
        lda     #$A4
        jmp     LBEE4

        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $75
