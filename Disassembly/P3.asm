; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/ROM_Board/P3.bin
; Page:       1


        .setcpu "6502"

_ZERO_          := $0000                        ; 0x0000
L0002           := $0002
L000B           := $000B
L00DC           := $00DC
POIN_F0         := $00F0
POINC1_F4       := $00F4
POINC2_F6       := $00F6
POINI_FA        := $00FA
InterruptControl_Reg:= $00FE
StackTail_211   := $0211
JCSREG_0212     := $0212
JumpAddrL_221   := $0221
JumpAddrH_222   := $0222
BankTemp_22D    := $022D
BRK_TRAP_22F    := $022F
BrkSRC_ROM_23C_FF:= $023C
PANIC_SRCL_25F  := $025F
PANIC_SRCH_260  := $0260
Status_Serial2_Backup_03e5:= $03E5
UART_Flags_03e7 := $03E7
StoreA_in_SerialCommandReg:= $03E8
A_from_to_SerialRX_TX_Reg:= $03EC
SpecialCharacters_Copy:= $03F0
FunctionByte_afterBRK_728:= $0728
L0A14           := $0A14
L1843           := $1843
L2020           := $2020
L202E           := $202E
L2041           := $2041
L2044           := $2044
L2045           := $2045
L2620           := $2620
L4144           := $4144
L414D           := $414D
L4150           := $4150
L4241           := $4241
L4249           := $4249
L424F           := $424F
L42FF           := $42FF
L4320           := $4320
L4520           := $4520
L4556           := $4556
L4620           := $4620
L464F           := $464F
L4720           := $4720
L4C43           := $4C43
L4C49           := $4C49
L4C6C           := $4C6C
L4D20           := $4D20
L4E49           := $4E49
L4E4D           := $4E4D
L4F46           := $4F46
L4F4C           := $4F4C
L500B           := $500B
L5020           := $5020
L5245           := $5245
L524F           := $524F
L5253           := $5253
L5320           := $5320
L5345           := $5345
L5420           := $5420
L564F           := $564F
L5849           := $5849
L5859           := $5859
L5920           := $5920
L5944           := $5944
L6520           := $6520
Vector_CALL     := $801E                        ; 0x801e
Vector_CREATE   := $8021                        ; 0x8021
Vector_DISMOUNT := $8024                        ; 0x8024
Vector_DRIVE    := $8027                        ; 0x8027
Vector_FHIRQRST := $802A                        ; 0x802A
Vector_FHIRQRW  := $802D                        ; 0x802D
Vector_FHIRQSK  := $8030                        ; 0x8030
Vector_FHNMIADR := $8033                        ; 0x8033
Vector_FHNMIRW  := $8036                        ; 0x8036
Vector_LOCK     := $8039                        ; 0x8039
Vector_LOGON    := $803C                        ; 0x803c
Vector_LODPOIS2 := $803F                        ; 0x803F
Vector_MYINIT   := $8042                        ; 0x8042
Vector_OUTAUS   := $8045                        ; 0x8045
Vector_OUTVOR   := $8048                        ; 0x8048
Vector_PROGTERM := $804B                        ; 0x804b
Vector_SCHEDUL  := $804E                        ; 0x804e
Vector_SVC60    := $8051                        ; 0x8051
Vector_SVC61    := $8054                        ; 0x8054
Vector_SVC62    := $8057                        ; 0x8057
Vector_SVC63    := $805A                        ; 0x805a
Vector_SVC64    := $805D                        ; 0x805d
Vector_SVC65    := $8060                        ; 0x8060
Vector_TASKTERM := $8063                        ; 0x8063
Vector_THPRMPTE := $8066                        ; 0x8066
Vector_THWRITEE := $8069                        ; 0x8069
Vector_LOFRUP   := $806C                        ; 0x806c
Vector_SVC0D    := $806F                        ; 0x806f
LC1C5           := $C1C5
LCE0B           := $CE0B
LD844           := $D844
LD941           := $D941
PORT_B_Shadow_f3e8:= $F3E8
PH3TSK_F400_PRINTER_BUSY:= $F400
C3USAD_F41A     := $F41A
C3USAD_P3_F41D  := $F41D
C3ENT_F420      := $F420
Users_Installed_F421:= $F421                    ; 0xf421
C3USNR_F422     := $F422
SingleMultiMode_f423:= $F423
C3DATE_F424     := $F424                        ; 0xf424
C3TIME_F42E     := $F42E                        ; 0xf42e
C3TI20_F436     := $F436                        ; 0xf436
Bank_Shadow_F438:= $F438
C3LGNZ_F43C_USERS:= $F43C
MY3MAP_F448     := $F448
MY3MAP_P1_F449  := $F449
MY3MAP_P2_F44A  := $F44A
MY3MAP_P3_F44B  := $F44B
IPSW_F4AB       := $F4AB
IPANZ_F4AC_INIT_IPC:= $F4AC
IPADR_F4AE      := $F4AE
IPADR_P1_F4AF   := $F4AF
NMIURS_M1       := $F7FE
NMIURS          := $F7FF
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
Floppy_DriveSelect:= $FC11                      ; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
LFE3D           := $FE3D
        jmp     LA3ED

        jmp     LA4D4

        jmp     LB5D2

        jmp     LB5BC

        jmp     LB47E

        jmp     LBB3F

        jmp     LBD65

        jmp     LBE0C

        jmp     LBE3E

        jmp     LBE3F

        .byte   $4C
        .byte   $B0
LA020:
        .byte   $BB
        jmp     LBC17

        jmp     LBAEF

        jmp     LBAFE

        jmp     LBAD3

        jmp     LBB51

        jmp     LBB57

        jmp     LBB77

        jmp     LBF4F

        jmp     LBCEC

        jmp     LBCF5

        jmp     LBCD2

        jmp     LB304

        .byte   $4C
LA046:
        .byte   $12
        .byte   $B4
LA048:
        rti

        inc     $2EEE
        eor     $4E
        .byte   $A0
LA04F:
        .byte   $4F
        rol     $2E56
        eor     ($53,x)
        .byte   $53
        rol     $3656
        and     $20,x
        jsr     L2020
        jsr     L2020
        jsr     L2020
        jsr     L2020
        jsr     L2020
        jsr     L2020
        jsr     L4D20
        eor     InterruptControl_Reg
        .byte   $FF
        eor     $4E
        inc     $FFFF,x
        .byte   $4F
        and     $FFFE,x
        jmp     LFE3D

        .byte   $FF
        eor     LFE3D
        .byte   $FF
        lsr     $3D,x
        inc     $40FF,x
        and     $FFFE,x
        rol     a
        eor     #$FE
        .byte   $FF
        rol     a
        .byte   $53
        inc     $2AFF,x
        lsr     $FFFE
        rol     a
        .byte   $5A
        inc     $FFFF,x
LA09D:
        .byte   $4C
LA09E:
        .byte   $0B
        dec     $480A
        .byte   $0C
LA0A3:
        .byte   $4F
        rol     L2020
        jmp     L202E

LA0AA:
        jsr     L414D
        .byte   $43
        rol     $3D3F
        .byte   $4B
        jmp     L0A14

        brk
        brk
        sbc     a:$08,x
        brk
        brk
        brk
        .byte   $5A
        inc     $44FF,x
        inc     $50FF,x
        inc     $53FF,x
        and     (InterruptControl_Reg),y
        .byte   $FF
        .byte   $53
        .byte   $32
        inc     $53FF,x
        .byte   $33
        inc     $53FF,x
        .byte   $34
        inc     $53FF,x
        and     InterruptControl_Reg,x
        .byte   $FF
        .byte   $53
        rol     InterruptControl_Reg,x
        .byte   $FF
        .byte   $53
        .byte   $37
        inc     $43FF,x
        and     (InterruptControl_Reg),y
        .byte   $FF
        .byte   $43
        .byte   $32
        inc     $43FF,x
        .byte   $33
        inc     a:$FF,x
        inc     $59FF,x
        inc     $41FF,x
        inc     $FFFF,x
LA0F8:
        .byte   $5A
        jsr     L2044
        bvc     LA11E
        .byte   $53
        and     ($53),y
        .byte   $32
        .byte   $53
        .byte   $33
        .byte   $53
        .byte   $34
        .byte   $53
        and     $53,x
        rol     $53,x
        .byte   $37
        .byte   $43
        and     ($43),y
        .byte   $32
        .byte   $43
        .byte   $33
        jsr     L5920
        jsr     L2041
LA118:
        jsr     L5859
        .byte   $4D
LA11C:
        .byte   $20
        .byte   $23
LA11E:
        rol     $302F
        and     ($32),y
        .byte   $33
        .byte   $34
        and     $36,x
        .byte   $37
        sec
        and     L2020,y
        rti

        eor     ($42,x)
        .byte   $43
        .byte   $44
        eor     $46
        .byte   $47
        pha
        eor     #$4A
        .byte   $4B
        jmp     L4E4D

        .byte   $4F
        bvc     LA18F
        .byte   $52
        .byte   $53
        .byte   $54
        eor     $56,x
        .byte   $57
        cli
        eor     $FF5A,y
        bit     $FE58
        .byte   $FF
        bit     $FE59
        .byte   $FF
        .byte   $FF
        bit     $2958
        inc     $29FF,x
        bit     $FE59
        .byte   $FF
        .byte   $FF
        rti

        .byte   $43
        eor     $2D44
        jmp     L4E49

        eor     $20
        .byte   $54
        .byte   $4F
        .byte   $4F
        jsr     L4F4C
        lsr     $41C7
        eor     #$4C
        jmp     L4320

        eor     $2D44
        jmp     L4E49

        cmp     $42
        eor     ($53,x)
        .byte   $53
        eor     $4D
        .byte   $42
        jmp     L5245

        jsr     L5345
        .byte   $43
        eor     ($50,x)
        eor     $C4
        .byte   $43
        .byte   $41
LA18F:
        .byte   $53
        .byte   $53
        eor     $4D
        .byte   $42
        jmp     L5245

        jsr     L4241
        .byte   $4F
        .byte   $52
        .byte   $54
        eor     $C4
        .byte   $44
        eor     $5349
        .byte   $53
        eor     #$4E
        .byte   $47
        jsr     L564F
        eor     $52
        jmp     LD941

        eor     $4D
        eor     ($43,x)
        jmp     L4249

        jsr     L4C49
        jmp     L4620

        .byte   $4F
        .byte   $52
        eor     $D441
        .byte   $FF
LA1C2:
        .byte   $43
LA1C3:
        eor     $45
        lsr     $5344
        eor     $51
        eor     #$45
        eor     #$4D
        eor     #$4E
        eor     #$50
        jmp     L4C43

        .byte   $53
        eor     $4D44
        eor     $4D
        cli
        eor     $504F
        .byte   $47
        .byte   $53
        .byte   $43
LA1E2:
        .byte   $53
        .byte   $53
        .byte   $53
        .byte   $47
        .byte   $53
        .byte   $54
        .byte   $54
        eor     #$41
        .byte   $44
        .byte   $42
        eor     $4946,y
        .byte   $FF
LA1F1:
        .byte   $19
LA1F2:
        .byte   $B2
        sta     $18AF
LA1F6:
        bcs     LA1E2
        .byte   $AF
        bit     $54B2
        .byte   $B2
        .byte   $3A
        .byte   $B2
        pha
        .byte   $B2
        and     $2FB1,y
        lda     ($B4),y
        .byte   $B2
        ldy     $C0B2,x
        .byte   $B2
        .byte   $C7
        bcs     LA1F6
        bcs     LA253
        lda     ($4D),y
        lda     ($59),y
        lda     ($78),y
        .byte   $B2
        ora     #$B1
        inc     $AE,x
        and     $AF
        .byte   $62
        .byte   $B0
LA21F:
        .byte   $52
LA220:
        .byte   $4F
LA221:
        .byte   $52
LA222:
        cmp     $C1
        ror     $667E
        ror     $6A,x
        eor     ($44,x)
        .byte   $43
        inx
        .byte   $DA
        adc     $797D
        adc     $75
        adc     ($61),y
        adc     #$41
        lsr     $E844
        .byte   $DA
        and     $393D
        and     $35
        and     ($21),y
        and     #$41
        .byte   $53
        jmp     LC1C5

        asl     $061E
        asl     $0A,x
        .byte   $42
        eor     #$54
        .byte   $82
        .byte   $80
        .byte   $2C
LA253:
        bit     $43
        eor     $E850
        .byte   $DA
        cmp     $D9DD
        cmp     $D5
        cmp     ($C1),y
        cmp     #$43
        bvc     LA2BC
        .byte   $83
        .byte   $82
        cpx     $E0E4
        .byte   $43
        bvc     LA2C5
        .byte   $83
        .byte   $82
        cpy     $C0C4
        .byte   $44
LA272:
        eor     $43
        cpy     $C0
        dec     $C6DE
        dec     $45,x
        .byte   $4F
        .byte   $52
        inx
        .byte   $DA
        eor     $595D
        eor     $55
        eor     ($41),y
        eor     #$49
        lsr     $C443
        cpy     #$EE
        inc     $F6E6,x
        lsr     a
        eor     $9250
        brk
        jmp     L4C6C

        .byte   $44
        eor     ($E8,x)
        .byte   $DA
        lda     LB9BD
        .byte   $A5
LA2A0:
        lda     $B1,x
        lda     ($A9,x)
        jmp     $5844

        lda     $A2
        ldx     LA6BE
        ldx     $A2,y
        jmp     L5944

        cmp     $C2
        ldy     LA4BC
        ldy     $A0,x
        jmp     L5253

        .byte   $C4
LA2BC:
        cmp     ($4E,x)
        lsr     $5646,x
        lsr     a
        .byte   $53
        .byte   $52
        .byte   $81
LA2C5:
        brk
        jsr     L524F
        eor     ($E8,x)
        .byte   $DA
        ora     $191D
        ora     $15
        ora     ($01),y
        ora     #$52
        .byte   $4F
        jmp     LC1C5

        rol     $263E
        rol     $2A,x
        .byte   $53
        .byte   $42
        .byte   $43
        inx
        .byte   $DA
        sbc     $F9FD
        sbc     POINC1_F4+1
        sbc     ($E1),y
        sbc     #$53
        .byte   $54
        eor     ($E7,x)
        cld
        sta     $999D
        sta     $95
        sta     ($81),y
        .byte   $53
        .byte   $54
        cli
        .byte   $83
        ldy     #$8E
        stx     $96
        .byte   $53
        .byte   $54
        eor     $C083,y
        sty     $9484
        .byte   $FF
LA308:
        .byte   $4C
LA309:
        .byte   $44
LA30A:
        .byte   $41
LA30B:
        .byte   $FF
        lsr     a
        .byte   $53
        .byte   $52
        .byte   $FF
        .byte   $53
        .byte   $54
        eor     ($FF,x)
        lsr     a
        eor     $FF50
        .byte   $43
        eor     $FF50
        .byte   $42
        lsr     $D045
        .byte   $42
        eor     $51
        beq     LA377
        .byte   $54
        .byte   $53
        rts

        .byte   $53
        .byte   $54
        cli
        .byte   $FF
        eor     #$4E
        cli
        inx
        eor     #$4E
        eor     $54C8,y
        eor     ($58,x)
        tax
        .byte   $54
        eor     ($59,x)
        tay
        .byte   $54
        .byte   $53
        cli
        tsx
        .byte   $54
        cli
        eor     ($8A,x)
        .byte   $54
        cli
        .byte   $53
        txs
        .byte   $54
        eor     $9841,y
        .byte   $43
        jmp     L1843

        .byte   $43
        jmp     LD844

        .byte   $43
        jmp     L5849

        .byte   $43
        jmp     LB856

        .byte   $53
        eor     $43
        sec
        .byte   $53
        eor     $44
        sed
        .byte   $53
        eor     $49
        sei
        lsr     $504F
        nop
        .byte   $52
        .byte   $54
        eor     #$40
        .byte   $44
        eor     $58
        dex
        .byte   $44
        eor     $59
LA377:
        dey
        bvc     LA3C2
        eor     ($48,x)
        bvc     LA3C6
        bvc     LA388
        bvc     LA3CE
        eor     ($68,x)
        bvc     LA3D2
        bvc     LA3B0
LA388:
        .byte   $42
        .byte   $52
        .byte   $4B
        brk
        .byte   $42
        .byte   $43
        .byte   $43
        bcc     LA3D3
        .byte   $43
        .byte   $53
        bcs     LA3D7
        eor     $3049
        .byte   $42
        bvc     LA3E7
        bpl     LA3DF
        lsr     $43,x
        bvc     LA3E3
        lsr     $53,x
        bvs     LA3F7
        .byte   $4F
        .byte   $52
        .byte   $FF
        eor     ($44,x)
        .byte   $43
        .byte   $FF
        eor     ($4E,x)
        .byte   $44
        .byte   $FF
LA3B0:
        eor     ($53,x)
        jmp     L42FF

        eor     #$54
        .byte   $FF
        .byte   $43
        bvc     LA413
        .byte   $FF
        .byte   $43
        bvc     LA418
        .byte   $FF
        .byte   $44
        .byte   $45
LA3C2:
        .byte   $43
        .byte   $FF
        eor     $4F
LA3C6:
        .byte   $52
        .byte   $FF
        eor     #$4E
        .byte   $43
        .byte   $FF
        .byte   $4C
        .byte   $44
LA3CE:
        cli
        .byte   $FF
        .byte   $4C
        .byte   $44
LA3D2:
        .byte   $59
LA3D3:
        .byte   $FF
        jmp     L5253

LA3D7:
        .byte   $FF
        .byte   $4F
        .byte   $52
        eor     ($FF,x)
        .byte   $52
        .byte   $4F
        .byte   $4C
LA3DF:
        .byte   $FF
        .byte   $53
        .byte   $42
        .byte   $43
LA3E3:
        .byte   $FF
        .byte   $53
        .byte   $54
        .byte   $59
LA3E7:
        .byte   $FF
        .byte   $53
        lsr     $43,x
        .byte   $FE
        .byte   $FF
LA3ED:
        ldx     #$00
        lda     #$00
        sta     _ZERO_,x
        inx
        cpx     #$F0
        .byte   $90
LA3F7:
        sbc     $A2,y
LA3FA:
        sta     $0800,x
        sta     $0900,x
        sta     $0A00,x
        sta     $0B00,x
        sta     $0C00,x
        sta     $0D00,x
        sta     $0E00,x
        sta     $0F00,x
        inx
LA413:
        bne     LA3FA
        lda     #$FD
        .byte   $8D
LA418:
        brk
        php
        lda     #$88
        sta     $08FD
        lda     #$8F
        sta     $0985
        lda     #$4D
        sta     L0A14
        sta     $0A83
        lda     #$21
        sta     $0A62
        brk
        .byte   $82
        sbc     $0E,x
        lda     $0F06
        sta     $57
        lda     $0EFE
        ldy     $0EFF
        ldx     $0F00
        sta     $0EF2
        sty     $0EF3
        stx     $0EF4
        ldx     #$09
        lda     #$20
LA450:
        sta     $79,x
        dex
        bpl     LA450
        brk
        ldy     #$FD
        php
        brk
        .byte   $27
        brk
        rol     $20
        jsr     L6520
        .byte   $73
        rol     $35,x
        and     $5341
        .byte   $53
        eor     $4D
        .byte   $42
        jsr     L4556
        .byte   $52
        .byte   $53
        rol     $2E42
        rol     $2E,x
        and     $20,x
        .byte   $4F
        lsr     a:$A0
        and     #$00
        rol     $20
        eor     ($54,x)
        ldy     #$00
        plp
        jsr     LB393
        ldx     #$2B
LA489:
        lda     #$20
        sta     $0CC6,x
        dex
        bpl     LA489
        brk
        ldy     #$62
        asl     a
        brk
        .byte   $27
        brk
        rol     $26
        jsr     L2620
        cli
        ldy     _ZERO_
        lda     $C6
        .byte   $0C
        brk
        rol     $A4
        lda     #$2C
        brk
        .byte   $23
        brk
        rol     $20
        rol     $44
        jsr     L2620
        .byte   $54
        jsr     L5020
        eor     ($47,x)
        eor     $20
        rol     $D0
LA4BC:
        brk
        ldy     #$00
        php
        brk
        .byte   $27
        brk
        .byte   $1B
        bvc     LA50E
LA4C6:
        brk
        ora     ($70),y
        .byte   $3B
        sta     _ZERO_
        brk
        and     ($50,x)
        inc     $A2,x
        rti

        ldy     #$00
LA4D4:
        jsr     LB448
        brk
        .byte   $C3
        dex
        .byte   $0B
        lda     #$00
        sta     $0BBA
        sta     $0B3C
        brk
        .byte   $C3
        dec     a:$0A
        .byte   $C3
        jmp     L000B

        .byte   $DB
        dec     a:$0A
        .byte   $DB
        jmp     L000B

        .byte   $D3
        pha
        .byte   $0C
        lda     $8B
        ldy     $8C
        beq     LA501
        ldx     #$14
        brk
        .byte   $31
LA501:
        lda     #$06
        brk
        ora     ($A5,x)
        brk
        cmp     #$2D
        bne     LA529
        dec     $0802
LA50E:
        ldx     #$44
        brk
        .byte   $89
        .byte   $B3
        ldy     #$50
        .byte   $02
        brk
        ora     (_ZERO_,x)
        bcc     LA52F
        asl     a
        lda     #$00
        brk
        .byte   $1C
        brk
        ora     a:$A9,x
        sta     _ZERO_
        jmp     LA4C6

LA529:
        brk
        bcc     LA52C
LA52C:
        php
        lda     #$00
LA52F:
        sta     $09
        sta     $0A
        brk
        .byte   $1C
        brk
        ora     $7DA2,x
        lda     #$00
LA53B:
        sta     $0BCA,x
        sta     $0ACE,x
        sta     $0B4C,x
        dex
        bpl     LA53B
        lda     #$41
        sta     $0BCA
        ldx     #$01
        brk
        tya
        dex
        .byte   $0B
        bvc     LA557
LA554:
        jmp     LA631

LA557:
        lda     $0C0A
        cmp     #$03
        bne     LA554
        lda     $0801
        sta     $01
        brk
        ora     ($50),y
        .byte   $03
        jmp     LA63D

        cmp     #$2C
        beq     LA571
        jmp     LA631

LA571:
        brk
        stx     $78,y
        ldy     #$70
        .byte   $C0
LA577:
        ldx     #$FF
        cmp     #$03
        bcc     LA5A7
        beq     LA58D
        cmp     #$04
        beq     LA59C
        tay
        lda     LA0AA,y
        tay
        stx     _ZERO_,y
        jmp     LA61E

LA58D:
        stx     $3C
        lda     #$41
        ldx     #$03
        brk
        .byte   $1F
        bvs     LA554
        sta     $8D
        jmp     LA61E

LA59C:
        brk
        .byte   $14
        bvs     LA554
        sta     $09
        sty     $0A
        jmp     LA61E

LA5A7:
        asl     a
        sta     _ZERO_
        ldx     _ZERO_
        lda     LA09E,x
        sta     $03
        lda     LA09D,x
        sta     L0002
        ldy     #$7D
        lda     #$00
LA5BA:
        sta     (L0002),y
        dey
        bpl     LA5BA
        ldy     #$00
        txa
        asl     a
        tax
LA5C4:
        lda     LA0A3,x
        sta     (L0002),y
        iny
        inx
        cpy     #$04
        bcc     LA5C4
        lda     _ZERO_
        cmp     #$04
        beq     LA60F
        lda     L0002
        ldy     $03
        ldx     #$02
        brk
        clc
        bvs     LA631
        lda     _ZERO_
        bne     LA61E
        lda     $79
        cmp     #$20
        bne     LA603
        ldx     #$02
        ldy     #$00
LA5ED:
        lda     $0B4C,x
        beq     LA603
        cmp     #$3B
        beq     LA603
        cmp     #$2E
        beq     LA603
        sta     $79,y
        inx
        iny
        cpy     #$0A
        bcc     LA5ED
LA603:
        lda     $0B8C
        beq     LA61E
        cmp     #$03
        beq     LA61E
        jmp     LA631

LA60F:
        ldx     #$01
        brk
        tya
        pha
        .byte   $0C
        bvs     LA631
        lda     $0C88
        cmp     #$03
        bne     LA631
LA61E:
        brk
        .byte   $1B
        bvc     LA638
        brk
        ora     ($C9),y
        bit     $09D0
        brk
        stx     $78,y
        ldy     #$70
        .byte   $03
        jmp     LA577

LA631:
        ldx     #$41
        ldy     #$00
        jmp     LA4D4

LA638:
        lda     $01
        sta     $0802
LA63D:
        lda     #$FD
        ldy     #$08
        sta     $0B0F
        sty     $0B10
        lda     #$02
        sta     $0AFC
        lda     #$85
        ldy     #$09
        sta     $0B8D
        sty     $0B8E
        lda     #$20
        sta     $0B7A
        ldx     #$02
        brk
        .byte   $C2
        dec     $500A
        asl     $AA
        lda     #$CE
        jmp     LA4D4

        lda     #$62
        ldy     #$0A
        sta     $0B31
        sty     $0B32
        ldx     #$02
        brk
        .byte   $C2
        jmp     L500B

        asl     $AA
        lda     #$4C
        jmp     LA4D4

        lda     $3C
        bne     LA688
        jmp     LA71A

LA688:
        ldx     #$00
        txa
LA68B:
        sta     $0EF5,x
        inx
        cpx     #$FF
        bne     LA68B
        ldx     #$14
        brk
        bmi     LA6E8
        asl     $AA
        ldy     #$00
        jmp     LA4D4

        sta     $8B
        sty     $8C
        sta     $0F82
        sty     $0F83
        sta     $0FA0
        sty     $0FA1
        sta     $0FA2
        clc
        cld
        tya
        adc     #$0A
        sta     $0FA3
        lda     #$DF
        .byte   $8D
        .byte   $7F
LA6BE:
        .byte   $0F
        lda     #$00
        ldy     #$A0
        sta     $0F7C
        sty     $0F7D
        lda     #$00
        ldy     #$08
        sta     $0F79
        sty     $0F7A
        lda     #$00
        sta     $0F76
        lda     #$FF
        sta     $0F75
        sta     $0F78
        sta     $0F7B
        sta     $0F7E
        .byte   $8D
        .byte   $81
LA6E8:
        .byte   $0F
        sta     $0F9F
        lda     #$FF
        sta     $0F20
        sta     $0F21
        lda     #$03
        sta     $0F35
        lda     $8D
        sta     $0F3D
        ldx     #$1F
LA700:
        lda     LA04F,x
        sta     $0EF5,x
        dex
        bpl     LA700
        lda     #$20
        sta     $0F62
        brk
        sty     $0EF5
        bvc     LA71A
        tax
        lda     #$F5
        jmp     LA4D4

LA71A:
        brk
        ldy     #$FD
        php
        lda     $4B
        bne     LA72B
        lda     $3C
        beq     LA72B
        ldy     #$12
        jsr     LBF26
LA72B:
        jsr     LB2CB
        ldx     #$00
        stx     $5A
        stx     $6E
        stx     $6F
        ldx     #$01
LA738:
        lda     #$00
        sta     $0EF5,x
        lda     #$FF
        sta     $0F00,x
        inx
        cpx     #$08
        bcc     LA738
        lda     #$04
        sta     $0EF5
        lda     #$0F
        sta     $0F00
        lda     #$0D
        sta     $0EFF
        lda     #$0F
        sta     $0F0A
        lda     $09
        sta     $0EFD
        lda     $0A
        sta     $0EFE
        lda     #$02
        ldy     #$00
        sta     $07
        sty     $08
        ldy     $57
        dey
        lda     #$BF
        sta     $15
        sty     $16
        lda     #$0B
        ldy     #$0F
        sta     $13
        sty     $14
        sta     $8E
        sty     $8F
        sta     $94
        sty     $95
        sta     $90
        sty     $91
        sta     $92
        sty     $93
        sta     $99
        sty     $9A
        sta     $9D
        sty     $9E
        sta     $9F
        sty     $A0
        lda     $0C48
        beq     LA7A4
        ldy     #$10
        jsr     LBF26
LA7A4:
        ldx     #$00
        cld
LA7A7:
        ldx     #$00
        stx     $6B
        stx     $6C
        stx     $41
        stx     $42
        stx     $43
        stx     $45
        stx     $A6
        lda     $5A
        beq     LA7C8
        lda     #$03
        sta     $0987
        jsr     LBE69
        lda     #$43
        jsr     LB578
LA7C8:
        ldx     #$00
        stx     $73
        stx     $74
        stx     $75
        ldx     #$0F
LA7D2:
        lda     #$00
        sta     $0D52,x
        sta     $0D62,x
        sta     $0D32,x
        sta     $0D42,x
        sta     $0D72,x
        dex
        bpl     LA7D2
        lda     #$FF
        sta     $0D80
        sta     $0D81
        lda     #$0F
        sta     $78
        lda     #$00
        ldx     #$03
LA7F6:
        sta     $5D,x
        sta     $61,x
        sta     $65,x
        dex
        bpl     LA7F6
        lda     #$FF
        sta     $46
        lda     #$40
        sta     $44
        ldx     #$00
        brk
        .byte   $27
        brk
        rol     $20
        jsr     L5320
        .byte   $54
        eor     ($52,x)
        .byte   $54
        jsr     L464F
        jsr     L4150
        .byte   $53
        .byte   $53
        ldy     #$A6
        .byte   $5A
        inx
        txa
        brk
        and     $9320
        .byte   $B3
LA827:
        ldx     #$FF
        txs
        lda     $5A
        beq     LA831
        jsr     LB586
LA831:
        lda     $42
        sta     $50
        lda     $46
        sta     $4E
        lda     $45
        sta     $4F
        jsr     LB314
        lda     $3C
        beq     LA84D
        lda     $41
        bne     LA84D
        ldy     #$0C
        jsr     LBF26
LA84D:
        ldx     #$00
        stx     $B7
        stx     $47
        stx     $48
        stx     $40
        stx     $70
        stx     $49
        stx     $3B
        stx     $4D
        lda     $43
        beq     LA8A8
        ldx     #$05
LA865:
        lda     L0A14,x
        inx
        cmp     #$20
        beq     LA865
        cmp     #$2E
        bne     LA883
        lda     L0A14,x
        cmp     #$43
        bne     LA886
        lda     $0A15,x
        cmp     #$45
        bne     LA886
        lda     #$00
        sta     $43
LA883:
        jmp     LA827

LA886:
        lda     L0A14,x
        cmp     #$45
        bne     LA897
        lda     $0A15,x
        cmp     #$4E
        bne     LA897
        jmp     LAF8D

LA897:
        lda     L0A14,x
        cmp     #$4D
        bne     LA883
        lda     $0A15,x
        cmp     #$58
        bne     LA883
        jmp     LB2C0

LA8A8:
        brk
        ldy     #$FD
        php
        brk
        .byte   $27
        lda     $73
        sta     $76
        lda     $74
        sta     $77
        lda     #$20
        ldx     #$0F
        brk
        bit     $5BA5
        sta     $5C
        brk
        .byte   $23
        lda     #$3A
        ldy     $42
        beq     LA8CA
        lda     #$2D
LA8CA:
        brk
        and     ($AD,x)
        .byte   $17
        asl     a
        sta     $58
        brk
        .byte   $23
        lda     $0A18
        sta     $59
        brk
        .byte   $23
        brk
        .byte   $2B
        brk
        ora     (_ZERO_),y
        ora     ($20),y
        .byte   $D3
        tsx
        jsr     LBB57
        bvc     LA8EB
        jmp     LAAAC

LA8EB:
        lda     $41
        beq     LA8F7
        ldy     #$06
        jsr     LBF26
        jmp     LA8FA

LA8F7:
        jsr     LBB13
LA8FA:
        lda     $48
        beq     LA935
        lda     $3A
        cmp     #$0D
        beq     LA916
        and     #$30
        cmp     #$30
        beq     LA916
        .byte   $A5
LA90B:
        rol     $40C9
        bne     LA916
        jsr     LB47E
        jmp     LA935

LA916:
        lda     $5A
        beq     LA930
        lda     $3A
        and     #$30
        cmp     #$30
        beq     LA928
        lda     $2E
        cmp     #$40
        beq     LA930
LA928:
        lda     #$2E
        jsr     LBCD2
        jmp     LA935

LA930:
        lda     #$2E
        jsr     LBCF5
LA935:
        lda     $0901
        cmp     #$20
        bne     LA958
        lda     $40
        bne     LA944
        lda     $4E
        beq     LA95B
LA944:
        lda     $5A
        beq     LA95B
        lda     $50
        beq     LA950
        lda     $4F
        beq     LA95B
LA950:
        lda     $4D
        beq     LA958
        lda     $4B
        bne     LA95B
LA958:
        jsr     LB3E0
LA95B:
        lda     $40
        lda     $40
        bne     LA964
        jmp     LA827

LA964:
        lda     $5A
        bne     LA983
        jsr     LB84A
        inc     $5A
        brk
        .byte   $C3
        dex
        .byte   $0B
        bvc     LA97D
        cmp     #$E1
        beq     LA97D
        tax
        lda     #$CA
        jmp     LA4D4

LA97D:
        jsr     LB2CB
        jmp     LA7A7

LA983:
        lda     $3D
        beq     LA98A
        jsr     LB869
LA98A:
        lda     #$22
        jsr     LB598
        jsr     LB41F
        jsr     LB967
        jsr     LB981
        jsr     LBEF0
        brk
        .byte   $27
        brk
        rol     $20
        jsr     L4520
        .byte   $52
        .byte   $52
        .byte   $4F
        .byte   $52
        .byte   $53
        jsr     L2020
        jsr     L2020
        ldy     #$A5
        ror     $6FA4
        brk
        and     $20
        .byte   $93
        .byte   $B3
        brk
        .byte   $27
        brk
        rol     $20
        jsr     L4720
        jmp     L424F

        eor     ($4C,x)
        .byte   $53
        plp
        cli
        bit     $2959
        ldy     #$A5
        .byte   $83
        ldy     $84
        brk
        and     $A9
        bit     $2100
        lda     $85
        ldy     $86
        brk
        and     $20
        .byte   $93
        .byte   $B3
        brk
        .byte   $27
        brk
        rol     $20
        jsr     L5020
        .byte   $43
        plp
        .byte   $5A
        bit     $2C44
        .byte   $43
        cli
        and     #$20
        jsr     LA2A0
        brk
        jsr     LBF17
        ldx     #$01
        jsr     LBF17
        ldx     #$0A
LA9FF:
        jsr     LBF17
        inx
        cpx     #$0D
        bcc     LA9FF
        dec     $08FF
        jsr     LB393
        brk
        .byte   $27
        brk
        rol     $20
        jsr     L5020
        .byte   $43
        plp
        bvc     LAA45
        .byte   $53
        cli
        bit     $2941
        jsr     LA020
        ldx     #$02
LAA23:
        jsr     LBF17
        inx
        cpx     #$0A
        bcc     LAA23
        ldx     #$0F
        jsr     LBF17
        dec     $08FF
        jsr     LB393
        ldx     $0BBA
        lda     $6E
        ora     $6F
        beq     LAA41
        ldx     #$00
LAA41:
        stx     $0BBA
        brk
LAA45:
        .byte   $C3
        jmp     L500B

        asl     $AA
        lda     #$4C
        jmp     LA4D4

        brk
        cpy     #$CE
        asl     a
        bvc     LAA5A
        cmp     #$E6
        bne     LAA60
LAA5A:
        brk
        .byte   $C3
        dec     $500A
        .byte   $06
LAA60:
        tax
        lda     #$CE
        jmp     LA4D4

        lda     $0C48
        beq     LAA77
        brk
        .byte   $D3
        pha
        .byte   $0C
        bvc     LAA77
        tax
        lda     #$48
        jmp     LA4D4

LAA77:
        brk
        .byte   $C3
        dex
        .byte   $0B
        bvc     LAA87
        cmp     #$E1
        beq     LAA87
        tax
        lda     #$CA
        jmp     LA4D4

LAA87:
        lda     $8B
        ldy     $8C
        beq     LAA9A
        ldx     #$14
        brk
        and     ($50),y
        .byte   $07
        ldy     #$00
        sty     $8C
        jmp     LA4D4

LAA9A:
        lda     $6E
        ora     $6F
        beq     LAAA8
        brk
        .byte   $DB
        jmp     LA90B

        asl     _ZERO_
        .byte   $01
LAAA8:
        lda     #$01
        brk
        .byte   $01
LAAAC:
        cmp     #$2E
        bne     LAABC
        lda     #$20
        ldx     #$0B
        brk
        bit     $4120
        .byte   $BF
        jmp     LAB48

LAABC:
        lda     $43
        beq     LAAC3
        jmp     LA95B

LAAC3:
        lda     $41
        beq     LAACF
        ldy     #$06
        jsr     LBF26
        jmp     LA8FA

LAACF:
        jsr     LBB51
        ldx     #$0A
        brk
        .byte   $97
        .byte   $1C
        brk
        jsr     LBAF9
        stx     $96
        cpx     #$03
        bne     LAAF8
        jsr     LBA6C
        bvs     LAAF8
        sta     $54
        lda     #$00
        sta     $96
LAAEC:
        lda     #$20
        ldx     #$0B
        brk
        bit     $4120
        .byte   $BF
        jmp     LAC50

LAAF8:
        ldx     $0A15
LAAFB:
        cpx     $0A16
        beq     LAAEC
        lda     L0A14,x
        cmp     #$3B
        beq     LAAEC
        cmp     #$28
        beq     LAAEC
        cmp     #$20
        bne     LAB13
        inx
        jmp     LAAFB

LAB13:
        ldx     #$0A
        brk
        lda     $1C,x
        brk
        brk
        .byte   $2B
        jsr     LBF41
        jsr     LBAC3
        ldx     #$09
LAB23:
        lda     $1C,x
        sta     $2E,x
        dex
        bpl     LAB23
        lda     $76
        sta     $38
        lda     $77
        sta     $39
        lda     $78
        sta     $3A
        inc     $48
        jsr     LBB51
        jsr     LBAD3
        jsr     LBAF9
        cmp     #$2E
        beq     LAB48
        jmp     LAC30

LAB48:
        brk
        ora     (_ZERO_),y
        and     ($A5,x)
        eor     (POIN_F0,x)
        .byte   $1A
        brk
        ora     $9600,x
        .byte   $6F
        ldy     #$70
        asl     a
        cmp     #$00
        bne     LAB5F
        jmp     LB2BC

LAB5F:
        jmp     LAF8D

        ldy     #$06
        jsr     LBF26
        jmp     LA8FA

        brk
        asl     $5120,x
        .byte   $BB
        ldx     #$0A
        brk
        .byte   $97
        .byte   $1C
        brk
        jsr     LBAF4
        cpx     #$02
        beq     LAB82
        bit     LA048
        jsr     LBAF4
LAB82:
        lda     $1C
        brk
LAB85:
        and     ($A5,x)
        ora     $2100,x
        brk
        .byte   $2B
        jsr     LBB51
        jsr     LBAC3
        ldx     #$00
LAB94:
        lda     LA1C2,x
        bmi     LABE8
        cmp     $1C
        bne     LABA4
        lda     LA1C3,x
        cmp     $1D
        beq     LABA8
LABA4:
        inx
        inx
        bne     LAB94
LABA8:
        lda     LA1F1,x
        sta     L0002
        lda     LA1F2,x
        sta     $03
        lda     $43
        beq     LABBD
        cpx     #$04
        bcc     LABBD
LABBA:
        jmp     LA95B

LABBD:
        jsr     LBB1F
        bvc     LABCE
        cpx     #$28
        bcc     LABCE
        lda     #$53
        jsr     LB47E
        jmp     LAC90

LABCE:
        jsr     LBAD3
        pha
        jsr     LBB51
        pla
        cmp     #$3B
        beq     LABE1
        cmp     #$0D
        beq     LABE1
        jsr     LBAFE
LABE1:
        ldx     #$00
        ldy     #$FF
        jmp     (L0002)

LABE8:
        lda     $43
        bne     LABBA
        lda     $3C
        bne     LABF8
        lda     #$50
        jsr     LB47E
        jmp     LAC90

LABF8:
        ldy     #$00
        jsr     LBF26
        jmp     LAC90

LAC00:
        brk
        lda     $1C,x
        brk
        brk
        .byte   $2B
        jsr     LBAC3
        jsr     LBAD3
        pha
        jsr     LBB51
        pla
        cmp     #$3B
        beq     LAC1C
        cmp     #$0D
        beq     LAC1C
        jsr     LBAFE
LAC1C:
        lda     $3C
        bne     LAC28
        lda     #$4D
        jsr     LB47E
        jmp     LAC90

LAC28:
        ldy     #$08
        jsr     LBF26
        jmp     LAC90

LAC30:
        jsr     LBB51
        ldx     #$0A
        brk
        .byte   $97
        .byte   $1C
        brk
        jsr     LBAEF
        bit     LA048
        stx     $96
        cpx     #$03
        bne     LAC50
        jsr     LBA6C
        bvs     LAC50
        sta     $54
        lda     #$00
        sta     $96
LAC50:
        ldx     $96
        bne     LAC00
        ldx     #$03
        brk
        lda     $1C,x
        brk
        brk
        .byte   $2B
        jsr     LBAC3
        jsr     LBAD3
        pha
        jsr     LBB51
        pla
        cmp     #$0D
        beq     LAC72
        cmp     #$3B
        beq     LAC72
        jsr     LBAFE
LAC72:
        jsr     LBB1F
        jsr     LBAEA
        lda     $54
        cmp     #$FE
        beq     LACC1
        cmp     #$FF
        bne     LAC85
        jmp     LAD33

LAC85:
        and     #$1F
        cmp     #$10
        beq     LACE0
        lda     $54
        jsr     LB52D
LAC90:
        ldy     $71
LAC92:
        cpy     $0A15
        bcs     LAC9F
        lda     L0A14,y
        brk
        and     ($C8,x)
        bne     LAC92
LAC9F:
        lda     $0901
        cmp     #$20
        bne     LACA8
        brk
        .byte   $2B
LACA8:
        jsr     LBAD3
        cmp     #$3B
        beq     LACB5
LACAF:
        jsr     LBB13
        jmp     LA8FA

LACB5:
        lda     $08FF
        cmp     #$38
        bcs     LACAF
        brk
        .byte   $2B
        jmp     LACB5

LACC1:
        lda     #$00
        jsr     LB52D
        lda     $5A
        beq     LACDA
        jsr     LB5BC
        jsr     LBB69
        jsr     LBAFE
        lda     #$F3
        jsr     LB598
        lda     $1A
LACDA:
        jsr     LB52D
        jmp     LAC90

LACE0:
        lda     $54
        jsr     LB52D
        lda     $5A
        beq     LAD2D
        jsr     LB5BC
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     $78
        beq     LACFE
        lda     #$52
        jsr     LB47E
        jmp     LAD1B

LACFE:
        lda     $19
        bne     LAD1B
        clc
        cld
        lda     $1A
        sbc     $73
        sta     _ZERO_
        lda     $1B
        sbc     $74
        bne     LAD23
        lda     _ZERO_
        cmp     #$80
        bcc     LAD2D
LAD16:
        lda     #$42
        jsr     LB47E
LAD1B:
        lda     #$FE
        jsr     LB52D
        jmp     LAC90

LAD23:
        cmp     #$FF
        bne     LAD16
        lda     _ZERO_
        cmp     #$80
        bcc     LAD16
LAD2D:
        jsr     LB52D
        jmp     LAC90

LAD33:
        ldx     #$00
        cld
        ldx     #$00
LAD38:
        lda     LA21F,x
        bpl     LAD43
        bit     LA048
        jsr     LBAEF
LAD43:
        cmp     $1C
        bne     LAD55
        lda     LA220,x
        cmp     $1D
        bne     LAD55
        lda     LA221,x
        cmp     $1E
        beq     LAD65
LAD55:
        clc
        lda     LA222,x
        and     #$0F
        sta     _ZERO_
        txa
        adc     _ZERO_
        adc     #$05
        tax
        bne     LAD38
LAD65:
        inx
        inx
        inx
        stx     $54
        jsr     LBF4F
        cmp     #$23
        beq     LAD74
        jmp     LADF7

LAD74:
        inc     $0A15
        lda     #$00
        sta     $01
        jsr     LBF4F
        cmp     #$3C
        beq     LADCA
        cmp     #$3E
        beq     LADDB
        cmp     #$27
        beq     LAD92
        cmp     #$22
        bne     LADAD
        lda     #$80
        sta     $01
LAD92:
        brk
        ora     ($A2),y
        .byte   $0B
        jsr     LB4EA
        brk
        ora     ($20),y
        inc     $05BA,x
        ora     ($48,x)
        lda     #$F3
        jsr     LB598
        pla
        jsr     LB52D
        jmp     LAC90

LADAD:
        ldx     #$00
        jsr     LB5BC
        ldx     #$0B
        jsr     LB4EA
        jsr     LBB69
        jsr     LBAFE
        lda     #$F3
        jsr     LB598
        lda     $1A
        jsr     LB52D
        jmp     LAC90

LADCA:
        inc     $0A15
        jsr     LB5B8
        ldx     #$0B
        jsr     LB4EA
        jsr     LB9F3
        jmp     LAC90

LADDB:
        inc     $0A15
        jsr     LB5B8
        ldx     #$0B
        jsr     LB4EA
        jsr     LBA1B
        jmp     LAC90

LADEC:
        inc     $0A15
        ldx     #$0C
        jsr     LB4EA
        jmp     LAC90

LADF7:
        and     #$DF
        cmp     #$41
        bne     LAE08
        ldy     $0A15
        lda     $0A15,y
        jsr     LBB57
        bvc     LADEC
LAE08:
        jsr     LBF4F
        cmp     #$3A
        bne     LAE32
        inc     $0A15
        jsr     LB5CE
        jsr     LBB57
        bvs     LAE1D
        jmp     LAEC4

LAE1D:
        brk
        ora     $9600,x
        pha
        lda     ($20,x)
        inc     a:$BA,x
        asl     a:$C9,x
        bne     LAE2F
        jmp     LAEC3

LAE2F:
        jmp     LAEC2

LAE32:
        cmp     #$28
        bne     LAE6C
        inc     $0A15
        jsr     LB5CE
        cmp     #$29
        bne     LAE59
        ldy     $0A15
        lda     $0A15,y
        jsr     LBB57
        bvs     LAE59
        inc     $0A15
        ldx     #$04
        jsr     LB4EA
        jsr     LB9D3
        jmp     LAC90

LAE59:
        ldx     #$00
        brk
        ora     $9600,x
        eor     ($A1),y
        jsr     LBAFE
        brk
        asl     a:$C9,x
        beq     LAEC0
        bne     LAEC1
LAE6C:
        jsr     LB5CE
        jsr     LBB57
        bvc     LAE87
        brk
        ora     $9600,x
        pha
        lda     ($20,x)
        inc     a:$BA,x
        asl     a:$C9,x
        beq     LAE86
        bne     LAE85
LAE85:
        inx
LAE86:
        inx
LAE87:
        inx
        lda     $4C
        beq     LAEB7
        lda     $3E
        bne     LAEB7
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     #$00
        beq     LAE9E
        cmp     #$0F
        bne     LAEB7
LAE9E:
        lda     $1B
        bne     LAEB7
        stx     $A7
        inx
        inx
        inx
        inx
        jsr     LB4F5
        ldx     $A7
        bvs     LAEB7
        lda     #$5A
        sta     $0902
        jmp     LAECC

LAEB7:
        jsr     LB4EA
        jsr     LB9D3
        jmp     LAC90

LAEC0:
        inx
LAEC1:
        inx
LAEC2:
        inx
LAEC3:
        inx
LAEC4:
        inx
        inx
        inx
        inx
        inx
        jsr     LB4EA
LAECC:
        lda     $53
        jsr     LB598
        lda     $53
        and     #$01
        beq     LAEE1
        lda     $51
        jsr     LB598
        lda     $52
        jsr     LB598
LAEE1:
        lda     $1A
        jsr     LB52D
        lda     $1B
        beq     LAEF3
        lda     $5A
        beq     LAEF3
        lda     #$5A
        jsr     LB47E
LAEF3:
        jmp     LAC90

        jsr     LB5B8
        lda     #$44
        jsr     LB598
        jsr     LB9D3
        jmp     LAC90

LAF04:
        lda     #$00
        sta     $49
        jsr     LB5D2
        jsr     LBB69
        jsr     LBAFE
        lda     $1A
        jsr     LAF77
LAF16:
        lda     #$2C
        brk
        .byte   $12
        bvc     LAF25
        jsr     LBB57
        jsr     LBAFE
        jmp     LAC90

LAF25:
        jsr     LBF4F
        cmp     #$3C
        beq     LAF5F
        cmp     #$3E
        beq     LAF6B
        cmp     #$27
        beq     LAF40
        cmp     #$2E
        beq     LAF40
        cmp     #$2D
        beq     LAF40
        cmp     #$22
        bne     LAF04
LAF40:
        ldx     #$40
        brk
        sta     $0CF2,y
LAF46:
        jsr     LBAFE
        bit     LA048
        cpx     #$00
        beq     LAF46
        ldy     #$00
LAF52:
        lda     $0CF2,y
        jsr     LAF77
        iny
        dex
        bne     LAF52
        jmp     LAF16

LAF5F:
        inc     $0A15
        jsr     LB5CE
        jsr     LB9F3
        jmp     LAF16

LAF6B:
        inc     $0A15
        jsr     LB5CE
        jsr     LBA1B
        jmp     LAF16

LAF77:
        pha
        stx     $A8
        sty     $A9
        lda     #$F3
        jsr     LB598
        pla
        jsr     LB52D
        jsr     LB586
        ldx     $A8
        ldy     $A9
        rts

LAF8D:
        jsr     LBB3F
        lda     $42
        ora     $43
        ora     $41
        ora     $69
        ora     $6A
        bne     LAF9F
        jmp     LAFA4

LAF9F:
        lda     #$D4
        jsr     LB47E
LAFA4:
        inc     $40
        ldx     $78
        lda     $73
        sta     $0D52,x
        lda     $74
        sta     $0D62,x
        jsr     LBF4F
        jsr     LBB57
        bvc     LAFE7
        jsr     LB5BC
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        jsr     LBB21
        bvc     LAFCD
        lda     #$53
        jsr     LB47E
LAFCD:
        lda     $0901
        cmp     #$20
        bne     LAFE7
        lda     #$FF
        sta     $8A
        lda     $53
        sta     $89
        lda     $1A
        sta     $87
        ldy     $1B
        sty     $88
        jsr     LB5A4
LAFE7:
        jmp     LAC90

        jsr     LB5BC
        lda     $19
        beq     LAFF6
        lda     #$D5
        jsr     LB47E
LAFF6:
        lda     $48
        bne     LB002
        lda     #$4C
        jsr     LB47E
        jmp     LAC90

LB002:
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        sta     $3A
        lda     $1A
        sta     $38
        ldy     $1B
        sty     $39
        jsr     LB5A4
        jmp     LAC90

        lda     $46
        beq     LB027
        lda     $5A
        beq     LB027
        lda     $73
        ldy     $74
        jsr     LB5A4
LB027:
        jsr     LB5BC
        lda     $19
        beq     LB033
        lda     #$D5
        jsr     LB47E
LB033:
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     #$0F
        beq     LB045
        lda     #$52
        jsr     LB47E
        jmp     LAC90

LB045:
        jsr     LB4BD
        cld
        clc
        lda     $1A
        adc     $73
        sta     $73
        lda     $1B
        adc     $74
        sta     $74
        lda     #$00
        adc     $75
        sta     $75
        jsr     LBA4D
        jmp     LAC90

        jsr     LB5D2
        jsr     LBB69
        jsr     LBAFE
        lda     $1A
        sta     $AA
        lda     #$2C
        brk
        .byte   $12
        jsr     LBAFE
        jsr     LB5BC
        lda     $19
        beq     LB082
        lda     #$D5
        jsr     LB47E
LB082:
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     $78
        beq     LB094
        lda     #$52
        jsr     LB47E
        jmp     LAC90

LB094:
        cld
        sec
        lda     $1A
        sbc     $73
        lda     $1B
        sbc     $74
        bcs     LB0A8
        lda     #$47
        jsr     LB47E
        jmp     LAC90

LB0A8:
        lda     $1A
        cmp     $73
        bne     LB0B7
        lda     $1B
        cmp     $74
        bne     LB0B7
        jmp     LAC90

LB0B7:
        lda     #$F3
        jsr     LB598
        lda     $AA
        jsr     LB52D
        jsr     LB586
        jmp     LB0A8

        jsr     LBB3F
        lda     #$1C
        ldx     #$00
        jsr     LBB77
        bvs     LB0E5
        jsr     LBF4F
        jsr     LBB57
        jsr     LBAFE
        ldx     #$09
LB0DE:
        lda     $1C,x
        sta     $79,x
        dex
        bpl     LB0DE
LB0E5:
        jmp     LAC90

        jsr     LBB3F
        lda     $5A
        beq     LB106
        lda     $46
        beq     LB106
        lda     #$41
        sta     $0900
        lda     $4B
        beq     LB106
        inc     $4D
        brk
        .byte   $27
        brk
        rol     $C1
        jsr     LB412
LB106:
        jmp     LAC90

        jsr     LBB3F
        inc     $4D
        lda     $5A
        beq     LB12C
        ldx     #$2B
        lda     #$20
LB116:
        sta     $0CC6,x
        dex
        bpl     LB116
        ldx     #$00
LB11E:
        brk
        ora     ($70),y
        asl     a
        and     #$7F
        sta     $0CC6,x
        inx
        cpx     #$2C
        bcc     LB11E
LB12C:
        jmp     LAC90

        jsr     LBB3F
        sty     $46
        inc     $4D
        jmp     LAC90

        jsr     LBB3F
        stx     $46
        inc     $4D
        jmp     LAC90

        jsr     LBB3F
        stx     $44
        inc     $4D
        jmp     LAC90

        jsr     LBB3F
        lda     #$40
        sta     $44
        inc     $4D
        jmp     LAC90

        jsr     LBB3F
        ldx     $78
        lda     $73
        sta     $0D52,x
        lda     $74
        sta     $0D62,x
        brk
        ora     $9600,x
        lda     $20A0,x
        inc     a:$BA,x
        asl     LAB85,x
        jsr     LBF4F
        jsr     LBB57
        bvs     LB180
        jmp     LB1FD

LB180:
        cmp     #$2C
        bne     LB18F
        brk
        ora     ($A6),y
        .byte   $AB
        lda     $0D72,x
        bmi     LB197
        beq     LB197
LB18F:
        lda     #$4F
        jsr     LB47E
        jmp     LAC90

LB197:
        ldx     #$00
        jsr     LB5BC
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     #$0F
        beq     LB1AE
        lda     #$52
        jsr     LB47E
        jmp     LAC90

LB1AE:
        lda     $19
        beq     LB1B7
        lda     #$D5
        jsr     LB47E
LB1B7:
        lda     $AB
        cmp     #$00
        bne     LB1C8
        lda     $1B
        beq     LB1C8
        ldx     #$A8
        ldy     #$00
        jsr     LA4D4
LB1C8:
        lda     $1A
        sta     $73
        ldy     $1B
        sty     $74
        jsr     LB5A4
        lda     #$00
        sta     $75
        ldx     $AB
        stx     $78
        lda     $0D72,x
        and     #$01
        bne     LB1EF
        txa
        asl     a
        tay
        lda     $1A
        sta     $0D32,y
        lda     $1B
        sta     $0D33,y
LB1EF:
        lda     $0D72,x
        ora     #$01
        sta     $0D72,x
        jsr     LBA4D
        jmp     LAC90

LB1FD:
        ldx     $AB
        cpx     #$0E
        beq     LB210
        lda     $0D52,x
        sta     $1A
        lda     $0D62,x
        sta     $1B
        jmp     LB1B7

LB210:
        lda     #$00
        sta     $1A
        sta     $1B
        jmp     LB1B7

        jsr     LBB3F
        inc     $4D
        ldx     #$00
        lda     $43
        bne     LB227
        jmp     LAC90

LB227:
        stx     $43
        jmp     LA95B

        jsr     LB260
        lda     $1A
        ora     $1B
        bne     LB237
        stx     $43
LB237:
        jmp     LAC90

        jsr     LB260
        lda     $1A
        ora     $1B
        beq     LB245
        stx     $43
LB245:
        jmp     LAC90

        jsr     LB260
        lda     $1B
        bmi     LB251
        stx     $43
LB251:
        jmp     LAC90

        jsr     LB260
        lda     $1B
        bpl     LB25D
        stx     $43
LB25D:
        jmp     LAC90

LB260:
        sty     $43
        jsr     LBB3F
        inc     $4D
        lda     #$40
        sta     $47
        jsr     LB5BC
        lda     $1A
        ldy     $1B
        jsr     LB5A4
        ldx     #$00
        rts

        jsr     LBB3F
        lda     #$40
        sta     $47
        lda     #$2E
        ldx     #$40
        jsr     LBB77
        bvs     LB293
        lda     $2F
        cmp     #$20
        bne     LB296
        lda     #$4F
        jsr     LB47E
LB293:
        jmp     LAC90

LB296:
        lda     #$3D
        brk
        .byte   $12
        jsr     LBAFE
        jsr     LB5BC
        lda     $1A
        sta     $38
        ldy     $1B
        sty     $39
        jsr     LB5A4
        lda     #$0D
        sta     $3A
        inc     $48
        jmp     LAC90

        ldy     #$02
LB2B6:
        jsr     LBF26
        jmp     LAC90

LB2BC:
        ldy     #$04
        bne     LB2B6
LB2C0:
        lda     $43
        beq     LB2BC
        ldx     #$00
        stx     $43
        jmp     LA95B

LB2CB:
        brk
        bcc     LB2CE
LB2CE:
        php
        lda     #$00
        brk
        .byte   $1C
        brk
        ora     $01A9,x
        sta     $5B
        jsr     LB2DD
        rts

LB2DD:
        jsr     LB304
        ldx     #$01
        brk
        tya
        dex
        .byte   $0B
        lda     #$01
        sta     $0C37
        lda     #$14
        ldy     #$0A
        sta     $0C0B
        sty     $0C0C
        ldx     #$01
        brk
        .byte   $C2
        dex
        .byte   $0B
        bvs     LB2FE
        rts

LB2FE:
        tax
        lda     #$CA
        jmp     LA4D4

LB304:
        ldx     #$7D
        lda     #$00
LB308:
        sta     $0BCA,x
        dex
        bpl     LB308
        lda     #$41
        sta     $0BCA
        rts

LB314:
        lda     $42
        beq     LB320
        ldy     #$0A
        jsr     LBF26
        jmp     LB368

LB320:
        brk
        cpy     $CA
        .byte   $0B
        bvs     LB328
        brk
        rti

LB328:
        bvc     LB32D
        jmp     LB2FE

LB32D:
        lda     $0A16
        cmp     #$03
        bne     LB368
        brk
        .byte   $C3
        dex
        .byte   $0B
        bvc     LB33D
        jmp     LB2FE

LB33D:
        brk
        bcc     LB340
LB340:
        php
        brk
        ora     $1100,x
        bvs     LB356
        sed
        clc
        lda     $5B
        adc     #$01
        sta     $5B
        cld
        jsr     LB2DD
        jmp     LB314

LB356:
        ldx     #$03
LB358:
        lda     LA046,x
        cmp     #$A0
        beq     LB365
        sta     L0A14,x
        inx
        bne     LB358
LB365:
        stx     $0A16
LB368:
        brk
        bcc     LB37F
        asl     a
        lda     #$40
        brk
        .byte   $1C
        lda     $0A16
        cmp     #$05
        bne     LB37F
        inc     $0A16
        lda     #$20
        sta     $0A19
LB37F:
        ldy     $0A16
        lda     #$0D
        sta     L0A14,y
        brk
        .byte   $07
        bvc     LB392
        ldx     #$42
        ldy     #$00
        jmp     LA4D4

LB392:
        rts

LB393:
        sta     $04
        sty     $05
        stx     $06
        lda     #$06
        sta     $AC
LB39D:
        brk
        cpy     #$CE
        asl     a
        bvc     LB3AD
        cmp     #$E6
        beq     LB3AD
        tax
        lda     #$CE
        jmp     LA4D4

LB3AD:
        ldx     #$45
        brk
        txa
        .byte   $B7
        ldy     #$50
        asl     $AA
        ldy     #$00
        jmp     LA4D4

LB3BB:
        lda     $AC
        sta     $08FF
        ldx     #$20
        stx     $0900
        stx     $0901
        stx     $0902
        cmp     #$06
        beq     LB3D9
        ldx     #$16
        lda     #$20
LB3D3:
        sta     $0900,x
        dex
        bpl     LB3D3
LB3D9:
        lda     $04
        ldx     $06
        ldy     $05
        rts

LB3E0:
        sta     $04
        sty     $05
        stx     $06
        lda     $08FF
        sta     $AC
LB3EB:
        brk
        cpy     #$CE
        asl     a
        bvc     LB3FB
        cmp     #$E6
        beq     LB3FB
LB3F5:
        tax
        lda     #$CE
        jmp     LA4D4

LB3FB:
        brk
        cmp     $CE
        asl     a
        bvs     LB3F5
        lda     $0901
        cmp     #$20
        beq     LB3BB
        lda     $0B0E
        cmp     #$01
        beq     LB3BB
        jmp     LB39D

LB412:
        sta     $04
        sty     $05
        stx     $06
        lda     #$06
        sta     $AC
        jmp     LB3EB

LB41F:
        sta     $04
        sty     $05
        stx     $06
        lda     $0987
        cmp     #$03
        beq     LB43C
        brk
        cmp     $4C
        .byte   $0B
        bvs     LB434
        brk
        rti

LB434:
        bvc     LB43C
        tax
        lda     #$4C
        jmp     LA4D4

LB43C:
        lda     #$03
        sta     $0987
        lda     $04
        ldy     $05
        ldx     $06
        rts

LB448:
        sta     $AD
        sty     $AE
        brk
        ldy     #$FD
        php
        brk
        .byte   $27
        brk
        .byte   $2B
        brk
        tax
        .byte   $5C
        lda     ($A5,x)
        ldx     $1FF0
        ldy     #$40
        lda     ($AD),y
        cmp     #$03
        bne     LB47B
        brk
        .byte   $2B
        brk
        .byte   $2B
        ldy     #$00
LB46A:
        lda     ($AD),y
        cmp     #$20
        beq     LB47B
        cmp     #$3B
        beq     LB47B
        brk
        and     ($C8,x)
        cpy     #$20
        bcc     LB46A
LB47B:
        jmp     LB393

LB47E:
        sta     $AF
        ldx     #$00
        lda     $0901
        cmp     #$20
        beq     LB490
        cmp     $AF
        beq     LB4A6
        jsr     LB3E0
LB490:
        sed
        clc
        lda     $6E
        adc     #$01
        sta     $6E
        lda     $6F
        adc     #$00
        sta     $6F
        cld
        lda     $AF
        and     #$7F
        sta     $0901
LB4A6:
        lda     $AF
        bmi     LB4AB
        rts

LB4AB:
        lda     $71
        sta     $0A15
        jsr     LBB13
        jsr     LB3E0
        ldx     #$43
        ldy     #$00
        jmp     LA4D4

LB4BD:
        lda     $78
        cmp     #$00
        bne     LB4D0
        lda     $74
        ora     $75
        beq     LB4DB
        ldx     #$A8
        ldy     #$00
        jmp     LA4D4

LB4D0:
        lda     $75
        beq     LB4DB
        ldx     #$A9
        ldy     #$00
        jmp     LA4D4

LB4DB:
        rts

LB4DC:
        jsr     LB4BD
        inc     $73
        bne     LB4E5
        inc     $74
LB4E5:
        bne     LB4E9
        inc     $75
LB4E9:
        rts

LB4EA:
        jsr     LB4F5
        bvc     LB4F4
        lda     #$41
        jsr     LB47E
LB4F4:
        rts

LB4F5:
        stx     $B1
        ldx     $54
        lda     LA220,x
        sta     $B2
        lda     #$04
        sta     $B0
        lda     LA21F,x
LB505:
        dec     $B0
        bmi     LB519
        asl     a
        bcc     LB50D
        inx
LB50D:
        dec     $B1
        bne     LB505
        bcs     LB525
LB513:
        ldx     #$00
        bit     LA048
        rts

LB519:
        lda     $B2
LB51B:
        asl     a
        bcc     LB51F
        inx
LB51F:
        dec     $B1
        bne     LB51B
        bcc     LB513
LB525:
        lda     LA220,x
        jsr     LB52D
        clv
        rts

LB52D:
        sta     $B3
        lda     $5A
        bne     LB537
        jsr     LB4DC
        rts

LB537:
        lda     $46
        beq     LB56F
        lda     $42
        beq     LB543
        lda     $45
        beq     LB56F
LB543:
        lda     $70
        bne     LB54E
        lda     $73
        ldy     $74
        jsr     LB5A4
LB54E:
        lda     $70
        cmp     #$03
        beq     LB56F
        inc     $70
        lda     $08FF
        sta     $08FE
        cld
        lda     $70
        asl     a
        adc     #$09
        sta     $08FF
        lda     $B3
        brk
        .byte   $23
        lda     $08FE
        sta     $08FF
LB56F:
        lda     $B3
        jsr     LB578
        jsr     LB4DC
        rts

LB578:
        sty     $05
        ldy     $0987
        sta     $0985,y
        inc     $0987
        ldy     $05
        rts

LB586:
        pha
        lda     $0987
        cmp     #$7C
        bcc     LB596
        jsr     LB41F
        lda     #$43
        jsr     LB578
LB596:
        pla
        rts

LB598:
        pha
        lda     $5A
        bne     LB59F
        pla
        rts

LB59F:
        pla
        jsr     LB578
        rts

LB5A4:
        ldx     $08FF
        stx     $08FE
        ldx     #$06
        stx     $08FF
        brk
        and     $AE
        inc     $8E08,x
        .byte   $FF
        php
        rts

LB5B8:
        lda     #$FF
        sta     $49
LB5BC:
        jsr     LB5D2
        jsr     LBB57
        bvs     LB5C5
        rts

LB5C5:
        lda     #$4F
        jsr     LB47E
        jsr     LBF4F
        rts

LB5CE:
        lda     #$FF
        sta     $49
LB5D2:
        ldx     #$00
        stx     $BC
        stx     $1A
        stx     $1B
        stx     $19
        stx     $55
        stx     $56
        stx     $3E
        stx     $26
        lda     #$0F
        asl     a
        asl     a
        asl     a
        asl     a
        sta     $53
        jmp     LB648

LB5EF:
        inc     $56
        lda     $3E
        ora     $BD
        sta     $3E
        jsr     LBF4F
        cmp     #$2B
        beq     LB645
        cmp     #$2D
        beq     LB66F
        ldx     $47
        beq     LB60D
        cmp     #$26
        bne     LB60D
        jmp     LB699

LB60D:
        lda     $BC
        bne     LB61A
        lda     $53
        ora     #$F0
        sta     $53
        jmp     LB631

LB61A:
        cmp     #$01
        bne     LB624
        lda     $53
        and     #$01
        beq     LB631
LB624:
        lda     $19
        bne     LB631
        lda     #$45
        jsr     LB47E
        lda     #$F0
        sta     $53
LB631:
        lda     #$00
        sta     $49
        sta     $3B
        lda     $56
        cmp     #$01
        beq     LB641
        lda     #$00
        sta     $26
LB641:
        jsr     LBF4F
        rts

LB645:
        inc     $0A15
LB648:
        jsr     LB70C
        bcs     LB631
        bvc     LB651
        inc     $19
LB651:
        clc
        cld
        lda     $1A
        adc     $B5
        sta     $1A
        lda     $1B
        adc     $B6
        sta     $1B
        lda     $49
        jsr     LB6B0
        cld
        clc
        lda     $BC
        adc     $B7
        sta     $BC
        jmp     LB5EF

LB66F:
        inc     $0A15
        jsr     LB70C
        bcs     LB631
        bvc     LB67B
        inc     $19
LB67B:
        sec
        cld
        lda     $1A
        sbc     $B5
        sta     $1A
        lda     $1B
        sbc     $B6
        sta     $1B
        lda     #$00
        jsr     LB6B0
        cld
        sec
        lda     $BC
        sbc     $B7
        sta     $BC
        jmp     LB5EF

LB699:
        inc     $0A15
        jsr     LB70C
        bcs     LB631
        lda     $1A
        and     $B5
        sta     $1A
        lda     $1B
        and     $B6
        sta     $1B
        jmp     LB5EF

LB6B0:
        cmp     #$00
        bne     LB6C2
        lda     $B8
        beq     LB6DB
        lda     #$58
        jsr     LB47E
        lda     #$00
        sta     $B7
        rts

LB6C2:
        lda     $B8
        beq     LB6DB
        lda     #$00
        sta     $49
        sta     $B7
        lda     $B9
        sta     $51
        lda     $BA
        sta     $52
        lda     $53
        ora     #$01
        sta     $53
        rts

LB6DB:
        lda     #$00
        sta     $B7
        lda     $BB
        cmp     #$0D
        bcs     LB70B
        inc     $B7
        lda     $53
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     #$0F
        beq     LB6FB
        cmp     $BB
        beq     LB70B
        lda     #$52
        jsr     LB47E
        rts

LB6FB:
        lda     $BB
        asl     a
        asl     a
        asl     a
        asl     a
        sta     $B4
        lda     $53
        and     #$0F
        ora     $B4
        sta     $53
LB70B:
        rts

LB70C:
        ldx     #$00
        stx     $B8
        stx     $B5
        stx     $B6
        stx     $BD
        lda     #$0F
        sta     $BB
        brk
        .byte   $14
        bvs     LB725
        sta     $B5
        sty     $B6
        clv
        clc
        rts

LB725:
        lda     #$2A
        brk
        .byte   $12
        bvs     LB73A
        lda     $76
        sta     $B5
        lda     $77
        sta     $B6
        lda     $78
        sta     $BB
        clv
        clc
        rts

LB73A:
        ldx     #$0A
        brk
        .byte   $97
        .byte   $1C
        brk
        bvc     LB75F
        ldx     #$01
        brk
        sta     $B5,y
        bvc     LB754
LB74A:
        lda     #$4F
        jsr     LB47E
        bit     LA048
        sec
        rts

LB754:
        cpx     #$00
        beq     LB74A
        lda     #$00
        sta     $B6
        clv
        clc
        rts

LB75F:
        lda     #$1C
        jsr     LBBB0
        ldx     #$07
LB766:
        lda     L000B,x
        sta     $26,x
        dex
        bpl     LB766
        lda     $42
        beq     LB77B
        ldy     #$0E
        jsr     LBF26
        bvs     LB77B
        jmp     LB7FF

LB77B:
        jsr     LBC17
        bvc     LB79A
LB780:
        lda     $1C
        cmp     #$40
        beq     LB793
        lda     $5A
        beq     LB793
        lda     $3B
        bne     LB793
        lda     #$55
        jsr     LB47E
LB793:
        bit     LA048
        inc     $BD
        clc
        rts

LB79A:
        ldy     #$0A
        lda     ($09),y
        and     #$0F
        cmp     #$0D
        bcs     LB7AF
        lda     $47
        beq     LB7AF
        lda     #$40
        jsr     LB47E
        clc
        rts

LB7AF:
        lda     ($09),y
        and     #$30
        cmp     #$20
        bne     LB7D3
        lda     #$FF
        sta     $B8
        ldy     #$08
        lda     ($09),y
        sta     $B9
        iny
        lda     ($09),y
        sta     $BA
        ldy     #$0A
        lda     ($09),y
        and     #$0F
        beq     LB7DE
        inc     $BD
        jmp     LB7DE

LB7D3:
        ldy     #$08
        lda     ($09),y
        sta     $B5
        iny
        lda     ($09),y
        sta     $B6
LB7DE:
        lda     $5A
        beq     LB7EC
        ldy     #$00
        lda     ($09),y
        and     #$40
        bne     LB7EC
        inc     $BD
LB7EC:
        ldy     #$0A
        lda     ($09),y
        and     #$0F
        sta     $BB
        ldy     #$0A
        lda     ($09),y
        ora     #$80
        sta     ($09),y
        clv
        clc
        rts

LB7FF:
        sta     $BE
        sty     $BF
        ldy     #$0A
        lda     ($BE),y
        and     #$80
        beq     LB810
        inc     $55
        jmp     LB780

LB810:
        lda     ($BE),y
        and     #$20
        beq     LB825
        lda     #$FF
        sta     $B8
        ldy     #$0B
        lda     ($BE),y
        sta     $B9
        iny
        lda     ($BE),y
        sta     $BA
LB825:
        ldy     #$08
        lda     ($BE),y
        sta     $B5
        iny
        lda     ($BE),y
        sta     $B6
        ldy     #$0A
        lda     ($BE),y
        and     #$0F
        sta     $BB
        lda     ($BE),y
        and     #$40
        beq     LB841
        jmp     LB780

LB841:
        lda     ($BE),y
        and     #$10
        sta     $BD
        clc
        clv
        rts

LB84A:
        jsr     LB93B
        jmp     LB863

LB850:
        ldy     #$0A
        lda     ($C2),y
        and     #$0F
LB856:
        cmp     #$0D
        bne     LB863
        ldy     #$08
        lda     #$00
        sta     ($C2),y
        iny
        sta     ($C2),y
LB863:
        jsr     LB94C
        bne     LB850
        rts

LB869:
        brk
        .byte   $27
        lda     #$41
        brk
        and     ($20,x)
        .byte   $12
        ldy     _ZERO_,x
        .byte   $27
        brk
        rol     $20
        jsr     L5320
        eor     $424D,y
        .byte   $4F
        jmp     L5420

        eor     ($42,x)
        jmp     L2045

        jsr     L4F46
        .byte   $52
        jsr     LA2A0
        brk
LB88E:
        lda     $79,x
        brk
        and     ($E8,x)
        cpx     #$0A
        bcc     LB88E
        jsr     LB412
        brk
        .byte   $27
        brk
        .byte   $2B
        jsr     LB412
        jsr     LB93B
LB8A4:
        ldy     #$0A
        lda     ($C2),y
        and     #$40
        bne     LB8AF
        jmp     LB932

LB8AF:
        ldy     #$07
        ldx     #$07
LB8B3:
        lda     ($C2),y
        sta     L000B,x
        dey
        dex
        bpl     LB8B3
        lda     #$1C
        ldy     #$00
        jsr     LBBED
        brk
        .byte   $27
        brk
        .byte   $2B
        ldx     #$20
        ldy     #$0A
        lda     ($C2),y
        and     #$80
        bne     LB8D2
        ldx     #$2A
LB8D2:
        txa
        brk
        and     (_ZERO_,x)
        .byte   $2B
        ldx     #$0A
        brk
        lda     $1C,x
        brk
        brk
        .byte   $2B
        brk
        .byte   $2B
        ldy     #$0A
        lda     ($C2),y
        and     #$30
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     LA118,x
        brk
        and     ($B1,x)
        .byte   $C2
        and     #$20
        beq     LB900
        lda     #$20
        ldx     #$0A
        brk
        bit     $244C
        .byte   $B9
LB900:
        brk
        .byte   $2B
        lda     ($C2),y
        and     #$0F
        asl     a
        tax
        lda     LA0F8,x
        brk
        and     ($BD,x)
        sbc     $A0,y
        and     (_ZERO_,x)
        .byte   $2B
        brk
        .byte   $2B
        brk
        .byte   $2B
        ldy     #$08
        lda     ($C2),y
        tax
        iny
        lda     ($C2),y
        tay
        txa
        brk
        and     $20
        .byte   $12
        ldy     _ZERO_,x
        .byte   $07
        bvc     LB932
        ldx     #$42
        ldy     #$00
        jmp     LA4D4

LB932:
        jsr     LB94C
        beq     LB93A
        jmp     LB8A4

LB93A:
        rts

LB93B:
        lda     $07
        ldy     $08
        sta     $C0
        sty     $C1
        lda     #$F5
        ldy     #$0E
        sta     $C2
        sty     $C3
        rts

LB94C:
        clc
        cld
        lda     $C2
        adc     #$0B
        sta     $C2
        lda     $C3
        adc     #$00
        sta     $C3
        lda     $C0
        bne     LB960
        dec     $C1
LB960:
        dec     $C0
        lda     $C0
        ora     $C1
        rts

LB967:
        jsr     LB93B
LB96A:
        ldy     #$0A
        lda     ($C2),y
        and     #$30
        beq     LB97B
        cmp     #$30
        beq     LB97B
        lda     #$45
        jsr     LB9A2
LB97B:
        jsr     LB94C
        bne     LB96A
        rts

LB981:
        lda     $3F
        bne     LB986
        rts

LB986:
        jsr     LB93B
LB989:
        ldy     #$0A
        lda     ($C2),y
        and     #$30
        bne     LB99C
        lda     ($C2),y
        and     #$40
        beq     LB99C
        lda     #$49
        jsr     LB9A2
LB99C:
        jsr     LB94C
        bne     LB989
        rts

LB9A2:
        jsr     LB578
        ldy     #$00
        lda     ($C2),y
        and     #$0F
        jsr     LB578
        iny
LB9AF:
        lda     ($C2),y
        jsr     LB578
        iny
        cpy     #$08
        bcc     LB9AF
        ldy     #$08
        lda     ($C2),y
LB9BD:
        jsr     LB578
        iny
        lda     ($C2),y
        jsr     LB578
        ldy     #$0A
        lda     ($C2),y
        and     #$3F
        jsr     LB578
        jsr     LB41F
        rts

LB9D3:
        lda     $53
        jsr     LB598
        lda     $53
        and     #$01
        beq     LB9E8
        lda     $51
        jsr     LB598
        lda     $52
        jsr     LB598
LB9E8:
        lda     $1A
        jsr     LB52D
        lda     $1B
        jsr     LB52D
        rts

LB9F3:
        lda     $53
        and     #$01
        bne     LBA06
        lda     $53
        ora     #$03
        jsr     LB598
        lda     $1A
        jsr     LB52D
        rts

LBA06:
        lda     #$54
        jsr     LB598
        lda     $51
        jsr     LB598
        lda     $52
        jsr     LB598
        lda     $1A
        jsr     LB52D
        rts

LBA1B:
        lda     $53
        and     #$01
        bne     LBA33
        lda     $53
        ora     #$07
        jsr     LB598
        lda     $1A
        jsr     LB598
        lda     $1B
        jsr     LB52D
        rts

LBA33:
        lda     #$64
        jsr     LB598
        lda     $51
        jsr     LB598
        lda     $52
        jsr     LB598
        lda     $1A
        jsr     LB598
        lda     $1B
        jsr     LB52D
        rts

LBA4D:
        lda     $5A
        beq     LBA6B
        jsr     LBB1F
        bvs     LBA6B
        lda     $78
        asl     a
        asl     a
        asl     a
        asl     a
        ora     #$0F
        jsr     LB598
        lda     $73
        jsr     LB598
        lda     $74
        jsr     LB598
LBA6B:
        rts

LBA6C:
        lda     $1C
        jsr     LBAAF
        bvs     LBAAB
        sta     $C4
        lda     $1D
        jsr     LBAAF
        bvs     LBAAB
        sta     $C5
        lda     $1E
        jsr     LBAAF
        bvs     LBAAB
        sta     $C6
        ldx     #$00
LBA89:
        lda     LA308,x
        bmi     LBAAB
        cmp     $C4
        bne     LBAA0
        lda     LA309,x
        cmp     $C5
        bne     LBAA0
        lda     LA30A,x
        cmp     $C6
        beq     LBAA6
LBAA0:
        inx
        inx
        inx
        inx
        bne     LBA89
LBAA6:
        lda     LA30B,x
        clv
        rts

LBAAB:
        bit     LA048
        rts

LBAAF:
        cmp     #$61
        bcc     LBAB5
        and     #$DF
LBAB5:
        cmp     #$41
        bcc     LBABF
        cmp     #$5F
        bcs     LBABF
        clv
        rts

LBABF:
        bit     LA048
        rts

LBAC3:
        ldx     #$09
LBAC5:
        lda     $1C,x
        cmp     #$61
        bcc     LBACD
        and     #$DF
LBACD:
        sta     $1C,x
        dex
        bpl     LBAC5
        rts

LBAD3:
        jsr     LBF4F
        cmp     #$20
        beq     LBADE
        bit     LA048
        rts

LBADE:
        inc     $0A15
        jsr     LBF4F
        cmp     #$20
        beq     LBADE
        clv
        rts

LBAEA:
        pha
        lda     #$53
        bne     LBB01
LBAEF:
        pha
        lda     #$4D
        bne     LBB01
LBAF4:
        pha
        lda     #$50
        bne     LBB01
LBAF9:
        pha
        lda     #$46
        bne     LBB01
LBAFE:
        pha
        lda     #$4F
LBB01:
        bvs     LBB06
        pla
        clv
        rts

LBB06:
        tax
        pla
        pla
        pla
        txa
        ldx     #$00
        jsr     LB47E
        jmp     LAC90

LBB13:
        brk
        asl     $1100,x
        bvs     LBB1D
        brk
        and     ($50,x)
        sed
LBB1D:
        clv
        rts

LBB1F:
        lda     $78
LBB21:
        cmp     #$0E
        beq     LBB3B
        cmp     #$00
        beq     LBB3B
        cmp     #$01
        beq     LBB3B
        cmp     #$0A
        beq     LBB3B
        cmp     #$0B
        beq     LBB3B
        cmp     #$0C
        beq     LBB3B
        clv
        rts

LBB3B:
        bit     LA048
        rts

LBB3F:
        lda     $48
        beq     LBB4C
        lda     #$4C
        jsr     LB47E
        lda     #$00
        sta     $48
LBB4C:
        ldx     #$00
        ldy     #$FF
        rts

LBB51:
        lda     $0A15
        sta     $71
        rts

LBB57:
        cmp     #$20
        beq     LBB67
        cmp     #$3B
        beq     LBB67
        cmp     #$0D
        beq     LBB67
        bit     LA048
        rts

LBB67:
        clv
        rts

LBB69:
        lda     $1B
        bne     LBB73
        lda     $BC
        bne     LBB73
        clv
        rts

LBB73:
        bit     LA048
        rts

LBB77:
        ldy     #$00
        stx     $C9
        ldx     #$0A
        brk
        .byte   $17
        bvc     LBB8A
        lda     #$4F
        jsr     LB47E
        bit     LA048
        rts

LBB8A:
        sta     $C7
        sty     $C8
        ldy     #$00
        lda     $C9
        bmi     LBB9C
        bne     LBBA1
        lda     ($C7),y
        cmp     #$40
        beq     LBBA7
LBB9C:
        jsr     LBAC3
        clv
        rts

LBBA1:
        lda     ($C7),y
        cmp     #$40
        beq     LBB9C
LBBA7:
        lda     #$40
        jsr     LB47E
        bit     LA048
        rts

LBBB0:
        sta     $CA
        ldy     #$00
        sty     $CB
        lda     #$00
        sta     $12
        ldy     #$00
LBBBC:
        lda     ($CA),y
        cmp     #$61
        bcc     LBBC4
        and     #$DF
LBBC4:
        ldx     #$00
LBBC6:
        cmp     LA11C,x
        beq     LBBCE
        inx
        bne     LBBC6
LBBCE:
        txa
        asl     a
        asl     a
        ldx     #$05
LBBD3:
        asl     a
        rol     $12
        rol     $11
        rol     $10
        rol     $0F
        rol     $0E
        rol     $0D
        rol     $0C
        rol     L000B
        dex
        bpl     LBBD3
        iny
        cpy     #$0A
        bcc     LBBBC
        rts

LBBED:
        sta     $CA
        ldy     #$00
        sty     $CB
        ldy     #$09
LBBF5:
        ldx     #$05
LBBF7:
        lsr     L000B
        ror     $0C
        ror     $0D
        ror     $0E
        ror     $0F
        ror     $10
        ror     $11
        ror     $12
        ror     a
        dex
        bpl     LBBF7
        lsr     a
        lsr     a
        tax
        lda     LA11C,x
        sta     ($CA),y
        dey
        bpl     LBBF5
        rts

LBC17:
        lda     #$00
        sta     $CC
        sta     $CD
        lda     $07
        ldy     $08
        sta     $CE
        sty     $CF
LBC25:
        sec
        cld
        lda     $CE
        sbc     $CC
        sta     $D0
        lda     $CF
        sbc     $CD
        lsr     a
        ror     $D0
        sta     $D1
        clc
        lda     $D0
        adc     $CC
        sta     $D0
        lda     $D1
        adc     $CD
        sta     $D1
        jsr     LBCB6
        ldy     #$07
        ldx     #$07
        sec
        cld
LBC4C:
        lda     ($09),y
        sbc     L000B,x
        dey
        dex
        bne     LBC4C
        lda     ($09),y
        and     #$0F
        sbc     L000B,x
        bcs     LBC6D
        lda     $D0
        ldy     $D1
        sta     $CC
        sty     $CD
        inc     $CC
        bne     LBC6A
        inc     $CD
LBC6A:
        jmp     LBC95

LBC6D:
        ldy     #$07
        ldx     #$07
LBC71:
        lda     ($09),y
        cmp     L000B,x
        bne     LBC85
        dey
        dex
        bne     LBC71
        lda     ($09),y
        and     #$0F
        cmp     L000B,x
        bne     LBC85
        clv
        rts

LBC85:
        lda     $D0
        ldy     $D1
        sta     $CE
        sty     $CF
        lda     $CE
        bne     LBC93
        dec     $CF
LBC93:
        dec     $CE
LBC95:
        sec
        cld
        lda     $CE
        sbc     $CC
        lda     $CF
        sbc     $CD
        bcs     LBC25
        lda     $CE
        ldy     $CF
        sta     $D0
        sty     $D1
        inc     $D0
        bne     LBCAF
        inc     $D1
LBCAF:
        jsr     LBCB6
        bit     LA048
        rts

LBCB6:
        lda     #$F5
        ldy     #$0E
        sta     $09
        sty     $0A
        ldx     #$0A
        cld
LBCC1:
        clc
        lda     $09
        adc     $D0
        sta     $09
        lda     $0A
        adc     $D1
        sta     $0A
        dex
        bpl     LBCC1
        rts

LBCD2:
        jsr     LBBB0
        jsr     LBC17
        ldy     #$00
        lda     ($09),y
        and     #$80
        beq     LBCE5
        lda     #$44
        jsr     LB47E
LBCE5:
        lda     ($09),y
        ora     #$40
        sta     ($09),y
        rts

LBCEC:
        sta     $D2
        ldy     #$00
        sty     $D3
        jmp     LBD19

LBCF5:
        sta     $D2
        ldy     #$00
        sty     $D3
        jsr     LBBB0
        jsr     LBC17
        bvs     LBD19
        ldy     #$0C
        lda     ($D2),y
        and     #$0F
        cmp     #$0D
        beq     LBD3F
        ldy     #$00
        lda     ($09),y
        ora     #$80
        sta     ($09),y
        bit     LA048
        rts

LBD19:
        lda     $09
        ldy     $0A
        sta     $17
        sty     $18
        lda     #$0B
        ldy     #$00
        jsr     LBD65
        inc     $07
        bne     LBD2E
        inc     $08
LBD2E:
        lda     #$0B
        jsr     LBDE9
        ldx     #$07
        ldy     #$07
LBD37:
        lda     L000B,x
        sta     ($09),y
        dey
        dex
        bpl     LBD37
LBD3F:
        ldy     #$0A
        lda     ($D2),y
        tax
        iny
        lda     ($D2),y
        ldy     #$09
        sta     ($09),y
        dey
        txa
        sta     ($09),y
        ldy     #$0C
        lda     ($D2),y
        ora     $44
        tax
        and     #$0F
        cmp     #$0D
        bne     LBD5E
        ldx     #$0D
LBD5E:
        txa
        ldy     #$0A
        sta     ($09),y
        clv
        rts

LBD65:
        sta     $D4
        sty     $D5
        cld
        clc
        adc     $13
        sta     $D8
        tya
        adc     $14
        sta     $D9
        sec
        lda     $15
        sbc     $D8
        lda     $16
        sbc     $D9
        bcs     LBD8D
        brk
        .byte   $32
        bvc     LBD89
        tax
        ldy     #$00
        jmp     LA4D4

LBD89:
        tay
        dey
        sty     $16
LBD8D:
        lda     $13
        ldy     $14
        sta     $D6
        sty     $D7
        lda     $14
        cmp     $18
        bne     LBDA1
        lda     $13
        cmp     $17
        beq     LBDDA
LBDA1:
        lda     $D6
        cmp     $17
        beq     LBDC0
        lda     $D6
        bne     LBDAD
        dec     $D7
LBDAD:
        dec     $D6
        lda     $D8
        bne     LBDB5
        dec     $D9
LBDB5:
        dec     $D8
        ldy     #$00
        lda     ($D6),y
        sta     ($D8),y
        jmp     LBDA1

LBDC0:
        lda     $D7
        cmp     $18
        beq     LBDDA
        dec     $D7
        dec     $D9
        ldy     #$FF
LBDCC:
        lda     ($D6),y
        sta     ($D8),y
        dey
        bne     LBDCC
        lda     ($D6),y
        sta     ($D8),y
        jmp     LBDC0

LBDDA:
        clc
        cld
        lda     $13
        adc     $D4
        sta     $13
        lda     $14
        adc     $D5
        sta     $14
        rts

LBDE9:
        ldx     #$8E
        jsr     LBE3F
        ldx     #$94
        jsr     LBE3F
        ldx     #$97
        jsr     LBE3F
        ldx     #$00
        jsr     LBE52
        ldx     #$0C
        jsr     LBE52
        ldx     #$18
        jsr     LBE52
        ldx     #$24
        jsr     LBE52
LBE0C:
        ldx     #$90
        jsr     LBE3F
        ldx     #$92
        jsr     LBE3F
        ldx     #$99
        jsr     LBE3F
        ldx     #$9B
        jsr     LBE3F
        ldx     #$9D
        jsr     LBE3F
        ldx     #$9F
        jsr     LBE3F
        ldx     #$0A
        jsr     LBE52
        ldx     #$16
        jsr     LBE52
        ldx     #$22
        jsr     LBE52
        ldx     #$2E
        jsr     LBE52
LBE3E:
        rts

LBE3F:
        sta     $DA
        clc
        cld
        lda     _ZERO_,x
        adc     $DA
        sta     _ZERO_,x
        lda     $01,x
        adc     #$00
        sta     $01,x
        lda     $DA
        rts

LBE52:
        sta     $DA
        clc
        cld
        lda     $0D82,x
        adc     $DA
        sta     $0D82,x
        lda     $0D83,x
        adc     #$00
        sta     $0D83,x
        lda     $DA
        rts

LBE69:
        lda     #$79
        jsr     LBBB0
        lda     #$48
        jsr     LB578
        ldx     #$00
LBE75:
        lda     L000B,x
        jsr     LB578
        inx
        cpx     #$08
        bcc     LBE75
        lda     #$00
        jsr     LB578
        ldx     #$00
LBE86:
        lda     $83,x
        jsr     LB578
        inx
        cpx     #$04
        bcc     LBE86
        lda     #$36
        jsr     LB578
        lda     #$41
        jsr     LB578
        lda     $6E
        jsr     LB578
        lda     $6F
        jsr     LB578
        lda     $0EF2
        jsr     LB578
        lda     $0EF3
        jsr     LB578
        lda     $0EF4
        jsr     LB578
        jsr     LB41F
        lda     #$41
        jsr     LB578
        ldx     #$00
LBEC0:
        lda     $0D32,x
        jsr     LB578
        inx
        cpx     #$1A
        bcc     LBEC0
        ldx     #$00
        ldy     #$00
LBECF:
        cld
        sec
        lda     $0D52,x
        sbc     $0D32,y
        php
        jsr     LB578
        plp
        lda     $0D62,x
        sbc     $0D33,y
        jsr     LB578
        iny
        iny
        inx
        cpx     #$0D
        bcc     LBECF
        jsr     LB41F
        rts

LBEF0:
        lda     #$53
        jsr     LB578
        lda     $8A
        jsr     LB578
        lda     $89
        jsr     LB578
        lda     $87
        jsr     LB578
        lda     $88
        jsr     LB578
        lda     $6E
        jsr     LB578
        lda     $6F
        jsr     LB578
        jsr     LB41F
        rts

LBF17:
        lda     $0D62,x
        brk
        .byte   $23
        lda     $0D52,x
        brk
        .byte   $23
        lda     #$2C
        brk
        and     ($60,x)
LBF26:
        sta     $DB
        lda     $3C
        bne     LBF33
        ldx     #$44
        ldy     #$00
        jmp     LA4D4

LBF33:
        lda     ($8B),y
        sta     L00DC
        iny
        lda     ($8B),y
        sta     $DD
        lda     $DB
        jmp     (L00DC)

LBF41:
        clc
        cld
        lda     $69
        adc     $6A
        tax
        beq     LBF4E
        lda     #$20
        brk
        .byte   $2C
LBF4E:
        rts

LBF4F:
        sty     $DE
        ldy     $0A15
        lda     L0A14,y
        ldy     $DE
        bit     LA048
        rts

        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $BF
