; da65 V2.18 - N/A
; Created:    2021-11-25 19:22:33
; Input file: E.FORMAT.bin
; Page:       1


        .setcpu "6502"

ATRACK          := $0000                        ; 0x00
ATRACKp1        := $0001                        ; 0x01
ATRKE           := $0002                        ; 0x02
ATRKEp1         := $0003                        ; 0x03
ATRKG1          := $0004                        ; 0x04
ATRKG1p1        := $0005                        ; 0x05
ATRKT#          := $0006                        ; 0x06
ATRKT#p1        := $0007                        ; 0x07
TEMPY           := $0008                        ; 0x08
TEMPX           := $0009                        ; 0x09
L0F62           := $0F62
L4556           := $4556
L7365           := $7365
; 0x800
TMBER:
        .byte   $00
L0801:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
L0809:
        .byte   $00,$00,$00,$00,$00,$00,$00
L0810:
        .byte   $00,$00,$00,$00,$00,$00
L0816:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00
; 0x828
BUFIN:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00
; 0x849
BUFOUT:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00
; 0x89B
SECTOR1:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
; 0x99B
FCB:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
L09E3:
        .byte   $00,$00
L09E5:
        .byte   $00
L09E6:
        .byte   $00
L09E7:
        .byte   $00
L09E8:
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
        .byte   $00,$00,$00,$00,$00,$00,$00,$00
L0A18:
        .byte   $00
; 0xa19
SECLI:
        .byte   $01,$03,$05,$07,$09,$02,$04,$06
        .byte   $08,$0A
; 0xa23
SETV:
        .byte   $40
; 0xA24
SIC:
        .byte   $28,$08
; 0xA26
SICpSICADR:
        .byte   $00
L0A27:
        .byte   $00
; 0xa28
SOC:
        .byte   $49,$08,$00,$00,$00,$00
; 0xa2e
TEXT3:
        .byte   "#"
        .byte   $FF
        .byte   "#   FORMAT THE FLOPPY (Y/N) ..."



        .byte   ". "
; 0xA51
TEXT4:
        .byte   "#"
        .byte   $FF
        .byte   "#   DRIVE NUMBER (1-5) ........"



        .byte   ". "
; 0xa74
TEXT6:
        .byte   "#"
        .byte   $FF
        .byte   "#   FLOPPY-NAME ..............."



        .byte   ". "
; 0xA97
OUTV:
        sty     TEMPY
        brk
        ldy     #$49
        php
        brk
        .byte   $27
        brk
        .byte   $2B
        ldy     TEMPY
        rts

; 0xAA4
OUT:
        sty     TEMPY
        stx     TEMPX
        ldx     #$45
        brk
        .byte   $8A,$28,$0A
        ldy     TEMPY
        ldx     TEMPX
        rts

; 0xAB3
IN:
        sta     SICpSICADR
        sty     L0A27
        ldx     #$46
        brk
        .byte   $89
        .byte   $24,$0A
        bvc     L0AC3
        brk
        .byte   $01
L0AC3:
        brk
        bcc     L0AEE
        php
        lda     #$C0
        brk
        .byte   $1C
        brk
        .byte   $1D
        rts

; 0xACE
ERROR:
        tax
        jsr     OUTV
        brk
        tax
        brk
        brk
        jsr     OUT
        rts

        .byte   $2C,$47,$41,$50,$33,$3D,$FE,$FF
        .byte   $FF
; 0xae3
START:
        lda     #$21
        sta     BUFIN
        lda     #$52
        sta     BUFOUT
        .byte   $20
L0AEE:
        .byte   $97
        asl     a
        brk
        rol     $20
        jsr     L7365
        rol     $35,x
        and     $4F46
        .byte   $52
        eor     $5441
        jsr     L4556
        .byte   $52
        .byte   $53
        rol     $2E42
        rol     $2E,x
        and     $20,x
        .byte   $4F
        lsr     a:$A0
        and     #$00
        rol     $20
        eor     ($54,x)
        ldy     #$00
        plp
        jsr     OUT
        brk
        .byte   $82
        brk
        php
        ldx     #$00
        lda     #$E5
L0B23:
        sta     a:$0C,x
        inx
        bne     L0B23
        lda     #$08
        sta     $0E
        ldx     #$7D
        lda     #$00
L0B31:
        sta     FCB,x
        dex
        bpl     L0B31
        lda     #$D9
        sta     FCB
        lda     #$07
        sta     $19
        lda     #$0F
        sta     $1A
        lda     #$00
        sta     $0F
        brk
        .byte   $1B
        bvc     L0BBB
        lda     #$2A
        ldx     #$01
        brk
        .byte   $1F
        bvs     L0B9A
        sta     L09E3
        lda     #$2C
        brk
        .byte   $12
        bvs     L0B9A
        ldx     #$08
        brk
        .byte   $97
        .byte   $0F
        brk
        bvs     L0B9A
        brk
        .byte   $1B
        bvc     L0BBB
        lda     L0816
        beq     L0B9A
        brk
        stx     $DA,y
        asl     a
        bvs     L0B9A
        lda     #$02
        brk
        .byte   $1A
        bvs     L0B9A
        brk
        php
        cmp     #$00
        beq     L0B9A
        sta     $1A
        lda     #$2D
        brk
        .byte   $12
        bvs     L0B9A
        lda     #$02
        brk
        .byte   $1A
        bvs     L0B9A
        brk
        php
        cmp     #$00
        beq     L0B9A
        sta     $19
        brk
        .byte   $1B
        bvc     L0BBB
L0B9A:
        jsr     OUTV
        ldx     #$A1
        brk
        tax
        brk
        brk
        jsr     OUT
        lda     #$00
        sta     $0F
        lda     #$07
        sta     $19
        lda     #$0F
        sta     $1A
        lda     L0810
        beq     L0BBB
        lda     #$06
        brk
        .byte   $01
L0BBB:
        ldx     #$20
        brk
        bmi     L0C10
        .byte   $02
        brk
        ora     ($85,x)
        brk
        sty     ATRACKp1
        lda     $0F
        bne     L0C0F
L0BCB:
        lda     #$51
        ldy     #$0A
        jsr     IN
        lda     #$2A
        ldx     #$01
        brk
        .byte   $1F
        bvs     L0BCB
        brk
        .byte   $1B
        bvs     L0BCB
        sta     L09E3
L0BE1:
        lda     #$74
        ldy     #$0A
        jsr     IN
        ldx     #$08
        brk
        .byte   $97
        .byte   $0F
        brk
        bvs     L0BE1
        brk
        .byte   $1B
        bvs     L0BE1
L0BF4:
        lda     #$2E
        ldy     #$0A
        jsr     IN
        brk
        ora     ($70),y
        sbc     ATRACK,x
        .byte   $1B
        bvs     L0BF4
        cmp     #$59
        beq     L0C0F
        cmp     #$4E
        bne     L0BF4
        lda     #$01
        brk
        .byte   $01
L0C0F:
        .byte   $A9
L0C10:
        ora     ($A0,x)
        brk
        sta     L09E5
        sty     L09E6
        lda     #$9B
        ldy     #$08
        sta     L09E7
        sty     L09E8
        brk
        dec     $9B,x
        ora     #$70
        .byte   $02
        brk
        bvc     L0C9C
        asl     $1BA2,x
        ldy     #$02
        jsr     DECTEST
        bvs     L0C5E
        ldx     #$1D
        ldy     #$03
        jsr     DECTEST
        bvs     L0C5E
        ldx     #$2B
        ldy     #$02
        jsr     DECTEST
        bvs     L0C5E
        jmp     L0C68

        cmp     #$C3
        beq     L0C5E
        cmp     #$F4
        beq     L0C5E
        cmp     #$F5
        beq     L0C5E
        jsr     ERROR
        lda     #$06
        brk
        .byte   $01
L0C5E:
        ldx     #$00
        lda     #$E5
L0C62:
        sta     SECTOR1,x
        inx
        bne     L0C62
L0C68:
        jsr     FMTDSK
        jsr     FMTDSK
        jsr     TRACK0
        jsr     OUTV
        brk
        .byte   $26
        .byte   "  DISMOUNT"

        .byte   $A0
        ldy     L09E3
        iny
        tya
        brk
        and     $A420
        asl     a
        brk
        bcc     L0CD7
        php
        lda     #$C0
        brk
        .byte   $1C
        brk
        .byte   $03
        bvc     L0C9E
        jsr     ERROR
        lda     #$06
L0C9C:
        brk
        .byte   $01
L0C9E:
        jsr     OUTV
        brk
        .byte   $26
        .byte   "  FORMAT COMPLETED NORMALL"



        .byte   $D9
        jsr     OUT
        lda     #$01
        brk
        .byte   $01
; 0xcc5
DECTEST:
        lda     SECTOR1,x
        and     #$0F
        cmp     #$0A
        bcs     L0CDF
        lda     SECTOR1,x
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        cmp     #$0A
L0CD7:
        bcs     L0CDF
        inx
        dey
        bne     DECTEST
        clv
        rts

L0CDF:
        bit     SETV
        rts

; 0xCE3
FMTDSK:
        cld
        clc
        lda     ATRACK
        sta     ATRKE
        lda     ATRACKp1
        adc     #$10
        sta     ATRKEp1
        clc
        lda     ATRACK
        adc     #$14
        sta     ATRKG1
        adc     #$01
        adc     $19
        sta     ATRKT#
        lda     ATRACKp1
        sta     ATRKG1p1
        sta     ATRKT#p1
        lda     ATRACK
        sta     L09E7
        lda     ATRACKp1
        sta     L09E8
        clc
        cld
        lda     #$19
        adc     $19
        sta     $1B
        lda     #$01
        adc     #$00
        sta     $1C
        clc
        lda     $1B
        adc     $1A
        sta     $1B
        lda     $1C
        adc     #$00
        sta     $1C
        lda     ATRACK
        sta     $0A
        lda     ATRACKp1
        sta     $0B
        ldy     #$00
L0D31:
        lda     #$FF
L0D33:
        sta     ($0A),y
        iny
        bne     L0D33
        inc     $0B
        lda     $0B
        cmp     ATRKEp1
        bne     L0D31
        lda     ATRKG1
        sta     $0A
        ldy     ATRKG1p1
        sty     $0B
        lda     #$01
        sta     $0C
L0D4C:
        ldy     #$00
        ldx     #$00
L0D50:
        cpx     $19
        beq     L0D5C
        lda     #$00
        sta     ($0A),y
        iny
        inx
        bne     L0D50
L0D5C:
        lda     #$FE
        sta     ($0A),y
        iny
        lda     #$27
        sta     ($0A),y
        iny
        lda     #$00
        sta     ($0A),y
        iny
        ldx     $0C
        lda     L0A18,x
        sta     ($0A),y
        iny
        lda     #$01
        sta     ($0A),y
        iny
        lda     #$F7
        sta     ($0A),y
        iny
        clc
        cld
        tya
        adc     #$0B
        tay
        ldx     #$06
        lda     #$00
L0D87:
        sta     ($0A),y
        iny
        dex
        bne     L0D87
        lda     #$FB
        sta     ($0A),y
        iny
        clc
        cld
        tya
        adc     $0A
        sta     $0A
        lda     $0B
        adc     #$00
        sta     $0B
        ldy     #$00
        lda     #$E5
L0DA3:
        sta     ($0A),y
        iny
        bne     L0DA3
        inc     $0B
        ldy     #$00
        lda     #$F7
        sta     ($0A),y
        sec
        cld
        lda     $0A
        adc     $1A
        sta     $0A
        lda     $0B
        adc     #$00
        sta     $0B
        inc     $0C
        lda     $0C
        cmp     #$0B
        bcc     L0D4C
        lda     #$27
        sta     $0D
        lda     #$95
        sta     L09E5
        lda     #$03
        sta     L09E6
L0DD4:
        brk
        cmp     FCB,y
        bvs     L0DDC
        brk
        .byte   $50
L0DDC:
        bvc     L0DE5
        jsr     ERROR
        lda     #$06
        brk
        .byte   $01
L0DE5:
        dec     $0D
        bpl     L0DEA
        rts

L0DEA:
        sec
        sed
        lda     L09E5
        sbc     #$10
        sta     L09E5
        lda     L09E6
        sbc     #$00
        sta     L09E6
        lda     ATRKT#
        sta     $0A
        lda     ATRKT#p1
        sta     $0B
        ldx     #$0A
        ldy     #$00
L0E08:
        lda     $0D
        sta     ($0A),y
        clc
        cld
        lda     $0A
        adc     $1B
        sta     $0A
        lda     $0B
        adc     $1C
        sta     $0B
        dex
        bne     L0E08
        jmp     L0DD4

; 0xE20
TRACK0:
        ldx     #$07
L0E22:
        lda     $0F,x
        sta     SECTOR1,x
        dex
        bpl     L0E22
        ldy     #$10
        jsr     L0EBC
        ldy     #$20
        jsr     L0EBC
        ldy     #$30
        lda     #$FF
L0E38:
        sta     SECTOR1,y
        iny
        cpy     #$70
        bne     L0E38
        lda     #$03
        ldy     #$80
        sta     $17
        sty     $18
        ldx     $0E
        ldy     #$30
L0E4C:
        lda     $17
        sta     SECTOR1,y
        iny
        lda     $18
        sta     SECTOR1,y
        iny
        clc
        cld
        lda     $17
        adc     #$01
        sta     $17
        lda     $18
        adc     #$00
        sta     $18
        dex
        bne     L0E4C
        ldy     #$70
        lda     #$01
        sta     SECTOR1,y
        iny
        sta     SECTOR1,y
        iny
        ldx     $0E
        lda     #$02
L0E79:
        sta     SECTOR1,y
        iny
        dex
        bne     L0E79
        lda     #$00
L0E82:
        sta     SECTOR1,y
        iny
        bne     L0E82
        lda     #$01
        ldy     #$00
        jsr     L0ED9
        ldy     #$00
        lda     #$00
L0E93:
        sta     SECTOR1,y
        iny
        bne     L0E93
        lda     #$02
        ldy     #$00
        jsr     L0ED9
        lda     #$03
        sta     $0C
        ldy     #$00
        lda     #$20
L0EA8:
        sta     SECTOR1,y
        iny
        bne     L0EA8
L0EAE:
        ldy     #$00
        lda     $0C
        jsr     L0ED9
        inc     $0C
        dec     $0E
        bne     L0EAE
        rts

L0EBC:
        ldx     #$00
L0EBE:
        lda     L0801,x
        sta     SECTOR1,y
        iny
        inx
        cpx     #$08
        bne     L0EBE
        ldx     #$00
L0ECC:
        lda     L0809,x
        sta     SECTOR1,y
        iny
        inx
        cpx     #$03
        bne     L0ECC
        rts

L0ED9:
        sta     L09E5
        sty     L09E6
        lda     #$9B
        ldy     #$08
        sta     L09E7
        sty     L09E8
        brk
        .byte   $D7
        .byte   $9B
        ora     #$70
        .byte   $02
        brk
        bvc     L0F62
        ora     ($60,x)
        jsr     ERROR
        lda     #$06
        brk
        .byte   $01
