; da65 V2.18 - N/A
; Created:    2021-12-03 15:54:30
; Input file: ../ROMS/MUC_Board_Beranek/MUC_Beranek_lo.bin
; Page:       1


        .setcpu "6502"

_INL0           := $00F0
_INL1           := $00F1
_ADRLO0         := $00F2
_ADRLO1         := $00F3

CBLOCK_Bank_F601:= $F601
CBLOCK_DestL_F602:= $F602
CBLOCK_DestH_F603:= $F603
CBLOCK_Blocks_F604:= $F604

Floppy_DMA_Read_in_RAM_0xF7A0:= $F7A0    ; 0xf7a0
FNMRAD          := $F7A8                 ;A(DATENADRESSE)
FNMRAD_p1       := $F7A9
FloppySCR_00    := $F800
;FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804

NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
NMI_Read_Vector := $F8D2

NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9

OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_DRB      := $F8F2

; 0xFC00
        jmp     Vector_Reset_Bootstrap

; 0xFC03
Floppy_DMA_Read_to_RAM:
        pha                            ; F7A0
        sta     NMI_DISABLE ; $F8D0    ; F7A1
LFC07:
        lda     FloppyDAT_03    ; F7A4
        sta     $FFFF           ; Ziel-ADRESSE WIRD EINGETRAGEN ; F7A8,9 = FNMRAD
        inc     FNMRAD
        bne     LFC15
        inc     FNMRAD_p1
LFC15:
        lda     VIAMUC_VIAORB  ; $F8F0
        lsr     a
        bcc     LFC07          ;NMI = L
        lsr     a
        bcc     LFC15          ;IRQ = H
        lda     $FFFA          ;RESET NMI-LATCH
        sta     NMI_ENABLE  ; $F8D8
        pla
        rti

; 0xFC26
Vector_Reset_Bootstrap:
        ldx     #$FF
        txs
        lda     #$FE
        sta     OUTUMAP_F8E8    ;MAP USER 1
        lda     #$30
        sta     VIAMUC_DRB
        sta     VIAMUC_VIAORB ; $F8F0  ;NO MEM-PROTECT
        lda     #$FFFA          ;RESET NMI-LATCH
        cld
        jsr     Wait
        ldx     #$00
LFC3F:
        lda     Floppy_DMA_Read_to_RAM,x
        sta     Floppy_DMA_Read_in_RAM_0xF7A0,x
        inx
        cpx     #$23
        bne     LFC3F
LFC4A:
        lda     VIAMUC_VIAORB ; $F8F0   ;WARTEN AUF RESTORE-IRQ
        sta     LED_IDLE_OFF  ;$F8D1 ; WATCHDOG
        and     #$02              ;(DURCH RESET)
        bne     LFC4A
        lda     FloppySCR_00      ;LOESCHEN
        lda     #%10000000        ; $80 MOTOR ON
        sta     Floppy_MotorSel   ;ALLE DRIVES FREIGEBEN
        lda     #%11110110        ; $f6 MOTOR ON & SEL1 & SEL2 & SEL3 /IN_USE
        sta     Floppy_MotorSel
        lda     #%10000000        ; $80 MOTOR ON
        sta     Floppy_MotorSel
        lda     #%10001000        ; $88 MOTOR ON & IN_USE
        sta     Floppy_MotorSel
        lda     #%11001000        ; $c8 MOTOR ON & SEL1 & IN_USE
        sta     Floppy_MotorSel   ;MOTOR+DRIVE#1
        lda     #%11000000        ; $c0 MOTOR ON & SEL1 & /IN_USE
        sta     Floppy_MotorSel
        lda     #10
        sta     _INL0             ;1.SECTOR-1
        sta     NMI_ENABLE  ; $F8D8     ;NMI ERLAUBEN
LFC7C:
        lda     FloppySCR_00     ;WARTEN AUF READY
        sta     LED_IDLE_OFF ;$F8D1    ;WATCHDOG
        bmi     LFC7C
        jsr     Wait
        sta     LED_IDLE_ON   ; $F8D9   ;IDLE LEUCHTET
        lda     #$0D             ; Restore Command, Head Load, Verify, Stepping Rate 12ms
        sta     FloppySCR_00
LFC8F:
        lda     VIAMUC_VIAORB ; $F8F0
        sta     LED_IDLE_ON  ; $F8D9
        and     #$02
        bne     LFC8F            ; Wait for IRQ of Floppy 
        lda     FloppySCR_00     ;IRQ LOESCHEN
ReadNextBlock:
        lda     #$00             ; <CBUFF
        ldy     #$F6             ; >CBUFF
        sta     FNMRAD
        sty     FNMRAD_p1        ;A(CONTROL-BUFFER)
        jsr     ReadNextSector   ;LIES CONTROL-SECTOR
        lda     $F600            ;CBUFF
        cmp     #$45             ; 'E
        beq     LFCD8
        cmp     #$43             ; 'C
        beq     LFCB9
        lda     #$00
        jmp     Error0            ;KEIN CONTROL-BLOCK

LFCB9:
        lda     CBLOCK_Bank_F601
        beq     LFCC1
        sta     OUTMAP_F8E0
LFCC1:
        lda     CBLOCK_DestL_F602
        ldy     CBLOCK_DestH_F603
        sta     FNMRAD
        sty     FNMRAD_p1
LFCCD:
        jsr     ReadNextSector
        dec     CBLOCK_Blocks_F604
        bne     LFCCD
        jmp     ReadNextBlock

LFCD8:
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        lda     #$80            ; MOTOR ON
        sta     Floppy_MotorSel
        lda     #$C0            ; MOTOR ON & SEL1 & /IN_USE
        sta     Floppy_MotorSel
        lda     #$80            ; MOTOR ON
        sta     Floppy_MotorSel
        sta     LED_IDLE_OFF ;$F8D1  ;IDLE WEG -- OK
        jmp     LFF00         ;WARTEN AUF UMSCHALTEN

; 0xFCF9
Wait:
        ldx     #$00
        ldy     #$00
; 0xFCFD
Wait1:
        nop
        sta     NMI_Read_Vector    ;WATCHDOG
        nop
        inx
        bne     Wait1
        iny
        bne     Wait1
        rts

; 0xFD09
ReadNextSector:
        inc     _INL0          ; Sector
        lda     _INL0
        cmp     #11            ; schon am Ende?
        bcc     NotLastSector
        lda     #$5D
        sta     FloppySCR_00   ;STEPIN (next Track)
LFD16:
        lda     VIAMUC_VIAORB ; $F8F0
        sta     LED_IDLE_ON   ; $F8D9       
        and     #$02
        bne     LFD16         ; Wait for IRQ of Floppy 
        lda     FloppySCR_00
        and     #$D8
        beq     FirstSector         ; Check for Error
        lda     #$01
        jmp     Error0

FirstSector:
        lda     #$01
        sta     _INL0
NotLastSector:
        lda     _INL0
        sta     FloppySEC_02
        lda     #$84          ; Read single record, 15ms delay
        sta     FloppySCR_00
LFD3A:
        lda     VIAMUC_VIAORB ; $F8F0
        sta     LED_IDLE_ON   ; $F8D9
        and     #$02
        bne     LFD3A         ; Wait for IRQ of Floppy (comes at the end of DMA Transfer = 256 Byte)
        lda     FloppySCR_00
        and     #$BC
        beq     LFD50         ; Check for Error
        lda     #$02
        jmp     Error0

LFD50:
        rts

Error0:
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        jsr     Wait
        lda     #$80            ; MOTOR ON
        sta     Floppy_MotorSel
        lda     #$C0            ; MOTOR ON & SEL1 & /IN_USE
        sta     Floppy_MotorSel
        lda     #$80            ; MOTOR ON
        sta     Floppy_MotorSel
; 0xFD72
Error1:
        sta     LED_IDLE_OFF  ;$F8D1
        jsr     Wait
        sta     LED_IDLE_ON   ; $F8D9
        jsr     Wait
        jmp     Error1

LFF00:
        sta     LED_IDLE_OFF  ;$F8D1
        jmp     LFF00

; 0xFF06
Vector_not_used:
        sei
        sta     NMI_DISABLE  ; $F8D0
LFF0A:
        jmp     LFF0A

; 0xffe0
IRQ7_Vector:
        .addr   Vector_not_used
; 0xffe2
IRQ6_Vector:
        .addr   Vector_not_used
; 0xffe4
IRQ5_Vector:
        .addr   Vector_not_used
; 0xffe6
IRQ4_Vector:
        .addr   Vector_not_used
; 0xffe8
IRQ3_Vector:
        .addr   Vector_not_used
; 0xffea
IRQ2_Vector:
        .addr   Vector_not_used
; 0xffec
IRQ1_Vector:
        .addr   Vector_not_used
; 0xffee
IRQ0_Vector:
        .addr   Vector_not_used
; 0xfff0
NMI3_Vector:
        .addr   Floppy_DMA_Read_in_RAM_0xF7A0
; 0xfff2
NMI2_Vector:
        .addr   Vector_not_used
; 0xfff4
NMI1_Vector:
        .addr   Vector_not_used
; 0xfff6
NMI0_Vector:
        .addr   Vector_not_used
        .addr   Vector_not_used
        .addr   Vector_not_used
; 0xfffc
ResetVector1:
        .addr   Vector_Koppelprogramm ; Deleted!
; 0xfffe
ResetVector0:
        .addr   Vector_Reset_Bootstrap
