; da65 V2.18 - N/A
; Created:    2022-01-06 11:53:54
; Input file: ../ROMS/RAM_ROM_Board/V65_all.bin
; Page:       1


        .setcpu "6502"

_ZERO_          := $0000                        ; 0x0000
POIN_F0         := $00F0
POINC1_F4       := $00F4
POINC2_F6       := $00F6
POINI_FA        := $00FA
InterruptControl_Reg:= $00FE
StackTail_211   := $0211
JCSREG_0212     := $0212
JumpAddrL_221   := $0221
JumpAddrH_222   := $0222
BankTemp_22D    := $022D
BRK_TRAP_22F    := $022F
BrkSRC_ROM_23C_FF:= $023C
L0251           := $0251
PANIC_SRCL_25F  := $025F
PANIC_SRCH_260  := $0260
L03E1           := $03E1
Status_Serial2_Backup_03e5:= $03E5
UART_Flags_03e7 := $03E7
StoreA_in_SerialCommandReg:= $03E8
A_from_to_SerialRX_TX_Reg:= $03EC
SpecialCharacters_Copy:= $03F0
L04F4           := $04F4
FunctionByte_afterBRK_728:= $0728
PORT_B_Shadow_f3e8:= $F3E8
PH3TSK_F400_PRINTER_BUSY:= $F400
C3USAD_F41A     := $F41A
C3USAD_P3_F41D  := $F41D
C3ENT_F420      := $F420
Users_Installed_F421:= $F421                    ; 0xf421
C3USNR_F422     := $F422
SingleMultiMode_f423:= $F423
C3DATE_F424     := $F424                        ; 0xf424
C3TIME_F42E     := $F42E                        ; 0xf42e
C3TI20_F436     := $F436                        ; 0xf436
Bank_Shadow_F438:= $F438
LF439           := $F439
C3LGNZ_F43C_USERS:= $F43C
MY3MAP_F448     := $F448
MY3MAP_P1_F449  := $F449
MY3MAP_P2_F44A  := $F44A
MY3MAP_P3_F44B  := $F44B
LF460           := $F460
IPSW_F4AB       := $F4AB
IPANZ_F4AC_INIT_IPC:= $F4AC
IPADR_F4AE      := $F4AE
IPADR_P1_F4AF   := $F4AF
Floppy_DMA_Read_in_RAM_0xF7A0:= $F7A0           ; 0xf7a0
Floppy_DMA_Write_in_RAM_0xF7D0:= $F7D0          ; 0xf7d0
NMIURS_M1       := $F7FE
NMIURS          := $F7FF
FloppySCR_00    := $F800
FloppyTRK_01    := $F801
FloppySEC_02    := $F802
FloppyDAT_03    := $F803
Floppy_MotorSel := $F804
Serial_0U0_DataReg_Minus1:= $F80F
Serial_0U0_DataReg:= $F810
Serial_0U0_StatusReg:= $F811
Serial_0U0_CommandReg:= $F812
Serial_0U0_ControlReg:= $F813
Serial_1U0_DataReg:= $F814
Serial_1U0_StatusReg:= $F815
Serial_1U0_CommandReg:= $F816
Serial_1U0_ControlReg:= $F817
SerialDIPSwitchesU0:= $F818
Serial_0U1_DataReg:= $F820
Serial_0U1_StatusReg:= $F821
Serial_0U1_CommandReg:= $F822
Serial_0U1_ControlReg:= $F823
Serial_1U1_DataReg:= $F824
Serial_1U1_StatusReg:= $F825
Serial_1U1_CommandReg:= $F826
Serial_1U1_ControlReg:= $F827
SerialDIPSwitchesU1:= $F828
Serial_0U2_DataReg:= $F830
Serial_0U2_StatusReg:= $F831
Serial_0U2_CommandReg:= $F832
Serial_0U2_ControlReg:= $F833
Serial_1U2_DataReg:= $F834
Serial_1U2_StatusReg:= $F835
Serial_1U2_CommandReg:= $F836
Serial_1U2_ControlReg:= $F837
SerialDIPSwitchesU2:= $F838
Serial_0U3_DataReg:= $F840
Serial_0U3_StatusReg:= $F841
Serial_0U3_CommandReg:= $F842
Serial_0U3_ControlReg:= $F843
Serial_1U3_DataReg:= $F844
Serial_1U3_StatusReg:= $F845
Serial_1U3_CommandReg:= $F846
Serial_1U3_ControlReg:= $F847
SerialDIPSwitchesU3:= $F848
Serial_0U4_DataReg:= $F850
Serial_0U4_StatusReg:= $F851
Serial_0U4_CommandReg:= $F852
Serial_0U4_ControlReg:= $F853
Serial_1U4_DataReg:= $F854
Serial_1U4_StatusReg:= $F855
Serial_1U4_CommandReg:= $F856
Serial_1U4_ControlReg:= $F857
SerialDIPSwitchesU4:= $F858
Serial_0U5_DataReg:= $F860
Serial_0U5_StatusReg:= $F861
Serial_0U5_CommandReg:= $F862
Serial_0U5_ControlReg:= $F863
Serial_1U5_DataReg:= $F864
Serial_1U5_StatusReg:= $F865
Serial_1U5_CommandReg:= $F866
Serial_1U5_ControlReg:= $F867
SerialDIPSwitchesU5:= $F868
Printer_Card    := $F870
NMI_StatusReg   := $F8C8
NMI_DISABLE     := $F8D0
LED_IDLE_OFF    := $F8D1
NMI_Read_Vector := $F8D2
Pin_6B_Low      := $F8D4
Pin_5B_Low      := $F8D5
Pin_4B_Low      := $F8D6
Pin_3B_Low      := $F8D7
NMI_ENABLE      := $F8D8
LED_IDLE_ON     := $F8D9
NMI_Write_Vector:= $F8DA
Pin_6B_High     := $F8DC
Pin_5B_High     := $F8DD
Pin_4B_High     := $F8DE
Pin_3B_High     := $F8DF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
HexConv         := $FC01                        ; 0xfc01
Floppy_DriveSelect:= $FC11                      ; $40 is Drive 0, $20 - Dr.1, $10 - Dr.2, $04 - Dr.3, $02 - Dr.4
Bank_Switch_Values_FC16:= $FC16                 ; 0xfc16 - Low Bit Switches on Bank
UserData_1      := $FC26                        ; 0xfc26 goes to 206,207,209,20A,20B,20C,20D,20E  208=1
UserData_2      := $FC2E                        ; 0xfc2e
UserData_3      := $FC36                        ; 0xfc36
UserData_4      := $FC3E                        ; 0xfc3e
UserData_5      := $FC46                        ; 0xfc46
UserData_6      := $FC4E                        ; 0xfc4e
BS              := $FC5E                        ; SpecialCharacters WTHSFT 0xfc5e
CVOR            := $FC5F
DUP             := $FC60
ENTER           := $FC61
RESET           := $FC62
ENDL            := $FC63
CLEAR           := $FC64
DEL             := $FC65
INS             := $FC66
TAB             := $FC67
STAB            := $FC68
CLTAB           := $FC69
DUPTAB          := $FC6A
SLOW            := $FC6B
ESCAPE          := $FC6C
STOP            := $FC6D
START           := $FC6E
THVIDL          := $FC6F
THVIDC          := $FC70
THSPAC          := $FC71
THSLOS          := $FC72
THSTPS          := $FC73
THUPCS          := $FC74
WPHDFN          := $FC75                        ; CHAR_per_LINE
SHIFT           := $FC76
LINES_per_PAGE  := $FC77
HEADLINE        := $FC78
first_DATALINE  := $FC79
ammount_DATALINE:= $FC7A
VTSTEL          := $FC7B                        ; 12 msec Step-Time
filler          := $FC7C
Clear_Pins_nB_HB:= $FC7D                        ; 0xfc7d
Clear_Pins_nB_LB:= $FC7E                        ; 0xfc7e
Set_Pins_nB_HB  := $FC85                        ; 0xfc85
Set_Pins_nB_LB  := $FC86                        ; 0xfc86
ServerName      := $FF07                        ; 0xff07
FirmwareVersion1:= $FF0F                        ; 0xff0f
FirmwareVersion2:= $FF10                        ; 0xff10
; 0x8000
Vector_Reset:
        jmp     _Reset_ISR

; 0x8003
Vector_IRQ7:
        jmp     IRQ0_7_ISR

; 0x8006
Vector_IRQ6_MUC:
        jmp     IRQ6_MUCISR

; 0x8009
Vector_IRQ5_USR1234_SER2_PRINTER:
        jmp     IRQ5_ISR_Console

; 0x800c
Vector_IRQ4_USR3_SER1:
        jmp     IRQ4_ISRUSR3_Serial1

; 0x800f
Vector_IRQ3_USR2_SER1:
        jmp     IRQ3_ISRUSR2_Serial1

; 0x8012
Vector_IRQ2_USR14_SER1:
        jmp     IRQ2_ISR_USR14_Serial1

; 0x8015
Vector_IRQ1_FLOPPY:
        jmp     IRQ1_ISRFloppy

; 0x8018
Vector_IRQ0_BRK:
        jmp     IRQ0_7_ISR

; 0x801b
Vector_NMI0_1:
        jmp     SESSION_TERM

; 0x801e
Vector_CALL:
        jmp     _CALL_

; 0x8021
Vector_CREATE:
        jmp     _CREATE_

; 0x8024
Vector_DISMOUNT:
        jmp     _DISMOUNT_

; 0x8027
Vector_DRIVE:
        jmp     L99D4

; 0x802A
Vector_FHIRQRST:
        jmp     L8BB7

; 0x802D
Vector_FHIRQRW:
        jmp     L8BDE

; 0x8030
Vector_FHIRQSK:
        jmp     L8AFD

; 0x8033
Vector_FHNMIADR:
        jmp     L8CC2

; 0x8036
Vector_FHNMIRW:
        jmp     L8CB3

; 0x8039
Vector_LOCK:
        jmp     _Floppy_8039_

; 0x803c
Vector_LOGON:
        jmp     _Logon_

; 0x803F
Vector_LODPOIS2:
        jmp     _GetNextChar_

; 0x8042
Vector_MYINIT:
        jmp     L9CC7

; 0x8045
Vector_OUTAUS:
        jmp     OUTAUS

; 0x8048
Vector_OUTVOR:
        jmp     OUTVOR

; 0x804b
Vector_PROGTERM:
        jmp     PANIC

; 0x804e
Vector_SCHEDUL:
        jmp     L83A1

; 0x8051
Vector_SVC60:
        jmp     L94F2

; 0x8054
Vector_SVC61:
        jmp     L9453

; 0x8057
Vector_SVC62:
        jmp     L951F

; 0x805a
Vector_SVC63:
        jmp     L9587

; 0x805d
Vector_SVC64:
        jmp     L95C5

; 0x8060
Vector_SVC65:
        jmp     L95FA

; 0x8063
Vector_TASKTERM:
        jmp     L87CF

; 0x8066
Vector_THPRMPTE:
        jmp     L925E

; 0x8069
Vector_THWRITEE:
        jmp     L92A0

; 0x806c
Vector_LOFRUP:
        jmp     L9D6F

; 0x806f
Vector_SVC0D:
        jmp     L894D

; 0x8072;###################################################################;##                              RESET                            ##;###################################################################
TMON2:
        .byte   $06,$03,$06
        .byte   "1/ "
; 0x8078
TMON3:
        .byte   " SYSTEM "
; 0x8080
_Reset_ISR:
        sei
        ldx     #$FF
        lda     NMIURS_M1
        eor     #$FF
        cmp     NMIURS
        bne     L808E
        tax
L808E:
        txa
        eor     #$FF
        sta     NMIURS
        ldx     #$FF
        txs
        cld
        sta     NMI_DISABLE
        lda     $FFFA
        ldx     #$00
        ldy     #$00
L80A2:
        inx
        bne     L80A2
        iny
        bne     L80A2
        sta     NMI_DISABLE
; $80ab Enable all Interrupt
RESET_VIAS:
        lda     #$00
        sta     VIAMUC_VIAORA
        lda     #$FE
        sta     OUTMAP_F8E0
        lda     #$7E
        sta     OUTUMAP_F8E8
; $80ba PB4..6 Output 
Write_6522_DDRB:
        lda     #$70
        sta     VIAMUC_VIAORB
        sta     VIAMUC_DRB
; $80c2 PORTA all Output 
Write_6522_DDRA:
        lda     #$FF
        sta     VIAMUC_DRA
; $80c7 C0=Bit7,6 set
; (0)PA and (1)PB Latch NOT Enabled,
; (4..2) Shift Register Disabled,
; (5) T2 as Interval Timer in one shot mode,
; (7..6) T1 Free Running Mode, Output to PB7 
Write_6522_ACR:
        lda     #$C0
        sta     MUC6522_ACR
; $80cc Write 0x270e(9998) to T1
; Interrupts at 50Hz (Squarewave with 50Hz) 
Write_6522_T1:
        lda     #$0E
        sta     MUC6522_T1L
        lda     #$27
        sta     MUC6522_T1H
; $80d6 Enable CA1 Interrupt for RTC
comm:
        lda     #$82
        sta     MUC6522_IER
; $80db Sum all of ROM for a checksum
Check_all_ROMs:
        lda     #$FC
        ldx     #$FF
        ldy     #$00
        jsr     APRTES
        lda     #$80
        ldx     #$A0
        ldy     #$01
        jsr     APRTES
        lda     Bank_Switch_Values_FC16
        sta     OUTMAP_F8E0
        lda     #$A0
        ldx     #$C0
        ldy     #$02
        jsr     APRTES
        lda     $FC17
        sta     OUTMAP_F8E0
        lda     #$A0
        ldx     #$C0
        ldy     #$03
        jsr     APRTES
        lda     $FC18
        sta     OUTMAP_F8E0
        lda     #$A0
        ldx     #$C0
        ldy     #$04
        jsr     APRTES
        lda     $FC19
        sta     OUTMAP_F8E0
        lda     #$A0
        ldx     #$C0
        ldy     #$05
        jsr     APRTES
        ldx     #$00
        lda     #$00
; 0x812d
Clear_RAM_from_F380:
        sta     $F380,x
        inx
        cpx     #$68
        bne     Clear_RAM_from_F380
        lda     #$FF
        sta     $F3E7
        lda     #$68
        sta     $F3E9
        lda     #$FF
        ldx     #$00
; 0x8143
Clear_RAM_from_F400:
        sta     PH3TSK_F400_PRINTER_BUSY,x
        inx
        bne     Clear_RAM_from_F400
        jsr     Check_RAM
        lda     #$00
        sta     MY3MAP_F448
        sta     MY3MAP_P1_F449
        sta     MY3MAP_P2_F44A
        sta     MY3MAP_P3_F44B
        sta     IPSW_F4AB
        sta     IPADR_P1_F4AF
        sta     IPANZ_F4AC_INIT_IPC
        sta     PH3TSK_F400_PRINTER_BUSY
        sta     C3LGNZ_F43C_USERS
        sta     IPADR_P1_F4AF
        sta     $F4B0
        lda     #$AD
        sta     C3USAD_F41A
        lda     #$4C
        sta     C3USAD_P3_F41D
        lda     #$00
        sta     C3USNR_F422
        sta     C3ENT_F420
        lda     $F790
        ora     $F793
        cmp     #$2D
        bne     L81B3
        lda     $F798
        ora     $F79B
        cmp     #$3A
        bne     L81B3
        ldx     #$11
L8197:
        lda     $F78C,x
        cmp     #$2D
        beq     L81AA
        cmp     #$3A
        beq     L81AA
        cmp     #$30
        bcc     L81B3
        cmp     #$3A
        bcs     L81B3
L81AA:
        sta     C3DATE_F424,x
        dex
        bpl     L8197
        jmp     L81CD

L81B3:
        ldx     #$11
        lda     #$30
L81B7:
        sta     C3DATE_F424,x
        dex
        bpl     L81B7
        lda     #$2D
        sta     $F428
        sta     $F42B
        lda     #$3A
        sta     $F430
        sta     $F433
L81CD:
        lda     #$32
        sta     C3TI20_F436
        jsr     FLOPPYRESET_8cd5
        ldx     #$FF
        lda     VIAMUC_VIAORB
        and     #$08
        beq     L81E0
        ldx     #$00
L81E0:
        stx     SingleMultiMode_f423
        ldx     #$01
L81E5:
        lda     Bank_Switch_Values_FC16,x
        and     #$7F
        sta     OUTUMAP_F8E8
        lda     #$00
        sta     $0200
        lda     $0200
        bne     L8208
        lda     #$FF
        sta     $0200
        lda     $0200
        cmp     #$FF
        bne     L8208
        inx
        cpx     #$06
        bcc     L81E5
L8208:
        stx     Users_Installed_F421
        lda     $FC1C
        and     #$7F
        sta     OUTUMAP_F8E8
        lda     #$00
        sta     $F43D
        sta     $0200
        lda     $0200
        bne     L822F
        lda     #$FF
        sta     $0200
        lda     $0200
        cmp     #$FF
        bne     L822F
        sta     $F43D
L822F:
        lda     Bank_Switch_Values_FC16
        and     #$7F
        sta     OUTUMAP_F8E8
        lda     SingleMultiMode_f423
        beq     L8246
        lda     #$01
        sta     Users_Installed_F421
        lda     #$00
        sta     $F43D
L8246:
        jsr     ATIVOR
        lda     $F43D
        bne     L8253
        lda     #$FF
        sta     C3USNR_F422
L8253:
        lda     #$26
        ldy     #$FC
        sta     $F41B
        sty     $F41C
        lda     #$6A
        sta     $F41E
        lda     #$82
        sta     $F41F
        jmp     C3USAD_F41A

Indexed_Jump_826a:
        sta     OUTUMAP_F8E8
        ldx     #$FF
        txs
        lda     #$FF
        ldx     #$00
; 0x8274
Clear_all_RAM_1:
        sta     $0200,x
        sta     $0300,x
        sta     $0400,x
        sta     $0500,x
        sta     $0600,x
        sta     $0700,x
        inx
        bne     Clear_all_RAM_1
        ldx     #$00
        lda     #$00
; 0x828d
Clear_all_RAM_2:
        sta     a:_ZERO_,x
        sta     $0100,x
        inx
        bne     Clear_all_RAM_2
        lda     $F41B
        sta     POINC1_F4
        sta     $020F
        lda     $F41C
        sta     POINC1_F4+1
        sta     $0210
        ldy     #$00
        lda     (POINC1_F4),y
        sta     $0206
        lda     #$01
        sta     $0208
        ldy     #$01
        lda     (POINC1_F4),y
        sta     $0207
        ldy     #$02
        lda     (POINC1_F4),y
        sta     $0209
        iny
        lda     (POINC1_F4),y
        sta     $020A
        ldy     #$04
        lda     (POINC1_F4),y
        sta     $020B
        iny
        lda     (POINC1_F4),y
        sta     $020C
        ldy     #$06
        lda     (POINC1_F4),y
        sta     $020D
        iny
        lda     (POINC1_F4),y
        sta     $020E
        lda     #$00
        sta     $04EE
        lda     #$60
        sta     L0251
        lda     #$83
        pha
        lda     #$DC
        pha
        lda     #$00
        pha
        lda     #$00
        pha
        lda     #$30
        pha
        lda     #$00
        pha
        pha
        lda     #$00
        sta     $0213
        tsx
        stx     JCSREG_0212
        lda     Bank_Switch_Values_FC16
        sta     $0220
MainLoop_830c:
        inc     C3ENT_F420
        lda     C3ENT_F420
        bmi     SEDA
        cmp     Users_Installed_F421
        bcc     L8330
        lda     $F43D
        beq     SEDA
        lda     #$56
        sta     $F41B
        lda     #$FC
        sta     $F41C
        lda     #$F0
        sta     C3ENT_F420
        jmp     C3USAD_F41A

L8330:
        clc
        cld
        lda     $F41B
        adc     #$08
        sta     $F41B
        lda     $F41C
        adc     #$00
        sta     $F41C
        jmp     C3USAD_F41A

; 0x8345;###################################################################;##                        TASK - SCHEDULER                       ##;###################################################################
SEDA:
        lda     #$61
        sta     $F41E
        lda     #$83
        sta     $F41F
        lda     #$00
        sta     C3ENT_F420
        lda     #$26
        sta     $F41B
        lda     #$FC
        sta     $F41C
        jmp     C3USAD_F41A

SED0:
        sei
        sta     NMI_DISABLE
        sta     NMI_DISABLE
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        sta     $F437
        lda     $0220
        sta     OUTMAP_F8E0
        sta     Bank_Shadow_F438
        cli
        jsr     L96B5
        sta     LED_IDLE_ON
        lda     $0213
        beq     L83BA
        lda     $0214
        ldy     $0215
        sei
        sta     POINI_FA
        sty     POINI_FA+1
        ldy     #$00
        lda     (POINI_FA),y
        cli
        cmp     #$FF
        bne     L83BA
        jmp     L83B7

L83A1:
        sei
        lda     #$02
        sta     MUC6522_T2L
        lda     #$00
        sta     MUC6522_T2H
L83AC:
        lda     MUC6522_IFR
        and     #$20
        beq     L83AC
        lda     MUC6522_T2L
        cli
L83B7:
        jmp     MainLoop_830c

L83BA:
        sta     LED_IDLE_OFF
        jsr     L0251
; $83c0 Write 0xc34e(49998) to T2
; One Shot 25ms
Write_6522_T2:
        lda     #$4E
        sta     MUC6522_T2L
        lda     #$C3
        sta     MUC6522_T2H
; $83ca Enable T2 Interrupt
Write_6522_IER:
        lda     #$A0
        sta     MUC6522_IER
        pla
        tay
        pla
        tax
        pla
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        pla
        rti

; 0x83dc;###################################################################;##                           CREATE - TASK                       ##;###################################################################
_CREATE_:
        jsr     L9CC7
        lda     #$67
        sta     $037A
        ldx     #$4E
        lda     #$20
L83E8:
        sta     $0326,x
        dex
        bpl     L83E8
        lda     #$52
        sta     $0323
        lda     #$20
        sta     $04A0
        sta     $0218
        lda     #$00
        sta     BRK_TRAP_22F
        sta     BRK_TRAP_22F+1
        sta     $0239
        sta     $04AA
        sta     $0249
        sta     $024A
        lda     #$06
        ldy     #$A0
        ldx     #$01
        jsr     _CALL_
        lda     #$03
        ldy     #$A0
        ldx     #$01
        jsr     _CALL_
        lda     #$09
        ldy     #$A0
        ldx     #$01
        jsr     _CALL_
        jsr     AAHINI
        bit     $0207
        bvs     EX1
        jsr     R4QM
        sta     $0200
        sta     POINC1_F4
        sty     $0201
        sty     POINC1_F4+1
        ldy     #$41
        lda     #$D1
        sta     (POINC1_F4),y
        iny
        lda     #$02
        sta     (POINC1_F4),y
        lda     #$52
        sta     $02D1
        lda     #$03
        sta     $02D3
        lda     $0200
        ldy     $0201
        ldx     #$02
        brk
        .byte   $42
        bvs     CREAE
; 0x8460 - Prepare SYSIN, SYSOUT
EX1:
        jsr     R4QM
        sta     $0204
        sta     POINC1_F4
        sty     $0205
        sty     POINC1_F4+1
        jsr     R4QM
        sta     $0202
        sta     POINC2_F6
        sty     $0203
        sty     POINC2_F6+1
        lda     #$52
        sta     $027E
        bit     $0207
        bvc     L8487
        jmp     L8578

L8487:
        ldy     #$40
        lda     $0208
        sta     $0236
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        ldy     #$2E
        lda     #$02
        sta     (POINC2_F6),y
        lda     #$72
        ldy     #$80
        sta     $0225
        sty     $0226
        lda     #$7E
        ldy     #$02
        sta     $0223
        sty     $0224
        sta     $0227
        sty     $0228
        lda     $0202
        ldy     $0203
        ldx     #$02
        brk
        .byte   $42
        bvc     L84C2
; 0x84BF
CREAE:
        jmp     CREAER

L84C2:
        jsr     OUTVOR
        jsr     OUTAUS
        bvs     CREAE
        lda     NMIURS
        beq     CREXX
        jsr     OUTVOR
; 0x84D2
comm:
        brk
        .byte   $26
        .byte   "  RESET-CODE"

        .byte   $A0
        lda     NMIURS
; 0x84e4
comm:
        brk
        .byte   $2F
; 0x84e6
comm:
        brk
        .byte   $24
        lda     #$00
        sta     NMIURS
        sta     NMIURS_M1
        jsr     OUTAUS
; 0x84F3
CREXX:
        jsr     OUTVOR
; 0x84f6 - 26 start of text, AE is a DOT
comm:
        brk
        .byte   $26
        .byte   "  es65-MONITOR VERS.B"


        .byte   $AE
; 0x850e - 2e writes high digit of A to console  ($65 -> 6)
comm:
        lda     #$65
        brk
        .byte   $2E
; 0x8512 - 26 start of text, AE is a DOT
comm:
        brk
        .byte   $26
        .byte   $AE
; 0x8515 - 2d writes low digit of A to console  ($65 -> 5)
comm:
        brk
        .byte   $2D
; 0x8517 - 26 start of text, A0 is SPACE
comm:
        brk
        .byte   $26
        .byte   " - KONFIGURATION:"


        .byte   $A0
        lda     Users_Installed_F421
; 0x852e - 2d writes low digit of A to console 
comm:
        brk
        .byte   $2D
        lda     $F43D
        and     #$01
; 0x8535 - 2d writes low digit of A to console 
comm:
        brk
        .byte   $2D
        ldx     #$00
L8539:
        lda     $F44C,x
        brk
        .byte   $23
        inx
        cpx     #$04
        bcc     L8539
        jsr     OUTAUS
        bvs     CREAER
        lda     $0204
        ldy     $0205
        ldx     #$01
        brk
        .byte   $42
        bvs     CREAER
        lda     $0204
        ldy     $0205
        sta     POINC1_F4
        sty     POINC1_F4+1
        ldy     #$43
        lda     #$72
        sta     (POINC1_F4),y
        iny
        lda     #$80
        sta     (POINC1_F4),y
        jmp     L86F3

; 0x856c - SYSIN FCB, SYSOUT FCB
R4QM:
        ldx     #$01
        brk
        bmi     L85E1
        ora     ($60,x)
        pla
        pla
; 0x8575
CREAER:
        jmp     CREAER

L8578:
        lda     #$FF
        sta     $F43E
        lda     #$00
        sta     $F43F
        sta     C3USNR_F422
        lda     #$3E
        ldy     #$F4
; 0x8589 - 2e 
comm:
        brk
        .byte   $05
        lda     #$00
        ldy     #$02
        sta     POINC2_F6
        sty     POINC2_F6+1
        lda     $F440
        ldy     $F441
        sta     POINC1_F4
        sty     POINC1_F4+1
        ldx     #$02
        ldy     #$5E
        jmp     L85A6

L85A4:
        ldy     #$00
L85A6:
        lda     (POINC1_F4),y
        sta     (POINC2_F6),y
        iny
        bne     L85A6
        inc     POINC1_F4+1
        inc     POINC2_F6+1
        inx
        cpx     #$08
        bcc     L85A4
        sei
        ldx     #$00
L85B9:
        lda     $04BA,x
        cmp     #$41
        bne     L85E4
        inc     $F750,x
        txa
        asl     a
        tay
        lda     $F755,y
        sta     POINC2_F6
        lda     $F756,y
        sta     POINC2_F6+1
        ldy     #$2B
        sed
        clc
        lda     (POINC2_F6),y
        adc     #$01
        sta     (POINC2_F6),y
        iny
        lda     (POINC2_F6),y
        adc     #$00
        sta     (POINC2_F6),y
L85E1:
        jmp     L85E9

L85E4:
        lda     #$20
        sta     $04BA,x
L85E9:
        inx
        cpx     #$05
        bcc     L85B9
        cli
        ldx     #$01
L85F1:
        lda     $0204,x
        sta     POINC1_F4,x
        lda     $0202,x
        sta     POINC2_F6,x
        dex
        bpl     L85F1
        ldy     #$7D
L8600:
        lda     #$00
        sta     (POINC1_F4),y
        sta     (POINC2_F6),y
        dey
        bpl     L8600
        ldy     #$00
        lda     #$49
        sta     (POINC1_F4),y
        lda     #$43
        sta     (POINC2_F6),y
        lda     $0204
        ldy     $0205
        ldx     #$01
; 0x861b - 2e 
comm:
        brk
        .byte   $18
        bvc     L8624
L861F:
        lda     #$A1
        jmp     L86AB

L8624:
        ldy     #$40
        lda     (POINC1_F4),y
        cmp     #$03
        bne     L861F
        sta     $0236
        lda     #$2C
; 0x8631 - 2e 
comm:
        brk
        .byte   $12
        bvs     L861F
        lda     $0202
        ldy     $0203
        ldx     #$02
        brk
        clc
        bvs     L861F
        ldy     #$40
        lda     (POINC2_F6),y
        cmp     #$01
        beq     L861F
        ldy     #$2E
        lda     #$02
        sta     (POINC2_F6),y
        lda     #$06
        ldy     #$A0
        ldx     #$02
        jsr     _CALL_
        bvs     L86AB
        sta     $0249
        sty     $024A
        lda     #$21
        cpy     #$00
        bne     L8668
        lda     #$FF
L8668:
        sta     $0248
        lda     #$7E
        ldy     #$02
        sta     $0223
        sty     $0224
        sta     $0227
        sty     $0228
        lda     $0204
        ldy     $0205
        ldx     #$01
        brk
        .byte   $42
        bvs     L86AB
        lda     $0202
        ldy     $0203
        ldx     #$02
        brk
        .byte   $42
L8691:
        bvc     L86A0
        pha
        lda     $0204
        ldy     $0205
        brk
        .byte   $43
        pla
        jmp     L86AB

L86A0:
        lda     #$01
        sta     $F442
        jsr     _Logon_
        jmp     L86C2

L86AB:
        pha
        lda     #$03
        ldy     #$A0
        ldx     #$02
        jsr     _CALL_
        lda     $027F
        sta     $F443
        pla
        sta     $F442
        jmp     _CREATE_

L86C2:
        ldx     #$07
L86C4:
        lda     TMON3,x
        sta     $0218,x
        dex
        bpl     L86C4
        lda     #$50
        sta     $027E
        lda     #$23
        ldy     #$02
        ldx     #$46
; 0x86d8 - 09 ? 
comm:
        brk
        .byte   $09
        bvs     L86F3
        lda     #$53
        sta     $027E
        lda     #$7E
        ldy     #$02
        brk
        bpl     L8691
        cpy     #$00
        .byte   $1C
        brk
        ora     a:$A2,x
        brk
        .byte   $03
        bvc     L8752
L86F3:
        sta     $025E
        jsr     OUTVOR
        lda     $04A0
        cmp     #$20
        bne     L8713
; 0x8700 - 26 start of text, CE is N
comm:
        brk
        .byte   $26
        .byte   "  PLEASE LOGO"

        .byte   $CE
        jmp     L8741

L8713:
        lda     $025E
        cmp     #$A1
        bne     L8738
        brk
        rol     $20
        ldy     #$38
        cld
        lda     $027F
        sbc     #$03
        tax
        beq     L872C
        lda     #$2E
        brk
        .byte   $2C
L872C:
        lda     #$5E
        brk
        and     ($20,x)
        cmp     $7099
        plp
        jsr     OUTVOR
L8738:
        lda     #$00
        ldy     #$00
        ldx     $025E
        brk
        rol     a
L8741:
        jsr     OUTAUS
        bvs     HALT
L8746:
        lda     $0236
        cmp     #$01
        beq     L8752
        lda     #$FF
        sta     $0239
L8752:
        jsr     OUTVOR
        jsr     OUTAUS
        bvs     HALT
        jmp     L86C2

; 0x875d
HALT:
        jmp     HALT

; 0x8760 - Stack is Higher than F9
PANIC:
        sta     $025E
        lda     #$00
        sta     BRK_TRAP_22F+1
        ldx     #$FF
        txs
        lda     #$20
        sta     $0218
        lda     #$FF
        ldy     #$FF
; 0x8774 - 31 
comm:
        brk
        .byte   $31
        jsr     L9CC7
        jsr     L95BB
; 0x877c - 37 
comm:
        brk
        .byte   $37
        lda     $025E
        cmp     #$01
        beq     L87CC
        sei
        lda     Bank_Switch_Values_FC16
        sta     Bank_Shadow_F438
        sta     OUTMAP_F8E0
        cli
        lda     $025E
        cmp     #$06
        beq     L87C9
        jsr     OUTVOR
; 0x879a - 26 - text 
comm:
        brk
        .byte   $26
        .byte   "> ABNORM TERM AT"

        .byte   $A0
        ldy     PANIC_SRCH_260
        lda     PANIC_SRCL_25F
; 0x87b3 - 25 ?
comm:
        brk
        .byte   $25
; 0x87b5 - 26 - text 
comm:
        brk
        .byte   $26
        .byte   " DUE T"
        .byte   $CF
        lda     $025E
        tax
; 0x87c2 - 
comm:
        brk
        .byte   $AA
; 0x87c4 - 00 ?
comm:
        brk
        .byte   $00
        jsr     OUTAUS
L87C9:
        jmp     L8746

L87CC:
        jmp     L8752

L87CF:
        lda     #$03
        ldy     #$A0
        ldx     #$02
        jsr     _CALL_
        dec     C3LGNZ_F43C_USERS
        bit     $0207
L87DE:
        bvc     L87DE
        ldx     #$FF
        txs
        brk
        .byte   $04
        jmp     _CREATE_

; 0x87e8
SESSION_TERM:
        pha
        lda     #$70
        sta     VIAMUC_VIAORB
        lda     NMI_StatusReg
        sta     NMIURS
        eor     #$FF
        sta     NMIURS_M1
L87F9:
        jmp     L87F9

; 0x87fc
NOT_GOOD:
        ldx     StackTail_211
        lda     $0107,x
        sta     PANIC_SRCH_260
        lda     $0106,x
        sta     PANIC_SRCL_25F
        lda     #$00
        jmp     PANIC

; 0x8810;--- $230 always 00
BreakInterrupt:
        lda     BRK_TRAP_22F+1
        beq     L883F
        sta     POIN_F0+1
        lda     BRK_TRAP_22F
        sta     POIN_F0
        sei
        ldx     StackTail_211
        lda     $0103,x
        ora     #$CF
        and     VIAMUC_VIAORB
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        cli
        jsr     IndexedJump_F0
        sei
        lda     VIAMUC_VIAORB
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        cli
L883F:
        lda     StackTail_211
        pha
        tsx
        inx
        stx     StackTail_211
        cpx     #$FA
; 0x884a - branch if Stack < $F9 ?
comm:
        bcc     Stack_4_or_more
        pla
        pla
        pla
        pla
        pla
        pla
        pla
        sta     PANIC_SRCL_25F
        pla
        sta     PANIC_SRCH_260
        lda     #$03
        jmp     PANIC

; 0x885f - Stack is more than 7 Byte deep and should be on 1F6
Stack_4_or_more:
        lda     Bank_Shadow_F438
        sta     BankTemp_22D
; 0x8865 - F0 and F1 ist the first (16bit) Address after the BRK ; if BRK is @ A0F1 the stack gets pushed A0F3
comm:
        lda     $0106,x
        sta     POIN_F0
        lda     $0107,x
        sta     POIN_F0+1
        lda     POIN_F0
        bne     L8875
        dec     POIN_F0+1
; 0x8875 - Set Pointer to Status Register on Stack
L8875:
        dec     POIN_F0
        ldy     #$00
; 0x8879 - Get if Source Address is ROM (>8000) or  RAM
comm:
        lda     $0107,x
        cmp     #$80
        bcc     Source_RAM
        ldy     #$FF
; 0x8882
Source_RAM:
        sty     BrkSRC_ROM_23C_FF
; 0x8885 Get Status Reg
comm:
        ldy     #$00
        lda     (POIN_F0),y
        sta     FunctionByte_afterBRK_728
        cmp     #$A6
        bne     L8893
        jmp     NOT_GOOD

L8893:
        lda     FunctionByte_afterBRK_728
        bpl     Function_below_80
; 0x8898 If Function after BRK is >= 80 the next two bytes get written;  to 101,x and 104,x  which is 1F9 and 1FC and the BIT7 gets cleared
comm:
        ldy     #$01
        lda     (POIN_F0),y
        ldx     StackTail_211
        sta     $0104,x
        iny
        lda     (POIN_F0),y
        sta     $0101,x
        clc
        cld
        lda     $0106,x
        adc     #$02
        sta     $0106,x
        lda     $0107,x
        adc     #$00
        sta     $0107,x
        lda     FunctionByte_afterBRK_728
        and     #$7F
        sta     FunctionByte_afterBRK_728
; 0x88c2
Function_below_80:
        cmp     #$70
        bcc     Function_below_70
        jmp     NOT_GOOD

; 0x88c9
Function_below_70:
        lda     FunctionByte_afterBRK_728
        and     #$F0
        cmp     #$10
        bne     Function_2x_or_3x
; 0x88d2 - Here are all functions from 10-1f
comm:
        lda     $049C
        ldy     $049D
        sta     POIN_F0
        sty     POIN_F0+1
        jmp     Function_3x

; 0x88df
Function_2x_or_3x:
        cmp     #$20
        bne     Function_3x
; 0x88e3 - Here are all functions from 20-2f
comm:
        lda     $049E
        ldy     $049F
        sta     POIN_F0
        sty     POIN_F0+1
; 0x88ED - Here are all functions from 30-3f
Function_3x:
        sei
        lda     Bank_Switch_Values_FC16
        sta     OUTMAP_F8E0
        lda     FunctionByte_afterBRK_728
        asl     a
        tax
        inx
        lda     $A000,x
        tay
        dex
        lda     $A000,x
        pha
        lda     BankTemp_22D
        sta     OUTMAP_F8E0
        cli
        pla
        cpy     #$00
        bne     L8912
        jmp     NOT_GOOD

L8912:
        ldx     #$00
        clv
        jsr     _CALL_
        pla
        sta     StackTail_211
        pla
        tay
        pla
        tax
        pla
        sta     $07AD
        pla
        sta     FunctionByte_afterBRK_728
        bvc     L8931
        pla
        ora     #$40
        pha
        jmp     L8935

L8931:
        pla
        and     #$BF
        pha
L8935:
        lda     FunctionByte_afterBRK_728
        pha
        sei
        lda     $07AD
        ora     #$CF
        and     VIAMUC_VIAORB
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        pla
        rti

; 0x894a
IndexedJump_F0:
        jmp     (POIN_F0)

L894D:
        ldx     StackTail_211
        lda     $0104,x
        beq     L8983
        sei
        lda     $F4B0
        cmp     $0207
        bne     L8968
        lda     IPADR_P1_F4AF
        beq     L8968
        bit     $FC00
        cli
        rts

L8968:
        cli
        lda     #$5F
        ldy     #$F4
        jsr     _Floppy_8039_
        sei
        lda     $0207
        sta     $F4B0
        ldx     StackTail_211
        lda     $0104,x
        sta     IPADR_P1_F4AF
        cli
        clv
        rts

L8983:
        sei
        lda     $F4B0
        cmp     $0207
        bne     L8999
        lda     IPADR_P1_F4AF
        beq     L8999
        lda     #$00
        sta     $F45F
        sta     IPADR_P1_F4AF
L8999:
        cli
        clv
        rts

; 0x899c
IRQ0_7_ISR:
        pha
        lda     VIAMUC_VIAORB
        pha
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        txa
        pha
        tya
        pha
        tsx
; 0x89ae
CheckBRK_Bit:
        lda     $0105,x
        and     #$10
        beq     RegularInterupt_89b9
        cli
        jmp     BreakInterrupt

; 0x89b9
RegularInterupt_89b9:
        pla
        tay
        pla
        tax
        pla
        ora     #$CF
        and     VIAMUC_VIAORB
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        pla
        rti

; 0x89cb
IRQ1_ISRFloppy:
        pha
        lda     VIAMUC_VIAORB
        pha
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        txa
        pha
        tya
        pha
        tsx
        lda     $0105,x
        and     #$10
        beq     RegularInterupt_89e8
        cli
        jmp     BreakInterrupt

; 0x89e8
RegularInterupt_89e8:
        lda     JCSREG_0212
        sta     $0217
        tsx
        stx     JCSREG_0212
        sta     NMI_DISABLE
        lda     $F47B
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        lda     $F475
        sta     $FC
        ldy     $F476
        sty     $FD
        jsr     L8A28
        sta     NMI_DISABLE
        lda     $F437
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        lda     $0217
        sta     JCSREG_0212
        jmp     RegularInterupt_89b9

L8A28:
        jmp     (LF460)

; 0x8a2b
Indirect_Jump_8A2B:
        lda     FloppySCR_00
        lda     #$00
        sta     $F45F
        rts

L8A34:
        lda     FloppySCR_00
        sta     $F474
        ldx     $F4A1
        and     #$40
        beq     L8A43
        lda     #$FF
L8A43:
        sta     $F497,x
        lda     $F474
        and     #$98
        bne     L8A51
        lda     #$01
        clv
        rts

L8A51:
        lda     $F474
        bpl     L8A5B
        lda     #$C1
        jmp     L8A6D

L8A5B:
        lda     $F474
        and     #$10
        beq     L8A66
        lda     #$F4
        bne     L8A68
L8A66:
        lda     #$C3
L8A68:
        bit     $FC00
        sec
        rts

L8A6D:
        bit     $FC00
        clc
        rts

L8A72:
        lda     FloppySCR_00
        sta     $F474
        and     #$FC
        bne     L8A9E
        lda     $F47A
        cmp     #$84
        beq     L8A87
        cmp     #$A4
        bne     L8A95
L8A87:
        lda     $F7AD
        ora     $F7DA
        and     #$7F
        beq     L8A95
        lda     #$C6
        bne     L8A99
L8A95:
        lda     #$01
        clv
        rts

L8A99:
        bit     $FC00
        clc
        rts

L8A9E:
        lda     $F474
        bpl     L8AA7
        lda     #$C1
        bne     L8A99
L8AA7:
        lda     $F474
        and     #$40
        beq     L8AB2
        lda     #$C5
        bne     L8A99
L8AB2:
        lda     $F474
        and     #$20
        beq     L8ABD
        lda     #$C2
        bne     L8AF8
L8ABD:
        lda     $F474
        and     #$10
        beq     L8AC8
        lda     #$F5
        bne     L8AF8
L8AC8:
        lda     $F474
        and     #$08
        bne     L8AD3
        lda     #$C4
        bne     L8AF8
L8AD3:
        lda     $F493
        sta     $F7AD
        sta     $F7DA
        lda     $F494
        sta     $F7AE
        sta     $F7DB
        jsr     L8C20
        dec     $F47C
        beq     L8AF6
        pla
        pla
        lda     $F47A
        sta     FloppySCR_00
        rts

L8AF6:
        lda     #$C3
L8AF8:
        bit     $FC00
        sec
        rts

L8AFD:
        jsr     L8A34
        bvc     L8B40
        bcc     L8B2C
        ldx     $F4A2
        bne     L8B2C
L8B09:
        lda     #$5B
        ldy     #$8B
        sta     LF460
        sty     $F461
        jsr     L8C20
        lda     #$0C
        ora     $F495
        sta     FloppySCR_00
        ldx     $F4A1
        lda     #$00
        sta     $F769,x
        lda     #$FF
        sta     $F4A2
        rts

L8B2C:
        ldy     #$49
        sta     ($FC),y
        ldy     #$72
        lda     $F474
        sta     ($FC),y
        jsr     Set_PB6
        lda     #$00
        sta     $F45F
        rts

L8B40:
        lda     $F478
        ldy     $F479
        sta     LF460
        sty     $F461
        jsr     Reset_PB6
        lda     #$05
        sta     $F47C
        lda     $F47A
        sta     FloppySCR_00
        rts

        jsr     L8A34
        bvs     L8B2C
        lda     #$FD
        ldy     #$8A
        sta     LF460
        sty     $F461
        lda     $F463
        ldx     $F4A1
        sta     $F769,x
        cmp     #$00
        beq     L8B40
        sta     FloppyDAT_03
        lda     #$1C
        ora     $F495
        sta     FloppySCR_00
        rts

        jsr     L8A72
        sta     $F45F
        jsr     Set_PB6
        rts

        jsr     L8A72
        bvc     L8B99
        sta     $F45F
        jsr     Set_PB6
        rts

L8B99:
        lda     #$02
        sta     FloppySEC_02
        lda     #$83
        ldy     #$8B
        sta     LF460
        sty     $F461
        lda     #$05
        sta     $F47C
        inc     $F494
        lda     $F47A
        sta     FloppySCR_00
        rts

L8BB7:
        jsr     L8A34
        bvc     L8BC0
        sta     $F45F
        rts

L8BC0:
        lda     #$01
        sta     FloppySEC_02
        lda     #$8D
        ldy     #$8B
        sta     LF460
        sty     $F461
        jsr     Reset_PB6
        lda     #$05
        sta     $F47C
        lda     $F47A
        sta     FloppySCR_00
        rts

L8BDE:
        lda     $F47A
        cmp     #$84
        bne     L8BFA
        ldy     #$4C
        lda     ($FC),y
        sta     POINI_FA
        iny
        lda     ($FC),y
        sta     POINI_FA+1
        ldy     #$00
L8BF2:
        lda     $F600,y
        sta     (POINI_FA),y
        iny
        bne     L8BF2
L8BFA:
        jsr     L8A72
        bvc     L8C0C
        bcc     L8C0C
        ldx     $F4A2
        bne     L8C0C
        jsr     Set_PB6
        jmp     L8B09

L8C0C:
        ldy     #$49
        sta     ($FC),y
        ldy     #$72
        lda     $F474
        sta     ($FC),y
        lda     #$00
        sta     $F45F
        jsr     Set_PB6
        rts

L8C20:
        lda     $F4A1
        asl     a
        tax
        lda     $F755,x
        sta     POINI_FA
        lda     $F756,x
        sta     POINI_FA+1
        ldy     #$1B
        clc
        sed
        lda     (POINI_FA),y
        adc     #$01
        sta     (POINI_FA),y
        iny
        lda     (POINI_FA),y
        adc     #$00
        sta     (POINI_FA),y
        rts

; 0x8c41
Floppy_DMA_Read_Handler_F7A0:
        pha
        sta     NMI_DISABLE
        lda     #$30
        sta     VIAMUC_VIAORB
L8C4A:
        lda     FloppyDAT_03
        sta     $FFFF
        inc     $F7AD
        bne     L8C58
        inc     $F7AE
L8C58:
        lda     VIAMUC_VIAORB
        lsr     a
        bcc     L8C4A
        lsr     a
        bcs     L8C58
        jmp     L8C87

; 0x8c64
Floppy_DMA_Write_Handler_F7D0:
        pha
        sta     NMI_DISABLE
        lda     #$30
        sta     VIAMUC_VIAORB
L8C6D:
        lda     $FFFF
        sta     FloppyDAT_03
        inc     $F7DA
        bne     L8C7B
        inc     $F7DB
L8C7B:
        lda     VIAMUC_VIAORB
        lsr     a
        bcc     L8C6D
        lsr     a
        bcs     L8C7B
        jmp     L8C87

L8C87:
        txa
        pha
        tsx
        lda     $0103,x
        and     #$10
        beq     L8CA3
        cld
        sec
        lda     $0104,x
        sbc     #$02
        sta     $0104,x
        lda     $0105,x
        sbc     #$00
        sta     $0105,x
L8CA3:
        pla
        tax
        lda     $FFFA
        sta     NMI_ENABLE
        lda     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        pla
        rti

L8CB3:
        sta     NMI_Read_Vector
        cmp     #$84
        beq     L8CC1
        cmp     #$E4
        beq     L8CC1
        sta     NMI_Write_Vector
L8CC1:
        rts

L8CC2:
        sta     $F7AD
        sty     $F7AE
        sta     $F7DA
        sty     $F7DB
        sta     $F493
        sty     $F494
        rts

; 0x8cd5
FLOPPYRESET_8cd5:
        lda     VTSTEL
        bpl     L8CDC
        lda     #$01
L8CDC:
        sta     $F495
        ldx     #$00
L8CE1:
        lda     Floppy_DMA_Read_Handler_F7A0,x
        sta     Floppy_DMA_Read_in_RAM_0xF7A0,x
        lda     Floppy_DMA_Write_Handler_F7D0,x
        sta     Floppy_DMA_Write_in_RAM_0xF7D0,x
        inx
        cpx     #$23
        bne     L8CE1
        lda     #$20
        sta     $F700
        sta     $F708
        sta     $F710
        sta     $F718
        sta     $F720
        lda     #$00
        sta     $F750
        sta     $F751
        sta     $F752
        sta     $F753
        sta     $F754
        sta     $F477
        sta     $F47D
        sta     $F47E
        lda     #$2B
        ldy     #$8A
        sta     LF460
        sty     $F461
        lda     UserData_1
        sta     $F47B
        ldx     #$00
; 0x8d2f
Floppy_Init:
        lda     #$80
        sta     Floppy_MotorSel
        ora     Floppy_DriveSelect,x
        sta     Floppy_MotorSel
        and     #$80
        sta     Floppy_MotorSel
        inx
        cpx     #$05
        bne     Floppy_Init
        ldx     #$00
        lda     #$00
L8D48:
        sta     $F500,x
        inx
        bne     L8D48
        rts

; 0x8d4f
IRQ5_ISR_Console:
        pha
        lda     VIAMUC_VIAORB
        pha
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        txa
        pha
        tya
        pha
        tsx
        lda     $0105,x
        and     #$10
        beq     RegularInterupt_8d6c
        cli
        jmp     BreakInterrupt

; 0x8d6c
RegularInterupt_8d6c:
        tsx
        lda     JCSREG_0212
        sta     $0217
        stx     JCSREG_0212
        jmp     (LF439)

Six_User_L8d79:
        lda     Serial_1U5_StatusReg
        bpl     Five_User_L8d95
        tay
        sta     NMI_DISABLE
        lda     UserData_6
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        jsr     UART_Read
        jmp     L8E1E

Five_User_L8d95:
        lda     Serial_1U4_StatusReg
        bpl     Four_User_L8db1
        tay
        sta     NMI_DISABLE
        lda     UserData_5
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        jsr     UART_Read
        jmp     L8E1E

Four_User_L8db1:
        lda     Serial_1U3_StatusReg
        bpl     Three_User_L8dcd
        tay
        sta     NMI_DISABLE
        lda     UserData_4
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        jsr     UART_Read
        jmp     L8E1E

Three_User_L8dcd:
        lda     Serial_1U2_StatusReg
        bpl     Two_User_L8de9
        tay
        sta     NMI_DISABLE
        lda     UserData_3
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        jsr     UART_Read
        jmp     L8E1E

Two_User_L8de9:
        lda     Serial_1U1_StatusReg
        bpl     Single_User_L8e05
        tay
        sta     NMI_DISABLE
        lda     UserData_2
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        jsr     UART_Read
        jmp     L8E1E

Single_User_L8e05:
        lda     Serial_1U0_StatusReg
        bpl     L8E1E
        tay
        sta     NMI_DISABLE
        lda     UserData_1
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        jsr     UART_Read
L8E1E:
        sta     NMI_DISABLE
        lda     $F437
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        lda     $0217
        sta     JCSREG_0212
        lda     $F87D
        bpl     L8E40
        and     #$02
        beq     L8E40
        jsr     L92EC
L8E40:
        jmp     RegularInterupt_89b9

; 0x8e43
UART_Read:
        tya
        sta     Status_Serial2_Backup_03e5
        and     #$08
        beq     UART_Write
        lda     #$AD
        sta     A_from_to_SerialRX_TX_Reg
        jsr     A_from_to_SerialRX_TX_Reg
        jsr     UART_Process_Char
        bvs     UART_Write
        ldx     #$8D
        stx     A_from_to_SerialRX_TX_Reg
        jsr     A_from_to_SerialRX_TX_Reg
; 0x8e60
UART_Write:
        lda     Status_Serial2_Backup_03e5
        and     #$10
        beq     L8E79
        bit     UART_Flags_03e7
        bvc     L8E79
        jsr     L8E7D
        bvs     L8E79
        ldx     #$8D
        stx     A_from_to_SerialRX_TX_Reg
        jsr     A_from_to_SerialRX_TX_Reg
L8E79:
        rts

L8E7A:
        inc     $037B
L8E7D:
        ldx     $037B
        cpx     $037C
        bcc     L8E88
        jmp     (L03E1)

L8E88:
        lda     $037A,x
        bmi     L8E92
        inc     $037B
        clv
        rts

L8E92:
        tay
        dey
        tya
        sta     $037A,x
        cmp     #$80
        beq     L8E7A
        cmp     #$7F
        beq     L8E7A
        lda     $037B,x
        clv
        rts

L8EA5:
        and     #$7F
        sta     $0407
        lda     $0323,x
        and     #$80
        ora     $0407
        sta     $0323,x
        and     #$7F
        rts

; 0x8eb8
UART_Process_Char:
        and     #$7F
        tay
        lda     UART_Flags_03e7
        bmi     L8EF7
        cpy     $03FE
        bne     L8ECA
        ora     #$01
        sta     UART_Flags_03e7
L8ECA:
        cpy     $03FD
        bne     L8ED7
        lda     $0404
        eor     #$FF
        sta     $0404
L8ED7:
        lda     $0405
        bne     L8EE9
        cpy     $03FF
        bne     UART_Bit_FC00_RTS
        lda     #$FF
        sta     $0405
        jmp     UART_Bit_FC00_RTS

L8EE9:
        cpy     $0400
        bne     UART_Bit_FC00_RTS
        lda     #$00
        sta     $0405
; 0x8ef3
UART_Bit_FC00_RTS:
        bit     $FC00
        rts

L8EF7:
        cpy     #$1B
        bne     UART_NO_ESC_received
        lda     #$80
        sta     $040B
        bne     UART_Bit_FC00_RTS
; 0x8f02
UART_NO_ESC_received:
        ldx     $0325
        tya
        ora     $040B
        pha
        lda     #$00
        sta     $040B
        pla
        ldy     #$0D
; 0x8f12
UART_Check_for_Special_Char:
        cmp     SpecialCharacters_Copy,y
        beq     UART_HandleSpacialChar_8F52
        dey
        bpl     UART_Check_for_Special_Char
        cmp     #$7F
        bne     UART_NO_DEL_received
        lda     #$5F
; 0x8f20
UART_NO_DEL_received:
        tay
        bmi     UART_Bit_FC00_RTS
        and     #$60
        beq     UART_Bit_FC00_RTS
L8F27:
        lda     $0406
        beq     L8F38
        cpy     #$61
        bcc     L8F38
        cpy     #$7B
        bcs     L8F38
        tya
        and     #$DF
        tay
L8F38:
        cpx     $0323
        bcs     UART_Bit_FC00_RTS
        tya
        jsr     L8EA5
        inc     $0325
        lda     $0325
        cmp     $0324
        bcc     L8F4F
        sta     $0324
L8F4F:
        tya
        clv
        rts

; 0x8f52
UART_HandleSpacialChar_8F52:
        tya
        asl     a
        tay
        lda     SpecialCharJumps,y
        sta     POINI_FA
        lda     SpecialCharJumps+1,y
        sta     POINI_FA+1
        jmp     (POINI_FA)

; 0x8F62
SpecialCharJumps:
        .addr   UART_HandleChar_15_8F7e
        .addr   UART_HandleChar_06_8F8c
        .addr   UART_HandleChar_5F_8F96
        .addr   UART_HandleChar_0D_8F9f
        .addr   UART_HandleChar_0A_8Fef
        .addr   UART_HandleChar_1a_904f
        .addr   UART_HandleChar_7F_9019
        .addr   UART_HandleChar_5B_90e5
        .addr   UART_HandleChar_5D_9164
        .addr   UART_HandleChar_5E_908f
        .addr   UART_HandleChar_14_906e
        .addr   UART_HandleChar_03_9079
        .addr   UART_HandleChar_5c_SLASH_9081
        .addr   UART_HandleChar_13_XOFF_9253
; 0x8f7e
UART_HandleChar_15_8F7e:
        cpx     #$03
        bne     L8F85
        jmp     UART_Bit_FC00_RTS

L8F85:
        dec     $0325
        lda     #$08
        clv
        rts

; 0x8f8c
UART_HandleChar_06_8F8c:
        cpx     $0324
        bcc     UART_HandleChar_5F_8F96
        ldy     #$20
        jmp     L8F27

; 0x8f96
UART_HandleChar_5F_8F96:
        lda     $0323,x
        and     #$7F
        tay
        jmp     L8F27

; 0x8f9f
UART_HandleChar_0D_8F9f:
        cpx     $0324
        bne     L8FA7
        jmp     UART_Handle_Rec_String_91c3

L8FA7:
        sec
        cld
        lda     $0324
        sbc     $0325
        ora     #$80
        sta     $037D
        sta     $037F
        lda     #$20
        sta     $037E
        lda     #$08
        sta     $0380
        ldy     #$07
        sty     $037C
        lda     #$C0
        ldy     #$91
        sta     L03E1
        sty     $03E2
L8FD0:
        lda     UART_Flags_03e7
        and     #$7E
        ora     #$40
        sta     UART_Flags_03e7
        ldy     #$03
        sty     $037B
        lda     $03E6
        and     #$F3
        ora     #$04
        sta     $03E6
        bit     $FC00
        jmp     StoreA_in_SerialCommandReg

; 0x8fef
UART_HandleChar_0A_8Fef:
        cpx     #$03
        bne     L8FF7
L8FF3:
        bit     $FC00
        rts

L8FF7:
        ldy     #$03
        jsr     L8FFF
        jmp     L8FD0

L8FFF:
        sec
        cld
        txa
        sbc     #$03
        ora     #$80
        sta     $037A,y
        iny
        lda     #$08
        sta     $037A,y
        iny
        sty     $037C
        ldx     #$03
        stx     $0325
        rts

; 0x9019
UART_HandleChar_7F_9019:
        ldy     #$03
        cpx     #$03
        beq     L9022
        jsr     L8FFF
L9022:
        cpx     $0324
        beq     L8FF3
        sec
        cld
        lda     $0324
        sbc     #$03
        ora     #$80
        sta     $037A,y
        sta     $037C,y
        iny
        lda     #$20
        sta     $037A,y
        iny
        iny
        lda     #$08
        sta     $037A,y
        iny
        sty     $037C
        lda     #$03
        sta     $0324
        jmp     L8FD0

; 0x904f
UART_HandleChar_1a_904f:
        cpx     $0324
        beq     L8FF3
        ldy     #$03
L9056:
        lda     $0323,x
        and     #$7F
        sta     $037A,y
        iny
        inx
        cpx     $0324
        bne     L9056
        stx     $0325
        sty     $037C
        jmp     L8FD0

; 0x906e
UART_HandleChar_14_906e:
        lda     $0323,x
        ora     #$80
L9073:
        sta     $0323,x
        jmp     UART_Bit_FC00_RTS

; 0x9079
UART_HandleChar_03_9079:
        lda     $0323,x
        and     #$7F
        jmp     L9073

; 0x9081
UART_HandleChar_5c_SLASH_9081:
        lda     #$80
        sta     $0409
        cpx     $0323
        bcc     L90A7
        bit     $FC00
        rts

; 0x908f
UART_HandleChar_5E_908f:
        lda     #$00
        sta     $0409
        txa
        tay
        jmp     L909E

L9099:
        lda     $0323,y
        bmi     L90A7
L909E:
        iny
        cpy     $0323
        bcc     L9099
        jmp     UART_Bit_FC00_RTS

L90A7:
        ldy     #$03
        lda     $0323,x
        jsr     L90D2
L90AF:
        iny
        inx
        cpx     $0323
        bcs     L90C1
        lda     $0323,x
        bmi     L90C1
        jsr     L90D2
        jmp     L90AF

L90C1:
        sty     $037C
        stx     $0325
        cpx     $0324
        bcc     L90CF
        stx     $0324
L90CF:
        jmp     L8FD0

L90D2:
        bit     $0409
        bmi     L90DE
        cpx     $0324
        bcc     L90DE
        lda     #$20
L90DE:
        jsr     L8EA5
        sta     $037A,y
        rts

; 0x90e5
UART_HandleChar_5B_90e5:
        cpx     $0323
        bcs     L910A
        txa
        tay
L90EC:
        lda     $0324,y
        and     #$7F
        sta     $0407
        lda     $0323,y
        and     #$80
        ora     $0407
        sta     $0323,y
        iny
        cpy     $0323
        bcc     L90EC
        lda     #$20
        sta     $0322,y
L910A:
        cpx     $0324
        bcc     L9113
        bit     $FC00
        rts

L9113:
        inx
        cpx     $0324
        bne     L912E
        dec     $0324
        lda     #$20
        sta     $037D
        lda     #$08
        sta     $037E
        lda     #$05
        sta     $037C
        jmp     L8FD0

L912E:
        ldy     #$03
        lda     #$81
        sta     $0408
        dex
        dec     $0324
L9139:
        lda     $0323,x
        and     #$7F
        sta     $037A,y
        iny
        inc     $0408
        inx
        cpx     $0324
        bcc     L9139
        lda     #$20
        sta     $037A,y
        iny
L9151:
        lda     $0408
        sta     $037A,y
        iny
        lda     #$08
        sta     $037A,y
        iny
        sty     $037C
        jmp     L8FD0

; 0x9164
UART_HandleChar_5D_9164:
        cpx     $0323
        bcs     L9190
        ldy     $0323
        dey
L916D:
        lda     $0323,y
        and     #$7F
        sta     $0407
        lda     $0324,y
        and     #$80
        ora     $0407
        sta     $0324,y
        dey
        cpy     $0325
        bcs     L916D
        lda     $0324,y
        and     #$80
        ora     #$20
        sta     $0324,y
L9190:
        cpx     $0324
        bcc     L9199
        bit     $FC00
        rts

L9199:
        ldy     $0324
        cpy     $0323
        bcs     L91A4
        inc     $0324
L91A4:
        ldy     #$03
        lda     #$80
        sta     $0408
L91AB:
        lda     $0323,x
        and     #$7F
        sta     $037A,y
        inc     $0408
        inx
        iny
        cpx     $0324
        bne     L91AB
        jmp     L9151

Indexed_Jumps_L91c0:
        jsr     UART_Disable_TX_Interrupt_92bf
UART_Handle_Rec_String_91c3:
        lda     UART_Flags_03e7
        and     #$3F
        sta     UART_Flags_03e7
        ldy     $0325
L91CE:
        cpy     $0323
        bcs     L91E0
        lda     $0323,y
        and     #$80
        ora     #$20
        sta     $0323,y
        iny
        bne     L91CE
L91E0:
        lda     $03E3
        ldy     $03E4
        sta     POINI_FA
        sty     POINI_FA+1
        ldy     #$46
        lda     #$01
        sta     (POINI_FA),y
        ldy     #$41
        lda     (POINI_FA),y
        tax
        iny
        lda     (POINI_FA),y
        sta     POINI_FA+1
        stx     POINI_FA
        ldy     #$02
        lda     $0325
        sta     (POINI_FA),y
        ldy     #$03
        cpy     $0325
        beq     L9217
L920A:
        lda     $0323,y
        and     #$7F
        sta     (POINI_FA),y
        iny
        cpy     $0325
        bne     L920A
L9217:
        lda     $0200
        ldy     $0201
        sta     POINI_FA
        sty     POINI_FA+1
        ldy     #$40
        lda     (POINI_FA),y
        beq     L924F
        ldx     $02D3
        cpx     $02D1
        bcs     L924F
        ldy     $0325
        cpy     #$03
        beq     L924F
        ldy     #$03
L9238:
        lda     $0323,y
        and     #$7F
        sta     $02D1,x
        iny
        inx
        cpy     $0325
        bcs     L924C
        cpx     $02D1
        bcc     L9238
L924C:
        stx     $02D3
L924F:
        bit     $FC00
        rts

; 0x9253
UART_HandleChar_13_XOFF_9253:
        lda     $0404
        eor     #$FF
        sta     $0404
        jmp     UART_Bit_FC00_RTS

L925E:
        lda     $0325
        cmp     #$03
        bne     L9285
UART_Jump_L9265:
        jsr     UART_Disable_TX_Interrupt_92bf
        lda     UART_Flags_03e7
        and     #$BE
        ora     #$80
        sta     UART_Flags_03e7
        lda     #$03
        sta     $037C
L9277:
        lda     #$65
        sta     L03E1
        lda     #$92
        sta     $03E2
        bit     $FC00
        rts

L9285:
        ldx     #$03
L9287:
        lda     $0323,x
        and     #$7F
        sta     $037A,x
        inx
        cpx     $0325
        bne     L9287
        stx     $037C
        lda     #$03
        sta     $037B
        jmp     L9277

L92A0:
        lda     $03E3
        ldy     $03E4
        sta     POINI_FA
        sty     POINI_FA+1
        ldy     #$46
        lda     #$01
        sta     (POINI_FA),y
        jsr     UART_Disable_TX_Interrupt_92bf
        lda     UART_Flags_03e7
        and     #$BF
        sta     UART_Flags_03e7
        bit     $FC00
        rts

UART_Disable_TX_Interrupt_92bf:
        lda     $03E6
        and     #$F3
        ora     #$08
        sta     $03E6
        jmp     StoreA_in_SerialCommandReg

;  Setup Terminal IRQ
ATIVOR:
        lda     Users_Installed_F421
        asl     a
        tax
        dex
        lda     Data_92e0,x
        sta     $F43A
        dex
        lda     Data_92e0,x
        sta     LF439
        rts

; 0x92e0
Data_92e0:
        .addr   Single_User_L8e05
        .addr   Two_User_L8de9
        .addr   Three_User_L8dcd
        .addr   Four_User_L8db1
        .addr   Five_User_L8d95
        .addr   Six_User_L8d79
L92EC:
        lda     $F40A
        beq     L930A
        dec     $F40A
        lda     #$0A
        sta     $F871
        inc     $F405
        lda     $F87C
        and     #$FD
        sta     $F87C
        ora     #$02
        sta     $F87C
        rts

L930A:
        lda     #$00
        sta     $F403
        lda     #$02
        sta     $F87E
        rts

; 0x9315
IRQ6_MUCISR:
        pha
        lda     VIAMUC_VIAORB
        pha
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        txa
        pha
        tya
        pha
        tsx
        lda     $0105,x
        and     #$10
        beq     RegularInterupt_9332
        cli
        jmp     BreakInterrupt

; 0x9332
RegularInterupt_9332:
        lda     MUC6522_IFR
        bpl     L9341
        and     #$02
        beq     L9341
        lda     VIAMUC_VIAORA
        jsr     L9363
L9341:
        lda     MUC6522_IFR
        bpl     L9360
        and     #$20
        beq     L9360
        lda     MUC6522_T2L
        lda     #$00
        sta     $0213
        tsx
        stx     JCSREG_0212
        lda     Bank_Shadow_F438
        sta     $0220
        cli
        jmp     L83B7

L9360:
        jmp     RegularInterupt_89b9

L9363:
        inc     $F447
        lda     IPADR_P1_F4AF
        beq     L9375
        dec     IPADR_P1_F4AF
        bne     L9375
        lda     #$00
        sta     $F45F
L9375:
        dec     C3TI20_F436
        beq     L938A
        lda     C3TI20_F436
        cmp     #$19
        bne     L9387
        inc     $F43B
        inc     $F446
L9387:
        jmp     L9401

L938A:
        lda     #$32
        sta     C3TI20_F436
        inc     $F43B
        inc     $F446
        inc     $F445
        dec     $F404
        ldx     #$05
L939D:
        dec     $F451,x
        dex
        bpl     L939D
        cld
        ldx     #$11
L93A6:
        lda     C3DATE_F424,x
        sta     $F78C,x
        dex
        bpl     L93A6
        ldx     #$07
        jsr     L9411
        bcc     L9401
        ldx     #$04
        jsr     L9411
        bcc     L9401
        clc
        lda     $F42F
        adc     #$01
        sta     $F42F
        cmp     #$3A
        beq     L9402
        cmp     #$34
        bne     L9401
        lda     C3TIME_F42E
        cmp     #$32
        bne     L9401
        lda     #$30
        sta     C3TIME_F42E
        sta     $F42F
        clc
        lda     $F42D
        adc     #$01
        sta     $F42D
        cmp     #$3A
        bne     L9401
        lda     #$30
        sta     $F42D
        clc
        lda     $F42C
        adc     #$01
        sta     $F42C
        cmp     #$3A
        bne     L9401
        lda     #$30
        sta     $F42C
L9401:
        rts

L9402:
        lda     #$30
        sta     $F42F
        clc
        lda     C3TIME_F42E
        adc     #$01
        sta     C3TIME_F42E
        rts

L9411:
        clc
        lda     C3TIME_F42E,x
        adc     #$01
        sta     C3TIME_F42E,x
        cmp     #$3A
        bne     L9438
        lda     #$30
        sta     C3TIME_F42E,x
        dex
        clc
        lda     C3TIME_F42E,x
        adc     #$01
        sta     C3TIME_F42E,x
        cmp     #$36
        bne     L9438
        lda     #$30
        sta     C3TIME_F42E,x
        sec
        rts

L9438:
        clc
        rts

; 0x943A - INIT_ASYS
AAHINI:
        clc
        sec
        lda     $020B
        sbc     #$0F
        sta     $0500
        lda     #$00
        sta     $04F0
        sta     $04FA
        sta     $04EF
        sta     $04EE
        rts

L9453:
        jsr     L969A
        lda     $04FA
        beq     L9467
        lda     #$31
L945D:
        ldx     StackTail_211
        sta     $0104,x
        bit     $FC00
        rts

L9467:
        ldx     StackTail_211
        lda     $0102,x
        bne     L9471
        lda     #$01
L9471:
        sta     $04FF
        inc     $04FD
        lda     #$02
        sta     $04FE
        ldx     StackTail_211
        lda     $0104,x
        sta     $F8
        lda     $0101,x
        sta     $F9
        ldy     #$00
        lda     ($F8),y
        sta     $04F8
        iny
        lda     ($F8),y
        sta     $04F9
        ldy     #$02
        lda     ($F8),y
        sta     $04F6
        iny
        lda     ($F8),y
        sta     $04F7
        lda     #$FF
        sta     $04FA
        ldx     $0500
        lda     #$9E
        sta     Serial_0U0_CommandReg,x
        lda     #$69
        sta     Serial_0U0_StatusReg,x
        sta     $04FB
        lda     #$5F
        ldy     #$F4
        jsr     _Floppy_8039_
        sei
        jsr     Reset_PB6
        jsr     Write_Status_Reg_Serial1_94c9
        cli
        clv
        rts

; 0x94c9
Write_Status_Reg_Serial1_94c9:
        lda     #$00
        sta     $04EF
        lda     #$40
        sta     $04F0
        lda     $04FB
        and     #$F3
        ora     #$04
        sta     $04FB
        ldx     $0500
        sta     Serial_0U0_StatusReg,x
        ldx     $0207
        lda     $04FF
        sta     $F450,x
        lda     #$FF
        sta     $04EE
        rts

L94F2:
        jsr     L969A
        lda     $04FA
        bne     L94FF
        lda     #$32
L94FC:
        jmp     L945D

L94FF:
        cmp     #$FF
        bne     L9511
        lda     $04F0
        lsr     a
        bcc     L950D
        lda     #$31
        bne     L94FC
L950D:
        brk
        sta     POINI_FA
        .byte   $04
L9511:
        lda     $04FA
        ldx     #$00
        stx     $04FA
        cmp     #$01
        bne     L94FC
        clv
        rts

L951F:
        jsr     L969A
        lda     $04FA
        beq     L952C
        lda     #$31
        jmp     L945D

L952C:
        ldx     StackTail_211
        lda     $0104,x
        sta     $F8
        sta     $0502
        lda     $0101,x
        sta     $F9
        sta     $0503
        ldy     #$02
        jsr     CLR_where_F8_points_at_plus_y
        ldy     #$06
        jsr     CLR_where_F8_points_at_plus_y
        ldy     #$0A
        jsr     CLR_where_F8_points_at_plus_y
        ldx     $0500
        sta     Serial_0U0_DataReg,x
        ldy     #$0E
        lda     ($F8),y
        ora     #$10
        sta     Serial_0U0_CommandReg,x
        iny
        lda     ($F8),y
        and     #$E0
        ora     #$09
        sta     Serial_0U0_StatusReg,x
        sta     $04FB
        lda     Serial_0U0_DataReg_Minus1,x
        lda     Serial_0U0_DataReg_Minus1,x
        lda     Serial_0U0_DataReg_Minus1,x
        lda     #$01
        sta     $04F0
        lda     #$FF
        sta     $04FA
        lda     #$00
        sta     $04EE
        sta     $0501
        clv
        rts

L9587:
        jsr     L969A
        lda     $04FA
        bne     L9594
        lda     #$32
L9591:
        jmp     L945D

L9594:
        lda     $04F0
        lsr     a
        bcs     L959E
        lda     #$31
        bne     L9591
L959E:
        php
        sei
        lda     #$00
        sta     $04F0
        sta     $04FA
        lda     $04FB
        and     #$F1
        ora     #$0A
        sta     $04FB
        ldx     $0500
        sta     Serial_0U0_StatusReg,x
        plp
        clv
        rts

L95BB:
        lda     $04F0
        lsr     a
        bcc     L95C4
        jmp     L959E

L95C4:
        rts

L95C5:
        jsr     L969A
        lda     $04F0
        lsr     a
        bcs     L95D3
        lda     #$32
L95D0:
        jmp     L945D

L95D3:
        php
        sei
        jsr     L96A7
        ldx     #$04
        jsr     L9674
        bvs     L95F5
        pha
        jsr     L96A7
        ldx     #$00
        jsr     L9674
        ldx     StackTail_211
        sta     $0104,x
        pla
        sta     $0102,x
        plp
        clv
        rts

L95F5:
        plp
        lda     #$35
        bne     L95D0
L95FA:
        jsr     L969A
        lda     $04F0
        lsr     a
        bcs     L9608
        lda     #$32
L9605:
        jmp     L945D

L9608:
        php
        sei
        jsr     L96A7
        lda     $0102,x
        ldx     #$08
        jsr     L963E
        bvc     L961C
        plp
        lda     #$34
        bne     L9605
L961C:
        lda     $04FB
        and     #$08
        beq     L9633
        lda     $04FB
        and     #$F3
        ora     #$04
        sta     $04FB
        ldx     $0500
        sta     Serial_0U0_StatusReg,x
L9633:
        plp
        clv
        rts

; 0x9636
CLR_where_F8_points_at_plus_y:
        lda     #$00
        sta     ($F8),y
        iny
        sta     ($F8),y
        rts

L963E:
        php
        sei
        pha
        txa
        tay
        iny
        iny
        iny
        clc
        cld
        lda     (POINI_FA),y
        adc     #$01
        dey
        cmp     (POINI_FA),y
        beq     L9661
        iny
        sta     (POINI_FA),y
        pha
        jsr     L9667
        pla
        tay
        dey
        pla
        sta     (POINI_FA),y
        plp
        clv
        rts

L9661:
        pla
        plp
        bit     $FC00
        rts

L9667:
        txa
        tay
        lda     (POINI_FA),y
        tax
        iny
        lda     (POINI_FA),y
        sta     POINI_FA+1
        stx     POINI_FA
        rts

L9674:
        php
        sei
        txa
        tay
        iny
        iny
        lda     (POINI_FA),y
        iny
        cmp     (POINI_FA),y
        beq     L9695
        dey
        lda     (POINI_FA),y
        pha
        clc
        cld
        adc     #$01
        sta     (POINI_FA),y
        jsr     L9667
        pla
        tay
        lda     (POINI_FA),y
        plp
        clv
        rts

L9695:
        plp
        bit     $FC00
        rts

L969A:
        bit     $0207
        bvc     L96A6
        pla
        pla
        lda     #$12
        jmp     L945D

L96A6:
        rts

L96A7:
        ldx     StackTail_211
        lda     $0104,x
        sta     POINI_FA
        lda     $0101,x
        sta     POINI_FA+1
        rts

L96B5:
        lda     $04EE
        beq     L96DE
        ldx     $0207
        lda     $F450,x
        bpl     L96DE
        dec     $04FE
        bpl     L96DB
        lda     #$00
        sta     $04EE
        sta     $04F0
        sta     $F45F
        jsr     Set_PB6
        lda     #$30
        sta     $04FA
        rts

L96DB:
        jsr     Write_Status_Reg_Serial1_94c9
L96DE:
        rts

; 0x96DF
BreakInterupt_96DF:
        cli
        jmp     BreakInterrupt

; 0x96e3
IRQ2_ISR_USR14_Serial1:
        pha
        lda     UserData_1
        sta     $F3EA
        lda     UserData_4
        sta     $F3EB
L96F0:
        lda     VIAMUC_VIAORB
        pha
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        txa
        pha
        tya
        pha
        tsx
        lda     $0105,x
        and     #$10
        bne     BreakInterupt_96DF
        tsx
        lda     JCSREG_0212
        sta     $0217
        stx     JCSREG_0212
        sta     NMI_DISABLE
        lda     $F3EA
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        ldx     $0500
        lda     Serial_0U0_DataReg,x
        bpl     L9730
        jsr     L9784
        jmp     L974B

L9730:
        sta     NMI_DISABLE
        lda     $F3EB
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        ldx     $0500
        lda     Serial_0U0_DataReg,x
        bpl     L974B
        jsr     L9784
L974B:
        sta     NMI_DISABLE
        lda     $F437
        sta     OUTUMAP_F8E8
        ldx     JCSREG_0212
        txs
        sta     NMI_ENABLE
        lda     $0217
        sta     JCSREG_0212
        jmp     RegularInterupt_89b9

; 0x9764
IRQ3_ISRUSR2_Serial1:
        pha
        lda     UserData_2
        sta     $F3EA
        lda     UserData_5
        sta     $F3EB
        jmp     L96F0

; 0x9774
IRQ4_ISRUSR3_Serial1:
        pha
        lda     UserData_3
        sta     $F3EA
        lda     UserData_6
        sta     $F3EB
        jmp     L96F0

L9784:
        sta     $04FC
        and     #$08
        beq     L97F5
        ldx     $0500
        lda     Serial_0U0_DataReg_Minus1,x
        sta     $04F3
        lda     $04F0
        lsr     a
        bcs     L97D0
        bit     $04F0
        bpl     L97AB
        lda     $04FC
        and     #$07
        beq     L97AE
        lda     #$00
        sta     $04F0
L97AB:
        jmp     L982F

L97AE:
        lda     $04EF
        asl     a
        tax
        lda     Jump_Table_99A0,x
        sta     L04F4
        inx
        lda     Jump_Table_99A0,x
        sta     $04F5
        lda     $04F6
        sta     POINI_FA
        lda     $04F7
        sta     POINI_FA+1
        lda     $04F3
        jmp     (L04F4)

L97D0:
        lda     $04FC
        and     #$07
        ora     $0501
        jsr     L9855
        ldx     #$04
        jsr     L963E
        lda     #$00
        bvc     L97E6
        lda     #$08
L97E6:
        sta     $0501
        lda     $04F3
        jsr     L9855
        ldx     #$00
        jsr     L963E
        rts

L97F5:
        lda     $04FC
        and     #$10
        beq     L982F
        lda     $04F0
        lsr     a
        bcs     L9833
        bit     $04F0
        bvc     L982F
        lda     $04EF
        asl     a
        tax
        lda     Jump_Table_98E3,x
        sta     L04F4
        inx
        lda     Jump_Table_98E3,x
        sta     $04F5
        lda     $04F8
        sta     POINI_FA
        lda     $04F9
        sta     POINI_FA+1
        clv
        jsr     L9830
        bvs     L982F
        ldx     $0500
        sta     Serial_0U0_DataReg_Minus1,x
L982F:
        rts

L9830:
        jmp     (L04F4)

L9833:
        jsr     L9855
        ldx     #$08
        jsr     L9674
        bvs     L9844
        ldx     $0500
        sta     Serial_0U0_DataReg_Minus1,x
        rts

L9844:
        lda     $04FB
        and     #$F3
        ora     #$08
        sta     $04FB
        ldx     $0500
        sta     Serial_0U0_StatusReg,x
        rts

L9855:
        pha
        lda     $0502
        sta     POINI_FA
        lda     $0503
        sta     POINI_FA+1
        pla
        rts

L9862:
        lda     #$02
        inc     $04EF
        rts

L9868:
        lda     $04FD
        sta     $04F1
        inc     $04EF
        rts

L9872:
        ldy     #$02
        lda     (POINI_FA),y
        cld
        sec
        sbc     #$03
        sta     $04F2
        clc
        adc     $04F1
        sta     $04F1
        ldy     #$01
        lda     #$03
        sta     (POINI_FA),y
        lda     $04F2
        bne     L9892
        inc     $04EF
L9892:
        inc     $04EF
        clv
        rts

L9897:
        ldy     #$01
        lda     (POINI_FA),y
        tay
        lda     (POINI_FA),y
        pha
        clc
        cld
        adc     $04F1
        sta     $04F1
        iny
        tya
        ldy     #$01
        sta     (POINI_FA),y
        pla
        clv
        dec     $04F2
        bne     L98B7
        inc     $04EF
L98B7:
        rts

L98B8:
        lda     $04F1
        inc     $04EF
        rts

L98BF:
        lda     #$03
        inc     $04EF
        rts

        lda     $04FB
        and     #$F3
        ora     #$08
        sta     $04FB
        ldx     $0500
        sta     Serial_0U0_StatusReg,x
        lda     #$00
        sta     $04EF
        lda     #$80
        sta     $04F0
        bit     $FC00
        rts

; 0x98E3
Jump_Table_98E3:
        .addr   L9862
        .addr   L9868
        .addr   L9872
        .addr   L9897
        .addr   L98B8
        .addr   L98BF
        cmp     $98
L98F1:
        cmp     #$02
L98F3:
        bne     L98F9
        inc     $04EF
        rts

L98F9:
        lda     #$00
        sta     $04F0
        rts

L98FF:
        sta     $04F1
        cmp     $04FD
        jmp     L98F3

L9908:
        sta     $04F2
        cld
        clc
        adc     $04F1
        sta     $04F1
        ldy     #$02
        lda     #$03
        sta     (POINI_FA),y
        ldy     #$01
        lda     $04F2
        sta     (POINI_FA),y
        bne     L9925
        inc     $04EF
L9925:
        inc     $04EF
        rts

L9929:
        sta     $04F3
        clc
        cld
        adc     $04F1
        sta     $04F1
        ldy     #$02
        lda     (POINI_FA),y
        ldy     #$00
        cmp     (POINI_FA),y
        bcs     L994A
        tay
        lda     $04F3
        sta     (POINI_FA),y
        iny
        tya
        ldy     #$02
        sta     (POINI_FA),y
L994A:
        dec     $04F2
        bne     L9952
        inc     $04EF
L9952:
        rts

L9953:
        cmp     $04F1
        jmp     L98F3

L9959:
        cmp     #$03
        beq     L9963
        lda     #$00
        sta     $04F0
        rts

L9963:
        lda     #$00
        sta     $04F0
        sta     $F45F
        sta     $04EE
        jsr     Set_PB6
        ldy     #$01
        lda     (POINI_FA),y
        bne     L997E
        inc     $04FD
        sta     $04F0
        rts

L997E:
        sta     $04F3
        lda     #$03
        sta     (POINI_FA),y
        lda     $04F3
        clc
        cld
        adc     #$03
        ldy     #$00
        cmp     (POINI_FA),y
        beq     L999A
        bcc     L999A
        lda     #$33
        sta     $04FA
        rts

L999A:
        lda     #$01
        sta     $04FA
        rts

; 0x99A0
Jump_Table_99A0:
        .addr   L98F1
        .addr   L98FF
        .addr   L9908
        .addr   L9929
        .addr   L9953
        .addr   L9959
; 0x99AC
Set_PB6:
        lda     VIAMUC_VIAORB
        ora     #$40
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        rts

; 0x99B8
Reset_PB6:
        lda     VIAMUC_VIAORB
        and     #$BF
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        rts

; 0x99c4
OUTVOR:
        brk
        .byte   $A0,$7E,$02
        brk
        .byte   $27
        brk
        .byte   $2B
        rts

; 0x99cd
OUTAUS:
        ldx     #$45
        brk
        .byte   $8A,$27,$02
        rts

L99D4:
        ldy     #$48
        clv
        lda     ($F8),y
        bmi     L99FD
        cmp     #$05
        bcc     L99E5
        lda     #$F3
        bit     $FC00
        rts

L99E5:
        lda     ($F8),y
        sta     $04BF
        asl     a
        sta     $04C0
        asl     a
        asl     a
        sta     $04C1
        ldx     $04BF
        lda     $04BA,x
        cmp     #$41
        beq     L9A03
L99FD:
        lda     #$21
        bit     $FC00
        rts

L9A03:
        clv
        rts

; 0x9A05
_CALL_:
        sta     JumpAddrL_221
        sty     JumpAddrH_222
        lda     Bank_Shadow_F438
        pha
        lda     Bank_Switch_Values_FC16,x
        php
        sei
        sta     OUTMAP_F8E0
        sta     Bank_Shadow_F438
        plp
        jsr     L9A2F
        sta     JumpAddrL_221
        pla
        php
        sei
        sta     OUTMAP_F8E0
        sta     Bank_Shadow_F438
        plp
        lda     JumpAddrL_221
        rts

L9A2F:
        jmp     (JumpAddrL_221)

; 0x9A32
_Floppy_8039_:
        sta     POIN_F0
        sty     POIN_F0+1
L9A36:
        ldy     #$00
        sei
        lda     (POIN_F0),y
        beq     L9A50
        cli
        lda     POIN_F0+1
        pha
        tay
        lda     POIN_F0
        pha
        brk
        ora     $68
        sta     POIN_F0
        pla
        sta     POIN_F0+1
        jmp     L9A36

L9A50:
        lda     #$FF
        sta     (POIN_F0),y
        cli
        rts

; 0x9A56
_GetNextChar_:
        lda     Bank_Shadow_F438
        php
        sei
        pha
        lda     BankTemp_22D
        sta     OUTMAP_F8E0
        lda     ($F2),y
        sta     $022E
        pla
        sta     OUTMAP_F8E0
        plp
        lda     $022E
        rts

_DISMOUNT_:
        stx     $04D1
        sta     $04BF
        asl     a
        sta     $04C0
        asl     a
        asl     a
        sta     $04C1
        ldx     $04BF
        lda     $04BA,x
        cmp     #$20
        bne     L9A8E
        lda     #$21
        jmp     L9B76

L9A8E:
        cmp     #$2A
        bne     L9A95
        jmp     L9B80

L9A95:
        ldx     #$00
        cld
L9A98:
        lda     $F500,x
        beq     L9ABC
        lda     $F501,x
        cmp     $0207
        bne     L9ABC
        lda     $F502
        cmp     $04BF
        bne     L9ABC
        lda     $04D1
        bne     L9AB7
        lda     #$D3
        jmp     L9B76

L9AB7:
        lda     #$00
        sta     $F500,x
L9ABC:
        clc
        txa
        adc     #$06
        tax
        cmp     #$FC
        bcc     L9A98
        ldx     $04BF
        lda     #$20
        sta     $04BA,x
        ldx     $04C0
        lda     $F755,x
        sta     POINC2_F6
        lda     $F756,x
        sta     POINC2_F6+1
        ldy     #$27
        ldx     #$07
L9ADE:
        lda     $04A0,x
        sta     (POINC2_F6),y
        dey
        dex
        bpl     L9ADE
        ldy     #$28
        ldx     #$02
L9AEB:
        lda     C3DATE_F424,x
        asl     a
        asl     a
        asl     a
        asl     a
        sta     FunctionByte_afterBRK_728
        inx
        lda     C3DATE_F424,x
        and     #$0F
        ora     FunctionByte_afterBRK_728
        sta     (POINC2_F6),y
        iny
        inx
        inx
        cpx     #$0A
        bcc     L9AEB
        ldx     $04BF
        dec     $F750,x
        lda     $F750,x
        bne     L9B6E
        ldy     $04C1
        lda     #$20
        sta     $F700,y
        lda     $F497,x
        bne     L9B2C
        lda     #$1B
        ldy     #$A0
        ldx     #$01
        jsr     _CALL_
        cmp     #$01
        bne     L9B76
L9B2C:
        lda     #$5F
        ldy     #$F4
        jsr     _Floppy_8039_
        sei
        lda     #$80
        sta     Floppy_MotorSel
        ldx     $04BF
        ora     Floppy_DriveSelect,x
        sta     Floppy_MotorSel
        and     #$F6
        sta     Floppy_MotorSel
        cli
        lda     #$00
        sta     $F45F
        lda     $0207
        sta     $0765
        lda     #$08
        sta     $0207
        ldx     $04C0
        lda     $F756,x
        tay
        lda     $F755,x
        ldx     #$05
        brk
        and     ($AE),y
        adc     $07
        stx     $0207
        bvs     L9B76
L9B6E:
        lda     #$00
        sta     $F477
        clv
        clc
        rts

L9B76:
        bit     $FC00
        ldx     #$00
        stx     $F477
        clc
        rts

L9B80:
        lda     #$20
        sta     $04BA,x
        lda     #$00
        sta     $F750,x
        ldx     $04C1
        lda     #$20
        sta     $F700,x
        jmp     L9B2C

; 0x9b95 Check_Checksum
APRTES:
        sta     POIN_F0+1
        stx     $F3
        sty     POINC1_F4
        lda     #$00
        sta     POIN_F0
        lda     #$00
        sta     POINC2_F6
        ldy     #$00
L9BA5:
        lda     (POIN_F0),y
        clc
        cld
        adc     POINC2_F6
        sta     POINC2_F6
        sta     NMI_DISABLE
        inc     POIN_F0
        bne     L9BB6
        inc     POIN_F0+1
L9BB6:
        lda     $F3
        cmp     POIN_F0+1
        bne     L9BA5
        lda     POINC2_F6
        bne     Checksum_wrong
        rts

; 0x9bc1
Checksum_wrong:
        lda     POINC1_F4
        sta     POIN_F0
L9BC5:
        sta     LED_IDLE_OFF
        jsr     Waitloop_1
        sta     LED_IDLE_ON
        jsr     Waitloop_1
        sta     LED_IDLE_OFF
        dec     POIN_F0
        bpl     L9BC5
        jsr     Waitloop_1
        jsr     Waitloop_1
        jsr     Waitloop_1
        jsr     Waitloop_1
        jmp     Checksum_wrong

; 0x9be7
Waitloop_1:
        ldx     #$00
        ldy     #$00
L9BEB:
        lda     #$00
        sta     NMI_DISABLE
        dex
        bne     L9BEB
        dey
        bne     L9BEB
        rts

; 0x9bf7
_Logon_:
        jsr     OUTVOR
; 26 marks beginning of text, before Username write 2x space (20, A0) ;  USERNAME LOGGED ON ERTL (265) WITH 32K ON YEAR-MM_DD AT  
comm:
        brk
        .byte   $26,$20,$A0
        ldx     #$08
; Get Username from 0x04A0, B5 - LDA zpg,X
comm:
        brk
        .byte   $B5,$A0,$04
        brk
        .byte   $26
        .byte   " LOGGED ON"

; A0 is just a SPACE with Bit7 set, A2 - no clue (LDX # but needs two byte)
comm:
        .byte   $A0
        .byte   $A2
; Get Servername from 0xFF07, BD - LDA abs,X
comm:
        brk
L9C13:
        lda     ServerName,x
        bpl     L9C1A
        lda     #$20
L9C1A:
        brk
        .byte   $21
        inx
        cpx     #$08
        bcc     L9C13
; Get Firmware Version from 0xFF0F, A8 is '(' with Bit 7 set, AC - LDY abs
comm:
        brk
        .byte   $26,$A8
        ldy     FirmwareVersion1
        lda     FirmwareVersion2
        brk
        .byte   $25
        brk
; 26 - start of Text, A0 - Space w. Bit 7
comm:
        .byte   $26
        .byte   ") WITH"
        .byte   $A0,$A9
        brk
        .byte   $85,$F4
        lda     #$08
        sta     POINC1_F4+1
        lda     #$02
        sta     $023A
L9C42:
        ldx     #$04
        ldy     #$00
L9C46:
        lda     (POINC1_F4),y
        sta     POINC2_F6
        lda     #$FF
        sta     (POINC1_F4),y
        lda     (POINC1_F4),y
        cmp     #$FF
        bne     L9C7F
        lda     #$00
        sta     (POINC1_F4),y
        lda     (POINC1_F4),y
        bne     L9C7F
        lda     $04A0
        cmp     #$40
        bne     L9C67
        lda     POINC2_F6
        sta     (POINC1_F4),y
L9C67:
        iny
        bne     L9C46
        inc     POINC1_F4+1
        dex
        bne     L9C46
        clc
        sed
        lda     $023A
        adc     #$01
        sta     $023A
        lda     POINC1_F4+1
        cmp     #$80
        bne     L9C42
L9C7F:
        lda     POINC1_F4+1
        and     #$F8
        sta     $0216
        lda     $023A
        brk
        .byte   $23
; 26 - start of Text, A0 - Space w. Bit 7
comm:
        brk
        .byte   $26
        .byte   "K ON"
        .byte   $A0
; 29 - write date
comm:
        brk
        .byte   $29
; 26 - start of Text, A0 - Space w. Bit 7
comm:
        brk
        .byte   $26
        .byte   " AT"
        .byte   $A0
; 28 - write time
comm:
        brk
        .byte   $28
        jsr     OUTAUS
        inc     C3LGNZ_F43C_USERS
        lda     $04A0
        cmp     #$40
        beq     L9CB4
        ldx     #$00
        txa
L9CAC:
        sta     a:_ZERO_,x
        inx
        cpx     #$F0
        bcc     L9CAC
L9CB4:
        lda     #$40
        ldx     $FF11
        cpx     #$FF
        beq     L9CBE
        txa
L9CBE:
        sta     $04AB
        lda     #$00
        sta     $0247
        rts

L9CC7:
        ldx     #$00
        ldy     #$00
L9CCB:
        sei
        lda     #$8D
        sta     L0251,y
        lda     Set_Pins_nB_HB,x
        sta     $0252,y
        lda     Set_Pins_nB_LB,x
        sta     $0253,y
        lda     #$60
        sta     $0254,y
        cli
        iny
        iny
        iny
        inx
        inx
        cpx     #$08
        bcc     L9CCB
        ldx     #$03
L9CEE:
        lda     MY3MAP_F448,x
        cmp     $0207
        bne     L9D11
        lda     #$00
        sta     MY3MAP_F448,x
        lda     $F44C,x
        cmp     $0216
        bcs     L9D11
        sta     $0216
        ldy     #$24
        cmp     #$30
        bne     L9D0E
        ldy     #$12
L9D0E:
        sty     $023A
L9D11:
        dex
        bpl     L9CEE
        rts

; 0x9d15
Check_RAM:
        lda     #$7F
        sta     OUTUMAP_F8E8
        sta     Pin_6B_High
        sta     Pin_5B_High
        sta     Pin_4B_High
        sta     Pin_3B_High
        ldx     #$00
L9D28:
        sta     Pin_6B_Low,x
        ldy     #$30
        lda     #$00
        sta     $3000
        lda     $3000
        bne     L9D43
        lda     #$FF
        sta     $3000
        lda     $3000
        cmp     #$FF
        beq     L9D5D
L9D43:
        ldy     #$60
        lda     #$00
        sta     $6000
        lda     $6000
        bne     L9D5B
        lda     #$FF
        sta     $6000
        lda     $6000
        cmp     #$FF
        beq     L9D5D
L9D5B:
        ldy     #$FF
L9D5D:
        tya
        sta     $F44C,x
        sta     Pin_6B_High,x
        inx
        cpx     #$04
        bcc     L9D28
        lda     #$7E
        sta     OUTUMAP_F8E8
        rts

L9D6F:
        pha
        txa
        pha
        lda     $05CF
        sta     InterruptControl_Reg
        lda     $05D0
        sta     $FF
        sei
        ldx     StackTail_211
        lda     $0103,x
        ora     #$CF
        and     VIAMUC_VIAORB
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        cli
        pla
        tax
        pla
        jsr     L9DB3
        pha
        sei
        lda     VIAMUC_VIAORB
        ora     #$30
        sta     PORT_B_Shadow_f3e8
        sta     VIAMUC_VIAORB
        cli
        sei
        lda     Bank_Switch_Values_FC16
        sta     OUTMAP_F8E0
        sta     Bank_Shadow_F438
        sta     BankTemp_22D
        cli
        pla
        rts

L9DB3:
        jmp     (InterruptControl_Reg)

        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
        .byte   $FF
; 0x9fff
Checksum:
        .byte   $69
