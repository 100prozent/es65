#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, sys, argparse, os, string, math, glob
from pathlib import Path
#from collections import Counter
import hashlib
BLOCK_SIZE = 65536 # The size of each read from the file

def get_hash(file):
	file_hash = hashlib.sha256() # Create the hash object, can use something other than `.sha256()` if you wish
	#print("Hash File: ", file)
	with open(file, 'rb') as f: # Open the file to read it's bytes
		fb = f.read(BLOCK_SIZE) # Read from the file. Take in the amount declared above
		while len(fb) > 0: # While there is still data being read from the file
			file_hash.update(fb) # Update the hash
			fb = f.read(BLOCK_SIZE) # Read the next block from the file

	return (file_hash.hexdigest()) # Get the hexadecimal digest of the hash

class es65_Batchconvert(object):
	def process_command_line(self):

		##
		## Parse command line
		##
	
		parser = argparse.ArgumentParser(description = 'Searches folder, collects all files and compares them and deletes doubles')

		parser.add_argument(
				'-v', '--verbose',
				action = 'count',
				help = 'increase logging level')

		parser.add_argument(
				'-w', '--overwrite',
				action = 'store_true',
				help = 'overwrites existing file')

		subparsers = parser.add_subparsers(
				dest = 'command',
				title = 'Ostrich operations',
				description = '(See "%(prog)s COMMAND -h" for more info)',
				help = '')
	
		subparser = subparsers.add_parser(
				'search',
				help = 'Searches for files and compares them')

		subparser.add_argument(
				'dir',
				nargs = '?',
				#type = _parse_output_file,
				#default = sys.stdout.buffer,
				help = 'input directory')

		args = parser.parse_args()
	
		if not args.verbose:
			level = logging.ERROR
		elif args.verbose == 1:
			level = logging.INFO
		else:
			level = logging.DEBUG
		logging.basicConfig(stream = sys.stderr, level = level, format = '%(message)s')

		##
		## Handle command
		##
		
		logging.info('Initialized')
		
		if args.command == 'search':
			self.search_dir(args.dir)

	def search_dir(self, d):
		print("Searching:" , d)
		dic = {}
		dic["Name"] = {}
		dic["Path"] = {}
		dic["Hash"] = {}
		dic["Number"] = {}
		nr = 0
		#list_of_files = sorted( filter( os.path.isfile,
        #                glob.glob(d + '/**/*', recursive=True)))
		#for root, dirs, files in os.walk(d):
		#list_of_files = [f for f in glob.glob(d + r'\**\*' , recursive=True)]
		list_of_files = glob.glob(d + "/**/*" , recursive=True)
		#print("Hash", nr)
		for fn in list_of_files:
		#for fn in glob.iglob(d + '/**', recursive=True):
			path, tail = os.path.split(fn)
			if path != d:
				dic["Name"][nr] = tail
				dic["Path"][nr] = path
				#dic["Hash"][nr] = 0
				dic["Hash"][nr] = get_hash(path + "/" + tail)
				dic["Number"][nr] = nr
				nr += 1

		#for i in range(nr):
		dup = 0
		for i in (dic["Number"]):
			for j in (dic["Number"]):
				#print("Searching for file: %s nr: %d" % (dic["Name"][i], i))
				if (dic["Hash"][i] == dic["Hash"][j] and i != j and not os.path.islink(dic["Path"][i] + "/" + dic["Name"][i])):
					p = (dic["Path"][i]).rsplit('/', 1)[-1]
					#print(p)
					#print("Found duplicte")
					os.remove(dic["Path"][j] + "/" + dic["Name"][j])
					os.symlink("../" + p + "/" + dic["Name"][i], 
					#os.symlink(dic["Path"][i] + dic["Name"][i], 
						dic["Path"][j] + "/" + dic["Name"][j])
					print("File %s from dir: %s in dir: %s dupNr: %s" % 
							(dic["Name"][i], dic["Path"][i], dic["Path"][j],  dup))
					dup += 1
			#nr += 1

if __name__ == "__main__":
	es65_Batchconvert().process_command_line()