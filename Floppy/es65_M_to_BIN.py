#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, sys, argparse, os, string, math

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result

def bytes_to_int_r(bytes):
    result = 0
    for b in reversed(bytes):
        result = result * 256 + int(b)
    return result

def number_to_linestart(number):
		return ':{:02X}'.format(number)

def number_to_address(number):
    return ('{:04X}'.format(number))

def number_to_hex_word(number):
    return ("0" + '{:02X}'.format(number))[-2:]

def convert_line(l):
	line = []
	for i in l:
		line += bytearray(number_to_hex_word(i), "ascii")
	return line

def checksum(l):
	#cs = 0x33
	cs = len(l)-2
	for i in l:
		cs += int(i)
		#print("Checksum: %x %x" % (int(i), (cs&0xFF)))
	cs = (cs&0xFF)
	csi = 1+(~(cs&0xFF)&0xFF)
	#print("Checksum: %x %x" % (cs, csi))
	return bytearray(number_to_hex_word(csi), "ascii")

def parse_es65_binfile(f, FILE, fmt):
	h1 = bytes_to_int(f[0:1])
	h2 = bytes_to_int(f[1:2])
	h3 = bytes_to_int(f[2:3])
	h4 = bytes_to_int(f[3:4])
	print("Header: %02x %02x %02x %02x Filelength: %d" % (h1, h2, h3, h4, len(f)))
	lstart = 4
	endaddress = 0
	if fmt == 'b':
		while lstart < len(f):
			linelen = bytes_to_int(f[lstart:lstart + 1])
			if linelen > 0:
				address = bytes_to_int_r(f[lstart+1:lstart+3])
				if endaddress+1 != address: 
					print("Startaddress: 0x%04X" % (address))
				endaddress = address + linelen - 3
				#print("Linelen %d Address %x" % (linelen, address))
				#print("Address %x %x" % (bytes_to_int(f[lstart+1:lstart+2]), bytes_to_int(f[lstart+2:lstart + 3])))
				FILE += f[lstart + 3: lstart + linelen + 1]
				lstart += linelen + 1
			else:
				lstart += 1
	elif fmt == 'i':
		while lstart < len(f):
			line = []
			linelen = bytes_to_int(f[lstart:lstart + 1])
			if linelen > 0:
				address = bytes_to_int_r(f[lstart+1:lstart+3])
				if endaddress+1 != address: 
					print("Startaddress: 0x%04X" % address)
				endaddress = address + linelen - 3
				#print("Linelen %d Address %x" % (linelen, address))
				#print("Address %x %x" % (bytes_to_int(f[lstart+1:lstart+2]), bytes_to_int(f[lstart+2:lstart + 3])))
				#FILE += bytearray(':', ascii)
				line += bytearray(number_to_linestart(linelen-2), "ascii")
				line += bytearray(number_to_address(address), "ascii")
				line += bytearray('00', "ascii") # Record Type
				line += convert_line(f[lstart + 3: lstart + linelen + 1])
				cs = checksum(f[lstart+1:lstart + linelen + 1]) #without length
				line += cs
				line += bytearray('\r\n', "ascii")
				lstart += linelen + 1
			else:
				lstart += 1
			FILE += line
		FILE += bytearray(':00000001FF', "ascii")

class ES65_parse_M:
	def process_command_line(self):

		##
		## Parse command line
		##

		parser = argparse.ArgumentParser(description = 'Reads es65 (Bin)Files (E. and M.) and converts to Intel HEX or BIN - v0.1')
		parser.add_argument(
				'-v', '--verbose',
				action = 'count',
				help = 'increase logging level')

		parser.add_argument(
				'-w', '--overwrite',
				action = 'store_true',
				help = 'overwrites existing file')

		parser.add_argument(
				'-d', '--delete',
				action = 'store_true',
				help = 'deletes original file')

		subparsers = parser.add_subparsers(
				dest = 'command',
				title = 'es65_conv operations',
				description = '(See "%(prog)s COMMAND -h" for more info)',
				help = '')
	
		subparser = subparsers.add_parser(
				'read',
				help = 'reads es65 file and converts it to binary')

		subparser.add_argument(
				'-o', '--outputformat',
				#action = 'count',
				#action = 'store_true',
				default = 'b',
				help = 'Output format can be b for binary, i for Intel HEX')

		subparser.add_argument(
				'file',
				nargs = '?',
				#type = _parse_input_file,
				#default = sys.stdin.buffer,
				help = 'input filename [default: use stdin]')

		args = parser.parse_args()
	
		if not args.verbose:
			level = logging.ERROR
		elif args.verbose == 1:
			level = logging.INFO
		else:
			level = logging.DEBUG
		logging.basicConfig(stream = sys.stderr, level = level, format = '%(message)s')

		##
		## Handle command
		##
		
		if args.command == 'read':
			self.read_file(args.file, args.overwrite, args.delete, args.outputformat)

	def read_file(self, fn, overwrite, delete, format):
		#print("Reads file in es65 format")
		print("Read file:", fn)
		if os.path.isfile(fn) != True:
			raise SystemExit
		with open(fn,"rb") as f:
			imagefile = bytearray(f.read())
		#print("Size: ", len(imagefile), "Bytes")
		
		FILE = []
		if format == 'b':
			name = fn + ".bin" 
		elif format == 'i':
			name = fn + ".hex" 
		
		parse_es65_binfile(imagefile, FILE, format)
		#print(FILE)
		print("new file: ", name)
		if overwrite == True:
			with open(name,"wb") as f:
				f.write(bytearray(FILE))
				f.close()
		else:
			if os.path.isfile(name) == True:
				print("File %s exists!" % name)
				raise SystemExit
			else:
				with open(name,"wb") as f:
					f.write(bytearray(FILE))
					f.close()
		if delete == True:
			os.remove(fn)

if __name__ == "__main__":
	ES65_parse_M().process_command_line()