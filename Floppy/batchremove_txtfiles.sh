#!/bin/bash

conv="./es65_conv.py read"
echo "Reading dir: " $1
if [ -z "$1" ]
then
    echo "Argument is empty! We dont want to delete all .txt files in root."
    exit
fi
for f in `find $1/* -type f -name '*.txt.txt'`
#for f in `find $1/* -type f -name '*.txt.txt'`
#for file in $1/A.* $1/I.* $1/H.* $1/B.* $1/C.* $1/L.* $1/D.* $1/X.*
do
    echo "---------------- removing:                 "$f
    rm $f # Uncomment if you know what you are doing!
done