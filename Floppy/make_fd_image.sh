#!/bin/bash

function log2 {
    local x=0
    for (( y=$1-1 ; $y > 0; y >>= 1 )) ; do
        let x=$x+1
    done
    echo $x
}

#z=$(log2 64)
###################################################################################
#                                  Disk Definitions
#NAME=mc1       # Directory @ 0x2800, Data @ 0x3800, 790 kB
#TRK=80         # Tracks per Side
#HD=2           # Number of Heads
#BPS=1024       # Byte / Sector
#BLOKS=4096     # Blocksize
#SEC=5          # Sector / Track
#MODE=mfm       # 
#MAXDIR=128     # Directory Entries
#SKEW=0         # 
#OFF=2          # Boottracks
#LT=1           # Logical Track is on one side or on both sides
###################################################################################
#NAME=o+r1      # Directory @ 0x5000, Data @ 0x6000, 780 kB
#TRK=80         # Tracks per Side
#HD=2           # Number of Heads
#BPS=1024       # Byte / Sector
#BLOKS=4096     # Blocksize
#SEC=5          # Sector / Track
#MODE=mfm       # 
#MAXDIR=128     # Directory Entries
#SKEW=1         # 
#OFF=4          # Boottracks
#LT=1           # Logical Track is on one side or on both sides
###################################################################################
#NAME=eucm      # Directory @ 0x5000, Data @ 0x7000, 790 kB (Eurocom 3)
#TRK=80         # Tracks per Side
#HD=2           # Number of Heads
#BPS=1024       # Byte / Sector
#BLOKS=2048     # Blocksize
#SEC=5          # Sector / Track
#MODE=mfm       # 
#MAXDIR=128     # Directory Entries
#SKEW=1         # 
#OFF=4          # Boottracks
#LT=1           # Logical Track is on one side or on both sides
###################################################################################
#NAME=o+r3      # Directory @ 0x4800, Data @ 0x5800, 702 kB
#TRK=80         # Tracks per Side
#HD=2           # Number of Heads
#BPS=512        # Byte / Sector
#BLOKS=2048     # Blocksize
#SEC=9          # Sector / Track
#MODE=mfm       # 
#MAXDIR=128     # Directory Entries
#SKEW=1         # 
#OFF=4          # Boottracks
#LT=1           # Logical Track is on one side or on both sides
###################################################################################
#NAME=ecma70    # Directory @ 0x4000, Data @ 0x4800, 144 kB
#TRK=40         # Tracks per Side
#HD=1           # Number of Heads
#BPS=256        # Byte / Sector
#BLOKS=1024     # Blocksize
#SEC=16         # Sector / Track
#MODE=mfm       # 
#MAXDIR=64      # Directory Entries
#SKEW=1         # 
#OFF=4          # Boottracks
#LT=1           # Logical Track is on one side or on both sides
###################################################################################
NAME=es65    # Directory @ 0x?, Data @ 0x?, ? kB
TRK=40         # Tracks per Side
HD=1           # Number of Heads
BPS=256        # Byte / Sector
BLOKS=1024     # Blocksize
SEC=10         # Sector / Track
MODE=fm       # 
MAXDIR=64      # Directory Entries
SKEW=1         # 
OFF=4          # Boottracks
LT=1           # Logical Track is on one side or on both sides
###################################################################################
#                                     Filenames
OUT=$NAME.img
#OUT=empty.$NAME.img
###################################################################################
#                                    Calculations
#BM=$(($BLOKS/128-1))         # CP/M Block Mask
#BS=$(log2 $(($BLOKS/128)))   # CP/M Block Shift
SPT=$(($SEC*$BPS/128))       # CPM sector / Track
SIZER=$(($TRK*$SPT*$HD-($OFF*$SPT)))  # Size of Image in Records
SIZEB=$((($SIZER*128/$LT)+($OFF*$SPT*128)))   # Size of Image in Byte
#DIROFF=$(($OFF*$BPS*$SEC))  # Offset in Byte of Directory
#RESTSZ=$(($SIZEB-$DIROFF))   # Rest of Imagesize
###################################################################################
echo "-----------------------------------------------------------------------------"
echo "Making GOTEK Confg"
echo >                    IMG_$NAME.CFG
echo "[$NAME::$SIZEB]" >> IMG_$NAME.CFG
echo "cyls = $TRK"     >> IMG_$NAME.CFG
echo "heads = $HD"     >> IMG_$NAME.CFG
echo "bps = $BPS"      >> IMG_$NAME.CFG
echo "secs = $SEC"     >> IMG_$NAME.CFG
echo "mode = $MODE"    >> IMG_$NAME.CFG
echo "-----------------------------------------------------------------------------"
#echo "Making cpmtools diskdef"
#echo >                                     diskdef_$NAME.txt
#echo "diskdef $NAME"                    >> diskdef_$NAME.txt
#echo "  seclen $BPS"                    >> diskdef_$NAME.txt
#echo "  tracks $(($TRK*$HD))"          >> diskdef_$NAME.txt
#echo "  sectrk $SEC"                    >> diskdef_$NAME.txt
#echo "  blocksize $BLOKS"               >> diskdef_$NAME.txt
#echo "  maxdir $MAXDIR"                 >> diskdef_$NAME.txt
#echo "  skew $SKEW"                     >> diskdef_$NAME.txt
#echo "  boottrk $OFF"                   >> diskdef_$NAME.txt
#echo "  os 2.2"                         >> diskdef_$NAME.txt
#echo "end"                              >> diskdef_$NAME.txt
#echo "-----------------------------------------------------------------------------"
#dd if=/dev/zero of=zout/$OUT bs=1 count=819200 
echo "Making Disk Image with Size: $SIZEB Bytes and filled with 0xE5"
echo "-----------------------------------------------------------------------------"
dd if=/dev/zero bs=1 count=$SIZEB | tr '\000' '\345' > $OUT

exit # exit here for an empty Image

#echo "Copying Files into Image"
#sudo cpmcp -f $NAME $OUT files/*.* 0:
#echo "Image contains following files:"
#cpmls -f $NAME $OUT 
#echo "Finished"