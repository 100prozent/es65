   
  es65-EDIT und es65-LINK
  -----------------------
   
  A.EDI.S + A.EDI.1 + A.EDI.2 + A.EDI.3  Sources es65-EDIT
   
  A.LNK.S + A.LNK.K + A.LNK.1 + A.LNK.2 + A.LNK.3  Sources es65-LINK
   
   
  Die folgenden Uebersetzungsprozeduren sind nur ablauffaehig, wenn
  sich die Diskette "V65.SYS" im Laufwerk 2 befindet.
   
  I.EDI.ASS   es65-EDIT uebersetzen, M.EDIT erzeugen
              nur im Single-User-System oder im PSYS moeglich
  I.LNK.ASS   es65-LINK uebersetzen, E.LNK  erzeugen
              nur im Single-User-System oder im PSYS moeglich
   
  I.EDI.ASS.PRINT es65-EDIT mit Liste uebersetzen
  I.LNK.ASS.PRINT es65-LINK mit Liste uebersetzen
   
