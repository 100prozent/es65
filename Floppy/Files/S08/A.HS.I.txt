.TI ES82/HS - System-Initialisierung
.MO HSI
;
;   ********************************************************************
;   ***                                                              ***
;   ***                 I N I T I A L I S I E R U N G                ***
;   ***                 -----------------------------                ***
;   ***                                                              ***
;   ***  Dieses Programmstueck wird beim Reset executiert. Dabei     ***
;   ***  werden folgende Aktionen gesetzt:                           ***
;   ***  - PROM-Checksumme                                           ***
;   ***  - RAM-Test (zerstoerungsfrei)                               ***
;   ***  - Coldreset-Test                                            ***
;   ***                                                              ***
;   ********************************************************************
;
;   ****** Globals ******
;
.EY RESET,G/COLDFLG,G/WRESCOU,CHARA,KEYMODE,SPC,SPE,OBCD,OPRG
.EY OPG,OVAL,SMXERR,ONDI,HSMODE
GSMX
@INIT
YVIA
;
;   ****** Equates ******
;
PTR             .EQ 0; Counter fuer Tests Zeropage 0 und 1
ANZT            .EQ 20; 20 MAL troeten
;
;   ****** Zero-Page ******
;
.SG Z,2
COLDFLDZ        .DS 10; Zero-Page Coldreset-Tabelle
G/COLDFLG       .DS 1; Kaltreset-Schalter
G/WRESCOU       .DS 2; Warmreset-Counter
CHARA           .DS 1; Globale Zelle, gedrueckte Taste aus IRQ
KEYMODE         .DS 1; Globale Zelle, Programm/Taste-Erkennung
HSMODE          .DS 1; Programm der Steuerung
;                      PAUS, PSOM, PWIN
SPC             .DS 1; Spiegel PC
SPE             .DS 1; Spiegel PE
;
OBCD            .DS 1; Entprellt BCD aus IRQ
OPRG            .DS 1; Entprellt PRG aus IRQ
OPG             .DS 1; Entprellt PG aus IRQ
ONDI            .DS 1; Entprellt Disable Nachtabsenkung (=$FF)
OVAL            .DS 1; Integriert Vorlauftemp. (900 ms) aus IRQ
                .DS 1; Integriert Aussentemp. (2,7 sec) aus IRQ
                .DS 1; Integriert Kesseltemp. (2,7 sec) aus IRQ
                .DS 1; Integriert Warmwassertemp. (2.7 sec) aus IRQ
;
;   ****** Daten ******
;
.SG D
COLDFLDD        .DS 10; Coldreset-Tabelle Datensegment
;
;   ****** Tabellen ******
;
.SG P
SETV .BY $40
;
COLDTAB1 .BY $AA,$A5,$EE,$E5,$EA,$55,$5F,$FF,0,$5A; Besetzung f. Coldres
TAB1 .AD 10000; Blink activ Checksummenerror
.AD 10000; SMX-Systemerror #1 ... wrong Version
.AD 10000; SMX-Systemerror #2 ... illegal Interrupt
.AD 30000; Blink activ RAM-Error
TAB2 .AD 10000; Blink not activ Checksummenerror
.AD 20000; SMX
.AD 30000; SMX
.AD 30000; Blink not activ RAM-Error
.PG
;
;   ****** Programm ******
;
.SG P
RESET .EQ *
@BEGIN.
  LDA #$C0; Printer Leitung PRINT auf 1 ziehen
  STA VIA+VIAPCR
  SPL ($D000,PTR); ++++++++++++++++++++ Checksumme $C000-$FFFF
  LDY #0
  TYA
  TAX
  @REPEAT.
    STA WDOG
    TXA
    CLC
    ADC (PTR),Y
    TAX
    INC2 (PTR)
  @UNTIL. (EQUAL)
  LDA PTR
  ORA PTR+1
  @END.
  @IF. (NOTZERO)
  CPX #0
  @THEN.
    LDA #0
    @GOTO (BLNK); Error Checksumme, blinken 1sec:1sec
  @ELSE.
    @CYCLE. (PTR,D,2,D,$3FF); +++++++++++++++++ RAM-Test
      STA WDOG
      LDA (PTR),Y
      TAX
      LDA #$A5; Bitmuster 1
      STA (PTR),Y
      CMP (PTR),Y
      @IF. (NOTEQUAL)
      @THEN.
        TXA
        STA (PTR),Y; Sicherheitshalber
        LDA #3
        @GOTO (BLNK); Error RAM, blinken 3sec:3sec
      @ELSE.
        LDA #$5A; Bitmuster 2
        STA (PTR),Y
        CMP (PTR),Y
        @IF. (NOTEQUAL)
        @THEN.
          TXA
          STA (PTR),Y; Sicherheitshalber
          LDA #3
          @GOTO (BLNK); Error RAM, blinken 3sec:3sec
        @END.
      @END.
      TXA
      STA (PTR),Y; Zelle o.k, zurueckspeichern
    @END.
;
    CLV; ++++++++++++++++++ RAM's und Checksumme o.k
    STA WDOG; +++++++++++++ Pruefeung auf Coldreset
    @REPEAT.
      @IF. (NOTEQUAL)
      LDA COLDTAB1,Y
      CMP COLDFLDZ,Y
      @OR. (NOTEQUAL)
      CMP COLDFLDD,Y
      @THEN.
        SEV
      @END.
    @UNTIL. (ERROR)
    @OR. (GREATEQU)
    INY
    CPY #10
    @END.
    @IF. (ERROR)
    @THEN.
      SWTON (G/COLDFLG); Coldreset erkannt
      LDY #10; Tabellen neu errichten
      @REPEAT.
        LDA COLDTAB1-1,Y
        STA COLDFLDD-1,Y
        STA COLDFLDZ-1,Y
        DEY
      @UNTIL. (ZERO)
      @END.
      LDA #0
      STA PE; Ausgaenge loeschen
      STA SPE
      STA PC
      STA SPC
      STA G/WRESCOU; Warmreset-Counter initialisieren
      STA G/WRESCOU+1
    @ELSE.
      SWTOFF (G/COLDFLG); Warmreset erkannt
      INC2 (G/WRESCOU)
    @END.
    STA WDOG
    @GOTO (SMXSTR)
  @END.
@END.
.PG
SMXERR .EQ *; SMX-Error-Programm
@BEGIN.
  SEI
  @GOTO (BLNK)
@END.
;
;
BLNK .EQ *; +++++++++++++++++++++++ Blinken der Error-LED lt. (A)
@BEGIN.
  ASL A
  TAX
  LDA #ANZT
  STA PTR
  @REPEAT.
    LDY #0
    STY PC
    STY PD
    STY PE
    LDY #100
    @REPEAT.
      LDA TAB1,X
      STA VIA+VIALL1
      LDA TAB1+1,X
      STA VIA+VIACH1
      LDA #8
      STA PC
      @REPEAT.
        BIT VIA+VIAIFR
        STA WDOG
      @UNTIL. (OVERFLOW)
      @END.
      DEY
    @UNTIL. (ZERO)
    @END.
    LDY #100
    @REPEAT.
      LDA TAB2,X
      STA VIA+VIALL1
      LDA TAB2+1,X
      STA VIA+VIACH1
      LDA #0
      STA PC
      @REPEAT.
        BIT VIA+VIAIFR
        STA WDOG
      @UNTIL. (OVERFLOW)
      @END.
      DEY
    @UNTIL. (ZERO)
    @END.
  @UNTIL. (ZERO)
  DEC PTR
  @END.
  JMP *
@END.
.EN
