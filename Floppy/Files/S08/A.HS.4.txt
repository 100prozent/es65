.TI ES82/HS - Input-Task
.MO HS4
;
;   ********************************************************************
;   ***                                                              ***
;   ***                    I N P U T  -  T A S K                     ***
;   ***                    ---------------------                     ***
;   ***                                                              ***
;   ***  Dieser Task bedient das Tastenfeld und den PRG-Schalter.    ***
;   ***  Er verarbeitet die Werte, die aus dem zyklischen Inter-     ***
;   ***  rupt des Displaytasks geliefert werden.                     ***
;   ***  Dieser Task ist reiner Producer.                            ***
;   ***                                                              ***
;   ********************************************************************
;
PCTAST @PROGRAM ($18)
;
;   ****** Globals ******
;
.EX TFRESEX,UCMDEX,RCMDEX,UDRESEX,PRESEX
.EX URESEX,DMUTEX,DCMDEX,DRESEX,SXL4EX,ADTAB,PCMDEX
GHS
.EY PCTAST
;
;   ****** Zero-Page ******
;
.SG Z
PTR             .DS 2
MSG             .DS 6
BUF             .DS 4
PTR2            .DS 2
BUFPOS          .DS 1; Fuer Edit-Mode
CODE            .DS 1; Fuer Priv.Mode
LMD             .DS 1; Merker fuer Wecker-Display-Nummer
TST             .DS 1; Weckertest
DIGI            .DS 1; Drucker-Ziffer
;
;   ****** Tabellen ******
;
.SG P
CDTAB           .BY 0,0,0,1; Code-Tabelle
CDLEN .EQ *-CDTAB
;
CTAB .BY 1,1,1,1; Drucker-Aus
     .BY 2,2,2,2; Drucker-Statistik Tag
     .BY 3,3,3,3; Drucker-Statistik Monat
     .BY 4,4,4,4; Drucker-Brenner-Betrieb
;
MSGTAB .BY POFMSG,PBTMSG,PBTMSG,PBBMSG
.PG
;
;   ****** Programm ******
;
.SG P
;
;   Unterprogramme:
;
BUFVOR @PROCEDURE; Buffer loeschen und an Display senden
; Vorausgesetzt muss sein: DMUTEX geholt
@BEGIN.
  STA WDOG
  LDA #$AA
  LDX #4
  @REPEAT.
    STA BUF-1,X
    DEX
  @UNTIL. (ZERO)
  @END.
  @PASS (BUFSND); Sende
@END.
;
;   Zerstoert: alles
;
BUFSND @PROCEDURE; Buffer an Display-Task schicken
@BEGIN.
  MESS (MSG,DISMSG,$A)
  SXSEND (DCMDEX,MSG)
  SXWAIT (DRESEX); Response
@END.
;
;   Zerstoert: alles
;
SENDPRG @PROCEDURE; PRG-Message zu Regeltask schicken
@BEGIN.
  STA WDOG
  LDA OPRG; aktueller Programm-Status
  LDX #3
  @REPEAT.
    DEX
    LSR A
  @UNTIL. (CARRY)
  @OR. (ZERO)
  CPX #0
  @END.; Umwandlung in 0,1,2
  TXA
  CLC
  ADC #PRGMSG; Erzeugung PRG-Message
  STA MSG+TYPE
  LDA #0
  STA LMD
  STA DIGI
  STA CODE
  MESS (MSG,,5,TFRESEX)
  SXSEND (RCMDEX,MSG)
  SXWAIT (TFRESEX)
@END.
;
;   Zerstoert: alles
;
GETTAST @PROCEDURE; Lese eine Taste, sende eventuell PRG-MSG, Taste in (
@BEGIN.; AKKU
  @REPEAT.
    SXWAIT (SXL4EX); Tasten oder Prog-Interrupt
    LDA KEYMODE
    @IF. (EQUAL)
    CMP #PRGNEW
    @THEN.
      @PASS (SENDPRG)
      CLV
    @ELSE.
      @IF. (EQUAL); Taste gedrueckt
      CMP #KEYNEW
      @THEN.
        SEV
        LDA CHARA
      @ELSE.
        CLV; sicherheitshalber
      @END.
    @END.
  @UNTIL. (OVERFLOW)
  @END.
@END.
;
;   Zerstoert: alles
;
GETNUM @PROCEDURE; Lese eine einzelne Nummer (0...9) mit Anzeigen
@BEGIN.
  @PASS (BUFVOR); Buffer loeschen
  @REPEAT.
    @PASS (GETTAST); Taste lesen
    STA PTR2
  @UNTIL. (NOERROR)
  CHEC (PTR2,0,9)
  @END.
  @PASS (INSRT); Anzeigen
@END.
;
;   Zerstoert: alles
;
DISCLK @PROCEDURE; Anzeige einer Zeit (Uhr oder Wecker)
@BEGIN.
  STA MSG+REM; DST lt. Uhrtask
  MESS (MSG,URDMSG,8,DCMDEX); Read
  SXSEND (UCMDEX,MSG)
  SXWAIT (DRESEX); Response
  SXSEND (UDRESEX); Response geben
@END.
;
;   Zerstoert: alles
;
UP JMP (PTR2); Fuer Priv. Mode
;
;
BUFJUS @PROCEDURE; Buffer und Pointer justieren (Edit)
@BEGIN.
  LDX #0
  STX BUFPOS
  STX PTR2+1; Merker fuer '-' - Taste (don't-care)
  @PASS (BUFVOR); Buffer loeschen
@END.
;
;   Zerstoert: alles
;
INSRT @PROCEDURE; Zeichen in Display-Buffer von rechts schieben (A)
@BEGIN.
  STA WDOG
  LDX BUFPOS
  @IF. (LESS)
  CPX #8
  @THEN.
    INC BUFPOS; naechste Position
  @END.
  LDX #4
  @REPEAT.
    ASL BUF; Verschieben
    ROL BUF+1
    ROL BUF+2
    ROL BUF+3
    DEX
  @UNTIL. (ZERO)
  @END.
  ORA BUF
  STA BUF
  @PASS (BUFSND); Anzeigen
@END.
;
;
CHECK @PROCEDURE; Pruefen auf gueltiges Code-Zeichen
@BEGIN.
  LDX DIGI
  @IF. (ZERO)
  @THEN.
    @REPEAT.
      CMP CTAB,X
    @UNTIL. (EQUAL)
    @OR. (GREATEQU)
    INX
    INX
    INX
    INX
    CPX #16
    @END.
    @IF. (LESS)
    CPX #16
    @THEN.
      INX
      STX DIGI
      CLV
      SEC
    @ELSE.
      SEV
    @END.
  @ELSE.
    @IF. (EQUAL)
    CMP CTAB,X
    @THEN.
      @IF. (NOCARRY)
      INX
      TXA
      LSR A
      @AND. (NOCARRY)
      LSR A
      @THEN.
        LDX #0
        STX DIGI
        CLV
        CLC
      @ELSE.
        INC DIGI
        CLV
        SEC
      @END
    @ELSE.
      LDX #0
      STX DIGI
      SEV
    @END.
  @END.
@END.
;
; V=0 ... O.K, (A) ist Code-Nummer (1..4)
.PG
;
; (****** Entry Tastenfeld-Task ******)
@BEGIN.
  @IF. (ON)
  SWTCK (G/COLDFLG,@A)
  @THEN.
    SXWAIT (SXL4EX); Coldreset, warten auf PRG-Schalter
    @PASS (SENDPRG)
  @END.
  LDA #0
  STA CODE; init. Code-Zaehler
  STA LMD
  STA DIGI
  @REPEAT.
    @PASS (GETTAST); warten auf Taste
    @CASE.
      @OF. (EQUAL)
      CMP #PRG
      @BEGIN; ++++++++++++++++++ Programmier-Taste Edit-Mode
        LDX #0
        STX LMD
        STX CODE
        STX DIGI
        SXWAIT (DMUTEX)
        STO2 (PTR); Displaytask anfordern
        @PASS (BUFJUS); loeschen
        @REPEAT.; Edit
          @PASS (GETTAST); Taste
          @CASE.
            @OF. (EQUAL)
            CMP #UHR
            @BEGIN.; Uhrzeit setzen (wenn 8 Zif.) oder don't care (-)
              @IF. (EQUAL)
              LDX BUFPOS
              CPX #8
              @AND. (PLUS)
              LDA PTR2+1
              @THEN.
                MESS (MSG,USTMSG,$A); Uhrzeit set (don't care unmoegl.)
                SXSEND (UCMDEX,MSG)
                SXWAIT (URESEX)
                SEV; Term Edit
              @ELSE.
                LDA #$F; don't care
                @PASS (INSRT); anzeigen ('-')
                SWTON (PTR2+1); Merker setzen
                CLV
              @END.
            @END.
            @OF. (NOERROR)
            STA PTR2
            CHEC (PTR2,AUS,IXT)
            @BEGIN; Tasten AUS,INC,EIN,DEC
              SEC
              SBC #UHR; umwandeln der Tastencodes
              @IF. (EQUAL)
              LDX BUFPOS
              CPX #8
              @THEN.
                STA MSG+REM+5; DST
                MESS (MSG,WSTMSG,$C); Wecker setzen
                @IF. (EQUAL)
                LDA MSG+REM+5
                CMP #DET-UHR
                @OR. (EQUAL)
                CMP #IXT-UHR
                @THEN.; Wenn INC oder DEC --> HK lesen, ohne Anzeige!!!
                  @REPEAT.
                    @PASS (GETTAST)
                    STA PTR2
                  @UNTIL. (NOERROR)
                  CHEC (PTR2,0,9)
                  @END.
                @ELSE.
                  LDA #0; bei EIN u. AUS HK=0
                @END.
                STA MSG+REM+4; HK fuer WSTMSG
                SXSEND (UCMDEX,MSG)
                SXWAIT (URESEX)
                SEV; Term Edit
              @ELSE.
                @IF. (ZERO)
                CPX #0
                @THEN.; Wecker loeschen, wenn keine Zifferntaste vorh.
                  STA MSG+REM
                  MESS (MSG,WDLMSG,7)
                  SXSEND (UCMDEX,MSG)
                  SXWAIT (URESEX)
                  SEV; Term Edit
                @ELSE.
                  CLV
                @END.
              @END.
            @END.
            @OF. (EQUAL)
            CMP #PRG
            @BEGIN.; Exit Edit-Mode
              SEV; Term Edit
            @END.
            @ELSE.; Zifferntaste
              @PASS (INSRT)
              CLV
          @END.
        @UNTIL. (OVERFLOW)
        @END; ++++++++++++++++ EDIT-LOOP
        LDA #0
        @PASS (DISCLK); Uhrzeit anzeigen
        LOD2 (PTR)
        SXSEND (DMUTEX); Displaytask freigeben
      @END.
.PG
      @OF. (EQUAL)
      CMP #UHR
      @BEGIN.; ++++++++++++++++ Anzeige der Weckzeiten (ohne HK!!!)
        LDX #0
        STX CODE
        STX DIGI
        @PASS (GETTAST)
        @IF. (NOERROR)
        STA PTR2
        CHEC (PTR2,UHR,IXT)
        @THEN.; bei UHR,AUS,EIN,INC,DEC moeglich
          SXWAIT (DMUTEX)
          STO2 (PTR)
          LDA PTR2
          @IF. (EQUAL)
          CMP TST
          @THEN.
            LDY LMD
            STY MSG+REM+1
            LDY #1
            STY LMD
          @ELSE.
            STA TST
            LDY #0
            STY MSG+REM+1
            INY
            STY LMD
          @END.
          SEC
          SBC #UHR
          @PASS (DISCLK)
          LOD2 (PTR)
          SXSEND (DMUTEX)
        @END.
      @END.
.PG
      @OF. (NOERROR)
      STA PTR2
      CHEC (PTR2,AUS,IXT)
      @BEGIN.; +++++++++++++++++ Ausfuehren der entspr. Funktion
        LDX #0
        STX LMD
        STX CODE
        STX DIGI
        SEC
        SBC #UHR
        STA PTR2+1
        @IF. (EQUAL)
        CMP #IXT-UHR
        @OR. (EQUAL)
        CMP #DET-UHR
        @THEN.; Lese noch Heizkurve (INC,DEC)
          SXWAIT (DMUTEX)
          STO2 (PTR)
          @PASS (GETNUM)
          LOD2 (PTR)
          SXSEND (DMUTEX)
          LDA PTR2
        @ELSE.
          LDA #0; 0 bei AUS,EIN
        @END.
        STA MSG+REM+1
        LDA PTR2+1
        STA MSG+REM
        MESS (MSG,RWMSG,8,TFRESEX)
        SXSEND (RCMDEX,MSG); Regeltask direkt
        SXWAIT (TFRESEX)
      @END.
.PG
      @OF. (EQUAL)
      LDX CODE
      CMP CDTAB,X
      @BEGIN; +++++++++++++++++++++ gueltige Ziffer fuer Code
        LDY #0
        STY LMD
        STY DIGI
        @IF. (LESS)
        CPX #CDLEN-1
        @THEN.
          INC CODE
        @ELSE.
          LDX #0; Code-Nummer o.k, exec. priv. Mode
          STX CODE
          STX DIGI
          SXWAIT (DMUTEX)
          STO2 (PTR)
          @PASS (BUFVOR)
          @REPEAT.
            @PASS (GETTAST)
            @IF. (ERROR)
            STA PTR2
            CHEC (PTR2,0,9)
            @THEN
              @CASE.
                @OF. (EQUAL)
                CMP #EIN
                @BEGIN.; Drucker Ausdruck Regelungsstatus bei Betrieb
                  LDA #$AB; Anzeige 'E'
                  STA BUF
                  @PASS (BUFSND)
                  LDA #PBBMSG
                  STA MSG+TYPE
                  CLV
                @END.
                @OF. (EQUAL)
                CMP #AUS
                @BEGIN.; Drucker off
                  LDA #$AA
                  STA BUF; Anzeige ' '
                  @PASS (BUFSND)
                  LDA #POFMSG
                  STA MSG+TYPE
                  CLV
                @END.
                @OF. (EQUAL)
                CMP #IXT
                @BEGIN.; Drucker Brenner-Betriebszeit drucken
                  LDA #$AB; Anzeige 'E'
                  STA BUF
                  @PASS (BUFSND)
                  LDA #PBTMSG
                  STA MSG+TYPE
                  CLV
                @END.
                @OF. (EQUAL)
                CMP #DET
                @BEGIN.; Drucker Regelung Trace
                  LDA #$AB
                  STA BUF
                  @PASS (BUFSND)
                  LDA #PBCMSG
                  STA MSG+TYPE
                  CLV
                @END.
                @ELSE.
                  SEV
              @END.
              @IF. (NOVERFLOW)
              @THEN.
                MESS (MSG,,5,TFRESEX)
                SXWAIT (PRESEX)
                SXSEND (PCMDEX,MSG)
                SXWAIT (TFRESEX)
                CLV
              @END.
            @ELSE.
              TAY
              ASL A; Ziffern von 0 ... 9:
; 0 ... Brenner on
; 1 ... Brenner off
; 2 ... Mischer auf
; 3 ... Mischer halt
; 4 ... Mischer zu
; 5 ... Pumpe ein
; 6 ... Pumpe aus
              TAX
              LDA ADTAB,X
              STA PTR2
              LDA ADTAB+1,X
              STA PTR2+1
              TYA
              ORA #$A0
              STA BUF
              @PASS (BUFSND)
              STA WDOG
              @PASS (UP)
              CLV
            @END.
          @UNTIL. (OVERFLOW)
          @END
          LDA #0
          @PASS (DISCLK)
          LOD2 (PTR)
          SXSEND (DMUTEX)
        @END.
      @END.
.PG
      @OF. (NOERROR)
      @PASS (CHECK)
      @BEGIN.
        @IF. (NOCARRY)
        @THEN.
          TAX
          MESS (MSG,,5,TFRESEX)
          LDA MSGTAB-1,X
          STA MSG+TYPE
          @IF. (EQUAL)
          CMP #PBTMSG
          @THEN.
            @IF. (EQUAL)
            CPX #2
            @THEN.
              LDA #DTAGS
            @ELSE.
              LDA #DMONS
            @END.
            STA MSG+REM
            INC MSG+MLEN
            INC MSG+MLEN
          @END.
          SXWAIT (PRESEX)
          SXSEND (PCMDEX,MSG)
          SXWAIT (TFRESEX)
        @END.
      @END.
      @ELSE.
        LDX #0
        STX CODE
        STX LMD
        STX DIGI
    @END.
    CLV
  @UNTIL. (ERROR)
  @END
@END.
.EN
