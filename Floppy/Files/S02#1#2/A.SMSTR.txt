;
;
;   ********************************************************************
;   ***                                                              ***
;   ***      S M - S T R U K T U R  -  M A K R O S  (small)          ***
;   ***      ++++++++++++++++++++++++++++++++++++++++++++++          ***
;   ***                                                              ***
;   ***   Implementierung von:                                       ***
;   ***   -------------------                                        ***
;   *** @IF. <Cond-list> {<con>} @THEN. <stat> {@ELSE. <stat>} @END. ***
;   *** @REPEAT. <stats> @UNTIL. <Cond-list> {<con>} @END.           ***
;   *** @WHILE. <Cond-list> {<con>} @DO. <stats> @END.               ***
;   *** @BEGIN.                                                      ***
;   *** @END.                                                        ***
;   *** @CASE. @OF. <Cond-list> {<con>} @BEGIN. <stats> @END.        ---
;   ***      {@OF. <Cond-list> {<con>} @BEGIN. <stats> @END.         ---
;   ***       @ELSE. <stats> @END.                                   ***
;   ***                                                              ***
;   ********************************************************************
.PG
;
@IF.            .MD (@COND); If-Structure
.SB
.ST @C.ON.D//=@COND
.ST @C.ON.T//=1
               .ME
;
;
@THEN.          .MD
.IE @C.ON.T//-1; after IF
.ST @C.ON.T//=2
.CE
.IE @C.ON.T//-2; after IF-AND
@B.RA.NCH. (@C.ON.D//,M.//.EL)
.ST @C.ON.T//=4
               .MX
.IE @C.ON.T//-3; after IF-OR
@B.RA.NCH. (@C.ON.D//,M.//.EL)
.SC
M.//.TH .EQ *
.ST @C.ON.T//=4
               .MX
? ILLEGAL THEN - IGNORED
               .ME
;
;
@ELSE.          .MD; Else for IF and CASE
.IE @C.ON.T//-4; IF-Else
.ST @C.ON.T//=5
JMP M.//.FI
.SC
M.//.EL .EQ *
               .MX
.IE @C.ON.T//-30
.ST @C.ON.T//=36; CASE-Else
.SC
M.@@.CA .EQ *
.BE
               .MX
? ILLEGAL ELSE - IGNORED
               .ME
;
;
@REPEAT.        .MD; Strucure REPEAT
.SB
.ST @C.ON.T//=20
.SC
M.//.REP .EQ *
               .ME
;
;
@UNTIL.         .MD (@COND)
.ST @C.ON.D//=@COND
.ST @C.ON.T//=21
               .ME
;
;
@WHILE.         .MD (@COND)
.SB
.ST @C.ON.D//=@COND
.ST @C.ON.T//=11
.SC
M.//.WL .EQ *
               .ME
;
;
@DO.            .MD
.IE @C.ON.T//-11
.ST @C.ON.T//=12
.CE
.IE @C.ON.T//-12
@B.RA.NCH. (@C.ON.D//,M.//.OD)
.SC
M.//.DO .EQ *
.ST @C.ON.T//=14
               .MX
.IE @C.ON.T//-13
@B.RA.NCH. (@C.ON.D//,M.//.OD)
.SC
M.//.DO .EQ *
.ST @C.ON.T//=14
               .MX
.IE @C.ON.T//-41
.SC
F.O0.1// CMP #@C.ON.D//
BEQ *+7
BCC *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-42
.SC
F.O0.1// CPX #@C.ON.D//
BEQ *+7
BCC *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-43
.SC
F.O0.1// CPY #@C.ON.D//
BEQ *+7
BCC *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-40
.SC
F.O0.1// .EQ *
PHA
SEC
CLD
LDA #<@C.ON.D//
SBC B/A.SE1//
LDA #>@C.ON.D//
SBC B/A.SE1//+1
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-44
.SC
F.O0.1// PHA
SEC
CLD
LDA #<@C.ON.D//
SBC @C.EL.L.//
LDA #>@C.ON.D//
SBC @C.EL.L.//+1
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-45
.ST @C.ON.T//=44
.SC
F.O0.1// PHA
SEC
CLD
LDA @C.ON.D//
SBC @C.EL.L.//
LDA @C.ON.D//+1
SBC @C.EL.L.//+1
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-46
.ST @C.ON.T//=40
.SC
F.O0.1// PHA
SEC
CLD
LDA @C.ON.D//
SBC B/A.SE1//
LDA @C.ON.D//+1
SBC B/A.SE1//+1
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-47
.ST @C.ON.T//=41
.SC
F.O0.1// CMP @C.ON.D//
BEQ *+7
BCC *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-48
.ST @C.ON.T//=42
.SC
F.O0.1// CPX @C.ON.D//
BEQ *+7
BCC *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-49
.ST @C.ON.T//=43
.SC
F.O0.1// CPY @C.ON.D//
BEQ *+7
BCC *+5
JMP F.EN.01//
PLP
               .MX
;
.IE @C.ON.T//-51
.SC
F.O0.1// CMP #@C.ON.D//
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-52
.SC
F.O0.1// CPX #@C.ON.D//
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-53
.SC
F.O0.1// CPY #@C.ON.D//
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-50
.SC
F.O0.1// PHA
SEC
CLD
LDA B/A.SE1//
SBC #<@C.ON.D//
LDA B/A.SE1//+1
SBC #>@C.ON.D//
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-54
.SC
F.O0.1// PHA
SEC
CLD
LDA @C.EL.L.//
SBC #<@C.ON.D//
LDA @C.EL.L.//+1
SBC #>@C.ON.D//
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-55
.ST @C.ON.T//=54
.SC
F.O0.1// PHA
SEC
CLD
LDA @C.EL.L.//
SBC @C.ON.D//
LDA @C.EL.L.//+1
SBC @C.ON.D//+1
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-56
.ST @C.ON.T//=50
.SC
F.O0.1// PHA
SEC
CLD
LDA B/A.SE1//
SBC @C.ON.D//
LDA B/A.SE1//+1
SBC @C.ON.D//+1
PLA
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-57
.ST @C.ON.T//=51
.SC
F.O0.1// CMP @C.ON.D//
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-58
.ST @C.ON.T//=52
.SC
F.O0.1// CPX @C.ON.D//
BCS *+5
JMP F.EN.01//
PLP
               .MX
.IE @C.ON.T//-59
.ST @C.ON.T//=53
.SC
F.O0.1// CPY @C.ON.D//
BCS *+5
JMP F.EN.01//
PLP
               .MX
? ILLEGAL DO - IGNORED
               .ME
;
;
@BEGIN.        .MD
.IE @C.ON.T//-31
.ST @C.ON.T//=32
.CE
.IE @C.ON.T//-32
@B.RA.NCH. (@C.ON.D//,M.@@.CA)
.ST @C.ON.T//=34
.BB
               .MX
.IE @C.ON.T//-33
@B.RA.NCH. (@C.ON.D//,M.@@.CA)
.SC
M.@@.BG .EQ *
.ST @C.ON.T//=34
.BB
               .MX
.SB
.ST @C.ON.T//=60
               .ME
;
;
@CASE.          .MD
.SB
.ST @C.ON.T//=30
.BB
               .ME
;
;
@OF.            .MD (@COND)
.IE @C.ON.T//-30
.SC
M.@@.CA .EQ *
.BE
.ST @C.ON.D//=@COND
.ST @C.ON.T//=31
.BB
               .MX
? ILLEGAL OF - IGNORED
               .ME
;
;
@AND.           .MD (@COND)
.IE @C.ON.T//-1
.ST @C.ON.T//=2
.CE
.IE @C.ON.T//-2
@B.RA.NCH. (@C.ON.D//,M.//.EL)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-11
.ST @C.ON.T//=12
.CE
.IE @C.ON.T//-12
@B.RA.NCH. (@C.ON.D//,M.//.OD)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-21
.ST @C.ON.T//=22
.CE
.IE @C.ON.T//-22
@B.RA.NCH. (@C.ON.D//,M.//.REP)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-31
.ST @C.ON.T//=32
.CE
.IE @C.ON.T//-32
@B.RA.NCH. (@C.ON.D//,M.@@.CA)
.ST @C.ON.D//=@COND
               .MX
? ILLEGAL AND - IGNORED
               .ME
;
;
@OR.            .MD (@COND)
.IE @C.ON.T//-1
.ST @C.ON.T//=3
.CE
.IE @C.ON.T//-3
@B.RA.NCH. (@C.ON.D//,M.//.TH,INVERS)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-11
.ST @C.ON.T//=13
.CE
.IE @C.ON.T//-13
@B.RA.NCH. (@C.ON.D//,M.//.DO,INVERS)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-21
.ST @C.ON.T//=23
.CE
.IE @C.ON.T//-23
@B.RA.NCH. (@C.ON.D//,M//REN,INVERS)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-31
.ST @C.ON.T//=33
.CE
.IE @C.ON.T//-33
@B.RA.NCH. (@C.ON.D//,M.@@.BG,INVERS)
.ST @C.ON.D//=@COND
               .MX
? ILLEGAL OR - IGNORED
               .ME
;
;
@END.           .MD
.IE @C.ON.T//-4
.SC
M.//.EL .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-5
.SC
M.//.FI .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-14
.SC
JMP M.//.WL
M.//.OD .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-21
.ST @C.ON.T//=22
.CE
.IE @C.ON.T//-22
@B.RA.NCH. (@C.ON.D//,M.//.REP)
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-23
@B.RA.NCH. (@C.ON.D//,M.//.REP)
.SC
M//REN .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-30
.ST @C.ON.T//=0
.SC
M.@@.CA .EQ *
M//CAE .EQ *
.BE
.SE
               .MX
.IE @C.ON.T//-36
.ST @C.ON.T//=0
.SC
M//CAE .EQ *
.SE
               .MX
.IE @C.ON.T//-34
.ST @C.ON.T//=30
JMP M//CAE
.BE
               .MX
.IE @C.ON.T//-40
.ST @C.ON.T//=0
.ST @F.OR.COU=@F.OR.COU-2
PHP
INC B/A.SE1//
BNE *+5
INC B/A.SE1//+1
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-41
.ST @C.ON.T//=0
PHP
SEC
CLD
ADC #0
BCS *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-42
.ST @C.ON.T//=0
PHP
INX
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-43
.ST @C.ON.T//=0
PHP
INY
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-44
.ST @C.ON.T//=0
PHP
INC @C.EL.L.//
BNE *+5
INC @C.EL.L.//+1
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-50
.ST @C.ON.T//=0
.ST @F.OR.COU=@F.OR.COU-2
PHP
PHA
LDA B/A.SE1//
BNE *+5
DEC B/A.SE1//+1
DEC B/A.SE1//
PLA
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-51
.ST @C.ON.T//=0
PHP
SEC
CLD
SBC #1
BCC *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-52
.ST @C.ON.T//=0
PHP
DEX
CPX #$FF
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-53
.ST @C.ON.T//=0
PHP
DEY
CPY #$FF
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-54
.ST @C.ON.T//=0
PHP
PHA
LDA @C.EL.L.//
BNE *+5
DEC @C.EL.L.//+1
DEC @C.EL.L.//
PLA
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-60
.ST @C.ON.T//=0
.SE
               .MX
? ILLEGAL END - IGNORED
               .ME
;
;
@B.RA.NCH.        .MD (@B.ED.ING,ZIEL,@I.NV.ERS)
.ST @B.ED..=@B.ED.ING
.IA @I.NV.ERS
.ST @B.ED..=0-@B.ED..
.CE
.IE @B.ED..-$FFFB
BCC *+7
BEQ *+5
JMP ZIEL
               .MX
.IE @B.ED..-5
BCC *+4
BNE *+5
JMP ZIEL
               .MX
.ST @I.NV.E=0
.IA @I.NV.ERS
.ST @I.NV.E=$20
.CE
.IE @B.ED.ING&%100000
.ST @B.ED.I=@B.ED.ING+@I.NV.E
.CE
.IN @B.ED.ING&%100000
.ST @B.ED.I=@B.ED.ING-@I.NV.E
.CE
.IN @B.ED.I&$FF1F-$10
? WRONG CONDITION FOR BRANCH - IGNORED
               .MX
.ST @C.HE.CK=ZIEL-*-2
.IE @C.HE.CK&$FF80
.ST @C.HE.CK=@C.HE.CK&$00FF
.BY @B.ED.I
.BY @C.HE.CK
               .MX
.IE @C.HE.CK&$FF80-$FF80
.ST @C.HE.CK=@C.HE.CK&$00FF
.BY @B.ED.I
.BY @C.HE.CK
               .MX
.ES
; ? BRANCH OUT OF BOUNDS  -  BXX 0  ASSUMED
.EC
.BY @B.ED.I,0
               .ME
;
;
