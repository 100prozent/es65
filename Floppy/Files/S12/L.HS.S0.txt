                                                        - 23 -
 
 4.) Beschreibung der Messages/Exchanges
     -----------------------------------
 
 4.0.) Allgemeines
       -----------
 
    Dieser Abschnitt beschaeftigt sich mit der Beschreibung der Ex-
 changes/Messages, welche fuer die Intertaskkommunikation verwendet
 werden.
 
 
 Prinzipiell sind 2 Abschnitte unterschieden:
 
 - Message-Gliederung
   Die Messages sind nach ihrem Typ aufsteigend sortiert
 
 - Exchange-Gliederung
   Die Exchanges sind alphabetisch geordnet
A
                                                        - 24 -
 
 
 4.1.) Messages
       --------
 
 Aufbau INTMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 1    --- ----   ----      ---  ---    ---    ---    ---    ---
 
 Tasks         : System
 Exchanges: SXL3EX, SXL4EX
 Funktion
 
 Interrupt occurred: Taste gedrueckt oder Programmschalter umgeschal-
 tet.
 An SXL3EX bedeutet dies das Acknowledge des Druckers.
 -----------------------------------------------------------------------
 
 Aufbau MISSEDINT
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 2    --- ----   ----      ---  ---    ---    ---    ---    ---
 
 Tasks         : System
 Exchanges: SXL3EX, SXL4EX
 Funktion
 
 Diese MSG zeigt an, dass mind. 2 Interrupts vorkamen, jedoch nicht
 alle behandelt wurden. Dieser Fall wird jedoch hier nicht beachtet.
 -----------------------------------------------------------------------
A
                                                        - 25 -
 
 
 Aufbau TOMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 3    4   ----   ----      ---  ---    ---    ---    ---    ---
 
 Tasks         : System, Display, Printer, Regel, Ticker
 Exchanges: DCMDEX, RCMDEX, PDATEX, SXL3EX, PCMDEX, TTICEX
 Funktion
 
 Time-Out
 -----------------------------------------------------------------------
 
 Aufbau OKMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 7    4   ----   ----      ---  ---    ---    ---    ---    ---
 
 Tasks         : Display, Printer
 Exchanges: DMUTEX, PRESEX
 Funktion
 
 Mutual exclusion des Display-Tasks. Wenn der Displaytask frei ist,
 steht die Message am Exchange und kann geholt werden.
 An PRESEX bedeutet sie das Response auf einen Druckbefehl.
 -----------------------------------------------------------------------
 
 Aufbau ERRMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 10   7   ----   ----      ENR  ---    ---    ---    ---    ---
 
 Tasks         : Regler, Drucker, Display
 Exchanges: DRESEX, DCMDEX, PCMDEX, D2RESEX
 Funktion
 
 Es wird eine Error-Meldung angezeigt, Text lt. Error-Tabelle.
 Die Anzeige wird durch ein weiteres Kommando an DCMDEX wieder ge-
 loescht. (s. Einzelbeschreibung Display-Task).
 ERRMSG an PCMDEX bewirkt einen Fehlerausdruck.
 -----------------------------------------------------------------------
A
                                                        - 26 -
 
 Aufbau DISMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 11   $A  ----   ----      FRR  FRL    FLR    FLL    ---    ---
 
 Tasks         : Input, Display
 Exchanges: DRESEX, DCMDEX
 Funktion
 
 Diese Message bedingt, dass der Inhalt der MSG angezeigt wird.
 Anzeigefeld:  FLL  FLR    FRL  FRR  (von links nach rechts).
 Der Code ist im Display-Task festgelegt (s. Einzelbeschreibung).
 -----------------------------------------------------------------------
 
 Aufbau PRGMSG (Fall 1: PAUS)
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 17   5   TFRESEX ---      ---  ---    ---    ---    ---    ---
 
 Tasks         : Input, Regler
 Exchanges: TFRESEX, RCMDEX
 Funktion
 
 Programm-Mode AUS an Regler uebergeben.
 
 -----------------------------------------------------------------------
 
 Aufbau PRGMSG+1 (Fall 2: PSOM)
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 18   5   TFRESEX ---      ---  ---    ---    ---    ---    ---
 
 Tasks         : Input, Regler
 Exchanges: TFRESEX, RCMDEX
 Funktion
 
 Programm-Mode SOM an Regler uebergeben.
 
 -----------------------------------------------------------------------
 
 Aufbau PRGMSG+2 (Fall 3: PWIN)
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 19   5   TFRESEX ---      ---  ---    ---    ---    ---    ---
 
 Tasks         : Input, Regler
 Exchanges: TFRESEX, RCMDEX
 Funktion
 
 Programm-Mode WIN an Regler uebergeben.
 
 -----------------------------------------------------------------------
 
 Aufbau TIMMSG (Fall 1: DST=DUHR ... Uhrzeitmessage)
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 20   $A  ----   ----      MIN  STD    TAG    MON    ---    ---
 
 Tasks         : Uhr, Display, Printer, Input
 Exchanges: DRESEX, DCMDEX, PDATEX, UDRESEX
 Funktion
 
 Diese Message dient dazu, die gelieferte Uhrzeit anzuzeigen.
 
 -----------------------------------------------------------------------
A
                                                        - 27 -
 
 Aufbau TICMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 21   4   ----   ----      ---  ---    ---    ---    ---    ---
 
 Tasks         : Ticker, Uhr
 Exchanges: T1RESEX, UCMDEX
 Funktion
 Diese MSG kommt in exaktem Minutenabstand und dient dem Uhrtask zum
 Zeit-Update. Die MSG wird bei der Initialisierung des Uhrtasks von ihm
 an T1RESEX geschickt (Init. Time-count).
 -----------------------------------------------------------------------
 
 Aufbau USTMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 22   $A  ----   ----      MIN  STD    TAG    MON    ---    ---
 
 Tasks         : Input, Uhr
 Exchanges: URESEX, UCMDEX
 Funktion
 
 Diese MSG dient zum Setzen der Uhrzeit. Es duerfen keine don't care
 Felder ($F) in der Zeit vorkommen.
 -----------------------------------------------------------------------
 
 Aufbau WSTMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 23   $C  ----   ----      MIN  STD    TAG    MON    HZK    DST
 
 Tasks         : Input, Uhr
 Exchanges: URESEX, UCMDEX
 Funktion
 
 Diese Message dient zum Setzen von einem Weckregister in einer der 4
 Weckregistergruppen.
 DST bezeichnet die Gruppe (s. globale Equates DEIN,DAUS,DINC,DDEC).
 HZK bezeichnet den Heizkurvenoffset.
 Hier duerfen don't care-Felder gesetzt werden ($F).
 -----------------------------------------------------------------------
 
 Aufbau URDMSG
 LINK TYPE LEN RESP   HOME      REM  REM+1  REM+2  REM+3  REM+4  REM+5
 xxxx 24   8   Resp.  ----      DST  WNR    ---    ---    ---    ---
 
 Tasks         : Input, Printer, Uhr
 Exchanges: UCMDEX
 Funktion
 
 Diese Message veranlasst den Uhrtask, die gespeicherten Uhr/Weckzeiten
 auszugeben. Response auf diese MSG ist also eine TIMMSG.
 DST gibt die Gruppe an (s. globale Equates DUHR,DAUS,DEIN,DDEC,DINC).
 WNR gibt an, ob der naechste Wecker in der Gruppe (WNR <> 0) oder der
 erste Wecker der Gruppe (WNR = 0) gelesen werden soll.
 Falls DST=DUHR, ist WNR uninteressant.
 -----------------------------------------------------------------------
