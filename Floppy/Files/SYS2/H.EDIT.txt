  1. META-AUSDRUECKE
 
     <Zeil#>        ::= <<maximal 4-Stellige Dezimalzahl>>
     <Zeile>        ::= P | C | L | <Zeil#>
     <Zeil.bereich> ::= A[LL] | <Zeile>[-<Zeile>]
     <Schritt>      ::= <<maximal 4-stellige Dezimalzahl>>
     <Zeil.ber.*>   ::= <Zeil.bereich> | <Zeile>-
 
  2. AUFRUF
 
     <Aufruf> ::= E[XEC] ED[IT][,<Ein-Fl-Datei>] |
                  ED[IT][ <Ein-Fl-Datei>]
 
  3. KOMMANDOS
 
     @
     @CH[ANGE] <Literal>,<Literal>[,<Zeil.bereich>]
     @CMD <<beliebiges abdruckbares ASCII-Zeichen>>
     @DEL[ETE] <Zeil.bereich>
     @D[ISPLAY][ <Zeil.ber.*>]
     @E[ND]
     @FILE <Ein-Fl-Datei>
     @FI[ND] <Literal>[,<Zeil.ber.*>]
     @H[ELP]
     @I[NSERT][ <Zeile>[,<Schritt>]] | @<Zeile>[,<Schritt>]
     @L[AST]
     @M[ODIFY][ <Zeile>]
     @OV[ERWRITE][ <Aus-Fl-Datei>[,<Zeil.bereich>]]
     @QU[IT]
     @REA[D] <Ein-Fl-Name>[,<Zeil#>-<Zeil#>]
     @REN[UMBER][ <Zeil#>[,<Schritt>]]
     @S:<<Monitor-Kommando>>
     @STA[TUS]
     @WRI[TE][ <Aus-Fl-Datei>[,<Zeil.bereich>]]
  
  4. FEHLERMELDUNGEN
 
     41  OVERFLOW (@REN 1,1 ASSUMED)
         Bei einem @REN-Kommando trat ein Ueberlauf der Zeilennummer
         auf. Es wurde ein @REN 1,1 durchgefuehrt.
     42  NOT SAVED (USE @OV, @QU, @WRI OR @DEL ALL)
         Das @END-Kommando wird nur angenommen, wenn der Textbereich lee
         ist, oder wenn der Text seit dem letzten @WRITE oder seit Begin
         nicht veraendert wurde.
         Dadurch wird verhindert, dass man vergisst, einen editierten
         Text auf die Floppy zurueckzuschreiben.
     43  LINE# OVERFLOW
         Die momentane Zeilennummer haette den Wert 9999 ueberschritten
         und wurde daher nicht geaendert.
         Abhilfe: @REN-Kommando verwenden.
