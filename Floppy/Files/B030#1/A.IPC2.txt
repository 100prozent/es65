;**************************************
;**                                  **
;**    TESTPROGRAMM IPC TEIL 2       **
;**                                  **
;**************************************
;
.SC
YSVC
.SS
;
.SG Z,$00
POINT .DS 2
POINT1 .DS 2
;
.SG P,$0800
START JMP IPC2
;
IPC2 .EQ *
SVC OPENI
BVC *+4
SVC TERM
STA :POINT
STY :POINT+1
STA :POINT1
STY :POINT1+1
INC :POINT1
INC :POINT1
INC :POINT1
INC :POINT1
INC :POINT1
INC :POINT1
;
X0 NOP;SVC PASS
LDY #0
X1 LDA (POINT),Y
STA (POINT1),Y
INY
CPY #6
BCC X1
SVC ESCCK
BVC X0
SVC CLOSEI
BVC *+4
SVC TERM
LDA #1
SVC TERM
;
.EN START
