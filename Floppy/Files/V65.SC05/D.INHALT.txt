   
  es65-ASSEMB
  -----------
   
  A.ASS.C + 2:A.ASS.S + A.ASS.K + A.ASS.1 + A.ASS.2 +
  A.ASS.3 + A.ASS.4                     Sources es65-ASSEMB
   
  A.ASV.C + 2:A.ASS.S + A.ASV.1         Sources O.V.ASS.V65
   
   
  Die folgenden Uebersetzungsprozeduren sind nur ablauffaehig, wenn
  sich eine Diskette mit folgenden Dateien auf Laufwerk 2 befindet:
   
   A.ASS.S          V65.SC01
   MAC.MACRO        V65.SYS
   O.SUMME          V65.SYS
   O.V.ASS.V65      V65.SYS
   E.MDB            V65.SYS
   O.V.MDB.DIAS     V65.SYS
 
 
  I.ASS.ASS   es65-ASSEMB uebersetzen, M.ASSEMB erzeugen
              nur in einem Single-User-System oder PSYS moeglich
  I.ASV.ASS   Overlay uebersetzen, O.V.ASS.V65 erzeugen
              nur in einem Single-User-System oder PSYS moeglich
 
  I.ASS.ASS.PRINT es65-ASSEMB mit Liste uebersetzen
  I.ASV.ASS.PRINT Overlay mit Liste uebersetzen
 
