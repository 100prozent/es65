;
;
;   ********************************************************************
;   ***                                                              ***
;   ***      S M - S T R U K T U R  -  M A K R O S  (small)          ***
;   ***      ++++++++++++++++++++++++++++++++++++++++++++++          ***
;   ***                                                              ***
;   ***   Implementierung von:                                       ***
;   ***   -------------------                                        ***
;   *** @IF. <Cond-list> {<con>} @THEN. <stat> {@ELSE. <stat>} @END. ***
;   *** @REPEAT. <stats> @UNTIL. <Cond-list> {<con>} @END.           ***
;   *** @WHILE. <Cond-list> {<con>} @DO. <stats> @END.               ***
;   *** @BEGIN.                                                      ***
;   *** @END.                                                        ***
;   *** @CASE. @OF. <Cond-list> {<con>} @BEGIN. <stats> @END.        ---
;   ***      {@OF. <Cond-list> {<con>} @BEGIN. <stats> @END.         ---
;   ***       @ELSE. <stats> @END.                                   ***
;   ***                                                              ***
;   ********************************************************************
.PG
;
@IF.            .MD (@COND); If-Structure
.SB
.ST @C.ON.D//=@COND
.ST @C.ON.T//=1
               .ME
;
;
@THEN.          .MD
.IE @C.ON.T//-1; after IF
.ST @C.ON.T//=2
.CE
.IE @C.ON.T//-2; after IF-AND
@B.RA.NCH. (@C.ON.D//,M.//.EL)
.ST @C.ON.T//=4
               .MX
.IE @C.ON.T//-3; after IF-OR
@B.RA.NCH. (@C.ON.D//,M.//.EL)
.SC
M.//.TH .EQ *
.ST @C.ON.T//=4
               .MX
? ILLEGAL THEN - IGNORED
               .ME
;
;
@ELSE.          .MD; Else for IF and CASE
.IE @C.ON.T//-4; IF-Else
.ST @C.ON.T//=5
JMP M.//.FI
.SC
M.//.EL .EQ *
               .MX
.IE @C.ON.T//-30
.ST @C.ON.T//=36; CASE-Else
.SC
M.@@.CA .EQ *
.BB
               .MX
? ILLEGAL ELSE - IGNORED
               .ME
;
;
@REPEAT.        .MD; Strucure REPEAT
.SB
.ST @C.ON.T//=20
.SC
M.//.REP .EQ *
               .ME
;
;
@UNTIL.         .MD (@COND)
.ST @C.ON.D//=@COND
.ST @C.ON.T//=21
               .ME
;
;
@WHILE.         .MD (@COND)
.SB
.ST @C.ON.D//=@COND
.ST @C.ON.T//=11
.SC
M.//.WL .EQ *
               .ME
;
;
@DO.            .MD
.IE @C.ON.T//-11
.ST @C.ON.T//=12
.CE
.IE @C.ON.T//-12
@B.RA.NCH. (@C.ON.D//,M.//.OD)
.SC
M.//.DO .EQ *
.ST @C.ON.T//=14
               .MX
.IE @C.ON.T//-13
@B.RA.NCH. (@C.ON.D//,M.//.OD)
.SC
M.//.DO .EQ *
.ST @C.ON.T//=14
               .MX
.IM @C.ON.T//-40
? ILLEGAL DO - IGNORED
               .MX
.IP @C.ON.T//-60
? ILLEGAL DO - IGNORED
               .MX
               .ME
;
;
@BEGIN.        .MD
.IE @C.ON.T//-31
.ST @C.ON.T//=32
.CE
.IE @C.ON.T//-32
@B.RA.NCH. (@C.ON.D//,M.@@.CA)
.ST @C.ON.T//=34
.BB
               .MX
.IE @C.ON.T//-33
@B.RA.NCH. (@C.ON.D//,M.@@.CA)
.SC
M.@@.BG .EQ *
.ST @C.ON.T//=34
.BB
               .MX
.SB
.ST @C.ON.T//=60
               .ME
;
;
@CASE.          .MD
.SB
.ST @C.ON.T//=30
.BB
               .ME
;
;
@OF.            .MD (@COND)
.IE @C.ON.T//-30
.SC
M.@@.CA .EQ *
.BE
.ST @C.ON.D//=@COND
.ST @C.ON.T//=31
.BB
               .MX
? ILLEGAL OF - IGNORED
               .ME
;
;
@AND.           .MD (@COND)
.IE @C.ON.T//-1
.ST @C.ON.T//=2
.CE
.IE @C.ON.T//-2
@B.RA.NCH. (@C.ON.D//,M.//.EL)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-11
.ST @C.ON.T//=12
.CE
.IE @C.ON.T//-12
@B.RA.NCH. (@C.ON.D//,M.//.OD)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-21
.ST @C.ON.T//=22
.CE
.IE @C.ON.T//-22
@B.RA.NCH. (@C.ON.D//,M.//.REP)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-31
.ST @C.ON.T//=32
.CE
.IE @C.ON.T//-32
@B.RA.NCH. (@C.ON.D//,M.@@.CA)
.ST @C.ON.D//=@COND
               .MX
? ILLEGAL AND - IGNORED
               .ME
;
;
@OR.            .MD (@COND)
.IE @C.ON.T//-1
.ST @C.ON.T//=3
.CE
.IE @C.ON.T//-3
@B.RA.NCH. (@C.ON.D//,M.//.TH,INVERS)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-11
.ST @C.ON.T//=13
.CE
.IE @C.ON.T//-13
@B.RA.NCH. (@C.ON.D//,M.//.DO,INVERS)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-21
.ST @C.ON.T//=23
.CE
.IE @C.ON.T//-23
@B.RA.NCH. (@C.ON.D//,M//REN,INVERS)
.ST @C.ON.D//=@COND
               .MX
.IE @C.ON.T//-31
.ST @C.ON.T//=33
.CE
.IE @C.ON.T//-33
@B.RA.NCH. (@C.ON.D//,M.@@.BG,INVERS)
.ST @C.ON.D//=@COND
               .MX
? ILLEGAL OR - IGNORED
               .ME
;
;
@END.           .MD
.IE @C.ON.T//-4
.SC
M.//.EL .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-5
.SC
M.//.FI .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-14
.SC
JMP M.//.WL
M.//.OD .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-21
.ST @C.ON.T//=22
.CE
.IE @C.ON.T//-22
@B.RA.NCH. (@C.ON.D//,M.//.REP)
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-23
@B.RA.NCH. (@C.ON.D//,M.//.REP)
.SC
M//REN .EQ *
.ST @C.ON.T//=0
.SE
               .MX
.IE @C.ON.T//-30
.ST @C.ON.T//=0
.SC
M.@@.CA .EQ *
M//CAE .EQ *
.BE
.SE
               .MX
.IE @C.ON.T//-36
.ST @C.ON.T//=0
.SC
M//CAE .EQ *
.SE
.BE
.BE
               .MX
.IE @C.ON.T//-34
.ST @C.ON.T//=30
JMP M//CAE
.BE
               .MX
.IE @C.ON.T//-40
.ST @C.ON.T//=0
.ST @F.OR.COU=@F.OR.COU-2
PHP
INC B/A.SE1//
BNE *+5
INC B/A.SE1//+1
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-41
.ST @C.ON.T//=0
PHP
SEC
CLD
ADC #0
BCS *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-42
.ST @C.ON.T//=0
PHP
INX
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-43
.ST @C.ON.T//=0
PHP
INY
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-44
.ST @C.ON.T//=0
PHP
INC @C.EL.L.//
BNE *+5
INC @C.EL.L.//+1
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-50
.ST @C.ON.T//=0
.ST @F.OR.COU=@F.OR.COU-2
PHP
PHA
LDA B/A.SE1//
BNE *+5
DEC B/A.SE1//+1
DEC B/A.SE1//
PLA
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-51
.ST @C.ON.T//=0
PHP
SEC
CLD
SBC #1
BCC *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-52
.ST @C.ON.T//=0
PHP
DEX
CPX #$FF
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-53
.ST @C.ON.T//=0
PHP
DEY
CPY #$FF
BEQ *+5
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-54
.ST @C.ON.T//=0
PHP
PHA
LDA @C.EL.L.//
BNE *+5
DEC @C.EL.L.//+1
DEC @C.EL.L.//
PLA
JMP F.O0.1//
.SC
F.EN.01// PLP
.SE
               .MX
.IE @C.ON.T//-60
.ST @C.ON.T//=0
.SE
               .MX
? ILLEGAL END - IGNORED
               .ME
;
;
@B.RA.NCH.        .MD (@B.ED.ING,ZIEL,@I.NV.ERS)
.ST @B.ED..=@B.ED.ING
.IA @I.NV.ERS
.ST @B.ED..=0-@B.ED..
.CE
.IE @B.ED..-$FFFB
BCC *+7
BEQ *+5
JMP ZIEL
               .MX
.IE @B.ED..-5
BCC *+4
BNE *+5
JMP ZIEL
               .MX
.ST @I.NV.E=0
.IA @I.NV.ERS
.ST @I.NV.E=$20
.CE
.IE @B.ED.ING&%100000
.ST @B.ED.I=@B.ED.ING+@I.NV.E
.CE
.IN @B.ED.ING&%100000
.ST @B.ED.I=@B.ED.ING-@I.NV.E
.CE
.IN @B.ED.I&$FF1F-$10
? WRONG CONDITION FOR BRANCH - IGNORED
               .MX
.ST @C.HE.CK=ZIEL-*-2
.IE @C.HE.CK&$FF80
.ST @C.HE.CK=@C.HE.CK&$00FF
.BY @B.ED.I
.BY @C.HE.CK
               .MX
.IE @C.HE.CK&$FF80-$FF80
.ST @C.HE.CK=@C.HE.CK&$00FF
.BY @B.ED.I
.BY @C.HE.CK
               .MX
.ES
; ? BRANCH OUT OF BOUNDS  -  BXX 0  ASSUMED
.EC
.BY @B.ED.I,0
               .ME
;
;
;   ********************************************************************
;   ***                                                              ***
;   ***                 E X T E R N A L  -  M A K R O S              ***
;   ***                 -------------------------------              ***
;   ***                                                              ***
;   ***   Diese Makros sind fuer die Anwendung in Programmen, wel-   ***
;   ***   che die es65-Link-Module verwenden.                        ***
;   ***   Inhalt:                                                    ***
;   ***   GIO: I/O Unterprogramme (IN/OUT)                           ***
;   ***   GIC: IN-CMD-Behandlung (IN/CMD)                            ***
;   ***   GCL: CMD-Lineauswertung Listfile (CMDL/L)                  ***
;   ***   GCA: CMD-Lineauswertung vollstaendig (CMDL/A)              ***
;   ***                                                              ***
;   ********************************************************************
.PG
GIO           .MD
.EX OUTVOR
.EX OUT/V
.EX OUT/V.0
.EX OUT/V.A
.EX OUT/V.CH
.EX OUT
.EX BUFOUT
;
.EX IN/VU.L
.EX IN/VU.T
.EX IN/VU.N
.EX IN/VL.L
.EX IN/VL.T
.EX IN/VL.N
.EX IN
.EX BUFIN
              .ME
;
;
GIC           .MD
.EX IN/CMD
.EX IN/CMD/BUF
.EX IN/CMD/TMO
              .ME
;
;
GCL           .MD
.EX CMDL/L
.EX FCBLST
              .ME
;
;
GCA           .MD
.EX CMDL/A
.EX BUFCMD
              .ME
.TI es65 - Utility-Makros
;
;   ********************************************************************
;   ***                                                              ***
;   ***                 U T I L I T Y  -  M A K R O S                ***
;   ***                 -----------------------------                ***
;   ***                                                              ***
;   ***  SAVE: Registerrettung am Stack                              ***
;   ***  REST: Registererstellung vom Stack                          ***
;   ***  INCD (cells) : Dezimal Increment v. max. 3 <cells>          ***
;   ***  INCDI (ptr,cells): Dezimal Increment v. max. 3 <cells>      ***
;   ***                     indirekt ueber ptr und offset            ***
;   ***  CHEC (cell,lower,higher,I): Pruefung auf Bereichsueber-     ***
;   ***                              schreitung (l <= c <= h)        ***
;   ***                              v=1  --> Fehler                 ***
;   ***  SVC80 (SVC#,ADR,XVAL): Aufruf des SVC#+$80, A,Y..ADR,       ***
;   ***                         X..XVAL.                             ***
;   ***                         ACHTUNG: Wenn ADR, XVAL angegeben    ***
;   ***                                  werden die Register zerst.  ***
;   ***  SEV: Set Overflow (es muss dazu eine Zelle SETV mit $40 de- ***
;   ***       klariert werden.)                                      ***
;   ***  LOD2 (ADR)      : Laden von A,Y (LOB/HOB) mit dem Inhalt    ***
;   ***                    von ADR und ADR+1.                        ***
;   ***                    ACHTUNG: A,Y zerstoert.                   ***
;   ***  MOV2 (SRC,DST)      : Moven von SRC, SRC+1 in DST, DST+1    ***
;   ***  INC2 (ADR)      : Hexadecimal increment von ADR, ADR+1      ***
;   ***  DEC2 (ADR): Hex. decrement von ADR,ADR+1                    ***
;   ***  STO2 (ADR): Abspeich. A,Y in ADR,ADR+1                      ***
;   ***  ADD2 (VAL1,VAL2,ERG): Add von VAL1,VAL1+1 und VAL2,VAL2+1   ***
;   ***                        wenn ERG angegeben, dann nach ERG.    ***
;   ***  SUB2 (VAL1,VAL2,ERG): Sub VAL1,VAL1+1 - VAL2,VAL2+1, analog ***
;   ***  ADD2D (CELL,VAL,ERG): Add #VAL zu Cell, analog ADD2         ***
;   ***  SUB2D (CELL,VAL,ERG): Sub #VAL von Cell, analog ADD2        ***
;   ***  SWTON (SWT,VAL): Setzen eines Schalters auf $FF (oder wenn  ***
;   ***                   angegeben auf VAL.                         ***
;   ***  SWTOFF (SWT,VAL): Loeschen eines Schalters auf 0 (oder wenn ***
;   ***                    angegeben auf VAL).                       ***
;   ***  SWTCK (SWT,REG,VAL): Pruefen, ob der Schalter gesetzt oder  ***
;   ***                       nicht durch Laden des REG mit (SWT).   ***
;   ***  SPL (SYMB,CELL): Aufsplitten einer Symb. Adr. in A,Y (L/H)  ***
;   ***                   wenn CELL angegeben, abspeichern.          ***
;   ***                   ACHTUNG: A,Y zerstoert.                    ***
;   ***                                                              ***
;   ********************************************************************
.PG
;
SAVE           .MD
PHA
TXA
PHA
TYA
PHA
               .ME
;
;
REST           .MD
PLA
TAY
PLA
TAX
PLA
               .ME
;
;
INCD           .MD (CELL1,CELL2,CELL3)
SED
CLC
LDA CELL1
ADC #1
STA CELL1
.IG CELL2
LDA CELL2
ADC #0
STA CELL2
.CE
.IG CELL3
LDA CELL3
ADC #0
STA CELL3
.CE
CLD
               .ME
;
;
CHEC          .MD (CELL,FROM,TO,I)
LDA CELL
BIT SETV
.IA I
CMP #FROM
.CE
.IG I
CMP FROM
.CE
BCC CH.E##
.IA I
CMP #TO
.CE
.IG I
CMP TO
.CE
BCC *+4
BNE *+3
CLV
CH.E## .EQ *
               .ME
;
;
INCDI          .MD (PTR,OFF1,OFF2,OFF3)
SED
CLC
LDY #OFF1
LDA (PTR),Y
ADC #1
STA (PTR),Y
.IG OFF2
LDY #OFF2
LDA (PTR),Y
ADC #0
STA (PTR),Y
.CE
.IG OFF3
LDY #OFF3
LDA (PTR),Y
ADC #0
STA (PTR),Y
.CE
CLD
               .ME
;
;
SVC80          .MD (NAME,ADR,XVAL)
.IG XVAL
LDX #XVAL
.CE
SVC NAME+$80
.IG ADR
.AD ADR
.CE
               .ME
;
;
SEV            .MD
BIT SETV
               .ME
;
;
INC2           .MD (VEKTOR)
INC VEKTOR
BNE AZ##
INC VEKTOR+1
AZ## .EQ *
               .ME
;
;
DEC2           .MD (VEKTOR)
LDA VEKTOR
BNE AZ##S
DEC VEKTOR+1
AZ##S DEC VEKTOR
               .ME
;
;
LOD2           .MD (CELL)
LDA CELL
LDY CELL+1
               .ME
;
;
ADD2           .MD (SUM1,SUM2,ERG)
.IG ERG
CLC
LDA SUM1
ADC SUM2
STA ERG
LDA SUM1+1
ADC SUM2+1
STA ERG+1
               .MX
CLC
LDA SUM1
ADC SUM2
LDA SUM1+1
ADC SUM2+1
               .ME
;
;
SUB2           .MD (SUB1,SUB2,ERG)
.IG ERG
SEC
LDA SUB1
SBC SUB2
STA ERG
LDA SUB1+1
SBC SUB2+1
STA ERG+1
               .MX
SEC
LDA SUB1
SBC SUB2
LDA SUB1+1
SBC SUB2+1
               .ME
;
;
ADD2D          .MD (CELL,VAL,ERG)
.IG ERG
CLC
LDA CELL
ADC #<VAL
STA ERG
LDA CELL+1
ADC #>VAL
STA ERG+1
               .MX
CLC
LDA CELL
ADC #<VAL
LDA CELL+1
ADC #>VAL
               .ME
;
;
SUB2D          .MD (CELL,VAL,ERG)
.IG ERG
SEC
LDA CELL
SBC #<VAL
STA ERG
LDA CELL+1
SBC #>VAL
STA ERG+1
               .MX
SEC
LDA CELL
SBC #<VAL
LDA CELL+1
SBC #>VAL
               .ME
;
;
MOV2 .MD (SRC,DST)
LDA SRC
STA DST
LDA SRC+1
STA DST+1
               .ME
;
SWTON             .MD (SWT,VAL)
.IG VAL
LDA #VAL
.CE
.IA VAL
LDA #$FF
.CE
STA SWT
               .ME
;
;
SWTOFF            .MD (SWT,VAL)
.IA VAL
LDA #0
.CE
.IG VAL
LDA #VAL
.CE
STA SWT
               .ME
;
;
SWTCK          .MD (SWT,REG)
.IS REG,@A
LDA SWT
               .MX
.IS REG,@X
LDX SWT
               .MX
.IS REG,@Y
LDY SWT
               .MX
? WRONG REGISTER-IDENTIFIER - IGNORED
               .ME
;
;
SPL           .MD (VAL,ADR)
LDA #<VAL
LDY #>VAL
.IG ADR
STA ADR
STY ADR+1
.CE
              .ME
;
;
STO2          .MD (ADR)
STA ADR
STY ADR+1
              .ME
;
