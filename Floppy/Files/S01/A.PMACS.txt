.TI PASCAL-MAKROS
;
;   ********************************************************************
;   ***                                                              ***
;   ***                 P A S C A L  -  M A K R O S                  ***
;   ***                 ---------------------------                  ***
;   ***                                                              ***
;   ***  Diese Makros dienen zur Erstellung des Pascal-Systemes.     ***
;   ***  Vorhanden sind:                                             ***
;   ***  - YSBIOS  Vereinbarungen des SBIOS (Dummy-Segment)          ***
;   ***  - SBIOS (name) : Aufruf eines SBIOS                         ***
;   ***  - EPERIP Vereinbarung der es65 Peripherie                   ***
;   ***  - EMUC   Vereinbarung der MUC-Promprogramme                 ***
;   ***  - RETURN Ruecksprung eines SBIOS-Calls                      ***
;   ***  - RETINT Return from Interrupt                              ***
;   ***                                                              ***
;   ********************************************************************
.PG
;
YSBIOS      .MD
.SG Y,0
SYSINIT     .DS 3
SYSHALT     .DS 3
CONINIT     .DS 3
CONSTAT     .DS 3
CONREAD     .DS 3
CONWRIT     .DS 3
SETDISK     .DS 3
SETTRAK     .DS 3
SETSECT     .DS 3
SETBUFR     .DS 3
DSKREAD     .DS 3
DSKWRIT     .DS 3
DSKINIT     .DS 3
DSKSTRT     .DS 3
DSKSTOP     .DS 3
PRNINIT     .DS 3
PRNSTAT     .DS 3
PRNREAD     .DS 3
PRNWRIT     .DS 3
REMINIT     .DS 3
REMSTAT     .DS 3
REMREAD     .DS 3
REMWRIT     .DS 3
USRINIT     .DS 3
USRSTAT     .DS 3
USRREAD     .DS 3
USRWRIT     .DS 3
CLKREAD     .DS 3
            .ME
;
;
EMUC        .MD
SBIOS       .EQ $FDA7
END/DR      .EQ $FC0F
ERR/OR      .EQ $FC12
FNMRAD      .EQ $F7A8
READS       .EQ $FC06
SECTOR      .EQ $F0
SEEK        .EQ $FC09
WAIT        .EQ $FC0C
RET         .EQ $FC18
RETI        .EQ $FC15
            .ME
;
;
EPERIP      .MD
Z1ZU        .EQ $F8D4
Z2ZU        .EQ $F8D5
Z3ZU        .EQ $F8D6
Z4ZU        .EQ $F8D7
;
Z1WEG       .EQ $F8DC
Z2WEG       .EQ $F8DD
Z3WEG       .EQ $F8DE
Z4WEG       .EQ $F8DF
;
VACAS1      .EQ $F814; Acia User 1
VACAS1/A    .EQ VACAS1-4; Acia USR 1 (Applikation)
VFC         .EQ $F800
VIAMUC      .EQ $F8F0
VIAPRT      .EQ $F880
OUTUMAP     .EQ $F8E8
OUTPMAP     .EQ $F8E0
OUTNMN      .EQ $F8D0
OUTNMJ      .EQ $F8D8
OUTIDL      .EQ $F8D9
OUTIDF      .EQ $F8D1
OUTNMP      .EQ $F8DA
            .ME
;
;
BIOS          .MD (SYMB)
JSR SBIOS+SYMB
              .ME
;
;
RETURN      .MD
JMP RET
            .ME
;
;
RETINT      .MD
JMP RETI
            .ME
