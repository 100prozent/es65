Floppy Drive: BASF 6106 (Single Side, Single or double Density)
on the floppy controller Pin 37 (/DINT or /DDEN) is high - that
suggests FM (single density).

Jumpers on Floppy drive:
JJ1: (1-2) or (2-3)
Drive# 1 or 2
2D: (7-8 and 11-12)
Radial Head Load (The head will be loaded only
if the inserted mini disk rotates)
3D:
nothing
4D: (3-4, 5-6 and 13-14)
ACTIVITY LED: Door Lock
Pin34: In USE
5D: (9-10, 11-12 and 13-14)
Door Lock: IN USE + IN USE FF


Modifying an adapter from 5.25" to 3.5" floppies for a gotek:
disconnect PIN2 and PIN6,
connect PIN2 from Gotek to Pin6 of Host (RDY Signal)
Enable the RDY Signal on Pin2 in the gotek config.
Pin34 is not connected.

es65_filesystem.hsl is a struct for Hex Worksop from Breakpoint Software
Floppy Format:
40 Tracks, 10 Sectors with 256 Bytes, Single Sided
first Track (2560 Bytes) is System (FAT and Directory), Data is from 0xA00.
Smallest addresseable unit is 256 Byte
maximum 32 files, no subdirectories

Disk Header 0-1FF (see DISK_STRUCT)
Directory 200-BFF (see FILE_ENTRY * 32)
Data      A00-18FFF (99840 Byte capacity)

typedef struct FILE_ENTRY      // struct for a directory entry
{
    char        Filename[32];
    char        firstUser[8];
    BYTE        userID[3];
    BYTE        Version;
    BYTE        dn[1];          // always 0
    BYTE        File_ID[1];     // This links the file with the sector table
    BYTE        Sectors[1];     // sectors used
    BYTE        dn[1];          // always 0
    char        lastUser[8];
    BYTE        userID[3];
    BYTE        always_zero[5];
} FILE_ENTRY;

struct DISK_STRUCT           // struct for the whole disk (placed @ 0)
{
    CHAR        DiskName[8] ;   // Name for mount command!
    BYTE        emptyMarker[8]; // just 8 * e5
    char        firstUser[8];   
    BYTE        userID[3];      // User who created the disk
    BYTE        ErrorCnt[2];
    BYTE        Sectors[3];     // sectors that are worked on
    char        lastUser[8];    // last User who worked the disk
    BYTE        userID[3];
    WORD        mounts;
    BYTE        emptyMarker[3]; // just 3 * e5
    BYTE        FileStatus[16]; // one nibble per file
    BYTE        always_ff[48];
    BYTE        sectorTable[0x190] ; // This shows all the sectors (40*10 or 0x190)
    FILE_ENTRY file_entry [32];      // 00: not used, 01,02: system, 03-0x22 are the fileIDs,
} ;                                  // The number shows which file occupies a sector

unclear:
. how defective sectors get marked
. how does the filestatus work
. do the zero bytes in the file entry have a purpose (7 Byte per file)
. do the ff and e5 bytes in the header have a purpose (48 Bytes, 11 bytes)
  (the total slack would be 283 Bytes if not)


Files in this directory:
R1_back.img: a new file which i made and used with a gotek (copied a file onto it)
R1_del.img: here i deleted the file and looked for changes
s01_ss_scp.img:
s02_ss_scp.img: bad image files i recovered with greaseweazle
make_fd_image.sh: obsolete shell script
es65_irw: python script in progress (can read imagefiles to dir (finished), show directory (works), create new files (works) and write files from dir to image (not started))
es65_filesystem.hsl: struct for Hex Workshop
FF.CFG: GOTEK config
IMG_es.CFG: GOTEK config

File Types:
the es-65 knows 12 filetypes:
A. - Assembler Source                               # Textfile with Linenumbers
B. - Basic File                                     # Textfile with Linenumbers
BAS. - Basic File                                   # Textfile with Linenumbers
C. - Syscopy File (made by SCOPY)                   # Textfile without Linenumbers
D. - Documentation                                  # Textfile with Linenumbers
E. - Program (to be launched with the EXEC command) # Binary
H. - Help file                                      # Textfile with Linenumbers
I. - Procedure file (for the SIN Command)           # Textfile with Linenumbers
L. - Assembler Listing                              # Textfile with Linenumbers
M. - Memory dump (from debugger)                    # Binary (works with es65_M_to_BIN.py)
O. - Object file                                    # Binary
T. - Testfile (from ASYS and for Debugger)          # ?
V. - Overlay module                                 # ?
W. - Work file                                      # ?
X. - ? file                                         # seems to be a Textfile with Linenumbers

A., B., D., H., I., L., X., and maybe W. are Textfiles with Linenumbers (python conversion tool works)
C. and BAS and MAC are Textfiles without Linenumbers (python conversion tool works))
E., M., O. are binary
T., V. - no idea

Bootable Disks have a 'C'(0x43) for Data or an 'E' (0x45) for End on the first Byte of the control block.
This indicates a write to RAM.
See the sourcecode A.BOG for further documentation.

struct CONTROL_BLOCK           // struct for the Control Block (placed @ beginning of file)
{
    CHAR        Check      ;   // Check if Bootable Disk (Must be 'C' or 'E')
    BYTE        BANK       ;   // If zero than Common ROM from 0x8000, else 0xA000
    WORD        DEST       ;   // Destination Address
    BYTE        SECS       ;   // Number of Sectors to write (usually 0x20 * 256 => 8k)
    BYTE    zeros[251]     ;   // Empty
    BYTE    DATA[SECS*256] ;   // Data
};

Just the code from offset 0x100 which is written to RAM.

##########################################################################################
Bootstrap Routine:

To Floppy Controller                              | From Floppy Controller
sta 0x80 @ F804 (Motor on)
sta 0xF6 @ F804 (Motor on, Select 1+2+3, In use)
sta 0x80 @ F804 (Motor on)
sta 0x88 @ F804 (Motor on, Not In use)
sta 0xC8 @ F804 (Motor on, Select 1, Not In use)
sta 0xC0 @ F804 (Motor on, Select 1, In use)
                                                  | Wait for Ready
                                                  | Wait
sta 0x0D @ F800 (Restore, HDLD, Ver., Step 12ms   |
                                                  | Wait for IRQ
sta 0x5D @ F800 (Stepin)
                                                  | Wait for IRQ
Check for ERROR
sta TRACK @ F802
sta 0x84 @ F800 (Read single rec. 15ms dly.)
                                                  | Wait for IRQ
####### DMA Happens Here (256 Byte)
Check for ERROR



