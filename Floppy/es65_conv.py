#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, sys, argparse, os, string, math

################################################## Helper functions

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result

def val_to_decval(v):
	if v <= 9:
		return v
	else:
		return int(math.ceil(10*v/16))

def bytes_to_dec(bytes):
    result = 0
    for b in bytes:
        result = result * 100 + int(val_to_decval(b))
    return result

def number_to_linenumber(number):
    return ('{:04}'.format(number) + ": ")

def linenumber_to_es65line(v): # es65 wants linenumbers in decimal representation so 0x12, 0x10
                               # is line 1210 !
	return int(math.floor(v/10)*16) + (v%10) # this shows linenumber 

def remove_zeros(l): # removes zeros from end of file
    for ele in reversed(l):
        if not ele:
            del l[-1]
        else:
            break
    return l

def pad_zeros(f): # pad to full 256 byte block (maybe not neccessary)
	z = math.ceil(len(f)/256)*256-len(f)

	print("length: %x %d adding: %d" % (len(f), len(f), z))
	b = list(bytearray(z))
	return f + b

################################################## Helper functions END

def make_beautiful_line(FILE, nr, txt, showln):
	if showln == True:
		FILE += bytearray(number_to_linenumber(nr), "ascii") #Show Linenumbers

	FILE += bytearray(txt)
	
	FILE += (bytearray("\n", "ascii")) # Line endings for Linux
	#FILE += (bytearray("\r", "ascii")) # Line endings for Mac
	#FILE += (bytearray("\r\n", "ascii")) # Line endings for Windows

def parse_text_file(l, showlines):
	nr = 0
	length = 0
	result = []
	strchar = '\r\n'
	for i in l:
		nr += 10
		line = i.rstrip(strchar) # strip linend
		#print("Number: %d Line: %s" % (nr,line))
		if line != '':
			if str.isnumeric(line[0]) == True and len(line) > 1: # sometimes ther is a '0' or '1' at first position but NOT a linenumber
				if str.isnumeric(line[1]) == True:
					line = line [6:]
		if showlines ==  True:
			length = len(line) + 2
		else:
			length = len(line)

		result.append(length)
		if showlines ==  True:
			result.append(linenumber_to_es65line(int(math.floor(nr/100)))) #high digits of linenumber (12 of 1210)
			result.append(linenumber_to_es65line((nr%100)))                #low digits (00 of 1210)
		for c in range(len(line)):
			result.append(ord(line[c]))
		#print("0x%02x %04d %s" % (length, nr, line))
	return result

def parse_es65_file(f, FILE, showln):
	f = remove_zeros(f)
	startidx = 0
	linenr = 0
	i = 0
	has_lenenubers = bytes_to_dec(f[startidx + 1:startidx + 2]) # second byte is zero if there are are the linenumber
	if has_lenenubers == 0:
		print("This file has linenumbers")
	else:
		print("This file does not have linenumbers")
		showln = False

	while i < len(f):
		linel = bytes_to_int(f[startidx:startidx + 1])  # first byte is length
		i = startidx + linel + 1
		if has_lenenubers == 0:
			linenr = bytes_to_dec(f[startidx + 1:startidx + 3]) # second and third byte are the linenumber
			text = f[startidx + 3:i]  # the rest is text
		else:
			text = f[startidx + 1:i]  # the rest is text

		make_beautiful_line(FILE, linenr, text, showln)
		startidx = i
		#print("linel: %d linenr: %d text: %s" % (linel, linenr, text))

class ES65_CONVERTER(object):
	def process_command_line(self):

		##
		## Parse command line
		##

		parser = argparse.ArgumentParser(description = 'Reads and Writes es65 (Text)Files - v1.0')
		parser.add_argument(
				'-v', '--verbose',
				action = 'count',
				help = 'increase logging level')

		parser.add_argument(
				'-w', '--overwrite',
				action = 'store_true',
				help = 'overwrites existing file')

		parser.add_argument(
				'-d', '--delete',
				action = 'store_true',
				help = 'deletes original file')

		subparsers = parser.add_subparsers(
				dest = 'command',
				title = 'es65_conv operations',
				description = '(See "%(prog)s COMMAND -h" for more info)',
				help = '')
	
		subparser = subparsers.add_parser(
				'read',
				help = 'reads es65 file and converts it to text')

		subparser.add_argument(
				'-s', '--showlines',
				#action = 'count',
				action = 'store_true',
				help = 'show linenubers in output file')

		subparser.add_argument(
				'file',
				nargs = '?',
				#type = _parse_input_file,
				#default = sys.stdin.buffer,
				help = 'input filename [default: use stdin]')

		subparser = subparsers.add_parser(
				'write',
				help = 'writes es65 file from textfile')

		subparser.add_argument(
				'-l', '--lines',
				#action = 'count',
				action = 'store_true',
				help = 'use if the es65 File has linenumbers (A., B., D., H., I., L.) otherwise dont use')

		subparser.add_argument(
				'file',
				nargs = '?',
				#type = _parse_output_file,
				#default = sys.stdout.buffer,
				help = 'output filename [default: use stdout]')

		args = parser.parse_args()
	
		if not args.verbose:
			level = logging.ERROR
		elif args.verbose == 1:
			level = logging.INFO
		else:
			level = logging.DEBUG
		logging.basicConfig(stream = sys.stderr, level = level, format = '%(message)s')

		##
		## Handle command
		##
		
		if args.command == 'write':
			self.write_file(args.file, args.lines)
		elif args.command == 'read':
			self.read_file(args.file, args.showlines, args.overwrite, args.delete)

	def write_file(self, fn, showlines):
		#print("Write file in es65 format")
		print("Read file:", fn)
		with open(fn,"r", encoding='utf8') as f:
			lines = f.readlines()
		print("Size: ", len(lines), "Lines")
		
		arr = pad_zeros(parse_text_file(lines, showlines))
		#print(arr)
		path, tail = os.path.split(fn)
		name = path + "/" + tail[:-4].upper() # strip the '.txt' and uppercas
		#name = "./" + (fn[:-4]).upper() # strip the '.txt'
		#name += "_" # Debugging - not to overwrite the original file
		print("Writing file:", name)
		#print("Writing file:", path + "/" + name)
		if False:
			pass
		#if os.path.isfile(name) == True:
		#	print("File %s exists!" % name)
		#	raise SystemExit
		else:
			with open(name,"wb") as f:
			#with open(path + "/" + name,"wb") as f:
				f.write(bytearray(arr))
				f.close()

	def read_file(self, fn, showln, overwrite, delete):
		#print("Reads file in es65 format")
		print("Read file:", fn)
		if os.path.isfile(fn) != True:
			raise SystemExit
		with open(fn,"rb") as f:
			imagefile = bytearray(f.read())
		print("Size: ", len(imagefile), "Bytes")
		
		#print(overwrite)
		#raise SystemExit

		FILE = []
		parse_es65_file(imagefile, FILE, showln)
		#print(FILE)

		name = fn + ".txt" 
		print("new file: ", name)
		if overwrite == True:
			with open(name,"wb") as f:
				f.write(bytearray(FILE))
				f.close()
		else:
			if os.path.isfile(name) == True:
				print("File %s exists!" % name)
				raise SystemExit
			else:
				with open(name,"wb") as f:
					f.write(bytearray(FILE))
					f.close()
		if delete == True:
			os.remove(fn)

if __name__ == "__main__":
	ES65_CONVERTER().process_command_line()