#!/bin/bash

irw="./es65_irw.py extract"
if [ -z "$1" ]
then
    echo "Argument is empty! We dont want to delete all .txt files in root."
    exit
fi

echo "Reading dir: " $1
for file in $1/*.img
do
    echo "---------------- EXTRACTING:                 "$file
    $irw $file
done