#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, sys, argparse, os, string, math
#import glob

#from struct import *
#from typing import NamedTuple
#import ctypes
#from struct import Struct

#from es65_irw import ES65_READER1
def number_to_hex_byte(number):
    return ("0" + hex(number)[2:])[-2:]

def print_bytes_as_hex(bytes):
    for byte in bytes:
        print(number_to_hex_byte(byte), end=" ")
    print()


def hex_to_number(hex):
    return (int(hex, 16))


def number_to_hex_word(number):
    return ("0" + hex(number)[2:])[-4:]

def number_to_hex_word3(number):
    return ("0" + hex(number)[2:])[-6:]


def bytes_to_addr(hh, ll):
    """ takes two hex bytes e.g. d0, 20 and returns their int address e.g. 53280"""
    return (int(hh, 16) << 8) + int(ll, 16)

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result

def bytes_to_int_r(bytes):
    result = 0
    for b in reversed(bytes):
        result = result * 256 + int(b)
    return result

def int_to_bytes(value, length):
    result = []
    for i in range(0, length):
        result.append(value >> (i * 8) & 0xff)
    result.reverse()
    return result

def bytes_to_hex_word(bytes):
	result = number_to_hex_word3(bytes_to_int(bytes))
	#return number_to_hex_word(3)
	return result

def bytes_to_hex_word2(bytes):
	result = number_to_hex_word(bytes_to_int(bytes))
	#return number_to_hex_word(3)
	return result

def bytes_to_hex_word_r(bytes):
	result = number_to_hex_word3(bytes_to_int_r(bytes))
	#return number_to_hex_word(3)
	return result

def print_bytes_array(bytes_table):
    for byte in bytes_table:
        print(byte)

def bytes_to_string(bytes):
		return bytes.decode("ascii")

def get_hdr(Disk, f):
	Disk['hdr_dname']   = bytes_to_string(f[0:8])
	Disk['hdr_fuser']   = bytes_to_string(f[0x10:0x18])
	Disk['hdr_fuserid'] = bytes_to_int(f[0x18:0x1b])
	Disk['hdr_error']   = bytes_to_int_r(f[0x1b:0x1d])
	Disk['hdr_sector']  = bytes_to_int_r(f[0x1d:0x20])
	Disk['hdr_luser']   = bytes_to_string(f[0x20:0x28])
	Disk['hdr_luserid'] = bytes_to_int(f[0x28:0x2b])
	Disk['hdr_mounts']  = bytes_to_int_r(f[0x2b:0x2d])
	Disk['hdr_dn']      = (f[0x30:0x40])
	Disk['hdr_fat']     = f[0x70:0x200]

def print_dir_line(bytes):
	for line in bytes:
		f = bytearray(line)
		if f[0] != 0x20:
			filename = bytes_to_string(f[0:0x20])
			firstuser = bytes_to_string(f[0x20:0x28])
			fuid = bytes_to_hex_word(f[0x28:0x2b]).rjust(6, '0')
			version = bytes_to_hex_word_r(f[0x2b:0x2d]).rjust(4, '0')
			sector = bytes_to_hex_word_r(f[0x2b:0x2d]).rjust(4, '0')
			lastuser = bytes_to_string(f[0x30:0x38])
			luid = bytes_to_hex_word(f[0x38:0x3b]).rjust(6, '0')
			print(filename, version, sector, firstuser, fuid, lastuser, luid)

def decode_directory2(l):
	n = 0x40
	entry = []
	i = 0
	while i < len(l):
		if i+n < len(l):
			entry.append(l[i:i+n])
		else:
			entry.append(l[i:len(l)])
		i += n
	print_dir_line(entry)

def app_entry(Disk, f, n, all):
	sz = 0x40
	Disk["entries"][n] = {}
	Disk["entries"][n]["stored"] = []
	Disk["entries"][n]["Data"] = []
	Disk["max_entries"] += 1
	Disk["entries"][n]["filename"] = bytes_to_string(f[0x00:0x20])
	Disk["entries"][n]["fuser"]    = bytes_to_string(f[0x20:0x28])
	Disk["entries"][n]["fuserid"]  = bytes_to_int   (f[0x28:0x2b])
	Disk["entries"][n]["version"]  = bytes_to_int   (f[0x2b:0x2c])
	Disk["entries"][n]["dn"]       = bytes_to_int   (f[0x2c:0x2d])
	Disk["entries"][n]["file_id"]  = bytes_to_int   (f[0x2d:0x2e])
	#Disk["entries"][n]["sectors"]  = bytes_to_int   (f[0x2e:0x2f])
	Disk["entries"][n]["dn2"]      = bytes_to_int   (f[0x2f:0x30])
	Disk["entries"][n]["luser"]    = bytes_to_string(f[0x30:0x38])
	Disk["entries"][n]["luserid"]  = bytes_to_int   (f[0x38:0x3b])

	sect = 0
	for i in range(len(Disk['hdr_fat'])):  # search for fileID in FAT
		if Disk['hdr_fat'][i] == Disk["entries"][n]["file_id"]: # if found append sector number
	# This is wrong sectors is NOT @ 2e, it is just counted how many references are in te FAT
#			if sect < Disk["entries"][n]["sectors"]:  # dismisss old entries above filesize
#				start = 256 * i
#				Disk["entries"][n]["stored"].append(start)
#				Disk["entries"][n]["Data"].append((all[start:start+256]))
#				sect += 1
			start = 256 * i
			Disk["entries"][n]["stored"].append(start)
			Disk["entries"][n]["Data"].append((all[start:start+256]))
			sect += 1

		Disk["entries"][n]["sectors"]  = sect
#	print("Name: %s sectors: %d stored: %d sect: %d" %  (
#		Disk["entries"][n]["filename"],
#		Disk["entries"][n]["sectors"],
#		len(Disk["entries"][n]["stored"]),
#		sect))
#	if Disk["entries"][n]["sectors"] != len(Disk["entries"][n]["stored"]):
#		print("-----File Read Error!----- in: ", Disk["entries"][n]["filename"])

def makeNewDisk(f):
	myArr = bytearray([0xE5] * 102400)
	name = f.ljust(8,' ')
	myArr[0:8] = bytes(name,"ascii")             # Diskname
	myArr[0x08:0x10] = bytes("@BERANEK","ascii") # Hidden DiskID (can stay E5)
	myArr[0x10:0x18] = bytes("F       ","ascii") # First User
	#myArr[0x18:0x1b] = [0x81, 0x04, 0x16]       # First Date
	myArr[0x18:0x1b] = [0x81, 0x12, 0x17]        # First Date
	myArr[0x1b:0x1d] = [0, 0]                    # ErrorCount (can stay E5)
	myArr[0x1d:0x20] = [0x45, 0x12, 0]          # Sectors worked on(can stay E5)
	#myArr[0x1d:0x20] = [0, 0, 0]                 # Sectors worked on(can stay E5)
	myArr[0x20:0x28] = bytes("F       ","ascii") # last User
	#myArr[0x28:0x2b] = [0x82, 0x09, 0x18]       # last Date
	myArr[0x28:0x2b] = [0x82, 0x01, 0x04]        # last Date
	myArr[0x2b:0x2d] = [0x61, 0]                 # Mounts (can stay E5)
	myArr[0x2d:0x30] = [0x59, 0xA6, 0xA7]        # Hidden ID? (can stay E5)

	s = 3
	for i in range(0x30,0x40,2): # File Status
		myArr[i:i+2] = [s,0x80]
		s += 1

	for i in range(0x40,0x70): # must be FF 
		myArr[i:i+1] = [0xff]

	for i in range(0x70,0x200): # FAT Table
		myArr[i:i+1] = [0x00]
	myArr[0x70:0x7a] = [1, 1, 2, 2, 2, 2, 2, 2, 2, 2] # System Sectors

	for i in range(0x200, 0xa00): # Directory 
		myArr[i:i+1] = [0x20]
		#pass
	return myArr

def get_dir(Disk, f):
	sz = 0x40
	Disk["entries"] = {}
	Disk["max_entries"] = 0
	i = 0
	n = 0
	d = f[0x200:0xa00]
	while i < len(d):
		if i+sz < len(d):
			if d[i] != 0x20:
				app_entry(Disk, d[i:i+sz], n, f)
				n += 1
		else:
			if d[i] != 0x20:
				app_entry(Disk, d[i:len(d)], n, f)
				n += 1
		i += sz

def getKeysByValue(dictOfElements, valueToFind):
    listOfKeys = list()
    listOfItems = dictOfElements.items()
    for item  in listOfItems:
        if item[1] == valueToFind:
            listOfKeys.append(item[0])
    return  listOfKeys

def _parse_input_file(fn):
	#print("Read image:", fn, end = ' -> ')
	with open(fn,"rb") as f:
		imagefile = bytearray(f.read())
	#if  len(imagefile) == 102400:
		#print("Size looks good.")
	#else:
		#print("Error! Size differs from 10240 Bytes. It is: ", len(imagefile), "Bytes")
	f.close()
	return (imagefile)

def check_path(p):
	dirnum = 1
	print("Checking: %s" % p)
	dirname = p
	while os.path.isdir(dirname) == True:
		print("--------------------------------------------- Directory %s exists!" % (dirname))
		dirname = dirname + '#' + str(dirnum)
		dirnum += 1
		#print(dirname)
	return dirname

def encode_directory(f, img, id):
	path, tail = os.path.split(f)
	print("Adding %s" % (f))
	# ---  Adding zeros to full Number of blocks
	fdata = _parse_input_file(f)
	flen = len(fdata)
	blocks = math.ceil(flen/256)
	diff = blocks * 256 - flen
	if diff > 0:
		fdata.extend(bytearray(diff))
		#print("Length: %d, difference to full block: %d Blocks: %d" % (flen, diff, blocks))
	# ---  Writing file in FAT
	fatstart = 0x70
	fatentry = fatstart
	fatend = 0x1ff
	while img[fatentry] != 0x00:
		fatentry += 1
	# --- found empty space
	print("Empty block in FAT found @ %0x" % fatentry)
	if fatentry > fatend:
		print("##################### ERROR FILES TOO LARGE FOR IMAGE ! ##############################")
	img[fatentry:fatentry + blocks] = bytearray(blocks * [id])
	# ---  Looking for empty space in directory
	datstart = 0xa00
	dirstart = 0x200
	while img[dirstart] != 0x20:
		dirstart += 0x40
	if dirstart > datstart:
		print("##################### ERROR TOO MANY FILES ! ##############################")		
	print("Empty place found @ %0x" % dirstart)
	img[dirstart:dirstart + 0x20] = bytes('{:32s}'.format(tail), "ascii")
	img[dirstart + 0x20:dirstart + 0x28] = bytes("F       ","ascii") # first User
	img[dirstart + 0x28:dirstart + 0x2b] = [0x81, 0x12, 0x19] # first Date
	#img[dirstart + 0x28:dirstart + 0x2b] = [0x82, 0x09, 0x18] # first Date
	img[dirstart + 0x2b:dirstart + 0x2c] = [1] # Version
	img[dirstart + 0x2c:dirstart + 0x2d] = [0] # don't know
	img[dirstart + 0x2d:dirstart + 0x2e] = [id] # file ID
	img[dirstart + 0x2e:dirstart + 0x30] = [0x66, 0x00] # don't know
	img[dirstart + 0x30:dirstart + 0x38] = bytes("F       ","ascii") # last User
	img[dirstart + 0x38:dirstart + 0x3b] = [0x81, 0x12, 0x19] # last Date
	#img[dirstart + 0x38:dirstart + 0x3b] = [0x82, 0x09, 0x18] # last Date
	img[dirstart + 0x3b:dirstart + 0x40] = [0,0,0,0,0] # don't know
	img[datstart:datstart + len(fdata)] = fdata
	#print("Length: %x, End: %x Blocks: %x" % (flen, len(fdata)+0xA00, math.ceil(flen/256)))
	# ---  Set the file as active in the header
	fidstart = 0x30
	while img[fidstart] != id:
		fidstart += 1
	img[fidstart + 1] = 0
	print("Blocks free: %d" % (400 - ((fatentry + blocks) - fatstart)))
	return img

#def add_file_to_image(f, img, id):
#	#img[0x84] = ord('A')
#	path, tail = os.path.split(f)
#	img = encode_directory(tail, img, id)
#	print("Adding %s" % (tail))
#	return img

def get_filelist(d):
	#print("i am in %s " % (d))
	img = _parse_input_file(d + ".img")
	id = 3
	print("Size1: %d" % len(img))
	for root, dirs, files in os.walk(d):
	#files = [f for f in glob.glob(d + "**/*")]
		for fn in files:
			#print("Adding %s to image %s" % (fn, str(root) + ".img"))
			img = encode_directory(d + "/" + fn, img, id)
			id += 1
	print("Size2: %d" % len(img))
	with open(d + ".img","wb") as f:
		f.write(bytearray(img))
		f.close()	

class ES65_READER(object):
	def process_command_line(self):

		##
		## Parse command line
		##
	
		parser = argparse.ArgumentParser(description = 'Extracts and Compresses es65 Floppy Images - v1.01')
		parser.add_argument(
				'-v', '--verbose',
				action = 'count',
				help = 'increase logging level')

		parser.add_argument(
				'-w', '--overwrite',
				action = 'store_true',
				help = 'overwrites existing file')

		subparsers = parser.add_subparsers(
				dest = 'command',
				title = 'Image Read Write - operations',
				description = '(See "%(prog)s COMMAND -h" for more info)',
				help = '')
	
		subparser = subparsers.add_parser(
				'dir',
				help = 'lists directory of imagefile')

		subparser.add_argument(
				'file',
				nargs = '?',
				type = _parse_input_file,
				default = sys.stdin.buffer,
				help = 'input filename [default: use stdin]')

		subparser = subparsers.add_parser(
				'new',
				help = 'creates empty imagefile ')

		subparser.add_argument(
				'file',
				nargs = '?',
				#type = _parse_output_file,
				default = "NEW",
				help = 'output filename [default: use stdout]')
	
		subparser = subparsers.add_parser(
				'extract',
				help = 'reads imagefile and puts files in directory')

		subparser.add_argument(
				'file',
				nargs = '?',
				#type = _parse_input_file,
				#default = sys.stdin.buffer,
				help = 'input filename [default: use stdin]')

		subparser = subparsers.add_parser(
				'compress',
				help = 'writes imagefile from files in directory')

		subparser.add_argument(
				'file',
				nargs = '?',
				#type = _parse_output_file,
				#default = sys.stdout.buffer,
				help = 'output filename [default: use stdout]')

		args = parser.parse_args()
	
		if not args.verbose:
			level = logging.ERROR
		elif args.verbose == 1:
			level = logging.INFO
		else:
			level = logging.DEBUG
		logging.basicConfig(stream = sys.stderr, level = level, format = '%(message)s')

		##
		## Handle command
		##
		
		Disk = {}
		logging.info('Initialized')
		
		logging.info('Connected')
		if args.command == 'dir':
			self.dir_image(args.file)
		elif args.command == 'compress':
			self.compress_files(args.file, args.overwrite)
		elif args.command == 'extract':
			self.extract_files(args.file)
		elif args.command == 'new':
			self.new_image(args.file, args.overwrite)
		

	def dir_image(self, f):
		Disk = {}
		get_hdr(Disk, f[0:0x200])
		get_dir(Disk, f)
		Disk['free'] = 390
		for i in range(Disk["max_entries"]):
			Disk['free'] -= Disk["entries"][i]["sectors"] # calculate frre sectors

		print("--------- Directory ---------")
		print ("DISKNAME ERROR SECTOR MOUNT      VER# SECT FIRST USER      LAST USER       File_ID, sectors @")
		print("%s %04x  %06x %04x            %04d %s %06x %s %06x " % (
			Disk['hdr_dname'], 
			Disk['hdr_error'], 
			Disk['hdr_sector'], 
			Disk['hdr_mounts'], 
			Disk['free'], 
			Disk['hdr_fuser'], 
			Disk['hdr_fuserid'], 
			Disk['hdr_luser'], 
			Disk['hdr_luserid']))
		for i in range(Disk["max_entries"]):
			print("%s %04x %04d %s %06x %s %06x %02x" % (
				Disk["entries"][i]["filename"], 
				Disk["entries"][i]["version"], 
				Disk["entries"][i]["sectors"], 
				Disk["entries"][i]["fuser"], 
				Disk["entries"][i]["fuserid"], 
				Disk["entries"][i]["luser"], 
				Disk["entries"][i]["luserid"], 
				Disk["entries"][i]["file_id"])) #, 
				#Disk["entries"][i]["stored"]))

	def compress_files(self, d, overwrite):
		
		print("compress directory %s to file %s" % (d, d + ".img"))
		self.new_image(d, overwrite)
		filelist = get_filelist(d)

	def extract_files(self, fn):
		f = _parse_input_file(fn)
		Disk = {}
		get_hdr(Disk, f[0:0x200])
		get_dir(Disk, f)
		Disk['free'] = 390
		for i in range(Disk["max_entries"]):
			Disk['free'] -= Disk["entries"][i]["sectors"]

		#path = os.path.basename(f)
		path, tail = os.path.split(fn)
		#print(path)
		#raise SystemExit

		if Disk["max_entries"] > 0:
			olddirname = path + '/' + str.rstrip(Disk['hdr_dname']).replace("/","_") # replace the slash in some directories with underscore
			#print("Creating Directory: %s" % dirname)
			dirname = check_path(olddirname)
			#dirname = dirname
			print("Diskname: \'%s\' Creating Directory: %s" % (Disk['hdr_dname'], dirname))
			os.makedirs(dirname)
			#if  os.path.isdir(dirname) == True:
			#	print("--------------------------------------------- Directory %s exists!" % (dirname))
			#	if dirname[:-2] == '#':
			#		dirname = dirname[:-1] + str(filenum)
			#	else:
			#		dirname = dirname + '#0'
			#	print("Creating Directory: %s" % dirname)
			#	raise SystemExit 
			#else:
			#	
			#	pass
			for i in range(Disk["max_entries"]):
				filename = str.rstrip(Disk["entries"][i]["filename"]).replace("/","_") # replace the slash in some filenames with underscore
				fname = dirname + '/' + filename
				print("Writing: ", fname)
				with open(fname,"wb") as fi:
					fi.write(b''.join(Disk["entries"][i]["Data"]))
					fi.close()

	def new_image(self, f, overwrite):
		path, tail = os.path.split(f)
		name = tail + ".img" 
		print("new image: ", name)
		myArr = makeNewDisk(tail)
		#print(myArr[0:1024])
		if overwrite == True:
				with open(path + "/" + name,"wb") as f:
					f.write(myArr)
					f.close()
		else:
			if os.path.isfile(path + "/" + name) == True:
				print("File %s exists!" % name)
				raise SystemExit
			else:
				with open(path + "/" + name,"wb") as f:
					f.write(myArr)
					f.close()
	

if __name__ == "__main__":
	ES65_READER().process_command_line()