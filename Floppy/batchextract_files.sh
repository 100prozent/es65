#!/bin/bash

conv="./es65_conv.py -wd read "
if [ -z "$1" ]
then
    echo "Argument is empty! We dont want to delete all .txt files in root."
    exit
fi

echo "Reading dir: " $1
for f in `find $1/* -type f -name 'A.*' -o -name 'B.*' -o -name 'C.*' -o -name 'D.*' -o -name 'H.*' -o -name 'I.*' -o -name 'L.*' -o -name 'X.*'`
#for file in $1/A.* $1/I.* $1/H.* $1/B.* $1/C.* $1/L.* $1/D.* $1/X.*
do
    echo "---------------- converting:                 "$f
    $conv $f
done