#!/bin/bash

conv="./es65_conv.py read"
#Homedir=`pwd`
Homedir=/home/rob

Searchdir1=$Homedir"/Projects/es65Doku/Files_Work/S11"
#Searchdir1=$Homedir"/Beranek/SYS#1#2"

#Searchdir2=$Homedir"/Schmid/SYS3#1"
Searchdir2=$Homedir"/Projects/es65Doku/Files_Work/SMX4#1"
#echo "Searchdir:" $Searchdir1

cd $Searchdir1
echo "Looking in: "$Searchdir1 "and "$Searchdir2
for f in * 
#for file in $1/A.* $1/I.* $1/H.* $1/B.* $1/C.* $1/L.* $1/D.* $1/X.*
do
    echo "Comparing to: "$Searchdir2/$f
    #echo "Searching in: "$Searchdir1 "and "$Searchdir2
    if [ -e ${Searchdir2}/$f ]
    then
        #echo "------------------- compare: "$f "with" $Searchdir2/$f
        colordiff $1 <(xxd $f) <(xxd $Searchdir2/$f) --exclude '*.txt' --suppress-common-lines -W140
        #colordiff -yq <(xxd $f) <(xxd $Searchdir2/$f) --exclude '*.txt' --suppress-common-lines -W140
    else
        echo $f" File doesnt exist in second dir"
    fi
done

cd $Homedir