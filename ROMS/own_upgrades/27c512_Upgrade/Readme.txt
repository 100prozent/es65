es65 27C512 Upgrade:

Ziel ist es alle EPROMs durch einen 27C512 EEPROM zu 
ersetzen um in weiterer Folge besser mit einem EPROM-
Simulator arbeiten zu können.

Ein GAL16V8 ist zur Address-dekodierung notwendig.
Pinbelegung: siehe decoder.chp
A0..A12 are directly connected to the EPROM
A13..A15 are connected to the GAL and EA13..EA15 
are going to the EPROM.
Also Bank0..Bank3 Signals are connected to the GAL.

Memory Map:
Physical Address       EPROM Address   orig. EPROM Writing

A000-A7FF Bank0        0000-07FF       es65 P# 0.0 (2716)
A800-AFFF Bank0        0800-0FFF       es65 P# 0.1 (2716)
B000-B7FF Bank0        1000-17FF       es65 P# 0.2 (2716)
B800-BFFF Bank0        1800-1FFF       es65 P# 0.3 (2716)
A000-A7FF Bank1        2000-27FF       es65 P# 1.0 (2716)
A800-AFFF Bank1        2800-2FFF       es65 P# 1.1 (2716)
B000-B7FF Bank1        3000-37FF       es65 P# 1.2 (2716)
B800-BFFF Bank1        3800-3FFF       es65 P# 1.3 (2716)
A000-A7FF Bank2        4000-47FF       es65 P# 2.0 (2716)
A800-AFFF Bank2        4800-4FFF       es65 P# 2.1 (2716)
B000-B7FF Bank2        5000-57FF       es65 P# 2.2 (2716)
B800-BFFF Bank2        5800-5FFF       es65 P# 2.3 (2716)
A000-A7FF Bank3        6000-67FF       es65 P# 3.0 (2716)
A800-AFFF Bank3        6800-6FFF       es65 P# 3.1 (2716)
B000-B7FF Bank3        7000-77FF       es65 P# 3.2 (2716)
B800-BFFF Bank3        7800-7FFF       es65 P# 3.3 (2716)

8000-87FF              8000-87FF       v65 ROOT $80-$87 (2716)
8800-8FFF              8800-8FFF       v65 ROOT $88-$8F (2716)
9000-97FF              9000-97FF       v65 ROOT $90-$97 (2716)
9800-9FFF              9800-9FFF       v65 ROOT $98-$9F (2716)

0000-07FF Bank0        
0800-0FFF Bank0        
1000-17FF Bank0        
1800-1FFF Bank0        
2000-27FF Bank0        
2800-2FFF Bank0        
3000-37FF Bank0        
3800-3FFF Bank0        
4000-47FF Bank0        
4800-4FFF Bank0        
5000-57FF Bank0        
5800-5FFF Bank0        
6000-67FF Bank0        
6800-6FFF Bank0        
7000-77FF Bank0        
7800-7FFF Bank0        

C000-C7FF               
C800-CFFF               
D000-D7FF               
D800-DFFF               
E000-E7FF               
E800-EFFF               
F000-F7FF               