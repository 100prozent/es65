es65 Dallas DS1248Y Upgrade:

The goal is the replacement off ALL RAM and EPROM chips 
with one NVRAM.
Since i only have a 1Mbit NVRAM only one User RAM Bank is 
supported, i have a 4Mbit RAM chip though and a EPROM and RAM
Emulator with 4Mbit capability so additional Users can ba added.
I use a GAL16V8 (maybe GAL20V10) for Address-decoding.
Pinout: see decoder.chp

A0..A12 are directly connected to the EPROM
A13..A15 are connected to the GAL and EA13..EA15 
are going to the EPROM.
Also Bank0..Bank3 Signals are connected to the GAL.

Memory Map:
Physical Address       EPROM Address   orig. EPROM Writing      Address Lines High

;0000-07FF User0        0000-07FF       Ram (TMM2016)            
;0800-0FFF User0        0800-0FFF       Ram (TMM2016)            A11
;1000-17FF User0        1000-17FF       Ram (TMM2016)            A12
;1800-1FFF User0        1800-1FFF       Ram (TMM2016)            
;2000-27FF User0        2000-27FF       Ram (TMM2016)            A13
;2800-2FFF User0        2800-2FFF       Ram (TMM2016)            
;3000-37FF User0        3000-37FF       Ram (TMM2016)            
;3800-3FFF User0        3800-3FFF       Ram (TMM2016)            
;4000-47FF User0        4000-47FF       Ram (TMM2016)            A14
;4800-4FFF User0        4800-4FFF       Ram (TMM2016)            
;5000-57FF User0        5000-57FF       Ram (TMM2016)            
;5800-5FFF User0        5800-5FFF       Ram (TMM2016)            
;6000-67FF User0        6000-67FF       Ram (TMM2016)            
;6800-6FFF User0        6800-6FFF       Ram (TMM2016)            
;7000-77FF User0        7000-77FF       Ram (TMM2016)            
;7800-7FFF User0        7800-7FFF       Ram (TMM2016)            

Memory Map:
Physical Address       EPROM Address   orig. EPROM Writing      Address Lines High

8000-87FF              8000-87FF       v65 ROOT $80-$87 (2716)  A15
8800-8FFF              8800-8FFF       v65 ROOT $88-$8F (2716)
9000-97FF              9000-97FF       v65 ROOT $90-$97 (2716)
9800-9FFF              9800-9FFF       v65 ROOT $98-$9F (2716)

A000-A7FF Bank0        A000-A7FF       es65 P# 0.0 (2716)       A15 * A13
A800-AFFF Bank0        A800-AFFF       es65 P# 0.1 (2716) 
B000-B7FF Bank0        B000-B7FF       es65 P# 0.2 (2716) 
B800-BFFF Bank0        B800-BFFF       es65 P# 0.3 (2716)

C000-C7FF              C000-C7FF       common RAM (TMM2016)     A15 * A14
C800-CFFF              C800-CFFF       common RAM (TMM2016)     
D000-D7FF              D000-D7FF       common RAM (TMM2016)     
D800-DFFF              D800-DFFF       common RAM (TMM2016)     
E000-E7FF              E000-E7FF       common RAM (TMM2016)     
E800-EFFF              E800-EFFF       common RAM (TMM2016)     
F000-F7FF              F000-F7FF       common RAM (TMM2016)     

A000-A7FF Bank1        10000-107FF       es65 P# 1.0 (2716)     A16
A800-AFFF Bank1        10800-10FFF       es65 P# 1.1 (2716)
B000-B7FF Bank1        11000-117FF       es65 P# 1.2 (2716)
B800-BFFF Bank1        11800-11FFF       es65 P# 1.3 (2716)
A000-A7FF Bank2        12000-127FF       es65 P# 2.0 (2716)        
A800-AFFF Bank2        12800-12FFF       es65 P# 2.1 (2716)
B000-B7FF Bank2        13000-137FF       es65 P# 2.2 (2716)
B800-BFFF Bank2        13800-13FFF       es65 P# 2.3 (2716)
A000-A7FF Bank3        14000-147FF       es65 P# 3.0 (2716)
A800-AFFF Bank3        14800-14FFF       es65 P# 3.1 (2716)
B000-B7FF Bank3        15000-157FF       es65 P# 3.2 (2716)
B800-BFFF Bank3        15800-15FFF       es65 P# 3.3 (2716)

-                      16000-1FFFF       empty                  

---------------------------------------------------------       
---------    End of Address Range for 1Mbit    ---------       
---------------------------------------------------------
