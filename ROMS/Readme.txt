es65 Memory Map:

ADDRESS           USER1  USER2  USER3  USER4   COMMON FUNCTION

0xFC00-0xFFFF     EPROM on MUX         

0xFA90-0xFBFF   FREE

0xFA88-0xFA8F     LEDON  LEDON  LEDON  LEDON   LED On on every RAM Card
0xFA80-0xFA87     LEDOFF LEDOFF LEDOFF LEDOFF  LED Off on every RAM 

0xFA00-0xFA7F     Switch Switch Switch Switch  Switches on every RAM Card

0xF900-0xF9FF   FREE

0xF8F0-0xF8FF                                   6522 on MUC
0xF8E8(-0xF8EF)                                 U5 on MUC (User Switch)
0xF8E0(-0xF8E7)                                 U4 on MUC (Bank Switch
						0xFE = BANK0, 0xFD = BANK1, 0xFB = BANK2, 0xF7 = BANK3)
0xF8D0-0xF8DF                                   U7 on MUC NMI On/Off, LED Single/Multi,...) (F8D0-F8D7 writes Bit low, F8D8-F8DF writes bit high)
0xF8C8-0xF8CF                                   U16 on MUC (mostly not populated stuff)

0xF890-0xF8C7   FREE

0xF880-0xF88F                                    not populated on Printer Card

0xF870-0xF87F                                    6522 (Printer Interface)
0xF849-0xF96F   FREE (Reserved for User 5 and 6)
0xF848                                 Serial Switches Read User3
0xF844-0xF847                          Serial 2 User3
0xF840-0xF843                          Serial 1 User3
0xF838                          Serial Switches Read User2
0xF834-0xF837                   Serial 2 User2
0xF830-0xF833                   Serial 1 User2
0xF828                   Serial Switches Read User1
0xF824-0xF827            Serial 2 User1
0xF820-0xF823            Serial 1 User1
0xF818            Serial Switches Read User0
0xF814-0xF817     Serial 2 User0
0xF810-0xF813     Serial 1 User0

0xF804-0xF80F   FREE

0xF800-0xF803                                 Floppy
0xC000-0xF7FF       -      -      -      -    Slot8  RAM/ROM (Common RAM 14k) 
0xA000-0xBFFF     Slot1  Slot1  Slot1  Slot1     -   ROM (8k per Bank)
0x8000-0x9FFF       -      -      -      -    Slot8  RAM/ROM (Common ROM 8k)
0x0000-0x7FFF     Slot9  Slot7  Slot5  Slot3     -   RAM (32k per User)

Strings are terminated with Bit 7 set on last character ('R' which is 0x52 becomes 0xD2)