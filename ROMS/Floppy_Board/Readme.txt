es65 Floppy Board (Slot 20):
zwei Decoder PROMs haben die Aufschrift: 
"FLOP xx00-xx07" und
"F8xx"

Die Decoder sind vom Typ 6301 (256x4)
(U1 und U2):

FLOP xx00-xx07:
alles 0x7 außer:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

00      00    0110  6  from 0x00-0x3F is Write F800-F803 (Write)
01      01    1111  F                          F804-F807 (Write)
02-63   02-3F 0111  7                          F808-F8FF (Write)
64      40    0101  5  from 0x40-0x7F is Read  F800-F803 (Read)
65-255  41-FF 0111  7                          F804-F8FF (Read)

F8xx:
alles 0xf außer:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-15    00-F7 1111  F
248     F8    1110  E
