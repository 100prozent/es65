es65 MUX Board (Slot 19):
drei Decoder PROMs haben die Aufschrift: 
"MUC PI", "MUC I/O" und
"MUC Vek"

Zwei Decoder sind vom Typ 6306 (512x4)
(U8 und U18) und 6331 (32x8) (U7).

MUC PI:
alles 0xF außer:

ADDR     ADDR     DATA             ADDR      Selected
DEC      HEX      3210  HEX   External(HEX)

0-247    0-F7     1111  F     0000-F7FF         -
248      F8       1101  D     F800-F8FF      CE_MUCIO
249-251  F9-FB    1111  F     F900-FBFF         -
252      FC       1110  E     FC00-FCFF      CE_EPROM
253      FD       1110  E     FD00-FDFF      CE_EPROM
254      FE       1110  E     FE00-FEFF      CE_EPROM
255      FF       0110  6     FF00-FFFF      CE_EPROM + CE_MUCVEK
256-511  100-1FF  1111  F     Ignored because A8 is tied LOW!

MUC Vek:

ADDR     ADDR     DATA             ADDR      Selected
DEC      HEX      3210  HEX   External(HEX)

0-124    0-7C     0111  7     FF00-FFF9      SEL_FF00
125      7D       1110  E     FFFA-FFFB      SEL_FFFA
126      7E       1101  D     FFFC-FFFD      SEL_FFFC
127      7F       1011  B     FFFE-FFFF      SEL_FFFE
128-255  80-FF    0111  7     0000-FEFF      SEL_FF00 (only used from FC00)
256-511  100-1FF  1111  F     Ignored because A8 is tied LOW!

MUC I/O: (Enabled at 0xF800)
alles 0xFF außer:

ADDR    ADDR  DATA             ADDR
DEC     HEX   7654 3210  HEX   External(HEX)

0-24    00-18 1111 1111  FF
25      19    1111 0110  F6
26      1A    1110 1110  EE
27      1B    1110 1110  EE
28      1C    1101 1110  DE
29      1D    1011 1110  BE
30      1E    0111 1110  7E
31      1F    0111 1110  7E

The EPROM is mapped:
CPU Address  EPROM Address
Jumper       JP2 closed JP2 open
FC00-FEFF    000-2FF    400-6FF
FF00-FFF9    300-3F9    700-7F9
FFFA-FFFB    3F0-3F7    7f0-7F7  (dep. on PB0 from 6522 and write to 0xF8D2 or 0xF8DA)              - NMI Vector
FFFC-FFFD    3FC-3FD    7FC-7FD  (or 3FE-3FF, 7FE-7FF depending on Jumper JP1) - RESET Vector
FFFE-FFFF    3E0-3EF    7E0-7EF  (dep. on Interrupt source) - IRQ Vector