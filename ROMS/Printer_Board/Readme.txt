es65 Printer Board (Slot 18):
zwei Decoder PROMs haben die Aufschrift: 
"PRIF 70/80" und
"F8xx"

Die Decoder sind vom Typ 6301 (256x4)
(U1 und U2):

PRIF 70/80:
alles 0xe außer:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-27    00-1B 0100  4  from 0x00-0x7F is Write
28      1C    1000  8  0xF870 6522  Write
29-155  1D-9B 0100  4  from 0x80-0xFF is Read
156     9C    1001  9  0xF870 6522  Read

F8xx:
alles 0xf außer:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-15    00-F7 1111  F     0000-F7FF
248     F8    1110  E     F800-FFFF

The Addresses are mapped:
CPU Address  EPROM Address