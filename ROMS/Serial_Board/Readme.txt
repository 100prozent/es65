es65 Serial Boards (Slot 13, 14, 15, 16):
two Decoder PROMs with writing:
"SIF2 40x", "SIF2 30x", "SIF2 20x", "SIF2 x10S" these are different per card.
"F8xx" these are all the same

Decoder type is: 6301 (256x4)
(U1 und U2):

SIF2 x10S:
everything 0xe except:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-03    00-03 1110  E  from 0x00-0x7F is Write
04      04    1010  A  UART1 Write
05      05    0110  6  UART2 Write
06-131  06-83 1110  E  from 0x80-0xFF is Read
132     84    1011  B  UART1 Read
133     85    0111  7  UART1 Read
134     86    1101  D  Switches Read

SIF2 x20S:
everything 0xe except:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-07    00-07 1110  E  from 0x00-0x7F is Write
08      08    1010  A  UART1 Write
09      09    0110  6  UART2 Write
10-135  0A-87 1110  E  from 0x80-0xFF is Read
136     88    1011  B  UART1 Read
137     89    0111  7  UART1 Read
138     8A    1101  D  Switches Read

SIF2 30x:
everything 0xe except:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-11    00-0F 1110  E  from 0x00-0x7F is Write
12      0C    1010  A  UART1 Write
13      0D    0110  6  UART2 Write
14-139  0E-8B 1110  E  from 0x80-0xFF is Read
140     8C    1011  B  UART1 Read
141     8D    0111  7  UART1 Read
142     8E    1101  D  Switches Read

SIF2 40x:
everything 0xe except:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-15    00-0F 1110  E  from 0x00-0x7F is Write
16      10    1010  A  UART1 Write
17      11    0110  6  UART2 Write
18-143  12-8F 1110  E  from 0x80-0xFF is Read
144     90    1011  B  UART1 Read
145     91    0111  7  UART1 Read
146     92    1101  D  Switches Read

F8xx:
everything 0xf except:

ADDR    ADDR  DATA             ADDR
DEC     HEX   3210  HEX   External(HEX)

0-15    00-F7 1111  F
248     F8    1110  E
