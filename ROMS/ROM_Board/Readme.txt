es65 ROM Board (Slot 1):
alles EPROMs Typ 2716
alle vier Decoder PROMs haben die selbe Aufschrift: "A,Bxxx"
zwei wurden ausgelötet und haben denselben Inhalt. Es wird 
angenommen dass die anderen beiden ebenso den glaichen Inhalt haben.

ADDR ADDR  DATA             ADDR
DEC  HEX   7654 3210  HEX   External(HEX)

00   00    1111 1111  FF   
01   01    1111 1111       
02   02    1111 1111       
03   03    1111 1111       
04   04    1111 1111       
05   05    1111 1111       
06   06    1111 1111       
07   07    1111 1111       
08   08    1111 1111       
09   09    1111 1111       
10   0A    1111 1111       
11   0B    1111 1111       
12   0C    1111 1111       
13   0D    1111 1111       
14   0E    1111 1111       
15   0F    1111 1111       
16   10    1111 1111       
17   11    1111 1111       
18   12    1111 1111       
19   13    1111 1111       
20   14    0111 1110  7E   8000
21   15    0111 1101  7D   8800
22   16    0111 1011  7B   9000
23   17    0111 0111  77   9800
24   18    1111 1111       
25   19    1111 1111       
26   1A    1111 1111       
27   1B    1111 1111       
28   1C    1111 1111       
29   1D    1111 1111       
30   1E    1111 1111       
31   1F    1111 1111       
