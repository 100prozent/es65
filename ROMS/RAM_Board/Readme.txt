es65 RAM Board (Slot 3,5,7,9):
alles SRAMs Typ TMM2016
vier Decoder PROMs haben die Aufschrift: 
"0,1xxx"
"2,3xxx"
"4,5xxx"
"6,7xxx"

Die ersten drei PROMs wurden ausgelötet und gelesen, der vierte wurde nicht gelesen da ein Muster sichtbar wurde.

erster Decoder (U14):

ADDR ADDR  DATA             ADDR
DEC  HEX   7654 3210  HEX   External(HEX)  Block

00   00    0111 1110  7E                   0
01   01    0111 1101  7D                   0
02   02    0111 1011  7B                   1
03   03    0111 0111  77                   1

zweiter Decoder (U13):

ADDR ADDR  DATA             ADDR
DEC  HEX   7654 3210  HEX   External(HEX)

00   00    1111 1111                       0
01   01    1111 1111                       0
02   02    1111 1111                       1
03   03    1111 1111                       1
04   04    0111 1110  7E                   2
05   05    0111 1101  7D                   2
06   06    0111 1011  7B                   3
07   07    0111 0111  77                   3

dritter Decoder (U12):

ADDR ADDR  DATA             ADDR
DEC  HEX   7654 3210  HEX   External(HEX)

00   00    1111 1111                       0  
01   01    1111 1111                       0  
02   02    1111 1111                       1  
03   03    1111 1111                       1  
04   04    1111 1111                       2  
05   05    1111 1111                       2  
06   06    1111 1111                       3  
07   07    1111 1111                       3  
08   08    0111 1110  7E                   4
09   09    0111 1101  7D                   4
10   0A    0111 1011  7B                   5
11   0B    0111 0111  77                   5

vierter Decoder (U11):

ADDR ADDR  DATA             ADDR
DEC  HEX   7654 3210  HEX   External(HEX)

00   00    1111 1111                        0
01   01    1111 1111                        0
02   02    1111 1111                        1
03   03    1111 1111                        1
04   04    1111 1111                        2
05   05    1111 1111                        2
06   06    1111 1111                        3
07   07    1111 1111                        3
08   08    1111 1111                        4
09   09    1111 1111                        4
10   0A    1111 1111                        5
11   0B    1111 1111                        5
12   0C    0111 1110  7E                    6
13   0D    0111 1101  7D                    6
14   0E    0111 1011  7B                    7
15   0F    0111 0111  77                    7

Der fünfte Decoder ist vom Typ 6301 (256x4)
(U10):
 alles 0xf außer:

ADDR ADDR  DATA             ADDR
DEC  HEX   3210  HEX   External(HEX)

250  FA    1110  E     

