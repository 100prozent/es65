 ;   1.) Task-List TLR  +  Exchange-List ELR
0;       -----------------------------------
@;   a.) TLR - In dieser Liste werden alle dem System verfuegbaren DTD
P;             Forward gekettet. (LIFO-Charakteristik)
`;       Die Listenelemente sind DTD (Dynamic Task Descriptors).
p;       Den Aufbau ersieht man aus <>.
;       Fuer die TLR wird das Feld *TLINK verwendet.
;
 ;   Listenaufbau:
;   ------------
 ;   Initialisierung: (.IDLE=.TLR, s. Speicherbelegung)
0;   .TLR     +==========+
@;            I          I
P;            I          I           <DTD IDLE-Task>
`;            I          I
p;            I          I
;            I          I
;            +----------+
 ;            I   xxxx   I            *TLINK
;            +==========+
 ;            I  .TLR(I) I            *LASTT ... Pointer zum letzten Task
0;            +==========+                       (gehoert nicht zu DTD)
@;
P;   Allgem. Aufbau:
`; .TLR +==========+    .DTD1 +==========+             .DTDn +==========+
p;      I          I          I          I                   I          I
;      I          I          I          I                   I          I
;      I          I          I          I                   I          I
 ;      I          I          I          I                   I          I
;      I          I          I          I                   I          I
 ;      +----------+          +----------+                   +----------+
0;      I .DTD1(I) I          I .DTD2(I) I                   I   $00    I
@;      +==========+          +==========+                   +==========+
P;      I .DTDn(I) I
`;      +==========+
p;
;   b.) ELR - In dieser Liste werden alle dem System verfuegbaren ED
;             forward gekettet. (LIFO-Charakteristik)
 ;       Die Listenelemente sind ED (Exchange-Descriptoren) und IED
;       (Interrupt-Exchange-Descriptoren).
 ;       Fuer die ELR wird das Feld ELINK in den (I)ED verwendet.
0;       Bem: ED und IED haben gleichen Aufbau, nur ist IED laenger.
@;
P;   Listenaufbau:
`;   Initialisierung und allg. Aufbau ganz analog zu TLR, nur statt
p;   TLR    -->  ELR
;   DTD    -->  ED resp. IED
;   TLINK  -->  ELINK
 ;   LASTT  -->  LASTE
.PG
 ;   ****** Enter Element in TLR/ELR ******
0;   Aufruf: ENTER (.LIST,.ELP,*LINK)
@;   .LIST ::=  .TLR \ .ELR
P;   .ELP  ::=  .TD/PAGE \ .ED/PAGE
`;   *LINK ::=  *TLINK \ *ELINK
p;
;   Uebergabe von .ED(I) oder .DTD(I) im X-Register.
;
	 ENTER     .MD (BAS,ELP,IND)
	LDY BAS+IND+1
	 TXA
	0STA ELP+IND,Y
	@STA BAS+IND+1
	PLDA #0
	`STA ELP+IND,X
	p          .ME
	;
	;   Zerstoert: A,Y,P:N,Z
 ;   Ausfuehrungszeit: 20 us
.PG
 ;   ****** Remove Element aus TLR/ELR ******
0;   Aufruf: RMV (.LIST,.ELP,*LINK)
@;   Legende analog ENTER
P;
`;   Uebergabe von .ED(I) oder .DTD(I) im X-Register.
p;   Rueckmeldung:  V=0 ... O.K Element deleted
;                  V=1 ... not found
;
 RMV       .MD (BAS,ELP,IND)
.IS BAS,EX/PAGE
LDY #0
.ST @RMV=$FF
.CE
.IE @RMV
LDY #BAS-ELP
 .CE
!LP## TXA
0CMP ELP+IND,Y
@BEQ FD##
PLDA ELP+IND,Y
`TAY
pBNE LP##
SEV
JMP EX##
 ;
FD## LDA ELP+IND,X
 STA ELP+IND,Y
0BNE *+5
@STY BAS+IND+1
PCLV
`EX##      .EQ *
p.ME
;
;   Zerstoert: A,Y,P:N,Z,V,C
 ;   Ausfuehrungszeit: ca. 16+n*22us  (n...Anz. der Listenelemente-1)
.PG
