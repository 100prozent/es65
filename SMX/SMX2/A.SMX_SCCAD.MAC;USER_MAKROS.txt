.TI SMX65/SCCAD - User-Makros (SVC's)
;   ********************************************************************
;   ***                                                              ***
;   ***                 U S E R  -  M A K R O S                      ***
;   ***                 -----------------------                      ***
;   ***                                                              ***
;   ***  Diese Makros dienen fuer die Verwendung in den User-Tasks   ***
;   ***  und stellen die Schnittstelle fuer SMX65-Nucleus dar.       ***
;   ***  Prinzipiell unterscheiden sich 2 Gruppen:                   ***
;   ***  - SVC: Nucleus-Operation call                               ***
;   ***  - DATA: Dummy-Sections fuer SMX-Datenstrukturen             ***
;   ***                                                              ***
;   ********************************************************************
;
GSMX          .MD
.EZ USTPTR,PRI/MSK,SQACTV
.EX TLR,RLR,SLR,DLH,ICT,CRTB,IST,SPT,DBRTS,DBG/R
.EX IDLE,SMXSTR,WDOG,VIA
.EX SQCTSK,SQDTSK,SQCXCH,SQDXCH,SQSETV,SQRESV,SQELVL,SQDLVL,SQISND
.EX SQSUSP,SQRESM,SQSEND,SQWAIT,SQACTP,SQSDEF,SQSETP,SQRESP
TD/PAGE       .EQ $20C
              .ME
;
;
SXISND        .MD (LEV); INT-MSG send, am Stack muessen X,Y gerettet s.
.IG LEV
LDY #<LEV
.CE
SEI
JMP SQISND
              .ME
;
;
SXSETV        .MD (LEV,ADR)
.IG ADR
LDA #<ADR
LDY #>ADR
.CE
.IG LEV
LDX #<LEV
.CE
SEI
JSR SQSETV
CLI
              .ME
;
;
SXRESV        .MD (LEV)
.IG LEV
LDX #<LEV
.CE
SEI
JSR SQRESV
CLI
              .ME
;
;
SXSETP        .MD (LEV)
.IG LEV
LDX #<LEV
.CE
SEI
JSR SQSETP
CLI
              .ME
;
;
SXRESP        .MD (LEV)
.IG LEV
LDX #<LEV
.CE
SEI
JSR SQRESP
CLI
              .ME
;
;
SXELVL        .MD (LEV)
.IG LEV
LDX #<LEV
.CE
SEI
JSR SQELVL
CLI
              .ME
;
;
SXDLVL        .MD (LEV)
.IG LEV
LDX #<LEV
.CE
SEI
JSR SQDLVL
CLI
              .ME
;
;
SXSEND        .MD (EXA,ADR)
.IG ADR
LDA #<ADR
LDY #>ADR
.CE
.IG EXA
LDX #<EXA
.CE
SEI
JSR SQSEND
CLI
CLD
              .ME
;
;
SXACTP        .MD (EXA)
.IG EXA
LDX #<EXA
.CE
SEI
JSR SQACTP
CLI
              .ME
;
;
SXWAIT        .MD (EXA,TL)
.IG TL
LDA #<TL
LDY #>TL
.CE
.IA TL
LDA #0
LDY #0
.CE
.IG EXA
LDX #<EXA
.CE
SEI
JSR SQWAIT
CLI
CLD
              .ME
;
;
SXCXCH        .MD (EXA)
.IG EXA
LDX #<EXA
.CE
SEI
JSR SQCXCH
CLI
              .ME
;
;
SXDXCH        .MD (EXA)
.IG EXA
LDX #<EXA
.CE
SEI
JSR SQDXCH
CLI
              .ME
;
;
SXSDEF        .MD (SADR)
.IG SADR
LDA #<SADR
LDY #>SADR
.CE
SEI
JSR SQSDEF
CLI
              .ME
;
;
SXSUSP        .MD (ADR)
.IG ADR
LDX #<ADR-TD/PAGE
.CE
SEI
JSR SQSUSP
CLI
CLD
              .ME
;
;
SXRESM        .MD (ADR)
.IG ADR
LDX #<ADR-TD/PAGE
.CE
SEI
JSR SQRESM
CLI
CLD
              .ME
;
;
SXCTSK        .MD (SADR)
.IG SADR
LDA #<SADR
LDY #>SADR
.CE
SEI
JSR SQCTSK
CLI
CLD
              .ME
;
;
SXDTSK        .MD (ADR)
.IG ADR
LDX #<ADR-TD/PAGE
.CE
SEI
JSR SQDTSK
CLI
CLD
              .ME
;
;
;
;
;   ****** Dummy-Section ******
;
;   CREATE-TABLE CRTB
YCRTB         .MD
.SG Y
ITT           .DS 2
TSKNR         .DS 1
IET           .DS 2
XCHNR         .DS 1
              .ME
;
;   STATIC TASK DESCRIPTOR STD
YSTD          .MD
.SG Y
TNAME         .DS 4
INITPC        .DS 2; Startaddresse Code-Segment
STPTR         .DS 1; erste verfuegbare Stackaddresse
STLEN         .DS 1; Stack-Laenge (bytes)
PRIOR         .DS 1
INITEX        .DS 1; Initial Exchange-Address
DTDPTR        .DS 1; Pointer zu DTD
STDLEN        .DS 0
              .ME
;
;   DYNAMIC TASK DESCRIPTOR
YDTD          .MD
.SG Y
DLFOR         .DS 1; Delay-Link forward
DLBACK        .DS 1; Delay-Link backward
THREADF       .DS 1; Forward-Link
THREADB       .DS 1; Backward-Link
DELAY         .DS 2; Delay/Message
EXADR         .DS 1; Default-Exchangeaddress
STP           .DS 1; Stackpointer
STU           .DS 1
PRI           .DS 1; Prioritaet
STATUS        .DS 1
DTDLEN        .DS 0
              .ME
;
;   EXCHANGE DESCRIPTOR
YED           .MD
.SG Y
MSGH          .DS 2; Message-Head
ETLINK        .DS 1; Task-Link (Queue)
EDLEN         .DS 0
              .ME
;
;   MESSAGE
YMSG          .MD
.SG Y
MSGL          .DS 2; Message-Link
TYPE          .DS 1; Type
MLEN          .DS 1; Laenge
RESP          .DS 1; RESP-Exchange
HOME          .DS 1; HOME-Exchange
REM           .DS 0; Remaning space
              .ME
;
;   INT-MESSAGE
YIMSG         .MD
.SG Y,EDLEN
INTMSGL       .DS 2
INTTYPE       .DS 1
              .ME
;
;
SXYDCL        .MD
YCRTB
YSTD
YDTD
YED
YMSG
YIMSG
              .ME
;
;
;   ****** V24 - TREIBER ******
;          -------------
;
SV            .MD (AD,ADR,VAL)
.IG VAL
LDX #<VAL
.CE
.IG ADR
LDA #<ADR
LDY #>ADR
.CE
JSR AD
              .ME
;
;
GV24          .MD
.EX T/INIT,T/SFTCHK,T/INCHAR,T/INLINE,T/OUTCHR,T/OUTLIN,T/OUTSFT
.EZ T/LINLEN,T/XPOS,T/YPOS,T/SFT,LEVINP,T/EXTCH
              .ME
;
;
GDEB          .MD
.EX DON,DEBUG,DE1TAB,DE2TAB
.EZ BUF
              .ME
;
;
TENTRY        .MD (VERS#)
.BY VERS#
              .ME
;
;
REI           .MD
PLA
RTI
              .ME
