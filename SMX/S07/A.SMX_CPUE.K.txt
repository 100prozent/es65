.TI SMX65/CPUE - Konfiguration-Modul
.MO KM
;   ********************************************************************
;   ***                                                              ***
;   ***            K O N F I G U R A T I O N  -  M O D U L           ***
;   ***            ---------------------------------------           ***
;   ***                                                              ***
;   ***   In diesem Linkmodul werden alle aufgabenspezifischen Daten ***
;   ***   fuer ein System festgelegt.                                ***
;   ***   Die folgenden Makros unterstuetzen die Erzeugung dieser    ***
;   ***   Tabelle.                                                   ***
;   ***   Achtung: KM muss nach dem Nucleus gelinkt werden!          ***
;   ***                                                              ***
;   ********************************************************************
.PG
;
INIT          .MD (LIST)
;       Namen
;
.EY PR/MSK/R,SPT,IST,CRTB,EX/PAGE
.EY CLOCK,CLKMODE,ICT,S/IDLE,SSTP,CLKLEV,IIST
.EX INT/ENT,IDLE
.EX WDOG
TD/PAGE .EQ $20E;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
.SG S5,0; FUER STACK
.SG D,$223; Datenbereich
;
.IG LIST
.ES
.CE
OUTENA        .EQ %11000000
OUTDIS        .EQ %01000000
IDSLG         .EQ 6
SST           .EQ 8
;
;   AUFBAU des KM's:
;   ---------------
;   INIT                          An erster Stelle
;
;   IRQ/GEN (...)
;
;   STD/START
;   .BY '....'
;   STD/GEN (...)
;   STD/END                       Static-Task-Descriptoren errichten
;
;   IET/START
;   IET/GEN (...)
;   IET/END                       Initial Exchange-Tabelle
;
;   DTD/START
;   TASK1 DTD/GEN
;   DTD/END                       Dynamic Task-Descriptoren
;
;   EXD/START
;   EXCH1 EXD/GEN
;   EXD/END                       Exchange-Descriptoren
;
;   CRT/GEN                       Create-Table
;
;   RTC/GEN (....)                Real-Time-Clock
;
;   VEC/GEN (....)                Hardware-Vektoren
;
.EC
              .ME
.PG
;   b.) Makros
;   1.) Declare Interrupt-Level
;   Aufruf: IRQ/GEN (MASKE,CLKLEV)
;
;   Bem: MASKE ist die korrespondierende Privilegisierungsmaske. Ein
;        @Bit=0 bedeutet korresp. Level privilegiert.
;        CLKLEV ist der Level des RTC's.
;
IRQ/GEN       .MD (MASKE,CLL)
PR/MSK/R .BY MASKE
CLKLEV .EQ CLL
;
IST .EQ *
.BY %00000001; Level #0
.BY %00000010; Level #1
.BY %00000100; Level #2
.BY %00001000; Level #3
.BY %00010000; Level #4
.BY %00100000; Level #5
.BY %01000000; Level #6
.BY %10000000; Level #7
;
IIST .EQ *
.BY %11111110
.BY %11111101
.BY %11111011
.BY %11110111
.BY %11101111
.BY %11011111
.BY %10111111
.BY %01111111
;
SPT .EQ *
.BY %11111111
.BY %11111110
.BY %11111100
.BY %11111000
.BY %11110000
.BY %11100000
.BY %11000000
.BY %10000000
.BY %00000000
.BY %00000000
.BY %00000000
.BY %00000000
.BY %00000000
.BY %00000000
.BY %00000000
.BY %00000000
              .ME
;
;
VEC/GEN       .MD (NMI1,NMI2,NMI3,NMI4,RES1,RES2)
.SG A,$FFE0
.AD IENT7
.AD IENT6
.AD IENT5
.AD IENT4
.AD IENT3
.AD IENT2
.AD IENT1
.AD IENT0
;
.AD NMI4
.AD NMI3
.AD NMI2
.AD NMI1
;
.AD $FFFF
.AD $FFFF
.AD RES2
.AD RES1
              .ME
.PG
;   2.) Task und Exchangedeklaration
;   Deklaration der STD's
;   Aufruf: STD/START
;           STD/GEN (INITPC,SLEN,PRIOR,IEX,DTDADR)
;           STD/GEN (INITPC,SLEN,PRIOR,IEX,DTDADR)
;                               .
;                               .
;           STD/GEN (INITPC,SLEN,PRIOR,IEX,DTDADR)
;           STD/END
;
STD/START     .MD (INITPC,SLEN,SSTPTR)
.SG S5;Stack System
.IG SSTPTR
.DS SSTPTR-1
.CE
.IA SSTPTR
.DS SST-1
.CE
SSTP .DS 0
.SG S7
S/IDLE .BY 'IDLE'
.IA INITPC
.AD PCID
.BY <IDSTP
.BY IDSLG
.SG S5
.DS IDSLG
.CE
.IG INITPC
.AD INITPC
.BY <IDSTP
.BY SLEN
.SG S5
.DS SLEN
.CE
IDSTP .DS 0; STACK-POINTER
.SG S7
.BY $FE
.BY $FF
.BY <IDLE-TD/PAGE
;
.IA INITPC
PCID .BY $11; Version
LPIY STA WDOG
JMP LPIY; Idle-Task default
.CE
;
ITT           .EQ *
.ST @TLNG=*
              .ME
;
;
STD/GEN       .MD (INITPC,SLEN,PRIOR,IEX,TDADR)
.ST @TCO=@TCO+1
.AD INITPC
.BY <SPTR##
.BY SLEN,PRIOR
.BY <IEX-EX/PAGE
.BY <TDADR-TD/PAGE
.SG S5
.DS SLEN
SPTR## .DS 0
.SG S7
.IN *-@TLNG-11
   ********************************** ERROR 20 NAME INVAL **************
.CE
.ST @TLNG=*
              .ME
;
;
STD/END       .MD
              .ME
.PG
;   Deklaration der Exchanges
;   Aufruf: IET/START
;           IET/GEN (EDADR)
;           IET/GEN (EDADR)
;                .
;                .
;           IET/GEN (EDADR)
;           IET/END
;
IET/START     .MD
.SG S7
IET           .EQ *
              .ME
;
;
IET/GEN       .MD (EDADR)
.BY <EDADR-EX/PAGE
.ST @ECO=@ECO+1
              .ME
;
;
IET/END       .MD
;
.SG S7
ICT           .BY <SQL0EX-EX/PAGE
.BY <SQL1EX-EX/PAGE
.BY <SQL2EX-EX/PAGE
.BY <SQL3EX-EX/PAGE
.BY <SQL4EX-EX/PAGE
.BY <SQL5EX-EX/PAGE
.BY <SQL6EX-EX/PAGE
.BY <SQL7EX-EX/PAGE
              .ME
.PG
;   Erzeug. der Create-Table CRTB
;   Aufruf: CRT/GEN
;
CRT/GEN       .MD
.SG S7
.IM @TCO-1
    *************************** ERROR 10 LOW TASKS *********************
.MX
.IP @TCO-19; max. 18 Tasks zulaessig
    *************************** ERROR 11 TOO MUCH TASKS ****************
.MX
.IP @ECO-65; max. 40 Exchanges moeglich
    *************************** ERROR 12 TOO MUCH XCH ******************
.MX
CRTB          .AD ITT
.BY @TCO
.AD IET
.BY @ECO
              .ME
.PG
;   3.) Speicherplaetze fuer DTD
;   Aufruf: DTD/START
; NAME      DTD/GEN
; NAME      DTD/GEN
;                .
;                .
; NAME      DTD/GEN
;           DTD/END
;
DTD/START      .MD
.SG D
              .ME
;
;
DTD/GEN        .MD
              .DS 11
              .ME
;
;
DTD/END        .MD
              .ME
;
;
;   4.) Speicherplaetze fuer ED
;   Aufruf: EXD/START
; NAME      EXD/GEN
; NAME      EXD/GEN
;               .
;               .
; NAME      EXD/GEN (I); Interrupt-Exchange-Descriptor
;           EXD/END
;
EXD/START      .MD
.SG D
EX/PAGE       .EQ *
              .ME
;
;
EXD/GEN        .MD (I)
              .DS 3
.IG I
.DS 3
.CE
              .ME
;
;
EXD/END        .MD
              .ME
;
;
;   5.) Real-Time-Clock
;   Aufruf: RTC/GEN (Clock,Mode)
;
RTC/GEN       .MD (CLK,MODE)
.IG CLK
CLOCK         .EQ CLK
.CE
.IG MODE
CLKMODE       .EQ MODE
.CE
              .ME
;
