.PG
.IE @&2
.TI SMX65/SCCAD - Nucleus SVC's
.CE
.IN @&2
.TI SMX65/CPUE - Nucleus SVC's
.CE
;   ********************************************************************
;   ***                                                              ***
;   ***                   N U C L E U S  -  S V C                    ***
;   ***                   -----------------------                    ***
;   ***                                                              ***
;   ********************************************************************
;
;   Behandlung der Interrupts:
;   -------------------------
;   * SQISND (Programm siehe A.SMX.I2)
;     Dieser SVC erlaubt es, aus User-supplied ISR eine Int-MSG an den
;     korrespondierende Int.Ex. zu schicken.
;     Aufgerufen muss dieser SVC ueber einen JMP werden, welcher natuer-
;     lich nur am Ende der ISR stehen kann.
;     Eventuell kann mit dem Systemstack eine weitere Interruptebene
;     realisiert werden. (s. Programming-Manual).
;
;     Params: X: Level
.IE @&$4000
SQSETV .EQ *
LDA #10
JMP SMXERR
.CE
.IN @&$4000
;
;
;   * SQSETV
;     Dieser SVC leitet einen bestimmten Level auf eine User-supplied
;     ISR um. Dadurch wird die Bearbeitung total vom System getrennt.
;     Bem: Durch einen Interrupt wird auch in diesem Fall immer der
;          Akku am Stack gerettet. Das RTI in der User-suppl. ISR muss
;          daher nach PLA erfolgen (am besten Makro REI verwenden).
;
;     Params: X: Level
;             A,Y: Prozeduraddresse
;
SQSETV PHA
TXA
ASL A
TAX
PLA
STA IJT,X
TYA
STA IJT+1,X
DBRTS  RTS
;
;   Zerstoert: A,X,P:Z,N,C
;   Ausfuehrungszeit: ca. 38 us
.CE
;
;
;   * SQRESV
;     Dieser SVC setzt den Interrupt-Vektor bestimmten Levels auf den
;     SMX65-Vektor zurueck.
;
;     Params: X: Level
;
SQRESV TXA
ASL A
TAX
LDA SVTAB,X
STA IJT,X
LDA SVTAB+1,X
STA IJT+1,X
RTS
;
;   Zerstoert: A,X,P:N,Z,C
;   Ausfuehrungszeit: ca. 37 us
;
;
;   * SQSETP
;     Define Interrupt-Level als Priorisiert
;     Dieser SVC setzt das korresp. Bit in der Zelle PR/MSK.
;
;     Params: X: Level
;
.IE @&2
SQSETP LDA IST,X
ORA PR/MSK
STA PR/MSK
AND IOINT/MSK
EOR #$7F
STA PRI/MSK
LDA IST,X
AND IOINT/MSK
ORA I/MSK
STA I/MSK
STA VIA+VIAIER
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 50 us
.CE
.IN @&2
SQSETP LDA IIST,X; Level# = 0
AND PR/MSK
STA PR/MSK; Bit loeschen (aktiv 0!!!)
ORA IOINT/MSK
STA PRI/MSK
AND I/MSK
STA I/MSK
STA VIA+VIAORA
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 41 us
.CE
;
;
;   * SQRESP
;     Reset Interrupt Level zu not priorisiert
;
;     Params: X: Level
;
.IE @&2
SQRESP LDA IST,X
EOR #$FF
AND PR/MSK
STA PR/MSK
LDA IST,X
ORA PRI/MSK
STA PRI/MSK
LDA IST,X
AND SPRI/MSK
AND IOINT/MSK
BNE CEXX
LDA IST,X
STA VIA+VIAIER
EOR #$FF
AND I/MSK
CEXX STA I/MSK
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 52-56 us
.CE
.IN @&2
SQRESP LDA IST,X
ORA PR/MSK
STA PR/MSK; Inaktiv
ORA IOINT/MSK
STA PRI/MSK
AND SPRI/MSK
ORA IOINT/MSK
ORA DI/MSK
STA I/MSK; Erzeuge neue I/MSK
STA VIA+VIAORA
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 47 us
.CE
.PG
;   * SQELVL
;     Enable Interrupt-Level.
;     Bem: Durch diesen SVC wird zwar IO-Interruptmaessig enabled, das
;          Softwareprioritaetsschema kann jedoch den Level noch nicht
;          zulassen.
;          D.h. das enable ist nur dann sofort aktiv, wenn dass Software
;          schema dies zulaesst.
;
;     Params: X: Level
;
;     Out: V=0: o.k. Interrupt enabled
;          V=1: Softwarepriority disabled
;
.IE @&2
SQELVL LDA IST,X
ORA IOINT/MSK
STA IOINT/MSK; IOINT-Maske aktualisieren
AND PR/MSK
EOR #$7F
STA PRI/MSK; PRI-Maske
SEV
LDA IST,X
BIT PR/MSK
BNE ELX
BIT SPRI/MSK
BEQ ENEL/Y; Softwareprioritaet disabled
ELX ORA I/MSK
STA I/MSK
STA VIA+VIAIER; Interrupt enable
CLV
ENEL RTS
ENEL/Y SEV
RTS
;
;   Zerstoert: A,P:Z,N,V
;   Ausfuehrungszeit: ca. 55-67 us
.CE
.IN @&2
SQELVL LDA IIST,X
AND IOINT/MSK
STA IOINT/MSK; aktiv 0
ORA PR/MSK
STA PRI/MSK
LDA IST,X
BIT PR/MSK
BEQ ELX; priv. Interrupt --> Enable moeglich
BIT SPRI/MSK
BNE ENEL/Y; Software-Prioritaet vorrangig
ELX LDA IIST,X; Enable
AND I/MSK
STA I/MSK
STA VIA+VIAORA
CLV
RTS
ENEL/Y SEV
RTS
;
;   ZERSTOERT: A,P:Z,N,V
;   Ausfuehrungszeit: ca. 55 us
.CE
;
;
;   * SQDLVL
;     Disable spez. Interrupt-Level.
;     Ein Level wird disabled, dieser Vorgang ist sofort wirksam.
;
;     Params: X: Level
;
.IE @&2
SQDLVL LDA IST,X
STA VIA+VIAIER; Interrupt off
EOR #$7F
AND IOINT/MSK
STA IOINT/MSK; IOINT-Maske
AND I/MSK
ORA #$80
STA I/MSK; I/MSK
LDA IST,X
ORA PRI/MSK
STA PRI/MSK
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 49 us
.CE
.IN @&2
SQDLVL LDA IST,X
ORA IOINT/MSK
STA IOINT/MSK; inaktiv
ORA PR/MSK
STA PRI/MSK
ORA I/MSK
STA I/MSK
STA VIA+VIAORA
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 41 us
.CE
.IN @&2
;
;
;   * SQENDI
;     End of Interrupt.
;     Dieser SVC hebt die Interrupt-Blockierung ueber den MUC auf.
;     Er muss nach einem WAIT auf einen Interrupt aufgerufen werden,
;     nachdem aber der Interrupt selbst schon geloescht wurde.
;
;     Params: X ... Level
;
SQENDI LDA IIST,X
AND DI/MSK
STA DI/MSK
LDA PRI/MSK
AND SPRI/MSK
ORA IOINT/MSK
ORA DI/MSK
STA I/MSK
STA VIA+VIAORA
RTS
;
;   Zerstoert: A,P:Z,N
;   Ausfuehrungszeit: ca. 47 us
.CE
.PG
.IE @&$200
SQSEND .EQ *
LDA #15
JMP SMXERR
.CE
.IN @&$200
;   Behandlung der Intertask-Kommunikation
;   --------------------------------------
;
;   * SQSEND
;     Send MSG zu bestimmtem Exchange.
;     Dieser SVC schickt eine MSG zu einem Exchange. Wartet bereits ein
;     Task an diesem E., so wird die MSG dem ersten (hoechstprioren)
;     Task uebergeben, ev. wird dieser dann sogar der hoechstpriore
;     lauffaehige Task (sceduling!).
;     Wartet kein Task, so wird die MSG in die FIFO-Queue der MSG's ge-
;     bracht. Die Einordnung erfolgt dabei nach der MSG-Prioritaet
;     (TYPE), fuer die die selben Kriterien gelten wie fuer die Task-
;     prioritaeten.
;
;     Params: X: ED(I) ... Exchange-Descriptor (Index)
;             A,Y: MSG-Adresse
;
;     Out: V=0: Kein Sceduling
;          V=1: Sceduling wurde ausgeloest (neuer RDY-Task)
;
SQSEND STA PTR
IOFF
TXA
SYSON
TAX
CLI
STA WDG
LDA EX/PAGE+ETLINK,X
BNE TSKWT; Task wartet
STY PTR+1; PTR --> MSG
TXA
CLC
ADC #<EX/PAGE
STA BPTR
LDA #>EX/PAGE
ADC #0
STA BPTR+1
LDA EX/PAGE+MSGH,X
STA B2PTR
LDA EX/PAGE+MSGH+1,X
STA B2PTR+1
L3 LDY #TYPE
CMP #$FF
BEQ L2B
LDA (PTR),Y
CMP (B2PTR),Y
BCC L2A
LDA B2PTR
STA BPTR
LDA B2PTR+1
STA BPTR+1
LDY #MSGL
LDA (B2PTR),Y
TAX
INY
LDA (B2PTR),Y
STA B2PTR+1
STX B2PTR
JMP L3
;
L2B LDY #MSGL
LDA #$FF
STA (PTR),Y
INY
STA (PTR),Y
JMP LK
;
L2A LDY #MSGL
LDA (BPTR),Y
STA (PTR),Y
INY
LDA (BPTR),Y
STA (PTR),Y
LK LDA PTR+1
STA (BPTR),Y
DEY
LDA PTR
STA (BPTR),Y
RETV CLV
RET SEI
SYSOFF
ION
RTS
;
TSKWT STY PTR+1; Task wartet
STA WDG
LDA EX/PAGE+ETLINK,X
TAX
LDA TD/PAGE+STATUS,X
AND #DELAYED
BEQ NO; nicht Delayed
; im X-Register DTD(I)
JSR U/RMVDLY
NO LDA PTR
STA TD/PAGE+DELAY,X; MSG-Adr. uebergeben
LDA PTR+1
STA TD/PAGE+DELAY+1,X
LDY TD/PAGE+EXADR,X; erstelle ED-Adr.
STX B2PTR
JSR U/RMVXCH
LDX B2PTR
JSR U/ENTRDY
LDY SQACTV
CPY RLR+RHEAD
BEQ RETV
LDA TD/PAGE+STATUS,Y
JMP DISPTCH; Dispatcher
;
;   Zerstoert: alles
;   Ausfuehrungszeit: ca. 200 us, wenn kein Task wartet, 1 hoehere MSG.
;                     ca. 470 us bis 1. Befehl des neuen Tasks
;                         (incl. Delay)
.CE
.IE @&$100
SQACTP .EQ *
LDA #16
JMP SMXERR
.CE
.IN @&$100
;
;
;   * SQACTP
;     Pruefe, ob MSG an Exchange steht, wenn ja, MSG liefern, wenn nein,
;     return.
;     Der Exchange kann auch der Defaultexchange sein, in diesem Falle
;     wird X=$FF vorausgesetzt!
;     Dieser SVC loest keinen Scedulvorgang aus, da der rufende Task auf
;     jeden Fall weiterlaeuft.
;
;     Params: X: ED(I)
;
;     Out: V=0: MSG wird geliefert in A,Y
;          V=1: keine MSG an Ex.
;
SQACTP IOFF
TXA
SYSON
TAX
CLI
STA WDG
LDY SQACTV
CPX #$FF; Default-Exchange
BNE NODEF
LDX TD/PAGE+EXADR,Y
JMP COM
;
NODEF TXA
STA TD/PAGE+EXADR,Y; neue EXADR
COM SEV
LDA EX/PAGE+MSGH+1,X
CMP #$FF
BEQ RET; keine MSG wartet, return in SXSEND
JMP MESSQ2; In SQWAIT
;
;   Zerstoert: A,X,Y,P:Z,N,V,C
;   Ausfuehrungszeit: ca. 80 us ohne MSG
;                     ca. 160 us mit MSG
.CE
.IE @&$180
SQWAIT .EQ *
LDA #17
JMP SMXERR
.CE
.IN @&$180
;
;
;   * SQWAIT
;     Wait on Exchange
;     Dieser SVC veranlasst einen Task zu einem Timed oder Untimed WAIT
;     an einem Exchange. (Wieder Defaultex. moeglich, X=$FF).
;     Wartet keine MSG am Exchange, so wird der Task not READY und
;     der Prozessor bearbeitet den naechsten hoechstprioren Task.
;     Wartet eine MSG, so wird sie uebergeben.
;
;     Params: X: ED(I)
;             A,Y: Time-Limit (in Systemclock-Einheiten)
;
;     Out: V=0: MSG hat gewartet
;          V=1: MSG erst nach Sceduling. (in A,Y)
;
SQWAIT STA PTR
IOFF
TXA
SYSON
TAX
CLI
STA WDG
STY PTR+1; PTR enth. Timelimit
LDX SQACTV
CMP #$FF
BNE EXSP; nicht def. Ex.
LDA TD/PAGE+EXADR,X
JMP A01
EXSP STA TD/PAGE+EXADR,X; neuer EXADR
A01 TAY; Y:=ED(I)
LDA EX/PAGE+MSGH+1,Y
CMP #$FF
BEQ ACT
JMP MESSQ; Message wartet
;
ACT JSR U/RMVRDY
STA WDG
LDY TD/PAGE+EXADR,X
ENTXCH
LDA PTR
ORA PTR+1
BNE IU1
JMP NOD1
IU1 LDA PTR; Delay
STA TD/PAGE+DELAY,X
LDA PTR+1
STA TD/PAGE+DELAY+1,X
ENTDLY
STA WDG
NOD1 LDY SQACTV
CPY RLR+RHEAD
BEQ AS2
LDA TD/PAGE+STATUS,Y
JMP DISPTCH
;
MESSQ TYA
TAX; X:=ED(I)
MESSQ2 LDA EX/PAGE+MSGH,X
STA PTR
LDA EX/PAGE+MSGH+1,X
STA PTR+1
LDY #MSGL
LDA (PTR),Y
STA EX/PAGE+MSGH,X
INY
LDA (PTR),Y
STA EX/PAGE+MSGH+1,X; ED[X].MSGH:=old.MSG.Link
LDY PTR+1
AS2 CLV
SEI
SYSOFF
ION
LDA PTR
RTS
;
;   Zerstoert: A,X,Y,P:Z,N,V,C
;   Ausfuehrungszeit: im Durchschnitt 370 us ohne Delay (incl. Disp.)
;                                     770 us mit 4 Delayed Tasks "
;                                     130 us, wenn MSG wartet
.CE
.PG
