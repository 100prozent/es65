;   4.) Delay-Liste DLH
;       ---------------
;       DLH ist eine zeitmaessig geordnete Liste (geringste Zeit am
;       Kopf), in der die im timed-wait befindlichen Tasks gelinkt
;       sind.
;       Das Delay der Tasks wird jedoch beim Einfuegen verringert
;       (s. ENTDLY), sodass im Interrupt nur das Delay des ersten Tasks
;       verleinert werden muss.
;       Die Liste ist niemals leer, zumindest der Idle-Task befindet
;       sich darin. Sein Delay wird allerdings im Interrupt nicht ver-
;       kleinert. IDLE.DELAY=$FFFF ist das hoechste Delay, das moeglich
;       ist.
;
;   Listenaufbau
;
;   * Initialisierung
;   1.) leere Liste (im Startup-Modul)
;   .DLH +==========+                    .IDLE +==========+
;        I   xxxx   I  *DHEAD                  I   xxxx   I  *DLFOR
;        +----------+                          +----------+
;        I  .DLH(I) I  *DTAIL                  I  .DLH(I) I  *DLBACK
;        +==========+                          +----------+
;                                              I          I
;                                              I          I
;                                              I          I
;                                              +==========+
;
;   Sobald diese beiden records initialisiert sind, kann die ordnungs-
;   gemaess initialisierte Delay-List erzeugt werden durch Verwendung
;   des fuer TLR\ELR vorgesehenen Makros ENTER:
;   LDX #IDLE-TD/PAGE
;   ENTER (DLH,TD/PAGE,DLFOR)
;
;   2.) Ordnungsgemaess initialisierte Delay-List
;   .DLH +==========+                    .IDLE +==========+
;        I .IDLE(I) I  <DLH-Listenkopf>        I     0    I  <DTD-IDLE>
;        +----------+                          +----------+
;        I .IDLE(I) I                          I  .DLH(I) I
;        +==========+                          +----------+
;                                              I          I
;                                              I          I
;                                              I          I
;                                              +==========+
;
;-----------------------------------------------------------------------
;   * Aufbau der Liste
;
;   .DLH +==========+                    .IDLE +==========+
;        I .DTD1(I) I  <DLH-Listenkopf>        I     0    I  <DTD-IDLE>
;        +----------+                          +----------+
;        I .IDLE(I) I                          I .DTDn(I) I
;        +==========+                          +----------+
;                                              I          I
;                                              I          I
;                                              I          I
;                                              +==========+
;
;  .DTD1 +==========+ .DTD2 +==========+       .DTDn +==========+
;        I .DTD2(I) I       I .DTD3(I) I             I .IDLE(I) I
;        +----------+       +----------+             +----------+
;        I .DLH(I)  I       I .DTD2(I) I             I.DTDn-1(I)I
;        +----------+       +----------+             +----------+
;        I          I       I          I             I          I
;        I          I       I          I             I          I
;        I          I       I          I             I          I
;        +==========+       +==========+             +==========+
;
;-----------------------------------------------------------------------
.PG
;   ****** Enter Element in Delay-Liste ******
;   Aufruf: ENTDLY
;
;   Das einzufuegende Element DTD(I) muss im X-Register uebergeben werd.
;   das Delay muss in DTD.DELAY gespeichert sein.
;   Funktion: Das Bit #DELAYED im Status wird gesetzt, der Counter
;   der delayed Tasks  erhoeht und, wenn noetig, der Clock gestartet.
;   Das Element selbst wird nach folgendem Schema in die Liste ein-
;   gefuegt:
;   - Vergleich Delay.new mit Delay.1.task
;   - Ist das Delay von new groesser: Delay.new:=Delay.new-Delay.1.task
;   - Naechster Task --> Loop zu Vergleich (1)
;   - Das Delay von new kleiner: Delay.n.task:=Delay.n.task-Delay.new
;     das neue Element wird vor dem n.task eingefuegt.
;   - Delay.idle:=$FFFF
;
ENTDLY      .MD
LDY DLH; 1. Element
SEC
L## LDA TD/PAGE+DELAY,X
SBC TD/PAGE+DELAY,Y
STA BPTR
LDA TD/PAGE+DELAY+1,X
SBC TD/PAGE+DELAY+1,Y
STA BPTR+1
BCC F##; DTD[X].DELAY < DTD[Y].DELAY
ORA BPTR
BEQ F##; DTD[X].DELAY = DTD[Y].DELAY
LDA BPTR; DTD[X].DELAY > DTD[Y].DELAY
STA TD/PAGE+DELAY,X
LDA BPTR+1
STA TD/PAGE+DELAY+1,X; DTD[X].DELAY:=DTD[X].DELAY-DTD[Y].DELAY
LDA TD/PAGE+DLFOR,Y
TAY; Y:=DTD[Y].DLFOR
JMP L##
;
F## SEC; Found
LDA TD/PAGE+DELAY,Y
SBC TD/PAGE+DELAY,X
STA TD/PAGE+DELAY,Y
LDA TD/PAGE+DELAY+1,Y
SBC TD/PAGE+DELAY+1,X
STA TD/PAGE+DELAY+1,Y; DTD[Y].DELAY:=DTD[Y].DELAY-DTD[X].DELAY
TYA
STA TD/PAGE+DLFOR,X; DTD[X].FOR:=.DTD[Y]
LDA TD/PAGE+DLBACK,Y
STA TD/PAGE+DLBACK,X; DTD[X].BACK:=DTD[Y].BACK
TXA
STA TD/PAGE+DLBACK,Y; DTD[Y].BACK:=.DTD[X]
LDY TD/PAGE+DLBACK,X
STA TD/PAGE+DLFOR,Y
LDA #$FF
STA IDLE+DELAY
STA IDLE+DELAY+1
LDA TD/PAGE+STATUS,X
ORA #DELAYED
STA TD/PAGE+STATUS,X
OUTCTA
OUTCTX
LDY DLCOU
BNE IN##; DLCOU > 0
STRCLK; DLCOU < 0 --> Start Clock
IN## INC DLCOU; DLCOU:=DLCOU+1
            .ME
;
;   Zerstoert: A,Y,P:N,Z,C,Zellen:BPTR,BPTR+1
;   Ausfuehrungszeit: bei 4 Del. Tasks ca. 350 us.
.PG
;   ****** Remove Task aus Delay-List ******
;   Aufruf: RMVDLY [(Z)]
;   Params: Wird Z angegeben, so wird das Element aus der Liste ohne
;           Addition entfernt (DELAY=0)
;
;   Das zu entfernende Element muss im X-Register uebergeben werden.
;   Achtung: der Task muss auch wirklich in der Delay-Liste stehen.
;   Funktion: Das Bit #DELAYED im Task-Status wird geloescht, der Delay-
;   counter decrementiert.
;   Der Task selbst wird aus der Liste entfernt, sein Delay wird dem
;   Nachfolger aufaddiert.
;
RMVDLY      .MD (Z)
LDY TD/PAGE+DLFOR,X; Y:=DTD[X].FOR
.IA Z
CLC
LDA TD/PAGE+DELAY,Y
ADC TD/PAGE+DELAY,X
STA TD/PAGE+DELAY,Y
LDA TD/PAGE+DELAY+1,Y
ADC TD/PAGE+DELAY+1,X
STA TD/PAGE+DELAY+1,Y; DTD[Y].DELAY:=DTD[Y].DELAY+DTD[X].DELAY
.CE
RMVDLY/Z LDA TD/PAGE+DLBACK,X
STA TD/PAGE+DLBACK,Y; DTD[Y].BACK:=DTD[X].BACK
TYA
LDY TD/PAGE+DLBACK,X; Y:=DTD[X].BACK
STA TD/PAGE+DLFOR,Y; DTD[Y].FOR:=.DTD[Y].pred
LDA #$FF
STA IDLE+DELAY
STA IDLE+DELAY+1
LDA TD/PAGE+STATUS,X
AND #NDELAYED
STA TD/PAGE+STATUS,X
OUTCTA
OUTCTX
DEC DLCOU
            .ME
;
;   Zerstoert: A,Y,P:N,Z,C
;   Ausfuehrungszeit: ca. 70 us
.PG
;   5.) Exchange-Task-queue ETQ
;       -----------------------
;       In der ETQ sind alle an diesem Exchange wartenden Tasks gemaess
;       ihrer Prioritaet gekettet, d.h. der hoechstpriore Task erhaelt
;       eine gesendete MSG zuerst.
;
;   Listenaufbau:
;   * Initialisierung
;
;   .ED +==========+
;       I          I  <Exch.-Descriptor>
;       +----------+
;       I          I
;       +----------+
;       I    0     I  *ETLINK
;       +==========+
;
;   Aufbau der Liste
;
; .ED +==========+ .DTD1 +==========+              .DTDn +==========+
;     I          I       I          I                    I          I
;     +----------+       +----------+                    +----------+
;     I          I       I          I                    I          I
;     +----------+       +----------+                    +----------+
;     I .DTD1(I) I       I .DTD2(I) I                    I    0     I
;     +==========+       +----------+                    +----------+
;                        I    0     I                    I.DTDn-1(I)I
;                        +----------+                    +----------+
;                        I          I                    I          I
;                        I          I                    I          I
;                        I          I                    I          I
;                        +==========+                    +==========+
;
.PG
;   ****** Enter Element in ETQ ******
;   Aufruf: ENTXCH
;
;   Der entsprechende DTD(I) muss im X-Register uebergeben werden.
;   Der entsprechende ED(I) muss im Y-Register uebergeben werden.
;   Funktion: Der Task wird gemaess seiner Prioritaet in die Liste
;   eingefuegt.
;
ENTXCH      .MD
LDA EX/PAGE+ETLINK,Y
BNE TNE##; ETQ nicht leer
STA TD/PAGE+THREADB,X; ETQ leer , DTD[X].BACK:=0
STA TD/PAGE+THREADF,X; DTD[X].FOR:=0
TXA
STA EX/PAGE+ETLINK,Y; ED[Y].ETLINK:=.DTD[X]
JMP EN##
;
TNE## STY B2PTR+1; ETQ nicht leer
NE## TAY; Y:=.DTD.next
LDA TD/PAGE+PRI,X
CMP TD/PAGE+PRI,Y
BCC FD##; DTD[Y].PRI >  DTD[X].PRI
LDA TD/PAGE+THREADF,Y
BNE NE##; naechster Task
TXA; Ende der Liste
STA TD/PAGE+THREADF,Y; DTD.last.FOR:=.DTD[X]
TYA
STA TD/PAGE+THREADB,X; DTD[X].BACK:=.DTD.last
LDA #0
STA TD/PAGE+THREADF,X; DTD[X].FOR:=0
JMP EN##
;
FD## LDA TD/PAGE+THREADB,Y; Einfuegen
STA TD/PAGE+THREADB,X; DTD[X].BACK:=DTD[Y].BACK
TYA
STA TD/PAGE+THREADF,X; DTD[X].FOR:=.DTD[Y]
TXA
STA TD/PAGE+THREADB,Y
LDA TD/PAGE+THREADB,X
BNE PR##; nicht 1. Task in Liste
TXA; erster Task  (einfuegen vor dem ersten Task)
LDY B2PTR+1
STA EX/PAGE+ETLINK,Y; ED[Y].ETLINK:=.DTD[X]
JMP EN##
;
PR## TAY; nicht erster Task in Liste Y:=DTD[X].BACK
TXA
STA TD/PAGE+THREADF,Y; DTD[Y].FOR:=.DTD[X]
EN##        .EQ *
.ME
;
;   Zerstoert: A,Y,P:N,Z,C,Zelle:B2PTR+1
;   Ausfuehrungszeit: ca. 70 us im Mittel (bei 1 Task)
;
.PG
;   ****** Remove Element aus ETQ ******
;   Aufruf: RMVXCH
;
;   Der zu entfern. DTD(I) muss in X-Reg., der ED(I)im Y-Register ueb.
;   werden.
;   Achtung: Es muss sichergestellt sein, dass DTD auch an diesem Exch.
;   haengt!
;   Funktion: Das DTD wird aus ETQ entfernt.
;
RMVXCH      .MD
LDA TD/PAGE+THREADF,X
BNE F##; ex. noch ein Nachfolger
LDA TD/PAGE+THREADB,X
BNE LA##; ex. noch ein Vorgaenger
STA EX/PAGE+ETLINK,Y; Liste leer
JMP EN##
;
LA## TAY; Vorgaenger existiert Y:=DTD[X].BACK
LDA #0
STA TD/PAGE+THREADF,Y; DTD[Y].FOR:=0
JMP EN##
;
F## LDA TD/PAGE+THREADB,X; ex. Nachfolger
BNE FA##; ex. Vorgaenger
LDA TD/PAGE+THREADF,X; ex. kein Vorgaenger
TAX; X:=DTD[X].FOR
STA EX/PAGE+ETLINK,Y; ED[Y]:=.DTD[X]
LDA #0
STA TD/PAGE+THREADB,X; DTD[X].BACK:=0
JMP EN##
;
FA## TAY; Y:=DTD[X].BACK
LDA TD/PAGE+THREADF,X
TAX; X:=DTD[X].FOR
STA TD/PAGE+THREADF,Y; DTD[Y].FOR:=.DTD[X]
TYA
STA TD/PAGE+THREADB,X; DTD[X].BACK:=.DTD[Y]
EN##        .EQ *
.ME
;
;   Zerstoert:A,X,Y,P:N,Z
;   Ausfuehrungszeit: ca. 30 us
.PG
