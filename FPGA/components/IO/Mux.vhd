-- Mux.vhd
-- Implement an 3-8 Multiplexer
	
library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

---------------------------------------------------

entity Mux is
	port(	
		dataOut8	:	out std_logic_vector(8-1 downto 0);
		clock		:	in std_logic;
		addrIn   	:	in std_logic_vector(3-1 downto 0);
		load	    :	in std_logic;
		clear		:	in std_logic
	);
end Mux;

----------------------------------------------------

architecture behv of Mux is

    signal Q_tmp: std_logic_vector(8-1 downto 0);

begin
	process(clock, load, clear)
	begin
		if clear = '0' then
		-- use 'range in signal assigment 
			--Q_tmp <= (Q_tmp'range => '0');
			Q_tmp <= (others => '1');
		elsif rising_edge(clock) then
			if load = '0' then
			with addrIn select
				Q_tmp <= "00000000" when "000",
				         load when "001",
				         load when "010",
				         load when "011",
				         load when "100",
				         load when "101",
				         load when "110",
				         load when "111";
			end if;
		end if;
	end process;

	-- concurrent statement
	dataOut8 <= Q_tmp;

end behv;
