-- AddressableLatch.vhd
-- Implement an 3-8 Multiplexer
    
library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

---------------------------------------------------

entity AddressableLatch is
    port(   
        clock       :   in std_logic;
        dataIn      :   in std_logic;
        addrIn      :   in std_logic_vector(3-1 downto 0);
        load        :   in std_logic;
        clear       :   in std_logic;
        dataOut8    :   out std_logic_vector(8-1 downto 0)
    );
end AddressableLatch;

----------------------------------------------------

architecture behv of AddressableLatch is

    signal Q_in0: std_logic;
    signal Q_in1: std_logic;
    signal Q_in2: std_logic;
    signal Q_in3: std_logic;
    signal Q_in4: std_logic;
    signal Q_in5: std_logic;
    signal Q_in6: std_logic;
    signal Q_in7: std_logic;
    signal Q_out0: std_logic;
    signal Q_out1: std_logic;
    signal Q_out2: std_logic;
    signal Q_out3: std_logic;
    signal Q_out4: std_logic;
    signal Q_out5: std_logic;
    signal Q_out6: std_logic;
    signal Q_out7: std_logic;

begin

    LATCH0: entity work.OutLatch1
    port map(
        dataIn1         => Q_in0,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out0
    );
    LATCH1: entity work.OutLatch1
    port map(
        dataIn1         => Q_in1,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out1
    );
    LATCH2: entity work.OutLatch1
    port map(
        dataIn1         => Q_in2,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out2
    );
    LATCH3: entity work.OutLatch1
    port map(
        dataIn1         => Q_in3,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out3
    );
    LATCH4: entity work.OutLatch1
    port map(
        dataIn1         => Q_in4,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out4
    );
    LATCH5: entity work.OutLatch1
    port map(
        dataIn1         => Q_in5,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out5
    );
    LATCH6: entity work.OutLatch1
    port map(
        dataIn1         => Q_in6,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out6
    );
    LATCH7: entity work.OutLatch1
    port map(
        dataIn1         => Q_in7,
        clock           => clock,
        load            => load,
        clear           => clear,
        latchOut        => Q_out7
    );

    process(load, clear)
    begin
        if clear = '0' then
        -- use 'range in signal assigment 
            --Q_tmp <= (Q_tmp'range => '0');
            Q_in0 <= '0';
            Q_in1 <= '0';
            Q_in2 <= '0';
            Q_in3 <= '0';
            Q_in4 <= '0';
            Q_in5 <= '0';
            Q_in6 <= '0';
            Q_in7 <= '0';
        elsif falling_edge(load) then
            case (addrIn) is
                when "000" => Q_in0 <= dataIn;
                when "001" => Q_in1 <= dataIn;
                when "010" => Q_in2 <= dataIn;
                when "011" => Q_in3 <= dataIn;
                when "100" => Q_in4 <= dataIn;
                when "101" => Q_in5 <= dataIn;
                when "110" => Q_in6 <= dataIn;
                when "111" => Q_in7 <= dataIn;
            end case;
        end if;
    end process;

    -- concurrent statement
    dataOut8 <= (Q_out7&Q_out6&Q_out5&Q_out4&Q_out3&Q_out2&Q_out1&Q_out0);

end behv;
