-- OutLatch1.vhd
-- Implement an n-bit output latch
	
library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

---------------------------------------------------

entity OutLatch1 is
generic(n: natural :=8);
	port(	
		dataIn1	    :	in std_logic;
		clock		:	in std_logic;
		load		:	in std_logic;
		clear		:	in std_logic;
		latchOut	:	out std_logic
	);
end OutLatch1;

----------------------------------------------------

architecture behv of OutLatch1 is

    signal Q_tmp: std_logic;

begin
	process(clock, load, clear)
	begin
		if clear = '0' then
			Q_tmp <= '1';
		elsif rising_edge(clock) then
			if load = '0' then
				Q_tmp <= dataIn1;
			end if;
		end if;
	end process;

	-- concurrent statement
	latchOut <= Q_tmp;

end behv;
