-- Grant Searle's Multicomp as described here:
-- http://searle.x10host.com/Multicomp/index.html
-- 
-- http://land-boards.com/blwiki/index.php?title=EP2C5-DB
--
-- es65 Implementation by Robert Offner 2021
-- 6502 CPU
--  1.08 MHz
--  128 KB SRAM (32k User RAM, 14k Common RAM, 8k + 1k ROM and 4x8k Banked ROM)
--
-- PS/2 Keyboard
--      F1 key switches between Intel HEX Loader and "Standard" MUC ROM
--          Default is Intel HEX Loader
--      F2 key switches standard Interrupt Vectors and modified IRQ Vectors
--          Default is standard Vectors -- see Dokumentation and Schematic for explaination
--
-- Memory Map
--      x0000-x7FFF - 32k User RAM
--      x8000-x9FFF - 8k Common ROM
--      xA000-xBFFF - 4*8k Banked ROM
--      xC000-xF7FF - 14k Common RAM
--      xFC00-xFFFF - 1k Common ROM on MUC
--  I/O (F800-FBFF)
--      xF800-FF803 - Floppy
--      xF814-FF817 - ACIA (6551) Serial 2 - Console
--      xF8D0-FF8DF - U7 (NMI Mask, Idle LED,...)
--      xF8E0-FF8E7 - Bank Switch
--      xF8E8-FF8EF - User Switch

library ieee;
use ieee.std_logic_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

entity es65_FPGA is
    port(
        KEY       : in std_logic_vector(3 downto 0);
        SW       : in std_logic_vector(9 downto 0);
        CLOCK_50     : in std_logic;
        
        UART_RXD     : in std_logic;
        UART_TXD     : out std_logic;
        --o_n_rts    : out std_logic;

        GPIO_0       : out std_logic_vector(35 downto 0);
        GPIO_1       : out std_logic_vector(35 downto 0);
        LEDR        : out std_logic_vector(7 downto 0);
        -- 512KB SRAM
        SRAM_DQ      : inout std_logic_vector(15 downto 0) := (others=>'Z');
        SRAM_ADDR    : out std_logic_vector(17 downto 0);
        SRAM_WE_N    : out std_logic := '1';
        SRAM_CE_N    : out std_logic := '1';
        SRAM_OE_N    : out std_logic := '1';
        SRAM_UB_N    : out std_logic := '1';
        SRAM_LB_N    : out std_logic := '0'
    );
end es65_FPGA;

architecture struct of es65_FPGA is

    signal w_R1W0           : std_logic;
    signal w_cpuAddress     : std_logic_vector(15 downto 0);
    signal w_cpuDataOut     : std_logic_vector(7 downto 0);
    signal w_cpuDataIn      : std_logic_vector(7 downto 0);
    
    signal w_MonitorRomData     : std_logic_vector(7 downto 0);
    signal w_BootRomData     : std_logic_vector(7 downto 0);
    signal w_P0_RomData     : std_logic_vector(7 downto 0);
    signal w_P1_RomData     : std_logic_vector(7 downto 0);
    signal w_P2_RomData     : std_logic_vector(7 downto 0);
    signal w_P3_RomData     : std_logic_vector(7 downto 0);
    signal w_VDUDataOut     : std_logic_vector(7 downto 0);
    signal w_aciaDataOut    : std_logic_vector(7 downto 0);
    signal w_aciaDataIn     : std_logic_vector(7 downto 0);
    signal w_ramDataOut     : std_logic_vector(7 downto 0);
    
    signal w_latch_User     : std_logic_vector(7 downto 0);
    signal w_latch_Bank     : std_logic_vector(7 downto 0);
    signal w_latch_U7       : std_logic_vector(7 downto 0);
    
    signal w_n_memWR        : std_logic;
    
    signal w_n_MonitorRomCS     : std_logic :='1';
    signal w_n_BootRomCS     : std_logic :='1';
    signal w_n_IRQ_address        : std_logic :='1';
    signal w_n_ramCS        : std_logic :='1';
    signal w_n_aciaCS       : std_logic :='1';
    signal w_io_enable       : std_logic :='0';
    signal w_n_userLatchCS  : std_logic :='1' ;
    signal w_n_bankLatchCS  : std_logic :='1' ;
    signal w_n_P0_RomCS     : std_logic :='1' ;
    signal w_n_P1_RomCS     : std_logic :='1' ;
    signal w_n_P2_RomCS     : std_logic :='1' ;
    signal w_n_P3_RomCS     : std_logic :='1' ;
    signal w_n_MuxCS        : std_logic :='1' ;
    signal w_n_VIACS        : std_logic :='1' ;
    
    signal w_cpuClkCt       : std_logic_vector(5 downto 0); 
    signal w_cpuClkPh       : std_logic; 
    signal w_cpuClk         : std_logic;
    signal w_ACIAClk        : std_logic;

    signal w_latBits        : std_logic_vector(7 downto 0);
    signal w_Sync           : std_logic;
    signal n_CPU_IRQ        : std_logic := '1';
    signal p_o_IRQ          : std_logic := '0';
    signal n_Interr8        : std_logic_vector(7 downto 0) := (others=>'1');
    signal n_InterrSRC      : std_logic_vector(2 downto 0) := (others=>'1');
    signal n_InterrSRC_latched : std_logic_vector(7 downto 0) := (others=>'1');

    signal serialClkCount   : unsigned(15 downto 0);
    signal clken_counter    :   unsigned(3 downto 0);
    signal mhz1_clken       :   std_logic; -- Used by 6522    
    signal mhz8_clken       :   std_logic; -- Used by 6522    
    signal mhz4_clken       :   std_logic; -- Used by 6522    
    signal mhz16_clken       :   std_logic;    
-- User VIA signals
    signal io_VIAData      :   std_logic_vector(7 downto 0);
    signal io_via_do_oe_n :   std_logic;
    signal io_via_ca1_in  :   std_logic := '0';
    signal io_via_ca2_in  :   std_logic := '0';
    signal io_via_ca2_out :   std_logic;
    signal io_via_ca2_oe_n    :   std_logic;
    --signal io_via_pa_in   :   std_logic_vector(7 downto 0);
    signal io_via_pa_out  :   std_logic_vector(7 downto 0)  := (others=>'1');
    signal io_via_pa_oe_n :   std_logic_vector(7 downto 0);
    signal io_via_cb1_in  :   std_logic := '0';
    signal io_via_cb1_out :   std_logic;
    signal io_via_cb1_oe_n    :   std_logic;
    signal io_via_cb2_in  :   std_logic := '0';
    signal io_via_cb2_out :   std_logic;
    signal io_via_cb2_oe_n    :   std_logic;
    signal io_via_pb_in   :   std_logic_vector(7 downto 0);
    signal io_via_pb_out  :   std_logic_vector(7 downto 0)  := (others=>'1');
    signal io_via_pb_oe_n :   std_logic_vector(7 downto 0);
    signal io_n_BootRomWE :   std_logic := '1';
begin
    -- ____________________________________________________________________________________
-- RAM Mapping
    SRAM_ADDR    <=
        "00"&(not w_latch_User(0))&w_cpuAddress(14 downto 0) when w_cpuAddress(15) = '0' else   -- User 0 gets 0-7fff, User1 8000-ffff (0000-7FFF)
        "01101"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "100" else -- Common ROM (8000-9FFF)
        "01000"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"FE"  else -- Bank 0 (A000-BFFF)
        "01001"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"FD"  else -- Bank 1 (A000-BFFF)
        "01010"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"FB"  else -- Bank 2 (A000-BFFF)
        "01011"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"F7"  else -- Bank 3 (A000-BFFF)
        "01"&w_cpuAddress                                    when w_cpuAddress > X"BFFF" and w_cpuAddress < X"FFFA" else -- Common RAM (C000-FFF9)
        "01"&X"FF"&"111100"&w_latch_U7(2)&w_cpuAddress(0)    when w_cpuAddress(15 downto 1) = X"FFF"&"101" and SW(1) = '1' else -- FFFA-B (NMI) gets mapped to FFF0-1 to FFF6-7
        "01"&X"FF"&"1111110"&w_cpuAddress(0)                 when w_cpuAddress(15 downto 1) = X"FFF"&"110" and SW(1) = '1' else -- FFFC-D (RST) gets mapped to FFFC-D to FFFE-F
        "01"&X"FF"&"1110"&n_InterrSRC_latched(2 downto 0)&w_cpuAddress(0) when w_cpuAddress(15 downto 1) = X"FFF"&"111" and SW(1) = '1' else -- FFFE (IRQ) gets mapped to FFE0 to FFEE
        "01"&w_cpuAddress                                    when w_cpuAddress > X"FFF9"  and SW(1) = '0' ;

    SRAM_DQ      <= "00000000"&w_cpuDataOut when ((w_R1W0='0') and (w_n_ramCS = '0')) else
                              (others => 'Z');
    SRAM_WE_N      <= not((not w_n_ramCS) and (not mhz1_clken) and (not w_R1W0));
    SRAM_OE_N      <= not((not w_n_ramCS) and (not mhz1_clken) and      w_R1W0);
    SRAM_CE_N      <= not((not w_n_ramCS) and (not mhz1_clken)); 
    SRAM_LB_N      <= not((not w_n_ramCS) and (not mhz1_clken)); 
-- Chip Selects
    w_n_ramCS       <=  '0' when w_cpuAddress(15) = '0'                else -- x0000-x7FFF (32KB) User RAM
                        '0' when w_cpuAddress(15 downto 14) = "10"     else -- x8000-xBFFF (16KB) Common ROM + Banked ROM
                        '0' when w_cpuAddress(15 downto 13) = "110"    else -- xC000-xDFFF (8KB)  Common RAM Part I
                        '0' when w_cpuAddress(15 downto 12) = "1110"   else -- xE000-xEFFF (4KB)  Common RAM Part II
                        '0' when w_cpuAddress(15 downto 11) = "11110"  else -- xF000-xF7FF (2KB)  Common RAM Part III
                        '0' when w_cpuAddress(15 downto 10) = "111111" and SW(0) = '1' else -- xFC00-xFFFF (1KB)  MUC ROM
                        '1';
    w_n_BootRomCS     <= '0' when   w_cpuAddress(15 downto 9) = "1111111" else '1'; -- xFE00-xFFFF (1KB)
    --
-- IO Area
    w_io_enable <= '1' when ((w_cpuAddress(15 downto 8)  = X"F8"))    else '0'; -- Xf8xx
    process (w_cpuAddress,w_io_enable)
    begin
        w_n_aciaCS <= '1';
        w_n_MuxCS <= '1';
        w_n_bankLatchCS <= '1';
        w_n_userLatchCS <= '1';
        w_n_VIACS <= '1';
        if w_io_enable = '1' then
            case w_cpuAddress(7 downto 2) is
            when "000101" => w_n_aciaCS <= '0';      -- F814 - F817
            when "110100" => w_n_MuxCS <= '0';       -- F8D0 - F8D3
            when "110101" => w_n_MuxCS <= '0';       -- F8D4 - F8D7
            when "110110" => w_n_MuxCS <= '0';       -- F8D8 - F8DB
            when "110111" => w_n_MuxCS <= '0';       -- F8DC - F8DF
            when "111000" => w_n_bankLatchCS <= '0'; -- F8E0 - F8E3
            when "111001" => w_n_bankLatchCS <= '0'; -- F8E4 - F8E7
            when "111010" => w_n_userLatchCS <= '0'; -- F8E8 - F8EB
            when "111011" => w_n_userLatchCS <= '0'; -- F8EC - F8EF
            when "111100" => w_n_VIACS <= '0';       -- F8F0 - F8F3
            when "111101" => w_n_VIACS <= '0';       -- F8F4 - F8F7
            when "111110" => w_n_VIACS <= '0';       -- F8F8 - F8FB
            when "111111" => w_n_VIACS <= '0';       -- F8FC - F8FF

            when others =>
                null;
            end case;
        end if;
    end process;
-- Bus Isolation
    w_cpuDataIn <=
        w_aciaDataOut    when w_n_aciaCS     = '0' else
        X"1F" when w_cpuAddress = X"F818" else          -- ACIA DIP Switches 19200Bd, 8n1
        --X"1F" when w_cpuAddress = X"F828" else          -- ACIA DIP Switches 19200Bd, 8n1
        SRAM_DQ(7 downto 0)   when w_n_ramCS      = '0' else
        io_VIAData       when w_n_VIACS      = '0' else
        w_BootRomData    when w_n_BootRomCS    = '0' and SW(0) = '0' else      -- HAS TO BE AFTER ANY I/O READS - why?
        x"FF";
-- Components
    VIA: entity work.M6522 -- U12 0xF8F0 (for Interrupt Masking, 50Hz Signal,..)
        port map (
        I_RS            => w_cpuAddress(3 downto 0),
        I_DATA          => w_cpuDataOut,
        O_DATA          => io_VIAData,
        O_DATA_OE_L     => io_via_do_oe_n,
        I_RW_L          => w_R1W0,
        I_CS1           => '1',
        I_CS2_L         => w_n_VIACS,
        O_IRQ_L         => n_Interr8(6),  -- map to IRQ6
        -- port a
        I_CA1           => io_via_ca1_in,
        I_CA2           => '0', --io_via_ca2_in,
        I_PA            => (others => '0'), --io_via_pa_in,
        O_PA            => io_via_pa_out,
        O_PA_OE_L       => io_via_pa_oe_n,
        -- port b
        I_CB1           => '0', --io_via_cb1_in,
        I_CB2           => '0', --io_via_cb2_in,
        I_PB            => io_via_pb_in,
        O_PB            => io_via_pb_out,
        O_PB_OE_L       => io_via_pb_oe_n,
        I_P2_H          => not mhz1_clken,
        RESET_L         => KEY(0),
        ENA_4           => '1', --not mhz4_clken,
        CLK             => mhz4_clken --not CLOCK_50
        );
    
    io_via_ca1_in <= io_via_pb_out(7);

    IRQENC: entity work.V74x148 -- U10 for Interrupts
        port map(
            --EI_L            => mhz1_clken,
            I_L             => (n_Interr8 or io_via_pa_out), -- mask Interrupts with 6522s Port A
            A_L             => n_InterrSRC,
            EO_L            => p_o_IRQ --n_CPU_IRQ
                -- GS_L            => 
        );

    --GPIO_0(0) <= not p_o_IRQ;              --DEBUG Output all the Interrupts (connected to MCU)
    --GPIO_0(3) <= '0' when (w_cpuAddress = X"8810") else '1';
    n_CPU_IRQ <= not p_o_IRQ;
    --GPIO_0(3) <= n_Interr8(6);
    --GPIO_0(1) <= n_Interr8(5);             -- DEBUG Output ACIAs Interrupt
    --GPIO_1 <= n_Interr8 or io_via_pa_out; --DEBUG mask Interrupts with 6522s Port A
    --GPIO_0(2) <= io_via_pb_out(7);

    CPU : entity work.T65
        port map(
            Enable          => '1',           -- 
            Mode            => "00",          -- "00" => 6502, "01" => 65C02, "10" => 65C816
            Res_n           => KEY(0),     -- 
            clk             => mhz1_clken,      -- CPU uses rising edge
            Rdy             => '1',           -- 
            Abort_n         => '1',           -- 
            IRQ_n           => n_CPU_IRQ,      -- 
            NMI_n           => '1',           -- 
            SO_n            => '1',           -- 
            R_w_n           => w_R1W0,        -- 
            Sync            => w_Sync,
            A(15 downto 0)  => w_cpuAddress,  -- 
            DI              => w_cpuDataIn,   -- 
            DO              => w_cpuDataOut   -- 
        );

    UART : entity work.ACIA -- 0xF814 for User 0 (only one ACIA for now)
        port map(
            RESET   => KEY(0),
            PHI2    => not mhz1_clken,
            CS      => w_n_aciaCS ,
            RWN     => w_R1W0,
            RS      => w_cpuAddress(1 downto 0),
            DATAIN  => w_cpuDataOut,
            DATAOUT => w_aciaDataOut,
            XTLI    => w_ACIAClk,
            --RTSB    => o_n_rts,
            CTSB    => '0',
            RXD     => UART_RXD,
            TXD     => UART_TXD,
            IRQn    => n_Interr8(5) -- map to IRQ5
        );

    MUX: entity work.AddressableLatch -- U7 0xF8D0-0xF8DF (for NMI Masking, Idle LED, NMI Vector shift,...)
        port map(
            dataIn          => w_cpuAddress(3),
            clock           => not mhz1_clken or w_R1W0,
            addrIn          => w_cpuAddress(2 downto 0),
            load            =>  w_n_MuxCS,
            clear           => '1', --KEY(0)
            dataOut8        => w_latch_U7
        );
    
    USRLATCH: entity work.OutLatch -- U5 0xF8E8 (User Switch)
        port map(
            dataIn8         => w_cpuDataOut,
            clock           => mhz1_clken or w_R1W0,
            load            => w_n_userLatchCS,
            clear           => '1', --KEY(0), WHY doesn't this work??
            latchOut        => w_latch_User
        );

    IRQLATCH: entity work.OutLatch -- U15 (IRQ Latch)
        port map(
            dataIn8         => "11111"&n_InterrSRC,
            clock           => not mhz1_clken,
            load            => not w_Sync,
            clear           => '1', --KEY(0), WHY doesn't this work??
            latchOut        => n_InterrSRC_latched
        );

    BNKLATCH: entity work.OutLatch -- U4 0xF8E0 (Bank Switch)
        port map(
            dataIn8         => w_cpuDataOut,
            clock           => mhz1_clken or w_R1W0,
            load            => w_n_bankLatchCS,
            clear           => '1', --KEY(0),
            latchOut        => w_latch_Bank -- (when KEY(0) = '0') else (others => '1')--w_latch_Bank
        );

    BOOTROM: entity work.intelHEX_dl -- 512B 0xFE00-0xFFFF (primary downloader, can be switched out with "F2")
        port map(
            address => w_cpuAddress(8 downto 0),
            clock   => CLOCK_50,
            q       => w_BootRomData
        );
        
-- SUB-CIRCUIT CLOCK SIGNALS 
    process(mhz16_clken,KEY(0))
    begin
        if KEY(0) = '0' then
            clken_counter <= (others => '0');
        elsif rising_edge(mhz16_clken) then
            clken_counter <= clken_counter + 1;
            
        end if;
    end process;

    mhz4_clken <= clken_counter(1); -- 1,5,9,13 16.6/4 = 4.2
    mhz1_clken <= clken_counter(3);
    
    process (CLOCK_50)
    begin
        if rising_edge(CLOCK_50) then

            if w_cpuClkCt < 2 then -- 4 = 10MHz, 3 = 12.5MHz, 2=16.6MHz, 1=25MHz
                w_cpuClkCt <= w_cpuClkCt + 1;
            else
                w_cpuClkCt <= (others=>'0');
            end if;
            if w_cpuClkCt < 2 then -- 2 when 10MHz, 2 when 12.5MHz, 2 when 16.6MHz, 1 when 25MHz
                mhz16_clken <= '0';
                --GPIO_0(3)  <=  '0';
            else
                mhz16_clken <= '1';
                --GPIO_0(3)  <=  '1';
            end if; 
        end if; 
    end process;

    process (CLOCK_50)
    begin
        if rising_edge(CLOCK_50) then
            -- 1.8432 MHz clock (ish) from 50 MHz
            if (serialClkCount < 27) then
                serialClkCount <= serialClkCount + 1;
            else
                serialClkCount <= (others => '0');
            end if;
            
            if (serialClkCount < 14) then
                w_ACIAClk <= '1';
            else
                w_ACIAClk <= '0';
            end if;
        end if;
    end process;



  process (CLOCK_50)
  begin
      if (rising_edge(CLOCK_50)) then
        --GPIO_1 <= w_latch_U7;
        LEDR(0) <= w_latch_U7(1); -- Idle LED
        --LEDR(1) <= not w_latch_User(0);
        LEDR(2) <= w_latch_Bank(0);
        LEDR(1) <= (SW(1) and SW(0));
      end if;
  end process;
     
end;
