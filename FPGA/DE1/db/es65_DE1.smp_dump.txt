
State Machine - |es65_FPGA|ACIA:UART|ACIA_TX:C_TX|r_tx_fsm
Name r_tx_fsm.state_Stop2 r_tx_fsm.state_Stop r_tx_fsm.state_Parity r_tx_fsm.state_Data r_tx_fsm.state_Start r_tx_fsm.state_Idle 
r_tx_fsm.state_Idle 0 0 0 0 0 0 
r_tx_fsm.state_Start 0 0 0 0 1 1 
r_tx_fsm.state_Data 0 0 0 1 0 1 
r_tx_fsm.state_Parity 0 0 1 0 0 1 
r_tx_fsm.state_Stop 0 1 0 0 0 1 
r_tx_fsm.state_Stop2 1 0 0 0 0 1 

State Machine - |es65_FPGA|ACIA:UART|ACIA_RX:C_RX|r_rx_fsm
Name r_rx_fsm.state_Stop2 r_rx_fsm.state_Stop r_rx_fsm.state_Parity r_rx_fsm.state_Data r_rx_fsm.state_Start r_rx_fsm.state_Idle 
r_rx_fsm.state_Idle 0 0 0 0 0 0 
r_rx_fsm.state_Start 0 0 0 0 1 1 
r_rx_fsm.state_Data 0 0 0 1 0 1 
r_rx_fsm.state_Parity 0 0 1 0 0 1 
r_rx_fsm.state_Stop 0 1 0 0 0 1 
r_rx_fsm.state_Stop2 1 0 0 0 0 1 
