; ----------------- assembly instructions ---------------------------- 
;
; this is a subroutine library only
; it must be included in an executable source file
;
;
;*** I/O Locations *******************************
; define the i/o address of the ACIA1 chip
;*** 6551 CIA ************************
;ACIA1dat       :=     $F810
;ACIA1Sta       :=     $F811
;ACIA1cmd       :=     $F812
;ACIA1ctl       :=     $F813
ACIA1dat       :=     $F814
ACIA1Sta       :=     $F815
ACIA1cmd       :=     $F816
ACIA1ctl       :=     $F817
VIAMUC_VIAORB   := $F8F0
VIAMUC_VIAORA   := $F8F1
VIAMUC_DRB      := $F8F2
VIAMUC_DRA      := $F8F3
MUC6522_T1L     := $F8F4
MUC6522_T1H     := $F8F5
MUC6522_T1_LatchL:= $F8F6
MUC6522_T1_LatchH:= $F8F7
MUC6522_T2L     := $F8F8
MUC6522_T2H     := $F8F9
MUC6522_SR      := $F8FA
MUC6522_ACR     := $F8FB
MUC6522_PCR     := $F8FC
MUC6522_IFR     := $F8FD
MUC6522_IER     := $F8FE
MUC6522_ORA     := $F8FF
OUTMAP_F8E0     := $F8E0
OUTUMAP_F8E8    := $F8E8
;***********************************************************************
; 6551 I/O Support Routines
;
Per_init:
         lda $00
         sta VIAMUC_VIAORA
         lda #$FE
         sta OUTMAP_F8E0
         lda #$7E
         sta OUTUMAP_F8E8         ; User 0
         ;lda $FF
         ;sta VIAMUC_VIAORA
         lda $70
         sta VIAMUC_DRB
         sta VIAMUC_VIAORB
         ;lda $ff
         ;sta VIAMUC_DRA      ;PORTA all Output 
         ;lda #$C0
         ;sta MUC6522_ACR  ;T1 Free Running Mode, Output to PB7 
         ;lda #$0E
         ;sta MUC6522_T1L
         ;lda #$27
         ;sta MUC6522_T1H
         ;lda #$82
         ;sta MUC6522_IER        
ACIA1portset:
               lda #$1f               ; 1E - 9k6, 1F - 19.2K/8/1
               sta ACIA1ctl           ; control reg 
               ;lda #$10
               ;sta ACIA1Sta
               lda #$0B               ; N parity/echo off/rx int off/ dtr active low
               ;lda #$09               ; N parity/echo off/rx int off/ dtr active low/ RX Interrupt
               sta ACIA1cmd           ; command reg 
               rts                      ; done
;
; input chr from ACIA1 (waiting)
;

ACIA1_Input:
               lda   ACIA1Sta           ; Serial port status             
               and   #$08               ; is recvr full
               beq   ACIA1_Input        ; no char to get
               lda   ACIA1dat           ; get chr
               rts                      ;
;
; non-waiting get character routine 
;

ACIA1_Scan:    clc
               lda   ACIA1Sta           ; Serial port status
               and   #$08               ; mask rcvr full bit
               beq   ACIA1_scan2
               lda   ACIA1dat           ; get chr
	      sec
ACIA1_scan2:   rts
;
; output to OutPut Port
;

ACIA1_Output:  PHA                      ; save registers
ACIA1_Out1:    lda   ACIA1Sta           ; serial port status
               and   #$10               ; is tx buffer empty
               beq   ACIA1_Out1         ; no
               pla                      ; get chr
               sta   ACIA1dat           ; put character to Port
               rts                      ; done
;
;end of file
